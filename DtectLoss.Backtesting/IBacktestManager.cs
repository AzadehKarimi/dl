﻿using System.Collections.Generic;
using DtectLoss.Common;
using DtectLoss.Common.Enums;
using DtectLossRealtime.Enums;
using DtectLossRealtime.Models;

namespace DtectLoss.Backtesting
{
    public interface IBacktestManager
    {
        void Initialize(string testRunDateString);
        (bool, double) RunScenarioToCompletion(ScenarioConfig scenario, string scenarioDataFilePath);
        BacktestEvaluateOutput EvaluateAndFinalize();
    }
}