﻿using System;
using System.Collections.Generic;
using DtectLoss.Common;
using DtectLossRealtime.Models;
using Microsoft.Extensions.Logging;

namespace DtectLoss.Backtesting
{
    public class ConsoleTimeseriesLogging : ITimeseriesLogging
    {
        private readonly ILogger<ConsoleTimeseriesLogging> _logger;

        public ConsoleTimeseriesLogging(ILogger<ConsoleTimeseriesLogging> logger)
        {
            _logger = logger;
        }

        public void Initialize(string dbName, string url, string userName, string pwd)
        {
            _logger.LogInformation("Initialized console timeseries logger.");
        }


        public void LogAllPoints(string testRun, DateTime timeStamp,
            Dictionary<string, DataVariable> oRawDataNonstandardUnits, Dictionary<string, double> oRawData,
            WashedData oWashedData, FlowInOutOutput oFlowInOutOutput,
            PresSPexpOutput oPresSPexpOutput, GainDetectorOutput oGainDetectorOutput, AlgResult oAlgResult,
            AlgData oAlgData,
            EvaluateRealTimeOutput oEvaluateRealTimeOutput, FlowlineAndPitOutput oFlowlineAndPitOutput,
            TripTankOutput oVolExpTripOutput, WashedProcessOutput oWashedProcessOutput,
            RealtimeOutputStatus outputStatus)
        {
        }

        public void LogTrueData(string testRun, DateTime timeStamp, Dictionary<string, DataVariable> data)
        {
        }
    }
}