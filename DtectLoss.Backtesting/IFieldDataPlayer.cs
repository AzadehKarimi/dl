﻿using System.Collections.Generic;
using DtectLoss.Common;

namespace DtectLoss.Backtesting
{
    public interface IFieldDataPlayer
    {
        void Initialize(Dictionary<string, DataVariableInfo> data, string[] desiredProcessTags);
        void AsyncPlay();
        Dictionary<string, string> RawDataUnits { get; }
        double StartTime { get; }
        double AbsTime { get; }
        double SimProgress { get; }
        double SimDuration { get; }
        IEnumerable<string> VariableNames { get; }
        Dictionary<string, DataVariable> Output { get; }
        Dictionary<string, int> CurrentIndex { get; }
        Dictionary<string, int> NDataPoints { get; }
    }
}