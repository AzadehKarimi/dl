﻿using System;
using DtectLoss.Common;
using DtectLoss.FileHandling;
using DtectLossRealtime;
using DtectLossRealtime.Calculations;
using DtectLossRealtime.EKDAlgoritm;
using DtectLossRealtime.Helpers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace DtectLoss.Backtesting
{
    public static class ServiceConfiguration
    {
        public static void ConfigureServices(IServiceCollection services, IConfigurationSection backTestingConfiguration)
        {
            services.AddLogging(configure => configure.AddSerilog()).AddSingleton<BacktestScenarioRunner>();
            services.AddScoped<IBacktestScenarioManager, BacktestScenarioRunner>();
            services.AddTransient<IBacktestManager, BacktestManager>();
            services.AddTransient<IFieldDataPlayer, FieldDataPlayer>();
            services.AddScoped<IExcelReader, ExcelReader>();
            services.AddScoped<ICsvDatafileReader, CsvDatafileReader>();
            services.AddScoped<IDefaultUnitsMappingReader, DefaultUnitsMappingReader>();

            bool useInfluxDbAsTimeseriesLogging = Convert.ToBoolean(backTestingConfiguration["useInfluxDbAsTimeseriesLogging"]);
            if (useInfluxDbAsTimeseriesLogging)
            {
                services.AddScoped<ITimeseriesLogging, InfluxTimeseriesLogging>();
            }
            else
            {
                services.AddScoped<ITimeseriesLogging, ConsoleTimeseriesLogging>();
            }

            services.AddTransient<IDtectLossRealtime, DtectLossRealtime.DtectLossRealtime>();
            services.AddTransient<IProcess, DtectLossRealtime.Process>();
            services.AddTransient<IBacktestScenarioReporting, BacktestScenarioReporting>();
            services.AddTransient<BacktestEvaluate, BacktestEvaluate>();
            services.AddTransient<EvaluateRealTime, EvaluateRealTime>();
            services.AddTransient<StateMachine, StateMachine>();
            services.AddTransient<Describe, Describe>();
            services.AddTransient<ExpectedFlowOutFromFlowIn, ExpectedFlowOutFromFlowIn>();
            services.AddTransient<ExpectedStandpipePres, ExpectedStandpipePres>();
            services.AddTransient<ExpectedTripTankVol, ExpectedTripTankVol>();
            services.AddTransient<FlowlineAndPit, FlowlineAndPit>();
            services.AddTransient<GainDetector, GainDetector>();
            services.AddTransient<FlowBackRecorder, FlowBackRecorder>();
            services.AddTransient<EkdAlgFactory, EkdAlgFactory>();
            services.AddTransient<TimeAlign, TimeAlign>();
            services.AddTransient<StandpipePressureDelay, StandpipePressureDelay>();
            services.AddTransient<PaddleConverter, PaddleConverter>();
            services.AddTransient<PiecewiseLinFit, PiecewiseLinFit>();
            services.AddTransient<LinReg, LinReg>();
            services.AddOptions();
            services.Configure<BacktestingOptions>(backTestingConfiguration);
        }
    }
}
