﻿using System;
using System.Collections.Generic;
using DtectLoss.Common;
using DtectLossRealtime;
using DtectLossRealtime.Models;

namespace DtectLoss.Backtesting
{
    public interface ITimeseriesLogging
    {
        void Initialize(string dbName, string url, string userName, string pwd);
        void LogAllPoints(string testRun, DateTime timeStamp, Dictionary<string, DataVariable> oRawDataNonstandardUnits,
            Dictionary<string, double> oRawData, WashedData oWashedData, FlowInOutOutput oFlowInOutOutput,
            PresSPexpOutput oPresSPexpOutput, GainDetectorOutput oGainDetectorOutput, AlgResult oAlgResult,
            AlgData oAlgData, EvaluateRealTimeOutput oEvaluateRealTimeOutput,
            FlowlineAndPitOutput oFlowlineAndPitOutput, TripTankOutput oVolExpTripOutput,
            WashedProcessOutput oWashedProcessOutput, RealtimeOutputStatus outputStatus);
        void LogTrueData(string testRun, DateTime timeStamp, Dictionary<string, DataVariable> data);
    }
}