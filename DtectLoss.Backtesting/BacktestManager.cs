﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using DtectLoss.Common;
using DtectLoss.Common.Enums;
using DtectLoss.FileHandling;
using DtectLossRealtime;
using DtectLossRealtime.Enums;
using DtectLossRealtime.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Serilog.Context;

namespace DtectLoss.Backtesting
{
    public class BacktestManager : IBacktestManager
    {
        private readonly IBacktestScenarioReporting _backtestScenarioReporting;
        private readonly IDtectLossRealtime _dtectLossRealtime;
        private readonly IFieldDataPlayer _fieldDataPlayer;
        private readonly ICsvDatafileReader _csvDatafileReader;
        private readonly BacktestingOptions _options;
        private readonly ILogger<BacktestManager> _logger;
        private string _simName = string.Empty;
        private string _testRun = string.Empty;
        private bool _didLoadOk;
        private const double DisplayEveryXPercent = 1;
        private double _lastDisplayPercent = 0;
        private ScenarioConfig? _scenario;
        private int _stepCounter = 0;
        private string _testRunDateString = string.Empty;
        private readonly Stopwatch _toc;
        private readonly Stopwatch _tocTotal;
        private TimeSpan _lastElapsed;

        public BacktestManager(IBacktestScenarioReporting backtestScenarioReporting, IDtectLossRealtime dtectLossRealtime, IFieldDataPlayer fieldDataPlayer, ILogger<BacktestManager> logger, ICsvDatafileReader csvDatafileReader, IOptions<BacktestingOptions> options)
        {
            _backtestScenarioReporting = backtestScenarioReporting;
            _dtectLossRealtime = dtectLossRealtime;
            _fieldDataPlayer = fieldDataPlayer;
            _csvDatafileReader = csvDatafileReader;
            _options = options.Value;
            _logger = logger;
            _toc = new Stopwatch();
            _tocTotal = Stopwatch.StartNew();
        }

        public void Initialize(string testRunDateString)
        {
            _testRunDateString = testRunDateString; 
        }
        public (bool, double) RunScenarioToCompletion(ScenarioConfig scenario, string scenarioDataFilePath)
        {
            try
            {
                _scenario = scenario;
                _logger.LogInformation($"Start loading scenario: {scenarioDataFilePath}");
                LoadSim(scenario, scenarioDataFilePath);
                using var logContext = LogContext.PushProperty("LoggingTestrun", _testRun);
                if (_didLoadOk)
                {
                    while (_fieldDataPlayer.SimProgress < _options.percentToRun)
                    {
                        _toc.Restart();
                        StepAllAlgorithmsAndSave();
                        _toc.Stop();
                        _lastElapsed += _toc.Elapsed;
                        DisplayProgress();
                    }

                    var analysis = _dtectLossRealtime.CheckData();
                    LogInformationMultiLine(analysis);
                }

                _tocTotal.Stop();
                _logger.LogInformation(
                    $"Time to do rig simulation: {_tocTotal.Elapsed.Minutes} minutes, {_tocTotal.Elapsed.Seconds} seconds.");
                return (_didLoadOk, _fieldDataPlayer.SimProgress);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                throw;
            }
        }


        public void StepAllAlgorithmsAndSave()
        {

            _fieldDataPlayer.AsyncPlay();
            var rawData = _fieldDataPlayer.Output;
            var rawDataWithChosenTags = ChooseTagsForRawData(rawData);
            var dtectLossRealtimeOutput = _dtectLossRealtime.StepAllAlgorithms(rawDataWithChosenTags);
            _backtestScenarioReporting.SaveIteration(_testRun, _fieldDataPlayer.AbsTime, dtectLossRealtimeOutput);
            _stepCounter++;
        }

        private Dictionary<string, DataVariable> ChooseTagsForRawData(Dictionary<string, DataVariable> rawData)
        {
            rawData[V.VolTripTank] = rawData[_scenario!.UsedTripTankMeasTagName];
            if (_scenario.UsedFlowMeasTagName.Equals("flowOutMissing"))
                rawData[V.FlowOut] = new DataVariable(rawData[V.VolTripTank].T, CommonConstants.MissingValueReplacement);
            else
                rawData[V.FlowOut] = rawData[_scenario.UsedFlowMeasTagName]; //Flow tag is missing in some datasets.

            rawData["pitVolume"] = rawData[_scenario.UsedPitVolMeasTagName];
            return rawData;
        }


        private void DisplayProgress()
        {
            if (_fieldDataPlayer.SimProgress >= _lastDisplayPercent + DisplayEveryXPercent)
            {
                _logger.LogDebug($"{_simName} : {_fieldDataPlayer.SimProgress:0.##} percent complete - Duration: {_lastElapsed.Seconds} s {_lastElapsed.Milliseconds} ms");
                _lastElapsed = TimeSpan.Zero;
                _lastDisplayPercent = _fieldDataPlayer.SimProgress;
            }
        }

        private void LoadSim(ScenarioConfig scenario, string scenarioDataFilePath)
        {
            var data = _csvDatafileReader.ReadCsvFileFromZip(scenarioDataFilePath);
            var trueData = _csvDatafileReader.ReadTrueCsvFileFromZip(scenarioDataFilePath);
            var simName = Path.GetFileName(scenarioDataFilePath)?.Replace(".zip", "");
            _simName = simName ?? "Sim name error";
            _testRun = $"{_simName.RenameToValidString()}_Date_{_testRunDateString}";
            double areaDppoohM2;
            double areaDprihM2;
            _fieldDataPlayer.Initialize(data, _options.NameDesiredProcessTags!);
            var unitsOfProvidedTags = GetUnitsOfProvidedTags(_fieldDataPlayer.RawDataUnits, scenario);
            (unitsOfProvidedTags, areaDppoohM2, areaDprihM2) = GetAreaUnitsAndValues(unitsOfProvidedTags, scenario);

            _dtectLossRealtime.Initialize(scenario.AlgorithmsToUse![0], unitsOfProvidedTags, _fieldDataPlayer.StartTime, areaDppoohM2, areaDprihM2);
            _backtestScenarioReporting.Initialize(trueData);
            _didLoadOk = true;
        }

        private static Dictionary<string, string> GetUnitsOfProvidedTags(Dictionary<string, string> rawDataUnits, ScenarioConfig scenario)
        {
            rawDataUnits = CopyFromExistingKey(rawDataUnits, "volTripTank", scenario.UsedTripTankMeasTagName);
            rawDataUnits = CopyFromExistingKey(rawDataUnits, "flowOut", scenario.UsedFlowMeasTagName);
            rawDataUnits = CopyFromExistingKey(rawDataUnits, "pitVolume", scenario.UsedPitVolMeasTagName);
            return rawDataUnits;
        }
        private static (Dictionary<string, string>, double, double) GetAreaUnitsAndValues(Dictionary<string, string> rawDataUnits, ScenarioConfig scenario)
        {
            rawDataUnits["areaDPPOOH_m2"] = "m2";
            rawDataUnits["areaDPRIH_m2"] = "m2";
            if (!string.IsNullOrEmpty(scenario.AreaDppoohM2) && !string.IsNullOrEmpty(scenario.AreaDprihM2))
            {
                return (rawDataUnits, Convert.ToDouble(scenario.AreaDppoohM2), Convert.ToDouble(scenario.AreaDprihM2));
            }
            return (rawDataUnits, 0.010, 0.015);
        }

        public static Dictionary<string, T> CopyFromExistingKey<T>(Dictionary<string, T> dictionaryToUpdate, string key, string value)
        {
            if (!dictionaryToUpdate.ContainsKey(value))
            {
                return dictionaryToUpdate;

            }
            dictionaryToUpdate[key] = dictionaryToUpdate[value];
            return dictionaryToUpdate;
        }

        public BacktestEvaluateOutput EvaluateAndFinalize()
        {
            return _backtestScenarioReporting.EvaluateCurrentRun(_scenario!, _testRun, _simName);
        }
        private void LogInformationMultiLine(string line)
        {
            var lines = line.Split(new[] { '\r', '\n' });
            foreach (var s in lines)
            {
                _logger.LogInformation(s);
            }
        }
    }
}