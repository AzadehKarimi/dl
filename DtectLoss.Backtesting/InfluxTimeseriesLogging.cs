﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdysTech.InfluxDB.Client.Net;
using DtectLoss.Common;
using DtectLossRealtime.Models;
using Microsoft.Extensions.Logging;
using static DtectLoss.Common.CommonConstants;
using static DtectLoss.Common.Helpers;

namespace DtectLoss.Backtesting
{
    public class InfluxTimeseriesLogging : ITimeseriesLogging
    {
        private readonly ILogger<InfluxTimeseriesLogging> _logger;
        private InfluxDBClient? _client;
        private string _dbName = string.Empty;

        public InfluxTimeseriesLogging(ILogger<InfluxTimeseriesLogging> logger)
        {
            _logger = logger;
        }

        public async void Initialize(string dbName, string url, string userName, string pwd)
        {
            _dbName = dbName;
            _client = new InfluxDBClient(url, userName, pwd);
            await _client.CreateDatabaseAsync(dbName);
            _logger.LogInformation("Initialized influx logger");
        }

        public async void LogAllPoints(string testRun, DateTime timeStamp,
            Dictionary<string, DataVariable> oRawDataNonstandardUnits, Dictionary<string, double> oRawData,
            WashedData oWashedData, FlowInOutOutput oFlowInOutOutput,
            PresSPexpOutput oPresSPexpOutput, GainDetectorOutput oGainDetectorOutput, AlgResult oAlgResult,
            AlgData oAlgData,
            EvaluateRealTimeOutput oEvaluateRealTimeOutput, FlowlineAndPitOutput oFlowlineAndPitOutput,
            TripTankOutput oVolExpTripOutput, WashedProcessOutput oWashedProcessOutput,
            RealtimeOutputStatus outputStatus)
        {
            var points = new List<InfluxDatapoint<InfluxValueField>>();
            points.Add(CreateRawDataNonStandardUnitsPoint(testRun, timeStamp, oRawDataNonstandardUnits));
            points.Add(CreateRawDataPoint(testRun, timeStamp, oRawData));
            points.Add(CreateWashedDataPoint(testRun, timeStamp, oWashedData));
            points.Add(CreateFlowInOutPoint(testRun, timeStamp, oFlowInOutOutput));
            points.Add(CreatePresSPExpPoint(testRun, timeStamp, oPresSPexpOutput));
            points.Add(CreateGainDetectorPoint(testRun, timeStamp, oGainDetectorOutput));
            points.Add(CreateAlgResultPoint(testRun, timeStamp, oAlgResult));
            points.Add(CreateAlgDataPoint(testRun, timeStamp, oAlgData));
            points.Add(CreateEvaluateRealtimePoint(testRun, timeStamp, oEvaluateRealTimeOutput));
            points.Add(CreateFlowlineAndPitPoint(testRun, timeStamp, oFlowlineAndPitOutput));
            points.Add(CreateVolExpTripPoint(testRun, timeStamp, oVolExpTripOutput));
            points.Add(CreateWashedProcessPoint(testRun, timeStamp, oWashedProcessOutput));
            points.Add(CreateRealtimeOutputStatusPoint(testRun, timeStamp, outputStatus));
            await _client!.PostPointsAsync(_dbName, points);
        }

        public async void LogTrueData(string testRun, DateTime timeStamp, Dictionary<string, DataVariable> data)
        {
            var points = new List<InfluxDatapoint<InfluxValueField>>();
            points.Add(CreateTrueDataPoint(testRun, timeStamp, data));
            await _client!.PostPointsAsync(_dbName, points);
        }
        private InfluxDatapoint<InfluxValueField> CreateRawDataNonStandardUnitsPoint(string testRun, DateTime timeStamp, Dictionary<string, DataVariable> value)
        {
            var valMixed = new InfluxDatapoint<InfluxValueField>();
            valMixed.UtcTimestamp = timeStamp;
            valMixed.Tags.Add("TestDate", timeStamp.ToShortDateString());
            valMixed.Tags.Add("TestTime", timeStamp.ToShortTimeString());
            valMixed.Tags.Add("TestRun", testRun);

            var v = value.ToDictionary(x => x.Key, val => val.Value.V);
            foreach (var dataVariable in v)
            {
                var tmp = double.IsNaN(dataVariable.Value) ? MissingValueReplacement : dataVariable.Value ;
                valMixed.Fields.Add(dataVariable.Key, new InfluxValueField(tmp));
            }

            valMixed.MeasurementName = "RawdataNonStandardUnits";
            valMixed.Precision = TimePrecision.Seconds;
            return valMixed;
        }

        public InfluxDatapoint<InfluxValueField> CreateTrueDataPoint(string testRun, DateTime timeStamp, Dictionary<string, DataVariable> value)
        {
            var valMixed = new InfluxDatapoint<InfluxValueField>();
            valMixed.UtcTimestamp = timeStamp;
            valMixed.Tags.Add("TestDate", timeStamp.ToShortDateString());
            valMixed.Tags.Add("TestTime", timeStamp.ToShortTimeString());
            valMixed.Tags.Add("TestRun", testRun);

            foreach (var (key, val) in value)
            {
                var tmp = double.IsNaN(val.V) ? MissingValueReplacement : val.V ;
                valMixed.Fields.Add(key, new InfluxValueField(tmp));
            }

            valMixed.MeasurementName = "TrueData";
            valMixed.Precision = TimePrecision.Seconds;
            return valMixed;
        }

        public InfluxDatapoint<InfluxValueField> CreateRawDataPoint(string testRun, DateTime timeStamp, Dictionary<string, double> value)
        {
            //_logger.LogInformation($"Logging measurement {name} with time {timeStamp}");
            var valMixed = new InfluxDatapoint<InfluxValueField>();
            valMixed.UtcTimestamp = timeStamp;
            valMixed.Tags.Add("TestDate", timeStamp.ToShortDateString());
            valMixed.Tags.Add("TestTime", timeStamp.ToShortTimeString());
            valMixed.Tags.Add("TestRun", testRun);

            foreach (var (key, val) in value)
            {
                var tmp = double.IsNaN(val) ? MissingValueReplacement : val ;
                valMixed.Fields.Add(key, new InfluxValueField(tmp));
            }

            valMixed.MeasurementName = "RawData";
            valMixed.Precision = TimePrecision.Seconds;
            //valMixed.Retention = new InfluxRetentionPolicy() { Name = "Test2" };
            //var r = await client.PostPointAsync(dbName, valMixed);
            //_logger.LogInformation($"Finished logging measurement {name} with time {timeStamp}");
            return valMixed;
        }

        public InfluxDatapoint<InfluxValueField> CreateWashedDataPoint(string testRun, DateTime timeStamp, WashedData washedData)
        {
            var valMixed = new InfluxDatapoint<InfluxValueField>();
            valMixed.UtcTimestamp = timeStamp;
            valMixed.Tags.Add("TestDate", timeStamp.ToShortDateString());
            valMixed.Tags.Add("TestTime", timeStamp.ToShortTimeString());
            valMixed.Tags.Add("TestRun", testRun);

            valMixed.Fields.Add("IsDownlinking", new InfluxValueField(washedData.IsDownlinking));
            valMixed.Fields.Add("IsCalibratedMeasFlowOut", new InfluxValueField(BoolToInt(washedData.IsCalibratedMeasFlowOut)));
            valMixed.Fields.Add("IsCalibratingPaddleBias", new InfluxValueField(BoolToInt(washedData.IsCalibratingPaddleBias)));
            valMixed.Fields.Add("IsCalibratingPaddleK", new InfluxValueField(BoolToInt(washedData.IsCalibratingPaddleK)));
            valMixed.Fields.Add("IsRampingUp", new InfluxValueField(BoolToInt(washedData.IsRampingUp)));
            valMixed.Fields.Add("NumberPumpStops", new InfluxValueField(washedData.NumberPumpStops));
            valMixed.Fields.Add("Selectflowout_boolean", new InfluxValueField(BoolToInt(washedData.SelectflowoutBoolean)));
            valMixed.Fields.Add("SelectpresSP_boolean", new InfluxValueField(BoolToInt(washedData.SelectpresSpBoolean)));
            valMixed.Fields.Add("StateCalibrationPaddle", new InfluxValueField((int)washedData.StateCalibrationPaddle));
            valMixed.Fields.Add("TimeCalibrating_s", new InfluxValueField(washedData.TimeCalibratingS));
            valMixed.Fields.Add("TimePumpsOn_s", new InfluxValueField(washedData.TimePumpsOnS));
            valMixed.Fields.Add("TimePumpsOff_s", new InfluxValueField(washedData.TimePumpsOffS));

            foreach (var dataVariable in washedData.Values)
            {
                var tmp = double.IsNaN(dataVariable.Value) ? MissingValueReplacement : dataVariable.Value ;
                valMixed.Fields.Add(dataVariable.Key, new InfluxValueField(tmp));
            }

            valMixed.MeasurementName = "WashedData";
            valMixed.Precision = TimePrecision.Seconds;
            return valMixed;
        }

        public InfluxDatapoint<InfluxValueField> CreateRealtimeOutputStatusPoint(string testRun, DateTime timeStamp, RealtimeOutputStatus realtimeOutputStatus)
        {
            var valMixed = new InfluxDatapoint<InfluxValueField>();
            valMixed.UtcTimestamp = timeStamp;
            valMixed.Tags.Add("TestDate", timeStamp.ToShortDateString());
            valMixed.Tags.Add("TestTime", timeStamp.ToShortTimeString());
            valMixed.Tags.Add("TestRun", testRun);

            valMixed.Fields.Add("ErrorCode", new InfluxValueField((int)realtimeOutputStatus.ErrorCode));
            valMixed.Fields.Add("WarningCode", new InfluxValueField((int)realtimeOutputStatus.WarningCode));
            valMixed.Fields.Add("IsCalibrating", new InfluxValueField(BoolToInt(realtimeOutputStatus.IsCalibrating)));
            valMixed.Fields.Add("SysState", new InfluxValueField((int)realtimeOutputStatus.SysState));
            valMixed.Fields.Add("VolGainLossExpectedM3", new InfluxValueField(realtimeOutputStatus.VolGainLossExpectedM3));
            valMixed.Fields.Add("VolGainLossPredErrorM3", new InfluxValueField(realtimeOutputStatus.VolGainLossPredErrorM3));
            valMixed.Fields.Add("VolGainLossPredModelledM3", new InfluxValueField(realtimeOutputStatus.VolGainLossPredModelledM3));

            valMixed.MeasurementName = "RealtimeOutputStatus";
            valMixed.Precision = TimePrecision.Seconds;
            return valMixed;
        }

        public InfluxDatapoint<InfluxValueField> CreateFlowInOutPoint(string testRun, DateTime timeStamp, FlowInOutOutput flowInOutOutput)
        {
            var valMixed = new InfluxDatapoint<InfluxValueField>();
            valMixed.UtcTimestamp = timeStamp;
            valMixed.Tags.Add("TestDate", timeStamp.ToShortDateString());
            valMixed.Tags.Add("TestTime", timeStamp.ToShortTimeString());
            valMixed.Tags.Add("TestRun", testRun);

            var isCalExF =BoolToInt(flowInOutOutput.IsCalibratedExpFlowOut);
            var isCalF = BoolToInt(flowInOutOutput.IsCalibratingFlowInFlowOut);
            valMixed.Fields.Add("FlowOutExpected_lpm", new InfluxValueField(flowInOutOutput.FlowOutExpectedLpm));
            valMixed.Fields.Add("IsCalibratedExpFlowOut", new InfluxValueField(isCalExF));
            valMixed.Fields.Add("IsCalibratingFlowInFlowOut", new InfluxValueField(isCalF));
            valMixed.Fields.Add("TimeConstFlowOut_s", new InfluxValueField(flowInOutOutput.TimeConstFlowOutS));
            valMixed.Fields.Add("TimeDelayFlowOut_s", new InfluxValueField(flowInOutOutput.TimeDelayFlowOutS));
            valMixed.Fields.Add("Vol0FlowOutPumpStart_m3", new InfluxValueField(flowInOutOutput.Vol0FlowOutPumpStartM3));


            valMixed.MeasurementName = "FlowInOut";
            valMixed.Precision = TimePrecision.Seconds;
            return valMixed;
        }

        public InfluxDatapoint<InfluxValueField> CreatePresSPExpPoint(string testRun, DateTime timeStamp, PresSPexpOutput presSPexpOutput)
        {
            var valMixed = new InfluxDatapoint<InfluxValueField>();
            valMixed.UtcTimestamp = timeStamp;
            valMixed.Tags.Add("TestDate", timeStamp.ToShortDateString());
            valMixed.Tags.Add("TestTime", timeStamp.ToShortTimeString());
            valMixed.Tags.Add("TestRun", testRun);

            valMixed.Fields.Add("IsCalibratedExpPresSP", new InfluxValueField(BoolToInt(presSPexpOutput.IsCalibratedExpPresSp)));
            valMixed.Fields.Add("IsCalibratingExpPresSP", new InfluxValueField(BoolToInt(presSPexpOutput.IsCalibratingExpPresSp)));
            valMixed.Fields.Add("IsSteadyFlowIn", new InfluxValueField(BoolToInt(presSPexpOutput.IsSteadyFlowIn)));
            valMixed.Fields.Add("IsSteadyPresSP", new InfluxValueField(BoolToInt(presSPexpOutput.IsSteadyPresSp)));
            valMixed.Fields.Add("IsSteadyandNonZeroPresSP", new InfluxValueField(BoolToInt(presSPexpOutput.IsSteadyandNonZeroPresSp)));
            valMixed.Fields.Add("PresSPDer_barpsec", new InfluxValueField(presSPexpOutput.PresSpDerBarpsec));
            valMixed.Fields.Add("PresSPErr_bar", new InfluxValueField(presSPexpOutput.PresSpErrBar));
            valMixed.Fields.Add("PresSPExpWithoutInjection_bar", new InfluxValueField(presSPexpOutput.PresSpExpWithoutInjectionBar));
            valMixed.Fields.Add("PresSPExpected_bar", new InfluxValueField(presSPexpOutput.PresSpExpectedBar));
            valMixed.Fields.Add("PressSPInjection_bar", new InfluxValueField(presSPexpOutput.PressSpInjectionBar));
            valMixed.Fields.Add("StateMachine", new InfluxValueField((int)presSPexpOutput.StateMachine));


            valMixed.MeasurementName = "PresSPExp";
            valMixed.Precision = TimePrecision.Seconds;
            return valMixed;
        }

        public InfluxDatapoint<InfluxValueField> CreateVolExpTripPoint(string testRun, DateTime timeStamp, TripTankOutput tripTankOutput)
        {
            var valMixed = new InfluxDatapoint<InfluxValueField>();
            valMixed.UtcTimestamp = timeStamp;
            valMixed.Tags.Add("TestDate", timeStamp.ToShortDateString());
            valMixed.Tags.Add("TestTime", timeStamp.ToShortTimeString());
            valMixed.Tags.Add("TestRun", testRun);

            valMixed.Fields.Add("AreaDPAverage_m2", new InfluxValueField(tripTankOutput.AreaDpAverageM2));
            valMixed.Fields.Add("AreaDPLastStand_m2", new InfluxValueField(tripTankOutput.AreaDpLastStandM2));
            valMixed.Fields.Add("DepthDelta_m", new InfluxValueField(tripTankOutput.DepthDeltaM));
            valMixed.Fields.Add("DepthLastStand_m", new InfluxValueField(tripTankOutput.DepthLastStandM));
            valMixed.Fields.Add("IsNewStandTripTank", new InfluxValueField(BoolToInt(tripTankOutput.IsNewStandTripTank)));
            valMixed.Fields.Add("IsOKLastStand", new InfluxValueField(BoolToInt(tripTankOutput.IsOkLastStand)));
            valMixed.Fields.Add("IsOnBottom", new InfluxValueField(BoolToInt(tripTankOutput.IsOnBottom)));
            valMixed.Fields.Add("IsPipeSteady", new InfluxValueField(BoolToInt(tripTankOutput.IsPipeSteady)));
            valMixed.Fields.Add("IsvolTTSteady", new InfluxValueField(BoolToInt(tripTankOutput.IsvolTtSteady)));
            valMixed.Fields.Add("LengthLastStand_m", new InfluxValueField(tripTankOutput.LengthLastStandM));
            valMixed.Fields.Add("LengthStandAverage_m", new InfluxValueField(tripTankOutput.LengthStandAverageM));
            valMixed.Fields.Add("NumOKStand", new InfluxValueField(tripTankOutput.NumOkStand));
            valMixed.Fields.Add("NumStand", new InfluxValueField(tripTankOutput.NumStand));
            valMixed.Fields.Add("StateTripping", new InfluxValueField(tripTankOutput.StateTripping));
            valMixed.Fields.Add("TimeLastStand_d", new InfluxValueField(tripTankOutput.TimeLastStandD));
            valMixed.Fields.Add("VolSteelLastStand_m3", new InfluxValueField(tripTankOutput.VolSteelLastStandM3));
            valMixed.Fields.Add("VolTTcum_m3", new InfluxValueField(tripTankOutput.VolTTcumM3));
            valMixed.Fields.Add("VolTTLastStand_m3", new InfluxValueField(tripTankOutput.VolTtLastStandM3));
            valMixed.Fields.Add("VolTTcumErr_m3", new InfluxValueField(tripTankOutput.VolTTcumErrM3));
            valMixed.Fields.Add("VolTTcumExp_m3", new InfluxValueField(tripTankOutput.VolTTcumExpM3));
            valMixed.Fields.Add("VolTTdiff_m3", new InfluxValueField(tripTankOutput.VolTTdiffM3));

            valMixed.MeasurementName = "VolExpTrip";
            valMixed.Precision = TimePrecision.Seconds;
            return valMixed;
        }

        public InfluxDatapoint<InfluxValueField> CreateFlowlineAndPitPoint(string testRun, DateTime timeStamp, FlowlineAndPitOutput flowlineAndPitOutput)
        {
            var valMixed = new InfluxDatapoint<InfluxValueField>();
            valMixed.UtcTimestamp = timeStamp;
            valMixed.Tags.Add("TestDate", timeStamp.ToShortDateString());
            valMixed.Tags.Add("TestTime", timeStamp.ToShortTimeString());
            valMixed.Tags.Add("TestRun", testRun);

            valMixed.Fields.Add("FlowPitDer_lpm", new InfluxValueField(flowlineAndPitOutput.FlowPitDerLpm));
            valMixed.Fields.Add("IsCalibratedExpPitVol", new InfluxValueField(BoolToInt(flowlineAndPitOutput.IsCalibratedExpPitVol)));
            valMixed.Fields.Add("VolFlExpected_m3", new InfluxValueField(flowlineAndPitOutput.VolFlExpectedM3));
            valMixed.Fields.Add("VolGLExpected_m3", new InfluxValueField(flowlineAndPitOutput.VolGlExpectedM3));

            valMixed.MeasurementName = "FlowlineAndPit";
            valMixed.Precision = TimePrecision.Seconds;
            return valMixed;
        }

        public InfluxDatapoint<InfluxValueField> CreateWashedProcessPoint(string testRun, DateTime timeStamp, WashedProcessOutput washedProcessOutput)
        {
            var valMixed = new InfluxDatapoint<InfluxValueField>();
            valMixed.UtcTimestamp = timeStamp;
            valMixed.Tags.Add("TestDate", timeStamp.ToShortDateString());
            valMixed.Tags.Add("TestTime", timeStamp.ToShortTimeString());
            valMixed.Tags.Add("TestRun", testRun);

            valMixed.Fields.Add("IsCalibratingSPdelay", new InfluxValueField(BoolToInt(washedProcessOutput.IsCalibratingSPdelay)));
            valMixed.Fields.Add("TimeDelayPresSP_s", new InfluxValueField(washedProcessOutput.TimeDelayPresSpS));

            valMixed.MeasurementName = "WashedProcess";
            valMixed.Precision = TimePrecision.Seconds;
            return valMixed;
        }

        public InfluxDatapoint<InfluxValueField> CreateGainDetectorPoint(string testRun, DateTime timeStamp, GainDetectorOutput gainDetectorOutput)
        {
            var valMixed = new InfluxDatapoint<InfluxValueField>();
            valMixed.UtcTimestamp = timeStamp;
            valMixed.Tags.Add("TestDate", timeStamp.ToShortDateString());
            valMixed.Tags.Add("TestTime", timeStamp.ToShortTimeString());
            valMixed.Tags.Add("TestRun", testRun);

            valMixed.Fields.Add("FlowDerGLAvg_lpm", new InfluxValueField(gainDetectorOutput.FlowDerGlAvgLpm));
            valMixed.Fields.Add("VolGainDetector_m3", new InfluxValueField(gainDetectorOutput.VolGainDetectorM3));

            valMixed.MeasurementName = "GainDetector";
            valMixed.Precision = TimePrecision.Seconds;
            return valMixed;
        }

        public InfluxDatapoint<InfluxValueField> CreateAlgResultPoint(string testRun, DateTime timeStamp, AlgResult algResult)
        {
            var valMixed = new InfluxDatapoint<InfluxValueField>();
            valMixed.UtcTimestamp = timeStamp;
            valMixed.Tags.Add("TestDate", timeStamp.ToShortDateString());
            valMixed.Tags.Add("TestTime", timeStamp.ToShortTimeString());
            valMixed.Tags.Add("TestRun", testRun);
            valMixed.Tags.Add("AlgName", "Insert Alg Name here");

            valMixed.Fields.Add("AlarmType", new InfluxValueField((int)algResult.AlarmType));
            valMixed.Fields.Add("AlarmState", new InfluxValueField((int)algResult.AlarmState));
            valMixed.Fields.Add("VolIO_m3", new InfluxValueField(algResult.VolIoM3));
            valMixed.Fields.Add("FlowIO_lpm", new InfluxValueField(algResult.FlowIOflowLpm));
            valMixed.Fields.Add("RecalibrateNow", new InfluxValueField(algResult.RecalibrateFlowOutNow));

            valMixed.MeasurementName = "AlgResult";
            valMixed.Precision = TimePrecision.Seconds;
            return valMixed;
        }
        public InfluxDatapoint<InfluxValueField> CreateAlgDataPoint(string testRun, DateTime timeStamp, AlgData algData)
        {
            var valMixed = new InfluxDatapoint<InfluxValueField>();
            valMixed.UtcTimestamp = timeStamp;
            valMixed.Tags.Add("TestDate", timeStamp.ToShortDateString());
            valMixed.Tags.Add("TestTime", timeStamp.ToShortTimeString());
            valMixed.Tags.Add("TestRun", testRun);

            valMixed.Fields.Add("FlowOutExpected_lpm", new InfluxValueField(algData.FlowOutExpectedLpm));
            valMixed.Fields.Add("PresSPExpected_bar", new InfluxValueField(algData.PresSpExpectedBar));
            valMixed.Fields.Add("NumberPumpStops", new InfluxValueField(algData.NumberPumpStops));
            valMixed.Fields.Add("TimePumpsOn_s", new InfluxValueField(algData.TimePumpsOnS));
            valMixed.Fields.Add("VolTripTankCumMeas_m3", new InfluxValueField(algData.VolTripTankCumMeasM3));
            valMixed.Fields.Add("VolTripTankCumExpected_m3", new InfluxValueField(algData.VolTripTankCumExpectedM3));
            valMixed.Fields.Add("VolTripTankCumError_m3", new InfluxValueField(algData.VolTripTankCumErrorM3));
            valMixed.Fields.Add("TimeSinceResetGL_s", new InfluxValueField(algData.TimeSinceResetGlS));
            valMixed.Fields.Add("VolGainLossExpected_m3", new InfluxValueField(algData.VolGainLossExpectedM3));
            valMixed.Fields.Add("VolGainDetector_m3", new InfluxValueField(algData.VolGainDetectorM3));
            valMixed.Fields.Add("IsCalibratedExpPresSP", new InfluxValueField(BoolToInt(algData.IsCalibratedExpPresSp)));
            valMixed.Fields.Add("IsCalibratedExpPitVol", new InfluxValueField(BoolToInt(algData.IsCalibratedExpPitVol)));
            valMixed.Fields.Add("IsCalibratedExpFlowOut", new InfluxValueField(BoolToInt(algData.IsCalibratedExpFlowOut)));
            valMixed.Fields.Add("IsCalibratedMeasFlowOut", new InfluxValueField(BoolToInt(algData.IsCalibratedMeasFlowOut)));
            valMixed.Fields.Add("IsCalibrating", new InfluxValueField(BoolToInt(algData.IsCalibrating)));
            valMixed.Fields.Add("IsDownlinking", new InfluxValueField(BoolToInt(algData.IsDownlinking)));
            valMixed.Fields.Add("IsRampingUp", new InfluxValueField(BoolToInt(algData.IsRampingUp)));
            valMixed.Fields.Add("ResetVolIOnow", new InfluxValueField(algData.ResetVolIOnow));

            valMixed.MeasurementName = "AlgData";
            valMixed.Precision = TimePrecision.Seconds;
            return valMixed;
        }


        public InfluxDatapoint<InfluxValueField> CreateEvaluateRealtimePoint(string testRun, DateTime timeStamp, EvaluateRealTimeOutput evaluateRealTimeOutput)
        {
            var valMixed = new InfluxDatapoint<InfluxValueField>();
            valMixed.UtcTimestamp = timeStamp;
            valMixed.Tags.Add("TestDate", timeStamp.ToShortDateString());
            valMixed.Tags.Add("TestTime", timeStamp.ToShortTimeString());
            valMixed.Tags.Add("TestRun", testRun);

            valMixed.Fields.Add("UpTime_h", new InfluxValueField(evaluateRealTimeOutput.UpTimeH));
            valMixed.Fields.Add("NumberOfAlarms", new InfluxValueField(evaluateRealTimeOutput.NumberOfAlarms));
            valMixed.Fields.Add("UpTime_percent", new InfluxValueField(evaluateRealTimeOutput.UpTimePercent));
            valMixed.Fields.Add("TotalTime_d", new InfluxValueField(evaluateRealTimeOutput.TotalTimeD));
            valMixed.Fields.Add("FlowAndPitInfluxAlarm", new InfluxValueField(evaluateRealTimeOutput.FlowAndPitInfluxAlarm));
            valMixed.Fields.Add("FlowAndPitLossAlarm", new InfluxValueField(evaluateRealTimeOutput.FlowAndPitLossAlarm));
            valMixed.Fields.Add("FlowAndPressureInfluxAlarm", new InfluxValueField(evaluateRealTimeOutput.FlowAndPressureInfluxAlarm));
            valMixed.Fields.Add("FlowAndPressureLossAlarm", new InfluxValueField(evaluateRealTimeOutput.FlowAndPressureLossAlarm));
            valMixed.Fields.Add("FlowOnlyInfluxAlarm", new InfluxValueField(evaluateRealTimeOutput.FlowOnlyInfluxAlarm));
            valMixed.Fields.Add("FlowOnlyLossAlarm", new InfluxValueField(evaluateRealTimeOutput.FlowOnlyLossAlarm));
            valMixed.Fields.Add("PitAndPressureInfluxAlarm", new InfluxValueField(evaluateRealTimeOutput.PitAndPressureInfluxAlarm));
            valMixed.Fields.Add("PitAndPressureLossAlarm", new InfluxValueField(evaluateRealTimeOutput.PitAndPressureLossAlarm));
            valMixed.Fields.Add("PitOnlyInfluxAlarm", new InfluxValueField(evaluateRealTimeOutput.PitOnlyInfluxAlarm));
            valMixed.Fields.Add("PitOnlyLossAlarm", new InfluxValueField(evaluateRealTimeOutput.PitOnlyLossAlarm));
            valMixed.Fields.Add("PitSlowGainAlarm", new InfluxValueField(evaluateRealTimeOutput.PitSlowGainAlarm));
            valMixed.Fields.Add("TripOnlyInfluxAlarm", new InfluxValueField(evaluateRealTimeOutput.TripOnlyInfluxAlarm));
            valMixed.Fields.Add("TripOnlyLossAlarm", new InfluxValueField(evaluateRealTimeOutput.TripOnlyLossAlarm));
            valMixed.Fields.Add("AlarmsLast12Hours", new InfluxValueField(evaluateRealTimeOutput.AlarmsLast12Hours));
            valMixed.Fields.Add("AlarmsPer12Hours", new InfluxValueField(evaluateRealTimeOutput.AlarmsPer12Hours));

            valMixed.MeasurementName = "EvaluateRealtime";
            valMixed.Precision = TimePrecision.Seconds;
            return valMixed;
        }
    }
}
