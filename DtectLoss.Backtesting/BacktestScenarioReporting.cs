﻿using System;
using System.Collections.Generic;
using DtectLoss.Common;
using DtectLoss.Common.Enums;
using DtectLossRealtime.Enums;
using DtectLossRealtime.Models;
using Microsoft.Extensions.Logging;

namespace DtectLoss.Backtesting
{
    public class BacktestAlgResult
    {
        public double TimeStamp { get; set; }
        public double VolIoM3 { get; set; }
        public double FlowIoLpm { get; set; }
        public AlarmState AlarmState { get; set; }
        public AlarmType AlarmType { get; set; }
        public bool IgnoreFlag { get; set; }
    }
    public interface IBacktestScenarioReporting
    {
        void Initialize(Dictionary<string, DataVariableInfo> trueData);
        BacktestEvaluateOutput EvaluateCurrentRun(ScenarioConfig scenarioConfig, string testRun, string simName);
        void SaveIteration(string testRun, double absTime, DtectLossRealtimeOutput realtimeOutput);
    }

    public class BacktestScenarioReporting : IBacktestScenarioReporting
    {
        private readonly ITimeseriesLogging timeseriesLogging;
        private readonly ILogger<BacktestScenarioReporting> _logger;
        private readonly BacktestEvaluate _backtestEvaluate;
        private readonly IFieldDataPlayer _trueFieldDataPlayer;
        private readonly List<BacktestAlgResult> _algResults;
        private Dictionary<string, DataVariableInfo>? _trueData;
        private double _lastDisplayPercent = 0;
        private const double DisplayEveryXPercent = 1;

        public BacktestScenarioReporting(ITimeseriesLogging timeseriesLogging, IFieldDataPlayer trueFieldDataPlayer, ILogger<BacktestScenarioReporting> logger, BacktestEvaluate backtestEvaluate)
        {
            this.timeseriesLogging = timeseriesLogging;
            _trueFieldDataPlayer = trueFieldDataPlayer;
            _algResults = new List<BacktestAlgResult>();
            _logger = logger;
            _backtestEvaluate = backtestEvaluate;
        }

        public void Initialize(Dictionary<string, DataVariableInfo> trueData)
        {
            _trueData = trueData;
            //TODO: Move these settings to appsettings
            timeseriesLogging.Initialize("ekd", "http://localhost:8086/", "admin","admin");
        }

        public void SaveIteration(string testRun, double absTime, DtectLossRealtimeOutput o)
        {
            var timeStamp = DateTime.FromOADate(absTime);
            var outputStatus = new RealtimeOutputStatus
            {
                ErrorCode = o.ErrorCode,
                WarningCode = o.WarningCode,
                IsCalibrating = o.IsCalibrating,
                SysState = o.SysState,
                VolGainLossExpectedM3 = o.VolGainLossExpectedM3,
                VolGainLossPredErrorM3 = o.VolGainLossPredErrorM3,
                VolGainLossPredModelledM3 = o.VolGainLossPredModelledM3
            };
            timeseriesLogging.LogAllPoints(testRun, timeStamp, o.RawDataNonstandardUnits,  o.RawData, o.WashedData, o.FlowInOutOutput, o.PresSPexpOutput, o.GainDetectorOutput
            , o.AlgResult, o.AlgData, o.EvaluateRealTimeOutput, o.FlowlineAndPitOutput, o.VolExpTripOutput, o.WashedProcessOutput, outputStatus);

            _algResults.Add(new BacktestAlgResult
            {
                TimeStamp = absTime,
                AlarmState = o.AlgResult.AlarmState,
                AlarmType = o.AlgResult.AlarmType,
                FlowIoLpm = o.AlgResult.FlowIOflowLpm,
                VolIoM3 = o.AlgResult.VolIoM3
            });
        }

        public BacktestEvaluateOutput EvaluateCurrentRun(ScenarioConfig scenarioConfig, string testRun, string simName)
        {
            _trueFieldDataPlayer.Initialize(_trueData!, new []{"flowIO", "volIO"});
            LogTrueData(testRun);

            _backtestEvaluate.Initialize(scenarioConfig.AlgorithmsToUse![0], testRun, simName);
            _backtestEvaluate.Evaluate(_trueData!, _algResults);
            return _backtestEvaluate.GenerateOutput();
        }

        private void LogTrueData(string testRun)
        {
            while (_trueFieldDataPlayer.SimProgress < 100)
            {
                _trueFieldDataPlayer.AsyncPlay();
                var data = _trueFieldDataPlayer.Output;
                var timeStamp = _trueFieldDataPlayer.AbsTime;
                DisplayTrueDataProgress();
                timeseriesLogging.LogTrueData(testRun, DateTime.FromOADate(timeStamp), data);
            }

        }
        private void DisplayTrueDataProgress()
        {
            if (_trueFieldDataPlayer.SimProgress >= _lastDisplayPercent + DisplayEveryXPercent) 
            {
                _logger.LogDebug($"True data : {_trueFieldDataPlayer.SimProgress:0.##} percent complete.");
                _lastDisplayPercent = _trueFieldDataPlayer.SimProgress;
            }
        }
    }
}
