﻿using System;
using System.Collections.Generic;
using System.Linq;
using DtectLoss.Common;
using Microsoft.Extensions.Logging;

namespace DtectLoss.Backtesting
{
    /// <summary>
    ///
    /// FIELD_DATA_PLAYER_OBJ Play field data for test of EKD algorithms
    ///   The field data player is a class which can play back field data
    ///   read from zipped of unzipped csv-files.The data is played back
    ///   with a time-step defined by dT[s]. The variables which should be
    ///   available in the output when playing is given by the cell
    ///   desiredVarNames.
    /// </summary>
    public class FieldDataPlayer : IFieldDataPlayer
    {
        public Dictionary<string, DataVariable> Output
        {
            get
            {
                var newOutput = _output.Select(x => x).ToDictionary(x => x.Key, y => new DataVariable(y.Value.T, y.Value.V));
                return newOutput;
            }
        }
        public Dictionary<string, string> RawDataUnits { get; private set; } = new Dictionary<string, string>();
        public double SimProgress { get; set; }
        public double AbsTime { get; private set; }
        public IEnumerable<string> VariableNames { get; private set; } = new List<string>();
        public Dictionary<string, double> CurrentTime { get; private set; } = new Dictionary<string, double>();
        public double StartTime { get; private set; }
        public double SimDuration { get; private set; }
        public Dictionary<string, int> CurrentIndex { get; private set; } = new Dictionary<string, int>();
        public Dictionary<string, int> NDataPoints { get; private set; } = new Dictionary<string, int>();

        private double _simTime;
        private const double Dt = 1;
        private double _endTime;
        private Dictionary<string, DataVariable> _output = new Dictionary<string, DataVariable>();
        private readonly ILogger<FieldDataPlayer> _logger;
        private Dictionary<string, DataVariableInfo> _sourceData = new Dictionary<string, DataVariableInfo>();

        public FieldDataPlayer(ILogger<FieldDataPlayer> logger)
        {
            _logger = logger;
        }


        /// <summary>
        /// initialize the output with the first non-NaN value from the
        /// data struct
        /// </summary>
        /// <param name="data"></param>
        /// <param name="desiredProcessTags"></param>
        public void Initialize(Dictionary<string, DataVariableInfo> data, string[] desiredProcessTags)
        {
            _endTime = 0;
            _simTime = 0;
            _sourceData = data;
            //check available var names vs desired
            var desiredVariableNames = desiredProcessTags;
            var sourceDataVariables = _sourceData.Keys.ToList();
            var (_, availableVariableNames) = ValidateMissingDesiredVariables(desiredVariableNames, sourceDataVariables);
            VariableNames = availableVariableNames.ToList();
            if(VariableNames.Any())
            {
                RawDataUnits = ValidateDataUnits();

                NDataPoints = VariableNames
                    .ToDictionary(k => k, x => _sourceData[x].Variables.Count);
                CurrentIndex = VariableNames
                    .ToDictionary(k => k, x => 0);
                _output = VariableNames
                    .ToDictionary(x => x, y => new DataVariable(_sourceData[y].Variables.FirstOrDefault().T, _sourceData[y].Variables.FirstOrDefault().V));
                CurrentTime = _output
                    .ToDictionary(k => k.Key, x => x.Value.T);
                _endTime = VariableNames
                    .Select(x => _sourceData[x].Variables.LastOrDefault().T).Max();
                StartTime = _output.Values.FirstOrDefault().T;
                AbsTime = StartTime;
                SimProgress = 0;
                SimDuration = Math.Max(Dt, Math.Floor(_endTime.DiffInSeconds(StartTime)));
            }
            else
            {
                StartTime = 0;
                AbsTime = StartTime;
                SimProgress = 100;
                SimDuration = Math.Max(Dt, Math.Floor(_endTime.DiffInSeconds(StartTime)));
            }


        }
        /// <summary>
        /// 
        /// play back data asynchronously, and not with a fixed time-step
        /// (asyncDT is updated by this function and reflect the duration
        /// of the previous time - step).
        ///
        /// determine if it is possible to make a step ahead in time for
        /// each of the variables.
        /// </summary>
        public void AsyncPlay()
        {
            var possibleStep = VariableNames.ToDictionary(k => k, _ => 1);
            var possibleNextTime = VariableNames
                .ToDictionary(k => k, x => 
                    CurrentIndex[x] < NDataPoints[x] - 1
                    ? _sourceData[x].Variables[CurrentIndex[x] + 1].T 
                    : CurrentTime[x]);

            // call function which updates the absolute time and provide
            // updated indicies for each variable.
            var (minT, newIndex) = GetPossibleAsyncStep(possibleNextTime, possibleStep, CurrentIndex, CurrentTime);
            AbsTime = minT;
            // update variables which are beeing stepped ahead in time
            _output = VariableNames
                .Select(x => GetNextOutput(x, newIndex))
                .ToDictionary(k => k.Item1, x => x.Item2);
            CurrentTime = _output
                .ToDictionary(k => k.Key, x => x.Value.T);
            CurrentIndex = newIndex
                .ToDictionary(k => k.Key, x => x.Value);
            _simTime = Math.Round(AbsTime.DiffInSeconds(StartTime));
            SimProgress = _simTime / SimDuration * 100;
        }

        /// <summary>
        /// Determine next output value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="newIndex"></param>
        /// <returns></returns>
        private (string, DataVariable) GetNextOutput(string key, Dictionary<string, int> newIndex)
        {
            if (newIndex[key] > CurrentIndex[key])
            {
                if (_sourceData[key].Variables[CurrentIndex[key]].T <= AbsTime)
                {
                    var v = _sourceData[key].Variables[newIndex[key]];
                    return (key, new DataVariable(v.T, v.V));
                }

            }
            else if (_sourceData[key].Variables[CurrentIndex[key]].T > AbsTime)
            {
                return (key, new DataVariable(AbsTime, CommonConstants.MissingValueReplacement)); // Use values to be used when missing values
            }
            var vTmp2 = _output[key];
            return (key, new DataVariable(vTmp2.T, vTmp2.V)); //use existing output
        }

        /// <summary>
        /// find the smallest of the available timestamp among the
        /// variables and return this time(minT), and update indicies
        /// for variables with this timestamp(newIdx).
        /// </summary>
        /// <param name="possibleNextTime"></param>
        /// <param name="possibleStep"></param>
        /// <param name="currentIndex"></param>
        /// <param name="currentTime"></param>
        /// <returns></returns>
        public static (double, Dictionary<string, int>) GetPossibleAsyncStep(Dictionary<string, double> possibleNextTime, Dictionary<string, int> possibleStep, Dictionary<string, int> currentIndex, Dictionary<string, double> currentTime)
        {
            var indexesNotAtEnd = possibleNextTime.ToDictionary(k => k.Key,x => x.Value > currentTime[x.Key]);
            if (indexesNotAtEnd.Values.Any(x => x))// If any indexes can be updated
            {
                var minT = possibleNextTime // Find min time
                    .Where(x => indexesNotAtEnd[x.Key])
                    .Select(x => x.Value)
                    .Min();
                var pbt = possibleNextTime
                    .Select(x => (x.Key, Math.Abs(x.Value.DiffInSeconds(minT)) < 0.1))
                    .ToDictionary(k => k.Key, x => x.Item2);
                var newIndex = 
                    currentIndex // iterate indicies which are not at end and equals minT
                    .Select(x =>
                        indexesNotAtEnd[x.Key] && pbt[x.Key]
                            ? (x.Key, currentIndex[x.Key] + possibleStep[x.Key])
                            : (x.Key, currentIndex[x.Key]))
                    .ToDictionary(k => k.Key, x => x.Item2);
                return (minT, newIndex);
            }
            else
            {
                var minT = possibleNextTime.Values.Max();
                var newIndex = currentIndex
                    .ToDictionary(k => k.Key, x => x.Value);
                return (minT, newIndex);
            }
        }

        //Create dictionary of name/units and report any variables where units are missing
        private Dictionary<string, string> ValidateDataUnits()
        {
            var units = _sourceData.Values
                .Select(x => (x.Name, x.Unit.Trim()))
                .ToDictionary(x => x.Name, y => y.Item2);
            foreach (var variableWithMissingUnit in units.Values.Where(string.IsNullOrEmpty))
            {
                _logger.LogWarning($"FILD_DATA_PLAYER_OBJ:missingUnit. The variable {variableWithMissingUnit} has no unit.");
            }
            return units;
        }

        /// <summary>
        /// Find any missing variables and set status/warn. Return status and available variables
        /// </summary>
        /// <param name="desiredVariableNames"></param>
        /// <param name="sourceVariableNames"></param>
        /// <returns></returns>
        private (int , IEnumerable<string>) ValidateMissingDesiredVariables(string[] desiredVariableNames, List<string> sourceVariableNames)
        {
            var availableVariableNames = desiredVariableNames.Intersect(sourceVariableNames);
            var missingDesiredVariableNames = desiredVariableNames.Except(sourceVariableNames);
            var currentDatasetComplete = 0;
            foreach (var missingDesiredVariableName in missingDesiredVariableNames)
            {
                _logger.LogWarning($"FILD_DATA_PLAYER_OBJ:desiredVariableNotAvailable. The variable {missingDesiredVariableName} was not found among the read data.");
                currentDatasetComplete = 2;
            }

            return (currentDatasetComplete, availableVariableNames);
        }


    }
}