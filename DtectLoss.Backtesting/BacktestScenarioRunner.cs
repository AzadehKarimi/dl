﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DtectLoss.Backtesting.Models;
using DtectLoss.Common;
using DtectLoss.Common.Enums;
using DtectLoss.FileHandling;
using DtectLossRealtime.Enums;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.DependencyInjection;

namespace DtectLoss.Backtesting
{
    public interface IBacktestScenarioManager
    {
        BacktestScenarioRunnerResult PerformBacktestingScenarios();
        void Initialize(string testRunDateString);
    }

    public class BacktestScenarioRunner : IBacktestScenarioManager
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IExcelReader _excelReader;
        private readonly BacktestingOptions _options;
        private readonly ILogger<BacktestScenarioRunner> _logger;
        private string _testRunDateString = string.Empty;

        public BacktestScenarioRunner(IServiceProvider serviceProvider, ILogger<BacktestScenarioRunner> logger, IExcelReader excelReader, IOptions<BacktestingOptions> options)
        {
            _serviceProvider = serviceProvider;
            _excelReader = excelReader;
            _options = options.Value;
            _logger = logger;
        }
        public BacktestScenarioRunnerResult PerformBacktestingScenarios()
        {
            var result = new BacktestScenarioRunnerResult();
            using var logContext = Serilog.Context.LogContext.PushProperty("LoggingTestrun", _testRunDateString);
            try
            {
                // This will get the current PROJECT directory "..\DtectLoss"
                var projectDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent?.Parent?.Parent?.FullName;
                var path = Path.Combine(projectDirectory ?? throw new InvalidOperationException("Could not read parent directory"), _options.RootDir);
                var outputScenarioPath = Path.Combine(projectDirectory ?? throw new InvalidOperationException("Could not read parent directory"), _options.OutputScenarioDir);
                var dataFiles = FileUtilities.GetAllZipFilesInDirectoryRecursively(path);
                var excelScenarioFiles = FileUtilities.GetAllXlsxFilesInDirectory(path).ToList();
                if (excelScenarioFiles.Count() != 1)
                {
                    throw new InvalidOperationException(
                        "Either missing or too many Excel files in directory. Please check if you have any of these files opened in Excel.");
                }

                var filePath = Path.Combine(path,
                    excelScenarioFiles.FirstOrDefault() ?? throw new InvalidOperationException());
                var scenarioConfigs = _excelReader.ReadXlsConfig(filePath);

                // Iterate all scenarios
                var processedResults = scenarioConfigs.Where(x => x.UseForBacktesting)
                    //.AsParallel()
                    .Select(scenarioConfig =>
                        PerformBacktestingScenario(scenarioConfig, dataFiles, path, _options.NameDesiredProcessTags!, _testRunDateString))
                    .ToList();

                var evaluated = processedResults
                    .Select(x => x.EvaluateAndFinalize())
                    .OrderBy(x => x.SimName)
                    .ToList();

                result.TotalFar = evaluated.Sum(x => x.AlgUptimeInHours) > 0 ? evaluated.Sum(x => x.AlgNoOfFalseAlams) /
                               evaluated.Sum(x => x.AlgUptimeInHours) : 0;

                result.MeanKpi = evaluated.Count > 0 ? evaluated.Sum(x => x.KeyPerformanceIndex) : double.NaN;
                
                result.TotalPd = evaluated.Sum(x => x.TrueNoOfAlams) > 0 ? 1.0 * evaluated.Sum(x => x.AlgNoOfCorrectAlams) /
                        evaluated.Sum(x => x.TrueNoOfAlams) : 0;


                result.TotalSimulationTime = evaluated.Sum(x => x.DurationInHours);
                result.TotalUptimeHours = evaluated.Sum(x => x.AlgUptimeInHours);
                result.TotalUptimePercent = result.TotalUptimeHours / result.TotalSimulationTime * 100;
                result.TotalAccumulatedVolIo = evaluated.Sum(x => x.AccumulatedVolIo);

                var outputScenarioFilePath = Path.Combine(outputScenarioPath, $"Evaluation_{_testRunDateString}.csv");
                var csv = new StringBuilder();
                LogInformationLine("");
                LogInformationLine("");
                var stats = $"Statistics after running {evaluated.Count} data sets:";

                LogInformationLine(stats);
                csv.AppendLine(stats);
                var simTimeStats =$"Total simulation time: {result.TotalSimulationTime:F2} hours, alg up-time: {result.TotalUptimeHours:F2} hours ({result.TotalUptimePercent}%)";
                csv.AppendLine(simTimeStats);
                LogInformationLine(simTimeStats);
                var kpiStats = $"KPI: {result.MeanKpi:F2}, FAR: {result.TotalFar:F2}, PD: {result.TotalPd:F2}";
                LogInformationLine(kpiStats);
                csv.AppendLine(kpiStats);
                var resultHeading = "Results for all simulations:";
                csv.AppendLine(resultHeading);
                LogInformationLine(resultHeading);
                var csvResultsHeading =
                    $"Time series;FAR;KPI;#correctAlarms;#falseAlarms;#trueAlarms;algUpTime [h];Sim duration [h];#lateAlarms;TD [s];VD [m3]";
                csv.AppendLine(csvResultsHeading);

                var allOutputs = new List<BacktestEvaluateOutput>();
                foreach (var output in evaluated)
                {
                    allOutputs.Add(output);
                    var testRun = output.RigName;
                    using (Serilog.Context.LogContext.PushProperty("LoggingTestrun", testRun))
                    {
                        LogInformationLine("");
                        LogInformationLine(
                            $"Time series: {output.RigName}, FAR: {output.FalseAlamsRate:F2}, KPI: {output.KeyPerformanceIndex:F2}");
                        LogInformationLine(
                            $"CorrectAlgAlarms: {output.AlgNoOfCorrectAlams}, FalseAlarms: {output.AlgNoOfFalseAlams}, TrueAlarms: {output.TrueNoOfAlams}");
                        LogInformationLine(
                            $"CorrectWarnings: {output.TrueNoOfWarnings}, AlgWarnings: {output.AlgNoOfWarnings}");
                        LogInformationLine($"Sim duration: {output.DurationInHours:F2}, LateAlarms: {output.AlgLateAlams}");
                        LogInformationLine(
                            $"TD[s] {output.TimeToDetection:F2}, VD[m3] {output.AccumulatedVolumeAtDetection:F2}");
                        LogInformationLine("");


                        var csvLineResults =
                            $"{output.SimName};{output.FalseAlamsRate:F2};{output.KeyPerformanceIndex:F2};" +
                            $"{output.AlgNoOfCorrectAlams};{output.AlgNoOfFalseAlams};{output.TrueNoOfAlams};{output.AlgUptimeInHours:F2};" +
                            $"{output.DurationInHours:F2};{output.AlgLateAlams};{output.TimeToDetection:F2};{output.AccumulatedVolumeAtDetection:F2}";
                        csv.AppendLine(csvLineResults);

                        var countOfAlarmsFiltered = output.AlarmTypeCount.Where(x =>

                            x.alarmType != AlarmType.Normal && x.alarmType != AlarmType.LossWarning &&
                            x.alarmType != AlarmType.InfluxWarning).Sum(x => x.Item2);
                        LogInformationLine($"Sum of filtered alarms for this simulation: {countOfAlarmsFiltered}");
                        LogInformationLine("Type of false alarms and warnings which have been issued");
                        foreach (var (alarmTypeName, alarmTypeCount) in output.AlarmTypeCount)
                        {
                            LogInformationLine($"AlarmTypeName: {alarmTypeName}, Count: {alarmTypeCount}");
                        }
                    }
                }

                csv.AppendLine();
                csv.AppendLine("Type of false alarms and warnings which have been issued:");
                var alarmTypesOrdered = ((AlarmType[]) Enum.GetValues(typeof(AlarmType)))
                    .OrderBy(x => x).ToList();
                var alarmTypesNames = alarmTypesOrdered.Select(x => Enum.GetName(typeof(AlarmType), x));
                var alarmTypesCsvHeadings = string.Join(";", alarmTypesNames);
                csv.AppendLine($"Time series;{alarmTypesCsvHeadings}");
                foreach (var output in evaluated)
                {
                    var alarmTypeCountDict = output.AlarmTypeCount.ToDictionary(x => x.alarmType, x => x.Item2);
                    //var orderedAlamtypeValues = alarmTypesOrdered.Select(x => alarmTypeCountDict[x]);
                    var orderedAlamtypeValues = alarmTypesOrdered.Select(x => alarmTypeCountDict.TryGetValue(x, out var value) ? value : 0);
                    var alarmCountCsv = string.Join(";", orderedAlamtypeValues);
                    var fullAlarmLine = $"{output.SimName};{alarmCountCsv}";
                    csv.AppendLine(fullAlarmLine);
                }

                result.BacktestEvaluateOutput = allOutputs;
                LogInformationLine("");
                LogInformationLine("");
                var totalCountOfAlarmsFiltered = evaluated.SelectMany(a => a.AlarmTypeCount)
                    .Where(x =>
                        x.alarmType != AlarmType.Normal && x.alarmType != AlarmType.LossWarning &&
                        x.alarmType != AlarmType.InfluxWarning).Sum(x => x.Item2);
                LogInformationLine(
                    $"Total - type of false alarms and warnings which have been issued: {totalCountOfAlarmsFiltered}");
                var totalGroupedAlarms = evaluated
                    .SelectMany(x => x.AlarmTypeCount)
                    .GroupBy(x => x.alarmType)
                    .Select(x => (x.Key, x.Sum(c => c.Item2)))
                    .ToList();

                var totalAlarmTypeCountDict = totalGroupedAlarms.ToDictionary(x => x.Key, x => x.Item2);
                var totalOrderedAlamtypeValues = alarmTypesOrdered.Select(x => totalAlarmTypeCountDict[x]);
                var totalAlarmCountCsv = string.Join(";", totalOrderedAlamtypeValues);
                var totalFullAlarmLine = $"Sum;{totalAlarmCountCsv}";
                csv.AppendLine(totalFullAlarmLine);

                result.TotalAlarmTypeCount = totalGroupedAlarms;
                foreach (var (alarmTypeName, alarmTypeCount) in totalGroupedAlarms)
                {
                    LogInformationLine($"AlarmTypeName: {alarmTypeName}, Count: {alarmTypeCount}");
                }
                LogInformationLine("");
                LogInformationLine("");
                if (_options.UseOutputScenarioResults) File.WriteAllText(outputScenarioFilePath, csv.ToString());
                // Return stats and eval result
                _logger.LogInformation("Done");
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            return result;
        }

        public void Initialize(string testRunDateString)
        {
            _testRunDateString = testRunDateString;
        }

        private IBacktestManager PerformBacktestingScenario(ScenarioConfig scenario, IEnumerable<string> dataFiles, string rootPath, IEnumerable<string> nameDesiredProcessTags, string testRunDateString)
        {
            var scenarioDataFilePath = ExactlyOneFileMatchingScenarioOrThrowException(dataFiles, scenario.Idx);
            var bm = _serviceProvider.GetService<IBacktestManager>();
            bm.Initialize(testRunDateString);
            bm.RunScenarioToCompletion(scenario, scenarioDataFilePath);
            return bm;
        }

        private static string ExactlyOneFileMatchingScenarioOrThrowException(IEnumerable<string> dataFiles, int scenarioIdx)
        {
            var matchingFiles =
                dataFiles.Select(x =>
                    {
                        var resultString = Regex.Match(x, @"(\d+)(?!.*\d)").Value;
                        var num = int.Parse(resultString);
                        return new {Num = num, Val = x};
                    })
                    .Where(x => x.Num == scenarioIdx)
                    .Select(x => x.Val)
                    .ToList();
            if (matchingFiles.Count < 1)
                throw new InvalidOperationException($"Could not find a data file for scenario - {scenarioIdx}.");
            if (matchingFiles.Count > 1)
                throw new InvalidOperationException($"There must be exactly one data file for scenario - {scenarioIdx}.");
            return matchingFiles.FirstOrDefault();
        }

        private void LogInformationLine(string line)
        {
            _logger.LogInformation(line);
        }
    }
}
