﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DtectLoss.Backtesting.Models;
using DtectLoss.Common;
using DtectLoss.Common.Enums;
using DtectLossRealtime;
using DtectLossRealtime.Enums;
using DtectLossRealtime.Models;
using Microsoft.Extensions.Logging;
using OfficeOpenXml.Drawing.Chart;

namespace DtectLoss.Backtesting
{
    /// <summary>
    ///   EVALUATE_OBJ Summary of this class goes here
    ///   Read detection data from algorithm
    ///   Calculate number of alarms.
    ///   Compare with true state if available: False alarm rate (FAR),
    ///   Probability of detection (PD), Time-to-detect (TD),
    ///   Volume-at-detection (VD), algorithm up-time (algUptime).
    /// </summary>
    public class BacktestEvaluate
    {
        private readonly ILogger<BacktestEvaluate> _logger;
        private int _trueNoOfAlarms;
        private int _trueNoOfWarnings;
        private int _algNoOfCorrectAlarms;
        private int _algNoOfWarnings;
        private int _algNoOfFalseAlarms;
        private double _accumulatedVolIo = 0;
        private int _algLateAlarms; // alarms issued either too late, or after correctly issuing an alarm
        private string _algName = string.Empty;
        private string _rigName = string.Empty;
        //private int _far; // [false alarms per 12 h] false alarm rate
        private double _far; // [false alarms per 12 h] false alarm rate
        private double? _pd; // [-] probability of detection
        private double _kpi; // [-] key performance index for algorithm
        private double _td; // [s] time to detection, an average if multiple events
        private double _vd; // [m3] mean absolute volume of actual influx at time of detection.
        private double _durationInHours; // [hours] duration of simulation
        private double _disabledTime; // [days] time the algorithm is disabled
        private double _algUpTime; // [%]  how much of the time the algorithm is not disabled.
        private double _algUpTimeInHours; // [hours]  how much of the time the algorithm is not disabled.
        private string _resultTxt = string.Empty; // one-line of text that summarized the evaulation
        private List<(AlarmType alarmType, int)> _falseAlarmsGroupedByAlarmType = new List<(AlarmType alarmType, int)>();
        private string _simName = string.Empty;


        private const double VolThresWarningM3 = 0.8;
        private const double VolThresAlarmM3 = 1;
        private const double DetectionGracePeriodS = 630; // [s] grace period for computing time of detection

        public BacktestEvaluate(ILogger<BacktestEvaluate> logger)
        {
            _logger = logger;
        }

        public void Initialize(string algName, string rigName, string simName)
        {
            _algName = algName;
            _rigName = rigName;
            _simName = simName;
        }

        /// <summary>
        /// 
        /// The inputs are structs with time and value, e.g.trueIgnore.t
        /// is the time and trueIgnore.V is the value.
        /// trueIgnore[boolean]: flag saying if alarms issued during
        /// this time-frame should be ignored, i.e.when trueIgnore.V == 1
        /// trueVolIO[m3]  : actual accumulated volume to/from
        /// reservoir, can simply be zero.
        /// algFlowIO[lpm] : flow rate to/from reservoir computed by
        /// algorithm
        /// algVolIO[m3]   : accumulated volume to/from reservoir
        /// computed by algorithm
        /// alarmState[-]  : Alarm produced by algorithm
            
        /// This function plays through all data and checks for alarms
        /// and false alarms.
            
        /// return with empty alarm count variables if called without
        /// algAlarmType
        /// </summary>
        public void Evaluate(Dictionary<string, DataVariableInfo> trueData, List<BacktestAlgResult> backtestAlgResults)
        {
            var trueAlarmInfo = GenerateTrueAlarmInfo(trueData);
            _falseAlarmsGroupedByAlarmType = GroupAlarms(backtestAlgResults, trueAlarmInfo);
            var updatedTrueAlarmInfo = CountIssuedAlarms(trueAlarmInfo, backtestAlgResults);
            CalculateTimeToDetection(updatedTrueAlarmInfo, trueData);
            CalculateKeyPerformanceIndices(backtestAlgResults.FirstOrDefault().TimeStamp, backtestAlgResults.LastOrDefault().TimeStamp);
            CalculateExtraKPIs(backtestAlgResults);
            MakeResultText();
        }

        private void CalculateExtraKPIs(List<BacktestAlgResult> backtestAlgResults)
        {
            _accumulatedVolIo = backtestAlgResults.Sum(x => x.VolIoM3);
        }

        private List<(AlarmType alarmType, int)> GroupAlarms(List<BacktestAlgResult> algResults, List<BacktestTrueAlarmInfo> trueAlarmInfo)
        {
            var alarmTypes = Enum.GetValues(typeof(AlarmType)).Cast<AlarmType>();

            // Function will have to return a minimum of list of all alarmtypes with zero count. Not "null". 
            var zeroList = alarmTypes
                .Select((alarmType, _) => (alarmType, 0))
                .ToList();
            
            if (algResults == null || trueAlarmInfo == null)
            {
                _logger.LogError("EVALUATE: Missing evaluate data.");
                //return new List<(AlarmType alarmType, int)>();
                return zeroList;
            }


            var firstTrue = trueAlarmInfo.FirstOrDefault();
            if (!(firstTrue != null && firstTrue.StartEvent != 0 && firstTrue.StopEvent != 0 ))
            {
                //TODO:Logger instead
                _logger.LogError("EVALUATE: Empty true file.");
                //return new List<(AlarmType alarmType, int)>();
                return zeroList;
            }


            var nonFalseAlarms = 
                algResults
                .Select(algResult => (algResult, trueAlarmInfo.Any(trueAlarm => algResult.TimeStamp < trueAlarm.StopEvent && algResult.TimeStamp > trueAlarm.StartEvent)))
                .Select(tmp => {
                    var (algResult, ignoreThis) = tmp;
                    algResult.IgnoreFlag = ignoreThis;
                    return algResult;
                })
                .ToArray();

            var nonFalseAlarmsExcludingPreviousEquals =
                nonFalseAlarms
                    .Select((o, index) => (o, index))
                    .Where(x => 
                        (x.index <= 0 && !x.o.IgnoreFlag) || 
                        !x.o.AlarmType.Equals(nonFalseAlarms[x.index - 1].AlarmType) && !x.o.IgnoreFlag)
                    .Select(x => x.o);

            var alamsGroupedWithAllTypes = alarmTypes
                .GroupJoin(nonFalseAlarmsExcludingPreviousEquals,
                    t => t,
                    s => s.AlarmType,
                    (alarmType, alarms) => (alarmType, alarms.Count()))
                .OrderByDescending(x => x.Item2)
                .ToList();
            return alamsGroupedWithAllTypes;
        }

        private void MakeResultText()
        {
            if (_trueNoOfAlarms != 0)
            {
                _resultTxt = $"FAR: {_far:F2} alarms/(12 h), PD: {_pd:F1} prc, TD: {_td:F1} s, 'VD: {_vd:F3} m3, Up-time: {_algUpTime:F2} prc, KPI: {_kpi:F2} -, # late alarms: {_algLateAlarms:D}";
            }
            else
            {
                _resultTxt = $"FAR: {_far:F2} alarms/(12 h) Up-time: {_algUpTime:F2} prc, KPI: {_kpi:F2}";
            }
            _logger.LogInformation(_resultTxt);
        }

        private void CalculateKeyPerformanceIndices(double timeStart, double timeEnd)
        {
            //calculate probability of detection
            if (_trueNoOfAlarms != 0)
            {
                _pd = Math.Min(100.0, 100.0 * _algNoOfCorrectAlarms / _trueNoOfAlarms);
            }

            // calculate a KPI which is a sum of false alarms per hour and
            // probability of missing a true event. The lower the KPI, the
            // better.
            double probMissingTrueAlarm = 0;
            if (_pd.HasValue)
            {
                probMissingTrueAlarm = (100.0 - _pd.Value) / 100.0;
            }

            // False alarm rate in alarms per 12 hours
            _far =  (double) (_algNoOfFalseAlarms / (timeEnd - timeStart) / 2.0);

            // kpi = | false alarms per hour | + | probability of missing a true alarm |
            var _kpiTmp = new double[] {_far / 12.0, probMissingTrueAlarm};
            //_kpi = Accord.Math.Norm.Euclidean(_kpiTmp);
            _kpi = Accord.Math.Matrix.Sum(_kpiTmp);

            _durationInHours = (timeEnd - timeStart) * 24;
            _algUpTime = 100 - _disabledTime / (timeEnd - timeStart) * 100;
            _algUpTimeInHours = (timeEnd - timeStart - _disabledTime) * 24;
        }

        private void CalculateTimeToDetection(List<BacktestTrueAlarmInfo> trueAlarmInfo, Dictionary<string, DataVariableInfo> trueData)
        {
            var trueVolIo = new List<DataVariable>();
            if (trueData.ContainsKey("volIO"))
            {
                trueVolIo = trueData["volIO"].Variables;
            }
            else
            {
                return;
            }

            if (!trueAlarmInfo.Any())
                return;

            var tdVec = new List<double>();
            var vdVec = new List<double>();

            foreach (var backtestTrueAlarmInfo in trueAlarmInfo)
            {
                if (backtestTrueAlarmInfo.DetectionTime.HasValue)
                {
                    var td = backtestTrueAlarmInfo.DetectionTime.Value.DiffInSeconds(backtestTrueAlarmInfo.StartEvent);
                    tdVec.Add(td);
                    // only count alarm if it is given within the grace period
                    if (td <= DetectionGracePeriodS)
                    {
                        _algNoOfCorrectAlarms += 1;
                        var idxDet = trueVolIo.FirstOrDefault(x => x.T >= backtestTrueAlarmInfo.DetectionTime);
                        if (idxDet != null)
                        {
                            vdVec.Add(idxDet.V);
                        }
                    }
                }
            }

            var tdV = tdVec.Where(x => x <= DetectionGracePeriodS);
            if (tdV.Any())
                _td = tdV.Average();
            else
                _td = 0;

            if(vdVec.Any())
                _vd = vdVec.Average();
            else
                _vd = 0;
        }

        private List<BacktestTrueAlarmInfo> CountIssuedAlarms(List<BacktestTrueAlarmInfo> trueAlarmInfo, List<BacktestAlgResult> backtestAlgResults)
        {
            var prevAlgAlarmState = backtestAlgResults.First().AlarmState;
            var disableAlgStart = backtestAlgResults.First().TimeStamp;
            var trueDataIsZero = !trueAlarmInfo.Any();
            var lastAlarmState = AlarmState.Normal;
            var lastTime = disableAlgStart;

            foreach (var backtestAlgResult in backtestAlgResults)
            {

                // compare true data with field data and count alarms
                if (backtestAlgResult.AlarmState == AlarmState.Normal)
                {
                    // no alarm from algorithm
                    if (prevAlgAlarmState == AlarmState.Disabled)
                    {
                        // count the time the algorithm has been disabled.
                        // note that the alarm state must pass through
                        // Normal for the time to be recorded.
                        _disabledTime = _disabledTime + backtestAlgResult.TimeStamp - disableAlgStart;
                    }
                    prevAlgAlarmState = AlarmState.Normal;
                }
                else if (backtestAlgResult.AlarmState == AlarmState.Alarm && trueDataIsZero && prevAlgAlarmState != AlarmState.Alarm)
                {
                    // False alarm from algorithm
                    _algNoOfFalseAlarms += 1;
                    prevAlgAlarmState = AlarmState.Alarm;
                }
                else if (backtestAlgResult.AlarmState == AlarmState.Alarm && !trueDataIsZero &&
                         (prevAlgAlarmState == AlarmState.Normal || prevAlgAlarmState == AlarmState.Warning))
                {
                    // possible false alarm from algorithm.Need to check if
                    // it is issued within the grace period of an actual
                    // alarm
                    
                    var falseAlarm = true;
                    (_algLateAlarms, falseAlarm, trueAlarmInfo) = DetectAlarmsAndUpdateAlarmInfo(backtestAlgResult.TimeStamp, _algLateAlarms, falseAlarm, trueAlarmInfo);

                    if (falseAlarm)
                        _algNoOfFalseAlarms += 1;
                    prevAlgAlarmState = AlarmState.Alarm;
                }
                else if (backtestAlgResult.AlarmState == AlarmState.Warning && prevAlgAlarmState != AlarmState.Warning)
                {
                    // Correct or incorrect warning from algorithm
                    _algNoOfWarnings += 1;
                    prevAlgAlarmState = AlarmState.Warning;
                }
                else if (backtestAlgResult.AlarmState == AlarmState.Disabled && prevAlgAlarmState != AlarmState.Disabled)
                {
                    disableAlgStart = backtestAlgResult.TimeStamp;
                    prevAlgAlarmState = AlarmState.Disabled;
                }
                lastAlarmState = backtestAlgResult.AlarmState;
                lastTime = backtestAlgResult.TimeStamp;
            }

            if (lastAlarmState == AlarmState.Disabled)
            {
                _disabledTime = _disabledTime + lastTime - disableAlgStart;
            }

            return trueAlarmInfo;
        }

        public static (int algLateAlarms, bool falseAlarm, List<BacktestTrueAlarmInfo> trueAlarmInfo) DetectAlarmsAndUpdateAlarmInfo(double algResultTimeStamp, int inputLateAlarms, bool inputFalseAlarms, List<BacktestTrueAlarmInfo> inputTrueAlarmInfo)
        {
            var (_, resLateAlarms, resFalseAlarms, resAlarmInfos) = inputTrueAlarmInfo
                .Aggregate((algResultTimeStamp, inputLateAlarms, inputFalseAlarms, new List<BacktestTrueAlarmInfo>()), AggregateDetectAndCalcAlarms);

            return (resLateAlarms, resFalseAlarms, resAlarmInfos);
        }

        private static (double timeStamp, int lateAlarms, bool falseAlarm, List<BacktestTrueAlarmInfo> alarmInfos) AggregateDetectAndCalcAlarms((double inputTimeStamp, int lateAlarms, bool inputFalseAlarm, List<BacktestTrueAlarmInfo> accAlarmInfos) state, BacktestTrueAlarmInfo current)
        {
            var outLateAlarms = state.lateAlarms;
            var outFalseAlarm = state.inputFalseAlarm;
            if (state.inputTimeStamp >= current.StartEvent && state.inputTimeStamp < current.StopEvent)
            {
                if (!current.Ignore)
                {
                    if (!current.DetectionTime.HasValue)
                    {
                        current.DetectionTime = state.inputTimeStamp;
                    }
                    else
                    {
                        outLateAlarms += 1;
                    }
                }
                outFalseAlarm = false;
            }

            state.accAlarmInfos.Add(current);
            return (state.inputTimeStamp, outLateAlarms, outFalseAlarm, state.accAlarmInfos );
        }

        private List<BacktestTrueAlarmInfo> GenerateTrueAlarmInfo(Dictionary<string, DataVariableInfo> trueData)
        {
            var trueIgnore = new List<DataVariable>();
            var trueVolIo = new List<DataVariable>();
            var trueAlarms = new List<BacktestTrueAlarmInfo>();
            var alarm = new BacktestTrueAlarmInfo();
            if (!trueData.ContainsKey("volIO"))
                // Log that no alarm data exists?
                return trueAlarms;

            // Fill in actual data into lists
            if (trueData.ContainsKey("ignore")) trueIgnore = trueData["ignore"].Variables;

            if (trueData.ContainsKey("volIO")) trueVolIo = trueData["volIO"].Variables;

            // Fill in values if ignore/volIo is empty
            if (!trueIgnore.Any() && trueVolIo.Any())
            {
                trueIgnore.Add(new DataVariable(trueVolIo.First().T, 0));
                trueIgnore.Add(new DataVariable(trueVolIo.Last().T, 0));
            }
            else if (trueIgnore.Any() && !trueVolIo.Any())
            {
                trueVolIo.Add(new DataVariable(trueIgnore.First().T, 0));
                trueVolIo.Add(new DataVariable(trueIgnore.Last().T, 0));
            }
            else if (!trueIgnore.Any() && !trueVolIo.Any())
            {
                //Log missing data?
                return trueAlarms;
            }
            // Init values before looping
            var prevTrueAlarmState = AlarmState.Normal;
            var trueNt = new BacktestTrueAlarms<int>(trueIgnore.Count - 1, trueVolIo.Count - 1);
            var trueIdx = new BacktestTrueAlarms<int>(0, 0);
            var trueCurrentT = new BacktestTrueAlarms<double>(trueIgnore.First().T, trueVolIo.First().T);
            var trueNextPossibleT = new BacktestTrueAlarms<double>(trueIgnore.Skip(1).First().T, trueVolIo.Skip(1).First().T);
            var trueV = new BacktestTrueAlarms<double>(trueIgnore.First().V, trueVolIo.First().V);
            var trueIdxNotAtEnd = GetIdxNotAtEndValues(trueIdx, trueNt);
            var ignoreEvent = false;
            var activeEvent = false;

            while (trueIdxNotAtEnd.Ignore || trueIdxNotAtEnd.True)
            {
                if (!ignoreEvent)
                {
                    
                    if ((!trueV.Ignore.Equals(0) || !trueV.True.Equals(0)) && !activeEvent)
                    {
                        alarm = new BacktestTrueAlarmInfo();
                        trueAlarms.Add(alarm);
                        if (!trueV.Ignore.Equals(0))
                        {
                            alarm.Ignore = true;
                            ignoreEvent = true;
                            alarm.StartEvent = trueCurrentT.Ignore;
                        }
                        else
                        {
                            alarm.Ignore = false;
                            activeEvent = true;
                            alarm.StartEvent = trueCurrentT.True;
                        }
                    }
                    else if (Math.Abs(trueV.True) >= VolThresAlarmM3 &&
                             (prevTrueAlarmState.Equals(AlarmState.Normal) ||
                              prevTrueAlarmState.Equals(AlarmState.Warning)))
                    {
                        // Alarm
                        prevTrueAlarmState = AlarmState.Alarm;
                        _trueNoOfAlarms += 1;
                        alarm.AlarmTime = trueCurrentT.True;
                    }
                    else if (trueV.True.Equals(0) && activeEvent)
                    {
                        alarm.StopEvent = trueCurrentT.True;
                        activeEvent = false;
                    }
                    else if (Math.Abs(trueV.True) < VolThresWarningM3)
                    {
                        // No Alarm
                        prevTrueAlarmState = AlarmState.Normal;
                    }
                    else if (Math.Abs(trueV.True) >= VolThresWarningM3 && prevTrueAlarmState == AlarmState.Normal)
                    {
                        // Warning
                        prevTrueAlarmState = AlarmState.Warning;
                        _trueNoOfWarnings += 1;
                    }
                }
                else
                {
                    if (trueV.Ignore.Equals(0))
                    {
                        alarm.StopEvent = trueCurrentT.Ignore;
                        ignoreEvent = false;
                    }
                }

                // determine which of the rows which are at the end.
                var trueIdxNotAtEndValues = GetIdxNotAtEndValues(trueIdx, trueNt);
                // Step forward
                var (trueNewT, trueNewIdx) = Step(trueNextPossibleT, trueCurrentT, trueIdx, trueIdxNotAtEndValues);
                // make sure that the indicies not already at the end is
                // less than the available indicies.
                trueIdx = UpdateTrueIdx(trueIdx, trueNewIdx, trueNt, trueIdxNotAtEndValues);
                // only update current time with those not at the end 
                trueCurrentT = UpdateTrueCurrentT(trueCurrentT, trueNewT, trueIdxNotAtEndValues);

                // Update value
                trueV = new BacktestTrueAlarms<double>(trueIgnore[trueIdx.Ignore].V, trueVolIo[trueIdx.True].V);

                // make sure that the next possible time is updated only if
                // necessary.

                if (trueIdx.Ignore < trueNt.Ignore)
                {
                    trueNextPossibleT.Ignore = trueIgnore[trueIdx.Ignore + 1].T;
                }

                if (trueIdx.True < trueNt.True)
                {
                    trueNextPossibleT.True = trueVolIo[trueIdx.True + 1].T;
                }
                trueIdxNotAtEnd = GetIdxNotAtEndValues(trueIdx, trueNt);
            }
            return trueAlarms;
        }

        private static BacktestTrueAlarms<double> UpdateTrueCurrentT(BacktestTrueAlarms<double> trueCurrentT, BacktestTrueAlarms<double> trueNewT, BacktestTrueAlarms<bool> trueIdxNotAtEndValues)
        {
            var i = trueIdxNotAtEndValues.Ignore ? trueNewT.Ignore : trueCurrentT.Ignore;
            var t = trueIdxNotAtEndValues.True ? trueNewT.True : trueCurrentT.True;
            return new BacktestTrueAlarms<double>(i, t);
        }

        private static BacktestTrueAlarms<int> UpdateTrueIdx(BacktestTrueAlarms<int> trueIdx, BacktestTrueAlarms<int> trueNewIdx, BacktestTrueAlarms<int> trueNt, BacktestTrueAlarms<bool> trueIdxNotAtEndValues)
        {
            var (ignoreMin, trueMin) = GetMinIdx(trueNewIdx, trueNt, trueIdxNotAtEndValues);
            var i = trueIdx.Ignore;
            var t = trueIdx.True;

            if (trueIdxNotAtEndValues.Ignore)
            {
                i = ignoreMin;
            }
            if (trueIdxNotAtEndValues.True)
            {
                t = trueMin;
            }
            return new BacktestTrueAlarms<int>(i, t);
        }


        private static (BacktestTrueAlarms<double> newT, BacktestTrueAlarms<int> newIdx) Step(BacktestTrueAlarms<double> possibleNextTime, BacktestTrueAlarms<double> currentTime, BacktestTrueAlarms<int> idx, BacktestTrueAlarms<bool> idxNotAtEnd)
        {
            var newT = new BacktestTrueAlarms<double>(currentTime.Ignore, currentTime.True);
            var newIdx = new BacktestTrueAlarms<int>(idx.Ignore, idx.True);

            var minT = GetMinT(possibleNextTime, idxNotAtEnd);
            if (idxNotAtEnd.Ignore && possibleNextTime.Ignore <= minT)
            {
                newT.Ignore = possibleNextTime.Ignore;
                newIdx.Ignore += 1;
            }
            if (idxNotAtEnd.True && possibleNextTime.True <= minT)
            {
                newT.True = possibleNextTime.True;
                newIdx.True += 1;
            }
            return (newT, newIdx);
        }

        private static double GetMinT(BacktestTrueAlarms<double> possibleNextTime, BacktestTrueAlarms<bool> idxNotAtEnd)
        {
            var pbt = new List<double>();
            if (idxNotAtEnd.Ignore) pbt.Add(possibleNextTime.Ignore);
            if (idxNotAtEnd.True) pbt.Add(possibleNextTime.True);
            return pbt.Min();
        }

        private static (int ignoreMin, int trueMin) GetMinIdx(BacktestTrueAlarms<int> newIdx, BacktestTrueAlarms<int> trueNT, BacktestTrueAlarms<bool> idxNotAtEnd)
        {
            var i = new List<int>() {trueNT.Ignore};
            var t = new List<int>() {trueNT.True};
            if (idxNotAtEnd.Ignore) i.Add(newIdx.Ignore);
            if (idxNotAtEnd.True) t.Add(newIdx.True);
            return (i.Min(), t.Min());
        }

        private static BacktestTrueAlarms<bool> GetIdxNotAtEndValues(BacktestTrueAlarms<int> trueIdx, BacktestTrueAlarms<int> trueNt)
        {
            return new BacktestTrueAlarms<bool>(trueIdx.Ignore < trueNt.Ignore, trueIdx.True < trueNt.True);
        }

        public BacktestEvaluateOutput GenerateOutput()
        {
            var backtestEvaluateOutput = new BacktestEvaluateOutput
            {
                TrueNoOfAlams = _trueNoOfAlarms,
                TrueNoOfWarnings = _trueNoOfWarnings,
                AlgNoOfCorrectAlams = _algNoOfCorrectAlarms,
                AlgNoOfWarnings = _algNoOfWarnings,
                AlgNoOfFalseAlams = _algNoOfFalseAlarms,
                AlgLateAlams = _algLateAlarms,
                AlgName = _algName,
                RigName = _rigName,
                SimName = _simName,
                ResultText = _resultTxt,
                FalseAlamsRate = _far,
                ProbabilityOfDetection = _pd ?? 0,
                KeyPerformanceIndex = _kpi,
                TimeToDetection = _td,
                AccumulatedVolumeAtDetection = _vd,
                DurationInHours = _durationInHours,
                AlgUpTime = _algUpTime,
                AlgUptimeInHours = _algUpTimeInHours,
                AccumulatedVolIo = _accumulatedVolIo,
                AlarmTypeCount = _falseAlarmsGroupedByAlarmType
            };
            return backtestEvaluateOutput;
        }
    }
}
