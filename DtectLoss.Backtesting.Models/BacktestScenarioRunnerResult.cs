﻿using System.Collections.Generic;
using DtectLoss.Common;
using DtectLoss.Common.Enums;

namespace DtectLoss.Backtesting.Models
{
    public class BacktestScenarioRunnerResult
    {
        public IEnumerable<BacktestEvaluateOutput> BacktestEvaluateOutput { get; set; }
        public double TotalNumberOfDatasets { get; set; }
        public double TotalFar { get; set; }
        public double TotalPd { get; set; }
        public double MeanKpi { get; set; }
        public double TotalSimulationTime { get; set; }
        public double TotalUptimeHours { get; set; }
        public double TotalUptimePercent { get; set; }
        public double TotalAccumulatedVolIo { get; set; }
        public string RunIdentifier  { get; set; }
        public List<(AlarmType alarmType, int)> TotalAlarmTypeCount { get; set; }

        public BacktestScenarioRunnerResult()
        {
            BacktestEvaluateOutput = new List<BacktestEvaluateOutput>();
            RunIdentifier = "Initialized";
            TotalAlarmTypeCount = new List<(AlarmType alarmType, int)>();
        }
    }
    
}
