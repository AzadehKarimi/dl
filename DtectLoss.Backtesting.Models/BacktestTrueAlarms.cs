﻿namespace DtectLoss.Backtesting.Models
{
    public class BacktestTrueAlarms<T>
    {
        public T Ignore { get; set; }
        public T True { get; set; }

        public BacktestTrueAlarms(T i, T t)
        {
            Ignore = i;
            True = t;
        }
    }
}