﻿namespace DtectLoss.Backtesting.Models
{
    public class BacktestTrueAlarmInfo
    {
        public double AlarmTime { get; set; }
        public double StartEvent { get; set; }
        public double StopEvent { get; set; }
        public bool Ignore { get; set; }
        public double? DetectionTime { get; set; }
        public double VolumeAtDetection { get; set; }
    }
}