﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DtectLossRealtime;
using DtectLoss.Common;
using DtectLoss.Common.Enums;
using DtectLossRealtime.Enums;
using DtectLossRealtime.Models;
using DtectLossRealtime.EKDAlgoritm;
using Microsoft.Extensions.Logging;
using Moq;

namespace DtectLoss.Realtime.Tests
{
    [TestClass]
    public class VotingTest
    {
        private VotingAlg _votingAlg;
        private AlgData _algData;
        private WashedData _washedData;
        private StateMachine _sysState;

        private AlarmConditions _alarmConditions;
        private Mock<ILogger<StateMachine>> _stateMachineLoggerMock;


        [TestInitialize()]
        public void Initialize()
        {
            _stateMachineLoggerMock = LoggerUtils.LoggerMock<StateMachine>();
            _votingAlg = new VotingAlg();
            _algData = new AlgData();
            _washedData = new WashedData();
            _sysState = new StateMachine(_stateMachineLoggerMock.Object);
            _votingAlg.Initialize(ParameterValues.VotingParam, new double[0]);

        }

        [TestCleanup()]
        public void Cleanup()
        {

        }

        [TestMethod]
        public void verify_SetInjection()
        {
            _votingAlg.VolIOflowM3 = 0;
            _votingAlg.VolIOpresM3 = 0;
            _votingAlg.SetInjection();
            Assert.IsTrue(Math.Abs(_votingAlg.VolIOflowM3) <= 0.001);
        }

        [TestMethod]
        public void verify_CalcIOFlow()
        {
            _algData.FlowOutExpectedLpm = 2000.0;
            _algData.IsDownlinking = false;
            _algData.IsCalibratedExpFlowOut = true;
            _algData.IsCalibratedMeasFlowOut = true;
            _algData.ResetVolIOnow = false;
            _washedData.Values[V.FlowInLpm] = 2000;
            _washedData.Values[V.FlowOutLpm] = 2101;
            _washedData.Values[V.FlowOutFilteredLpm] = 2150;
            _washedData.Values[V.DepthHoleM] = 5000;
            _washedData.Values[V.DepthBitM] = 5000 - 1;
            _sysState.IsOnBottom = true;

            const double dtS = 2.0;
            _votingAlg.UpdateTime(0, dtS);
            _votingAlg.CalcIoFlow(_algData, _washedData, _sysState);

            Assert.AreEqual((_washedData.Values[V.FlowOutFilteredLpm] - _algData.FlowOutExpectedLpm), _votingAlg.FlowIOflowLpm);
            Assert.AreEqual(dtS * (_washedData.Values[V.FlowOutFilteredLpm] - _algData.FlowOutExpectedLpm) / 60000.0, _votingAlg.VolIOflowM3);

            // Set FlowOutFiltered_lpm invalid and run test again
            _washedData.Values[V.FlowOutFilteredLpm] = CommonConstants.MissingValueReplacement;
            var v0 = _votingAlg.VolIOflowM3;
            _votingAlg.UpdateTime(0, dtS);
            _votingAlg.CalcIoFlow(_algData, _washedData, _sysState);
            Assert.AreEqual((_washedData.Values[V.FlowOutLpm] - _algData.FlowOutExpectedLpm), _votingAlg.FlowIOflowLpm);
            Assert.AreEqual(v0 + dtS * (_washedData.Values[V.FlowOutLpm] - _algData.FlowOutExpectedLpm) / 60000.0, _votingAlg.VolIOflowM3);
        }

        [TestMethod]
        public void verify_Calc_IOPres()
        {
            _algData.PresSpExpectedBar = 200;
            _algData.IsDownlinking = false;
            _algData.IsCalibratedExpPresSp = true;
            _algData.ResetVolIOnow = false;
            _washedData.Values[V.FlowInLpm] = 3000;
            _washedData.Values[V.PresSpBar] = 203;
            _washedData.Values[V.DepthHoleM] = 5000;
            _washedData.Values[V.DepthBitM] = 5000 - 1;
            _sysState.IsOnBottom = true;

            const double dtS = 2.0;
            _votingAlg.UpdateTime(0, dtS);
            _votingAlg.CalcIoPres(_algData, _washedData, _sysState);

            Assert.AreEqual(_washedData.Values[V.PresSpBar] - _algData.PresSpExpectedBar, _votingAlg.PresIoBar);
            Assert.AreEqual(_votingAlg.PresIoBar * _votingAlg.Parameters.ScalePresToFlow, _votingAlg.FlowIOpresLpm);
            Assert.AreEqual(dtS * _votingAlg.FlowIOpresLpm / 60000.0, _votingAlg.VolIOpresM3);
        }

        [TestMethod]
        public void verify_Calc_IOPit()
        {
            _algData.VolGainLossExpectedM3 = 21;
            _algData.IsDownlinking = false;
            _algData.IsCalibratedExpPitVol = true;
            _algData.ResetVolIOnow = false;
            _washedData.Values[V.FlowInLpm] = 2000;
            _washedData.Values[V.VolGainLossM3] = 20;
            _washedData.Values[V.DepthHoleM] = 5000;
            _washedData.Values[V.DepthBitM] = 5000 - 1;
            _washedData.Values[V.FlowInLpm] = 3000;
            _sysState.IsOnBottom = true;
            const double dtS = 2.0;

            _votingAlg.UpdateTime(0, dtS);
            _votingAlg.CalcIoPit(_algData, _washedData, _sysState);
            Assert.AreEqual(_washedData.Values[V.VolGainLossM3] - _algData.VolGainLossExpectedM3, _votingAlg.VolIOpitM3);
            Assert.AreEqual(0.0, _votingAlg.FlowIOpitLpm);

            var flowIoLpm = -130.0;
            double timeNowD = 0.0;

            var vincM3 = Enumerable.Range(1, 100).Select(x => (double)x * flowIoLpm / 60000.0 * dtS).ToArray();
            for (var i = 0; i < 100; i++)
            {
                _algData.VolGainLossExpectedM3 = 21 + vincM3[i];
                _washedData.Values[V.VolGainLossM3] = 20 + 2 * vincM3[i];
                timeNowD += dtS.SecondsToOADate();
                _votingAlg.UpdateTime(timeNowD, dtS);
                _votingAlg.CalcIoPit(_algData, _washedData, _sysState);
            }
            Assert.AreEqual(_washedData.Values[V.VolGainLossM3] - _algData.VolGainLossExpectedM3, _votingAlg.VolIOpitM3);
            Assert.IsTrue(Math.Abs(_votingAlg.FlowIOpitLpm / flowIoLpm) - 1 < 0.01);
        }

        [TestMethod]
        public void verify_SetTime_HighFlowError()
        {
            _algData.IsCalibratedExpFlowOut = true;
            _algData.IsCalibratedMeasFlowOut = true;
            _algData.FlowOutExpectedLpm = 2000;
            _algData.IsDownlinking = false;
            _algData.IsCalibratedExpFlowOut = true;
            _algData.IsCalibratedMeasFlowOut = true;
            _algData.ResetVolIOnow = false;
            _washedData.Values[V.FlowInLpm] = 2000;
            _washedData.Values[V.FlowOutLpm] = 2101;
            _washedData.Values[V.FlowOutFilteredLpm] = 2150;
            _washedData.Values[V.DepthHoleM] = 5000;
            _washedData.Values[V.DepthBitM] = 5000 - 1;
            _sysState.IsOnBottom = true;
            const double dtS = 2.0;

            _votingAlg.UpdateTime(0, dtS);
            _votingAlg.CalcIoFlow(_algData, _washedData, _sysState);
            _votingAlg.SetTimeHighFlowError(_algData, _washedData);
            Assert.AreEqual(0, _votingAlg.TimeHighFlowErrorS);

            _washedData.Values[V.FlowInLpm] = 300;
            _algData.FlowOutExpectedLpm = _washedData.Values[V.FlowInLpm];
            _votingAlg.CalcIoFlow(_algData, _washedData, _sysState);
            _votingAlg.SetTimeHighFlowError(_algData, _washedData);
            Assert.AreEqual(dtS, _votingAlg.TimeHighFlowErrorS);
        }

        [TestMethod]
        public void verifySetTimeHighVolError()
        {
            _algData.VolGainLossExpectedM3 = 20;
            _algData.IsDownlinking = false;
            _algData.IsCalibratedExpPitVol = true;
            _algData.ResetVolIOnow = false;
            _algData.IsRampingUp = false;
            _washedData.Values[V.FlowInLpm] = 2000;
            _washedData.Values[V.VolGainLossM3] = 20.3;
            _washedData.Values[V.DepthHoleM] = 5000;
            _washedData.Values[V.DepthBitM] = 5000 - 1;
            _sysState.IsOnBottom = true;

            const int dtS = 2;
            _votingAlg.UpdateTime(0, dtS);
            _votingAlg.CalcIoPit(_algData, _washedData, _sysState);
            _votingAlg.SetTimeHighVolError(_algData, _washedData);
            Assert.AreEqual(0, _votingAlg.TimeHighVolErrorS);

            _washedData.Values[V.VolGainLossM3] = 23;
            _votingAlg.CalcIoPit(_algData, _washedData, _sysState);
            _votingAlg.SetTimeHighVolError(_algData, _washedData);
            Assert.AreEqual(dtS, _votingAlg.TimeHighVolErrorS);
        }

        [TestMethod]
        public void verify_CheckFor_NewAlarm()
        {
            _alarmConditions.HighGainVol = true;
            _alarmConditions.HighGainFlow = false;
            _alarmConditions.HighGainPres = false;
            _alarmConditions.HighLossVol = false;
            _alarmConditions.HighLossFlow = false;
            _alarmConditions.HighLossPres = false;
            _alarmConditions.Inhibit = false;
            _alarmConditions.AllowInfluxAlarm = false;
            _alarmConditions.Startup = false;
            _alarmConditions.SlowGain = false;
            _alarmConditions.HighGainVolOnly = false;
            _alarmConditions.HighLossVolOnly = false;
            _alarmConditions.HighGainFlowOnly = false;
            _alarmConditions.HighLossFlowOnly = false;
            _votingAlg.CheckForNewAlarm(_alarmConditions);
            Assert.AreEqual(AlarmState.Disabled, _votingAlg.AlarmState);
            Assert.AreEqual(AlarmType.Normal, _votingAlg.AlarmType);

            _alarmConditions.HighGainVol = true;
            _alarmConditions.HighGainFlow = true;
            _alarmConditions.HighGainPres = true;
            _alarmConditions.HighLossVol = false;
            _alarmConditions.HighLossFlow = false;
            _alarmConditions.HighLossPres = false;
            _alarmConditions.Inhibit = true;
            _alarmConditions.AllowInfluxAlarm = true;
            _alarmConditions.Startup = true;
            _alarmConditions.SlowGain = true;
            _alarmConditions.HighGainVolOnly = true;
            _alarmConditions.HighLossVolOnly = true;
            _alarmConditions.HighGainFlowOnly = true;
            _alarmConditions.HighLossFlowOnly = true;
            _votingAlg.CheckForNewAlarm(_alarmConditions);
            Assert.AreEqual(AlarmState.Alarm, _votingAlg.AlarmState);
            Assert.AreEqual(AlarmType.FlowAndPressureInfluxAlarm, _votingAlg.AlarmType);

            _alarmConditions.HighGainPres = false;
            _votingAlg.CheckForNewAlarm(_alarmConditions);
            Assert.AreEqual(AlarmState.Alarm, _votingAlg.AlarmState);
            Assert.AreEqual(AlarmType.FlowAndPitInfluxAlarm, _votingAlg.AlarmType);

            _alarmConditions.HighGainPres = true;
            _alarmConditions.HighGainFlow = false;
            _votingAlg.CheckForNewAlarm(_alarmConditions);
            Assert.AreEqual(AlarmState.Alarm, _votingAlg.AlarmState);
            Assert.AreEqual(AlarmType.PitAndPressureInfluxAlarm, _votingAlg.AlarmType);

            _alarmConditions.HighGainVol = false;
            _alarmConditions.HighGainPres = false;
            _alarmConditions.HighGainFlow = false;
            _alarmConditions.HighLossVol = true;
            _alarmConditions.HighLossFlow = true;
            _alarmConditions.HighLossPres = true;
            _votingAlg.CheckForNewAlarm(_alarmConditions);
            Assert.AreEqual(AlarmState.Alarm, _votingAlg.AlarmState);
            Assert.AreEqual(AlarmType.FlowAndPressureLossAlarm, _votingAlg.AlarmType);

            _alarmConditions.HighLossPres = false;
            _votingAlg.CheckForNewAlarm(_alarmConditions);
            Assert.AreEqual(AlarmState.Alarm, _votingAlg.AlarmState);
            Assert.AreEqual(AlarmType.FlowAndPitLossAlarm, _votingAlg.AlarmType);
        }

        [TestMethod]
        public void verify_SetVol_IOAlarm()
        {
            _votingAlg.VolIOpitM3   = -2;
            _votingAlg.VolIOflowM3  =  1;
            _votingAlg.VolIOpresM3  =  3;
            _votingAlg.AlarmType = AlarmType.PitSlowGainAlarm;
            _votingAlg.SetVolIoAlarm();
            Assert.AreEqual(_votingAlg.VolIOpitM3, _votingAlg.VolIoM3);

            _votingAlg.AlarmType = AlarmType.PitOnlyLossAlarm;
            _votingAlg.SetVolIoAlarm();
            Assert.AreEqual(_votingAlg.VolIOpitM3, _votingAlg.VolIoM3);

            _votingAlg.AlarmType = AlarmType.PitOnlyInfluxAlarm;
            _votingAlg.SetVolIoAlarm();
            Assert.AreEqual(_votingAlg.VolIOpitM3, _votingAlg.VolIoM3);
        }
    }
}
