﻿using System;
using System.Globalization;
using Accord.Math;
using DtectLoss.Common;
using DtectLossRealtime;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static DtectLoss.Common.CommonConstants;

namespace DtectLoss.Realtime.Tests
{
    [TestClass]
    public class FlowlineAndPitTest
    {
        private const double V0Fl=4.5;
        private const double FlowOutSs=2340;
        private const double VLow=1.5;
        private const double QLow=10;
        private const double VDrill= VLow + V0Fl;
        private readonly double[] _x0= {VDrill, 0, 0};
        private const double K2F=-0.5;
        private const double Kf=FlowOutSs/60e3*(1-K2F)/VDrill;
        private const double KOiFl=1e-2;
        private const double KOiGl=1e-2;
        private const double TimeStep=1;
        private const double TimeDelay=15;
        private FlowlineAndPit _fap;

        [TestInitialize()]
        public void Initialize()
        {
            _fap = new FlowlineAndPit();
        }

        [TestCleanup()]
        public void Cleanup()
        {
        }

        [TestMethod]
        public void constructor_correctInit_correctOutput()
        {
            Assert.AreEqual(0, _fap.VolDrainback);
            Assert.AreEqual(0, _fap.KOutputInjectionFl);
            Assert.AreEqual(0, _fap.KIntOutputInjectionGl);
            Assert.AreEqual(0, _fap.Dt);
            Assert.IsNull(_fap.PrevTimeStamp);
            Assert.AreEqual(0, _fap.Td);
            Assert.AreEqual(0, _fap.KFlowline);
            Assert.AreEqual(0, _fap.K2Flowline);
        }

        [TestMethod]
        public void initialize_correctInitNoTimeDelay_correctOutput()
        {
            _fap.Initialize(_x0, V0Fl, KOiFl, KOiGl, TimeStep);
            Assert.AreEqual(_x0, _fap.X);
            Assert.AreEqual(V0Fl, _fap.VolDrainback);
            Assert.AreEqual(KOiFl, _fap.KOutputInjectionFl);
            Assert.AreEqual(KOiGl, _fap.KOutputInjectionGl);
            Assert.AreEqual(TimeStep, _fap.Dt);
            Assert.IsNull(_fap.PrevTimeStamp);
            Assert.AreEqual(0, _fap.Td);
        }

        [TestMethod]
        public void initialize_noTimeStamp_correctOutput()
        {
            _fap.Initialize(_x0, V0Fl, KOiFl, KOiGl);
            Assert.AreEqual(_x0, _fap.X);
            Assert.AreEqual(V0Fl, _fap.VolDrainback);
            Assert.AreEqual(KOiFl, _fap.KOutputInjectionFl);
            Assert.AreEqual(KOiGl, _fap.KOutputInjectionGl);
            Assert.AreEqual(0, _fap.Dt);
            Assert.IsNull(_fap.PrevTimeStamp);
        }

        [TestMethod]
        public void dyn_noFlowChange_noVolChange()
        {
            const double numTol = 1e-15;
            _fap.Initialize(_x0, V0Fl, KOiFl, KOiGl, TimeStep);
            _fap.UpdateKFlowline(Kf);
            _fap.UpdateK2Flowline(K2F);
            const double qOutLpm = FlowOutSs;
            const double qPumpLpm = qOutLpm;
            const int volGainLossM3 = 0;
            const bool useOutputInjection = false;
            var xDotExp = new double[] {0, 0, 0 };

            var xDotActual = _fap.Dyn(qOutLpm, qPumpLpm, volGainLossM3, useOutputInjection);
            Assert.AreEqual(xDotExp[0], xDotActual[0], numTol);
            Assert.AreEqual(xDotExp[1], xDotActual[1], numTol);
            Assert.AreEqual(xDotExp[2], xDotActual[2], numTol);

        }

        [TestMethod]
        public void updateTimeStep_prevTimeStampNotSet_prevTimeSet()
        {
            _fap.Initialize(_x0, V0Fl, KOiFl, KOiGl);
            _fap.UpdateKFlowline(Kf);
            _fap.UpdateK2Flowline(K2F);
            var qOutExpTime = DateTime.Parse("26/09/1981  01:00:02", new CultureInfo("nb-NO", false)).ToOADate();
            var qPumpTime = DateTime.Parse("26/09/1981  01:00:03", new CultureInfo("nb-NO", false)).ToOADate();
            var volGlTime = DateTime.Parse("26/09/1981  01:00:01", new CultureInfo("nb-NO", false)).ToOADate();
            var qOutExp = new DataVariable(qOutExpTime, 2500);
            var qPump = new DataVariable(qPumpTime, 2510);
            var volGl = new DataVariable(volGlTime, -1);

            _fap.UpdateTimeStep(qOutExp, qPump, volGl);
            var expected = qPumpTime;
            var actual = _fap.PrevTimeStamp.Value;
            Assert.AreEqual(expected, actual, "Timestamp should be set.");
            const int expected2 = 0;
            var actual2 = _fap.Dt;
            Assert.AreEqual(expected2, actual2, "Time step should be zero.");
        }

        [TestMethod]
        public void updateTimeStep_negativeDt_noChangeInDt()
        {
            _fap.Initialize(_x0, V0Fl, KOiFl, KOiGl);
            _fap.UpdateKFlowline(Kf);
            _fap.UpdateK2Flowline(K2F);
            var qOutExpTime = DateTime.Parse("26/09/1981  01:00:02", new CultureInfo("nb-NO", false)).ToOADate();
            var qPumpTime = DateTime.Parse("26/09/1981  01:00:03", new CultureInfo("nb-NO", false)).ToOADate();
            var volGlTime = DateTime.Parse("26/09/1981  01:00:01", new CultureInfo("nb-NO", false)).ToOADate();
            var qOutExp = new DataVariable(qOutExpTime, 2500);
            var qPump = new DataVariable(qPumpTime, 2510);
            var volGl = new DataVariable(volGlTime, -1);

            _fap.UpdateTimeStep(qOutExp, qPump, volGl);
            var expected = qPumpTime;
            var actual = _fap.PrevTimeStamp ?? 0;
            Assert.AreEqual(expected, actual, "Timestamp should be set.");

            const int expected2 = 0;
            var actual2 = _fap.Dt;
            Assert.AreEqual(expected2, actual2, "Time step should be zero.");
            var qOutExpTimeNew = DateTime.Parse("26/09/1981  01:00:00", new CultureInfo("nb-NO", false)).ToOADate();
            var qPumpTimeNew = DateTime.Parse("26/09/1981  01:00:00", new CultureInfo("nb-NO", false)).ToOADate();
            var volGlTimeNew = DateTime.Parse("26/09/1981  01:00:01", new CultureInfo("nb-NO", false)).ToOADate();
            var qOutExpNew = new DataVariable(qOutExpTimeNew, 2500);
            var qPumpNew = new DataVariable(qPumpTimeNew, 2510);
            var volGlNew = new DataVariable(volGlTimeNew, -1);
            _fap.UpdateTimeStep(qOutExpNew, qPumpNew, volGlNew);
            var expected3 = qPumpTime;
            var actual3 = _fap.PrevTimeStamp ?? 0;
            Assert.AreEqual(expected3, actual3, "Timestamp should be set as the previous value.");
            const int expected4 = 0;
            var actual4 = _fap.Dt;
            Assert.AreEqual(expected4, actual4, "No change in time step.");
        }
        [TestMethod]
        public void updateTimeStep_correctUpdate_changeInDtAndPrevTime()
        {
            _fap.Initialize(_x0, V0Fl, KOiFl, KOiGl);
            _fap.UpdateKFlowline(Kf);
            _fap.UpdateK2Flowline(K2F);
            var qOutExpTime = DateTime.Parse("26/09/1981  01:00:02", new CultureInfo("nb-NO", false)).ToOADate();
            var qPumpTime = DateTime.Parse("26/09/1981  01:00:02", new CultureInfo("nb-NO", false)).ToOADate();
            var volGlTime = DateTime.Parse("26/09/1981  01:00:01", new CultureInfo("nb-NO", false)).ToOADate();
            var qOutExp = new DataVariable(qOutExpTime, 2500);
            var qPump = new DataVariable(qPumpTime, 2510);
            var volGl = new DataVariable(volGlTime, -1);
            _fap.UpdateTimeStep(qOutExp, qPump, volGl);
            var expected = qPumpTime;
            var actual = _fap.PrevTimeStamp ?? MissingValueReplacement;
            Assert.AreEqual(expected, actual, "Timestamp should be set.");

            const int expected2 = 0;
            var actual2 = _fap.Dt;
            Assert.AreEqual(expected2, actual2, "Time step should be zero.");
            var qOutExpTimeNew = DateTime.Parse("26/09/1981  01:00:04", new CultureInfo("nb-NO", false)).ToOADate();
            var qPumpTimeNew = DateTime.Parse("26/09/1981  01:00:04", new CultureInfo("nb-NO", false)).ToOADate();
            var volGlTimeNew = DateTime.Parse("26/09/1981  01:00:01", new CultureInfo("nb-NO", false)).ToOADate();
            var qOutExpNew = new DataVariable(qOutExpTimeNew, 2500);
            var qPumpNew = new DataVariable(qPumpTimeNew, 2510);
            var volGlNew = new DataVariable(volGlTimeNew, -1);
            _fap.UpdateTimeStep(qOutExpNew, qPumpNew, volGlNew);
            var expected3 = qPumpTimeNew;
            var actual3 = _fap.PrevTimeStamp ?? MissingValueReplacement;
            Assert.AreEqual(expected3, actual3, "Timestamp should be set as the new value");

            const int expected4 = 2;
            var actual4 = _fap.Dt;
            Assert.AreEqual(expected4, actual4, "The time step should be two seconds.");
        }
        [TestMethod]
        public void stepIt_timeStepAboveMaxDt_reset()
        {
            _fap.Initialize(_x0, V0Fl, KOiFl, KOiGl);
            _fap.UpdateKFlowline(Kf);
            _fap.UpdateK2Flowline(K2F);
            var qOutExpTime = DateTime.Parse("26/09/1981  01:00:02", new CultureInfo("nb-NO", false)).ToOADate();
            var qPumpTime = DateTime.Parse("26/09/1981  01:00:02", new CultureInfo("nb-NO", false)).ToOADate();
            var volGlTime = DateTime.Parse("26/09/1981  01:00:01", new CultureInfo("nb-NO", false)).ToOADate();
            var qOutExp = new DataVariable(qOutExpTime, 2500);
            var qPump = new DataVariable(qPumpTime, 2510);
            var volGl = new DataVariable(volGlTime, -1);

            _fap.UpdateTimeStep(qOutExp, qPump, volGl);
            var expected = qPumpTime;
            var actual = _fap.PrevTimeStamp ?? MissingValueReplacement;
            Assert.AreEqual(expected, actual, "Timestamp should be set.");

            var expected2 = 0;
            var actual2 = _fap.Dt;
            Assert.AreEqual(expected2, actual2, "Time step should be zero.");

            var qOutExpTimeNew = DateTime.Parse("26/09/1981  01:00:04", new CultureInfo("nb-NO", false)).ToOADate();
            var qPumpTimeNew = DateTime.Parse("26/09/1981  01:00:04", new CultureInfo("nb-NO", false)).ToOADate();
            var volGlTimeNew = DateTime.Parse("26/09/1981  01:00:01", new CultureInfo("nb-NO", false)).ToOADate();
            var qOutExpNew = new DataVariable(qOutExpTimeNew, 2500);
            var qPumpNew = new DataVariable(qPumpTimeNew, 2510);
            var volGlNew = new DataVariable(volGlTimeNew, -1);
            _fap.UpdateTimeStep(qOutExpNew, qPumpNew, volGlNew);
            var expected3 = qPumpTimeNew;
            var actual3 = _fap.PrevTimeStamp ?? MissingValueReplacement;
            Assert.AreEqual(expected3, actual3, "Timestamp should be set as the new value");

            const int expected4 = 2;
            var actual4 = _fap.Dt;
            Assert.AreEqual(expected4, actual4, "The time step should be two seconds.");

            qOutExpTimeNew = DateTime.Parse("26/09/1981  01:02:04", new CultureInfo("nb-NO", false)).ToOADate();
            qPumpTimeNew = DateTime.Parse("26/09/1981  01:02:04", new CultureInfo("nb-NO", false)).ToOADate();
            volGlTimeNew = DateTime.Parse("26/09/1981  01:02:01", new CultureInfo("nb-NO", false)).ToOADate();
            qOutExpNew = new DataVariable(qOutExpTimeNew, 2500);
            qPumpNew = new DataVariable(qPumpTimeNew, 2510);
            volGlNew = new DataVariable(volGlTimeNew, -2);
            _fap.StepIt(qOutExpNew, qPumpNew, volGlNew, true);
            var expected5 = qPumpTimeNew;
            var actual5 = _fap.PrevTimeStamp.Value;
            Assert.AreEqual(expected5, actual5, "Timestamp should be set as the new value");
            const int expected6 = 120;
            var actual6 = _fap.Dt;
            Assert.AreEqual(expected6, actual6, "The time step should be 120 seconds.");

            var expected7 = volGlNew.V;
            var actual7 = _fap.X[1];
            Assert.AreEqual(expected7, actual7, "The estimated gain/loss should be set to the measured value.");

            const int expected8 = 0;
            var actual8 = _fap.X[2];
            Assert.AreEqual(expected8, actual8, "The integral of the gain/loss error should be reset.");
        }
        [TestMethod]
        public void stepIt_timeStepAboveMaxDtNewGLIsErrVal_resetToPrevGoodVal()
        {
            _fap.Initialize(_x0, V0Fl, KOiFl, KOiGl);
            _fap.UpdateKFlowline(Kf);
            _fap.UpdateK2Flowline(K2F);
            var qOutExpTime = DateTime.Parse("26/09/1981  01:00:02", new CultureInfo("nb-NO", false)).ToOADate();
            var qPumpTime = DateTime.Parse("26/09/1981  01:00:02", new CultureInfo("nb-NO", false)).ToOADate();
            var volGlTime = DateTime.Parse("26/09/1981  01:00:01", new CultureInfo("nb-NO", false)).ToOADate();
            var qOutExp = new DataVariable(qOutExpTime, 2500);
            var qPump = new DataVariable(qPumpTime, 2510);
            var volGl = new DataVariable(volGlTime, -1);

            _fap.UpdateTimeStep(qOutExp, qPump, volGl);
            var expected = qPumpTime;
            var actual = _fap.PrevTimeStamp ?? MissingValueReplacement;
            Assert.AreEqual(expected, actual, "Timestamp should be set.");

            const int expected2 = 0;
            var actual2 = _fap.Dt;
            Assert.AreEqual(expected2, actual2, "Time step should be zero.");
            var qOutExpTime1 = DateTime.Parse("26/09/1981  01:00:04", new CultureInfo("nb-NO", false)).ToOADate();
            var qPumpTime1 = DateTime.Parse("26/09/1981  01:00:04", new CultureInfo("nb-NO", false)).ToOADate();
            var volGlTime1 = DateTime.Parse("26/09/1981  01:00:01", new CultureInfo("nb-NO", false)).ToOADate();
            var qOutExp1 = new DataVariable(qOutExpTime1, 2540);
            var qPump1 = new DataVariable(qPumpTime1, 2610);
            var volGl1 = new DataVariable(volGlTime1, -1.5);
            _fap.StepIt(qOutExp1, qPump1, volGl1, true);
            var expected3 = qPumpTime1;
            var actual3 = _fap.PrevTimeStamp ?? MissingValueReplacement;
            Assert.AreEqual(expected3, actual3, "Timestamp should be set");

            var qOutExpTime2 = DateTime.Parse("26/09/1981  01:02:04", new CultureInfo("nb-NO", false)).ToOADate();
            var qPumpTime2 = DateTime.Parse("26/09/1981  01:02:04", new CultureInfo("nb-NO", false)).ToOADate();
            var volGlTime2 = DateTime.Parse("26/09/1981  01:02:01", new CultureInfo("nb-NO", false)).ToOADate();
            var qOutExp2 = new DataVariable(qOutExpTime2, 2500);
            var qPump2 = new DataVariable(qPumpTime2, 2510);
            var volGl2 = new DataVariable(volGlTime2, MissingValueReplacement);
            _fap.StepIt(qOutExp2, qPump2, volGl2, true);

            var expected4 = qPumpTime2;
            var actual4 = _fap.PrevTimeStamp ?? MissingValueReplacement;
            Assert.AreEqual(expected4, actual4, "Timestamp should be set as the new value.");

            const int expected6 = 120;
            var actual6 = _fap.Dt;
            Assert.AreEqual(expected6, actual6, "The time step should be 120 seconds.");
            var expected7 = volGl1.V;
            var actual7 = _fap.X[1];
            Assert.AreEqual(expected7, actual7, "The estimated gain/loss should be set to the previous measured value.");
            const int expected8 = 0;
            var actual8 = _fap.X[2];
            Assert.AreEqual(expected8, actual8, "The integral of the gain/loss error should be reset.");
        }
        [TestMethod]
        public void updateVolDrainback_newV0_V0SetCorrectly()
        {
            _fap.Initialize(_x0, V0Fl, KOiFl, KOiGl);
            _fap.UpdateKFlowline(Kf);
            _fap.UpdateK2Flowline(K2F);
            const int negativeV0 = -1;
            const int zeroV0 = 0;
            const int unphysicalV0 = 1000;
            const double realV0 = 5.5;

            var prevV0 = _fap.VolDrainback;
            _fap.UpdateVolDrainback(negativeV0);
            var expected = prevV0;
            var actual = _fap.VolDrainback;
            Assert.AreEqual(expected, actual);

            _fap.UpdateVolDrainback(zeroV0);
            var expected2 = prevV0;
            var actual2 = _fap.VolDrainback;
            Assert.AreEqual(expected2, actual2);

            _fap.UpdateVolDrainback(unphysicalV0);
            var expected3 = prevV0;
            var actual3 = _fap.VolDrainback;
            Assert.AreEqual(expected3, actual3);

            _fap.UpdateVolDrainback(realV0);
            var expected4 = realV0;
            var actual4 = _fap.VolDrainback;
            Assert.AreEqual(expected4, actual4);
        }

        [TestMethod]
        public void updateState_pumpOffNoTimedelay_flowlineVolToPit()
        {
            double numTol = 1e-2;
            _fap.Initialize(_x0, V0Fl, KOiFl, KOiGl);
            _fap.SetQuickConvergeNow(false);
            _fap.UpdateKFlowline(Kf);
            _fap.UpdateK2Flowline(K2F);
            const int dt = 10;
            var t = Vector.Range(0, 2500, dt); // Simulate 1500 seconds
            _fap.SetTimeStep(dt);
            const int expected = dt;
            var actual = _fap.Dt;
            Assert.AreEqual(expected, actual);
            foreach (var _ in t)
            {
                _fap.UpdateState(0,0,0,false, 0);
            }
            // Verify with a numerical tolerance
            var xDotExp = new[] {0, VDrill, 0 };
            Assert.AreEqual(xDotExp[0], _fap.X[0], numTol);
            Assert.AreEqual(xDotExp[1], _fap.X[1], numTol);
            Assert.AreEqual(xDotExp[2], _fap.X[2], numTol);
        }

        [TestMethod]
        public void updateState_pumpOnNoTimedelay_pitVolToFlowline()
        {
            const double numTol = 1e-2;
            double[] input= {VLow, VDrill - VLow, 0};
            _fap.Initialize(input, V0Fl, KOiFl, KOiGl);
            _fap.SetQuickConvergeNow(false);
            _fap.UpdateKFlowline(Kf);
            _fap.UpdateK2Flowline(K2F);
            const int dt = 10;
            var t = Vector.Range(0, 1200, dt); // Simulate 600 seconds
            _fap.SetTimeStep(dt);
            const int expected = dt;
            var actual = _fap.Dt;
            Assert.AreEqual(expected, actual);
            const double qOutLpm = FlowOutSs;
            const double qPumpLpm = qOutLpm;
            foreach (var _ in t)
            {
                _fap.UpdateState(qOutLpm, qPumpLpm, 0, false, 0);
            }
            // Verify with a numerical tolerance
            var xDotExp = _x0;
            Assert.AreEqual(xDotExp[0], _fap.X[0], numTol);
            Assert.AreEqual(xDotExp[1], _fap.X[1], numTol);
            Assert.AreEqual(xDotExp[2], _fap.X[2], numTol);
        }
    }
}
