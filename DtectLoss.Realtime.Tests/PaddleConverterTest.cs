﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accord.Math;
using DtectLossRealtime;
using DtectLossRealtime.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DtectLoss.Realtime.Tests
{

    // Missing implementation in Matlab
    [TestClass]
    public class PaddleConverterTest
    {
        private PaddleConverter pc;
        [TestInitialize()]
        public void Initialize()
        {
            var loggerMock = LoggerUtils.LoggerMock<PaddleConverter>();
            pc = new PaddleConverter(loggerMock.Object);
        }


        [TestCleanup()]
        public void Cleanup()
        {
        }


        [TestMethod]
        public void EmptyTest()
        {
            Assert.AreEqual(1, 1);
        }
    }
}




