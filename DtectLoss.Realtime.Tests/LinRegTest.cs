﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accord.Math;
using DtectLossRealtime;
using DtectLossRealtime.Calculations;
using DtectLossRealtime.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DtectLoss.Realtime.Tests
{

    [TestClass]
    public class LinRegTest
    {
        private LinReg lr;


        [TestInitialize()]
        public void Initialize()
        {
            var loggerMock = LoggerUtils.LoggerMock<LinReg>();
            lr = new LinReg(loggerMock.Object);
        }


        [TestCleanup()]
        public void Cleanup()
        {
        }


        [TestMethod]
        public void CorrectCoeffFitLineV1()
        {
            var X = new double[] {1, 2, 3 }.ToMatrix().Transpose();
            var y = new double[] {1, 2, 3 };
            lr.FitData(X, y);
            Assert.AreEqual(1, lr.Coeffs[0], 0.0001);
            //Assert.AreEqual(_gdo._ringBufferVolGainLoss.Vbuffer.Length, expected.Length);
        }

        [TestMethod]
        public void CorrectCoeffFitLineV2()
        {
            // Data used from https://www.statisticshowto.datasciencecentral.com/probability-and-statistics/regression-analysis/find-a-linear-regression-equation/
            var X = new double[] {43, 21, 25, 42, 57, 59}.ToMatrix().Transpose();
            var y = new double[] {99, 65, 79, 75, 87, 81 };
            lr.FitData(X, y);
            const double expectedSlope = 0.385225;
            Assert.AreEqual(expectedSlope, lr.Coeffs[0], 0.0001);
            //Assert.AreEqual(_gdo._ringBufferVolGainLoss.Vbuffer.Length, expected.Length);
        }
        [TestMethod]
        public void TestDot()
        {
            var X = new double[] {1, 2, 3 }.ToMatrix().Transpose();
            var c = new double[] {0.3};
            var result = X.Dot(c);
            //Assert.AreEqual(1, lr.coeffs[0], 0.0001);
            //Assert.AreEqual(_gdo._ringBufferVolGainLoss.Vbuffer.Length, expected.Length);
        }
    }
}




