﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Accord.Math;
using DtectLoss.Common;
using DtectLossRealtime.Calculations;
using DtectLossRealtime.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DtectLoss.Realtime.Tests
{
    [TestClass]
    public class TimeAlignTest
    {
        private TimeAlign _timeAlign;

        [TestInitialize()]
        public void Initialize()
        {
            var loggerMock = LoggerUtils.LoggerMock<TimeAlign>();
            _timeAlign = new TimeAlign(loggerMock.Object);
        }

        [TestCleanup()]
        public void Cleanup()
        {
        }

        [TestMethod]
        public void calcDiffSum_twoequalseriesNoShift_zerosum()
        {
            var T = Vector.Range(1.0, 100, 1);
            var s1 = Vector.Range(2.0, 200, 2);
            var s2 = s1;
            _timeAlign.Initialize(ParameterValues.FlowoutParams);
            var actual = _timeAlign.CalcDiffSum(0, 0, T, s1, s2);
            const int expected = 0;
            Assert.AreEqual(expected, actual, "Thow equal series should have diffsume zero.");
        }
        [TestMethod]
        public void calcDiffSum_shiftedbyone_nonzerosum()
        {
            var T = Vector.Range(1.0, 101, 1);
            var s1 = Vector.Range(1.0, 101, 1);
            var s2 = Vector.Range(2.0, 102, 1);
            _timeAlign.Initialize(ParameterValues.FlowoutParams);
            var actual = _timeAlign.CalcDiffSum(0, 0, T, s1, s2);
            var expected = 0.99;
            Assert.IsTrue(actual > expected, $"The two nonequal series should have diffsum of exactly 1:{actual}");
            expected = 1.01;
            Assert.IsTrue(actual < expected, $"The two nonequal series should have diffsum of exactly 1:{actual}");
        }
        [TestMethod]
        public void calcDiffSum_shiftedbyoneandtimedelayadjusted_zerosum()
        {
            var T = Vector.Range(1.0, 101, 1);
            var s2 = Vector.Range(1.0, 101, 1);
            var s1 = Vector.Range(2.0, 102, 1);
            _timeAlign.Initialize(ParameterValues.FlowoutParams);
            var actual = _timeAlign.CalcDiffSum(1, 0, T, s1, s2);
            const int expected = 0;
            Assert.AreEqual(expected, actual, $"the sum of differences of the two shifted time-series should be zero when compensated for with time-delay:{actual}");
        }

        [TestMethod]
        public void align_equalseries_zerodelayandtimeconst()
        {
            var deltaTd = 2.0.SecondsToOADate();
            double time = DateTime.Parse("01/01/2000", new CultureInfo("nb-NO", false)).ToOADate();
            var tD = new List<double>() { time};
            var s1 = new List<double>() { 1};
            var s2 = new List<double>() { 1};
            foreach (var k in Enumerable.Range(0, 99))
            {
                tD.Add(tD[k].AddSeconds(deltaTd));
                s1.Add(s1[k] + 1);
                s2.Add(s2[k] + 1);
            }
            _timeAlign.Initialize(ParameterValues.FlowoutParams);
            var (tdSamples, tcS, tdS) = _timeAlign.Align(s1.ToArray(), s2.ToArray(), tD.ToArray());
            Assert.AreEqual(0, tcS, $"Two equal series should produce zero Tc {tcS}");
            Assert.AreEqual(0, tdSamples, $"Two equal series should produce zero td samples {tdSamples}");
            Assert.AreEqual(0, tdS, $"Two equal series should produce zero tdS {tdS}");
        }
        [TestMethod]
        public void align_shifteseries_correcttimedelayandnotimeconst()
        {
            var deltaTd = 3.0.SecondsToOADate();
            double time = DateTime.Parse("01/01/2000", new CultureInfo("nb-NO", false)).ToOADate();
            var tD = new List<double>() { time};
            var s1 = new List<double>() { 2};
            var s2 = new List<double>() { 1};
            foreach (var k in Enumerable.Range(0, 99))
            {
                tD.Add(tD[k].AddSeconds(deltaTd));
                if (k < 48)
                {
                    s1.Add(s1[k] + 1);
                    s2.Add(s2[k] + 1);
                }
                else
                {
                    s1.Add(s2[k]);
                    s2.Add(s2[k]);
                }
            }

            s1[48] = s2[48];
            _timeAlign.Initialize(ParameterValues.FlowoutParams);
            var (tdSamples, tcS, tdS) = _timeAlign.Align(s1.ToArray(), s2.ToArray(), tD.ToArray());
            Assert.AreEqual(0, tcS, $"Shifted series should produce zero Tc:{tcS}");
            Assert.AreEqual(1, tdSamples, $"Shifted series should produce one td samples {tdSamples}");
            var actual = tdS.SubtractSeconds(deltaTd);
            var expected = deltaTd.OADateToSeconds() / 100.0;
            Assert.IsTrue(actual < expected, $"shifted series should produce sec equal to one sample td_s:{actual}, td_true:{expected}");
        }
        [TestMethod]
        public void align_biggershiftedseries_correcttimedelayandnotimeconst()
        {
            var deltaTd = 2.0.SecondsToOADate();
            const int offset = 4;
            var time = DateTime.Parse("01/01/2000", new CultureInfo("nb-NO", false)).ToOADate();
            var tD = new List<double>() { time};
            var s1 = new List<double>() { 5};
            var s2 = new List<double>() { s1[0] - offset};
            foreach (var k in Enumerable.Range(0, 99))
            {
                tD.Add(tD[k].AddSeconds(deltaTd));
                s1.Add(s1[k] + 1);
                s2.Add(s2[k] + 1);
            }
            _timeAlign.Initialize(ParameterValues.FlowoutParams);
            var (tdSamples, tcS, tdS) = _timeAlign.Align(s1.ToArray(), s2.ToArray(), tD.ToArray());
            Assert.AreEqual(0, tcS, $"Test should produce zero Tc: {tcS}");
            Assert.AreEqual(offset, tdSamples, $"shifted time-series should produce one td_samples {tdSamples}");
            var actual = tdS.SubtractSeconds(deltaTd) * offset;
            var expected = deltaTd.OADateToSeconds() / 100.0;
            Assert.IsTrue(actual < expected, $"shifted series should produce sec equal to one sample td_s:{tdS}, td_true:{deltaTd}");
        }

        [TestMethod]
        public void align_lowpassfilteredseries_correcttimeconstandnodelay()
        {
            var deltaTd = 2.0.SecondsToOADate();
            var time = DateTime.Parse("01/01/2000", new CultureInfo("nb-NO", false)).ToOADate();
            var tD = new List<double>() { time};
            var s1 = new List<double>() { 1};
            var s2 = new List<double>() { 1};
            const int tcTrueS = 6;
            var lp = new LowPass(deltaTd.OADateToSeconds(), tcTrueS);
            foreach (var k in Enumerable.Range(0, 99))
            {
                tD.Add(tD[k].AddSeconds(deltaTd.OADateToSeconds()));
                if (k < 18)
                {
                    s1.Add(s1[k]);
                }
                else
                {
                    s1.Add(5);
                }
                lp.Filter(s1.LastOrDefault(), 0);
                s2.Add(lp.FilteredSignal ?? 0);
            }
            _timeAlign.Initialize(ParameterValues.FlowoutParams);
            var (tdSamples, tcS, tdS) = _timeAlign.Align(s1.ToArray(), s2.ToArray(), tD.ToArray());
            Assert.IsTrue(tcS - tcTrueS < deltaTd * 0.5, $"Tc: {tcS} TcTrue: {tcTrueS}");
            Assert.AreEqual(0, tdSamples, $"Two equal series should produce zero td samples {tdSamples}");
            Assert.AreEqual(0, tdS, $"No timedelay in this testcase tds {tdS}");
        }
        [TestMethod]
        public void align_lowpassfilteredandshiftedseries_bothTcanddelayCorrect()
        {
            var deltaTd = 5.0.SecondsToOADate();
            var time = DateTime.Parse("01/01/2000", new CultureInfo("nb-NO", false)).ToOADate();
            var tD = new List<double>() { time};
            var s1 = new List<double>() { 10};
            var s2 = new List<double>() { 10};
            const int tdTrueSamples = 5;
            const int tcTrueS = 7;

            foreach (var k in Enumerable.Range(0, tdTrueSamples))
            {
                tD.Add(tD[k].AddSeconds(deltaTd.OADateToSeconds()));
                s1.Add(s1[k]);
                s2.Add(s2[k]);
            }
            var lp = new LowPass(deltaTd.OADateToSeconds(), tcTrueS);

            foreach (var k in Enumerable.Range(tdTrueSamples, 199 - tdTrueSamples))
            {
                tD.Add(tD[k].AddSeconds(deltaTd.OADateToSeconds()));
                if (k < 48)
                {
                    s1.Add(s1[k]);
                }
                else
                {
                    s1.Add(0);
                }
                lp.Filter(s1[k - tdTrueSamples + 1], 0);
                s2.Add(lp.FilteredSignal ?? 0);
            }
            _timeAlign.Initialize(ParameterValues.FlowoutParams);
            var sum = _timeAlign.CalcDiffSum(tdTrueSamples, tcTrueS, tD.ToArray(), s1.ToArray(), s2.ToArray());
            Assert.IsTrue(sum < 0.01, $"When using the correct values, obj fun should be zero {sum}");
            
            var (tdSamples, tcS, _) = _timeAlign.Align(s1.ToArray(), s2.ToArray(), tD.ToArray());
            var actual = tcS - tcTrueS;
            var expected = (deltaTd * 0.3).OADateToSeconds();
            Assert.IsTrue(actual < expected, $"Tc: {tcS} TcTrue: {tcTrueS}");
            Assert.AreEqual(tdTrueSamples, tdSamples, $"Two equal series should produce {tdTrueSamples} samples delay, but actually tdSamples: {tdSamples}");
        }
    }
}
