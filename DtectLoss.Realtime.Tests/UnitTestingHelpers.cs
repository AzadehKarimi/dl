﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DtectLoss.Realtime.Tests
{
    public class RelativeDoubleComparer : System.Collections.IComparer
    {
        private readonly double _numTol;

        public RelativeDoubleComparer(double numTol)
        {
            _numTol = numTol;
        }
        public int Compare(object xIn, object yIn)
        {
            var x = (double)xIn;
            var y = (double)yIn;

            var relDiff = x * _numTol;
            var diff = x - y;
            if (diff > relDiff)
            {
                return 1;
            }
            if (diff < -relDiff)
            {
                return -1;
            }

            return 0;
        }
    }
    class ToleranceComparer : EqualityComparer<double>
    {
        private readonly double _tolerance = 0.0;

        public ToleranceComparer(double tolerance)
        {
            _tolerance = tolerance;
        }

        public override bool Equals(double b1, double b2)
        {
            var diff = Math.Abs(b1 - b2);
            return !(diff > _tolerance);
        }

        public override int GetHashCode(double d)
        {
            return d.GetHashCode();
        }
    }
    public class ToleranceDoubleComparer : System.Collections.IComparer
    {
        private readonly double _numTol;

        public ToleranceDoubleComparer(double numTol)
        {
            _numTol = numTol;
        }
        public int Compare(object xIn, object yIn)
        {
            var x = (double)xIn;
            var y = (double)yIn;

            var diff = x - y;
            if (diff > _numTol)
            {
                return 1;
            }
            if (diff < -_numTol)
            {
                return -1;
            }

            return 0;
        }
    }
    public static class Helpers
    {
        /// <summary>
        /// Helper function for finding delay
        /// </summary>
        /// <param name="ser1"></param>
        /// <param name="ser2"></param>
        /// <param name="dtS"></param>
        /// <returns></returns>
        public static double FindDelay(double[] ser1, double[] ser2, double dtS)
        {
            var ns = ser1.Length;
            const int delayIndexSeed = 0;
            // Zip the two arrays into pairwise tuples and perform calculation for each tuple
            var minDevMeanSeed = ser1.Zip(ser2, (s1, s2) => Math.Abs(s1 - s2)).Average();
            var l = (int)Math.Round(20 / dtS);

            // Local function to be used by the Aggregate function
            (int, double) CalcDelayIndex((int delayIndexLoc, double minDevMeanLoc) state, int i)
            {
                var ser1T = ser1.Take(ns - l);
                var ser2T = ser2.Skip(i).Take(ns - i - l + i - 1);
                var devMean = ser1T.Zip(ser2T, (s1, s2) => Math.Abs((s1 - s2))).Average();
                if (devMean < state.minDevMeanLoc)
                {
                    state.delayIndexLoc = i;
                    state.minDevMeanLoc = devMean;
                }
                return (state.delayIndexLoc, state.minDevMeanLoc);
            }
            // Perform the Aggregation. Uses a tuple to accumulate index and minDevMean
            var (idx, _) = Enumerable.Range(0, l).Aggregate((delayIndexSeed, minDevMeanSeed), CalcDelayIndex);
            return idx * dtS;
        }
    }
}