﻿using DtectLossRealtime;
using DtectLossRealtime.Helpers;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DtectLoss.Realtime.Tests
{

    [TestClass]
    public class StateMachineTest
    {
        private StateMachine _sm;
        private Process _process;
        private Mock<ILogger<Process>> _loggerMock;
        private Mock<ILogger<StateMachine>> _stateMachineLoggerMock;
        private Mock<ILogger<StandpipePressureDelay>> _loggerMockStandpipePressureDelay;

        [TestInitialize()]
        public void Initialize()
        {
            _stateMachineLoggerMock = LoggerUtils.LoggerMock<StateMachine>();
            var nullLogger = new Mock<ILogger<DefaultUnitsMappingReader>>().Object;
            _loggerMockStandpipePressureDelay = LoggerUtils.LoggerMock<StandpipePressureDelay>();
            var standpipePressureDelay = new StandpipePressureDelay(_loggerMockStandpipePressureDelay.Object);
            var mappingReader = new DefaultUnitsMappingReader(nullLogger);
            _loggerMock = LoggerUtils.LoggerMock<Process>();
            var loggerMockPaddleConverter = LoggerUtils.LoggerMock<PaddleConverter>();
            var paddleConverter = new PaddleConverter(loggerMockPaddleConverter.Object);
            _process = new Process(_loggerMock.Object, mappingReader, standpipePressureDelay, paddleConverter);


            var rwdu = new []
            {
                "flowIn", "m3/s",
                "flowOut", "m3/s",
                "flowPercent", "%",
                "rpm", "rpm",
                "velRop", "m/s",
                "presSP", "kPa",
                "depthHole", "m",
                "depthBit", "m",
                "heightHook", "m",
                "volGainLoss", "m3",
                "volTripTank", "m3",
                "weightOnBit", "kN",
            }.CreateDictionaryFromEvenOddList();

            _process.SetAvailableTags(rwdu);
            _sm = new StateMachine(_stateMachineLoggerMock.Object);
        }


        [TestCleanup()]
        public void Cleanup()
        {
        }
    }
}
