﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accord;
using Accord.Math;
using DtectLoss.Common;
using DtectLossRealtime.Calculations;
using DtectLossRealtime.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DtectLoss.Realtime.Tests
{

    [TestClass]
    public class FilterDerivativeTest
    {
        private FilterDerivative _filterDerivative;
        private int _testEx = 1; // 1=sinus, 2=ramp
        private double _tfiltS = 0.5;
        private double _dtS = 0.01; //Time step in simulation
        private double[] _tS;
        private double[] _dataVec;
        private int _n;
        private double[] _derTrue;
        private double[] _derAlg;

        public void InitFilterDerivativeTest(int ex, bool constantDtParam)
        {
            var constantDt = constantDtParam;
            _testEx = ex;     // 1 = sinus, 2 = ramp
            const int tendS = 100;
            _tfiltS = 0.5;
            _dtS = 0.01;    //  Time step in simulation

            if (constantDt)
            {
                _tS = Vector.Range(_dtS, tendS, _dtS);
            }
            else
            {
                var nt = Math.Floor(tendS / _dtS);
                _tS = Vector.Create(2 * (int)nt, 0.0);

                // Local function to be used by aggregation
                List<double> AccumulateNewTs(List<double> accumulatedList, double currentValue)
                {
                    var dtS = _dtS * 2 * new Random().NextDouble();
                    if (dtS < 0.2 * _dtS)
                    {
                        dtS = 0;
                    }
                    accumulatedList.Add(accumulatedList.Last() + dtS + currentValue);
                    return accumulatedList;
                }
                // Perform aggregation to create a new ts list
                _tS = _tS.Aggregate(new List<double>() { _tS.First() }, AccumulateNewTs)
                        .TakeWhile(x => x < tendS)
                        .ToArray();

            }
            _n = _tS.Length;
            if (_testEx == 1)
            {
                var amp = 1;
                var tperS = 20;
                _dataVec = _tS.Select(x => amp * Math.Sin(2 * Math.PI * x / tperS)).ToArray();
                _derTrue = _tS.Select(x => amp * 2 * Math.PI / tperS * Math.Cos(2 * Math.PI * x / tperS)).ToArray();
            }
            else if (_testEx == 2)
            {
                //TODO:NB Check if this needs to be 3600.0. Currently fails when using that value
                const int ropMps = 30 / 3600;
                _dataVec = _tS.Select(x => x * ropMps).ToArray();
                _derTrue = Vector.Create(_n, 1.0 * ropMps);
            }
        }


        [TestInitialize()]
        public void Initialize()
        {
            _filterDerivative = new FilterDerivative(_tfiltS);
            _derAlg = Vector.Create(_n, 0.0);
        }


        [TestCleanup()]
        public void Cleanup()
        {
            _filterDerivative = null;
        }

        [TestMethod]
        [DataRow(1, false)]
        [DataRow(2, false)]
        [DataRow(1, true)]
        [DataRow(2, true)]
        public void Smoothderivative1storderCorrectInitCorrectOutput(int ex, bool constantDt)
        {
            InitFilterDerivativeTest(ex, constantDt);
            const double numTol = 0.1; //relative error which is tolerated
            var rangeTrue = _derTrue.Max() - _derTrue.Min();

            _derAlg = Enumerable
                .Range(1, _n - 1)
                .Select((x, index) => _filterDerivative.Smoothderivative1storder(_dataVec[index + 1], _tS[index + 1].SecondsToOADate()))
                .ToArray();
            //    % verify with a numerical tolerance
            var startindex = (int)Math.Round(50 / _dtS);

            var delaytimeS = Helpers.FindDelay(_derTrue, _derAlg, _dtS);
            var delayindex = Math.Floor(delaytimeS / _dtS);
            var delayTrue = _derTrue;
            delayTrue.Take(_n - (int)delayindex).ToArray().CopyTo(delayTrue, (int)delayindex);

            var ampFilterTmp = Accord.Statistics.Measures.StandardDeviation(delayTrue.Skip((int)delayindex).Take(_n - ((int)delayindex) - 1).ToArray());
            var ampFilter = Accord.Statistics.Measures.StandardDeviation(_derAlg) / ampFilterTmp;

            if (_testEx == 1)
            {
                var act1 = _derAlg.Skip(startindex).Take(_n - startindex).Select(x => x).Average();
                var exp1 = _derTrue.Skip(startindex).Take(_n - startindex).Average();
                Assert.AreEqual(exp1, act1, numTol * rangeTrue, "Mean value should be the same");


                var act2 = _derAlg.Skip(startindex).Take(_n - startindex).Select(x => x).ToArray().Variance();
                var exp2 = _derTrue.Skip(startindex).Take(_n - startindex).ToArray().Variance();
                Assert.AreEqual(exp2, act2, numTol * rangeTrue * exp2, "Variance value should be the same");

                Assert.IsTrue(delaytimeS.IsLessThan(5.0), "Delay should be less than 4 seconds.");
                Assert.AreEqual(ampFilter, 1.0, numTol, "Filter gain should be 1");
            }
            else
            {
                var act1 = _derAlg.Skip(startindex).Take(_n - startindex).Select(x => x).Average();
                var exp1 = _derTrue.Skip(startindex).Take(_n - startindex).Average();
                Assert.AreEqual(exp1, act1, numTol * exp1, "Mean value should be the same");
            }
        }


        [TestMethod]
        [DataRow(1, false)]
        [DataRow(2, false)]
        [DataRow(1, true)]
        [DataRow(2, true)]
        public void Smoothderivative2ndOrderCorrectInitCorrectOutput(int ex, bool constantDt)
        {
            InitFilterDerivativeTest(ex, constantDt);
            const double numTol = 0.1; //relative error which is tolerated
            var rangeTrue = _derTrue.Max() - _derTrue.Min();

            _derAlg = Enumerable
                .Range(1, _n - 1)
                .Select((x, index) => _filterDerivative.Smoothderivative2ndorder(_dataVec[index + 1], _tS[index + 1].SecondsToOADate()))
                .ToArray();
            //    % verify with a numerical tolerance
            var startindex = (int)Math.Round(50 / _dtS);

            var delaytimeS = Helpers.FindDelay(_derTrue, _derAlg, _dtS);
            var delayindex = (int)Math.Floor(delaytimeS / _dtS);
            var delayTrue = _derTrue;
            delayTrue.Take(_n - delayindex).ToArray().CopyTo(delayTrue, delayindex);

            var ampFilterTmp = Accord.Statistics.Measures.StandardDeviation(delayTrue.Skip(delayindex).Take(_n - (delayindex) - 1).ToArray());
            var ampFilter = Accord.Statistics.Measures.StandardDeviation(_derAlg) / ampFilterTmp;

            if (_testEx == 1)
            {
                var act1 = _derAlg.Skip(startindex).Take(_n - startindex).Select(x => x).Average();
                var exp1 = _derTrue.Skip(startindex).Take(_n - startindex).Average();
                Assert.AreEqual(exp1, act1, numTol * rangeTrue, "Mean value should be the same");


                var act2 = _derAlg.Skip(startindex).Take(_n - startindex).Select(x => x).ToArray().Variance();
                var exp2 = _derTrue.Skip(startindex).Take(_n - startindex).ToArray().Variance();
                Assert.AreEqual(exp2, act2, numTol * rangeTrue * exp2, "Variance value should be the same");

                Assert.IsTrue(delaytimeS.IsLessThan(5.0), "Delay should be less than 4 seconds.");
                Assert.AreEqual(ampFilter, 1.0, numTol, "Filter gain should be 1");
            }
            else
            {
                var act1 = _derAlg.Skip(startindex).Take(_n - startindex).Select(x => x).Average();
                var exp1 = _derTrue.Skip(startindex).Take(_n - startindex).Average();
                Assert.AreEqual(exp1, act1, numTol * rangeTrue, "Mean value should be the same");
            }
        }
    }
} 