﻿using System.Linq;
using DtectLossRealtime.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static DtectLossRealtime.Helpers.MathFunctions;

namespace DtectLoss.Realtime.Tests
{

    [TestClass]
    public class VecTest
    {


        [TestInitialize()]
        public void Initialize()
        {
        }


        [TestCleanup()]
        public void Cleanup()
        {
        }

        [TestMethod]
        public void DiffTest()
        {
            var testData = new double[]{1, 10, 20, 30};
            var expected = new double[]{9, 10, 10};
            var actual = testData.Diff();
            Assert.AreEqual(expected.Average(), actual.Average());
        }
        [TestMethod]
        public void InterpolatePreviousV1()
        {
            var v = new[] {100.0, 200, 300, 400, 500, 600, 700, 800}; 
            var t = new[] {4100.0, 4200, 4300, 4400, 4500, 4600, 4700, 4800}; 
            var tToFindValuesFor = new[] {345, 4150.0, 4320.0, 6777};
            var result = Interp1Previous(t, v, tToFindValuesFor);
            Assert.AreEqual(double.NaN, result[0] );
            Assert.AreEqual(100.0, result[1] );
            Assert.AreEqual(300.0, result[2] );
            Assert.AreEqual(double.NaN, result[3] );
        }
    }
}




