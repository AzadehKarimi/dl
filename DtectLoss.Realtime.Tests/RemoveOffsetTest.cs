﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accord.Math;
using DtectLossRealtime;
using DtectLossRealtime.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static DtectLoss.Common.CommonConstants;

namespace DtectLoss.Realtime.Tests
{
    [TestClass]
    public class RemoveOffsetTest
    {
        private const double Treshold = 5;
        private const double Offset = -2;
        private readonly double _flowInLowLimLpm = 400;

        [TestInitialize()]
        public void Initialize()
        {
        }

        [TestCleanup()]
        public void Cleanup()
        {
        }

        [TestMethod]
        public void NoChangeInputCorrectOutput()
        {
            var yIn = new double[] {0, 0};
            var (yOutAct, offsetOutAct) = MathFunctions.RemoveOffset(yIn, Offset, Treshold);
            const int yOutExp = -2;
            const double offsetOutExp = Offset;
            Assert.AreEqual(yOutExp, yOutAct);
            Assert.AreEqual(offsetOutExp, offsetOutAct);
        }

        [TestMethod]
        public void PositiveChangeWithinThresholdInInputCorrectOutput()
        {
            var yIn = new double[] {0, 2};
            var (yOutAct, offsetOutAct) = MathFunctions.RemoveOffset(yIn, Offset, Treshold);
            const int yOutExp = 0;
            const double offsetOutExp = Offset;
            Assert.AreEqual(yOutExp, yOutAct);
            Assert.AreEqual(offsetOutExp, offsetOutAct);
        }

        [TestMethod]
        public void NegativeChangeWithinThresholdInInputCorrectOutput()
        {
            var yIn = new double[] {0, -2};
            var (yOutAct, offsetOutAct) = MathFunctions.RemoveOffset(yIn, Offset, Treshold);
            const int yOutExp = -4;
            const double offsetOutExp = Offset;
            Assert.AreEqual(yOutExp, yOutAct);
            Assert.AreEqual(offsetOutExp, offsetOutAct);
        }
        [TestMethod]
        public void PositiveChangeGreaterThanThresholdInInputCorrectOutput()
        {
            var yIn = new double[] {0, 6};
            var (yOutAct, offsetOutAct) = MathFunctions.RemoveOffset(yIn, Offset, Treshold);
            const int yOutExp = -2;
            const int offsetOutExp = -8;
            Assert.AreEqual(yOutExp, yOutAct);
            Assert.AreEqual(offsetOutExp, offsetOutAct);
        }
        [TestMethod]
        public void NegativeChangeGreaterThanThresholdInInputCorrectOutput()
        {
            var yIn = new double[] {0, -6};
            var (yOutAct, offsetOutAct) = MathFunctions.RemoveOffset(yIn, Offset, Treshold);
            const int yOutExp = -2;
            const int offsetOutExp = 4;
            Assert.AreEqual(yOutExp, yOutAct);
            Assert.AreEqual(offsetOutExp, offsetOutAct);
        }
        [TestMethod]
        public void MissingValueReturnErrorVal()
        {
            var yIn = new[] {10, MissingValueReplacement};
            var (yOutAct, offsetOutAct) = MathFunctions.RemoveOffset(yIn, Offset, Treshold);
            const double yOutExp = MissingValueReplacement;
            const double offsetOutExp = Offset;
            Assert.AreEqual(yOutExp, yOutAct);
            Assert.AreEqual(offsetOutExp, offsetOutAct);
        }
        [TestMethod]
        public void FlowInErrValCorrectOutput()
        {
            var yIn = new double[] {0, -6};
            var (yOutAct, offsetOutAct) = MathFunctions.RemoveOffset(yIn, Offset, Treshold, MissingValueReplacement);
            const int yOutExp = -2;
            const int offsetOutExp = 4;
            Assert.AreEqual(yOutExp, yOutAct);
            Assert.AreEqual(offsetOutExp, offsetOutAct);
        }
        [TestMethod]
        public void FlowInLessThanLimitUseOriginalThreshold()
        {
            var yIn = new double[] {0, -6};
            var (yOutAct, offsetOutAct) = MathFunctions.RemoveOffset(yIn, Offset, Treshold, _flowInLowLimLpm - 10);
            const int yOutExp = -2;
            const int offsetOutExp = 4;
            Assert.AreEqual(yOutExp, yOutAct);
            Assert.AreEqual(offsetOutExp, offsetOutAct);
        }
        [TestMethod]
        public void FlowInGreaterThanLimitUseHalfOfOriginalThreshold()
        {
            var yIn = new double[] {0, -3};
            var (yOutAct, offsetOutAct) = MathFunctions.RemoveOffset(yIn, Offset, Treshold, _flowInLowLimLpm + 10);
            const int yOutExp = -2;
            const int offsetOutExp = 1;
            Assert.AreEqual(yOutExp, yOutAct);
            Assert.AreEqual(offsetOutExp, offsetOutAct);
        }
    }
}




