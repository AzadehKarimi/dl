﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accord.Math;
using Accord;
using DtectLossRealtime.Calculations;
using DtectLossRealtime.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static System.Math;

namespace DtectLoss.Realtime.Tests
{
    [TestClass]
    public class LowpassFilterTest
    {
        private LowPass _lowPass;
        private int _testEx = 1; // 1=sinus, 2=ramp
        private double _tfiltS = 0.5;
        private double _dtS = 0.01; //Time step in simulation
        private double[] _tS;
        private double[] _dataVec;
        private int _n;
        private double[] _lpAlg;

        public void InitLowpassFilterTest(int ex, bool constantDtParam)
        {
            var constantDt = constantDtParam;
            _testEx = ex;
            const int tendS = 100;
            _tfiltS = 0.5;
            _dtS = 0.01;    //  Time step in simulation
            if (constantDt)
            {
                _tS = Vector.Range(_dtS, tendS, _dtS);
            }
            else
            {
                var nt = Floor(tendS / _dtS);
                _tS = Vector.Create(2 * (int)nt, 0.0);
                // Local function to be used by aggregation
                List<double> AccumulateNewTs(List<double> accumulatedList, double currentValue)
                {
                    var dtS = _dtS * 2 * new Random().NextDouble();
                    if (dtS < 0.2 * _dtS)
                    {
                        dtS = 0;
                    }
                    accumulatedList.Add(accumulatedList.Last() + dtS + currentValue);
                    return accumulatedList;
                }
                // Perform aggregation to create a new ts list
                _tS = _tS.Aggregate(new List<double>() { _tS.First() }, AccumulateNewTs)
                        .TakeWhile(x => x < tendS)
                        .ToArray();

            }
            _n = _tS.Length;
            if (_testEx == 1)
            {
                const int amp = 1;
                const int tperS = 20;
                _dataVec = _tS.Select(x => amp * Sin(2 * PI * x / tperS)).ToArray();
            }
            else if (_testEx == 2)
            {
                const int ropMps = 30 / 3600;
                _dataVec = _tS.Select(x => x * ropMps).ToArray();
            }

        }

        [TestInitialize()]
        public void Initialize()
        {
            _lowPass = new LowPass(_dtS, _tfiltS);
            _lpAlg = Vector.Create(_n, 0.00);
        }


        [TestCleanup()]
        public void Cleanup()
        {
            _lowPass = null;
        }

        [TestMethod]
        [DataRow(1, false)]
        [DataRow(2, false)]
        [DataRow(1, true)]
        [DataRow(2, true)]
        public void FilterForCustomDtCorrectInitCorrectInput(int ex, bool constantDt)
        {
            InitLowpassFilterTest(ex, constantDt);
            const double numTol = 0.1; //relative error which is tolerated
            var rangeTrue = _dataVec.Max() - _dataVec.Min();
            double CalcFilter(double input, int idx)
            {
                if (constantDt)
                {
                    _lowPass.Filter(_dataVec[idx + 1], 0);

                }
                else
                {
                    var dtS = _tS[idx + 1] - _tS[idx];
                    _lowPass.FilterForCustomDt(_dataVec[idx + 1], dtS, 0);
                }
                return _lowPass.FilteredSignal ?? 0;
            }

            _lpAlg = Enumerable
                .Range(1, _n - 1)
                .Select((x, index) => CalcFilter(x, index)) 
                .ToArray();
            //    % verify with a numerical tolerance
            var startindex = (int)Round(50 / _dtS);

            var delaytimeS = Helpers.FindDelay(_dataVec, _lpAlg, _dtS);
            var delayindex = Floor(delaytimeS / _dtS);
            var delayTrue = _dataVec;
            delayTrue.Take(_n - (int)delayindex).ToArray().CopyTo(delayTrue, (int)delayindex);

            var ampFilterTmp = Accord.Statistics.Measures.StandardDeviation(delayTrue.Skip((int)delayindex).Take(_n - (int)delayindex - 1).ToArray());
            var ampFilter = Accord.Statistics.Measures.StandardDeviation(_lpAlg) / ampFilterTmp;

            if (_testEx == 1)
            {
                var act1 = _lpAlg.Skip(startindex).Take(_n - startindex).Select(x => x).Average();
                var exp1 = _dataVec.Skip(startindex).Take(_n - startindex).Average();
                Assert.AreEqual(exp1, act1, numTol * rangeTrue, "Mean value should be the same");


                var act2 = _lpAlg.Skip(startindex).Take(_n - startindex).Select(x => x).ToArray().Variance();
                var exp2 = _dataVec.Skip(startindex).Take(_n - startindex).ToArray().Variance();
                Assert.AreEqual(exp2, act2, numTol * rangeTrue * exp2, "Variance value should be the same");
                Assert.IsTrue(delaytimeS.IsLessThan(5.0), "Delay should be less than 4 seconds.");
                Assert.AreEqual(ampFilter, 1.0, numTol, "Filter gain should be 1");
            }
            else
            {
                var act1 = _lpAlg.Skip(startindex).Take(_n - startindex).Select(x => x).Average();
                var exp1 = _dataVec.Skip(startindex).Take(_n - startindex).Average();
                Assert.AreEqual(exp1, act1, numTol * exp1, "Mean value should be the same");
            }
        }
    }
}
