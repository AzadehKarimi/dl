﻿using DtectLossRealtime;
using DtectLossRealtime.Calculations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DtectLoss.Realtime.Tests
{
    // Missing implementation in Matlab
    [TestClass]
    public class ExpectedStandpipePresTest
    {
        private ExpectedStandpipePres _expectedStandpipePres;
        [TestInitialize()]
        public void Initialize()
        {
            var loggerMock = LoggerUtils.LoggerMock<PiecewiseLinFit>();
            var piecewiseLinFit = new PiecewiseLinFit(loggerMock.Object);
            piecewiseLinFit.Initialize(6000, 0, 60, 50);
            _expectedStandpipePres = new ExpectedStandpipePres(piecewiseLinFit);
        }


        [TestCleanup()]
        public void Cleanup()
        {
        }


        [TestMethod]
        public void EmptyTest()
        {
            Assert.AreEqual(1, 1);
        }
    }
}




