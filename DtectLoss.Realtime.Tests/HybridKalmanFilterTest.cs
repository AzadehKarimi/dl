﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Accord;
using Accord.Math;
using DtectLoss.Common;
using DtectLossRealtime;
using DtectLossRealtime.Calculations;
using DtectLossRealtime.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static DtectLoss.Common.DateHelpers;

namespace DtectLoss.Realtime.Tests
{

    [TestClass]
    public class HybridKalmanFilterTest
    {
        private const double MeasCovariance = 50;
        private const double PInit = 50;
        private const double XInit = 0;
        private const double InitTimeD = 2017;
        private HybridKalmanFilter _hkf;

        [TestInitialize()]
        public void Initialize()
        {
            _hkf = new HybridKalmanFilter();
        }

        [TestCleanup()]
        public void Cleanup()
        {
        }

        [TestMethod]
        public void UpdateMeasFlowOutZeroSleepModeActiveTrue()
        {
            const int dTs = 2;
            var tD = Vector.Range(InitTimeD + 1, InitTimeD + 3, 1);
            _hkf.Initialize(MeasCovariance, XInit, PInit);
            const int flowInLpm = 0;
            const int flowOutLpm = 0;
            const int depthBitAndHoleM = 2000;
            foreach (var d in tD)
            {
                _hkf.AddFlowAndDepthMeas(d, flowOutLpm, depthBitAndHoleM, depthBitAndHoleM);
                _hkf.Update(flowInLpm, flowOutLpm, dTs);
                Assert.IsTrue(_hkf.SleepModeActive);
            }
        }

        [TestMethod]
        public void UpdateMeasFlowOutZeroAndFlowInPositiveFlowOutPredZero()
        {
            const int dTs = 2;
            var tD = Vector.Range(InitTimeD + 1, InitTimeD + 3, 1);
            _hkf.Initialize(MeasCovariance, XInit, PInit);
            const int flowInLpm = 2000;
            const int flowOutLpm = 0;
            const int depthBitAndHoleM = 2000;
            foreach (var d in tD)
            {
                _hkf.AddFlowAndDepthMeas(d, flowOutLpm, depthBitAndHoleM, depthBitAndHoleM);
                _hkf.Update(flowInLpm, flowOutLpm, dTs);
                Assert.AreEqual(_hkf.Xk, 0);
            }
        }

        [TestMethod]
        public void UpdateMeasFlowOutPositiveAndFlowInPositiveSleepModeNotActive()
        {
            const int dTs = 2;
            var tD = Vector.Range(InitTimeD + 1, InitTimeD + 3, 1);
            _hkf.Initialize(MeasCovariance, XInit, PInit);
            const int flowInLpm = 2000;
            const int flowOutLpm = 2000;
            const int depthBitAndHoleM = 2000;
            foreach (var d in tD)
            {
                _hkf.AddFlowAndDepthMeas(d, flowOutLpm, depthBitAndHoleM, depthBitAndHoleM);
                _hkf.Update(flowInLpm, flowOutLpm, dTs);
                Assert.IsFalse(_hkf.SleepModeActive);
            }
        }

        [TestMethod]
        //% in case the return flow has been positive for a while, but
        //% then is zero for a long period, e.g.due to tripping, then
        //% the filter should go into sleep mode again.
        public void UpdateLongPeriodWOFlowOutSleepModeActive()
        {
            const int dTs = 2;
            var tD = Vector.Range(1, _hkf.NBuffer * dTs + 7, dTs);
            _hkf.Initialize(MeasCovariance, XInit, PInit);
            const int flowInLpm = 2000;
            var flowOutLpm = 2000;
            const int depthBitAndHoleM = 2000;
            foreach (var d in Enumerable.Range(0, 1))
            {
                _hkf.AddFlowAndDepthMeas(tD[d], flowOutLpm, depthBitAndHoleM, depthBitAndHoleM);
                _hkf.Update(flowInLpm, flowOutLpm, dTs);
                Assert.IsFalse(_hkf.SleepModeActive);
            }
            flowOutLpm = 0;
            foreach (var d in Enumerable.Range(2, _hkf.NBuffer))
            {
                _hkf.AddFlowAndDepthMeas(tD[d], flowOutLpm, depthBitAndHoleM, depthBitAndHoleM);
                _hkf.Update(flowInLpm, flowOutLpm, dTs);
            }
            Assert.IsTrue(_hkf.SleepModeActive);
        }

        [TestMethod]
        public void CheckForLossOfReturnFlowFlowZeroReturnFalse()
        {
            const int dTs = 2;
            const double durationS = 6.0;
            var tD = Vector.Range(0.0, durationS)
                            .Select(x => InitTimeD.AddSeconds(x))
                            .ToArray();

            _hkf.Initialize(MeasCovariance, XInit, PInit);
            const int flowOutLpm = 2000;
            foreach (var _ in tD)
            {
                _hkf.CheckForLossOfReturnFlow(flowOutLpm, dTs);
            }
            Assert.IsFalse(_hkf.TotalLossOfReturnFlowRateBoolean);
        }
        [TestMethod]
        public void CheckForLossOfReturnFlowFlowZeroReturnTrue()
        {
            const int dTs = 2;
            const int durationS = 30;
            var tD = Vector.Range(0, durationS)
                            .Select(x => InitTimeD.AddSeconds(x))
                            .ToArray();

            _hkf.Initialize(MeasCovariance, XInit, PInit);
            var flowOutLpm = 2000;
            foreach (var d in tD)
            {
                _hkf.CheckForLossOfReturnFlow(flowOutLpm, dTs);
                if (d > tD[0].AddSeconds(6))
                {
                    flowOutLpm = 0;
                }
            }
            Assert.IsTrue(_hkf.TotalLossOfReturnFlowRateBoolean);
        }
        [TestMethod]
        public void ZeroReturnFlowTimerReturnFlowZeroAccumulateTime()
        {
            const int dTs = 2;
            const int durationS = 6;
            var tD = Vector.Range(1, durationS, dTs)
                            .Select(x => InitTimeD.AddSeconds(x))
                            .ToArray();

            _hkf.Initialize(MeasCovariance, XInit, PInit);
            const int flowOutLpm = 0;
            foreach (var _ in Enumerable.Range(0, tD.Length))
            {
                _hkf.ZeroReturnFlowTimer(flowOutLpm, dTs);
            }
            Assert.AreEqual(durationS, _hkf.TimeDurationWithZeroReturnFlowS);
        }
        [TestMethod]
        public void ZeroReturnFlowTimerReturnFlowGTZeroAccumulateTimeZero()
        {
            const int dTs = 2;
            const int durationS = 8;
            var tD = Vector.Range(1, durationS, dTs)
                            .Select(x => InitTimeD.AddSeconds(x))
                            .ToArray();

            _hkf.Initialize(MeasCovariance, XInit, PInit);
            var flowOutLpm = 0;
            foreach (var d in tD)
            {
                _hkf.ZeroReturnFlowTimer(flowOutLpm, dTs);
                if (d > tD[0])
                {
                    flowOutLpm = 3000;
                }
            }
            Assert.AreEqual(0, _hkf.TimeDurationWithZeroReturnFlowS);
        }
    }
}




