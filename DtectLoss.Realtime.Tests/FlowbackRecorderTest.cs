﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Accord.Math;
using Accord.Statistics;
using Accord.Statistics.Distributions.Univariate;
using Accord.Statistics.Models.Regression.Linear;
using DtectLoss.Common;
using DtectLossRealtime;
using DtectLossRealtime.Calculations;
using DtectLossRealtime.Enums;
using DtectLossRealtime.Helpers;
using DtectLossRealtime.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static System.Math;

namespace DtectLoss.Realtime.Tests
{

    [TestClass]
    public class FlowbackRecorderTest
    {
        private const int NFlowbacks = 16;
        private const double MinVolume = 1;
        private const double MinFlowRate = 50;
        private const double FlowTolerance = 400;
        private const double DtFlowRate = 3;
        private const double DtVolGl = 30;
        private const double TimeDuration = 900;
        private PitVol _tuningParams;
        private FlowBackRecorder _fbr;

        [TestInitialize()]
        public void Initialize()
        {
            _tuningParams.NTd = 4;
            var loggerMockFlowbackRecorder = LoggerUtils.LoggerMock<FlowBackRecorder>();
            var loggerMockLinReg = LoggerUtils.LoggerMock<LinReg>();
            var lr = new LinReg(loggerMockLinReg.Object);
            _fbr = new FlowBackRecorder(loggerMockFlowbackRecorder.Object, lr);
            _fbr.Initialize(NFlowbacks, MinVolume, MinFlowRate, FlowTolerance, DtFlowRate, DtVolGl, TimeDuration, _tuningParams);
        }


        [TestCleanup()]
        public void Cleanup()
        {
        }

        [TestMethod]
        public void intialize_correctInit_correctOutput()
        {
            Assert.AreEqual(NFlowbacks, _fbr.NFbs);
            Assert.AreEqual(MinVolume, _fbr.MinVol);
            Assert.AreEqual(MinFlowRate, _fbr.MinFlow);
            Assert.AreEqual(FlowTolerance, _fbr.FlowTol);
            Assert.AreEqual(300, _fbr.NRbFlow);
            Assert.AreEqual(30, _fbr.NRbVol);
            Assert.AreEqual(CirculationStates.NotCirculating, _fbr.StateAtPrevTime);
            Assert.AreEqual(false, _fbr.PumpsOn);
            Assert.AreEqual(0, _fbr.IdxStore);
        }

        [TestMethod]
        public void softReset_clearSomeData()
        {
            _fbr.IdxStore = 2;
            _fbr.IdxCon = 2;
            _fbr.IsInit = false;
            _fbr.SoftReset();
            Assert.AreEqual(0, _fbr.IdxStore);
            Assert.AreEqual(0, _fbr.IdxCon);
            Assert.AreEqual(true, _fbr.IsInit);
        }
        [TestMethod]
        public void addVolToRingbuffer_correctInput_correctOutput()
        {
            var volTimeD = DateTime.Parse("24/10/2016 12:00:00", new CultureInfo("nb-NO", false)).ToOADate();
            const int volM3 = -4;
            _fbr.AddVolToRingbuffer(volTimeD, volM3);
            var actual = _fbr.RbVol.GetLastTime();
            Assert.AreEqual(volTimeD, actual);
            var actual2 = _fbr.RbVol.GetLastValue();
            Assert.AreEqual(volM3, actual2);
        }
        [TestMethod]
        public void addExpVolToRingbuffer_correctInput_correctOutput()
        {
            var volTimeD = DateTime.Parse("24/10/2016 12:00:00", new CultureInfo("nb-NO", false)).ToOADate();
            const int volM3 = -4;
            _fbr.AddExpVolToRingbuffer(volTimeD, volM3);
            var actual = _fbr.RbExpVol.GetLastTime();
            Assert.AreEqual(volTimeD, actual);
            var actual2 = _fbr.RbExpVol.GetLastValue();
            Assert.AreEqual(volM3, actual2);
        }
        [TestMethod]
        public void addFlowToRingbuffer_correctInput_correctOutput()
        {
            var flowInTimeD = DateTime.Parse("24/10/2016 12:00:00", new CultureInfo("nb-NO", false)).ToOADate();
            const int flowInLpm = 2400;
            _fbr.AddFlowToRingbuffer(flowInTimeD, flowInLpm);
            var actual = _fbr.RbFlowIn.GetLastTime();
            Assert.AreEqual(flowInTimeD, actual);
            var actual2 = _fbr.RbFlowIn.GetLastValue();
            Assert.AreEqual(flowInLpm, actual2);
        }

        [TestMethod]
        public void updateCirculationState_correctInput_correctStateChange()
        {
            const int flowInLpm = 2000;
            const int deptBitM = 1000;
            const int depthHoleM = 2000;
            Assert.AreEqual(CirculationStates.NotCirculating, _fbr.StateAtPrevTime);
            Assert.AreEqual(CirculationStates.NotCirculating, _fbr.Cs.CurrentState);
            Assert.AreEqual(false, _fbr.PumpsOn);
            _fbr.UpdateCirculationState(flowInLpm, deptBitM, depthHoleM);
            Assert.AreEqual(CirculationStates.NotCirculating, _fbr.StateAtPrevTime);
            Assert.AreEqual(CirculationStates.Circulating, _fbr.Cs.CurrentState);
            Assert.AreEqual(true, _fbr.PumpsOn);
        }
        [TestMethod]
        public void updateTimeSpentAtState_noStateChange_update()
        {
            Assert.AreEqual(CirculationStates.NotCirculating, _fbr.StateAtPrevTime);
            Assert.AreEqual(CirculationStates.NotCirculating, _fbr.Cs.CurrentState);
            Assert.AreEqual(0, _fbr.TimeSpentAtState);

            const int timeStep = 1;
            _fbr.UpdateTimeSpentAtState(timeStep);
            Assert.AreEqual(timeStep, _fbr.TimeSpentAtState);
        }
        [TestMethod]
        public void updateTimeSpentAtState_stateChange_reset()
        {
            Assert.AreEqual(CirculationStates.NotCirculating, _fbr.StateAtPrevTime);
            Assert.AreEqual(CirculationStates.NotCirculating, _fbr.Cs.CurrentState);
            Assert.AreEqual(0, _fbr.TimeSpentAtState);

            const int flowInLpm = 2000;
            const int deptBitM = 1000;
            const int depthHoleM = 2000;
            _fbr.UpdateCirculationState(flowInLpm, deptBitM, depthHoleM);

            const int timeStep = 1;
            _fbr.UpdateTimeSpentAtState(timeStep);
            Assert.AreEqual(0, _fbr.TimeSpentAtState);
        }

        [TestMethod]
        public void updateFap_isInit_doNotUpdateFap()
        {
            const int flowInLpm = 2000;
            const int deptBitM = 1000;
            const int depthHoleM = 2000;

            var fap = new FlowlineAndPit();
            fap.Initialize(new double[] { 1, 0 }, 6, 0, 1, 0);

            var prevIdxStore = _fbr.IdxStore;
            _fbr.UpdateCirculationState(flowInLpm, deptBitM, depthHoleM);
            _fbr.UpdateFap(fap);
            Assert.AreEqual(CirculationStates.NotCirculating, _fbr.StateAtPrevTime);
            Assert.AreEqual(CirculationStates.Circulating, _fbr.Cs.CurrentState);
            Assert.AreEqual(true, _fbr.PumpsOn);
            Assert.AreEqual(prevIdxStore, _fbr.IdxStore, "The index counter for flowbacks should not be iterated when still in init state.");
        }
        [TestMethod]
        public void updateFap_pumpStart_identifyCorrectPumpOffIdx()
        {
            const double flowInLpm = 2000.0;
            const double deptBitM = 1000.0;
            const double depthHoleM = 2000.0;

            const double pumpOffTime = 300.0;
            const int expPumpOffIdx = 99; // Was 100 in Matlab
            const int expVolOffIdx = 8; //Was 9 in Matlab
            const double rampDownTime = 20.0;
            const double volTimeConstant = 120.0;
            var tVol = Vector.Range(0, TimeDuration, DtVolGl);
            var vol = tVol.Select(x => 6 * (1 - Exp(Min(0, -(x - pumpOffTime) / volTimeConstant)))).ToArray();

            var tFlow = Vector.Range(0, TimeDuration, DtFlowRate);
            var expVol = tFlow.Select(x => 6 * (1 - Exp(Min(0, -(x - pumpOffTime) / volTimeConstant)))).ToArray();

            var flow = tFlow.Select(x => Max(0, flowInLpm * (1 - Max(0, (x - (pumpOffTime - rampDownTime)) / rampDownTime)))).ToArray();
            var noisyFlow = flow;

            var timeStartD = DateTime.Parse("24/10/2016 12:00:00", new CultureInfo("nb-NO", false)).ToOADate();
            foreach (var ii in Enumerable.Range(0, _fbr.NRbFlow))
            {
                var flowTimeD = timeStartD.AddSeconds(tFlow[ii]);
                double flowLpm;
                if (tFlow[ii] >= pumpOffTime)
                {
                    flowLpm = flow[ii];
                }
                else
                {
                    flowLpm = flow[ii] + 20 * NormalDistribution.Random();
                    noisyFlow[ii] = flowLpm;
                }
                _fbr.AddFlowToRingbuffer(flowTimeD, flowLpm);
                _fbr.AddExpVolToRingbuffer(flowTimeD, expVol[ii]);
            }

            foreach (var ii in Enumerable.Range(0, _fbr.NRbVol))
            {
                var volTimeD = timeStartD.AddSeconds(tVol[ii]);

                var volM3 = vol[ii];
                _fbr.AddVolToRingbuffer(volTimeD, volM3);
            }

            var fap = new FlowlineAndPit();
            fap.Initialize(new double[] { 1, 0 }, 6, 0, 1, 0);

            Assert.AreEqual(CirculationStates.NotCirculating, _fbr.StateAtPrevTime);
            Assert.AreEqual(CirculationStates.NotCirculating, _fbr.Cs.CurrentState);
            Assert.AreEqual(false, _fbr.PumpsOn);
            Assert.AreEqual(false, _fbr.IsInit);
            _fbr.UpdateCirculationState(flowInLpm, deptBitM, depthHoleM);
            _fbr.UpdateFap(fap);
            var pumpOffIdx = _fbr.PumpOffIdxStore[_fbr.IdxStore - 1];
            var volOffIdx = _fbr.VolOffIdxStore[_fbr.IdxStore - 1];
            Assert.AreEqual(expPumpOffIdx, pumpOffIdx, "The pumpOffIdx should be found correctly.");
            Assert.AreEqual(expVolOffIdx, volOffIdx);
        }
        [TestMethod]
        public void updateFap_pumpStartNoisyFlowMeas_doNotUpdateK()
        {
            const double flowInLpm = 2000.0;
            const double deptBitM = 1000.0;
            const double depthHoleM = 2000.0;

            const double pumpOffTime = 300.0;
            const double rampDownTime = 20.0;
            const double volTimeConstant = 120.0;
            var tVol = Vector.Range(0, TimeDuration, DtVolGl);
            var vol = tVol.Select(x => 6 * (1 - Exp(Min(0, -(x - pumpOffTime) / volTimeConstant)))).ToArray();

            var tFlow = Vector.Range(0, TimeDuration, DtFlowRate);
            var expVol = tFlow.Select(x => 6 * (1 - Exp(Min(0, -(x - pumpOffTime) / volTimeConstant)))).ToArray();

            var flow = tFlow.Select(x => Max(0, flowInLpm * (1 - Max(0, (x - (pumpOffTime - rampDownTime)) / rampDownTime)))).ToArray();
            var noisyFlow = flow;
            var timeStartD = DateTime.Parse("24/10/2016 12:00:00", new CultureInfo("nb-NO", false)).ToOADate();
            foreach (var ii in Enumerable.Range(0, _fbr.NRbFlow))
            {
                var flowTimeD = timeStartD.AddSeconds(tFlow[ii]);
                double flowLpm;
                if (tFlow[ii] >= pumpOffTime)
                {
                    flowLpm = flow[ii];
                }
                else
                {
                    flowLpm = flow[ii] + 500 * NormalDistribution.Random();
                    noisyFlow[ii] = flowLpm;
                }
                _fbr.AddFlowToRingbuffer(flowTimeD, flowLpm);
                _fbr.AddExpVolToRingbuffer(flowTimeD, expVol[ii]);
            }

            foreach (var ii in Enumerable.Range(0, _fbr.NRbVol))
            {
                var volTimeD = timeStartD.AddSeconds(tVol[ii]);
                var volM3 = vol[ii];
                _fbr.AddVolToRingbuffer(volTimeD, volM3);
            }

            var fap = new FlowlineAndPit();
            fap.Initialize(new double[] { 1, 0 }, 6, 0, 1, 0);


            Assert.AreEqual(CirculationStates.NotCirculating, _fbr.StateAtPrevTime);
            Assert.AreEqual(CirculationStates.NotCirculating, _fbr.Cs.CurrentState);
            Assert.AreEqual(false, _fbr.PumpsOn);
            Assert.AreEqual(false, _fbr.IsInit);

            _fbr.UpdateCirculationState(flowInLpm, deptBitM, depthHoleM);
            _fbr.UpdateFap(fap);

            Assert.AreEqual(true, _fbr.PumpsOn);
            Assert.AreEqual(0, _fbr.KStore[_fbr.IdxStore - 1], "The kFlowback constant should not have been updated'");
        }
        [TestMethod]
        public void updateFap_pumpRampDown_doUpdateK()
        {
            const double flowInLpm = 2000.0;
            const double deptBitM = 1000.0;
            const double depthHoleM = 2000.0;

            const double pumpOffTime = 300.0;
            const double rampDownTime = 20.0;
            const double volTimeConstant = 120.0;
            var tVol = Vector.Range(0, TimeDuration, DtVolGl);
            var vol = tVol.Select(x => 6 * (1 - Exp(Min(0, -(x - pumpOffTime) / volTimeConstant)))).ToArray();

            var tFlow = Vector.Range(0, TimeDuration, DtFlowRate);
            var expVol = tFlow.Select(x => 6 * (1 - Exp(Min(0, -(x - pumpOffTime) / volTimeConstant)))).ToArray();

            var flow = tFlow.Select(x => Max(0, flowInLpm * (1 - Max(0, (x - (pumpOffTime - rampDownTime)) / rampDownTime)))).ToArray();
            var noisyFlow = flow;

            var timeStartD = DateTime.Parse("24/10/2016 12:00:00", new CultureInfo("nb-NO", false)).ToOADate();
            foreach (var ii in Enumerable.Range(0, _fbr.NRbFlow))
            {
                var flowTimeD = timeStartD.AddSeconds(tFlow[ii]);
                double flowLpm;
                if (tFlow[ii] >= pumpOffTime)
                {
                    flowLpm = flow[ii];
                }
                else
                {
                    flowLpm = flow[ii] + 20 * NormalDistribution.Random();
                    noisyFlow[ii] = flowLpm;
                }
                _fbr.AddFlowToRingbuffer(flowTimeD, flowLpm);
                _fbr.AddExpVolToRingbuffer(flowTimeD, expVol[ii]);
            }

            foreach (var ii in Enumerable.Range(0, _fbr.NRbVol))
            {
                var volTimeD = timeStartD.AddSeconds(tVol[ii]);
                var volM3 = vol[ii];
                _fbr.AddVolToRingbuffer(volTimeD, volM3);
            }

            var fap = new FlowlineAndPit();
            fap.Initialize(new double[] { 1, 0 }, 6, 0, 1, 0);

            Assert.AreEqual(CirculationStates.NotCirculating, _fbr.StateAtPrevTime);
            Assert.AreEqual(CirculationStates.NotCirculating, _fbr.Cs.CurrentState);
            Assert.AreEqual(false, _fbr.PumpsOn);
            Assert.AreEqual(false, _fbr.IsInit);

            _fbr.UpdateCirculationState(flowInLpm, deptBitM, depthHoleM);
            _fbr.UpdateFap(fap);

            Assert.AreEqual(true, _fbr.PumpsOn);
            Assert.IsTrue(_fbr.KStore[_fbr.IdxStore - 1] > 0, "The kFlowback should have been updated'");
        }

        [TestMethod]
        public void GetNTdLogicalIndexStoreIsLessThanNtd()
        {
            var idx = _fbr.GetNTdLogicalIndex();
            Assert.AreEqual(true, idx[0], "Should be true");
            Assert.AreEqual(false, idx[1], "Should be false");
            Assert.AreEqual(true, idx[_fbr.NFbs -1 ], "Should be true");
        }
        [TestMethod]
        public void GetNTdLogicalIndexStoreIsGreaterThanNtd()
        {
            _fbr.IdxStore = 4;
            var idx = _fbr.GetNTdLogicalIndex();
            Assert.AreEqual(true, idx[0], "Should be true");
            Assert.AreEqual(true, idx[3], "Should be true");
            Assert.AreEqual(false, idx[4], "Should be false");
        }

        [TestMethod]
        public void GetNTdLogicalIndexExcludingCurrent_StoreIdx_is_0()
        {
            var idx = _fbr.GetNTdLogicalIndexExcludingCurrent();
            Assert.IsFalse(idx[0], "Should be false");
            Assert.IsTrue(idx[_fbr.NFbs -1 ], "Should be true");
        }
        [TestMethod]
        public void GetNTdLogicalIndexExcludingCurrent_StoreIdx_is_greater_than_ntd()
        {
            _fbr.IdxStore = 4;
            var idx = _fbr.GetNTdLogicalIndexExcludingCurrent();
            Assert.IsTrue(idx[0], "Should be true");
            Assert.IsFalse(idx[_fbr.NFbs -1 ], "Should be false");
        }
        [TestMethod]
        public void GetNTdLogicalIndexExcludingCurrent_StoreIdx_is_inBetween()
        {
            _fbr.IdxStore = 2;
            var idx = _fbr.GetNTdLogicalIndexExcludingCurrent();
            Assert.IsTrue(idx[0], "Should be true");
            Assert.IsTrue(idx[1], "Should be true");
            Assert.IsTrue(idx[_fbr.NFbs -2 ], "Should be true");
            Assert.IsTrue(idx[_fbr.NFbs -1 ], "Should be true");
        }
        [TestMethod]
        public void storeFlowbackData_missingData_correctPadding()
        {
            const double numTol = 1e-5;
            const int flowInLpm = 2000;
            const double pumpOffTime = 120.0;
            var rampDownTime = 20.0;

            const double volTimeConstant = 60.0;
            var tEnd = Ceiling(TimeDuration / 3.0 * 2);
            var tVol = Vector.Range(0, tEnd + DtVolGl, DtVolGl);
            var vol = tVol.Select(x => 6 * (1 - Exp(Min(0, -(x - pumpOffTime) / volTimeConstant)))).ToArray();

            var tFlow = Vector.Range(0, tEnd, DtFlowRate);
            var expVol = tFlow.Select(x => 6 * (1 - Exp(Min(0, -(x - pumpOffTime) / volTimeConstant))) + 0.5).ToArray();

            var flow = tFlow.Select(x => Max(0, flowInLpm * (1 - Max(0, (x - (pumpOffTime - rampDownTime)) / rampDownTime)))).ToArray();
            var noisyFlow = flow;

            var timeStartD = DateTime.Parse("24/10/2016 12:00:00", new CultureInfo("nb-NO", false)).ToOADate();
            var flowTimeStoreDtmp = new List<double>();
            foreach (var ii in Enumerable.Range(0, tFlow.Length))
            {
                var flowTimeD = timeStartD.AddSeconds(tFlow[ii]);
                double flowLpm;
                if (tFlow[ii] >= pumpOffTime)
                {
                    flowLpm = flow[ii];
                }
                else
                {
                    flowLpm = flow[ii] + 20 * NormalDistribution.Random();
                    noisyFlow[ii] = flowLpm;
                }

                flowTimeStoreDtmp.Add(flowTimeD);
                _fbr.AddFlowToRingbuffer(flowTimeD, flowLpm);
                _fbr.AddExpVolToRingbuffer(flowTimeD, expVol[ii]);
            }
            var flowTimeStoreD = flowTimeStoreDtmp.ToArray();
            var volTimeStoreDtmp = new List<double>();
            foreach (var ii in Enumerable.Range(0, tVol.Length))
            {
                var volTimeD = timeStartD.AddSeconds(tVol[ii]);
                volTimeStoreDtmp.Add(volTimeD);
                var volM3 = vol[ii];
                _fbr.AddVolToRingbuffer(volTimeD, volM3);
            }

            var volTimeStoreD = volTimeStoreDtmp.ToArray();

            _fbr.StoreFlowbackData();
            var tFInAct = _fbr.TimeFlowInStore.GetRow(0);
            var fInAct = _fbr.FlowInStore.GetRow(0);
            var tVolAct = _fbr.TimeVolStore.GetRow(0);
            var volAct = _fbr.VolStore.GetRow(0);
            var tExpVolAct = _fbr.TimeExpVolStore.GetRow(0);
            var expVolAct = _fbr.ExpVolStore.GetRow(0);

            var nValidVolSamples = tVol.Length;
            var nValidFlowSamples = tFlow.Length;
            var nInvalidVolSamples = _fbr.NRbVol - nValidVolSamples;
            var nInvalidFlowSamples = _fbr.NRbFlow - nValidFlowSamples;
            var tMissingVolStart = volTimeStoreD[0].SubtractSeconds(DtVolGl * nInvalidVolSamples);
            var tMissingFlowStart = flowTimeStoreD[0].SubtractSeconds(DtFlowRate * nInvalidFlowSamples);
            var tFInExp = Vector
                .Range(tMissingFlowStart, flowTimeStoreD[0].AddSeconds(DtFlowRate), DtFlowRate.SecondsToOADate())
                .Concat(flowTimeStoreD.Skip(1)).ToArray();
            var fInExp = Vector.Create(nInvalidFlowSamples, noisyFlow[0]).Concat(noisyFlow).ToArray();
            var tVolExp = Vector
                .Range(tMissingVolStart, volTimeStoreD[0], DtVolGl.SecondsToOADate())
                .Concat(volTimeStoreD.Skip(1)).ToArray();
            var volExp = Vector.Create(nInvalidVolSamples, vol[0]).Concat(vol).ToArray();
            var tExpVolExp = Vector
                .Range(tMissingFlowStart, flowTimeStoreD[0].AddSeconds(DtFlowRate), DtFlowRate.SecondsToOADate())
                .Concat(flowTimeStoreD.Skip(1)).ToArray();
            var expVolExp = Vector.Create(nInvalidFlowSamples, expVol[0]).Concat(expVol).ToArray();

            CollectionAssert.AreEqual(tFInExp, tFInAct, new RelativeDoubleComparer(numTol));
            CollectionAssert.AreEqual(fInExp, fInAct, new RelativeDoubleComparer(numTol));
            CollectionAssert.AreEqual(tVolExp, tVolAct, new RelativeDoubleComparer(numTol));
            CollectionAssert.AreEqual(volExp, volAct, new RelativeDoubleComparer(numTol));
            CollectionAssert.AreEqual(tExpVolExp, tExpVolAct, new RelativeDoubleComparer(numTol));
            CollectionAssert.AreEqual(expVolExp, expVolAct, new RelativeDoubleComparer(numTol));
        }

        [TestMethod]
        public void updateInitState_inInit_isInitTrue()
        {
            _fbr.UpdateInitState();
            Assert.AreEqual(true, _fbr.IsInit);
        }
        [TestMethod]
        public void updateInitState_doneInit_counterUpdatedAndDoneInit()
        {
            foreach (var ii in Enumerable.Range(0, _fbr.NRbFlow))
            {
                _fbr.AddFlowToRingbuffer(ii, ii);
                _fbr.AddVolToRingbuffer(ii, ii);
                _fbr.AddExpVolToRingbuffer(ii, ii);
            }
            Assert.AreEqual(false, _fbr.IsInit);
        }

        [TestMethod]
        public void calcParams_noData_retEmptyParams()
        {
            _fbr.CalcParams(out double? kVol, out double? kFlow, out double? meanV0, 1.0);
            Assert.IsNull(kVol, "kVol != null");
            Assert.IsNull(kFlow, "kFlow != null");
            Assert.IsNull(meanV0, "meanV0 != null");
        }
        [TestMethod]
        public void calcParams_oneDataSet_retCorParams()
        {
            const double flowInLpm = 2000.0;
            const double deptBitM = 1000.0;
            const double depthHoleM = 2000.0;
            const double numTol = 1e-5;


            const double pumpOffTime = 297.0;
            const double timeDelay = 20.0;
            const double rampDownTime = 20.0;
            const double volTimeConstant = 60.0;
            const double v0Exp = 6.0;
            
            var tVol = Vector.Range(0, TimeDuration, DtVolGl);
            var vol = tVol.Select(x => v0Exp * (1 - Exp(Min(0, -(x - pumpOffTime - timeDelay) / volTimeConstant)))).ToArray();

            var tFlow = Vector.Range(0, TimeDuration, DtFlowRate);
            var expVol = tFlow.Select(x => v0Exp * (1 - Exp(Min(0, -(x - pumpOffTime - timeDelay) / volTimeConstant)))).ToArray();

            var flow = tFlow.Select(x => Max(0, flowInLpm * (1 - Max(0, (x - (pumpOffTime - rampDownTime)) / rampDownTime)))).ToArray();
            var noisyFlow = flow;

            var timeStartD = DateTime.Parse("24/10/2016 12:00:00", new CultureInfo("nb-NO", false)).ToOADate();
            foreach (var ii in Enumerable.Range(0, _fbr.NRbFlow))
            {
                var flowTimeD = timeStartD.AddSeconds(tFlow[ii]);
                double flowLpm;
                if (tFlow[ii] >= pumpOffTime)
                {
                    flowLpm = flow[ii];
                }
                else
                {
                    flowLpm = flow[ii] + 20 * NormalDistribution.Random();
                    noisyFlow[ii] = flowLpm;
                }
                _fbr.AddFlowToRingbuffer(flowTimeD, flowLpm);
                _fbr.AddExpVolToRingbuffer(flowTimeD, expVol[ii]);
            }

            foreach (var ii in Enumerable.Range(0, _fbr.NRbVol))
            {
                var volTimeD = timeStartD.AddSeconds(tVol[ii]);
                vol[ii] = vol[ii] + 0.1 * NormalDistribution.Random();
                var volM3 = vol[ii];
                _fbr.AddVolToRingbuffer(volTimeD, volM3);
            }

            var fap = new FlowlineAndPit();
            fap.Initialize(new double[] { 1, 0 }, 6, 0, 1, 0);


            Assert.AreEqual(CirculationStates.NotCirculating, _fbr.StateAtPrevTime);
            Assert.AreEqual(CirculationStates.NotCirculating, _fbr.Cs.CurrentState);
            Assert.AreEqual(false, _fbr.PumpsOn);
            Assert.AreEqual(false, _fbr.IsInit);

            _fbr.UpdateCirculationState(flowInLpm, deptBitM, depthHoleM);
            fap = _fbr.UpdateFap(fap);

            Assert.AreEqual(true, _fbr.PumpsOn);
            var k = fap.KFlowline;
            Assert.AreEqual(volTimeConstant, 1/k, numTol * volTimeConstant, "Time-constant should be reasonable.");
        }
        [TestMethod]
        public void calcParams_singlePrecision_noWarning()
        {
            const double flowInLpm = 2000.0;
            const double deptBitM = 1000.0;
            const double depthHoleM = 2000.0;


            const double pumpOffTime = 297.0;
            const double timeDelay = 20.0;
            const double rampDownTime = 20.0;
            const double volTimeConstant = 120.0;
            const double v0Exp = 6.0;
            
            var tVol = Vector.Range(0, TimeDuration, DtVolGl);
            var vol = tVol.Select(x => v0Exp * (1 - Exp(Min(0, -(x - pumpOffTime - timeDelay) / volTimeConstant)))).ToArray();

            var tFlow = Vector.Range(0, TimeDuration, DtFlowRate);
            var expVol = tFlow.Select(x => v0Exp * (1 - Exp(Min(0, -(x - pumpOffTime - timeDelay) / volTimeConstant)))).ToArray();

            var flow = tFlow.Select(x => Max(0, flowInLpm * (1 - Max(0, (x - (pumpOffTime - rampDownTime)) / rampDownTime)))).ToArray();
            var noisyFlow = flow;

            // mock object for pit and flowline volume
            var fap = new FlowlineAndPit();
            fap.Initialize(new double[] { 1, 0 }, 6, 0, 1, 0);

            var timeStartD = DateTime.Parse("24/10/2016 12:00:00", new CultureInfo("nb-NO", false)).ToOADate();
            foreach (var jj in Enumerable.Range(0, 1))
            {
                foreach (var ii in Enumerable.Range(0, _fbr.NRbFlow))
                {
                    var flowTimeD = timeStartD.AddSeconds(tFlow[ii]);
                    double flowLpm;
                    if (tFlow[ii] >= pumpOffTime)
                    {
                        flowLpm = flow[ii];
                    }
                    else
                    {
                        flowLpm = flow[ii] + 20 * NormalDistribution.Random();
                        noisyFlow[ii] = flowLpm;
                    }
                    _fbr.AddFlowToRingbuffer(flowTimeD, flowLpm);
                    _fbr.AddExpVolToRingbuffer(flowTimeD, expVol[ii]);
                }

                foreach (var ii in Enumerable.Range(0, _fbr.NRbVol))
                {
                    var volTimeD = timeStartD.AddSeconds(tVol[ii]);
                    vol[ii] = vol[ii] + 0.1 * NormalDistribution.Random();
                    var volM3 = vol[ii];
                    _fbr.AddVolToRingbuffer(volTimeD, volM3);
                }
                _fbr.UpdateCirculationState(0, deptBitM, depthHoleM);
                _fbr.UpdateFap(fap);
                // No asserts. In Matlab, verifyWarningFree is used. Assume this means no exceptions are thrown
            }
            
        }

        [TestMethod]
        public void getNTdLogicalIndex_currentIdxIsZero_returnCorrectIndicies()
        {
            Assert.AreEqual(0, _fbr.IdxStore);
            var idx = _fbr.GetNTdLogicalIndex();

            const int startLength = 0 + 1;
            var startValues = Vector.Create(startLength, true);
            var middleValueLength = _fbr.NFbs - _fbr.Parameters.NTd;
            var middleValues = Vector.Create(middleValueLength, false);
            var endValues = Vector.Create(_fbr.NFbs - middleValueLength - startLength, true);
            var expIdx = startValues.Concat(middleValues).Concat(endValues).ToArray();
            CollectionAssert.AreEqual(expIdx, idx);
        }
        [TestMethod]
        public void getNTdLogicalIndex_currentIdxIsNTd_returnCorrectIndicies()
        {
            _fbr.IdxStore = _fbr.Parameters.NTd;
            Assert.AreEqual(_fbr.Parameters.NTd, _fbr.IdxStore);
            var idx = _fbr.GetNTdLogicalIndex();
            var expIdx = Vector.Create(_fbr.Parameters.NTd, true).Concat(Vector.Create(_fbr.NFbs - _fbr.Parameters.NTd, false))
                .ToArray();
            CollectionAssert.AreEqual(expIdx, idx);
        }

        [TestMethod]
        public void getNTdLogicalIndexExcludingCurrent_curIdxIsZero_retCorIndicies()
        {
            Assert.AreEqual(0, _fbr.IdxStore);
            var idx = _fbr.GetNTdLogicalIndexExcludingCurrent();
            var expIdx = Enumerable.Repeat(false, _fbr.NFbs).Select((x, i) => i >=  _fbr.NFbs - _fbr.Parameters.NTd ).ToArray();
            CollectionAssert.AreEqual(expIdx, idx);
        }
        [TestMethod]
        public void getNTdLogicalIndexExcludingCurrent_curIdxNTd_retCorIndicies()
        {
            _fbr.IdxStore = _fbr.Parameters.NTd;
            Assert.AreEqual(_fbr.Parameters.NTd, _fbr.IdxStore);
            var idx = _fbr.GetNTdLogicalIndexExcludingCurrent();
            
            var expIdx = Enumerable.Repeat(false, _fbr.NFbs).Select((x, i) => i < _fbr.Parameters.NTd ).ToArray();
            CollectionAssert.AreEqual(expIdx, idx);
        }

        [TestMethod]
        public void getNTdLogicalIndexExcludingCurrent_gtNTd_retCorIndicies()
        {
            _fbr.IdxStore = _fbr.Parameters.NTd + 1;
            Assert.AreEqual(_fbr.Parameters.NTd + 1, _fbr.IdxStore);
            var idx = _fbr.GetNTdLogicalIndexExcludingCurrent();

            var expIdx = Enumerable.Repeat(false, _fbr.NFbs).Select((x, i) => i > 0 && i < _fbr.Parameters.NTd + 1 ).ToArray();
            CollectionAssert.AreEqual(expIdx, idx);
        }

        [TestMethod]
        public void getExpDrainbackData_noDataAtIdxVol_returnEmpty()
        {
            const int idxVol = 1;
            var (expVol, expVolTime) = _fbr.GetExpDrainbackData(idxVol);
            Assert.IsFalse(expVol.Any());
            Assert.IsFalse(expVolTime.Any());
        }
        [TestMethod]
        public void getExpDrainbackData_idxOutOfBounds_returnEmpty()
        {
            const int idxVol = 1000;
            var (expVol, expVolTime) = _fbr.GetExpDrainbackData(idxVol);
            Assert.IsFalse(expVol.Any());
            Assert.IsFalse(expVolTime.Any());
        }

        [TestMethod]
        public void analyzeRamp_noRampDown_returnEmpty()
        {
            var fi = Vector.Create(300, 0.0);
            fi.SetValue(30.0, fi.Length - 1);
            var (_, _, rIdx) = _fbr.AnalyzeRamp(new[]{0.0}, fi, "down");
            Assert.IsFalse(rIdx.HasValue);
        }

        [TestMethod]
        public void analyzeRamp_singleRampWoNoise_returnCorRIdxAndQDrill()
        {
            var n = (int)Ceiling(TimeDuration / DtFlowRate);
            const double pumpOffTime = 297.0;
            const double rampDownTime = 20.0;
            const double flowInLpm = 3000.0;
            
            var tFlow = Vector.Range(0, TimeDuration, DtFlowRate);
            var flow = tFlow.Select(x => Max(0, flowInLpm * (1 - Max(0, (x - (pumpOffTime - rampDownTime)) / rampDownTime)))).ToArray();
            var expPumpOffIdx = flow.Find(x => x < flowInLpm).First();
            var timeStartD = DateTime.Parse("24/10/2016 12:00:00", new CultureInfo("nb-NO", false)).ToOADate();
            var fiTimeD = Enumerable.Range(0, n).Select(x => timeStartD.AddSeconds(tFlow[x])).ToArray();
            var fiLpm = flow;
            var (_, qDrillLpm, rIdx) = _fbr.AnalyzeRamp(fiTimeD, fiLpm, "down");
            Assert.IsTrue(rIdx.Value >= expPumpOffIdx - 10, "rIdx.Value >= expPumpOffIdx - 10");
            Assert.IsTrue(rIdx.Value <= expPumpOffIdx, "rIdx.Value <= expPumpOffIdx");
            Assert.AreEqual(qDrillLpm.Value, flowInLpm);
        }

        [TestMethod]
        public void analyzeRamp_singleRampWithNoise_returnCorRIdx()
        {
            var n = (int)Ceiling(TimeDuration / DtFlowRate);
            const double pumpOffTime = 297.0;
            const double rampDownTime = 20.0;
            const double flowInLpm = 3000.0;
            var tFlow = Vector.Range(0, TimeDuration, DtFlowRate);
            var flow = tFlow.Select(x => Max(0, flowInLpm * (1 - Max(0, (x - (pumpOffTime - rampDownTime)) / rampDownTime)))).ToArray();
            var expPumpOffIdx = flow.Find(x => x < flowInLpm).First();

            var timeStartD = DateTime.Parse("24/10/2016 12:00:00", new CultureInfo("nb-NO", false)).ToOADate();
            var fiTimeD = new List<double>();
            var fiLpm = new List<double>();
            foreach (var ii in Enumerable.Range(0, n))
            {
                fiTimeD.Add(timeStartD.AddSeconds(tFlow[ii]));
                if (tFlow[ii] >= pumpOffTime)
                {
                    fiLpm.Add(flow[ii]);
                }
                else
                {
                    fiLpm.Add(flow[ii] + 20 * NormalDistribution.Random());
                }
            }
            var (_, qDrillLpm, rIdx) = _fbr.AnalyzeRamp(fiTimeD.ToArray(), fiLpm.ToArray(), "down");

            Assert.IsTrue(rIdx.Value >= expPumpOffIdx - 10, "rIdx.Value >= expPumpOffIdx - 10");
            Assert.IsTrue(rIdx.Value <= expPumpOffIdx, "rIdx.Value <= expPumpOffIdx");
            // allow +/ -50 lpm difference in qDrill from actual flow-rate.
            Assert.IsTrue(qDrillLpm.Value >= flowInLpm - 50, "qDrillLpm.Value >= flowInLpm - 50");
            Assert.IsTrue(qDrillLpm.Value <= flowInLpm + 50, "qDrillLpm.Value <= flowInLpm + 50");
        }
        [TestMethod]
        public void analyzeRamp_singleRampNotSS_retEmpty()
        {
            var n = (int)Ceiling(TimeDuration / DtFlowRate);
            const double pumpOffTime = 60.0;
            const double rampDownTime = 20.0;
            const double flowInLpm = 3000.0;
            
            var tFlow = Vector.Range(0, TimeDuration, DtFlowRate);
            var flow = tFlow.Select(x => Max(0, flowInLpm * (1 - Max(0, (x - (pumpOffTime - rampDownTime)) / rampDownTime)))).ToArray();

            var timeStartD = DateTime.Parse("24/10/2016 12:00:00", new CultureInfo("nb-NO", false)).ToOADate();
            var fiTimeD = new List<double>();
            var fiLpm = new List<double>();
            foreach (var ii in Enumerable.Range(0, n))
            {
                fiTimeD.Add(timeStartD.AddSeconds(tFlow[ii]));
                if (tFlow[ii] >= pumpOffTime)
                {
                    fiLpm.Add(flow[ii]);
                }
                else
                {
                    fiLpm.Add(flow[ii] + 20 * NormalDistribution.Random());
                }
            }
            var (_, qDrillLpm, rIdx) = _fbr.AnalyzeRamp(fiTimeD.ToArray(), fiLpm.ToArray(), "down");
            Assert.IsFalse(rIdx.HasValue);
            Assert.IsFalse(qDrillLpm.HasValue);
        }
        [TestMethod]
        public void analyzeRamp_stageDownWoNoise_returnCorRIdxAndQDrill()
        {
            var n = (int)Ceiling(TimeDuration / DtFlowRate);
            const double pumpOffTime = 297.0;
            const double rampDownSlope = 100.0;
            const double flowInLpm = 3000.0;
            
            var tFlow = Vector.Range(0, TimeDuration + DtFlowRate, DtFlowRate);

            var timeStartD = DateTime.Parse("24/10/2016 12:00:00", new CultureInfo("nb-NO", false)).ToOADate();
            var fiTimeD = new List<double>();
            var fiLpm = new List<double>();
            fiTimeD.Add(timeStartD.AddSeconds(tFlow[0]));
            var fiLpmPrev = flowInLpm;
            foreach (var ii in Enumerable.Range(1, n))
            {
                fiTimeD.Add(timeStartD.AddSeconds(tFlow[ii]));
                var dts = tFlow.Skip(ii - 1).Take(2).ToArray().Diff().First();
                if (tFlow[ii] >= pumpOffTime && fiLpmPrev > 1500 || tFlow[ii] >= pumpOffTime + 60)
                {
                    fiLpmPrev = Max(0, fiLpmPrev - rampDownSlope * dts);
                    fiLpm.Add(fiLpmPrev);
                }
                else
                {
                    fiLpm.Add(fiLpmPrev);
                }
            }
            var expPumpOffIdx = fiLpm.ToArray().Find(x => x < flowInLpm).First();
            var (_, qDrillLpm, rIdx) = _fbr.AnalyzeRamp(fiTimeD.ToArray(), fiLpm.ToArray(), "down");

            Assert.IsTrue(rIdx.Value >= expPumpOffIdx - 10, "rIdx.Value >= expPumpOffIdx - 10");
            Assert.IsTrue(rIdx.Value <= expPumpOffIdx, "rIdx.Value <= expPumpOffIdx");
            Assert.AreEqual(flowInLpm, qDrillLpm.Value);
        }
        [TestMethod]
        public void analyzeRamp_stageDownWithNoise_returnCorRIdx()
        {
            var n = (int)Ceiling(TimeDuration / DtFlowRate);
            const double pumpOffTime = 297.0;
            const double rampDownSlope = 100.0;
            const double flowInLpm = 3000.0;
            
            var tFlow = Vector.Range(0, TimeDuration + DtFlowRate, DtFlowRate);

            var timeStartD = DateTime.Parse("24/10/2016 12:00:00", new CultureInfo("nb-NO", false)).ToOADate();
            var fiTimeD = new List<double>();
            var fiLpm = new List<double>();
            fiTimeD.Add(timeStartD.AddSeconds(tFlow[0]));
            var fiLpmPrev = flowInLpm;
            foreach (var ii in Enumerable.Range(1, n))
            {
                fiTimeD.Add(timeStartD.AddSeconds(tFlow[ii]));
                var dts = tFlow.Skip(ii - 1).Take(2).ToArray().Diff().First();
                if (tFlow[ii] >= pumpOffTime && fiLpmPrev > 1500 || tFlow[ii] >= pumpOffTime + 60)
                {
                    fiLpmPrev = Max(0, fiLpmPrev - rampDownSlope * dts);
                    fiLpm.Add(fiLpmPrev);
                }
                else
                {
                    fiLpm.Add(fiLpmPrev);
                }
            }
            // Add noise to flow meas
            var fiLpmNoised = fiLpm.Select(x => x > 0 ? x + 20 * NormalDistribution.Random() : x).ToArray();
            var expPumpOffIdx = fiLpmNoised.Find(x => x < flowInLpm - 100).First();
            var (_, qDrillLpm, rIdx) = _fbr.AnalyzeRamp(fiTimeD.ToArray(), fiLpmNoised, "down");

            Assert.IsTrue(rIdx.Value >= expPumpOffIdx - 10, "rIdx.Value >= expPumpOffIdx - 10");
            Assert.IsTrue(rIdx.Value <= expPumpOffIdx, "rIdx.Value <= expPumpOffIdx");
            Assert.IsTrue(qDrillLpm.Value >= flowInLpm - 50, "qDrillLpm.Value >= flowInLpm - 50");
            Assert.IsTrue(qDrillLpm.Value <= flowInLpm + 50, "qDrillLpm.Value <= flowInLpm + 50");
        }
        [TestMethod]
        public void analyzeRamp_longStageDownWithNoise_returnCorRIdx()
        {
            var n = (int)Ceiling(TimeDuration / DtFlowRate);
            const double pumpOffTime = 50.0;
            const double rampDownSlope = 100.0;
            const double flowInLpm = 3000.0;
            const double stageFlowLpm = 1500.0;
            var tFlow = Vector.Range(0, TimeDuration + DtFlowRate, DtFlowRate);
            var timeStartD = DateTime.Parse("24/10/2016 12:00:00", new CultureInfo("nb-NO", false)).ToOADate();
            var fiTimeD = new List<double>();
            var fiLpm = new List<double>();
            fiTimeD.Add(timeStartD.AddSeconds(tFlow[0]));
            var fiLpmPrev = flowInLpm;
            foreach (var ii in Enumerable.Range(1, n))
            {
                fiTimeD.Add(timeStartD.AddSeconds(tFlow[ii]));
                var dts = tFlow.Skip(ii - 1).Take(2).ToArray().Diff().First();
                if (tFlow[ii] >= pumpOffTime && fiLpmPrev > stageFlowLpm || tFlow[ii] >= pumpOffTime + 360)
                {
                    fiLpmPrev = Max(0, fiLpmPrev - rampDownSlope * dts);
                    fiLpm.Add(fiLpmPrev);
                }
                else
                {
                    fiLpm.Add(fiLpmPrev);
                }
            }
            // Add noise to flow meas
            var fiLpmNoised = fiLpm.Select(x => x > 0 ? x + 20 * NormalDistribution.Random() : x).ToArray();
            var expPumpOffIdx = fiLpmNoised.Find(x => x < stageFlowLpm - 100).First() - 1;
            var (_, qDrillLpm, rIdx) = _fbr.AnalyzeRamp(fiTimeD.ToArray(), fiLpmNoised, "down");

            Assert.IsTrue(rIdx.Value >= expPumpOffIdx - 10, "rIdx.Value >= expPumpOffIdx - 10");
            Assert.IsTrue(rIdx.Value <= expPumpOffIdx, "rIdx.Value <= expPumpOffIdx");
            Assert.IsTrue(qDrillLpm.Value >= stageFlowLpm - 50, "qDrillLpm.Value >= flowInLpm - 50");
            Assert.IsTrue(qDrillLpm.Value <= stageFlowLpm + 50, "qDrillLpm.Value <= flowInLpm + 50");
        }
        [TestMethod]
        public void analyzeRamp_smallHumpWithNoise_returnEmpty()
        {
            var n = (int)Ceiling(TimeDuration / DtFlowRate);
            const double pumpOnTime = 700.0;
            const int pumpOnDuration = 100;
            const double rampSlope = 100.0;
            const double flowInLpm = 500.0;
            var tFlow = Vector.Range(0, TimeDuration + DtFlowRate, DtFlowRate);

            // add data to ring buffers
            var timeStartD = DateTime.Parse("24/10/2016 12:00:00", new CultureInfo("nb-NO", false)).ToOADate();
            var fiTimeD = new List<double>();
            var fiLpm = new List<double>();
            fiTimeD.Add(timeStartD.AddSeconds(tFlow[0]));
            var fiLpmPrev = 0.0;
            foreach (var ii in Enumerable.Range(1, n))
            {
                fiTimeD.Add(timeStartD.AddSeconds(tFlow[ii]));
                var dts = tFlow.Skip(ii - 1).Take(2).ToArray().Diff().First();
                if (tFlow[ii] >= pumpOnTime && tFlow[ii] < pumpOnTime + pumpOnDuration && fiLpmPrev < flowInLpm)
                {
                    fiLpmPrev = Min(flowInLpm, fiLpmPrev + rampSlope * dts);
                    fiLpm.Add(fiLpmPrev);
                }
                else if (tFlow[ii] >= pumpOnTime + pumpOnDuration)
                {
                    fiLpmPrev = Max(0, fiLpmPrev - rampSlope * dts);
                    fiLpm.Add(fiLpmPrev);
                }
                else
                {
                    fiLpm.Add(fiLpmPrev);
                }
            }
            // Add noise to flow meas
            var fiLpmNoised = fiLpm.Select(x => x > 0 ? x + 20 * NormalDistribution.Random() : x).ToArray();
            var (_, qDrillLpm, rIdx) = _fbr.AnalyzeRamp(fiTimeD.ToArray(), fiLpmNoised, "down");

            Assert.IsFalse(rIdx.HasValue, "rIdx.HasValue");
            Assert.IsFalse(qDrillLpm.HasValue, "qDrillLpm.HasValue");
        }

        [TestMethod]
        public void getFlowbackStatistics_corInput_corOutput()
        {
            const double qDrillLpm = 3000.0;
            const double volFlowback = 3.0;
            const int nFbs = 6;
            foreach (var ii in Enumerable.Range(0, nFbs))
            {
                _fbr.QStore[ii] = qDrillLpm;
                _fbr.V0Store[ii] = volFlowback;
            }

            _fbr.IdxStore = nFbs;
            var (meanV0, stdV0) = _fbr.GetFlowbackStatistics(qDrillLpm);
            Assert.AreEqual(volFlowback, meanV0.Value);
            Assert.AreEqual(0, stdV0.Value);
        }
        [TestMethod]
        public void getFlowbackStatistics_noMatchingFlowRate_returnEmpty()
        {
            const double qDrillLpm = 3000.0;
            const double volFlowback = 3.0;
            const int nFbs = 6;
            foreach (var ii in Enumerable.Range(0, nFbs))
            {
                _fbr.QStore[ii] = qDrillLpm + 400;
                _fbr.V0Store[ii] = volFlowback;
            }

            _fbr.IdxStore = nFbs;
            var (meanV0, stdV0) = _fbr.GetFlowbackStatistics(qDrillLpm);
            Assert.IsFalse(meanV0.HasValue, "meanV0.HasValue");
            Assert.IsFalse(stdV0.HasValue, "stdV0.HasValue");
        }
        [TestMethod]
        public void getFlowbackStatistics_oneOutOfBoundFlow_retCorVal()
        {
            const double qDrillLpm = 3000.0;
            const int nFbs = 6;

            var count = 2;
            foreach (var ii in Enumerable.Range(0, nFbs))
            {
                _fbr.QStore[ii] = qDrillLpm;
                _fbr.V0Store[ii] = count;
                count += 1;
            }

            _fbr.QStore[nFbs - 2] = qDrillLpm - 2 * _fbr.QWinLpm;
            _fbr.IdxStore = nFbs;
            var (meanV0, stdV0) = _fbr.GetFlowbackStatistics(qDrillLpm);

            var expMeanV0 = new double[] {4, 5, 7 }.Average();
            var expStdV0 = new double[] {4, 5, 7 }.StandardDeviation();
            Assert.AreEqual(expMeanV0, meanV0.Value);
            Assert.AreEqual(expStdV0, stdV0.Value);
        }

        [TestMethod]
        public void compareFlowbackVolumeWithPrev_noPrevFb_returnTrue()
        {
            var qDrillLpm = 3000.0;
            var newV0 = 6;
            _fbr.IdxStore = 0;

            var isVolumeWithinTolerance = _fbr.CompareFlowbackVolumeWithPrev(newV0, qDrillLpm);
            Assert.IsTrue(isVolumeWithinTolerance, "isVolumeWithinTolerance");
        }

        [TestMethod]
        public void pickDelayedSamples_unevenSampling_interpolate()
        {

            const int flowLpm = 2000;
            var timeStartD = DateTime.Parse("31/03/2017 14:50", new CultureInfo("nb-NO", false)).ToOADate();

            var dtFlow = Vector.Ones(_fbr.NRbFlow);
            dtFlow = dtFlow.Select((x, index) => index == 0 || index == 2 || index == _fbr.NRbFlow - 1 ? 2 : x).ToArray();
            var tFlow = dtFlow.CumulativeSum();

            var flowTimeD = 0.0;
            foreach (var ii in Enumerable.Range(0, _fbr.NRbFlow))
            {
                flowTimeD = timeStartD.AddSeconds(tFlow[ii]);
                _fbr.AddFlowToRingbuffer(flowTimeD, flowLpm);
            }

            var nDelays = _fbr.DelayArraySamples.Length;
            var expFlowTimeD = Vector.Zeros(nDelays + 1);

            foreach (var jj in Enumerable.Range(0, nDelays))
            {
                expFlowTimeD[jj] = flowTimeD.SubtractSeconds(_fbr.DelayArraySamples[jj] * _fbr.TimeSamplingS);
            }

            expFlowTimeD[^1] = flowTimeD;
            var (actTimeD, _) = _fbr.PickDelayedSamples();
            var actTimeDArray = actTimeD.ToArray();

            var actTimeDates = actTimeD.Select(DateTime.FromOADate).ToArray();
            var expFlowTimeDDates = expFlowTimeD.Select(DateTime.FromOADate).ToArray();
            CollectionAssert.AreEqual(expFlowTimeD, actTimeDArray);
            CollectionAssert.AreEqual(expFlowTimeDDates, actTimeDates);
        }

        [TestMethod]
        public void VerifyExpVolCurrentConClippedAndScaled()
        {
            var volConClipped = new[] {0.1, 2.0, 3.0};
            var expVolCurrentConClipped = new[] {1.0, 2.0, 3.0};
            var result = FlowBackRecorder.ScaleExpVolCurve(volConClipped, expVolCurrentConClipped);
            CollectionAssert.AreEqual(new [] {0.25, 1.7, 3.15}, result, new ToleranceDoubleComparer(1e-10));
        }

        [TestMethod]
        public void VerifyLeastSquareCalculation()
        {
            var expSlope = -0.0018;
            var expVolume = 8.7851;
            // vector storing the mean flow before pumps are stopped [lpm]
            var qStore = new[] { 513.9, 514.9, 2327.9, 2328.5, 2413.1, 2003.4, 2004.3, 1587.9, 1818.4, 2003.1, 2010.7, 2191.6, 2379.8, 2373.0, 2375.4, 2412.8, 2003.6, 2265.2 };
            // vector storing the recorded flowback after pumps are stopped [m3]
            var V0Store = new[] { 8.6614, 8.6614, 6.1254, 2.1830, 6.1353, 5.8042, 5.8077, 3.2843, 4.1804, 4.9082, 4.8493, 4.8341, 4.4742, 5.3761, 4.2060, 4.7376, 3.4607, 5.4029 };     
            OrdinaryLeastSquares ordinaryLeastSquares = new OrdinaryLeastSquares();
            SimpleLinearRegression slr = ordinaryLeastSquares.Learn(qStore, V0Store);
            var slope = slr.Slope;
            var intercept = slr.Intercept;
            Assert.AreEqual(expSlope, slope, 0.0001);
            Assert.AreEqual(expVolume, intercept, 0.0001);

        }
    }
}
