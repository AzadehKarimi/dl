﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using DtectLoss.Common;
using DtectLossRealtime;
using DtectLossRealtime.Enums;
using DtectLossRealtime.Helpers;
using Microsoft.Extensions.Logging;
using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DtectLoss.Realtime.Tests
{
    [TestClass]
    public class ProcessTest
    {
        private Process _process;
        private Mock<ILogger<Process>> _loggerMock;
        private Mock<ILogger<StateMachine>> _stateMachineLoggerMock;
        private Dictionary<string, string> _rawDataUnitsList;
        private Dictionary<string, string> _expDefaultDataUnits;
        private Dictionary<string, double> _expDefaultUnitsFactor;
        private Mock<ILogger<StandpipePressureDelay>> _loggerMockStandpipePressureDelay;

        [TestInitialize()]
        public void Initialize()
        {
            var nullLogger = new Mock<ILogger<DefaultUnitsMappingReader>>().Object;
            var mappingReader = new DefaultUnitsMappingReader(nullLogger);
            _loggerMock = LoggerUtils.LoggerMock<Process>();
            _loggerMockStandpipePressureDelay = LoggerUtils.LoggerMock<StandpipePressureDelay>();
            _stateMachineLoggerMock = LoggerUtils.LoggerMock<StateMachine>();
            var standpipePressureDelay = new StandpipePressureDelay(_loggerMockStandpipePressureDelay.Object);
            var loggerMockPaddleConverter = LoggerUtils.LoggerMock<PaddleConverter>();
            var paddleConverter = new PaddleConverter(loggerMockPaddleConverter.Object);
            _process = new Process(_loggerMock.Object, mappingReader, standpipePressureDelay, paddleConverter);

            _rawDataUnitsList = new []
            {
                "flowIn", "m3/s", "depthBit", "m","presSP", "kPa", "weightOnBit", "kkgf", "flowOutPercent", "Euc", "torque", "kN.m", "velRop", "m/s", "tempBHA", "K"
            }
                .CreateDictionaryFromEvenOddList();

            _expDefaultDataUnits = new []
            {
                "flowIn", "lpm", "depthBit", "m", "presSP", "bar", "weightOnBit", "kN", "flowOutPercent", "%", "torque", "kNm", "velRop", "mph", "tempBHA", "degC"
            }.CreateDictionaryFromEvenOddList();

            (string k, double v)[] tmpExpDefaultUnitsFactor =
            {
                ("flowIn", 60000.0),
                ("depthBit", 1.0),
                ("presSP", 0.01), 
                ("weightOnBit", 9.08665), 
                ("flowOutPercent", 100.0),
                ("torque", 1.0), 
                ("velRop", 3600.0), 
                ("tempBHA", 1.0)
            };
            _expDefaultUnitsFactor = tmpExpDefaultUnitsFactor.ToDictionary(x => x.k, x => x.v);
        }

        [TestCleanup()]
        public void Cleanup()
        {
        }


        [TestMethod]
        public void InitInformationOk()
        {
            _process.Initialize(234.2);
            _loggerMock.VerifyLog(LogLevel.Information, "Process initialized.");
        }

        [TestMethod]
        public void specifyUnits_correctInit_correctOutput()
        {
            _process.SpecifyUnits(_rawDataUnitsList);
            var ru = _process.RawDataUnits;
            foreach (var (key, _) in ru)
            {
                Assert.AreEqual(_expDefaultDataUnits[key], _process.DefaultDataUnits[key]);
                Assert.AreEqual(_expDefaultUnitsFactor[key], _process.DefaultDataUnitsFactors[key]);
            }
        }

        [TestMethod]
        public void specifyUnits_varNotInUnitMap_issueWarning()
        {
            var rawDataWithMissingVar = new []
            {
                "flowIn", "m3/s", "depthBit", "m", "presSP", "kPa", "weightOnBit", "kkgf", "flowOutPercent", "Euc", "torque", "kN.m", "velRop", "m/s", "missingVar", "unitless"
            }.CreateDictionaryFromEvenOddList();
            _process.SpecifyUnits(rawDataWithMissingVar);
            const string variableName = "missingVar";
            _loggerMock.VerifyLog(LogLevel.Warning, $"PROCESS_OBJ:variableNotInUnitMap: The variable {variableName} has no default unit. The factor 1 will be used");
        }
        [TestMethod]
        public void specifyUnits_varWoConvFactor_issueWarning()
        {
            var rawDataWithWoConvFactor = new []
            {
                "flowIn", "m3/s", "depthBit", "m", "presSP", "kPa", "weightOnBit", "kkgf", "flowOutPercent", "Euc",
                "torque", "kN.m", "velRop", "m/s", "testUnitWoConv", "unitless"
            }.CreateDictionaryFromEvenOddList();
            _process.SpecifyUnits(rawDataWithWoConvFactor);
            const string variableName = "testUnitWoConv";
            const string unitName = "unitless";
            _loggerMock.VerifyLog(LogLevel.Warning, $"PROCESS_OBJ:noConversionInUnitMap: The variable {variableName} is missing a conversion factor for {unitName}. The factor 1 will be used");
        }
        [TestMethod]
        public void specifyUnits_varMissingUnitInMap_issueWarning()
        {
            var rawDataWithMissingUnitInMap = new []
            {
                "flowIn", "m3/s", "depthBit", "m",
                "presSP", "psig", "weightOnBit", "kkgf", "flowOutPercent", "Euc",
                "torque", "kN.m", "velRop", "m/s"
            }.CreateDictionaryFromEvenOddList();
            _process.SpecifyUnits(rawDataWithMissingUnitInMap);
            const string variableName = "presSP";
            const string unitName = "psig";
            _loggerMock.VerifyLog(LogLevel.Warning, $"PROCESS_OBJ:noConversionInUnitMap: The variable {variableName} is missing a conversion factor for {unitName}. The factor 1 will be used");
        }

        [TestMethod]
        public void preProcess_emptyInput_emptyOutput()
        {
            var rwdu = new []
            {
                "flowIn", "m3/s",
                "flowOut", "m3/s",
                "flowPercent", "%",
                "rpm", "rpm",
                "velRop", "m/s",
                "presSP", "kPa",
                "depthHole", "m",
                "depthBit", "m",
                "heightHook", "m",
                "volGainLoss", "m3",
                "tempBHA", "K",
                "volTripTank", "m3",
            }.CreateDictionaryFromEvenOddList();
            _process.UpdateTagList(rwdu);
            var rawDataNonStd = new Dictionary<string,DataVariable>();
            var result = _process.PreProcess(rawDataNonStd);
            Assert.IsFalse(result.RawDataStdUnit.Any());
            Assert.AreEqual(DtectLossRealtimeErrorCodes.MissingAllValues, result.ErrorCode);
        }
        [TestMethod]
        public void preProcess_missingRequiredTag_returnErrorCode()
        {
            var rwdu = new []
            {
                //"flowIn", "m3/s", // crucial tag is missing
                "flowOut", "m3/s",
                "flowPercent", "%",
                "rpm", "rpm",
                "velRop", "m/s",
                "presSP", "kPa",
                "depthHole", "m",
                "depthBit", "m",
                "volGainLoss", "m3",
                "volTripTank", "m3",
            }.CreateDictionaryFromEvenOddList();
            _process.UpdateTagList(rwdu);

            var rawDataNonStd = new Dictionary<string,DataVariable>();
            rawDataNonStd.Add("flowOut", new DataVariable(DateTime.Now.ToOADate(), 0.15));
            rawDataNonStd.Add("flowOutPercent", new DataVariable(DateTime.Now.ToOADate(), 15));
            rawDataNonStd.Add("rpm", new DataVariable(DateTime.Now.ToOADate(), 200));
            rawDataNonStd.Add("velRop", new DataVariable(DateTime.Now.ToOADate(), 0.01));
            rawDataNonStd.Add("presSP", new DataVariable(DateTime.Now.ToOADate(), 5000));
            rawDataNonStd.Add("depthHole", new DataVariable(DateTime.Now.ToOADate(), 3000));
            rawDataNonStd.Add("depthBit", new DataVariable(DateTime.Now.ToOADate(), 2999));
            rawDataNonStd.Add("volGainLoss", new DataVariable(DateTime.Now.ToOADate(), 3));
            rawDataNonStd.Add("volTripTank", new DataVariable(DateTime.Now.ToOADate(), 2));
            var result = _process.PreProcess(rawDataNonStd);
            Assert.AreEqual(DtectLossRealtimeErrorCodes.NoValueFlowIn, result.ErrorCode);
        }
        [TestMethod]
        public void preProcess_missingRequiredUnit_returnErrorCode()
        {
            var rwdu = new []
            {
                "flowIn", "wrongUnit", // tag has unknown unit
                "flowOut", "m3/s",
                "flowPercent", "%",
                "rpm", "rpm",
                "velRop", "m/s",
                "presSP", "kPa",
                "depthHole", "m",
                "depthBit", "m",
                "volGainLoss", "m3",
                "volTripTank", "m3",
            }.CreateDictionaryFromEvenOddList();
            _process.UpdateTagList(rwdu);

            var rawDataNonStd = new Dictionary<string,DataVariable>();
            rawDataNonStd.Add("flowOut", new DataVariable(DateTime.Now.ToOADate(), 0.15));
            rawDataNonStd.Add("flowOutPercent", new DataVariable(DateTime.Now.ToOADate(), 15));
            rawDataNonStd.Add("rpm", new DataVariable(DateTime.Now.ToOADate(), 200));
            rawDataNonStd.Add("velRop", new DataVariable(DateTime.Now.ToOADate(), 0.01));
            rawDataNonStd.Add("presSP", new DataVariable(DateTime.Now.ToOADate(), 5000));
            rawDataNonStd.Add("depthHole", new DataVariable(DateTime.Now.ToOADate(), 3000));
            rawDataNonStd.Add("depthBit", new DataVariable(DateTime.Now.ToOADate(), 2999));
            rawDataNonStd.Add("volGainLoss", new DataVariable(DateTime.Now.ToOADate(), 3));
            rawDataNonStd.Add("volTripTank", new DataVariable(DateTime.Now.ToOADate(), 2));
            var result = _process.PreProcess(rawDataNonStd);
            Assert.AreEqual(DtectLossRealtimeErrorCodes.NoUnitFlowIn, result.ErrorCode);
        }
        [TestMethod]
        public void preProcess_noVelRop_missingValCode()
        {
            var rwdu = new []
            {
                "flowIn", "lpm", 
                "depthHole", "m",
                "depthBit", "m",
                "volTripTank", "m3",
            }.CreateDictionaryFromEvenOddList();
            _process.UpdateTagList(rwdu);

            var rawDataNonStd = new Dictionary<string,DataVariable>();
            rawDataNonStd.Add("flowIn", new DataVariable(DateTime.Now.ToOADate(), 3200));
            rawDataNonStd.Add("flowOut", new DataVariable(DateTime.Now.ToOADate(), 0.15));
            rawDataNonStd.Add("depthHole", new DataVariable(DateTime.Now.ToOADate(), 3000));
            rawDataNonStd.Add("depthBit", new DataVariable(DateTime.Now.ToOADate(), 2999));
            rawDataNonStd.Add("volTripTank", new DataVariable(DateTime.Now.ToOADate(), 2));
            var result = _process.PreProcess(rawDataNonStd);
            Assert.AreEqual(CommonConstants.MissingValueReplacement, result.RawDataStdUnit[V.VelRopMph]);
        }
        [TestMethod]
        public void preProcess_noFlowOut_addTagAndMissingValCode()
        {
            var rwdu = new []
            {
                "flowIn", "lpm", 
                "depthHole", "m",
                "depthBit", "m",
                "volTripTank", "m3",
            }.CreateDictionaryFromEvenOddList();
            _process.UpdateTagList(rwdu);

            var rawDataNonStd = new Dictionary<string,DataVariable>();
            rawDataNonStd.Add("flowIn", new DataVariable(DateTime.Now.ToOADate(), 3200));
            rawDataNonStd.Add("depthHole", new DataVariable(DateTime.Now.ToOADate(), 3000));
            rawDataNonStd.Add("depthBit", new DataVariable(DateTime.Now.ToOADate(), 2999));
            rawDataNonStd.Add("volTripTank", new DataVariable(DateTime.Now.ToOADate(), 2));
            var result = _process.PreProcess(rawDataNonStd);
            // append flowOut_lpm to rdsu, but with error code.
            Assert.AreEqual(CommonConstants.MissingValueReplacement, result.RawDataStdUnit[V.FlowOutLpm]);
        }
        [TestMethod]
        /// when only the required tags are defined the preProcess
        /// function shall still return a rawDataStdUnit struct with the
        /// tags defined in required and optional tags.But the optional
        /// tags should have the value -0.99925.
        public void preProcess_onlyRequiredTags_returnCompleteNameSpace()
        {
            var rwdu = new []
            {
                "flowIn", "lpm", 
                "depthHole", "m",
                "depthBit", "m",
                "volTripTank", "m3",
            }.CreateDictionaryFromEvenOddList();
            _process.UpdateTagList(rwdu);

            var rawDataNonStd = new Dictionary<string,DataVariable>();
            rawDataNonStd.Add("flowIn", new DataVariable(DateTime.Now.ToOADate(), 3200));
            rawDataNonStd.Add("depthHole", new DataVariable(DateTime.Now.ToOADate(), 3000));
            rawDataNonStd.Add("depthBit", new DataVariable(DateTime.Now.ToOADate(), 2999));
            rawDataNonStd.Add("volTripTank", new DataVariable(DateTime.Now.ToOADate(), 2));

            (string k, double v)[] tmpExpRdsu =
            {
                ("flowIn_lpm", 3200.0),
                ("depthBit_m", 2999.0),
                ("depthHole_m", 3000.0),
                ("volTripTank_m3", 2.0),
                ("flowOut_lpm", CommonConstants.MissingValueReplacement),
                ("flowOut1_lpm", CommonConstants.MissingValueReplacement),
                ("presSP_bar", CommonConstants.MissingValueReplacement),
                ("presSP1_bar", CommonConstants.MissingValueReplacement),
                ("volGainLoss_m3", CommonConstants.MissingValueReplacement),
                ("volPitDrill_m3", CommonConstants.MissingValueReplacement),
                ("volTripTank2_m3", CommonConstants.MissingValueReplacement),
                ("heightHook_m", CommonConstants.MissingValueReplacement),
                ("rpm", CommonConstants.MissingValueReplacement),
                ("torque_kNm", CommonConstants.MissingValueReplacement),
                ("weightOnBit_kN", CommonConstants.MissingValueReplacement),
                ("rop_mph", CommonConstants.MissingValueReplacement),
                ("velRop_mph", CommonConstants.MissingValueReplacement),
                ("velBlock_mph", CommonConstants.MissingValueReplacement),
                //("velBit_mph", CommonConstants.MissingValueReplacement), //was this value in Matlab test
                ("velBit_mph", 0.0),
            };
            var expRdsu = tmpExpRdsu.ToDictionary(x => x.k, x => x.v);
            var result = _process.PreProcess(rawDataNonStd);

            var fNExpRdsu = expRdsu.Select(x => x.Key).ToList();
            var fNRdsu = result.RawDataStdUnit.Select(x => x.Key).ToList();

            CollectionAssert.AreEqual(fNExpRdsu, fNRdsu);
            var fNExpRdsuV = expRdsu.Select(x => x.Value).ToList();
            var fNRdsuV = result.RawDataStdUnit.Select(x => x.Value).ToList();
            CollectionAssert.AreEqual(fNExpRdsuV, fNRdsuV);
        }

        [TestMethod]
        public void setAvailableTags_correctInput_correctOutput()
        {
            var rwdu = new []
            {
                "flowIn", "m3/s",
                "flowOut", "m3/s",
                "flowPercent", "%",
                "rpm", "rpm",
                "velRop", "m/s",
                "presSP", "kPa",
                "depthHole", "m",
                "depthBit", "m",
                "heightHook", "m",
                "volGainLoss", "m3",
                "volTripTank", "m3",
                "weightOnBit", "kN",
            }.CreateDictionaryFromEvenOddList();
            _process.SetAvailableTags(rwdu);
            var expAvailableRequiredTags = new [] {true, true, true};
            var expAvailableOptionalTags = new [] {true, false, true, false, true, false, true, false, true, true, false, true, false};

            CollectionAssert.AreEqual(expAvailableRequiredTags, _process.AvailableRequiredTags.Select(x => x.Value).ToArray());
            CollectionAssert.AreEqual(expAvailableOptionalTags, _process.AvailableOptionalTags.Select(x => x.Value).ToArray());
        }

        [TestMethod]
        public void washData_missingRequiredTag_returnErrorCode()
        {
            var rwdu = new []
            {
                //"flowIn", "m3/s", // Crucial tag is missing
                "flowOut", "m3/s",
                "flowOutPercent", "%",
                "rpm", "rpm",
                "velRop", "m/s",
                "presSP", "kPa",
                "depthHole", "m",
                "depthBit", "m",
                "volGainLoss", "m3",
                "volTripTank", "m3",
            }.CreateDictionaryFromEvenOddList();
            _process.UpdateTagList(rwdu);

            var rawDataNonStd = new Dictionary<string, DataVariable>
            {
                {"flowOut", new DataVariable(DateTime.Now.ToOADate(), 0.15)},
                {"flowOutPercent", new DataVariable(DateTime.Now.ToOADate(), 15.0)},
                {"rpm", new DataVariable(DateTime.Now.ToOADate(), 200.0)},
                {"velRop", new DataVariable(DateTime.Now.ToOADate(), 0.01)},
                {"presSP", new DataVariable(DateTime.Now.ToOADate(), 5000.0)},
                {"depthHole", new DataVariable(DateTime.Now.ToOADate(), 3000)},
                {"depthBit", new DataVariable(DateTime.Now.ToOADate(), 2999)},
                {"volGainLoss", new DataVariable(DateTime.Now.ToOADate(), 3)},
                {"volTripTank", new DataVariable(DateTime.Now.ToOADate(), 2)}
            };

            var result = _process.WashData(rawDataNonStd, new StateMachine(_stateMachineLoggerMock.Object), false);
            // return with error code indicating that flowIn is missing
            Assert.AreEqual(DtectLossRealtimeErrorCodes.NoValueFlowIn, result.ErrorCode);
        }
        [TestMethod]
        public void washData_correctInit_correctConversion()
        {
            var rwdu = new []
            {
                "flowIn", "m3/s",
                "flowOut", "m3/s",
                "flowOutPercent", "%",
                "rpm", "rpm",
                "velRop", "m/s",
                "presSP", "kPa",
                "depthHole", "m",
                "depthBit", "m",
                "heightHook", "m",
                "volGainLoss", "m3",
                "tempBHA", "K",
                "volTripTank", "m3",
            }.CreateDictionaryFromEvenOddList();
            _process.UpdateTagList(rwdu);

            var rawDataNonStd = new Dictionary<string, DataVariable>
            {
                {"flowIn", new DataVariable(DateTime.Now.ToOADate(), 0.1)},
                {"flowOut", new DataVariable(DateTime.Now.ToOADate(), 0.15)},
                {"flowOutPercent", new DataVariable(DateTime.Now.ToOADate(), 15.0)},
                {"rpm", new DataVariable(DateTime.Now.ToOADate(), 200.0)},
                {"velRop", new DataVariable(DateTime.Now.ToOADate(), 0.01)},
                {"presSP", new DataVariable(DateTime.Now.ToOADate(), 5000.0)},
                {"depthHole", new DataVariable(DateTime.Now.ToOADate(), 3000)},
                {"depthBit", new DataVariable(DateTime.Now.ToOADate(), 2999)},
                {"heightHook", new DataVariable(DateTime.Now.ToOADate(), 29)},
                {"volGainLoss", new DataVariable(DateTime.Now.ToOADate(), 3)},
                {"volTripTank", new DataVariable(DateTime.Now.ToOADate(), 2)},
                {"tempBHA", new DataVariable(DateTime.Now.ToOADate(), 273.15)}
            };

            (string k, double v)[] tmpExpRdsu =
            {
                ("flowIn_lpm", 6000.0),
                ("flowOut_lpm", 9000.0),
                ("rpm", 200.0),
                ("velRop_mph", 36),
                ("presSP_bar", 50),
                ("depthHole_m", 3000.0),
                ("depthBit_m", 2999.0),
                ("heightHook_m", 29),
                ("volGainLoss_m3", 3),
                ("volTripTank_m3", 2.0),
            };
            var expRdsu = tmpExpRdsu.ToDictionary(x => x.k, x => x.v).OrderBy(x => x.Key).ToDictionary(x => x.Key, x => x.Value);
            var result = _process.WashData(rawDataNonStd, new StateMachine(_stateMachineLoggerMock.Object), false);
            var r = result.RawData.Where(x => expRdsu.ContainsKey(x.Key)).OrderBy(x => x.Key).ToDictionary(x => x.Key, x => x.Value);
            CollectionAssert.AreEqual(expRdsu, r);
        }

        [TestMethod]
        public void generateNoValueErrorCode_tagNotDefined_returnNonSpecific()
        {
            var ec = _process.GenerateNoValueErrorCode("TagNotDefined");
            Assert.AreEqual(DtectLossRealtimeErrorCodes.MissingValueNonSpecfic, ec);
        }
        [TestMethod]
        public void generateNoValueErrorCode_tagDefined_returnCorrectEnum()
        {
            var ec = _process.GenerateNoValueErrorCode("FlowIn");
            Assert.AreEqual(DtectLossRealtimeErrorCodes.NoValueFlowIn, ec);
        }
        [TestMethod]
        public void generateNoUnitErrorCode_tagNotDefined_returnNonSpecific()
        {
            var ec = _process.GenerateNoUnitErrorCode("TagNotDefined");
            Assert.AreEqual(DtectLossRealtimeErrorCodes.MissingUnitNonSpecfic, ec);
        }
        [TestMethod]
        public void generateNoUnitErrorCode_tagDefined_returnCorrectEnum()
        {
            var ec = _process.GenerateNoUnitErrorCode("FlowIn");
            Assert.AreEqual(DtectLossRealtimeErrorCodes.NoUnitFlowIn, ec);
        }
        [TestMethod]
        public void defineNamespace_noValidCombination_returnEmpty()
        {
            var rwdu = new []
            {
                "flowIn", "wrongUnit",
                "tempBHA", "K",
                "volTripTank", "wrongUnit",
            }.CreateDictionaryFromEvenOddList();
            _process.UpdateTagList(rwdu);
            Assert.IsTrue(!_process.NameSpace.Any());
        }
        [TestMethod]
        public void defineNamespace_validCombination_returnCorrectNamespace()
        {
            var rwdu = new []
            {
                "flowIn", "m3/s",
                "flowOut", "m3/s",
                "flowPercent", "%",
                "rpm", "rpm",
                "velRop", "cm/s",
                "presSP", "kPa",
                "depthHole", "m",
                "depthBit", "m",
                "heightHook", "m",
                "volGainLoss", "m3",
                "tempBHA", "K",
                "volTripTank", "m3",
            }.CreateDictionaryFromEvenOddList();
            _process.UpdateTagList(rwdu);
            var expNameSpace = new[]
            {
                "flowIn", "depthBit", "depthHole", "flowOut", "presSP", "volGainLoss", "volTripTank", "heightHook", "rpm"
            };
            CollectionAssert.AreEqual(expNameSpace, _process.NameSpace.ToList());
        }

        [TestMethod]
        public void defineNamespace_wrongUnitFlowOut_returnFlowOutAnyway()
        {
            var rwdu = new []
            {
                "flowIn", "m3/s",
                "flowOut", "%",
                "depthHole", "m",
                "depthBit", "m",
            }.CreateDictionaryFromEvenOddList();
            _process.UpdateTagList(rwdu);

            var expNameSpace = new[]
            {
                "flowIn", "depthBit", "depthHole", "flowOut"
            };
            var expUndefinedTags = new[]
            {
                "flowOut1_lpm", "presSP_bar", "presSP1_bar", "volGainLoss_m3", "volPitDrill_m3","volTripTank_m3","volTripTank2_m3","heightHook_m","rpm","torque_kNm","weightOnBit_kN","rop_mph"
            };
            CollectionAssert.AreEqual(expNameSpace, _process.NameSpace.ToList());
            CollectionAssert.AreEqual(expUndefinedTags, _process.UndefinedTags.Select(x => x.v).ToList());
        }

        [TestMethod]
        public void defineNamespace_correctInit_setUndefinedTags()
        {
            var rwdu = new []
            {
                "flowIn", "m3/s",
                "flowOut", "lpm",
                "depthHole", "m",
                "depthBit", "m",
            }.CreateDictionaryFromEvenOddList();
            _process.UpdateTagList(rwdu);

            var expNameSpace = new[]
            {
                "flowIn", "depthBit", "depthHole", "flowOut"
            };
            var expUndefinedTags = new[]
            {
                "flowOut1_lpm", "presSP_bar", "presSP1_bar", "volGainLoss_m3", "volPitDrill_m3","volTripTank_m3","volTripTank2_m3","heightHook_m","rpm","torque_kNm","weightOnBit_kN","rop_mph"
            };
            CollectionAssert.AreEqual(expNameSpace, _process.NameSpace.ToList());
            CollectionAssert.AreEqual(expUndefinedTags, _process.UndefinedTags.Select(x => x.v).ToList());

        }

        [TestMethod]
        public void unifyTime_presSPNewest_usePresSPTimeAsTimebase()
        {
            var rwdu = new []
            {
                "flowIn", "m3/s",
                "presSP", "kPa",
                "depthHole", "m",
            }.CreateDictionaryFromEvenOddList();
            _process.UpdateTagList(rwdu);

            var t = DateTime.Parse("31/03/2017 14:50", new CultureInfo("nb-NO", false)).ToOADate();
            var rawDataNonStd = new Dictionary<string, DataVariable>
            {
                {"flowIn", new DataVariable(t, 0.1)},
                {"presSP", new DataVariable(t.AddSeconds(2), 5000.0)},
                {"depthHole", new DataVariable(t, 3000)},
                {"depthBit", new DataVariable(t, 2999)}
            };

            _process.UnifyTime(rawDataNonStd);

            var expTimebaseT = t.AddSeconds(2);
            Assert.AreEqual(expTimebaseT, _process.TimeCurrentD);
        }
        [TestMethod]
        public void unifyTime_oldTimeForDepthHole_setMissingValCode()
        {
            var rwdu = new []
            {
                "flowIn", "m3/s",
                "presSP", "kPa",
                "depthHole", "m",
            }.CreateDictionaryFromEvenOddList();
            _process.UpdateTagList(rwdu);

            var t = DateTime.Parse("20/02/2017 13:17:10", new CultureInfo("nb-NO", false)).ToOADate();
            var rawDataNonStd = new Dictionary<string, DataVariable>
            {
                {"flowIn", new DataVariable(t, 0.1)},
                {"presSP", new DataVariable(t.AddSeconds(2), 5000.0)},
                {"depthHole", new DataVariable(t.SubtractSeconds(600), 3000)},
                {"depthBit", new DataVariable(t, 2999)}
            };
            var rd = _process.UnifyTime(rawDataNonStd);
            Assert.AreEqual(CommonConstants.MissingValueReplacement, rd["depthHole"].V);
        }
    }
}




