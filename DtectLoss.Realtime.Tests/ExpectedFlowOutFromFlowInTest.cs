﻿using DtectLossRealtime;
using DtectLossRealtime.Calculations;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DtectLoss.Realtime.Tests
{

    // Missing implementation in Matlab
    [TestClass]
    public class ExpectedFlowOutFromFlowInTest
    {
        private ExpectedFlowOutFromFlowIn _expectedFlowOutFromFlowIn;
        private Mock<ILogger<TimeAlign>> _loggerMockTimeAlign;

        [TestInitialize()]
        public void Initialize()
        {
            _loggerMockTimeAlign = LoggerUtils.LoggerMock<TimeAlign>();
            var timeAlign = new TimeAlign(_loggerMockTimeAlign.Object);
            var loggerMockExpectedFlowOutFromFlowIn = new Mock<ILogger<ExpectedFlowOutFromFlowIn>>().Object;
            _expectedFlowOutFromFlowIn = new ExpectedFlowOutFromFlowIn(loggerMockExpectedFlowOutFromFlowIn, timeAlign);
        }


        [TestCleanup()]
        public void Cleanup()
        {
        }


        [TestMethod]
        public void EmptyTest()
        {
            Assert.IsNotNull(_expectedFlowOutFromFlowIn);
        }
    }
}




