﻿using System;
using System.Linq;
using Accord.Math;
using DtectLossRealtime;
using DtectLossRealtime.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DtectLoss.Realtime.Tests
{

    [TestClass]
    public class ExpectedTriptankVolTest
    {
        private ExpectedTripTankVol _eTtv;
        private readonly double _areaDppoohM2                   = 0.005; 
        private readonly double _areaDprihM2                    = 0.015;

        [TestInitialize()]
        public void Initialize()
        {
            var loggerMock = LoggerUtils.LoggerMock<ExpectedTripTankVol>();
            _eTtv = new ExpectedTripTankVol(loggerMock.Object);
        }

        [TestCleanup()]
        public void Cleanup()
        {
        }


        [TestMethod]
        public void constructor_correctConstruct_correctOutput()
        {
            Assert.IsTrue(_eTtv.NumStand.Equals(0));
            Assert.IsTrue(_eTtv.DepthBitPrevM.Equals(0));
        }

        [TestMethod]
        public void initialize_correctInit_correctOutput()
        {
            _eTtv.Initialize(ParameterValues.TripTankParams);
            Assert.IsTrue(_eTtv.NumStand.Equals(0));
        }

        [TestMethod]
        public void initialize_setAreadp_correctOutput()
        {
            _eTtv.Initialize(ParameterValues.TripTankParams);
            _eTtv.SetAreadp(_areaDppoohM2, _areaDprihM2);
            Assert.AreEqual(_areaDppoohM2, _eTtv.AreaDppoohM2);
            Assert.AreEqual(_areaDprihM2, _eTtv.AreaDprihM2);
        }
        [TestMethod]
        public void reset_correctOutput()
        {
            _eTtv.Reset();
            _eTtv.SetAreadp(_areaDppoohM2, _areaDprihM2);
            Assert.IsTrue(_eTtv.NumStand.Equals(0));
            Assert.IsTrue(_eTtv.TimeLastStandD.Equals(0));
        }
        [TestMethod]
        public void resetTripVol_correctOutput()
        {
            _eTtv.VolTTcumExpM3 = 1;
            _eTtv.VolTTcumM3 = 2;
            var volTTcumErrM3 = _eTtv.VolTTcumM3 - _eTtv.VolTTcumExpM3;
            _eTtv.ResetTripVol();
            Assert.AreEqual(_eTtv.VolTTcumExpM3, _eTtv.VolTTcumM3);
            Assert.AreEqual(0, _eTtv.VolTTcumErrM3);
        }
        [TestMethod]
        public void haveWeDrilledAnotherStandNow_correctOutput()
        {
            const double numStandTrue = 4.0;
            const double numTrip = 100.0;
            const int numConn = 50;
            const double num = numStandTrue * (numTrip + numConn);
            const double lengthStand_m = 29.0;
            const double areaDP_m2 = 0.01;
            const double volStand_m3 = lengthStand_m / areaDP_m2;
            const double ddM = lengthStand_m / numTrip;
            const double dV_m3 = volStand_m3 / numTrip;
            const double dTd = 2.0 / 3600.0 / 24.0;
            var depthBitM = Vector.Range(1000.0, 1000.0 + lengthStand_m, ddM)
                                    .Concat(Vector.Create(numConn, 1000+lengthStand_m))
                                    .ToArray();
            var volTripTankM3 = Vector.Range(5.0, 5 + volStand_m3, dV_m3)
                                    .Concat(Vector.Create(numConn, 5 + volStand_m3))
                                    .ToArray();
            foreach (var ii in Enumerable.Range(1, (int)numStandTrue + 1) )
            {
                var depthBitMAdd = Vector.Range(1000.0 + ii * lengthStand_m, 1000.0 + (ii + 1) * lengthStand_m, ddM)
                                        .Concat(Vector.Create(numConn, 1000+ (ii + 1) * lengthStand_m))
                                        .ToArray();
                var volTripTankM3Add = Vector.Range(5.0 + ii * volStand_m3, 5 + (ii + 1) * volStand_m3, dV_m3)
                                        .Concat(Vector.Create(numConn, 5 + (ii + 1) * volStand_m3))
                                        .ToArray();

                depthBitM = depthBitM.Concat(depthBitMAdd).ToArray();
                volTripTankM3 = volTripTankM3.Concat(volTripTankM3Add).ToArray();
            }

            var now = DateTime.Now.ToOADate();
            var timeNewD = Vector.Range(now, now + num * dTd, dTd);
            const int depthHoleM = 2000;
            const int flowInLpm = 0;
            _eTtv.Initialize(ParameterValues.TripTankParams);
            _eTtv.DepthLastStandM = depthBitM[0];
            var numStands = 0;
            foreach (var ii in Enumerable.Range(0, (int)num ))
            {
                _eTtv.Step(depthBitM[ii], depthHoleM, volTripTankM3[ii], flowInLpm, timeNewD[ii]);
                if (_eTtv.IsNewStandTripTank)
                {
                    numStands += 1;
                }
            }
            Assert.AreEqual((int)numStandTrue, numStands);
        }
    }
}




