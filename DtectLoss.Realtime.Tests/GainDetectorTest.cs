﻿using System;
using System.Linq;
using Accord.Math;
using DtectLossRealtime;
using DtectLossRealtime.Calculations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DtectLoss.Realtime.Tests
{
    [TestClass]
    public class GainDetectorTest
    {
        private GainDetector _gdo;

        [TestInitialize()]
        public void Initialize()
        {
            var loggerMockLinReg = LoggerUtils.LoggerMock<LinReg>();
            var lr = new LinReg(loggerMockLinReg.Object);
            _gdo = new GainDetector(lr);
            _gdo.Initialize();
        }


        [TestCleanup()]
        public void Cleanup()
        {
        }


        [TestMethod]
        public void InitializeCorrectInitCorrectOutput()
        {
            var expected = Vector.Zeros(_gdo.NSamples);
            Assert.AreEqual(_gdo.RingBufferVolGainLoss.Vbuffer.Length, expected.Length);
        }

        [TestMethod]
        public void IntegrateVolumeAddFirstValueDoNotAccVolume()
        {
            const int flowRateLpm = 1000;
            var timeD = new DateTime(2017, 11, 21, 13, 00, 00).ToOADate();
            _gdo.IntegrateVolume(timeD, flowRateLpm);
            Assert.AreEqual(0, _gdo.VolGainM3);
        }
        [TestMethod]
        public void IntegrateVolumeAddSecondPositiveValueDoAccVolume()
        {
            const double numTol = 1e-5;
            var volGainLossM3 = 0.0;
            const int depthBitM = 1000;
            const int deptHoleM = 1000;
            const int flowInLpm = 2500;
            const double volGainM3PerSec = 0.2 / 60;

            foreach (var i in Enumerable.Range(1, 303 + 1))
            {
                volGainLossM3 += volGainM3PerSec;
                var timeD = new DateTime(2017, 11, 21, 13, 00, 00).AddSeconds(i).ToOADate();
                _gdo.Step(timeD, volGainLossM3, depthBitM, deptHoleM, flowInLpm);
            }

            const double expected = volGainM3PerSec * 3;
            Assert.AreEqual(expected, _gdo.VolGainM3, numTol);
        }
        [TestMethod]
        public void StepNegativeAverageResetGainedVolume_m3()
        {
            var volGainLossM3 = 0.0;
            const int depthBitM = 1000;
            const int deptHoleM = 1000;
            const int flowInLpm = 2500;
            const double volGainM3PerSec = 0.2 / 60;

            foreach (var i in Enumerable.Range(1, 310 + 1))
            {
                volGainLossM3 += volGainM3PerSec;
                var timeD = new DateTime(2017, 11, 21, 13, 00, 00).AddSeconds(i).ToOADate();
                _gdo.Step(timeD, volGainLossM3, depthBitM, deptHoleM, flowInLpm);
            }

            var expected = 0;
            Assert.IsTrue(_gdo.VolGainM3 > expected);
            foreach (var i in Enumerable.Range(1, 310 + 1))
            {
                volGainLossM3 -= volGainM3PerSec;
                var timeD = new DateTime(2017, 11, 21, 13, 30, 00).AddSeconds(i).ToOADate();
                _gdo.Step(timeD, volGainLossM3, depthBitM, deptHoleM, flowInLpm);
            }
            expected = 0;
            Assert.IsTrue(_gdo.FlowBasedOnDerivativeOfGainLossLpm < expected);
            expected = 0;
            Assert.AreEqual(expected, _gdo.VolGainM3);
        }

        [TestMethod]
        public void StepBitOffBottomAddFlowRate()
        {
            const int volGainLossM3 = 1;
            const int depthBitM = 1000;
            const int deptHoleM = 1000;
            const int flowInLpm = 1000;
            foreach (var i in Enumerable.Range(1, 700))
            {
                var timeD = new DateTime(2017, 11, 21, 13, 00, 00).AddSeconds(i).ToOADate();
                _gdo.Step(timeD, volGainLossM3 + i, depthBitM, deptHoleM, flowInLpm);
            }
            Assert.IsTrue(_gdo.VolGainM3 > 0);
        }
        [TestMethod]
        public void StepBitOffBottomDoNotAddFlowRate()
        {
            const int volGainLossM3 = 1;
            const int depthBitM = 998;
            const int deptHoleM = 1000;
            const int flowInLpm = 2500;
            foreach (var i in Enumerable.Range(1, 2))
            {
                var timeD = new DateTime(2017, 11, 21, 13, 00, 00).AddSeconds(i).ToOADate();
                _gdo.Step(timeD, volGainLossM3 + i, depthBitM, deptHoleM, flowInLpm);
            }

            const int expected = 0;
            Assert.AreEqual(expected, _gdo.VolGainM3);
        }
        [TestMethod]
        public void StepTimeStepAboveMaxResetVolume()
        {
            const int volGainLossM3 = 1;
            const int depthBitM = 1000;
            const int deptHoleM = 1000;
            const int flowInLpm = 2500;
            var timeD = new DateTime(2017, 11, 21, 13, 00, 00);
            foreach (var i in Enumerable.Range(1, 310))
            {
                timeD = timeD.AddSeconds(1);
                _gdo.Step(timeD.ToOADate(), volGainLossM3 + i, depthBitM, deptHoleM, flowInLpm);
            }
            const int expected = 0;
            Assert.IsTrue(_gdo.VolGainM3 > expected);
            timeD = timeD.AddSeconds(_gdo.TimeStepMaxS + 1);
            _gdo.Step(timeD.ToOADate(), volGainLossM3, depthBitM, deptHoleM, flowInLpm);
            Assert.AreEqual(expected, _gdo.VolGainM3);
        }
    }
}
