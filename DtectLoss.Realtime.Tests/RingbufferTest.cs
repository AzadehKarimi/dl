﻿using System;
using System.Globalization;
using System.Linq;
using Accord.Math;
using DtectLoss.Common;
using DtectLossRealtime.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static DtectLoss.Common.CommonConstants;

namespace DtectLoss.Realtime.Tests
{

    [TestClass]
    public class RingBufferTest
    {
        private const int NSamples = 10;
        //static readonly double errVal = -0.99925;
        private RingBuffer _ringBuffer;

        [TestInitialize()]
        public void Initialize()
        {
            _ringBuffer = new RingBuffer(NSamples);
        }


        [TestCleanup()]
        public void Cleanup()
        {
            _ringBuffer = null;
        }


        [ClassCleanup()]
        public static void ClassCleanup()
        {
        }


        [TestMethod]
        public void AddSingleValueAndTimeOk()
        {
            const double testTime = 333;
            const double testValue = 12345;
            _ringBuffer.AddDataToCircularBuffer(testTime, testValue);
            Assert.AreEqual(_ringBuffer.GetLastValue(), testValue);
        }
        [TestMethod]
        public void VerifySimpleDeringBuffer()
        {
            const double testTime = 333;
            const double testValue = 12345;
            _ringBuffer.AddDataToCircularBuffer(testTime, testValue);
            var deringbuffer = _ringBuffer.Deringbuffer();
            var vbufferStraight = deringbuffer.VbufferStraight;
            Assert.AreEqual(vbufferStraight[NSamples - 1], testValue);
        }

        [TestMethod]
        public void VarExtraFirstHalfWithFullBufferOnFirstIndex()
        {
            const double fraction = 0.7;
            FillFractionOfRingBuffer(fraction);

            var (varCalc, _) = _ringBuffer.VarFirstHalf();
            Assert.AreEqual(6.666666666666667, varCalc);
        }
        [TestMethod]
        public void VarFirstHalfWithFullBufferOnFirstIndex()
        {
            const int fraction = 1;
            FillFractionOfRingBuffer(fraction);
            var (varCalc, _) = _ringBuffer.VarFirstHalf();
            Assert.AreEqual(10, varCalc);
        }
        [TestMethod]
        public void VarFirstHalfWithHalfBufferOnHalfIndex()
        {
            var fraction = 2;
            FillFractionOfRingBuffer(fraction);
            var (varCalc, _) = _ringBuffer.VarFirstHalf();
            Assert.IsNull(varCalc);
        }

        [TestMethod]
        public void GetPastValueBeforeReturnsMissingValue()
        {
            const int fraction = 1;
            FillFractionOfRingBuffer(fraction);
            var startTime = DateTime.Parse("26/09/1981  00:00:00", new CultureInfo("nb-NO", false)).ToOADate();
            var (vdelayedLpm, _) = _ringBuffer.GetPastValue(100, startTime);
            Assert.AreEqual(MissingValueReplacement, vdelayedLpm);
        }

        [TestMethod]
        public void GetPastValueInsideFirsReturnsValue()
        {
            var fraction = 1;
            FillFractionOfRingBuffer(fraction);
            var startTime = DateTime.Parse("26/09/1981  01:00:22", new CultureInfo("nb-NO", false)).ToOADate();
            var (vdelayedLpm, _) = _ringBuffer.GetPastValue(10, startTime);
            Assert.AreEqual(19, vdelayedLpm);
        }

        [TestMethod]
        public void GetPastValueInsideDelayReturnsValue()
        {
            const int fraction = 1;
            FillFractionOfRingBuffer(fraction);
            var startTime = DateTime.Parse("26/09/1981  01:00:22", new CultureInfo("nb-NO", false)).ToOADate();
            var (vdelayedLpm, _) = _ringBuffer.GetPastValue(17, startTime);
            Assert.AreEqual(7, vdelayedLpm, 0.00001);
        }

        [TestMethod]
        public void GetLastNValues_2_Ok()
        {
            var (_, values) = FillFractionOfRingBuffer(5);
            var result = _ringBuffer.GetLastNValues(2);

            Assert.AreEqual(0, result[0]);
            Assert.AreEqual(values[0], result[1]);
            Assert.AreEqual(values[1], result[2]);
        }
        [TestMethod]
        public void GetLastNValues_2of9_Ok()
        {
            var (_, values) = FillFractionOfRingBuffer(1.2);
            var result = _ringBuffer.GetLastNValues(2);

            Assert.AreEqual(values[6], result[0]);
            Assert.AreEqual(values[7], result[1]);
            Assert.AreEqual(values[8], result[2]);
        }
        [TestMethod]
        public void GetLastNValues_2of10_Ok()
        {
            var (_, values) = FillFractionOfRingBuffer(1);
            var result = _ringBuffer.GetLastNValues(2);
            Assert.AreEqual(values[7], result[0]);
            Assert.AreEqual(values[8], result[1]);
            Assert.AreEqual(values[9], result[2]);
        }

        [TestMethod]
        public void GetLastNValues_5of10_Ok()
        {
            var (_, values) = FillFractionOfRingBuffer(1);
            var result = _ringBuffer.GetLastNValues(5);
            Assert.AreEqual(values[4], result[0]);
            Assert.AreEqual(values[5], result[1]);
            Assert.AreEqual(values[6], result[2]);
            Assert.AreEqual(values[7], result[3]);
            Assert.AreEqual(values[8], result[4]);
            Assert.AreEqual(values[9], result[5]);
        }

        [TestMethod]
        public void GetLastNTimes_2_Ok()
        {
            var (times, _) = FillFractionOfRingBuffer(5);
            var result = _ringBuffer.GetLastNTimes(2);

            Assert.AreEqual(MissingValueReplacement, result[0]);
            Assert.AreEqual(times[0], result[1]);
            Assert.AreEqual(times[1], result[2]);
        }
        [TestMethod]
        public void GetLastNTimes_2of9_Ok()
        {
            var (times, _) = FillFractionOfRingBuffer(1.2);
            var result = _ringBuffer.GetLastNTimes(2);

            Assert.AreEqual(times[6], result[0]);
            Assert.AreEqual(times[7], result[1]);
            Assert.AreEqual(times[8], result[2]);
        }
        [TestMethod]
        public void GetLastNTimes_2of10_Ok()
        {
            var (times, _) = FillFractionOfRingBuffer(1);
            var result = _ringBuffer.GetLastNTimes(2);
            Assert.AreEqual(times[7], result[0]);
            Assert.AreEqual(times[8], result[1]);
            Assert.AreEqual(times[9], result[2]);
        }

        [TestMethod]
        public void GetLastNTimes_5of10_Ok()
        {
            var (times, _) = FillFractionOfRingBuffer(1);
            var result = _ringBuffer.GetLastNTimes(5);
            Assert.AreEqual(times[4], result[0]);
            Assert.AreEqual(times[5], result[1]);
            Assert.AreEqual(times[6], result[2]);
            Assert.AreEqual(times[7], result[3]);
            Assert.AreEqual(times[8], result[4]);
            Assert.AreEqual(times[9], result[5]);
        }

        [TestMethod]
        public void lookUp_timeDelayZero_returnLastValue()
        {
            // Arrange
            // fill up ringbuffer "08/18/2018 07:22:16"
            var startTime = DateTime.Parse("26/09/1981  01:00:02", new CultureInfo("nb-NO", false)).ToOADate();

            // Act
            for (var ii = 0; ii < NSamples; ii++)
                _ringBuffer.AddDataToCircularBuffer(startTime.AddSeconds(Convert.ToDouble(ii)), 2 * Convert.ToDouble(ii));

            var result = _ringBuffer.LookUp(startTime.AddSeconds(Convert.ToDouble(NSamples)));

            // Assert
            var actual = 2 * Convert.ToDouble(NSamples - 1);
            Assert.AreEqual(actual, result);
        }

        [TestMethod]
        public void lookUp_noValueInBuffer_returnErrval()
        {
            // Value should be error value');
            // Arrange
            // Already arranged by [TestInitialize]

            // Act
            var result = _ringBuffer.LookUp(DateTimeOffset.UtcNow.ToUnixTimeSeconds());

            // Assert
            double actual = MissingValueReplacement;
            Assert.AreEqual(actual, result);
        }



        [TestMethod]
        public void lookUp_timeGreaterThanAvailableValue_returnLastValue()
        {
            // Value should be LastValue
            // Arrange
            // fill up ringbuffer "08/18/2018 07:22:16"
            var startTime = DateTime.Parse("26/09/1981  01:00:02", new CultureInfo("nb-NO", false)).ToOADate();

            // Act
            for (var ii = 0; ii < NSamples - 2; ii++)
                _ringBuffer.AddDataToCircularBuffer(startTime.AddSeconds(Convert.ToDouble(ii)), 3 * Convert.ToDouble(ii));


            // Assert
            var result = _ringBuffer.LookUp(startTime + 2.0);
            const double actual = 21;
            Assert.AreEqual(actual, result);
        }



        [TestMethod]
        public void lookUp_timeDelayGreaterThanAvailableValue_returnErrval()
        {
            // Value should be error value');
            // Arrange
            // fill up ringbuffer "08/18/2018 07:22:16"
            var startTime = DateTime.Parse("26/09/1981  01:00:02", new CultureInfo("nb-NO", false)).ToOADate();

            // Act
            for (var ii = 0; ii < NSamples - 2; ii++)
                _ringBuffer.AddDataToCircularBuffer(startTime.AddSeconds(Convert.ToDouble(ii)), 3 * Convert.ToDouble(ii));


            // Assert
            var result = _ringBuffer.LookUp(startTime.SubtractSeconds(Convert.ToDouble(NSamples - 3)));
            var actual = MissingValueReplacement;
            Assert.AreEqual(actual, result);
        }

        [TestMethod]
        public void vecCreate()
        {
            // Arrange
            var result = Vector.Range(9, 1, 1);


            int[] expected = { 9, 8, 7, 6, 5, 4, 3, 2, 1 };

            Assert.IsTrue(expected[5] == result[5]);

        }

        [TestMethod]
        public void lookUp_timeDelayMatchAPrevValExactly_retCorVal()
        {

            //fill up ringbuffer
            // Arrange
            var startTime = DateTime.Parse("26/09/1981  01:00:02", new CultureInfo("nb-NO", false)).ToOADate();

            // Act
            for (var ii = 0; ii < NSamples - 2; ii++)
                _ringBuffer.AddDataToCircularBuffer(startTime.AddSeconds(Convert.ToDouble(ii)), 2 * Convert.ToDouble(ii));

            //Assert
            var result = _ringBuffer.LookUp(startTime.AddSeconds(4));
            const double actual = 8;
            Assert.AreEqual(actual, result);
        }


        [TestMethod]
        public void lookUp_needToInterpolate_retCorVal()
        {
            const double numTol = 2e-5; // absolute error which is tolerated

            // fill up ringbuffer
            // Arrange
            var startTime = DateTime.Parse("26/09/1981  01:00:02", new CultureInfo("nb-NO", false)).ToOADate();

            // Act
            for (var ii = 0; ii < NSamples - 2; ii++)
                _ringBuffer.AddDataToCircularBuffer(startTime.AddSeconds(Convert.ToDouble(ii)), 2 * Convert.ToDouble(ii));

            // look up value in between sampled values
            // Assert
            var result = _ringBuffer.LookUp(startTime.AddSeconds(4.5));
            const double actual = 9;
            Assert.AreEqual(actual, result, numTol);
        }

        [TestMethod]
        public void lookUp_missingValueWhenInterpolating_retCorVal()
        {
            const double numTol = 2e-5; // absolute error which is tolerated
            // fill up ringbuffer
            // Arrange
            var startTime = DateTime.Parse("26/09/1981  01:00:02", new CultureInfo("nb-NO", false)).ToOADate();
            // Act
            for (var ii = 0; ii < NSamples - 2; ii++)
            {
                if (ii % 2 == 0)
                    _ringBuffer.AddDataToCircularBuffer(startTime.AddSeconds(Convert.ToDouble(ii)), 2 * Convert.ToDouble(ii));

                else // the following value will not be added to the ringbuffer.
                    _ringBuffer.AddDataToCircularBuffer(startTime.AddSeconds(Convert.ToDouble(ii)), MissingValueReplacement);
            }
            // Assert
            var result = _ringBuffer.LookUp(startTime.AddSeconds(4.5));
            const double actual = 9;
            Assert.AreEqual(actual, result, numTol);
        }

        [TestMethod]
        public void lookUp_onlyOneValueInBuffer_retErrVal()
        {
            // fill up ringbuffer
            // Arrange
            var startTime = DateTime.Parse("26/09/1981  01:00:02", new CultureInfo("nb-NO", false)).ToOADate();
            // Act
            _ringBuffer.AddDataToCircularBuffer(startTime.AddSeconds(1.0), 3.0);

            // Assert
            var result = _ringBuffer.LookUp(startTime); // look up value not in buffer
            var actual = MissingValueReplacement;
            Assert.AreEqual(actual, result);
        }

        [TestMethod]
        public void lookUp_lookUpTimeGreaterThanNewestTime_correctInterpolation()
        {
            // fill up ringbuffer
            // Arrange
            var startTime = DateTime.Parse("26/09/1981  01:00:02", new CultureInfo("nb-NO", false)).ToOADate();
            // Act
            var ii = 0;
            for (; ii < NSamples - 2; ii++)
                _ringBuffer.AddDataToCircularBuffer(startTime.AddSeconds(Convert.ToDouble(ii)), 2 * Convert.ToDouble(ii));

            // Assert
            var result = _ringBuffer.LookUp(startTime.AddSeconds(Convert.ToDouble(ii) + 1.0));
            const double actual = 14.0;
            Assert.AreEqual(actual, result);
        }

        [TestMethod]
        public void deringbuffer_missingValuesInBuffer_returnCorrectDtAvg_d()
        {
            const double fraction = 2;
            var (tStore, _) = FillFractionOfRingBuffer(fraction);
            var deringbuffer = _ringBuffer.Deringbuffer();
            var dtAct = deringbuffer.DtAvgD;
            var dtExp = tStore.Diff().Average();
            // Assert
            Assert.AreEqual(dtAct, dtExp);
        }

        [TestMethod]
        public void deringbufferAndResample_missingValuesInBuffer_keepMissingVals()
        {
            const double numTol = 3e-5; // relative error which is tolerated
            const int fraction = 2;
            var (tStore, vStore) = FillFractionOfRingBuffer(fraction);
            var n = tStore.Count(i => i > 0);

            var deringbufferAndResample = _ringBuffer.DeringbufferAndResample();
            var tAct = deringbufferAndResample.TbufferDStraightResampled;
            var vAct = deringbufferAndResample.VbufferStraightResampled;

            var tExp = Vector.Create(NSamples - n, MissingValueReplacement).Concat(tStore).ToArray();
            var vExp = Vector.Create(NSamples - n, MissingValueReplacement).Concat(vStore).ToArray();

            Assert.IsTrue(tAct.SequenceEqual(tExp, new ToleranceComparer(numTol * 1e-5)));
            Assert.IsTrue(vAct.SequenceEqual(vExp, new ToleranceComparer(numTol)));
        }

        [TestMethod]
        public void deringbufferAndResample_missingValuesInBuffer_padIfSecondArg()
        {
            const double numTol = 1e-5; // relative error which is tolerated
            const double fraction = 2.0;
            var (tStore, vStore) = FillFractionOfRingBuffer(fraction);

            var n = tStore.Count(i => i > 0); 
            var tMissingStart = tStore[0].SubtractSeconds(n);

            var range1 = Vector.Range(tMissingStart, tStore[0].AddSeconds(1.0), 1.0.SecondsToOADate());
            //var range2 = tStore.SubsetToEnd(1);
            var range2 = tStore[1..];
            var tPadded = range1.Concat(range2).ToArray();
            var deringbufferAndResample = _ringBuffer.DeringbufferAndResample(true);
            var tAct = deringbufferAndResample.TbufferDStraightResampled;
            var vAct = deringbufferAndResample.VbufferStraightResampled;
            var tExp = tPadded;
            var vExp = Vector.Create(NSamples - n, 1.0).Concat(vStore).ToArray();
            Assert.IsTrue(tAct.SequenceEqual(tExp, new ToleranceComparer(numTol)));
            Assert.IsTrue(vAct.SequenceEqual(vExp, new ToleranceComparer(numTol)));
        }

        [TestMethod]
        public void deringbufferAndResample_singleValue_returnEmpty()
        {
            var rb = new RingBuffer(5);
            rb.AddDataToCircularBuffer(2, DateTimeOffset.UtcNow.ToUnixTimeSeconds());

            var deringbufferAndResample = rb.DeringbufferAndResample(true);
            var tbufferDStraightResampled = deringbufferAndResample.TbufferDStraightResampled;
            var vbufferStraightResampled = deringbufferAndResample.VbufferStraightResampled;
            Assert.IsFalse(tbufferDStraightResampled.Any());
            Assert.IsFalse(vbufferStraightResampled.Any());
        }


        [TestMethod]
        public void padMissingValues_useErrValForPadding_correctOutput()
        {
            const double fraction = 2;
            var (tStore, _) = FillFractionOfRingBuffer(fraction);

            var n = tStore.Count(i => i > 0);
            var deringbuffer = _ringBuffer.Deringbuffer();
            var tbufferDStraight = deringbuffer.TbufferDStraight;
            var vbufferStraight = deringbuffer.VbufferStraight;
            var dtAvgD = deringbuffer.DtAvgD;
            var validDataPoints = deringbuffer.ValidDataPoints;

            var tValidD = tbufferDStraight.Subset(validDataPoints);
            var vValid = vbufferStraight.Subset(validDataPoints);
            var nInvalidDataPoints = validDataPoints.Count(x => !x);
            var (tAct, vAct) = _ringBuffer.PadMissingValues(tValidD, vValid, dtAvgD, nInvalidDataPoints, true);
            var tExp = Vector.Create(n, MissingValueReplacement);
            var vExp = tExp;

            Assert.IsTrue(tAct.SequenceEqual(tExp));
            Assert.IsTrue(vAct.SequenceEqual(vExp));
        }

        [TestMethod]
        public void padMissingValues_doNotUseErrValForPadding_correctOutput()
        {
            const double fraction = 2;
            var (tStore, _) = FillFractionOfRingBuffer(fraction);
            var n = tStore.Count(i => i > 0);
            var deringbuffer = _ringBuffer.Deringbuffer();
            var tbufferDStraight = deringbuffer.TbufferDStraight;
            var vbufferStraight = deringbuffer.VbufferStraight;
            var dtAvgD = deringbuffer.DtAvgD;
            var validDataPoints = deringbuffer.ValidDataPoints;

            var tValidD = tbufferDStraight.Subset(validDataPoints);
            var vValid = vbufferStraight.Subset(validDataPoints);
            var nInvalidDataPoints = validDataPoints.Count(x => !x);

            const bool useErrValForPadding = false;
            var (tAct, vAct) = _ringBuffer.PadMissingValues(tValidD, vValid, dtAvgD, nInvalidDataPoints, useErrValForPadding);

            var tExp = Enumerable.Range(1, NSamples - n)
                .Select(x => new DateTime(1981, 09, 26, 00, 59, 56).AddSeconds(x).ToOADate())
                .ToArray();

            var vExp = Vector.Create(n, 1.0);
            var tExpDate = tExp.Select(DateTime.FromOADate);
            var tActDate = tAct.Select(DateTime.FromOADate);
            Assert.IsTrue(tActDate.SequenceEqual(tExpDate));
            Assert.IsTrue(vAct.SequenceEqual(vExp));
        }


        [TestMethod]
        public void deringbufferAndResample_noValidDataPoints_returnEmpty()
        {
            var deringbufferAndResample = _ringBuffer.DeringbufferAndResample();
            var tAct = deringbufferAndResample.TbufferDStraightResampled;
            var vAct = deringbufferAndResample.VbufferStraightResampled;
            Assert.IsFalse(tAct.Any());
            Assert.IsFalse(vAct.Any());
        }

        [TestMethod]
        public void getWindowOfLastValues_timeNewerThanLastValInBuffer_returnEmpty()
        {
            const double fraction = 1;
            FillFractionOfRingBuffer(fraction);
            var tooOldTimeD = DateTime.Parse("26/09/2017  01:00:50", new CultureInfo("nb-NO", false)).ToOADate();
            var (timeD, val) = _ringBuffer.GetWindowOfLastValues(tooOldTimeD);

            Assert.IsFalse(timeD.Any());
            Assert.IsFalse(val.Any());
        }


        [TestMethod]
        public void getWindowOfLastValues_emptyBuffer_returnEmpty()
        {
            var desiredTimeD = DateTime.Parse("26/08/1981  01:00:50", new CultureInfo("nb-NO", false)).ToOADate(); //datenum(1981, 08, 26, 01, 00, 50);
            var (timeD, val) = _ringBuffer.GetWindowOfLastValues(desiredTimeD);

            Assert.IsFalse(timeD.Any());
            Assert.IsFalse(val.Any());
        }


        [TestMethod]
        public void getWindowOfLastValues_timeOlderThanBuffer_returnValidVals()
        {
            const double fraction = 2;
            var (tStore, vStore) = FillFractionOfRingBuffer(fraction);
            var expTime = tStore.Where(x => x > 0).ToArray();
            var expVal = vStore.Where(x => x > 0).ToArray();
            var oldTimeD = DateTime.Parse("26/08/1981  01:00:50", new CultureInfo("nb-NO", false)).ToOADate();
            var (timeD, val) = _ringBuffer.GetWindowOfLastValues(oldTimeD);

            Assert.IsTrue(timeD.SequenceEqual(expTime));
            Assert.IsTrue(val.SequenceEqual(expVal));
        }

        [TestMethod]
        public void getWindowOfLastValues_timeInBuffer_returnValidVals()
        {
            const double fraction = 1;
            var (tStore, vStore) = FillFractionOfRingBuffer(fraction);
            var askingTimeD = DateTime.Parse("26/09/1981  01:00:56", new CultureInfo("nb-NO", false)).ToOADate();
            var expTime = tStore.Where(x => x > askingTimeD).ToArray();
            var expVal = vStore.Where(x => x > askingTimeD).ToArray();
            var (timeD, val) = _ringBuffer.GetWindowOfLastValues(askingTimeD);

            Assert.IsTrue(timeD.SequenceEqual(expTime));
            Assert.IsTrue(val.SequenceEqual(expVal));

        }
        [TestMethod]
        public void GetValueRangeAll()
        {
            const double fraction = 1;
            FillFractionOfRingBuffer(fraction);
            var askingTimeD = DateTime.Parse("26/09/1981  00:00:00", new CultureInfo("nb-NO", false)).ToOADate();
            var val = _ringBuffer.GetValueRange(askingTimeD);
            const int exp = 18;
            Assert.AreEqual(exp, val);
        }
        [TestMethod]
        public void GetValueRangeHalf()
        {
            const double fraction = 1;
            FillFractionOfRingBuffer(fraction);
            var askingTimeD = DateTime.Parse("26/09/1981  01:00:06", new CultureInfo("nb-NO", false)).ToOADate();
            var val = _ringBuffer.GetValueRange(askingTimeD);
            const int exp = 8;
            Assert.AreEqual(exp, val);
        }

        public (double[] tStore, double[] vStore) FillFractionOfRingBuffer(double fraction)
        {
            // fill up ringbuffer
            var startTime = DateTime.Parse("26/09/1981  01:00:02", new CultureInfo("nb-NO", false)).ToOADate();
            var N = (int)Math.Ceiling(NSamples / fraction);
            var tStore = new double[N];
            var vStore = new double[N];

            var dtD = 1.0.SecondsToOADate();
            for (var ii = 0; ii < N; ii++)
            {
                var t = startTime + Convert.ToDouble(ii) * dtD;
                var v = 2.0 * Convert.ToDouble(ii) + 1.0;
                tStore[ii] = t;
                vStore[ii] = v;
                _ringBuffer.AddDataToCircularBuffer(t, v);
            }
            return (tStore, vStore);
        }
    }
}




