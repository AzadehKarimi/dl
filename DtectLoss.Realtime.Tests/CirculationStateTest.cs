﻿using DtectLossRealtime;
using DtectLossRealtime.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static DtectLoss.Common.CommonConstants;

namespace DtectLoss.Realtime.Tests
{

    [TestClass]
    public class CirculationStateTest
    {
        private CirculationState circulationState;


        [TestInitialize()]
        public void Initialize()
        {
            circulationState = new CirculationState();
        }


        [TestCleanup()]
        public void Cleanup()
        {
            circulationState = null;
        }


        [TestMethod]
        public void CorrectInitCorrectSystemState()
        {
            Assert.AreEqual(CirculationStates.NotCirculating, circulationState.CurrentState);
            Assert.AreEqual(CirculationStates.Circulating, circulationState.PreviousState);
        }
        [TestMethod]
        public void ChangeStateCorrectStateChange()
        {
            circulationState.ChangeState(CirculationStates.Drilling);
            Assert.AreEqual(CirculationStates.Drilling, circulationState.CurrentState);
        }
        [TestMethod]
        public void StateMachineMissingFlowDoNothing()
        {
            Assert.AreEqual(CirculationStates.NotCirculating, circulationState.CurrentState);
            circulationState.StateMachine(100,50,100);
            Assert.AreEqual(CirculationStates.Circulating, circulationState.CurrentState);
            circulationState.StateMachine(MissingValueReplacement,50,100);
            Assert.AreEqual(CirculationStates.Circulating, circulationState.CurrentState);
        }
        [TestMethod]
        public void StateMachineMissingDepthBitDoNothing()
        {
            Assert.AreEqual(CirculationStates.NotCirculating, circulationState.CurrentState);
            circulationState.StateMachine(100,100,100);
            Assert.AreEqual(CirculationStates.Circulating, circulationState.CurrentState);
            circulationState.StateMachine(100,100,100);
            Assert.AreEqual(CirculationStates.Drilling, circulationState.CurrentState);
            circulationState.StateMachine(100,MissingValueReplacement,100);
            Assert.AreEqual(CirculationStates.Drilling, circulationState.CurrentState);
        }
        [TestMethod]
        public void StateMachineMissingDepthHoleDoNothing()
        {
            Assert.AreEqual(CirculationStates.NotCirculating, circulationState.CurrentState);
            circulationState.StateMachine(100,100,100);
            Assert.AreEqual(CirculationStates.Circulating, circulationState.CurrentState);
            circulationState.StateMachine(100,100,MissingValueReplacement);
            Assert.AreEqual(CirculationStates.Circulating, circulationState.CurrentState);
        }
        [TestMethod]
        public void ChangeStateCorrectStateMachineBehaviour()
        {
            Assert.AreEqual(CirculationStates.NotCirculating, circulationState.CurrentState);
            circulationState.StateMachine(100,50,100);
            Assert.AreEqual(CirculationStates.Circulating, circulationState.CurrentState);
            
            // test transition from circulating to drilling
            circulationState.StateMachine(100,99.6,100);
            Assert.AreEqual(CirculationStates.Drilling, circulationState.CurrentState);
            // test transition from drilling to circulating
            circulationState.StateMachine(100,99.4,100);
            Assert.AreEqual(CirculationStates.Circulating, circulationState.CurrentState);
            // test transition from circulating to NotCirculating
            circulationState.StateMachine(0,98.9,100);
            Assert.AreEqual(CirculationStates.NotCirculating, circulationState.CurrentState);
            // change state back to drilling and test transition from drilling to NotCirculating
            circulationState.ChangeState(CirculationStates.Drilling);
            Assert.AreEqual(CirculationStates.Drilling, circulationState.CurrentState);
            circulationState.StateMachine(0,98.9,100);
            Assert.AreEqual(CirculationStates.NotCirculating, circulationState.CurrentState);
        }
    }
}
