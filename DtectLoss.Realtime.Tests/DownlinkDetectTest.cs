﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Accord.Math;
using MathNet.Numerics.Data.Matlab;
using DtectLossRealtime;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DtectLoss.Realtime.Tests
{

    [TestClass]
    public class DownlinkDetectTest
    {
        private DownLinkDetect _downLinkDetect;
        private readonly double _initTime = new DateTime(2017, 03, 27, 09, 49, 00).ToOADate();

        [TestInitialize()]
        public void Initialize()
        {
            _downLinkDetect = new DownLinkDetect();
        }


        [TestCleanup()]
        public void Cleanup()
        {
            _downLinkDetect = null;
        }

        [TestMethod]
        public void CheckIfChangingIncreasing()
        {
            var (isIncreasing, isDecreasing) = DownLinkDetect.CheckIfChanging(new double[] {1, 2, 3, 4}, 2);
            Assert.IsTrue(isIncreasing);
            Assert.IsFalse(isDecreasing);
        }
        [TestMethod]
        public void CheckIfChangingNotIncreasing()
        {
            var (isIncreasing, isDecreasing) = DownLinkDetect.CheckIfChanging(new double[] {1, 2, 3, 4}, 6);
            Assert.IsFalse(isIncreasing);
            Assert.IsFalse(isDecreasing);
        }
        [TestMethod]
        public void CheckIfChangingDecreasing()
        {
            var (isIncreasing, isDecreasing) = DownLinkDetect.CheckIfChanging(new double[] {4,3,2,1}, 2);
            Assert.IsTrue(isDecreasing);
            Assert.IsFalse(isIncreasing);
        }
        [TestMethod]
        public void CheckIfChangingNotDecreasing()
        {
            var (isIncreasing, isDecreasing) = DownLinkDetect.CheckIfChanging(new double[] {4,3,2,1}, 6);
            Assert.IsFalse(isIncreasing);
            Assert.IsFalse(isDecreasing);
        }

        [TestMethod]
        public void CheckIfChangingTooFewItems()
        {
            var (isIncreasing, isDecreasing) = DownLinkDetect.CheckIfChanging(new double[] {1}, 6);
            Assert.IsFalse(isIncreasing);
            Assert.IsFalse(isDecreasing);
        }

        [TestMethod]
        public void StepActualDownlinkingSimilarResultsAsInitialVersion()
        {
            //Cannot find actualDownlink data file
            var downlinkTestFile = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\HelperFiles\downlinking_actualDownlink.mat";
            var fileExists = File.Exists(downlinkTestFile);
            Assert.IsTrue(fileExists, $"Downlink test file not found: {downlinkTestFile}");

            var downlink = MatlabReader.ReadAll<double>(downlinkTestFile);
            var t = downlink["timeDownlink_d"].Column(0).ToArray().Select( x => x + _initTime).ToArray();
            var actIsDownlinking = new bool[t.Length];
            var actDownlinking = new bool[t.Length];
            var differences = 0;

            for (var i=0; i<t.Length; i++)
            {
                _downLinkDetect.Step(downlink["XDownlink"][i, 1], t[i], downlink["XDownlink"][i, 0]);
                actIsDownlinking[i] = _downLinkDetect.IsDownlinking;
                actDownlinking[i] = Convert.ToBoolean(downlink["XDownlink"][i, 5]);
                if (!_downLinkDetect.IsDownlinking.IsEqual(actDownlinking[i]))
                {
                    differences++;
                }
            }
            Assert.IsTrue(differences < 5);

        }

        [TestMethod]
        public void step_actualDownlinkingNoFlowInMeas_sameResultAsInitialVersion()
        {
            // Verify data file exists
            var downlinkTestFile = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\HelperFiles\downlinking_actualDownlink.mat";
            var fileExists = File.Exists(downlinkTestFile);
            Assert.IsTrue(fileExists, $"Downlink test file not found: {downlinkTestFile}");

            var downlink = MatlabReader.ReadAll<double>(downlinkTestFile);
            var t = downlink["timeDownlink_d"].Column(0).ToArray().Select(x => x + _initTime).ToArray();
            var actIsDownlinking = new bool[t.Length];
            var actDownlinking = new bool[t.Length];
            var differences = 0;

            for (var i = 0; i < t.Length; i++)
            {
                _downLinkDetect.Step(downlink["XDownlink"][i, 1], t[i]  /* zero flow*/);
                actIsDownlinking[i] = _downLinkDetect.IsDownlinking;
                actDownlinking[i] = Convert.ToBoolean(downlink["XDownlink"][i, 5]);
                if (!_downLinkDetect.IsDownlinking.IsEqual(actDownlinking[i]))
                {
                    differences++;
                }
            }
            Assert.IsTrue(differences < 1);
        }

        [TestMethod]
        public void step_rampUp_notDownlinking()
        {
            // Verify data file exists
            var rampUpTestFile = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\HelperFiles\downlinking_rampUp.mat";
            var fileExists = File.Exists(rampUpTestFile);
            Assert.IsTrue(fileExists, $"RampUp test file not found: {rampUpTestFile}");

            var rampUp = MatlabReader.ReadAll<double>(rampUpTestFile);
            var t = rampUp["timeRampUp_d"].Column(0).ToArray().Select(x => x + _initTime).ToArray();
            var detectedDownlinking = new bool[t.Length];

            for (var i = 0; i < t.Length; i++)
            {
                _downLinkDetect.Step(rampUp["XRampUp"][i, 1], t[i], rampUp["XRampUp"][i, 0]);
                detectedDownlinking[i] = _downLinkDetect.IsDownlinking;
            }

            Assert.IsTrue(detectedDownlinking.Sum() < 1);
        }

        [TestMethod]
        public void step_rampDown_notDownlinking()
        {
            // Verify data file exists
            var rampUpTestFile = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\HelperFiles\downlinking_rampDown.mat";
            var fileExists = File.Exists(rampUpTestFile);
            Assert.IsTrue(fileExists, $"Rampdown test file not found: {rampUpTestFile}");

            var rampUp = MatlabReader.ReadAll<double>(rampUpTestFile);
            var t = rampUp["timeRampDown_d"].Column(0).ToArray().Select(x => x + _initTime).ToArray();
            var detectedDownlinking = new bool[t.Length];

            for (var i = 0; i < t.Length; i++)
            {
                _downLinkDetect.Step(rampUp["XRampDown"][i, 1], t[i], rampUp["XRampDown"][i, 0]);
                detectedDownlinking[i] = _downLinkDetect.IsDownlinking;
            }
            Assert.IsTrue(detectedDownlinking.Sum() < 1);
        }
    }
}
