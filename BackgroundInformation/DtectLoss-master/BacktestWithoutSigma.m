close all; clc; clear;

varNames={'flowIn', 'flowOut', 'flowOutPercent', 'volGainLoss', 'presSP',...
    'depthHole', 'depthBit'};

fdp=FIELD_DATA_PLAYER_OBJ('simData/Simrig-1.mat', 1, varNames); foConv=6e4; fiConv=1; flowTag='flowOut';
fdp=fdp.initialize;

nSim=2*max((size(fdp.ds.flowIn.V)));
simTime_s=zeros(nSim,1);
simTime_d=zeros(nSim,1);
flowOut=zeros(nSim,1);
predError = zeros(nSim,1);
presSP=zeros(nSim,1);
flowOutTime=zeros(nSim,1);
volGLCorr=zeros(nSim,1);
x2InjStore=zeros(nSim,1);
rollAvgVar=zeros(nSim,1);
x=zeros(3,nSim);

X=zeros(nSim,5);

prevGoodVolGainLoss_m3 = -0.99925;
%%

params = getparams(0);

offset=-fdp.output.volGainLoss.V;
volDrainback=5;
x(:,1)=[3000*60/60e3; fdp.ds.volGainLoss.V(1); 0]; % [volFlowLine; volGainLossCorrected]
timeDelaySec=0;
prevVolGL=fdp.output.volGainLoss.V;

% output injection
kInj=1/400;

% expected pit and flowline volume
fap=FLOWLINE_AND_PIT_OBJ();
fap=fap.initialize(x(:,1),volDrainback,kInj*0,kInj*1);
fap=fap.updateTimeStep(fdp.output.flowIn, fdp.output.flowIn, fdp.output.volGainLoss);
t(1)=fap.prevTimeStamp;

% recorder for flow backs
nFlowbacks=100; % [int]
minVolume=1; % [m3]
minFlowrate=50; % [lpm] threshold for flow-rate from rig pumps. qp < minFlowrate -> pumps off
flowTolerance=400; % [lpm] stddev which is tolerated as steady state for flow-rate from rig pumps
dtFlowrate=1;%round(mean(diff(fdp.ds.flowIn.t)*86400)); % [s]
dtVolGL=1;%round(mean(diff(fdp.ds.volGainLoss.t)*86400)); % [s]
timeDuration=1200; % [s]
flowbackRecorderObj=FLOWBACK_RECORDER_OBJ();
flowbackRecorderObj=flowbackRecorderObj.initialize(nFlowbacks, minVolume, minFlowrate, flowTolerance,...
    dtFlowrate, dtVolGL, timeDuration, params.pitvol);

c=1; % counter
flowOut(c)=max(0, min(1e4,fdp.output.(flowTag).V*foConv));
c=2;

% Process obj
processParams=getparams(0);
po=PROCESS_OBJ(params, fdp.startTime);
po=po.specifyUnits(fdp.rawDataUnits);
po=po.setAvailableTags(fdp.rawDataUnits);
po=po.defineNameSpace;

% system state
sysState=STATE_MACHINE_OBJ();

% expected flow out
flowOutFlowInObj = EXPECTED_FLOWOUT_FROM_FLOWIN_OBJ();
flowOutFlowInObj = flowOutFlowInObj.initalize(params.flowout);

% expected standpipe pressure
expStandPipePresObj = EXPECTED_STANDPIPE_PRES_OBJ();
expStandPipePresObj = expStandPipePresObj.initalize(params.pressp);

expTripTankVolObj   = EXPECTED_TRIPTANKVOL_OBJ();
expTripTankVolObj   = expTripTankVolObj.initialize(params.triptank);            

gainDetectorObj     = GAIN_DETECTOR_OBJ();
gainDetectorObj     = gainDetectorObj.initialize;



% voting
algObj = VOTING_ALG_OBJ;
algObj = algObj.initialize(params.voting);

% machinelearning data:
X(1,1)=fdp.output.flowIn.V(1)*fiConv;
X(1,2)=fdp.output.volGainLoss.V;
X(1,3)=fap.x(2);
X(1,4)=fdp.output.depthBit.V;
X(1,5)=fdp.output.depthHole.V;

simTime_s(1)=0;
simTime_d(1)=fdp.startTime;

resetDone = false;
prevPumpState=false; % rig pump off



while fdp.simProgress < 50
    
    fdp = fdp.asyncPlay;
    
    % wash data
    [po, washedData, rawDataStdUnit, processOutput, errorCode, warningCode] = ...
        po.washData(fdp.output, sysState, false);
    sysState = sysState.update(washedData, po.dt_s);
    
    [flowOutFlowInObj, flowInOutOutput] = flowOutFlowInObj.step(washedData.flowIn_lpm, washedData.flowOut_lpm,...
        washedData.isDownlinking, 0, po.timeCurrent_d);
    
    [expStandPipePresObj, presExpOutput] = expStandPipePresObj.step(washedData.flowInfilt_lpm, washedData.presSP_bar,...
        washedData.isDownlinking, 0, po.timeCurrent_d);
    
    [expTripTankVolObj, volExpTripOutput] = expTripTankVolObj.step(washedData.depthBit_m, washedData.depthHole_m,  washedData.volTripTank_m3, washedData.flowInfilt_lpm, po.timeCurrent_d);
    
    flowbackRecorderObj = flowbackRecorderObj.addFlowToRingbuffer(po.timeCurrent_d, washedData.flowIn_lpm);
    flowbackRecorderObj = flowbackRecorderObj.addPresToRingbuffer(po.timeCurrent_d, washedData.presSP_bar);
    flowbackRecorderObj = flowbackRecorderObj.addVolToRingbuffer(po.timeCurrent_d, washedData.volGainLoss_m3);
    flowbackRecorderObj = flowbackRecorderObj.addExpVolToRingbuffer(fap.prevTimeStamp, fap.x(2));
    flowbackRecorderObj = flowbackRecorderObj.updateCirculationState(washedData.flowIn_lpm, washedData.depthBit_m, washedData.depthHole_m);
    flowbackRecorderObj = flowbackRecorderObj.updateTimeSpentAtState(po.dt_s);
    [flowbackRecorderObj, fap] = flowbackRecorderObj.updateFap(fap);
    flowbackRecorderObj = flowbackRecorderObj.updateConnectionData;

    volGainLossPredError = 0;
    if ~isempty(flowbackRecorderObj.mdl)
        if ~isempty(flowbackRecorderObj.mdl.coeffs)
            volGainLossPredError = flowbackRecorderObj.predictFromModel;
        end
    end

    
%     resetExpctedVolGainLoss(washedData);
    if prevGoodVolGainLoss_m3 == -0.99925 && washedData.volGainLoss_m3 ~= -0.99925
        prevGoodVolGainLoss_m3 = washedData.volGainLoss_m3;
    end

    if washedData.volGainLoss_m3 ~= -0.99925
        fap = fap.resetGLWithSlidingWindowOfFlowIn(prevGoodVolGainLoss_m3, washedData.volGainLoss_m3,...
            po.volGainLossThreshold_m3_per_time_step);
        prevGoodVolGainLoss_m3 = washedData.volGainLoss_m3;
    end

    if flowbackRecorderObj.isInit
        fap = fap.resetGL(prevGoodVolGainLoss_m3);
    end
    
    
    fap = fap.stepIt(...
        struct('t', po.timeCurrent_d, 'V', flowInOutOutput.flowOutExpected_lpm),... % expected flow rate out
        struct('t', po.timeCurrent_d, 'V', washedData.flowIn_lpm),... % flow in
        struct('t', po.timeCurrent_d, 'V', washedData.volGainLoss_m3),... % gain/loss volume which is reset
        flowbackRecorderObj.pumpsOn); % boolean which indicate if output injection should be used. 

    volGainLossPredModelled = fap.x(2);
    volGainLossExpected_m3 = volGainLossPredModelled + volGainLossPredError;
    
%     [volGainLossPredModelled] = stepFlowlineAndPitObj(flowInOutOutput, washedData);
%     volGainLossExpected_m3 = volGainLossPredModelled + volGainLossPredError;

    isCalibratedExpPitVol_boolean = any(flowbackRecorderObj.kStore > 0); % isCalibratedExpPitVol();
    
    
    
    flowlineAndPitOutput= fap.generateOutput(volGainLossExpected_m3, isCalibratedExpPitVol_boolean);
    
    gainDetectorObj = gainDetectorObj.step(po.timeCurrent_d, washedData.volGainLoss_m3,...
        washedData.depthBit_m, washedData.depthHole_m, washedData.flowIn_lpm);
    gainDetectorOutput = gainDetectorObj.generateOutput;    
    
    isCalibrating = expStandPipePresObj.isCalibratingExpPresSP || ...
        po.paddleConvObj.isCalibratingPaddleBias || po.paddleConvObj.isCalibratingPaddleK || ...
        flowOutFlowInObj.isCalibratingFlowInFlowOut || po.presSPdelayObj.isCalibratingSPdelay;
    
    
        algData = struct('timeCurrent_d',             po.timeCurrent_d,...
                         'flowOutExpected_lpm',       flowOutFlowInObj.flowOutExpected_lpm,...
                         'presSPExpected_bar',        expStandPipePresObj.presSPExpected_bar,...
                         'numberPumpStops',           washedData.numberPumpStops,...
                         'timePumpsOn_s',             washedData.timePumpsOn_s,...                             
                         'volTripTankCumMeas_m3',     expTripTankVolObj.volTTcum_m3,...
                         'volTripTankCumExpected_m3', expTripTankVolObj.volTTcumExp_m3,...
                         'volTripTankCumError_m3',    expTripTankVolObj.volTTcumErr_m3,...
                         'timeSinceResetGL_s',        fap.timeSinceResetGL_s,...
                         'volGainLossExpected_m3',    volGainLossExpected_m3,...
                         'volGainDetector_m3',        gainDetectorObj.volGain_m3,...
                         'isCalibratedExpPresSP',     expStandPipePresObj.isCalibratedExpPresSP,...
                         'isCalibratedExpPitVol',     isCalibratedExpPitVol_boolean,...
                         'isCalibratedExpFlowOut',    flowInOutOutput.isCalibratedExpFlowOut, ...
                         'isCalibratedMeasFlowOut',   washedData.isCalibratedMeasFlowOut, ...
                         'isCalibrating',             isCalibrating, ...
                         'isDownlinking',             washedData.isDownlinking, ...
                         'isRampingUp',               washedData.isRampingUp, ...
                         'resetVolIOnow',             washedData.isCalibratingPaddleK ...
            );

%     algData = struct(...
%         'timeCurrent_d',           po.timeCurrent_d,...
%         'flowOutExpected_lpm',     flowOutFlowInObj.flowOutExpected_lpm,...
%         'presSPExpected_bar',      expStandPipePresObj.presSPExpected_bar,...
%         'volTripTankExpected_m3',  0,...
%         'volPitExpected_m3',       fap.x(2),...
%         'isCalibratedExpPresSP',   expStandPipePresObj.isCalibratedExpPresSP,...
%         'isCalibratedTripTank',    true,...
%         'isCalibratedExpPitVol',   true,...
%         'isCalibratedExpFlowOut',  flowInOutOutput.isCalibratedExpFlowOut, ...
%         'isCalibratedMeasFlowOut', washedData.isCalibratedMeasFlowOut, ...
%         'isCalibrating',           false, ...
%         'isDownlinking',           washedData.isDownlinking, ...
%         'isRampingUp',             washedData.isRampingUp, ...
%         'resetVolIOnow',           false, ...
%         'volGainDetector_m3',      0);
    
    
    
    [algObj, algResult] = algObj.stepIt(washedData, algData, sysState, po.timeCurrent_d, po.dt_s);
    
%     if ~resetDone && fdp.simProgress >= 25
%         fprintf(1, 'Soft reset at %s\n', datestr(po.timeCurrent_d));
%         fr = fr.softReset;
%         po = po.softReset;
%         fap = fap.softReset;
%         flowOutFlowInObj = flowOutFlowInObj.softReset;
%         algObj = algObj.softReset();
%         resetDone = true;
%     end
%     
%     fr = fr.addFlowToRingbuffer(po.timeCurrent_d, washedData.flowIn_lpm);
%     fr = fr.addPresToRingbuffer(po.timeCurrent_d, washedData.presSP_bar);
%     fr = fr.addVolToRingbuffer(po.timeCurrent_d, washedData.volGainLoss_m3);
%     fr = fr.addExpVolToRingbuffer(fap.prevTimeStamp, fap.x(2));
%     fr = fr.updateCirculationState(washedData.flowIn_lpm, washedData.depthBit_m, washedData.depthHole_m);
%     fr = fr.updateTimeSpentAtState(po.dt_s);
%     [fr, fap] = fr.updateFap(fap);
%     fr = fr.updateConnectionData;
%     if ~isempty(fr.mdl)
%         if fr.mdl.RMSE < 1
%             predError(c) = fr.predictFromModel;
%         end
%     end
    
    % if gain/loss is reset, then reset expected volGainLoss.
    % initialize previous good value of volGainLoss, which will be
    % used for comparison with current volGainLoss
    if prevVolGL == -0.99925 && washedData.volGainLoss_m3 ~= -0.99925
        prevVolGL = washedData.volGainLoss_m3;
    end
    
    if washedData.volGainLoss_m3 ~= -0.99925
        fap = fap.resetGLWithSlidingWindowOfFlowIn(prevVolGL, washedData.volGainLoss_m3,...
                    po.volGainLossThreshold_m3_per_time_step);
        prevVolGL = washedData.volGainLoss_m3;
        volGLCorr(c) = washedData.volGainLoss_m3;
    else
        volGLCorr(c)=prevVolGL;
    end
    
    fap = fap.stepIt(...
        struct('t', po.timeCurrent_d, 'V', washedData.flowIn_lpm),... % expected flow rate out
        struct('t', po.timeCurrent_d, 'V', washedData.flowIn_lpm),... % flow in
        struct('t', po.timeCurrent_d, 'V', washedData.volGainLoss_m3),... % gain/loss volume which is reset
        flowbackRecorderObj.pumpsOn); % boolean which indicate if output injection should be used.
    
    simTime_s(c)=round(simTime_s(c-1)+fdp.asyncDT);
    simTime_d(c)=fdp.absTime;
    
    if washedData.flowOut_lpm==-0.99925
        flowOut(c)=flowOut(c-1);
    else
        flowOut(c)=max(0, washedData.flowOut_lpm);
    end
    
%     if (~prevPumpState && fr.pumpsOn)
%         % fap=fap.resetGL(fdp.output.volGainLoss.V);
%     elseif (prevPumpState && ~fr.pumpsOn)
%         fap=fap.resetGL(fdp.output.volGainLoss.V);
%     end
%     prevPumpState=fr.pumpsOn;
    
    x(:,c)=fap.x;
    x2InjStore(c)=flowbackRecorderObj.pumpsOn*max(fap.lowerInjLimit/60e3, min(kInj*(volGLCorr(c)-x(2,c)),fap.upperInjLimit/60e3));
    
    X(c,1)=washedData.flowIn_lpm;
    X(c,2)=volGLCorr(c);
    X(c,3)=fap.x(2);
    if washedData.depthBit_m == -0.99925
        X(c,4)=X(c-1,4);
    else
        X(c,4)=washedData.depthBit_m;
    end
    if washedData.depthHole_m == -0.99925
        X(c,5)=X(c-1,5);
    else
        X(c,5)=washedData.depthHole_m;
    end
    
    c=c+1;
end

X(c:end,:)=[];

%% plotting
nEnd=c-1;
pitLim=2;

hFig1=figure;
ax(1)=subplot(311);
plot(simTime_d(1:nEnd), X(1:nEnd,1), simTime_d(1:nEnd), flowOut(1:nEnd));
legend('flowIn', 'flowOut');
ax(2)=subplot(312);
plot(simTime_d(1:nEnd), volGLCorr(1:nEnd), simTime_d(1:nEnd), x(2,1:nEnd),...
    simTime_d(1:nEnd), x(2,1:nEnd) + predError(1:nEnd)');
legend('volGL', 'volGLHat', 'model + pred');
ax(3)=subplot(313);
plot(simTime_d(1:nEnd), x(1,1:nEnd));

mask = diff([0; volGLCorr]) ~= 0;
mask = mask & simTime_d > 0;

meanDt_s = round(mean(diff(simTime_s(mask))));
windowSize = ceil(600/meanDt_s);
b = 1/windowSize*ones(1,windowSize);
lossRate = filter(b,1,(x(3,mask)'+x2InjStore(mask))*60e3);

hFig2=figure;
ax(4)=subplot(311);
stairs(simTime_d(mask), volGLCorr(mask)-x(2,mask)')
hold on;
stairs(simTime_d(mask), volGLCorr(mask) - x(2,mask)' - predError(mask))
title('volGL-volGLHat');
ax(5)=subplot(312);
plot(simTime_d(1:nEnd), X(1:nEnd, 4:5)')
set(ax(5), 'YDir', 'Reverse');
title('Bit and hole depth');
ax(6)=subplot(313);
plot(simTime_d(mask), (x(3,mask)'+x2InjStore(mask))*60e3,...
    simTime_d(mask), lossRate)
title('Estimated loss/gain rate');
legend('Raw', 'Running avg', 'Location', 'NorthWest');

for ii=1:numel(ax)
    grid(ax(ii));
    datetick(ax(ii),'x');
    zoom(ax(ii),'on');
end

linkaxes(ax,'x');
axis(ax, 'tight');

maeBenchmark = mean(abs(volGLCorr(mask)-x(2,mask)'));
maePred = mean(abs(volGLCorr(mask)-x(2,mask)' - predError(mask)));

fprintf('MAE of model only\t%0.2f m3\nMAE of prediction\t%0.2f m3\n', maeBenchmark, maePred);

dcmObj1 = datacursormode(hFig1);
set(dcmObj1,'UpdateFcn',@datestrInDatatips)

dcmObj2 = datacursormode(hFig2);
set(dcmObj2,'UpdateFcn',@datestrInDatatips)

