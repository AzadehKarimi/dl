
FLOW_DEADBAND = 200;% what is the UNIT???????
VOLUME_THRESHOLD = 1; % what is the UNIT?????
PIT_THRESHOLD = 5;% what is the UNIT??????

clear
close all
load('fieldData\eightAndAHalf\CoslPromoterDrilling85.mat');

dvec={'holeDepth' 'bitDepth' 'ropAvg' 'flowOut' 'flowIn' 'flowOutPercent' 'spp' 'gainLoss' 'pitDrill' };
%HKDLAV WOBAVG TORQUE_AVG SURF_RPM 'DMIAVG' 'DMOAVG' FLOWOUT_CORIOLIS TRPT
%PIT_TRIPIN PIT_TRIPOUT ACTECDX PWEA-T__MEMORY_TIME 
%FLOWOUTPC TMIAVG TMOAVG PITT PWPA-T PWTAZ-T
for j=1:3,
    if j==1,
        ts=tsColl1;
    elseif j==2,
        ts=tsColl2;
    else
        ts=tsColl3;
    end
    ts.flowIn.Data=ts.flowIn.Data+1500;%missing boost

    fdp=FIELD_DATA_PLAYER_OBJ(ts,PIT_THRESHOLD,dvec);
    fdp=fdp.initialize;

    algObj=SIMPLE_ALG_OBJ();
    algObj=algObj.initialize;
    algObj.params.pitThreshold=PIT_THRESHOLD;

    anaObj=DUMMY_ANALYZE_OBJ(FLOW_DEADBAND,VOLUME_THRESHOLD);

    N=ts.Length;

    [t, outputStore] = runAlgorithmWithFieldData( algObj, fdp, N );

    anaObj=anaObj.analyze(outputStore(:,10),outputStore(:,11));
    fprintf(1,'Number of alarms=%g\n',anaObj.noOfAlarms);

    figure(j)
    subplot(211)
    plot(t,outputStore(:,4),t,outputStore(:,5),t,outputStore(:,10),'LineWidth',2)
    grid
    legend('Qin','Qout','Qinflux')
    ylabel('Flow rate [lpm]')
    subplot(212)
    plot(t,outputStore(:,8),t,outputStore(:,11),'LineWidth',2)
    grid
    legend('GainLoss','Vinflux')
    ylabel('Gain/loss volume [m3]')
    xlabel('Time [min]')
end
