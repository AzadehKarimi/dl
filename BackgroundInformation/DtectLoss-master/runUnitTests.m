function numFailedtests = runUnitTests 
% runs all unit test 
% return value 
%  -1: an error occured running unit tests
%   0: no unit tests failed
%   >0: the number of failed unit tests.

close all; %clc; clear classes;
numFailedtests = -1;

% firstly add ekd-directory to path
filanameWithPath = strcat(mfilename('fullpath'),'.m');
[FILEPATH,NAME,EXT] = fileparts(filanameWithPath);
cd(FILEPATH);
addpath(genpath(pwd));  % Add current path and all subfolders to MATLAB path.

% secondly,set ut testing suite and run unit tests.
import matlab.unittest.TestSuite;
import matlab.unittest.TestRunner;
import matlab.unittest.plugins.TAPPlugin;
import matlab.unittest.plugins.ToFile;

try
%     tapFile = fullfile('C:\Appl\Jenkins\workspace\StopLoss','testResults.tap');
%     warning('off', 'MATLAB:DELETE:FileNotFound');
%     delete(tapFile);
%     warning('on', 'MATLAB:DELETE:FileNotFound');

    suite = TestSuite.fromFolder('Tests\unitTests');
    % Create a typical runner with text output
    runner = TestRunner.withTextOutput();
    % Add the TAP plugin and direct its output to a file
   % runner.addPlugin(TAPPlugin.producingOriginalFormat(ToFile(tapFile)));
    % Run the tests
    results = runner.run(suite);
    display(results);
    close all;
    % count number of failed tests
    numFailedtests = 0;
    for testIdx =1:1:numel(results)
        if results(testIdx).Failed==1
            numFailedtests = numFailedtests+1;
        end
    end
catch e
    numFailedtests =-1;
    disp(getReport(e,'extended'));
%    exit(1);
end
%exit;