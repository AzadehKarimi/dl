function [returnCode, KPI, PD, FAR, numberOfScenariosRun, totalSimDuration_s] = ...
    runBacktestTripping(buildNr, varargin)
% ----------------------------------------- 
% streams data to a bank of algorithms,
% against against logged field data,
% usage: call  runBacktest(0) to call the file manually
% - runBacktest(buildNr) if called with nonzero buildNr the build number is
% padded to the "plantName" of each simulation, useful for Continous
% Integration
% - optionally excepts a variable in which disables 
% to call method in jar on dynamic path: package.myclass.mymethod(params)
if (length(varargin)==1)
    START_SIGMAPLOTTER = varargin{1};
else
    START_SIGMAPLOTTER = 1;
end

KPI     = -0.99925;
PD      = -0.99925;
FAR     = -0.99925;
numberOfScenariosRun = 0;
totalSimDuration_s   = 0;

% Constants
FIELD_DATA_ROOT_DIR         = 'fieldDataTripping';
DO_PROFILER                 = 0;
IS_DEBUG_MODE               = 0;
doWrapperTest               = 1;

isBuildSrv                  = isBuildServer();
if isBuildSrv 
    doParalell              = 1;
else
    doParalell              = 0;
end

try
    returnCode  = 0;
    [evaRes ,numberOfScenariosRun,totalSimDuration_s]  = ...
        backTestAlgorithmsOnCatalog(FIELD_DATA_ROOT_DIR,DO_PROFILER,IS_DEBUG_MODE,doWrapperTest,buildNr,doParalell,START_SIGMAPLOTTER);
    KPI         = evaRes.meanKpi;
    PD          = evaRes.totalPd;
    FAR         = evaRes.totalFar;
catch ME
    disp(['ERROR:',ME.identifier,':',ME.message,':',ME.getReport('extended', 'hyperlinks','off')]);
    returnCode = 1;
end

