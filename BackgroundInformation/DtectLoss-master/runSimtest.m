function  [returnCode, KPI, PD, FAR, numberOfScenariosRun, totalSimDuration_s] =  runSimtest(buildNr, varargin)
KPI     = -0.99925;
PD      = -0.99925;
FAR     = -0.99925;
numberOfScenariosRun = 0;
totalSimDuration_s   = 0;
% ----------------------------------------- 
% streams data to a bank of algorithms,
% against against logged field data,
% returns 1 if runSimtest crashed, otherwise returns 0.
if nargin < 1
    buildNr = 0;
end

if (length(varargin)==1)
    START_SIGMAPLOTTER = varargin{1};
else
    START_SIGMAPLOTTER = 1;
end

close all
%FIELD_DATA_ROOT_DIR         = 'simData\';
FIELD_DATA_ROOT_DIR         = 'fieldData\';
DO_PROFILER                 = 0;
IS_DEBUG_MODE               = 0;
doWrapperTest               = 1;
doParalell                  = 0;

try
    returnCode  = 0;
    
    [evaRes, numberOfScenariosRun, totalSimDuration_s]  = ...        
        backTestAlgorithmsOnCatalog(FIELD_DATA_ROOT_DIR, DO_PROFILER, IS_DEBUG_MODE,...
        doWrapperTest, buildNr, doParalell, START_SIGMAPLOTTER);
    KPI         = evaRes.meanKpi;
    PD          = evaRes.totalPd;
    FAR         = evaRes.totalFar;

catch ME
    disp(['ERROR:',ME.identifier,':',ME.message,':',ME.getReport('extended', 'hyperlinks','off')]);
    returnCode = 1;    
end
