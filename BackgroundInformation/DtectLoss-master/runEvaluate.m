function [evaTable] = runEvaluate(varargin)

    ROOT_DIR = 'C:\Appl\Source\IntelligentDrilling\StopLossGit\SIGMA\loggedData\';
    d                       = dir('.\SIGMA\loggedData');
    comment                 = [];
    index                   = [];

    %Defaults
    tripdata = false;
    t_start  = '19-jan-2018 09:40:00';
    t_end    = '19-jan-2018 19:30:00';
    
    if nargin == 1
        tripdata = varargin{1};
    elseif nargin == 3
        t_start  = varargin{2};
        t_end    = varargin{3};
    end

    N = size(d,1);
    for ii = 3:N-1
        if datetime(d(ii).date,'Locale','no_NO') >= datetime(t_start) && datetime(d(ii).date,'Locale','no_NO') <= datetime(t_end)
            index = [index ii];
        end
    end

    numScenarios     = length(index);
    dd               = d(index);
    algClassNames    = 'VOTING_ALG_OBJ()';
    algShortNames    = getAlgorithmShortNamesFromClassName(algClassNames);
    flowIOname       = ['flowIO_lpm_',algShortNames];
    volIOname        = ['volIO_m3_',algShortNames];
    alarmStateName   = ['alarmState_', algShortNames];
    alarmTypeName    = ['alarmType_', algShortNames];
    evaTable         = zeros(numScenarios,9);
    alarmTypes       = enumeration('AlarmType'); % types of alarms
    nAlarmTypes      = size(alarmTypes,1); % number of alarm types
    alarmTypeTable   = zeros(numScenarios,nAlarmTypes);
    res              = struct();
    res.fIO          = struct();
    res.vIO          = struct();
    res.aS           = struct();
    res.aT           = struct();

    %Part 1: evaluation calculations
    for scenarioIdx=1:numScenarios
        disp(['Item no: ' num2str(scenarioIdx,'%.0f')]);
        plantName       = dd(scenarioIdx).name;
        k               = strfind(plantName,'_');
        simName         = plantName(1:k(1)-1);
        alarmTypeCount  = [];
        evaluateResults = [];

        if strcmp(simName, 'SimRig-1')
            td              = readTrueFileFromZip(fullfile('simData', 'SimRig-1.zip'));
        elseif tripdata
            td              = readTrueFileFromZip(fullfile('fieldDataTripping', sprintf('%s.zip', simName)));
        else
            td              = readTrueFileFromZip(fullfile('fieldData', sprintf('%s.zip', simName)));
        end

        evalObj          = EVALUATE_OBJ(algShortNames, plantName);
        evalObj          = evalObj.initialize();
        ff  = dir([ROOT_DIR plantName]);
        Nf=size(ff,1);
        sf=zeros(Nf,1);
        for i=1:Nf
            sf(i)=ff(i).bytes;
        end
        if max(sf) == 0
            disp(['All files empty in ' ROOT_DIR plantName])
            continue;
        end
        mf=find(sf==max(sf));        
        fileName = [ROOT_DIR plantName '\' ff(mf).name];
        opts                       = detectImportOptions(fileName);
        opts.SelectedVariableNames = {flowIOname,volIOname,alarmStateName,alarmTypeName};

        T    = readtable(fileName,opts);
        Tt   = readtable(fileName);
        Tt   = Tt(:,1);
        Nt   = size(Tt,1);
        tvec = zeros(Nt,1);
        for i=1:Nt
            s       = Tt{i,1}{1};
            t_unix  = str2double(s(3:end));
            tvec(i) = t_unix/86400 + 719529;
        end
        res.fIO.T = tvec;
        res.vIO.T = tvec;
        res.aS.T  = tvec;
        res.aT.T  = tvec;
        res.fIO.V = zeros(Nt,1);
        res.vIO.V = zeros(Nt,1);
        res.aS.V  = zeros(Nt,1);
        res.aT.V  = zeros(Nt,1);
        for i=1:Nt
            res.fIO.V(i) = T{i,1};
            res.vIO.V(i) = T{i,2};
            res.aS.V(i)  = T{i,3};
            res.aT.V(i)  = T{i,4};
        end
        
        if isempty(td)
            [evalObj, alarmTypeCount] = evalObj.evaluate(0, 0, res.fIO, res.vIO, res.aS, res.aT);
        else
            ignoreStruct = evalObj.determineIfIgnoreStructIsAvailable(td);
            [evalObj, alarmTypeCount] = evalObj.evaluate(ignoreStruct, td.volIO, res.fIO, res.vIO, res.aS, res.aT);
        end
        comment = [comment, algShortNames,'|',evalObj.resultTxt];
        
        evaluateResults = evalObj.generateOutput;
                   
        % headers for evaluate table: kpi, nCorrectAlarms, nFalseAlarms,
        % nTrueAlarms, algUpTimeInHours, simDuration, algLateAlarms
        
        if ~isempty(evaluateResults)
            evaTable(scenarioIdx, 1) = evaluateResults.keyPerformanceIndex;
            evaTable(scenarioIdx, 2) = evaluateResults.algNoOfCorrectAlarms;
            evaTable(scenarioIdx, 3) = evaluateResults.algNoOfFalseAlarms;
            evaTable(scenarioIdx, 4) = evaluateResults.trueNoOfAlarms;
            evaTable(scenarioIdx, 5) = evaluateResults.algUpTimeInHours;
            evaTable(scenarioIdx, 6) = evaluateResults.durationInHours;
            evaTable(scenarioIdx, 7) = evaluateResults.algLateAlarms;
            evaTable(scenarioIdx, 8) = evaluateResults.timeToDetection;
            evaTable(scenarioIdx, 9) = evaluateResults.accumulatedVolumeAtDetection;
        else
            fprintf(1,'No evaluate result available for %s\n',simName);
        end
        
        if ~isempty(alarmTypeCount)
            alarmTypeTable(scenarioIdx,:)=alarmTypeCount';
        else
            fprintf(1,'No count of alarm types available for %s\n',simName);
        end       
    end
    
    % statistics from evaluate for all simulations
    totalFAR = sum(evaTable(:,3))/sum(evaTable(:,5)); % number of false alarms per active algorithm hour
    if numScenarios > 0
        meanKPI = sum(evaTable(:,1))/numScenarios;
    else
        meanKPI = NaN;
    end
    totalPD = sum(evaTable(:,2))/sum(evaTable(:,4))*100; % probability of detection in percent for all data sets    
    
    %Part 2: store evaluations to file
    tm = clock;
    fname = ['.\Evaluation Results\Evaluation_' date '-' num2str(tm(4),'%2.0f') '-' num2str(tm(5),'%2.0f') '-' num2str(tm(6),'%2.0f') '.csv'];
    fid = fopen(fname,'W');

    fprintf(fid, '\nStatistics after running %0.0f data sets:\n', numScenarios);
    fprintf(fid, 'Total simulation time: %0.2f hours; alg up-time: %0.2f hours (%0.2f%%)\n',...
        sum(evaTable(:,6)), sum(evaTable(:,5)), sum(evaTable(:,5))/sum(evaTable(:,6))*100);
    fprintf(fid, 'KPI: %0.2f; FAR: %0.2f alarms/active hour; PD: %0.2f%%\n', meanKPI, totalFAR, totalPD);
    fprintf(fid, 'Results for all simulations:\n');
    fprintf(fid, 'Time series; KPI; #correctAlarms; #falseAlarms; #trueAlarms; algUpTime [h]; sim duration [h]; #lateAlarms; TD [s]; VD [m3]\n');

    for scenarioIdx = 1:numScenarios
        plantName       = dd(scenarioIdx).name;
        k               = strfind(plantName,'_');
        simName         = plantName(1:k(1)-1);
        fprintf(fid, '%-20s; %0.2f; %g; %g; %g; %0.2f; %0.2f; %g; %0.2f; %0.2f\n',...
            simName, evaTable(scenarioIdx,1), evaTable(scenarioIdx,2), evaTable(scenarioIdx,3),...
            evaTable(scenarioIdx,4), evaTable(scenarioIdx,5), evaTable(scenarioIdx,6), ...
            evaTable(scenarioIdx,7), evaTable(scenarioIdx,8), evaTable(scenarioIdx,9));
    end
    fprintf(fid,'\n');
     % print count of alarm type to file
    fprintf(fid,'\nType of false alarms and warnings which have been issued:\n');
    hstr = 'Time series';
    fprintf(fid,'%-20s; ',hstr);

    for kk=1:nAlarmTypes
        fprintf(fid,'%-30s; ', char(alarmTypes(kk)));
    end
    fprintf(fid,'\n');

    for scenarioIdx = 1:numScenarios
        plantName       = dd(scenarioIdx).name;
        k               = strfind(plantName,'_');
        simName         = plantName(1:k(1)-1);        
        fprintf(fid,'%-20s; ', simName);
        for kk=1:nAlarmTypes
            fprintf(fid,'%-30g; ', alarmTypeTable(scenarioIdx,kk));
        end
        fprintf(fid,'\n');
    end

    fprintf(fid,'%-20s; ', 'Sum');
    for kk=1:nAlarmTypes
        fprintf(fid,'%-30g; ', sum(alarmTypeTable(:,kk)));
    end
    fprintf(fid,'\n\n');
    fclose(fid);
end