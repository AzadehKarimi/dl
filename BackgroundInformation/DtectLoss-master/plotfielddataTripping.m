function plotfielddataTripping

close all
root_dir = 'fieldDataTripping\';

dataFiles = returnEveryFileInDirectoryTree(root_dir,'.mat');

for curFile = 1:1:numel(dataFiles)
   
%    profile off;
%    profile on;
     load([root_dir dataFiles{curFile}]);
     retCell = outData;
%    profile off;
%    profile viewer;
%    pause
    
    variables_to_plot{1}    = {retCell.depthBit.t,     retCell.depthBit.V,      'depthBit'};
    variables_to_plot{2}    = {retCell.depthHole.t,    retCell.depthHole.V,     'depthHole'};
    variables_to_plot{3}    = {retCell.volTripTank.t,  retCell.volTripTank.V,   'volTripTank'};
    variables_to_plot{4}    = {retCell.volTripTank2.t, retCell.volTripTank2.V,  'volTripTank2'};
    variables_to_plot{5}    = {retCell.flowIn.t,       retCell.flowIn.V,        'flowIn'};
        
    figure(curFile)
    ax(1) = subplot(311);
    suptitle(dataFiles{curFile});
    plot(retCell.depthBit.t,     retCell.depthBit.V,retCell.depthHole.t,    retCell.depthHole.V,'linewidth',2);
    ylabel('Depth [m]')
    legend('bit','Hole');
    ax(2) = subplot(312);
    plot(retCell.volTripTank.t,     retCell.volTripTank.V,retCell.volTripTank2.t,    retCell.volTripTank2.V,'linewidth',2);
    ylabel('VolTripTank [m3]')
    legend('1','2');
    ax(3) = subplot(313);
    plot(retCell.flowIn.t,     retCell.flowIn.V,'linewidth',2);
    ylabel('flowIn [lpm]')
    
    for ii=1:numel(ax)
        grid(ax(ii));
        datetick(ax(ii),'x');
        zoom(ax(ii),'on');
    end
    linkaxes(ax,'x');
    axis(ax, 'tight');
    
    disp('PAUSED, press any key to open next file....' );
    pause
     
    
end

