# DtectLoss

Repository contains an algorithm for automatic loss/kick detection during 
drilling operations, developed as part of Stoploss project initated 2016.

## Backtesting with Matlab algorithms

Run runSimtest to analyze the datasets.
 - rumSimtest defaults to include datalogging
 - runSimtest(0,0) turns datalogging off to increase speed

Change between fieldData and simData in runSimtest.

Sigma plotter can be deactivated as it is a Equinor proprietary tool.

This backtesting will use InfluxDB to store the analysis results.

Make sure the MatlabEKD database has been created before setting datalogging to true.

Use the SHOW DATABASES in the Influx command line (dockerbinbash -> influx) to verify it has been created.
