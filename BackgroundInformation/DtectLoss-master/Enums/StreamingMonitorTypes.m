classdef StreamingMonitorTypes < int32
    %DRILLINGSTATES States which represents the basic drilling state
    %flow.
    
enumeration
    undefined (0)
    historicFromTsCollect (1)
    liveOverWITSML (2)
end
    
end

