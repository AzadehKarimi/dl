classdef CirculationStates < int32
    %CIRCULATIONSTATES States which represents the circulation system.
    
    enumeration
        notCirculating (0)
        circulating (1)
        drilling (2)
    end
    
end