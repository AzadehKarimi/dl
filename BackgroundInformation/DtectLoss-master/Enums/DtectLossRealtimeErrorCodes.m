classdef DtectLossRealtimeErrorCodes < int32
    %ALARMSTATE States which represent possible alarm modes.
    
    enumeration
        Undefined(0)
        AllOk (1)
        
        NoUnit_flowOut (-1)
        NoUnit_flowIn(-2)
        NoUnit_rpm(-3)
        NoUnit_presSP(-4)
        NoUnit_depthHole(-5)
        NoUnit_depthBit(-6)
        NoUnit_volGainLoss(-7)
        NoUnit_volTripTank(-8)
        NoUnit_tempBHA(-9)
        NoValue_flowOut(-11)
        NoValue_flowIn(-12)
        NoValue_rpm(-13)
        NoValue_presSP(-14)
        NoValue_depthHole(-15)
        NoValue_depthBit(-16)
        NoValue_volGainLoss(-17)
        NoValue_volTripTank(-18)
        NoValue_tempBHA(-19)

        NoUnit_heightHook (-50)
        NoUnit_tvdBit(-51)
        NoUnit_torque(-52)
        NoUnit_densityIn(-53)
        NoUnit_densityOut(-54)
        NoUnit_tempIn(-55)
        NoUnit_tempOut(-56)
        NoUnit_densECDMWD(-57)
        NoUnit_densECDMEM(-58)
        NoUnit_presBHA(-59)
        NoUnit_heaveRig(-60)
        NoUnit_weightOnBit(-61)
        NoUnit_weightHookload(-62)
        NoUnit_activityBHI(-63)
        NoUnit_activitySLB(-64)
        NoUnit_activityHAL(-65)

        NoValue_heightHook (-80)
        NoValue_tvdBit(-81)
        NoValue_torque(-82)
        NoValue_densityIn(-83)
        NoValue_densityOut(-84)
        NoValue_tempIn(-85)
        NoValue_tempOut(-86)
        NoValue_densECDMWD(-87)
        NoValue_densECDMEM(-88)
        NoValue_presBHA(-89)
        NoValue_heaveRig(-90)
        NoValue_weightOnBit(-91)
        NoValue_weightHookload(-92)
        NoValue_activityBHI(-93)
        NoValue_activitySLB(-94)
        NoValue_activityHAL(-95)

        BoolAsDoubleDifferentFromZeroOrOne_selectpresSP(-100)
        BoolAsDoubleDifferentFromZeroOrOne_validvolTripTank(-101)
        BoolAsDoubleDifferentFromZeroOrOne_validflowOut(-102)
        BoolAsDoubleDifferentFromZeroOrOne_validflowOut1(-103)
        BoolAsDoubleDifferentFromZeroOrOne_validrop(-104)
        BoolAsDoubleDifferentFromZeroOrOne_validvolPitDrill(-105)
        BoolAsDoubleDifferentFromZeroOrOne_validdepthBit(-106)
        BoolAsDoubleDifferentFromZeroOrOne_validvelBlock(-107)
        BoolAsDoubleDifferentFromZeroOrOne_validdepthHole(-108)
        BoolAsDoubleDifferentFromZeroOrOne_validrpm(-109)
        BoolAsDoubleDifferentFromZeroOrOne_validflowIn(-110)
        BoolAsDoubleDifferentFromZeroOrOne_validpresSP(-111)
        BoolAsDoubleDifferentFromZeroOrOne_validpresSP1(-112)
        BoolAsDoubleDifferentFromZeroOrOne_selectflowout(-113)
        
        MissingUnitNonSpecfic(-1000) % if no specific error tag is defined, revert to this
        MissingValueNonSpecfic(-1001) % if no specific error tag is defined, revert to this
        MissingAllValues(-1002)
        
        % warnings: postive numbers
    end
    
end
