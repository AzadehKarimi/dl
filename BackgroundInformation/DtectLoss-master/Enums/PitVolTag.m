classdef PitVolTag < int32
    % The tag to be used for pit volume calculations for a particular
    % scenario
    
    enumeration
        Error (0)
        volGainLoss (1) 
        NA (2) % not availiable
    end
    
end
