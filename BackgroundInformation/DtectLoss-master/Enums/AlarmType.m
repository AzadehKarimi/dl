classdef AlarmType < int32
    %ALARMTYPE which represent possible alarm types.
    
    enumeration
        TripOnlyLossAlarm           (-7)
        PitOnlyLossAlarm            (-6)
        FlowOnlyLossAlarm           (-5)
        FlowAndPressureLossAlarm    (-4)
        FlowAndPitLossAlarm         (-3)
        PitAndPressureLossAlarm     (-2)

        LossWarning                 (-1)
        Normal                      ( 0)
        InfluxWarning               (+1)

        PitAndPressureInfluxAlarm   (+2)
        FlowAndPitInfluxAlarm       (+3)
        FlowAndPressureInfluxAlarm  (+4)
        FlowOnlyInfluxAlarm         (+5)
        PitOnlyInfluxAlarm          (+6)
        TripOnlyInfluxAlarm         (+7)
		PitSlowGainAlarm            (+8)
    end
    
end

