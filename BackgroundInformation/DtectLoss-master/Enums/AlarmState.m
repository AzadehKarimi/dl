classdef AlarmState < int32
    %ALARMSTATE States which represent possible alarm modes.
    
    enumeration
        InputError(-2)
        Disabled (-1)
        Normal (0)
        Warning (1)
        Alarm (2)
    end
    
end
