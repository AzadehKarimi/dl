classdef SimulatorSolvers < int32
    %SimulatorSolvers: set the desired solver to be used in SIMULATOR_OBJ
    
enumeration
    ODE45 (0)
    FORWARD_EULER (1)
end
end

