classdef activityCodesSLB < int32
    %activityCodesSLB States used by Schlumberger
    
    enumeration
        
        Undefined (0) % Anything not covered by other activity codes.
        RigUpandTearDown (1) % The act of installing and removing the rig. The start and end of these operations will be specified by the operator.
        Drilling (2) % The process of making new hole.
        ConnectionWhileDrilling (3) % The process of adding a joint of drillpipe to the string.
        Reaming (4) % Return to total depth with pumps on, pipe rotating and/or weight on bit.
        HoleOpening (5) % Act of opening pilot hole to full gauge.
        Coring (6) % Act of cutting a conventional core.
        ConditionAndCirculateMud (7) % Pumps on with pipe turning slowly and/or some pipe reciprocation and/or no weight on bit and no increase in total depth.
        TrippingIn (8) % The act of running the drillstring into the hole.
        TrippingOut (9) % The act of pulling the drillstring from the hole.
        LubricateRig (10) % Regularly scheduled service or preventative maintenance which interrupts the current rig activity.
        RigRepair (11) % An unscheduled interruption to the current rig activity resulting from equipment failure, e.g. blown pop-off valve, blown swivel packing, dropping blocks etc.
        CutSlipDrillingLine (12) % The act of cutting and slipping the drilling line.
        DeviationSurvey (13) % The activity associated with taking a survey.
        WirelineLogs (14) % Time spent rigging up, running, and rigging down wireline tools.
        RunCasing (15) % The time from rigging up to run casing/liner until casing/liner is in position.
        Cementing (16) % The time spent on cementing operations.
        PlugBack (17) % The time spent setting a cement plug. This does not include tripping.
        SqueezeCementing (18) % The time spent performing a cement squeeze job. This does not include tripping.
        WaitOnCement (19) % The time required for the cement to gain sufficient strength to drillout, or the time required by a regulatory agency after the cement plug is bumped. This time will be supplied by the operator.
        DrillCementAndOrFloatEquipment (20) % Drilling cement, float collar and/or casing shoe following a cement job, plug back, sidetrack etc.
        NippleUpNipple (21) % Down BOP StackThe act of installing or removing the BOP stack. This time will be recorded only if it exceeds the wait-on-cement time.
        TestBOP (22) % Regularly scheduled BOP test after casing, etc.
        DrillStemTest (23) % The time spent drill stem testing. This does not include tripping the test tool.
        Fishing (24) % The time spent attempting to fish items from the hole. This does not include tripping.
        DirectionalWork (25) % The time spent performing directional work (not simply a survey). This does not include tripping.
        WellControl (26) % Well kill and associated operations, e.g. observe pressures, weight up mud, circulate on choke, etc.
        StuckPipe (27) % Act of freeing pipe which cannot be pulled by normal processes.
        WaitOnWeather (28) % Waiting caused by adverse weather conditions.
        Subsea (29) % Run/Pull Riser, stack, wear bushing, etc.
        FlowCheck (30) % Check for fluid flow from well while not pumping
        PressureIntegrityTest (31) % Pressure test on the casing or a "leak-off" test on exposed formation.
        LostCirculation (32) % Time spent combatting lost circulation problems
        ShortTripIn (33) % The act of running the drillstring back to bottom after a short trip.
        ShortTripOut (34) % The act of pulling the drillstring for a short trip.
        
    end
end
