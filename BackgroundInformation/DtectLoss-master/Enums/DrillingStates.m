classdef DrillingStates < int32
    %DRILLINGSTATES States which represents the basic drilling state
    %flow.
    
enumeration
    %notCirculating (0)
    %circulating (1)
    %drilling (2)
    BadData	(-1)
    Other (0)
    Drilling (1) %RotaryDrilling and Sliding, on bottom
    FlowCheck (2) %Testing
    Conditioning (3) %Circulate bottoms up etc
    Tripping (4) %Washing/reaming/working pipe
    %Connection (60) %ToDo - from pulling off bottom to on bottom drilling
end
    
end

%Rig states us patent 6892812
% 1. Rotary drilling
% 2. Sliding
% 3. Testing state: 
%    a) Flow check on bottom
%    b) Flow check off bottom
%    c) Parameter check
% 4. Conditioning state: 
%    a) Bottom hole conditioning
%    b) Circulating off bottom
% 5. Tripping state: 
%    a) Tripping in hole
%    b) Reaming while tripping in hole
%    c) Working pipe while tripping in hole
%    d) Washing while tripping in hole
%    e) Back reaming while tripping out of hole
%    f) Working pipe while tripping out of hole
%    g) Washing while tripping out of hole.

%Booleans:
% 1. Drilling bit is on bottom
% 2. Drilling fluid is circulating
% 3. Bit position is constant
% 4. Drill pipe in slips state
% 5. Slip and cut line state
% 6. Mechanical and hydraulic data is valid

%    State                  OnBottom    Circulating	ConstBitPos	InSlips	Rotating	Up	Down	ConstFlowIn
% 	    -1	BadData								
% 	    0	Other								
% 1.	1	RotaryDrilling	Yes         Yes         Any         No      Yes         No	Any     Yes
% 2.	2	Sliding	Yes	Yes	Any	No	No	No	Any	Yes
% 3.	3	FlowCheck	Any	No	Yes	No	No	No	No	Yes
% 3.a.	31	FlowCheckOnBottom	Yes	No	Yes	No	No	No	No	Yes
% 3.b.	32	FlowCheckOffBottom	No	No	Yes	No	No	No	No	Yes
% 3.c.	33	ParameterCheck	Yes	Any	Yes	No	Any	No	No	Any
% 4.a.	41	BottomHoleConditioning	Any	Yes	Yes	No	Any	Any	Any	Yes
% 4.b.	42	CirculatingOff Bottom	No	Yes	Yes	No	No	No	No	Yes
% 5.	50	Tripping	No	Any	No	No	No	Any	Any	Any
% 5.a.	51	TrippingIn	No	Any	No	No	No	No	Yes	Any
% 5.b.	52	ReamingIn	No	Yes	No	No	Any	No	Yes	Any
% 5.c.	53	TripInWorkingPipe	No	Any	No	No	Yes	No	Yes	Any
% 5.d.	54	TripInWashing	No	Yes	No	No	Any	No	Yes	Any
% 5.e.	55	BackReaming	No	Yes	No	No	Any	Yes	No	Any
% 5.f.	56	TripOutWorkingPipe	No	Any	No	No	Yes	Yes	No	Any
% 5.g.	57	TripOutWashing	No	Any	No	No	No	Yes	No	Any
% 	58	TrippingOut	No	Any	No	No	No	Yes	No	Any
% 	59	Reaming	No	Yes	No	No	Any	Any	Any	Yes
% 	60	Connection	No	Any	Yes	Any	No	No	No	No
