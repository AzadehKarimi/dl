classdef TripTankTag < int32
    % The tag to be used for trip tank calculations for a particular
    % scenario
    
    enumeration
        Error (0)
        volTripTank    (1) 
        volTripTankIn  (2)
        volTripTankOut (3)
        NA (4) % not avialiable
    end
    
end
