% ----------------------------------------- 
% streams data to a bank of algorithms,
% against against logged field data,

function [evaUkf, evaPdiff, evaSimple]=runCommitTest( )
% to call method in jar on dynamic path: package.myclass.mymethod(params)
% Constants
FIELD_DATA_ROOT_DIR         = 'simData';
DO_PROFILER                 = 0;
IS_DEBUG_MODE               = 0;
doWrapperTest               = 0;

backTestAlgorithmsOnCatalog(FIELD_DATA_ROOT_DIR,DO_PROFILER,IS_DEBUG_MODE, doWrapperTest);
%%
variableNameVec = {'depthBit_m_wash','depthHole_m_wash','velRop_mph_wash','velBlock_mph_wash','flowIn_lpm_wash','flowOut_lpm_wash',...
    'presSP_bar_wash','presSP_bar_ukf','alarmState_pdiff','alarmState_simple','alarmState_ukf','volIO_m3_pdiff','volIO_m3_simple','volIO_m3_ukf',...
    'betaDivByV_e_bar_per_m3_pdiff','betaperVol_barm3_ukf','flowIO_lpm_pdiff','flowIO_lpm_simple','flowIO_lpm_ukf','presSP_bar_wash',...
    'volGainLossWithoutOffset_m3_wash','volPit_m3_ukf'};
queryServerURL  = 'ws://localhost:4651/QueryService';

fromTime_matlab = datenum('2016-01-01 01:00:01');
toTime_matlab   = datenum('2016-01-01 05:25:59');

% get sim data
sd=getHistoricalDataFromWebsocketDB(variableNameVec,plantName,fromTime_matlab,toTime_matlab,queryServerURL);
% get true data
td=readTrueFileFromZip(fullfile('simData', 'SimRig-1.zip'));

evaUkf=EVALUATE_OBJ('UKF', 'SimData');
evaPdiff=EVALUATE_OBJ('pdiff', 'SimData');
evaSimple=EVALUATE_OBJ('simple', 'SimData');

volThresWarning=0.8;
volThresAlarm=1;
flowRateThres=1000;
detGracePer=120; % [s] 

evaUkf=evaUkf.initialize(volThresWarning, volThresAlarm, flowRateThres, detGracePer);
evaPdiff=evaPdiff.initialize(volThresWarning, volThresAlarm, flowRateThres, detGracePer);
evaSimple=evaSimple.initialize(volThresWarning, volThresAlarm, flowRateThres, detGracePer);

%dbstop if error
evaUkf=evaUkf.evaluate(td.flowIO, td.volIO, sd.flowIO_lpm_ukf, sd.volIO_m3_ukf, sd.alarmState_ukf);
evaPdiff=evaPdiff.evaluate(td.flowIO, td.volIO, sd.flowIO_lpm_pdiff, sd.volIO_m3_pdiff, sd.alarmState_pdiff);
evaSimple=evaSimple.evaluate(td.flowIO, td.volIO, sd.flowIO_lpm_simple, sd.volIO_m3_simple, sd.alarmState_simple);