
% fprintf('test %s', 'test');

% Build an InfluxDB client
% URL = 'http://localhost:8086';
% USER = 'admin';
% PASS = 'admin';
% DATABASE = 'ekd';
% influxdb = InfluxDB(URL, USER, PASS, DATABASE);

% Check the status of the InfluxDB instance
%[ok, ping] = influxdb.ping()

% Show the databases
%dbs = influxdb.databases()


% series1 = Series('weather') ...
% .tags('city', 'barcelona', 'country', 'catalonia') ...
% .fields('temperature', 24.3, 'humidity', 70.4) ...
% .time(datetime('today', 'TimeZone', 'local'));
% influxdb.writer().append(series1).execute();

% Create a series with many samples
series2 = Series('weather') ...
    .tags('city', 'copenhagen', 'country', 'denmark') ...
    .fields('temperature', [12.6; 11.8], 'humidity', [45.7; 46.3]) ...
    .time(datetime('now', 'TimeZone', 'local') - [0; 1] / 24);

influxdb.runCommand('CREATE DATABASE "MatlabEKD"', true)

influxdb.use('MatlabEKD');