
function  [returnCodeVec, KPIvec, PDVec, FARVec, numberOfScenariosRunVec, totalSimDurationVec] ...
            = runBacktestwithSeveralParameterSets(buildNr, ParameterVector)
    global ParSet 
    doSigmaPlotter           = 0;
    fclose all;
    doSigmaPlotter           = 0;
    returnCodeVec            = [];returnCode=[];
    KPIvec                   = [];KPI=[];
    PDVec                    = [];PD=[];
    FARVec                   = [];FAR=[];
    KPISvec                  = [];
    PDSVec                   = [];
    FARSVec                  = [];
    numberOfScenariosRunVec = [];numberOfScenariosRun=[];
    totalSimDurationVec     = [];totalSimDuration_s=[];
    
    doBackTest              = true;
    doSimTest               = true;
    
    SIGMAcleanup();
    
    for ii = 1: length(ParameterVector)
        ParSet = ParameterVector(ii);
        if doBackTest
            [returnCode, KPI,PD,FAR,numberOfScenariosRun,totalSimDuration_s] =  runBacktest(buildNr, doSigmaPlotter);
            %[returnCode, KPI,PD,FAR,numberOfScenariosRun,totalSimDuration_s] =  runSingletest(buildNr, doSigmaPlotter);
            SIGMAcleanup();
            returnCodeVec           = [returnCodeVec, returnCode];
            KPIvec                  = [KPIvec, KPI];
            PDVec                   = [PDVec, PD];
            FARVec                  = [FARVec, FAR];
            numberOfScenariosRunVec = [numberOfScenariosRunVec, numberOfScenariosRun];
            totalSimDurationVec     = [totalSimDurationVec, totalSimDuration_s];
        end
        if doSimTest
            [returnCodeS, KPIS,PDS,FARS,numberOfScenariosRunS,totalSimDurationS_s] =  runSimtest(buildNr, doSigmaPlotter);
            SIGMAcleanup();
            KPISvec                  = [KPISvec, KPIS];
            PDSVec                   = [PDSVec, PDS];
            FARSVec                  = [FARSVec, FARS];
        end
    end
    fprintf('\nStatistics after running %0.0f data sets and %.0f parameter sets:\n', numberOfScenariosRun, length(ParameterVector));
    fprintf('Total simulation time: %0.2f hours\n', sum(totalSimDurationVec)/3600);
    if doBackTest && doSimTest
        for ii = 1: length(ParameterVector)
            fprintf('Parameter set %.0f: KPI=%0.2f, FAR=%0.2f alarms/active hour, PD=%0.2f%%. SimData: KPI=%0.2f, FAR=%0.2f alarms/active hour, PD=%0.2f%%\n', ...
                ParameterVector(ii), KPIvec(ii), FARVec(ii), PDVec(ii), KPISvec(ii), FARSVec(ii), PDSVec(ii));
        end
    elseif doBackTest
        for ii = 1: length(ParameterVector)
            fprintf('Parameter set %.0f: KPI=%0.2f, FAR=%0.2f alarms/active hour, PD=%0.2f%%\n', ...
                ParameterVector(ii), KPIvec(ii), FARVec(ii), PDVec(ii));
        end
    elseif doSimTest
        for ii = 1: length(ParameterVector)
            fprintf('Parameter set %.0f SimData: KPI=%0.2f, FAR=%0.2f alarms/active hour, PD=%0.2f%%\n', ...
                ParameterVector(ii), KPISvec(ii), FARSVec(ii), PDSVec(ii));
        end
    end
    fclose all;
end

function SIGMAcleanup()
    killProcessIfRunning('SIGMAloggingserver.exe');
    killProcessIfRunning('SIGMAqueryserver.exe');
    killProcessIfRunning('SIGMAtagServer.exe');
    killProcessIfRunning('SIGMAloggingclient.exe');
end
