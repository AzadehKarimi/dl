function [EXITCODE, jarFilePath] = buildjarforwrapper(doExitWhenFinished, id_str)

if nargin ~= 2
    fprint('Not enough input parameters.\n')
    EXITCODE = -1;
    jarFilePath = [];
    return
end

try
    setenv('MCC_USE_DEPFUN', '1'); %% do this or the upgrade will hang...
    directoryDetectloss = 'DTECTLOSS_REALTIME_JAVA';
    directoryEvaluate = 'EVALUATE_JAVA';
    cleanOldAndMakeNewDirectory(directoryDetectloss);
    cleanOldAndMakeNewDirectory(directoryEvaluate);
    
    rehash();
    
    jarFilePath = '';
    EXITCODE = 0;
    thandle = tic;
    
    disp('mcc requires the JDK:');
    version -java
    tmp2 = getenv('JAVA_HOME');
    disp(['JAVA_HOME (where the JDK is expected found):', tmp2]);
    disp('calling mcc to compile jar, this step may take several minutes... ')
    
    % note that Matlab 2012b needs "-Y network_r2012b.lic " otherwise it
    % hangs for 20 minutes.....
    
    disp('Build jar file for DTECTLOSS');
    mcc -Y network_r2012b.lic -N -v -W 'java:DTECTLOSS,DTECTLOSS_REALTIME_OBJ' -d 'DTECTLOSS_REALTIME_JAVA'  ...
        ..\functions\DTECTLOSS_REALTIME_OBJ_init.m ...
        ..\functions\DTECTLOSS_REALTIME_OBJ_stepAlgorithm.m...
        ..\functions\DTECTLOSS_REALTIME_OBJ_getOutputFormat.m...
        ..\functions\DTECTLOSS_REALTIME_OBJ_softreset.m...
        ..\functions\DTECTLOSS_REALTIME_OBJ_reset.m...
        ..\functions\DTECTLOSS_REALTIME_OBJ_updateTagList.m...
        ..\functions\DTECTLOSS_REALTIME_OBJ_setAreadp.m...
        ..\Classes\VOTING_ALG_OBJ.m
    
    disp('Build jar file for EVALUATE');
    mcc -Y network_r2012b.lic -N -v -W 'java:EVALUATE,EVALUATE_OBJ' -d 'EVALUATE_JAVA' ...
        'EVALUATE_OBJ_init.m' ...
        'EVALUATE_OBJ_evaluate.m' ...
        'EVALUATE_OBJ_generateOutput.m'
    
    disp('mcc done. Building zip files...');
    zip_file_name_1 = ['DTECTLOSS_REALTIME_JAVA_', id_str, '.zip'];
    disp(['building ', zip_file_name_1]);
    zip(zip_file_name_1, {directoryDetectloss, directoryEvaluate, [pwd, '\..\functions\defaultUnitsMapping.csv']})
    
    jarFilePath = ['codegen\', zip_file_name_1];
    
    fprintf(1, 'mcc took %f seconds to build jar', toc(thandle));
catch ME
    disp([ME.identifier, ':', ME.message]);
    
    if strcmp(ME.identifier, 'MATLAB:mcc_err_checkout_failed' )
        EXITCODE = -2;
    else
        EXITCODE = -1;
    end
end
if doExitWhenFinished
    exit(EXITCODE)
end

    function cleanOldAndMakeNewDirectory(directoryName)
        if( exist(directoryName,'dir') == 7 )
            rmdir(directoryName,'s');
        end
        mkdir(directoryName);
        
    end

end