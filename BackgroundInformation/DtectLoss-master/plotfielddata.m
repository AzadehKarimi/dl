 function plotfielddata

 opengl software;
 
root_dir = 'fieldData\';

dataFiles = returnEveryFileInDirectoryTree(root_dir,'.zip');

for curFile = 1:1:numel(dataFiles)
   
%    profile off;
%    profile on;
    retCell                 = readDataFromZip(dataFiles{curFile});
%    profile off;
%    profile viewer;
%    pause
    
    new_subplot_config      = {{1,2},{3};{4,5},{6}};
    variables_to_plot{1}    = {retCell.depthBit.t,  retCell.depthBit.V,      'depthBit'};
    variables_to_plot{2}    = {retCell.depthHole.t, retCell.depthHole.V,    'depthHole'};
    variables_to_plot{3}    = {retCell.velRop.t,    retCell.velRop.V,          'velRop'};
    variables_to_plot{4}    = {retCell.flowIn.t,    retCell.flowIn.V,          'flowIn'};
    variables_to_plot{5}    = {retCell.flowOut.t,   retCell.flowOut.V,        'flowOut'};
    variables_to_plot{6}    = {retCell.presSP.t,    retCell.presSP.V,          'presSP'};
    
    fig_handles{1}          = new_subplot(new_subplot_config, variables_to_plot{:}   );
    suptitle(dataFiles{curFile});
    
    disp('PAUSED, press any key to open next file....' );
    pause
    try
        close(fig_handles{1});
    catch
    end
    
    
end

