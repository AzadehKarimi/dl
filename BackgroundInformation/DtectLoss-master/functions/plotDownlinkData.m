function plotDownlinkData(folderName, matFileName)

load(fullfile(folderName, matFileName));

figure;
ax(1) = subplot(311);
plot(simTime, X(:, 1));
ax(2) = subplot(312);
plot(simTime, X(:, 2), simTime(boolean(X(:, 6))), X(boolean(X(:, 6)), 2), 'r.');
ax(3) = subplot(313);
plot(simTime, X(:,6));

linkaxes(ax, 'x');
axis tight
zoom on;

end

