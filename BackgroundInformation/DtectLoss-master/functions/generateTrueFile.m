function [fid, status]= generateTrueFile( folderName, matName, cursorPos, ioVol, timeOffset )
%GENERATETRUEFILE make a true-file to accompany field data. To be used with
%the evaluate function of the EVALUATE_OBJ.
%   NA

if nargin<6
    timeOffset=-1/24;
end

load(fullfile(folderName,matName));

startTime=outData.presSP.t(1);
endTime=outData.presSP.t(end);
oneSec=1/86400; % [day]
dt=2; % [s] time step to use when generating in/outflux.

fileName=strcat('true', matName(1:end-4), '.csv');
fid=fopen(fullfile(folderName, fileName),'w');

volIO=0; % [m3]

if cursorPos(1).Position(1) > cursorPos(2).Position(1)
    ioStartTime = cursorPos(2).Position(1);
    ioEndTime = cursorPos(1).Position(1);
else
    ioStartTime = cursorPos(1).Position(1);
    ioEndTime = cursorPos(2).Position(1);
end

ioFlowRate = ioVol*1000 / (ioEndTime - ioStartTime) / (24*60);

try
    fprintf(fid, 'flowIO;flowIO;volIO;volIO\n');
    fprintf(fid, 'INDEX;VALUE;INDEX;VALUE\n');
    fprintf(fid, 'time;lpm;time;m3\n');
    t=genZuluTimeString(startTime, timeOffset);
    fprintf(fid, '%s;0;%s;0\n', t,t);
    t=genZuluTimeString(ioStartTime-oneSec, timeOffset);
    fprintf(fid, '%s;0;%s;0\n', t,t);
    for t=ioStartTime:dt*oneSec:ioEndTime
        to=genZuluTimeString(t,timeOffset);
        fprintf(fid, '%s;%g;%s;%g\n', to, ioFlowRate, to, volIO);
        volIO=volIO+dt*ioFlowRate/60000;
    end
    t=genZuluTimeString(ioEndTime+oneSec, timeOffset);
    fprintf(fid, '%s;0;%s;0\n', t,t);
    t=genZuluTimeString(endTime, timeOffset);
    fprintf(fid, '%s;0;%s;0\n', t,t);
catch ME
    getReport(ME)
    status=fclose(fid); %#ok<NASGU>
end

status=fclose(fid);

end

function ts=genZuluTimeString(t,tOffset)
% generate a time string in proper format, from serial time.
ds=datevec(t+tOffset);
ts=sprintf('%04g-%02g-%02gT%02g:%02g:%02gZ',ds);
end