function outData=plotPitData(folderName, fileName)
% PLOTCSVDATA plots field data in csv-files. The csv-files can be zipped.

% usage: plotCsvData('a_folder') - plots all zipped csv files in directory
% usage: plotCsvData('a_folder', '*.csv') - plots all csv files in directory
% usage: plotCsvData('a_folder', 'a_file.zip') - plots the zipped csv-file
% in a_file.zip
% usage: plotCsvData('a_folder', 'a_file.csv') - plots a_file.csv
% usage: plotCsvData('a_folder', 'a_file.mat') - plots results in
% a_file.mat
% usage: plotCsvData('a_folder', '*.mat') - plots results in all mat files
% in a_folder

outData=[];

readCsv=false;
readMat=false;
if nargin<2
    fileName= returnEveryFileInDirectoryTree(folderName,'.zip');
elseif strcmp(fileName(end-3:end),'.csv')
    readCsv=true;
elseif strcmp(fileName(end-3:end),'.mat')
    readMat=true;
end

if readCsv && strcmp(fileName, '*.csv')
    fileName = returnEveryFileInDirectoryTree(folderName,'.csv');
end

if readMat && strcmp(fileName, '*.mat')
    fileName = returnEveryFileInDirectoryTree(folderName,'.mat');
end


varNames={{'volGainLoss'},...
    {'volPitDrill'}}; % default variables to plot grouped in sub plots

if iscell(fileName)
    nFiles=numel(fileName);
else
    nFiles=1;
end
    
for curFile = 1:1:nFiles
    if readMat && nFiles==1
        load(fullfile(folderName, fileName));
    elseif readMat && nFiles>1
        load(fullfile(folderName, fileName{curFile}));
    elseif nFiles==1 && ~readCsv
        outData = readDataFromZip(fullfile(folderName,fileName));
    elseif nFiles==1 && readCsv
        outData = readDataFromCsv(fullfile(folderName,fileName));
    elseif readCsv
        outData = readDataFromCsv(fullfile(folderName,fileName{curFile}));
    else
        outData = readDataFromZip(fullfile(folderName,fileName{curFile}));
    end
    
    % find raw data units and use a process object to convert to std units
    rawDataUnits=struct();
        
    % check if variables to plot are avaiable
    isAvailableVar=cell(size(varNames));
    nSubPlots=size(varNames,2);
    for ii=1:size(varNames,2)
        isAvailableVar{ii}=false(size(varNames{ii}));
        for jj=1:size(varNames{ii},2)
            if isfield(outData, varNames{ii}{jj})
                isAvailableVar{ii}(jj)=true;
                rawDataUnits.(varNames{ii}{jj})=outData.(varNames{ii}{jj}).unit;
            end
        end
        if ~any(isAvailableVar{ii})
            nSubPlots=nSubPlots-1;
        end
    end
    
    po=PROCESS_OBJ(getparams(0));
    po=po.specifyUnits(rawDataUnits);
    
    hFig=figure('OuterPosition', [14 5 1889 1004]);
    if ~iscell(fileName)
        set(hFig, 'Name', fileName);
    else
        set(hFig, 'Name', fileName{curFile});
    end
    
    ax=zeros(nSubPlots,1);
    subPlotCounter=1;
    for kk=1:size(varNames,2)
        if any(isAvailableVar{kk})
            ax(subPlotCounter)=subplot(nSubPlots,1,subPlotCounter);
            subPlotCounter=subPlotCounter+1;
            hold on;
            legstr=cell(1,1);
            legstrC=1;
            for ll=1:size(isAvailableVar{kk},2)
                if isAvailableVar{kk}(ll)
                    plot(outData.(varNames{kk}{ll}).t, outData.(varNames{kk}{ll}).V*po.defaultDataUnitsFactor.(varNames{kk}{ll}));
                    legstr{legstrC}=varNames{kk}{ll};
                    legstrC=legstrC+1;
                end
            end
            legend(legstr);
            datetick('x');
            xlabel('Time');
            grid;
            zoom on;
            axis tight
        end
    end
    linkaxes(ax,'x');
    
    if nFiles>1
        disp('PAUSED, press any key to open next file....' );
        pause
        try
            close(hFig);
        catch ME
            getReport(ME);
        end
    end
end


end

