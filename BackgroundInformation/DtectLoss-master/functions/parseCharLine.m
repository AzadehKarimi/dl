function strCell=parseCharLine(tline)
%PARSECHARLINE Splits a single string with variables separted by semicolons
%into a cell of strings.
%
% Authors:
%   Espen Hauge <eshau@statoil.com>
%
% Revision history
%   2016/01/14    Espen Hauge v1.0 created
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% find semicolons which separate variables
semiColons=strfind(tline,';');
% number of available variables
NVars=numel(semiColons)+1;
% output cell
strCell=cell(1,NVars);

% populate strCell with text delimited by semicolon
charCount=1;
for colCount=1:NVars-1
    strCell{colCount}=tline(charCount:semiColons(colCount)-1);
    charCount=semiColons(colCount)+1;
end
strCell{colCount+1}=tline(charCount:end);
end