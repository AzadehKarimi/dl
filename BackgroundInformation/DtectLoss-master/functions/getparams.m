% index=0:default params
function params = getparams(index, silentMode)

if nargin ~= 2
    silentMode = true;
else
    silentMode = false;
end

params = struct();
params.voting  = struct();
params.paddle  = struct();
params.flowout = struct();
params.pressp  = struct();
params.pitvol  = struct();

%DTECTLOSS_REALTIME_OBJ
params.voting.pitThreshold_m3                 = 0;
params.voting.flowDeadband_lpm                = 100;%50
params.voting.volumeThresholdWarning_m3       = 0.5;%0.8
params.voting.volumeThresholdAlarm_m3         = 1;
params.voting.flowInLowpassTc_s               = 5;%5
params.voting.volumeThresholdTrippingAlarm_m3 = 0.7;


%VOTING_ALG_OBJ
params.voting.timeResetAlarm_s                =  300;%Reset volume and alarm automatically after 5 minutes
params.voting.timeInhibitAlarmAfterReset_s    =  300;%Inhibit alarm 5 minutes after reset
params.voting.timeConstInjVIOP_s              =  200;
params.voting.timeConstInjVIOF_s              =  400;%200
params.voting.timeConstInjFIO_s               =  600;%400
params.voting.timeWarning_s                   =  120;%desired time from warning to alarm
params.voting.timeFlowOnlyAlarm_s             =   30;
params.voting.timeVolOnlyAlarm_s              =   30;
params.voting.flowIOAlarm_lpm                 =  800;%1000
params.voting.flowIOAlarm_pc                  =   40;%75;
params.voting.volIOAlarm_m3                   =    2;
params.voting.volIOAlarmPumpStart_m3          =    6;
params.voting.timeFiltPitVol_s                =   30;
params.voting.flowMaxInjection_lpm            =   20;%50
params.voting.scalePresToFlow                 =   50;%Temporary scaling error of 1 bar in pressure to 50 lpm in flow (loss/influx)
params.voting.timeWaitRecalibratePaddle_s     = 30;%20 desired waiting time before calibration in case of real gain/loss

%PADDLE_CONVERTER_OBJ 
params.paddle.T_lp_s                          = 30; % lowpass filter time constant in seconds
params.paddle.flowIn_min_to_tune_K_lpm        = 900;% minimum flow before starting to tune K.
params.paddle.flowderThreshold_lpmpsec        = 10; % [lpm/sec] if the flow rate from the rig pumps change slower than this value -> constant/zero flow
params.paddle.TfiltFlowDer_s                  = 20; % Filter time constant for estimating time derivative of flow in from flow in
params.paddle.timeWaitKCalibration_s          = 120; % [sec] 60 Waiting time after pumps are back on with constant flow in before calibration shall be initiated
params.paddle.timeWaitBiasCalibration_s       = 60; % [sec] Waiting time after pumps are back on with constant flow in before calibration shall be initiated
params.paddle.timeTuning_s                    = 60; % 60 for how long should K be updated after a startup?
params.paddle.numberPumpStopsBiasCalibration  = 3;
params.paddle.useFlowInFilt                   = true; %false

%EXPECTED_FLOWOUT_FROM_FLOWIN_OBJ
params.flowout.flowInShutdownThreshold_lpm         = 300;  % make these slack
params.flowout.flowInFullflowThreshold_lpm         = 1000; % make these slack
params.flowout.flowInShutDownDurationThresholdToCalibrateFlow_s    = 120;
params.flowout.vol0FlowOutPumpStartThreshold_m3    = 0.3; %0.4 After pump start, fill this volume before flowout > 0.
params.flowout.N_vec                               = 20;%To be optimized

%EXPECTED_STANDPIPE_PRES_OBJ
params.pressp.flowInShutdownThreshold_lpm                      = 300;  
params.pressp.flowInFullflowThreshold_lpm                      = 1200; 
params.pressp.flowInShutDownDurationThresholdToCalibrateFlow_s = 120;
params.pressp.flowInChangeToTriggerHighRateRecal_lpm           = 99;
params.pressp.numberOfSamplesToWaitAfterFlowChangeToRecal      = 50;
params.pressp.usefminsearch                                    = false;%false; both true and false are ok
params.pressp.flowPivot_lpm                                    = 1500;
params.pressp.factor_forget                                    = 1/2;% average of last 2 time delay estimates (1/5)
params.pressp.presDeadbandCalibrate_bar                        = 1; % Train if offset is more than 1 bar
params.pressp.timeDelay_max_s                                  = 10;

%FLOWLINE_AND_PIT_OBJ
params.pitvol.kInjGainLoss_hz               = 1/400; % 1/50 [1/s]
params.pitvol.upperInjLimit_lpm             =  300; % [lpm] upper limit of injection term
params.pitvol.lowerInjLimit_lpm             = -300; % [lpm] lowerlimit of injection term
params.pitvol.pitDerivativeTimeConstant_s   =  900; % [s] time constant for the first 
                                                    % order filter for finding derivative
                                                    % of the pit volume
params.pitvol.maxTimeDelay_s                =  180; % [s] max allowed time delay between flowline and pit
params.pitvol.damping                       =    1; % [-] damping for the gain/loss observer. Used to calculate tuning.
params.pitvol.nTd                           =    8; % 4 number of time-delays which should be used to calculate mean of time-delay - tuning parameter
params.pitvol.flowstd_R_KF_lpm              =   50;
params.pitvol.flowstd_P0_KF_lpm             =   50;

params.triptank.N_vec                       =   10;

if index == 1
    params.voting.timeWaitRecalibratePaddle_s  = 10; 
elseif index == 2
    params.voting.timeWaitRecalibratePaddle_s  = 20; 
elseif index == 3
    params.voting.timeWaitRecalibratePaddle_s  = 30; 
end

if ~silentMode
    if index > 0 && index < 6
        fprintf(1, 'Parameter set %g read\n', index)
    elseif index == 0
        disp('Default parameter set read');
    else
        disp('Undefined parameter set');
    end
end