%% Start process if not already started
function startProcessIfNotRunning(executableName,path,args)
    [s,w] = dos( 'tasklist' );
    numRunningProcsses = length( regexp( w, strcat('(^|\n)',executableName) ) );
    if numRunningProcsses  == 0,
        startProcess(executableName,path,args);
    else
        disp(strcat('  ',executableName,' already running.'));
    end
end