function [t, outputStore] = runAlgorithmWithFieldData( algObj, fdp, N )
%RUNALOGRITHMWITHFIELDDATA Run an EKD algorithm with a field data set
%   Traverse dataset with a given algorithm for kick/loss detection. The
%   dataset is given by the input fdp, which is a field data player object.
%   The algorithm is given by the input algObj which must be a valid
%   EKD_ALGORITHM_OBJ (abstract class).

%% allocate memory for output
if nargin<3 % if number of datapoints is not specified, run through entire dataset.
    dtInTsc=ceil(mean(diff(fdp.tsc.Time(1:100))*24*2600)); % time step duration in field data
    N=ceil(dtInTsc*fdp.nDataPoints/fdp.dT); % number of datapoints to be stored
end

outputStore=zeros(N,fdp.nVars+2); % output vector
t=(0:N-1)'*fdp.dT; % time vector

%% for saving data
pitVolIdx=fdp.findVar('pitDrill');
offset=-fdp.output(pitVolIdx); % init of offset used for removing offsets from the pit volume
pitVolWithOutOffset=zeros(1,N); % pit volume without offsets


%% main loop
outputStore(1,1:fdp.nVars)=fdp.output;
outputStore(1,end)=algObj.qIo;
for ii=2:N
    fdp=fdp.play;
    outputStore(ii,1:fdp.nVars)=fdp.output;
        
    % remove offsets in pit volume.
    [pitVolWithOutOffset(ii), offset]=removeOffset(outputStore(ii-1:ii,6), offset, algObj.params.pitThreshold);
    
    % run algorithm
    algObj=algObj.stepIt(fdp.dT, fdp.output(flowInIdx), fdp.output(flowOutIdx), fdp.output(pitVolIdx),...
    outputStore(ii,end-1:end)=[algObj.qIo algObj.vIo];
end

end

