function [tsColl, endFilePos]=importDiscoWebData(fileName, folderName, NLinesToRead, columnsToSave, fPos, fMnemonicFile)
% IMPORTDISCOWEBDATA imports data from a delimited text file with data from
% Discovery Web.

% Input:
% fileName: [char] name of file (*.asc)
% folderName: [char] folder name
% NLinesToRead: [int] number of lines to read from the semicolon delimited file.
% columnsToSave: [cell with chars] cell with variables to save, e.g. {'TIME' 'DEP' 'ROPA'}. Must include TIME.
% fPos: [byte] position to start reading in bytes.
% fMnemonicFile: [char] filename of the semicolon delimited mnemonic
% mapping file which tells the relation between DW mnemonics and variable
% name.
% Output:
% tsColl: [timeseries collection] data read from file
% endFilePos: [byte] position in file where reading ended.
%
% Authors:
%   Espen Hauge <eshau@statoil.com>
%
% Revision history
%   2016/01/14    Espen Hauge v1.0 created
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin<5
    fPos=[];
end
if nargin<6
    fMnemonicFile='mnemonicMapping.asc';
end

% output:
tsColl=[]; % timeseries collection
endFilePos=[]; %#ok<NASGU> % position in file where reading ended

% number of columns to save
NColumnsToSave=numel(columnsToSave);

%% create mapping between mnemonics and variable name, e.g. SPP_AVG -> spp
% open file with mapping of possible mnemonics
fidMnemonic=fopen(fMnemonicFile);
if fidMnemonic==-1
    error('importDiscoWebData:mappingFileNotFound', 'The mnemonic file was not found')
end

mapCell=cell(100,10); % a cell with mapping between mnemonics and field names based on the mnemonicMapping.asc file
try
    mline=fgetl(fidMnemonic);
    rowCount=1;
    while ischar(mline)
        mCell=parseCharLine(mline);
        mapCell(rowCount,1:numel(mCell))=mCell;
        rowCount=rowCount+1;
        mline=fgetl(fidMnemonic);
    end
    mapCell(rowCount:end,:)=[];
    NMapCellRows=rowCount-1;
    fclose(fidMnemonic);
catch ME
    getReport(ME);
    fclose(fidMnemonic);
end

%% Find variables to save
try
    % open file and get first line with variables
    fid=fopen(fullfile(folderName,fileName));
    tline=fgetl(fid);
    % column names for available variables
    colNames=parseCharLine(tline);
    % number of variables
    NVars=numel(colNames);
    % logic array saying which variables to keep
    saveColumns=false(1,NVars);
    % reference array saying which mapping to use
    mnemonicIdx=zeros(1,NColumnsToSave);
    % counter for columns in the mnemonic mapping cell
    colCount=1;
    % find which mapping to use for the columns which should be saved
    for pp=1:NColumnsToSave
        for ll=1:NMapCellRows
            while ~isempty(mapCell{ll,colCount}) && ~strcmp(mapCell{ll,colCount},columnsToSave{pp})
                colCount=colCount+1;
            end
            if strcmp(mapCell{ll,colCount},columnsToSave{pp})
                mnemonicIdx(pp)=ll;
            end
            colCount=1;
        end
        % issue warning if the mapping was not found
        if mnemonicIdx(pp)==0
            warning('importDiscoWebData:variableMappingDoesNotExist', 'The variable %s was not found in the mapping matrix', columnsToSave{pp});
        end
    end
    % find which variables to save based on columnsToSave and mapCell
    colCount=1;
    for ii=1:NColumnsToSave
        for jj=1:NVars
            if mnemonicIdx(ii)>0
                while ~isempty(mapCell{mnemonicIdx(ii),colCount}) && ~strcmp(mapCell{mnemonicIdx(ii),colCount},colNames{jj})
                    colCount=colCount+1;
                end
                if strcmp(colNames{jj},mapCell{mnemonicIdx(ii),colCount})
                    saveColumns(jj)=true;
                    colCount=1;
                    colNames{jj}=mapCell{mnemonicIdx(ii),1};
                    break
                end
                colCount=1;
            else
                if strcmp(colNames{jj},columnsToSave{ii})
                    saveColumns(jj)=true;
                    break
                end
            end
        end
        if ~saveColumns(jj)
            warning('importDiscoWebData:mnemonicNotFound', 'The mnemonic %s was not found among the available variables', columnsToSave{ii})
        end
    end
    % considering that NColumnsToSave can be greater than the available
    % variables, NColumnsToSave should be equal to the sum of saveColumns
    NColumnsToSave=sum(saveColumns);
    
    % save units related to each variable
    tline=fgetl(fid); % read next line with units
    units=parseCharLine(tline);
    
    % create temporary storage of variables
    tempData=zeros(NLinesToRead,NColumnsToSave-1); % omit time which will be handled separately
    timeData=cell(NLinesToRead,1); % time
    
    %% read remainder of file, or to specified line number
    % go to last position in file (if specified)
    if ~isempty(fPos)
        fseek(fid,fPos,-1);
    end
    % start reading
    tline=fgetl(fid);
    lineCount=1;
    while ischar(tline) && lineCount<=NLinesToRead
        [~, timeData{lineCount}]=parseTime(tline); % save time
        tempData(lineCount,:)=parseData(tline,saveColumns); % save data
        if ~all(isnan(tempData(lineCount,:))) % if all data is NaN, then skip this line by doing nothing
            if lineCount>1 && strcmp(timeData{lineCount},timeData{lineCount-1}) % there is a duplicate time value
                for kk=1:NColumnsToSave-1
                    if ~isnan(tempData(lineCount,kk)) % if the newly read data is not NaN
                        tempData(lineCount-1,kk)=tempData(lineCount,kk); % save the newly read data to the row read previously
                    end
                end
                tempData(lineCount,:)=zeros(1,NColumnsToSave-1); % reset this line so it is ready for new values
            else
                lineCount=lineCount+1;
            end
        end
        tline = fgetl(fid);
    end
    
    tempData(lineCount:end,:)=[];
    timeData(lineCount:end)=[];
    
    dataUnits=units([false saveColumns(2:end)]);
    % make timeseries and a tscollection
    tsColl=tscollection(timeData);
    % name of columns to save
    saveColNames=colNames([false saveColumns(2:end)]);
    for ii=1:NColumnsToSave-1
        ts=timeseries(tempData(:,ii),timeData,'Name', saveColNames{ii});
        ts.DataInfo.Units=dataUnits{ii};
        tsColl=addts(tsColl,ts);
    end
    tsColl.Name=fullfile(folderName,fileName); % save file info as Name
    endFilePos=ftell(fid);
    fclose(fid);
catch ME
    getReport(ME)
    endFilePos=ftell(fid);
    fclose(fid);
%     fprintf('Failing at index %g. The following line failed to parse\n',lineCount);
%     disp(tline);
end
end

% function strCell=parseCharLine(tline)
% % find semicolons which separate variables
% semiColons=strfind(tline,';');
% % number of available variables
% NVars=numel(semiColons)+1;
% % output cell
% strCell=cell(1,NVars);
% 
% % populate strCell with text delimited by semicolon
% charCount=1;
% for colCount=1:NVars-1
%     strCell{colCount}=tline(charCount:semiColons(colCount)-1);
%     charCount=semiColons(colCount)+1;
% end
% strCell{colCount+1}=tline(charCount:end);
% end

% function [timeAsDateNum, dateAndTime]=parseTime(tline)
% % Convert time to datenum
% y=tline(1:10);
% t=tline(12:19);
% timeAsDateNum=datenum([y ' ' t],'yyyy-mm-dd HH:MM:SS');
% dateAndTime=datestr(timeAsDateNum);
% end

% function dataVec=parseData(tline,saveColumns)
% % read a line of the text file and return an array with data
% NDataToRead=sum(saveColumns)-1;
% semiColons=strfind(tline,';');
% startPos=semiColons(saveColumns(2:end))+1;
% endPos=zeros(1,NDataToRead);
% for kk=1:NDataToRead
%     endIdx=find(semiColons>startPos(kk),1,'first');
%     if isempty(endIdx)
%         endPos(kk)=numel(tline);
%     else
%         endPos(kk)=semiColons(endIdx)-1;
%     end
% end
% % number of available variables
% dataVec=zeros(1,NDataToRead);
% % populate dataVec with data
% for ii=1:NDataToRead
%     dataPoint=str2double(tline(startPos(ii):endPos(ii)));
%     if dataPoint==-999.25
%         dataVec(ii)=NaN;
%     else
%         dataVec(ii)=dataPoint;
%     end
% end
% 
% end
