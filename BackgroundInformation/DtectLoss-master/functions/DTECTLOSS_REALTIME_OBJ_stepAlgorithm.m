function [dtectLossOutputStruct, dtectLossOutputCell] = DTECTLOSS_REALTIME_OBJ_stepAlgorithm(rawData_nonstandardUnits)
%
% function dtectLossOutput = DTECTLOSS_REALTIME_OBJ_stepAlgorithm(rawData_nonstandardUnits)
%
% - this is the "main" call of DTECTLOSS_REALTIME_OBJ, it processes/scales/washes the raw data
% it gets in and updtates all the estimates and alarms of all the algorithms to be run
% - this method must only be called after calling DTECTLOSS_REALTIME_OBJ_construct and DTECTLOSS_REALTIME_OBJ_init
% input:
% 1) rawData_nonstandardUnits - struct of all the tags, with values given in the units
% specified during the DTECTLOSS_REALTIME_OBJ_init call ()
%   example:
%
% rawData_nonstandardUnits =
%
%             flowIn: [1x1 struct]
%            flowOut: [1x1 struct]
%     flowOutPercent: [1x1 struct]
%                rpm: [1x1 struct]
%           velBlock: [1x1 struct]
%             velRop: [1x1 struct]
%             presSP: [1x1 struct]
%          depthHole: [1x1 struct]
%           depthBit: [1x1 struct]
%        volGainLoss: [1x1 struct]
%         heightHook: [1x1 struct]
%        volTripTank: [1x1 struct]
%
% where each variable is on the form:
% rawData_nonstandardUnits.flowIn =
%     t: 7.3633e+05
%     V: 2000
% --------------------------------------
%  output: the DTECTLOSS_REALTIME_OBJ object
%  note that the output will have a member "dtectLossOutput" which is a
%  struct that contains the internally calculated variables of importance
%  both raw data in standard units, washed data, and outputs of the
%  algorithms(qIO, vIO and alarmStatus) plus internal paramters in the algorithms.
%
%                     flowOut_lpm_wash: 2.0016e+03
%              flowOutPercent_prc_wash: 59.7500
%                      flowIn_lpm_wash: 2000
%                             rpm_wash: 150
%                      velRop_mph_wash: 30
%                      presSP_bar_wash: 77.1700
%                     depthHole_m_wash: 3000
%                      depthBit_m_wash: 3000
%                    heightHook_m_wash: 2
%                  volGainLoss_m3_wash: 0
%                  volTripTank_m3_wash: -0.9992
%                    velBlock_mph_wash: 0
%              flowOutPercent_lpm_wash: 2000
%     volGainLossWithoutOffset_m3_wash: 0
%                      flowOut_lpm_raw: 2.0016e+03
%               flowOutPercent_prc_raw: 59.7500
%                       flowIn_lpm_raw: 2000
%                              rpm_raw: 150
%                       velRop_mph_raw: 30
%                       presSP_bar_raw: 77.1700
%                      depthHole_m_raw: 3000
%                       depthBit_m_raw: 3000
%                     heightHook_m_raw: 2
%                   volGainLoss_m3_raw: 0
%                   volTripTank_m3_raw: -0.9992
%                     velBlock_mph_raw: 0
%                    flowIO_lpm_simple: 1.5800
%                      volIO_m3_simple: 0
%                    alarmState_simple: 0
%                     flowIO_lpm_pdiff: 1.5800
%                       volIO_m3_pdiff: 0
%                     alarmState_pdiff: 0
%        betaDivByV_e_bar_per_m3_pdiff: 25
%                        volPit_m3_ukf: -3.2994e-04
%                       presSP_bar_ukf: 56.9798
%                         volFL_m3_ukf: 0
%                       flowIO_lpm_ukf: 0.0081
%                 betaperVol_barm3_ukf: 96.1537
%                      kpit_lpmpm3_ukf: 1.0000e+03
%                   kshaker_lpmpm3_ukf: 10.0000
%                    spaddle_lpmpc_ukf: 1
%                      flowPit_lpm_ukf: 0
%                   flowShaker_lpm_ukf: 0
%                       alarmState_ukf: 0
%                         volIO_m3_ukf: 0

global GLOBAL_DTECTLOSS_REALTIME_OBJ_obj

GLOBAL_DTECTLOSS_REALTIME_OBJ_obj.stepAllAlgorithms(rawData_nonstandardUnits);
dtectLossOutputStruct = GLOBAL_DTECTLOSS_REALTIME_OBJ_obj.dtectLossOutputStruct;
dtectLossOutputCell = GLOBAL_DTECTLOSS_REALTIME_OBJ_obj.dtectLossOutputCell;

end
