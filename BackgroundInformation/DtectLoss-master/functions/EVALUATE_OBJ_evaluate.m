function [nFalseAlarms, alarmTypes] = EVALUATE_OBJ_evaluate(trueFlowIO, trueVolIO,...
    algFlowIO, algVolIO, algAlarmState, algAlarmType)
%function [nFalseAlarms, alarmTypes] = EVALUATE_OBJ_init(trueFlowIO, trueVolIO,...
%algFlowIO, algVolIO, algAlarmState, algAlarmType)
% Inputs:
% ---------------------
% 1) trueFlowIO: struct with fields: t, V, and unit. 
% 2) trueVolIO: struct with fields: t, V, and unit. 
% 3) algFlowIO: struct with fields: T, V. The unit is lpm.
% 4) algVolIO: struct with fields: T, V. The unit is m3.
% 5) algAlarmState: struct with fields: T, V. The values V are integers and
% the mapping is given by the enum AlarmState
% 6) algAlarmType: struct with fields: T, V. The values V are integers and
% the mapping is given by the enum AlarmType
%
% output: 
% 1) nFalseAlarms: array of integers. Number of false alarms of the type
% given by the second output, alarmTypes. The first value in nFalseAlarms
% corresponds to the first type given by alarmTypes, and so on.
% 2) alarmTypes: Enumeration class AlarmTypes.

if nargin < 6
    algAlarmType = [];
end

global EVALUATE_OBJ_obj

[EVALUATE_OBJ_obj, nFalseAlarms, alarmTypes] =...
    EVALUATE_OBJ_obj.evaluate(trueFlowIO, trueVolIO, algFlowIO, ...
    algVolIO, algAlarmState, algAlarmType);
end
