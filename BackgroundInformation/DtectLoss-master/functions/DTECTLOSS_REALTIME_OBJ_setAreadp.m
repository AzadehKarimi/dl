function DTECTLOSS_REALTIME_OBJ_setAreadp(areaDPPOOH_m2, areaDPRIH_m2)
%function DTECTLOSS_REALTIME_OBJ_setAreadp(areaDPPOOH_m2, areaDPRIH_m2)
% This function must be called after
% DTECTLOSS_REALTIME_OBJ_init
% to set the steel areas while tripping in and out
%
% Inputs:
% ---------------------
% 1) areaDPPOOH_m2: open end, from config page (dtectloss live), rig (offshore) or csv file (back test)
% 2) areaDPRIH_m2 : closed end 
%
% output: no output

DO_DEBUG = 0;

if nargin < 1
    areaDPPOOH_m2 = 0.01;
    areaDPRIH_m2  = 0.02;
elseif nargin < 2
    areaDPRIH_m2  = 0.02;    
end

if DO_DEBUG
    if isnumeric(areaDPPOOH_m2)
        disp(['areaDPPOOH_m2 is a number - OK: ', num2str(areaDPPOOH_m2,'%.3f') 'm2']);
    else
        disp('areaDPPOOH_m2 is NOT a number - NOT OK');
    end
    if isnumeric(areaDPRIH_m2)
        disp(['areaDPRIH_m2 is a number - OK: ', num2str(areaDPRIH_m2,'%.3f') 'm2']);
    else
        disp('areaDPRIH_m2 is NOT a number! - NOT OK');
    end
end

global GLOBAL_DTECTLOSS_REALTIME_OBJ_obj

GLOBAL_DTECTLOSS_REALTIME_OBJ_obj = GLOBAL_DTECTLOSS_REALTIME_OBJ_obj.setAreadp(areaDPPOOH_m2, areaDPRIH_m2);
end
