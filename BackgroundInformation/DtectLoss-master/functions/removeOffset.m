function [ yOut, offsetOut ] = removeOffset(yIn, offsetIn, threshold, flowIn)
%REMOVEOFFSET Remove abnormal changes typically found in pit volume
%   yIn is a vector with the previous and current measurement,
%   respectively. That is, yIn = [yPrev yCurrent]. offsetIn is the offset
%   used at previous time-step. If the change in yIn between the current
%   and previous time-step is larger than the threshold, then the offset is
%   updated.

% yOut=[];
% offsetOut=[];

errVal = -0.99925;
flowInLowLim_lpm = 400;

if nargin < 4
    flowIn = errVal;
end

%Handle bad/missing meas: y(2) == errVal
if yIn(2) == errVal
    yOut      = errVal;
    offsetOut = offsetIn;
else
    % compute yOut, disregarding the threshold for now
    yOut = yIn(2) + offsetIn;
    % maintain offset as is
    offsetOut = offsetIn;
    % compute difference between time-steps
    yDiff = diff(yIn);
    % if the difference is larger than given threshold
    if flowIn > flowInLowLim_lpm
        updatedThreshold = threshold/2;
    else
        updatedThreshold = threshold;
    end
    if abs(yDiff) > updatedThreshold
        % update offset
        offsetOut = offsetIn - yDiff;
        % update yOut
        yOut = yIn(2) + offsetOut;
    end
end

end

