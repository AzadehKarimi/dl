function dataVec=parseData(tline,saveColumns)
%PARSEDATA Splits a single string with variables separted by semicolons
%into an array of doubles. The first string is assumed to be time and is
%omitted.
%
% Input: tline [char] and saveColumns [logical array]
%
% Authors:
%   Espen Hauge <eshau@statoil.com>
%
% Revision history
%   2016/01/14    Espen Hauge v1.0 created
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% read a line of the text file and return an array with data
NDataToRead=sum(saveColumns)-1;
semiColons=strfind(tline,';');
startPos=semiColons(saveColumns(2:end))+1;
endPos=zeros(1,NDataToRead);
for kk=1:NDataToRead
    endIdx=find(semiColons>startPos(kk),1,'first');
    if isempty(endIdx)
        endPos(kk)=numel(tline);
    else
        endPos(kk)=semiColons(endIdx)-1;
    end
end
% number of available variables
dataVec=zeros(1,NDataToRead);
% populate dataVec with data
for ii=1:NDataToRead
    dataPoint=str2double(tline(startPos(ii):endPos(ii)));
    if dataPoint==-999.25 % these values are treated as NaNs
        dataVec(ii)=NaN;
    else
        dataVec(ii)=dataPoint;
    end
end

end