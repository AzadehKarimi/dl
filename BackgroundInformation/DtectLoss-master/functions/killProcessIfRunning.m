%% Start process if not already started
function killProcessIfRunning(executableName,~)
    [s,w] = dos( 'tasklist' );
    numRunningProcsses = length( regexp( w, strcat('(^|\n)',executableName) ) );
    if numRunningProcsses  > 0,
      cmd = ['taskkill.exe /IM ',executableName,' /F'];
      dos(cmd);
    end
end
