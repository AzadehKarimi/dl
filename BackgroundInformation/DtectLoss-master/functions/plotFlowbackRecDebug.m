function plotFlowbackRecDebug( fr, idx )
%PLOTFLOWBACKRECDEBUG Summary of this function goes here
%   Detailed explanation goes here

cIdx=fr.idxStore-idx;
if cIdx < 1
    cIdx = fr.nFbs + cIdx;
end

figure; 
ax1=subplot(211);
plot(fr.timeFlowInStore(cIdx,:), fr.flowInStore(cIdx,:));
datetick('x')
grid;
ax2=subplot(212);
plot(fr.timeVolStore(cIdx,:), fr.volStore(cIdx,:)-fr.volStore(cIdx, fr.volOffIdxStore(cIdx)),...
    fr.timeExpVolStore(cIdx,:), fr.expVolStore(cIdx,:)-fr.expVolStore(cIdx, fr.expVolOffIdxStore(cIdx)));
datetick('x')
grid;
legend('Actual', 'Expected', 'Location', 'NorthWest')
linkaxes([ax1, ax2], 'x');

xlim(ax2, [fr.timeFlowInStore(cIdx,1) fr.timeFlowInStore(cIdx,end)])
zoom on;
end

