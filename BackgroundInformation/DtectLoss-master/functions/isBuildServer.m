function ret = isBuildServer()
%determines if the current machine is the build server.

ret     = strcmp(getenv('COMPUTERNAME'),'PC-739039');