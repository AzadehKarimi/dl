function [ ax ] = plotTscData( tsc,flowOutScaleFactor )
%PLOTTSCDATA Plots some timeseries collection data
%   Detailed explanation goes here
if nargin<2
    flowOutScaleFactor=1;
end

availableFieldNames=gettimeseriesnames(tsc);

figure;
Nax=1;
if any(tsc.flowOut.Data>0)
    ax(Nax)=subplot(211);
    Nax=Nax+1;
    plot(tsc.spp,'x');
    ax(Nax)=subplot(212);
    Nax=Nax+1;
    plot(tsc.Time,tsc.flowIn.Data, tsc.Time,tsc.flowOut.Data*flowOutScaleFactor,'x');
    legend('Flow rate in', 'Flow rate out');
    ylabel('Flow-rate (lpm)');
else
    ax(Nax)=subplot(311);
    Nax=Nax+1;
    plot(tsc.spp,'x');
    ax(Nax)=subplot(312);
    Nax=Nax+1;
    plot(tsc.flowIn,'x');
    ax(Nax)=subplot(313);
    Nax=Nax+1;
    plot(tsc.flowOutPercent,'o');
    ylabel('Flow-rate (%)');
end
zoom on;

figure;
ax(Nax)=subplot(211);
Nax=Nax+1;
plot(tsc.holeDepth,'x');
hold on;
plot(tsc.bitDepth,'x');
ax(Nax)=subplot(212);
Nax=Nax+1;
plot(tsc.ropAvg,'x');
hold on;
plot(tsc.rpmAvg,'x');
zoom on;

if checkIfField(availableFieldNames,'pitDrill')
    figure;
    plot(tsc.pitDrill,'x');
    ax(Nax)=gca;
    Nax=Nax+1;
    zoom on;
end

if checkIfField(availableFieldNames,'gainLoss') && ~all(isnan(tsc.gainLoss.Data))
    figure;
    plot(tsc.gainLoss,'x');
    ax(Nax)=gca;
    Nax=Nax+1;
    zoom on;
end

if ~all(isnan(tsc.mwdEcd.Data))
    figure;
    plot(tsc.mwdEcd,'x')
    ax(Nax)=gca;
    Nax=Nax+1;
end

linkaxes(ax,'x');

end

function isField=checkIfField(availableFieldNames,fieldName)
isField=false;
k=strfind(availableFieldNames,fieldName);

if iscell(k)
    for ii=1:numel(k)
        if ~isempty(k{ii})
            isField=true;
        end
    end
elseif ~isempty(k)
    isField=true;
end


end

