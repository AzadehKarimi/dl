function DTECTLOSS_REALTIME_OBJ_reset(initTime_d)
%function DTECTLOSS_REALTIME_OBJ_reset(initTime_d)
% Initiate a full reset, i.e. shut down and start up. This will clear all
% buffers.
%
% input: initTime_d: datenum (days since 00-Jan-0000)
% output: no output

global GLOBAL_DTECTLOSS_REALTIME_OBJ_obj

GLOBAL_DTECTLOSS_REALTIME_OBJ_obj = GLOBAL_DTECTLOSS_REALTIME_OBJ_obj.reset(initTime_d);

end
