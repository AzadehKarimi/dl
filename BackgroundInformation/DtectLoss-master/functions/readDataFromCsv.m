function outData=readDataFromCsv(fileName)

csvTable             = readtable(fileName,'delimiter',';');% nb: quick, but results are tables.
csvCell              = table2cell(csvTable);

fid                  = fopen(fileName);
headerStr            = fgetl(fid);
header               = strsplit(headerStr,';') ;
fclose(fid);

% fprintf(1,'read:%i rows, %i cols\n',size(csvCell,1),size(csvCell,2) );
% row 1: value names
% row 2: index or value
% row 3: unit

csvColumnIdx            = 0;
valueIdx                = 1;

TYPE_ROW                = 1;
UNIT_ROW                = 2;
FIRST_DATA_ROW          = 3;

% quickly:first build "datacell" cell with metadata containted in the header
dataCell                = []; % struct containing data
csvColumnIdx            = 1;
while csvColumnIdx<=size(csvCell,2)
    if csvColumnIdx> 1
        if ~strcmp(header{csvColumnIdx},header{csvColumnIdx-1})
            valueIdx     = valueIdx+1;
        end
    end
    dataCell(valueIdx).t = [];
    dataCell(valueIdx).V = [];
    dataCell(valueIdx).name = header{csvColumnIdx};
    if strcmp(csvCell{TYPE_ROW,csvColumnIdx}, 'VALUE')
        dataCell(valueIdx).unit = csvCell{UNIT_ROW, csvColumnIdx};
        dataCell(valueIdx).csvValueColIdx =csvColumnIdx;
    end
    if  strcmp(csvCell{TYPE_ROW,csvColumnIdx}, 'INDEX')
        dataCell(valueIdx).csvTimeColIdx =csvColumnIdx;
    end
    csvColumnIdx = csvColumnIdx+1;
end

% doing this outside of parfor-loop speeds up parfor 50%, something to do
% with "broadcast variables".
rawTime = cell(1,numel(dataCell));
rawValue = cell(1,numel(dataCell));
for valueIdx = 1:numel(dataCell)
    rawTime{valueIdx}                 = csvCell(FIRST_DATA_ROW:size(csvCell,1),dataCell(valueIdx).csvTimeColIdx);
    rawValue{valueIdx}                = csvCell(FIRST_DATA_ROW:size(csvCell,1),dataCell(valueIdx).csvValueColIdx);
end

% bulk of time spent here: parallell reading of values from memory.
%parfor valueIdx = 1:numel(dataCell)
for valueIdx = 1:numel(dataCell)
%     fprintf('parsing column %i variable: %s, valueIdx: %i\n',csvColumnIdx, dataCell(valueIdx).name,valueIdx);

    curTestIdx              = 1;
    continue_while          = 1;
    lastNonEmptyRowIdx       = numel(rawTime{valueIdx});
    while ( curTestIdx <= numel(rawTime{valueIdx}) ) && continue_while==1
        if ( strcmp(rawTime{valueIdx}(curTestIdx),'') )
            continue_while =0;
            lastNonEmptyRowIdx = curTestIdx-1;
        end%if
        curTestIdx = curTestIdx+1;
    end%while
    try
        dataCell(valueIdx).t = zeros(lastNonEmptyRowIdx,1);
        dataCell(valueIdx).V = zeros(lastNonEmptyRowIdx,1);
        for curValIdx   = 1:lastNonEmptyRowIdx
            dataCell(valueIdx).t(curValIdx) = datenum8601(rawTime{valueIdx}{curValIdx});
            dataCell(valueIdx).V(curValIdx) = parseVal(rawValue{valueIdx}{curValIdx});
        end
%         h = figure;
%         hold on
%         plot(dataCell(valueIdx).t, dataCell(valueIdx).V);
%         pause
%         close(h) 
        fprintf('%s: read %i time-value pairs\n',dataCell(valueIdx).name,lastNonEmptyRowIdx);
    catch ME
        getReport(ME);
        fprintf('WARNING: readDatafromCSV %s skipping column %i\n',fileName,csvColumnIdx);
        continue;%skip variable
    end
end

% read the mnemonic mapping file and map the mnemonics to variable name
% e.g. BITDEP -> bitDepth
fMnemonicFile='mnemonicMapping.asc';
mapCell=createVariableMnemonicMap(fMnemonicFile);
mappedVarNames=mapMnemonicsToVarName(mapCell, {dataCell.name});

% create a new struct which uses the variable name as field name for easy
% reference later.
outData=struct();
for ii=1:numel(mappedVarNames)
    if ~isempty(mappedVarNames{ii})
        outData.(mappedVarNames{ii}).t=dataCell(ii).t;
        outData.(mappedVarNames{ii}).V=dataCell(ii).V;
        outData.(mappedVarNames{ii}).unit=dataCell(ii).unit;
    end
end

end

function ret = parseVal(valStr)
    if isempty(valStr)
        ret         = NaN;
    elseif isnumeric(valStr)
        ret         = valStr;
    else
        ret         = sscanf(valStr,'%f');
    end
end

function mapCell=createVariableMnemonicMap(fMnemonicFile)
%% create mapping between mnemonics and variable name, e.g. SPP_AVG -> spp
% open file with mapping of possible mnemonics
fidMnemonic=fopen(fMnemonicFile);
if fidMnemonic==-1
    error('createVariableMnemonicMap:mappingFileNotFound', 'The mnemonic file was not found')
end

mapCell=cell(100,10); % a cell with mapping between mnemonics and field names based on the mnemonicMapping.asc file
try
    mline=fgetl(fidMnemonic);
    rowCount=1;
    while ischar(mline)
        mCell=parseCharLine(mline);
        mapCell(rowCount,1:numel(mCell))=mCell;
        rowCount=rowCount+1;
        mline=fgetl(fidMnemonic);
    end
    mapCell(rowCount:end,:)=[];
    fclose(fidMnemonic);
catch ME
    getReport(ME);
    fclose(fidMnemonic);
end
end

function mappedVarNames=mapMnemonicsToVarName(mapCell, colNames)

% number of variables, save all variables in colNames
NColumnsToSave=numel(colNames);
% mapped variable names
mappedVarNames=cell(NColumnsToSave,1);
% reference array saying which mapping to use
mnemonicIdx=zeros(1,NColumnsToSave);
% counter for columns in the mnemonic mapping cell
colCount=1;
% find which mapping to use for the columns which should be saved
for pp=1:NColumnsToSave
    for ll=1:size(mapCell,1)
        while ~isempty(mapCell{ll,colCount}) && ~strcmp(mapCell{ll,colCount},colNames{pp})
            colCount=colCount+1;
        end
        if strcmp(mapCell{ll,colCount},colNames{pp})
            mnemonicIdx(pp)=ll;
            mappedVarNames{pp}=mapCell{ll,1};
        end
        colCount=1;
    end
    % issue warning if the mapping was not found
    if mnemonicIdx(pp)==0
        warning('mapMnemonicsToVarName:variableMappingDoesNotExist', 'The variable %s was not found in the mapping matrix', colNames{pp});
    end
end
end