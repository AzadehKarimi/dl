function [outputFieldnames_full, outputFieldnames_noUnit, outputUnits] = DTECTLOSS_REALTIME_OBJ_getOutputFormat()
%
% NOTE THAT THIS METHOD SHOULD ONLY BE CALLED AFTER CALLING
% StepAllAlgorithms AT LEAST ONCE!
% 
% [outputFieldnames_full,outputFieldnames_noUnit, outputUnits] =  DTECTLOSS_REALTIME_OBJ_getOutputFormat()
% 
% outputs: outputFieldnames_full,outputFieldnames_noUnit, outputUnits
% 
% outputFieldnames_full: a 2D array of charaters denoting the full variable
% name including unit, for instance outputFieldnames_full{1} ="volIO_lpm_voting"
% 
% outputFieldnames_noUnit: a cell array of strings denoting the variable
% names without the unit, instance outputFieldnames_noUnit{1} ="volIO_voting"
%
% outputUnits: a cell array of strings denoting the just the units
% for instance outputUnits{1} ="lpm"
% 
% the indexes uniquely identify a variable across the three supplied cell
% arrays.
%----------------------------
% example
% 
% size(outputFieldnames_full)
% 
% ans =
% 
%     59    35
% 
%     ----------------------
% outputFieldnames_full =
% 
% sysState_e                         
% flowOut_lpm_wash                   
% flowOutPercent_prc_wash            
% densECDMWD_wash                    
% flowIn_lpm_wash                    
% rpm_wash                           
% velRop_mph_wash               
% presSP_bar_wash                    
% depthHole_m_wash                   
% depthBit_m_wash                    
% heightHook_m_wash                  
% volGainLoss_m3_wash                
% volTripTank_m3_wash                
% velBlock_mph_wash                  
% flowOutPercent_lpm_wash            
% isCalibratingPaddleBias_wash       
% isCalibratingPaddleK_wash          
% biasPaddle_prc_wash                
% KPaddle_lpm_per_prc_wash           
% volGainLossWithoutOffset_m3_wash   
% flowOut_lpm_raw                    
% flowOutPercent_prc_raw             
% densECDMWD_raw                     
% flowIn_lpm_raw                     
% rpm_raw                            
% velRop_mph_raw                
% presSP_bar_raw                     
% depthHole_m_raw                    
% depthBit_m_raw                     
% heightHook_m_raw                   
% volGainLoss_m3_raw                 
% volTripTank_m3_raw                 
% velBlock_mph_raw                   
% isCalibratingFlowInFlowOut_stat    
% flowOutExpected_lpm_stat           
% tdFlowInflowOut_s_stat             
% TFlowInflowOut_s_stat              
% isCalibratingExpStandpipePres_stat 
% isCalibratingExpStandpipePresK_stat
% presSPExpected_bar_stat            
% bPresSPExp_stat                    
% cPresSPExp_stat                    
% p0PresSPExp_stat                   
% KpresSPExp_stat                    
% isCalibratedExpSP_stat             
% volFlExpected_m3_stat              
% volGLExpected_m3_stat              
% flowPitDer_lpm_stat                
% isCalibrating                      
% flowIOflow_lpm_voting              
% flowIOpres_lpm_voting              
% presIO_bar_voting                  
% volIOflow_m3_voting                
% volIOpres_m3_voting                
% volIOpit_m3_voting                 
% flowIO_lpm_voting                  
% volIO_m3_voting                    
% alarmState_voting                  

global GLOBAL_DTECTLOSS_REALTIME_OBJ_obj;
[GLOBAL_DTECTLOSS_REALTIME_OBJ_obj,outputFieldnames_full,outputFieldnames_noUnit, outputUnits] = GLOBAL_DTECTLOSS_REALTIME_OBJ_obj.getOutputFormat();
  
end
