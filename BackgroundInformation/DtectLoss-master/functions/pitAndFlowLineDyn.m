function xDot = pitAndFlowLineDyn( x,u,p,dt )
%PITANDFLOWLINEDYN Dynamics of pit and flow line
%   x=[Vpit theta Vfl]', u=[qIn qOut isConnection], p=paramstruct,
%   dt=timestep
xDot=zeros(3,1);

xDot(1)=(max(p.cFl*x(3),0)-u(1))*p.lpm2m3s; % c*VFl - qIn
xDot(3)=(u(2)*x(2)-max(p.cFl*x(3),0))*p.lpm2m3s; % theta*qOut - c*VFl
    

end
