function hFig = plotConnectionDataDebug( fr, idx )
%PLOTCONNECTIONDATADEBUG For debugging of the flowback recorder when
%working with connection data.
%   Detailed explanation goes here

cIdx=fr.idxCon-idx;
if cIdx < 1
    cIdx = fr.nConToRecord + cIdx;
end

hFig = figure; 
ax1=subplot(211);
plot(fr.timeFlowInCon(cIdx,:), fr.flowInCon(cIdx,:));
datetick('x')
grid;
ax2=subplot(212);
plot(fr.timeVolCon(cIdx,:), fr.volCon(cIdx,:)-fr.volCon(cIdx, 1),...
    fr.timeExpVolCon(cIdx,:), fr.expVolCon(cIdx,:)-fr.expVolCon(cIdx, 1));
datetick('x')
grid;
legend('Actual', 'Expected', 'Location', 'NorthWest')
linkaxes([ax1, ax2], 'x');

idxTimeIsNan = isnan(fr.timeFlowInCon(cIdx, :));
time = fr.timeFlowInCon(cIdx, ~idxTimeIsNan);
xlim(ax2, [time(1) time(end)])
zoom on;
end

