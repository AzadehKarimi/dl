function EVALUATE_OBJ_init(algorithmName, rigName)
%function EVALUATE_OBJ_init(algorithmName, rigName)
% This function must be called  before calling
% EVALUATE_OBJ_evaluate
%
% Inputs:
% ---------------------
% 1) algorithmName: for example 'VOTING_ALG_OBJ' (string, no
% parenthesis)
% 2) rigName: string
% 
% output: no output

global EVALUATE_OBJ_obj

EVALUATE_OBJ_obj = EVALUATE_OBJ(algorithmName, rigName);
EVALUATE_OBJ_obj = EVALUATE_OBJ_obj.initialize();
end
