%
% runs the specified algorithms on all the csv-files in the specified
% catalog, plotting and saving the results as we go.
%
%
function [evaRes, numberOfScenariosRun, totalSimDuration_s] = backTestAlgorithmsOnCatalog(FIELD_DATA_ROOT_DIR,...
    doProfiler, isDebugMode, doWrapperTest, buildNr, doParalell, doSigmaPlotter)

global ParSet
if not(isempty(ParSet))
    dowriteresultstofile = true;
    tm                   = clock;
    fname                = ['.\Evaluation Results\Evaluation_' date '-' num2str(tm(4), '%02.0f') '-'...
        num2str(tm(5), '%02.0f') '-' num2str(tm(6), '%02.0f') '-parameter_set_' num2str(ParSet, '%.0f') '.csv'];
    fileId              = fopen(fname,'W');
else
    dowriteresultstofile = false;
end
SIGMA_ip                 = '1'% start_SIGMA_processes;
% INTIALIZATION
nameDesiredProcessTags   = {'flowIn', 'depthBit', 'depthHole', ...
    'flowOut', 'flowOutPercent', 'flowOutCoriolis', 'presSP',...
    'volGainLoss', 'volPitDrill',...
    'volTripTank', 'volTripTank2', 'volTripTankIn', 'volTripTankOut', 'heightHook', ...
    'rpm', 'torque', 'weightOnBit'};

opengl software ;% draw opengl in software, otherwise graphs look ugly,as Matlab disables opengl.
fprintf('*** running every scenario in directory: %s ***\n',FIELD_DATA_ROOT_DIR);
dataFiles           = returnEveryFileInDirectoryTree(FIELD_DATA_ROOT_DIR,'.zip');
xlsConfigCell       = readXLSconfig(FIELD_DATA_ROOT_DIR);

numScenarios        = numel(xlsConfigCell);

if doProfiler
    profile on;
else
    profile off;
end

if isDebugMode
    PERCENT_TO_RUN  = 5;
else
    PERCENT_TO_RUN  = 100;
end
bm                  = cell(1,numScenarios);
simDuration_s       = zeros(1,numScenarios);
% MAIN LOOP
maintimer           = tic;
if doParalell  % paralell run: requires paralell toolbox, alot faster(do on build machine)
    parfor scenarioIdx     = 1:numScenarios  % run given number of scenarios in paralell
        [bm{scenarioIdx}, simDuration_s(scenarioIdx) ] = innerLoop(scenarioIdx, xlsConfigCell, dataFiles, isDebugMode, FIELD_DATA_ROOT_DIR,...
            nameDesiredProcessTags, doWrapperTest, SIGMA_ip, buildNr, PERCENT_TO_RUN, doSigmaPlotter);
    end
else    % serial:run does not require paralell toolbox
    for scenarioIdx     = 1:numScenarios % serial: run one scenario at a time
        [bm{scenarioIdx}, simDuration_s(scenarioIdx)  ] = innerLoop(scenarioIdx, xlsConfigCell, dataFiles, isDebugMode, FIELD_DATA_ROOT_DIR,...
            nameDesiredProcessTags, doWrapperTest, SIGMA_ip, buildNr, PERCENT_TO_RUN, doSigmaPlotter);
    end%parfor
end
totalSimDuration_s   = sum(simDuration_s);
numberOfScenariosRun = numel(find(simDuration_s > 0));
timeToDoMainLoop_s   = toc(maintimer);

plotTimer            = tic;
% plotting cannot be parallellized.

% Evaluate all simulations.
sC                  = 0; % scenario counter

% headers for evaluate table: kpi, nCorrectAlarms, nFalseAlarms,
% nTrueAlarms, algUpTimeInHours, simDuration, algLateAlarms
evaTable = zeros(numScenarios, 9);
alarmTypes = enumeration('AlarmType'); % types of alarms
nAlarmTypes = size(alarmTypes, 1); % number of alarm types
alarmTypeTable = zeros(numScenarios, nAlarmTypes);

for scenarioIdx = 1:numScenarios
    if ~isempty(bm{scenarioIdx})
        if bm{scenarioIdx}.didLoadOk
            [~, evaluateResults, alarmTypeCount] = bm{scenarioIdx}.evaluateAndFinalize(scenarioIdx, FIELD_DATA_ROOT_DIR);
            sC = sC + 1;
            if ~isempty(evaluateResults)
                evaTable(sC, 1) = evaluateResults.keyPerformanceIndex;
                evaTable(sC, 2) = evaluateResults.algNoOfCorrectAlarms;
                evaTable(sC, 3) = evaluateResults.algNoOfFalseAlarms;
                evaTable(sC, 4) = evaluateResults.trueNoOfAlarms;
                evaTable(sC, 5) = evaluateResults.algUpTimeInHours;
                evaTable(sC, 6) = evaluateResults.durationInHours;
                evaTable(sC, 7) = evaluateResults.algLateAlarms;
                evaTable(sC, 8) = evaluateResults.timeToDetection;
                evaTable(sC, 9) = evaluateResults.accumulatedVolumeAtDetection;
            else
                fprintf(1, 'No evaluate result available for %s\n', bm{scenarioIdx}.simName{scenarioIdx});
            end
            
            if ~isempty(alarmTypeCount)
                alarmTypeTable(sC, :) = alarmTypeCount';
            else
                fprintf(1, 'No count of alarm types available for %s\n', bm{scenarioIdx}.simName{scenarioIdx});
            end
        end
    end
end

% statistics from evaluate for all simulations
totalFAR = sum(evaTable(:, 3)) / sum(evaTable(:, 5)); % number of false alarms per active algorithm hour
if sC > 0
    meanKPI = sum(evaTable(:, 1)) / sC;
else
    meanKPI = NaN;
end
totalPD = sum(evaTable(:, 2)) / sum(evaTable(:, 4)) * 100; % probability of detection in percent for all data sets

timeToDoMainPlotting_s = toc(plotTimer);

if dowriteresultstofile
    write_results(fileId, ';');
    fclose(fileId);
end

write_results(1, '\t')

% make struct with aggregated results for all (valid) backtests
evaRes = struct('totalFar', totalFAR, 'meanKpi', meanKPI, 'totalPd', totalPD);

%cleanup
killProcessIfRunning('tagServer.exe');

    function write_results(fid, sep)
        
        fprintf(fid, '\nStatistics after running %0.0f data sets:\n', sC);
        fprintf(fid, ['Total simulation time: %0.2f hours,' sep ' alg up-time: %0.2f hours (%0.2f%%)\n'],...
            sum(evaTable(:, 6)), sum(evaTable(:, 5)), sum(evaTable(:, 5)) / sum(evaTable(:, 6)) * 100);
        fprintf(fid, ['KPI: %0.2f,' sep ' FAR: %0.2f alarms/active hour,' sep ' PD: %0.2f%%\n'], meanKPI, totalFAR, totalPD);
        fprintf(fid, 'Results for all simulations:\n');
        
        if strcmp(sep, '\t')
            fprintf('%-30s\t%-4s\t%-4s\t%-13s\t%-16s\t%-10s\t%-12s\t%-16s\t%-10s\t%-4s\t%-4s\n',...
                'Time series', 'FAR', 'KPI', '#correctAlarms', '#falseAlarms', '#trueAlarms', 'algUpTime [h]', 'Sim duration [h]',...
                '#lateAlarms', 'TD [s]', 'VD [m3]');
        else
            fprintf(fid, ['Time series' sep ' FAR' sep ' KPI' sep ' #correctAlarms' sep ' #falseAlarms' sep ...
                ' #trueAlarms' sep ' algUpTime [h]' sep ' sim duration [h]' sep ' #lateAlarms' sep ...
                ' TD [s]' sep ' VD [m3]\n']);
        end
        
        sC = 0;
        for sIdx = 1:numScenarios
            if ~isempty(bm{sIdx})
                if bm{sIdx}.didLoadOk
                    sC = sC + 1;
                    fprintf(fid, ['%-30s' sep ' %-4.2f' sep ' %-4.2f' sep ' %-13g' sep ' %-16g' sep ' %-10g' sep...
                        ' %-12.2f' sep ' %-16.2f' sep ' %-10g' sep ' %-4.2f' sep ' %-4.2f' sep '\n'],...
                        bm{sIdx}.simName{sIdx}, evaTable(sC, 3)/max(1, evaTable(sC, 5)), evaTable(sC, 1), ...
                        evaTable(sC, 2), evaTable(sC, 3), evaTable(sC, 4), evaTable(sC, 5), evaTable(sC, 6),...
                        evaTable(sC, 7), evaTable(sC, 8), evaTable(sC, 9));
                end
            end
        end
        fprintf(fid, '\n');
        
        % print count of alarm type to file
        fprintf(fid, '\nType of false alarms and warnings which have been issued:\n');
        fprintf(fid, strcat('%-30s', sep, ' %-30s', sep), 'Time series', '#total alarms');
        
        for kk=1:nAlarmTypes
            fprintf(fid, ['%-30s' sep ' '], char(alarmTypes(kk)));
        end
        fprintf(fid,'\n');
        
        sC=0;
        for sIdx = 1:numScenarios
            if ~isempty(bm{sIdx})
                if bm{sIdx}.didLoadOk
                    sC = sC + 1;
                    fprintf(fid, ['%-30s' sep '%-30g' sep ''], bm{sIdx}.simName{sIdx},...
                        sum(alarmTypeTable(sC, 1:6)) + sum(alarmTypeTable(sC, 10:16)));
                    for kk = 1:nAlarmTypes
                        fprintf(fid, ['%-30g' sep ' '], alarmTypeTable(sC, kk));
                    end
                    fprintf(fid,'\n');
                end
            end
        end
        fprintf(fid, ['%-30s' sep '%-30g' sep ''], 'Sum', ...
            sum(sum(alarmTypeTable(:, 1:6))) + sum(sum(alarmTypeTable(:, 10:16))));
        for kk = 1:nAlarmTypes
            fprintf(fid, ['%-30g' sep ' '], sum(alarmTypeTable(:, kk)));
        end
        fprintf(fid,'\n\n');
        
        fprintf(fid,'Duration (Main loop): %8.2f [s]\n', timeToDoMainLoop_s);
        fprintf(fid,'Duration (Plotting) : %8.2f [s]\n', timeToDoMainPlotting_s);
    end
end

function [bm,simDuration_s ] = innerLoop(scenarioIdx, xlsConfigCell, dataFiles, isDebugMode,...
    FIELD_DATA_ROOT_DIR, nameDesiredProcessTags, doWrapperTest, SIGMA_ip, buildNr, PERCENT_TO_RUN,...
    doSigmaPlotter)

bm = [];
simDuration_s = 0;

if xlsConfigCell(scenarioIdx).useForBacktesting == 1
    dataFilePresent=0;
    for k= 1:1:numel(dataFiles)
        if ~isempty( strfind(dataFiles{k},sprintf('-%i.',scenarioIdx)   ))
            dataFilePresent=dataFilePresent+1;
        end
    end
    
    if dataFilePresent
        bm  = BACKTESTING_MANAGER_PARALELL_OBJ();
        bm  = bm.initalize(FIELD_DATA_ROOT_DIR,nameDesiredProcessTags,doWrapperTest,SIGMA_ip,isDebugMode,buildNr,doSigmaPlotter);
        [bm, ~, simDuration_s]  = bm.RunScenarioToCompletionPrc(scenarioIdx,PERCENT_TO_RUN);
    elseif (dataFilePresent >1)
        disp('ERROR: please close the xls-file in the fieldData directory and restart.');
    end
end

end

