function [P, theta] = WLS(Pprev, thetaprev, a, alpha, phi, beta, z)
%WLS computes the least squares estimate, with dynamic weighting, of
%unknown parameters for discrete signals.
%   The matrix P (adaptive gain) and vector theta (unknown parameters) are
%   computed from the previous computed matrix Pprev, the previous
%   parameter estimate thetaprev, a normalizing factor alpha>0, the weight
%   a>0, the regressor phi, forgetting factor beta (1>beta>0), and
%   measurements z. The implemented algorithm is from Adaptive control
%   tutorial by Ioannou and Fidan pp. 5.24, eq. 5.55 and 5.56.
%   link to book: https://books.google.no/books?id=o7d0R6Zj2pMC&lpg=PA388&ots=5c7auCyrYi&dq=weighted%20least%20squares%20fidan%20ioannou&pg=PA108#v=onepage&q&f=false

m = 1 + alpha*(phi')*(phi);
P = 1/beta*( Pprev - a*Pprev*(phi)*(phi')*Pprev / (m^2*beta+a*(phi')*Pprev*(phi)) );
theta = thetaprev + sqrt(a)*P*phi*(z - (thetaprev')*(phi) ) / m^2;

end