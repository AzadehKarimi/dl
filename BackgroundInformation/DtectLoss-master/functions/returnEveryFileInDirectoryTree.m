% list every file in root, traversing every sub directoy
function   allFilesFound = returnEveryFileInDirectoryTree(root,filetype)
    allFilesFound =[];
    if strcmp(root,'')
        listing = dir;
      else
        listing = dir(root);
    end
    for curListing = 1:1:numel(listing)
        if (strcmp(listing(curListing).name, '.'))
            continue;
        end
        if (strcmp(listing(curListing).name,'..'))
            continue;
        end
        if (listing(curListing).isdir ==1)% recursively run this function for each folder found
            filesFoundInSubDir = returnEveryFileInDirectoryTree(fullfile( root,listing(curListing).name ),filetype);
            if ~isempty(filesFoundInSubDir)
                allFilesFound = [allFilesFound  strcat(listing(curListing).name,filesep,filesFoundInSubDir)]; 
            end
        else
            if strfind(listing(curListing).name,filetype)
                allFilesFound{end+1} = listing(curListing).name ;
            end
        end
    end
end