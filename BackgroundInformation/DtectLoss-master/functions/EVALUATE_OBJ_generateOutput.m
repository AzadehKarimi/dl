function resultStruct = EVALUATE_OBJ_generateOutput()
%function resultStruct = EVALUATE_OBJ_generateOutput()
% Inputs: no inputs
% 
% output: resultStruct: struct witht the fields (and typical values):
%                   trueNoOfAlarms: 7
%                 trueNoOfWarnings: 7
%             algNoOfCorrectAlarms: 2
%                  algNoOfWarnings: 15
%               algNoOfFalseAlarms: 1
%                    algLateAlarms: 1
%                          algName: 'ukf'
%                          rigName: 'simRig'
%                   falseAlarmRate: 0.8001
%           probabilityOfDetection: 28.5714
%              keyPerformanceIndex: 0.7810
%                  timeToDetection: 117.0000
%     accumulatedVolumeAtDetection: 1.4000
%                  durationInHours: 14.9986
%                        algUpTime: 100
%                 algUpTimeInHours: 14.9986

global EVALUATE_OBJ_obj

resultStruct = EVALUATE_OBJ_obj.generateOutput();
end
