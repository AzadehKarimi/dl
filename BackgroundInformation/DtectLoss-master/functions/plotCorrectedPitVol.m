function [ volWoOffset ] = plotCorrectedPitVol( folderName, fileName, threshold, hFig )
%PLOTCORRECTEDPITVOL Summary of this function goes here
%   Detailed explanation goes here

if nargin < 4
    hFig=gcf;
end

load(fullfile(folderName, fileName)); % load mat file

if isfield(outData, 'volGainLoss')
    offset=-outData.volGainLoss.V(1);
    volWoOffset=zeros(size(outData.volGainLoss.t));
    time=outData.volGainLoss.t;
    for ii=2:numel(outData.volGainLoss.t)
        [volWoOffset(ii), offset]=removeOffset(outData.volGainLoss.V(ii-1:ii), offset, threshold);
    end
else
    offset=-outData.volPitDrill.V(1);
    volWoOffset=zeros(size(outData.volPitDrill.t));
    time=outData.volPitDrill.t;
    for ii=2:numel(outData.volPitDrill.t)
        [volWoOffset(ii), offset]=removeOffset(outData.volPitDrill.V(ii-1:ii), offset, threshold);
    end
end

figure('Name', sprintf('Corrected act vol for %s', fileName));
ax(1)=subplot(211);
stairs(time, volWoOffset);
xlabel('Time');
ylabel('Volume [m3]');
grid on;
title('Volume wo offset');
datetick('x');
ax(2)=subplot(212);
stairs(time, outData.volGainLoss.V);
xlabel('Time');
ylabel('Volume [m3]');
grid on;
title('Volume with offset');
datetick('x');
linkaxes(ax,'x');
zoom on;

c=get(hFig, 'children');
if numel(c)>1
    linkaxes([ax, c(2)], 'x');
end

axis tight;
end

