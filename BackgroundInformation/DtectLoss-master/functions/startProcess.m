function startProcess(executableName,path,args)
    disp(strcat('  Launching ',executableName));
    command = ['cd ',path,' & start ',executableName,' ',args];
    dos(command);
end