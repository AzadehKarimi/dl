
function configStruct = readXLSconfig(root_dir)

    configStruct    = [];
    % find xls file 
    listing         = dir(root_dir);
    nXLSFilesFound  = 0;
    xlsFile         = [];
    for k           = 1:1:numel(listing)
        if ~isempty(strfind(listing(k).name,'.xlsx')) || ~isempty(strfind(listing(k).name,'.xls'))
            nXLSFilesFound = nXLSFilesFound+1;
            xlsFile     = fullfile(root_dir,listing(k).name);
        end%if
    end
    if nXLSFilesFound ~= 1
       error(['no configuration excel file found in ',root_dir] );
    end
    % 
    [num, txt, raw]         = xlsread(xlsFile);
    flowTagColIdx           = findHeaderIdx('DtectLossUseFlowTag',raw);
    tripTankTagColIdx       = findHeaderIdx('DtectLossUseTripTankTag',raw);
    pitVolTagColIdx         = findHeaderIdx('DtectLossUsePitVolTag',raw);
    useForBacktestTagIdx    = findHeaderIdx('DtectLossUseForBacktest',raw);
    tripAreaDPPOOHIdx       = findHeaderIdx('AREA_POOH',raw);
    tripAreaDPRIHIdx        = findHeaderIdx('AREA_RIH',raw);

    algorithmOnOffColIdx =[];
    %find every colum that contains "ALG_OBJ()"
    for k = 1:1:size(raw,2)
        if ~isempty(strfind(raw{1,k},'ALG_OBJ()'))
            algorithmOnOffColIdx = [algorithmOnOffColIdx,k];
        end
    end
    
%     curFile = 0;
    curLine = 1;
    while curLine < size(raw,1)
        curLine                                             = curLine+1;
        if isnan(raw{curLine,1}) 
           continue;
        end
        scenarioIdx                                         = raw{curLine,1};
        configStruct(scenarioIdx).usedFlowMeasTagName       = raw{curLine,flowTagColIdx};
        configStruct(scenarioIdx).usedTripTankMeasTagName   = raw{curLine,tripTankTagColIdx};
        configStruct(scenarioIdx).usedPitVolMeasTagName     = raw{curLine,pitVolTagColIdx};
        configStruct(scenarioIdx).useForBacktesting         = raw{curLine,useForBacktestTagIdx};
        configStruct(scenarioIdx).areaDPPOOH_m2             = raw{curLine,tripAreaDPPOOHIdx};
        configStruct(scenarioIdx).areaDPRIH_m2              = raw{curLine,tripAreaDPRIHIdx};
        
        configStruct(scenarioIdx).algorithmsToRun = [];
        for curAlg = 1:1:numel(algorithmOnOffColIdx)
           if raw{curLine,algorithmOnOffColIdx(curAlg)} ==1 
               configStruct(scenarioIdx).algorithmsToRun{end+1} = raw{1,algorithmOnOffColIdx(curAlg)};
           end
        end
    end

end
function idx = findHeaderIdx(headerText,raw)
    idx = nan;
    for k= 1:1:size(raw,2)
        if ~isempty(strfind(raw{1,k},headerText))
            idx = k;
        end
    end
    if isnan(idx)
        error(['did not find column in excel file header: ',headerText]);
    end
end
   






