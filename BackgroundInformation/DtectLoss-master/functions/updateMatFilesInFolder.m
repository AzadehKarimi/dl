function updateMatFilesInFolder(dataFolder)
% UPDATEMATFILESINFOLDER update mat files in folder which are older than
% the corresponding zip files.

% When using field data, we would like to check if there is a mat file
% available which is newer than a corresponding zip file. If so, then load
% the mat file instead of unzipping and reading data from csv.

lZip=dir(fullfile(dataFolder, '*.zip'));
lMat=dir(fullfile(dataFolder, '*.mat'));
zipNames={lZip.name}';
matNames={lMat.name}';
matDate={lMat.datenum}';

nZipFiles=size(lZip,1);
nMatFiles=size(lMat,1);

readZipFile=false(nZipFiles,1);

matFileExists=false;
for ii=1:nZipFiles
    zn=zipNames{ii}(1:end-4);
    k=strfind(matNames,zn);
    for jj=1:nMatFiles
        if ~isempty(k{jj})
            matFileExists=true;
            % check date of unzipped files
            unzippedFiles=unzip(zipNames{ii});
            for ll=1:size(unzippedFiles,2)
                dirRes=dir(unzippedFiles{ll});
                if ~strncmpi(unzippedFiles{ll}, 'true', 4) && matDate{jj}<dirRes.datenum
                    readZipFile(ii)=true; % mat file is old and must be updated
                end
                delete(unzippedFiles{ll});
            end
        end
    end
    if ~matFileExists
        readZipFile(ii)=true; % the mat file does not exist
    end
    matFileExists=false;
end

for kk=1:nZipFiles
    if readZipFile(kk)
        outData=readDataFromZip(fullfile(dataFolder,zipNames{kk}));
        save(fullfile(dataFolder,strcat(zipNames{kk}(1:end-3), 'mat')),'outData');
    end
end

end