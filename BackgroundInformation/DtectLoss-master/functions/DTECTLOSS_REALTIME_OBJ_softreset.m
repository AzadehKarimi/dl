function DTECTLOSS_REALTIME_OBJ_softreset()
%function DTECTLOSS_REALTIME_OBJ_softreset()
% A soft reset resets the algorithm to zero gain/loss but does not 
% discard buffered input data. 
% The advantage of this is that the algorithm will have much shorter
% downtime than if the dtectloss object is deleted and re-initated.
%
% input: no input
% output: no output

global GLOBAL_DTECTLOSS_REALTIME_OBJ_obj

GLOBAL_DTECTLOSS_REALTIME_OBJ_obj = GLOBAL_DTECTLOSS_REALTIME_OBJ_obj.softreset();

end
