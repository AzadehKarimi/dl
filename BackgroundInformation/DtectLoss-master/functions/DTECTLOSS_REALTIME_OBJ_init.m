function DTECTLOSS_REALTIME_OBJ_init(algorithmName, rawDataUnits, initTime_d, backTestModeOn_bool)
%function DTECTLOSS_REALTIME_OBJ_init(algorithmName, rawDataUnits, initTime_d, backTestModeOn_bool)
% This function must be called  before calling
% DTECTLOSS_REALTIME_OBJ_stepAlgorithm
% (note:you should no longer need to call  DTECTLOSS_REALTIME_OBJ_construct first)
%
% Inputs:
% ---------------------
% 1) algorithmName: for example 'VOTING_ALG_OBJ' (string, no
% parenthesis) (replaces configCell in earlier versions of API)
% 2) rawDataUnits : struct with units of all the variables to be provided
% example:
% rawDataUnits =
%             flowIn: 'lpm'
%            flowOut: 'lpm'
%     flowOutPercent: '%'
%                rpm: 'rpm'
%           velBlock: 'm/s'
%             velRop: 'm/h'
%             presSP: 'bar'
%          depthHole: 'm'
%           depthBit: 'm'
%        volGainLoss: 'm3'
%         heightHook: 'm'
%        volTripTank: 'm3'
% 3) initTime_d: datenum (days since 00-Jan-0000)
% 4) backTestModeOn_bool: boolean. Indicates if the algorithm is run as a
% backtest or not. Optional argument, if not given then backtestModeOn_bool
% is set to false.
%
% output: no output

DO_DEBUG = 1;

if nargin < 3
    initTime_d = [];
end

if nargin < 4
    backTestModeOn_bool = false;
end

if DO_DEBUG
    if (ischar(algorithmName))
        disp(['algorithmName is a string - OK: ', algorithmName]);
    else
        disp('algorithmName is NOT string - NOT OK');
    end
    if isstruct(rawDataUnits)
        disp('rawDataUnits is struct - OK');
        fields = fieldnames(rawDataUnits);
        for k = 1:1:numel(fields)
            disp(['rawDataUnits field:', fields{k}]);
        end
    else
        disp('rawDataUnits is NOT STRUCT! - NOT OK');
    end
end

global GLOBAL_DTECTLOSS_REALTIME_OBJ_obj

GLOBAL_DTECTLOSS_REALTIME_OBJ_obj = DTECTLOSS_REALTIME_OBJ();
GLOBAL_DTECTLOSS_REALTIME_OBJ_obj = GLOBAL_DTECTLOSS_REALTIME_OBJ_obj.init(algorithmName, rawDataUnits, initTime_d, backTestModeOn_bool);
end
