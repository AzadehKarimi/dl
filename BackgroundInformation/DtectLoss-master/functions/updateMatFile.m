function matFileName=updateMatFile(zipFileName)
% UPDATEMATFILE update mat file which are older than
% the corresponding zip file.

% When using field data, we would like to check if there is a mat file
% available which is newer than a corresponding zip file. If so, then load
% the mat file instead of unzipping and reading data from csv.

matFileName=strcat(zipFileName(1:end-3), 'mat');
lMat=dir(matFileName);
lZip=dir(zipFileName);

if isempty(lZip)
    error('updateMatFile:noSuchFile', 'The file %s does not exist', zipFileName);
end
readZipFile=false;

if isempty(lMat)
    readZipFile=true;
else
    zipDate=lZip.datenum;
    matDate=lMat.datenum;
    
    if matDate<zipDate
        readZipFile=true; % mat file is old and must be updated
    end
end

if readZipFile
    fprintf('Generating new mat file for %s\n', zipFileName);
    outData=readDataFromZip(zipFileName);
    save(matFileName,'outData');
end

end