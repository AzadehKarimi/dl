function [timeAsDateNum, dateAndTime]=parseTime(tline)
%PARSETIME Convert time in the format '2016-02-04T08:28:54.000+01:00' into a
%datenum.
%
% Authors:
%   Espen Hauge <eshau@statoil.com>
%
% Revision history
%   2016/01/14    Espen Hauge v1.0 created
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

persistent dateMapping

dateMapping={'Jan' 'Feb' 'Mar' 'Apr',...
    'May' 'Jun' 'Jul' 'Aug' 'Oct' 'Nov' 'Dec'};

% Convert time to datenum
y=tline(1:10);
t=tline(12:19);
timeAsDateNum=datenum([y ' ' t],'yyyy-mm-dd HH:MM:SS');
dateAndTime=[y(9:10) '-' dateMapping{str2double(y(6:7))} '-' y(1:4) ' ' t];
end