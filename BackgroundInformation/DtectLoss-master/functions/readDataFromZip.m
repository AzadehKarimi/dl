function outData = readDataFromZip(fileNameZip)
fprintf('reading data from %s...\n',fileNameZip );
t_handle    = tic ;
fileNames    = unzip(fileNameZip);
for k= 1:1:numel(fileNames)
    % read data from the other file in the zip, not the "TRUE" file
    if isempty(strfind(lower(fileNames{k}),'true')) 
        outData = readDataFromCsv(fileNames{k});
    end
end
delete(fileNames{:});
duration    = toc(t_handle);
fprintf('duration:%.2f seconds\n',duration);