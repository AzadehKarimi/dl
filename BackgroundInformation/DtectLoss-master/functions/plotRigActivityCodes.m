function plotRigActivityCodes( folderName, fileName, hFig )
%PLOTCORRECTEDPITVOL Summary of this function goes here
%   Detailed explanation goes here

if nargin < 3
    hFig=gcf;
end

load(fullfile(folderName, fileName)); % load mat file

if isfield(outData, 'activityBHI')
    actCode=outData.activityBHI.V;
    time=outData.activityBHI.t;
elseif isfield(outData, 'activitySLB')
    actCode=outData.activitySLB.V;
    time=outData.activitySLB.t;
elseif isfield(outData, 'activityHAL')
    actCode=outData.activityHAL.V;
    time=outData.activityHAL.t;
else
    disp('No activity code in log file');
    return
end

figure('Name', sprintf('Rig activity code for %s', fileName));
ax(1)=subplot(311);
plot(time, actCode);
xlabel('Time');
ylabel('Activity code');
grid on;
title('Rig activity code');
datetick('x');
ax(2)=subplot(312);
plot(outData.presSP.t, outData.presSP.V*1e5);
xlabel('Time');
ylabel('Pressure [bar]');
grid on;
title('Standpipe pressure');
datetick('x');
ax(3)=subplot(313);
plot(outData.depthHole.t, outData.depthHole.V, outData.depthBit.t,outData.depthBit.V);
set(ax(3), 'YDir', 'Reverse');
xlabel('Time');
ylabel('Depth [m]');
grid on;
title('Hole and bit depth');
datetick('x');

linkaxes(ax,'x');

c=get(hFig, 'children');
if numel(c)>1
    linkaxes([ax, c(2)], 'x');
else
    close(hFig);
end

axis tight;
end

