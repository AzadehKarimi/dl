function xDot = pflDyn( x,u,p,dt )
%PFLDYN Dynamics of pit and flow line
%   x=[volFlowLine volGainLoss qShaker]', u=[qIn qOutEstimate], p=paramstruct,
%   dt=timestep
xDot=zeros(3,1);

xDot(1)=max(0,u(2))*p.lpm2m3s - p.k*max(0,x(1)^p.m);
if u(1)<100 || u(2)<100
    xDot(2)=p.k*max(0,x(1)^p.m) - max(0,u(1))*p.lpm2m3s;
else
    xDot(2)=p.k*max(0,x(1)^p.m) - x(3)*p.lpm2m3s - max(0,u(1))*p.lpm2m3s;
end

end
