function shortName = getAlgorithmShortNamesFromClassName(algorithmClassName)
        shortName = lower(strrep(algorithmClassName,'_ALG_OBJ()','')); 
end