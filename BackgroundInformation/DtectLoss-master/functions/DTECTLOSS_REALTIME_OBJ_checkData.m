function [textResult] =  DTECTLOSS_REALTIME_OBJ_checkData()
%
% runs the describe.checkdataseries method to check if dataset is ok
% returns a long text array with output describing the maximums and
% minimums observed in the dataset,
% (it is not intended that the this data needs to be retained by 
%  dtectloss.statoil.no web-portal, used for backtesting.)
%
% returns: names :

global GLOBAL_DTECTLOSS_REALTIME_OBJ_obj

[GLOBAL_DTECTLOSS_REALTIME_OBJ_obj, textResult] = GLOBAL_DTECTLOSS_REALTIME_OBJ_obj.checkData();
end


