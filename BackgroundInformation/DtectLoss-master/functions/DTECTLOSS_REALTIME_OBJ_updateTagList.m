function DTECTLOSS_REALTIME_OBJ_updateTagList(rawDataUnits)
%function DTECTLOSS_REALTIME_OBJ_updateTagList(rawDataUnits)
% If tags are added or removed, then updateTagList must be used to make
% sure that the conversion factors and namespace is updated.
% 
% input:
% rawDataUnits : struct with units of all the variables to be provided
% example:
% rawDataUnits =
%             flowIn: 'lpm'
%            flowOut: 'lpm'
%     flowOutPercent: '%'
%                rpm: 'rpm'
%           velBlock: 'm/s'
%             velRop: 'm/h'
%             presSP: 'bar'
%          depthHole: 'm'
%           depthBit: 'm'
%        volGainLoss: 'm3'
%         heightHook: 'm'
%        volTripTank: 'm3'
%
% output: no output

global GLOBAL_DTECTLOSS_REALTIME_OBJ_obj

GLOBAL_DTECTLOSS_REALTIME_OBJ_obj = GLOBAL_DTECTLOSS_REALTIME_OBJ_obj.updateTagList(rawDataUnits);

end
