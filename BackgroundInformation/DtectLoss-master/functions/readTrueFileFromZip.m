function outData = readTrueFileFromZip(fileNameZip)
fprintf('reading data from %s...\n',fileNameZip );
t_handle    = tic ;
fileNames    = unzip(fileNameZip);
hasFoundTrueFile=false;
for k= 1:1:numel(fileNames)
    % read data from the "TRUE" file
    if ~isempty(strfind(lower(fileNames{k}),'true')) 
        outData = readDataFromCsv(fileNames{k});
        hasFoundTrueFile=true;
    end
end

if ~hasFoundTrueFile
    warning('READTRUEFILEFROMZIP:noTrueFileFound', 'No true-file found within the zip-file %s', fileNameZip);
    outData=[];
end
delete(fileNames{:});
duration    = toc(t_handle);
fprintf('duration:%.2f seconds\n',duration);