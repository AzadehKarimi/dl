% The bactesting manager is in charge of orchestrating the back tests 
% of the DTECTLOSS_REALTIME class on historical data.
% the backtesting manager simulates a "live stream" of data by calling the
% FIELD_DATA_PLAYER and feeding the DTECTLOSS_REALTIME object one vector at
% a time. The backtesting manager runs through all the files of historical
% data in the data folder specified.

classdef BACKTESTING_MANAGER_OBJ < handle
	properties
        fieldDataPlayerDT_s = 1;
        processDataNames
        bs      %simscenario handle: hold the saved simulation results of the method
        fdp     % fieldplayer handle: loads and runs a timeseries collection one step at a time.
        curSimName % name to be used in plots and savefiles to identify this run
        dT  % timestep of current file
        pitThreshold
        flowDeadBand_lpm  
        volumeThreshold_m3  
        realtimeObj
        rawDataUnits =[];
        main_loop_counter
        paralell_pool
        isDebug;
        numScenarios;
    end
    
    
    properties (SetAccess = private)
        dataFiles;   % cell with the name of all the files to run through
        curScenarioIdx;
        nameDesiredProcessTags
        algClassNames

        rootDir
        configCell;
        curDataFileIdx;
        
        doWrapperTest;% if set to 1, the wrapper functions of DTECTLOSS_REALTIME_OBJ are tested
    end
	methods
        function obj    = BACKTESTING_MANAGER_OBJ()
            obj.dataFiles ;
            obj.curScenarioIdx;
            obj.processDataNames;
            obj.bs;
            obj.fdp;
            obj.dT ;
            obj.flowDeadBand_lpm  ;
            obj.volumeThreshold_m3  ;
            obj.pitThreshold   ;
            obj.rootDir ;
            obj.main_loop_counter;
            obj.algClassNames;
            obj.realtimeObj;
            obj.paralell_pool ;
            obj.configCell;
            obj.curDataFileIdx;
            obj.doWrapperTest;
        end
        % 
		function obj                = initalize(obj, root_dir,nameDesiredProcessTags,doWrapperTest,isDebug)
            obj.dataFiles           = returnEveryFileInDirectoryTree(root_dir,'.zip');
            obj.configCell          = readXLSconfig(root_dir);
            obj.curScenarioIdx      = 0;%+1 will be added by loadNextSim
            obj.nameDesiredProcessTags = nameDesiredProcessTags;
            obj.rootDir             = root_dir;
            obj.doWrapperTest       = doWrapperTest;
            obj.isDebug             = isDebug;
            obj.numScenarios        = (numel(obj.configCell))
            obj                     = obj.loadNextSim();% load the first simulation
        end
        
        % can involve either loading a new file or loading the next time
        % series collection
        function [obj , isDone]      = loadNextSim(obj)
            isDone                   = 0;
            obj.curScenarioIdx       = obj.curScenarioIdx+1;
            
            if obj.curScenarioIdx>obj.numScenarios
               isDone = 1;
               return;
            end
            
            if obj.configCell(obj.curScenarioIdx).useForBacktesting ~=1
                disp('useForBacktesting not set to 1, skipping.... ');
               loadNextSim(obj);%skip scenarios 
               return;
            end

            obj.curDataFileIdx = nan;
            numFilesFound =0;
            for k= 1:1:numel(obj.dataFiles)
               if ~isempty( strfind(obj.dataFiles{k},sprintf('-%i.',obj.curScenarioIdx)))
                   obj.curDataFileIdx = k;
                   numFilesFound = numFilesFound+1;
               end
            end
            if numFilesFound~=1
               fprintf('****Warning: found %i files with filename with index -%i., skipping.....*******************\n',numFilesFound,obj.curScenarioIdx);
               obj.loadNextSim();
               return;
            end
            
            obj.curSimName          = sprintf('%s',strrep(strrep(obj.dataFiles{obj.curDataFileIdx},'.mat',''),'.zip',''));
            currentDataFile         = fullfile(obj.rootDir ,obj.dataFiles{obj.curDataFileIdx});
            obj.fdp                 = FIELD_DATA_PLAYER_OBJ(currentDataFile,obj.fieldDataPlayerDT_s,obj.nameDesiredProcessTags);
            obj.fdp                 = obj.fdp.initialize;
            obj.main_loop_counter   = 1;
            
            if obj.doWrapperTest ==1
                DTECTLOSS_REALTIME_OBJ_construct();
                DTECTLOSS_REALTIME_OBJ_init(obj.configCell(obj.curScenarioIdx) ,obj.fdp.rawDataUnits, obj.fdp.varNames);
            else
                obj.realtimeObj = DTECTLOSS_REALTIME_OBJ();
                obj.realtimeObj = obj.realtimeObj.init(obj.configCell(obj.curScenarioIdx) ,obj.fdp.rawDataUnits, obj.fdp.varNames);
            end
            obj.dT          = obj.fdp.dT;
            obj.bs          = BACKTEST_SCENARIO_OBJ ();
            obj.bs          = obj.bs.initalize(obj.curSimName,obj.isDebug);     
            disp(['****************************' ,obj.curSimName ,'****************************']);
            
        end
        
        %write statistics to comment.txt and makes backtesting graph in
        %folder on g-disk
        function [obj] = PlotAndFinalizeCurrentSim(obj)
            disp('THIS CODE SEEM TO BE NOT IN USE...')
            comment = '';
           % TODO: refactor
%             for curAlg = 1:1:numel(obj.analysisObj);
%                 noOfAlarms = obj.analysisObj{curAlg}.noOfAlarms;
%                 comment = strcat(comment,sprintf('Algorithm %i (%s) : %i alarms \r\n',curAlg,obj.algShortname{curAlg},noOfAlarms  ));
%             end
%             disp(comment);
            obj.bs.evaluateCurrentRun(comment);
        end   
        %------------------------------------ 
        % run at each iteration to save the result of all algorithms 
        function [obj]  = StepAllAlgorithmsAndSave(obj)
             obj.main_loop_counter      = obj.main_loop_counter + 1;
             
             obj.fdp                    = obj.fdp.play; % iterate field data player
             rawData                    = obj.fdp.output;
        
             % run all algorithms one step forward (call in the same way as when running in real-time for testing)
             if obj.doWrapperTest ==1
                 dtectLossOutput = DTECTLOSS_REALTIME_OBJ_stepAllAlgorithms(rawData, obj.dT);
             else
                [obj.realtimeObj] = obj.realtimeObj.stepAllAlgorithms(rawData, obj.dT );
                dtectLossOutput  = obj.realtimeObj.dtectLossOutput ;
             end
             obj.bs.saveIteration(obj.fdp.absTime,dtectLossOutput); 
        end
        
	end
end













