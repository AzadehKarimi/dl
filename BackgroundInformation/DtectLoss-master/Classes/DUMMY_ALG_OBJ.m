classdef DUMMY_ALG_OBJ < EKD_ALGORITHM_OBJ
    %DUMMY_ALG_OBJ Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        params
        qIo_lpm
    end
    
    methods
        function obj=DUMMY_ALG_OBJ()
            obj.params=[];
            obj.qIo_lpm=[];
        end
        
        function obj=initialize(obj)
            obj.qIo_lpm=0;
        end
        
        function obj=stepIt(obj, flowIn, flowOut, pitVol, holeDepth, p_sp,bitDepth)
            obj.qIo_lpm=-1;
        end
    end
    
end

