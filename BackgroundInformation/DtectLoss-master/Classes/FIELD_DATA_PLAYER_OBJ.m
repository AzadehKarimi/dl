classdef FIELD_DATA_PLAYER_OBJ  < handle
    %FIELD_DATA_PLAYER_OBJ Play field data for test of EKD algorithms
    %   The field data player is a class which can play back field data
    %   read from zipped of unzipped csv-files. The data is played back
    %   with a time-step defined by dT [s]. The variables which should be
    %   available in the output when playing is given by the cell
    %   desiredVarNames.
    
    properties (SetAccess = private)
        dT; % [s] desired time-step for replay of data.
        ds; % data struct where field names are variable names. t and V is time and value, respectively.
        output; % struct with data which will be available when playing data
        
        fullFileName; % [char] full file name of file which has been loaded
        
        nDataPoints; % number of datapoints available for each of the variables
        currentIdx; % [array with integers] current index for each of the variables in the output, sorted as in varNames
        currentTime; %[array with serial time] current time for each of the variables in the output, sorted as in varNames
        
        varNames; % [cell] a cell with names of variables which are actually visible when playing data
        nVars; % number of variables.
        rawDataUnits; % struct containing raw data units as read from file
                
        simProgress; % [%] 0 - 100, a number which tells the progress of the simulation. It is 100 at completion. 
        simTime; % [s] simulated time = accumulate time-step
        startTime; % [serial time] first time instance in csv-file, assumed to be the same for all variables
        endTime; % [serial time] last time instance in cvs-file across all variables
        simDuration % [s] endTime - startTime in seconds
        absTime; % [serial time] startTime + simTime
        asyncDT; % [s] time-step used during asynchronous playback
        
        isCurrentDataSetComplete ; % is 1 if all the asked for tags are there, zero otherwise (may cause dataset to be skipped)
    end
    
    properties (GetAccess = private,SetAccess = private)
    
    end
    
    methods
        function obj = FIELD_DATA_PLAYER_OBJ(fileName, timeStep, desiredVarNames)
            
            % check if file is zipped and unzip if necessary
            [~,~,fExt] = fileparts(fileName);
            if strcmp(fExt,'.zip')
                % read data from csv file into struct
                 obj.ds=readDataFromZip(fileName);
            elseif strcmp(fExt,'.csv')
                % read data from csv file into struct
                obj.ds=readDataFromCsv(fileName);
            elseif strcmp(fExt,'.mat')
                load(fileName);
                obj.ds=outData;
            else
                error('FIELD_DATA_PLAYER_OBJ:UnknownFileFormat', 'The file extension %s is not handled', fExt);
            end
            
            obj.fullFileName = fileName;
            
            obj.dT          = timeStep;
            obj.nVars       = numel(desiredVarNames);
            
            dsFields=fieldnames(obj.ds); % name of fields in struct
            
            % check if the desired variables are available. If not, issue
            % warning. Return a cell with the subset of available
            % variables.            
            isAvailableVarNames=false(size(desiredVarNames));
            
            for ii=1:obj.nVars
                for jj=1:numel(dsFields)
                    if strcmp(desiredVarNames{ii},dsFields{jj})
                        isAvailableVarNames(ii)=true;
                    end
                end
            end
            % display a warning if a variable is not found
            if any(~isAvailableVarNames)
                for missingVarName=desiredVarNames(~isAvailableVarNames)
                    obj.isCurrentDataSetComplete  = 0;%running
                    warning('FIELD_DATA_PLAYER_OBJ:desiredVariableNotAvailable', 'The variable %s was not found among the read data', missingVarName{:})
                end
            else
                obj.isCurrentDataSetComplete = 2; % 2=skipped
            end
            
            obj.varNames        = desiredVarNames(isAvailableVarNames);
            obj.nVars           = numel(obj.varNames); % the number of available variables might be less than the number of desired.
            % determine the maximum number of data points
            obj.nDataPoints     = zeros(obj.nVars,1);
            for kk=1:obj.nVars
                obj.nDataPoints(kk)=size(obj.ds.(obj.varNames{kk}).t,1);
                % check if unit is missing
                if isempty(strrep(obj.ds.(obj.varNames{kk}).unit, ' ', '')) % remove whitespace and check if empty
                    warning('FIELD_DATA_PLAYER_OBJ:missingUnit', 'The variable %s has no unit', obj.varNames{kk})
                    obj.rawDataUnits.(obj.varNames{kk})='';
                else
                    obj.rawDataUnits.(obj.varNames{kk})=obj.ds.(obj.varNames{kk}).unit;
                end
            end
            
            obj.simTime = 0;
            obj.currentIdx  = ones(obj.nVars,1);
            obj.currentTime = zeros(obj.nVars,1);
            
            obj.output=[];
            obj.startTime=[];
            obj.absTime=[];
            obj.simProgress=[];
            obj.endTime=[];
            obj.simDuration=[];
        end
        
        function obj=initialize(obj)
            % initialize the output with the first non-NaN value from the
            % data struct
            obj.output=struct();
            obj.endTime=0;
            for ii=1:obj.nVars
                % currently no check on non-NaN or erronous values.
                obj.output.(obj.varNames{ii}).t=obj.ds.(obj.varNames{ii}).t(1);
                obj.output.(obj.varNames{ii}).V=obj.ds.(obj.varNames{ii}).V(1);
                % update currentTime vector
                obj.currentTime(ii)=obj.output.(obj.varNames{ii}).t;
                % determine endTime
                obj.endTime=max(obj.endTime, obj.ds.(obj.varNames{ii}).t(obj.nDataPoints(ii)));
            end
            
            % start time is the first time instance from the csv-file
            obj.startTime=obj.output.(obj.varNames{1}).t;
            obj.absTime=obj.startTime;
            
            obj.simProgress=0;
            obj.simDuration=max(obj.dT,floor((obj.endTime-obj.startTime)*86400)); % avoid zero;
        end
        
        function obj=play(obj)
            % function which iterates with a fixed time-step (dT [s]) and
            % holds output values in case of missing values (zero order
            % hold).
            
            % iterate time
            obj.simTime=obj.simTime+obj.dT;
            obj.absTime=obj.absTime+obj.dT/86400;
            
            % update progress
            obj.simProgress=obj.simTime/obj.simDuration*100;
            
            for ii=1:obj.nVars
                % currently no check on non-NaN or erronous values.
                
                if obj.currentIdx(ii)<obj.nDataPoints(ii) 
                    % because of floating point errors,  a trivial check
                    % "(obj.absTime-obj.ds.(obj.varNames{ii}).t(obj.currentIdx(ii)+1)>=0"
                    % this was worked around using round, but to speed
                    % things up a sub-second tolerance check is done
                    % instead.
                    doesNewerDataBeforeAbsTimeExistForVariable  =...
                        (obj.absTime-obj.ds.(obj.varNames{ii}).t(obj.currentIdx(ii)+1))>=-1.157407407407407e-06;% 0.1/86400=(0.1 second tolerance)
                    if doesNewerDataBeforeAbsTimeExistForVariable
                        obj.currentIdx(ii) = obj.currentIdx(ii)+1;
                        obj.output.(obj.varNames{ii}).t=obj.ds.(obj.varNames{ii}).t(obj.currentIdx(ii));
                        obj.output.(obj.varNames{ii}).V=obj.ds.(obj.varNames{ii}).V(obj.currentIdx(ii));
                        obj.currentTime(ii)=obj.output.(obj.varNames{ii}).t;
                    end
                end
            end
            
        end
        
        function obj=asyncPlay(obj)
            % play back data asynchronously, and not with a fixed time-step
            % (asyncDT is updated by this function and reflect the duration
            % of the previous time-step).
            
            % determine if it is possible to make a step ahead in time for
            % each of the variables.
            possibleNextTime=obj.currentTime;
            possibleStep=ones(obj.nVars,1);
            
            for ii=1:obj.nVars
                if obj.currentIdx(ii)<obj.nDataPoints(ii)
                    possibleNextTime(ii)=obj.ds.(obj.varNames{ii}).t(obj.currentIdx(ii)+1);
                end
            end
            
            % call function which updates the absolute time and provide
            % updated indicies for each variable.
            [minT, newIdx]=obj.asyncStep(possibleNextTime, possibleStep,obj.currentIdx, possibleNextTime>obj.currentTime);
            
            obj.asyncDT=(minT-obj.absTime)*86400; % time-step 
            obj.absTime=minT; % update absolute time from asyncStep
            
            % update variables which are stepped ahead in time.
            for ii=1:obj.nVars
                if newIdx(ii)>obj.currentIdx(ii)
                    if obj.ds.(obj.varNames{ii}).t(obj.currentIdx(ii))<=obj.absTime
                        obj.output.(obj.varNames{ii}).t=obj.ds.(obj.varNames{ii}).t(newIdx(ii));
                        obj.output.(obj.varNames{ii}).V=obj.ds.(obj.varNames{ii}).V(newIdx(ii));
                    end
                elseif obj.ds.(obj.varNames{ii}).t(obj.currentIdx(ii))>obj.absTime
                    obj.output.(obj.varNames{ii}).t=obj.absTime;
                    obj.output.(obj.varNames{ii}).V=-0.99925;
                end
                obj.currentTime(ii)=obj.output.(obj.varNames{ii}).t;
            end
            obj.currentIdx=newIdx;
            
            % iterate time
            obj.simTime=round((obj.absTime-obj.startTime)*86400);
            
            % update progress
            obj.simProgress=obj.simTime/obj.simDuration*100;
        end
        
        function formattedHeader = formatHeader(obj)
            formattedHeader = 'time,';
            
            for ii = 1 : obj.nVars - 1
                formattedHeader = strcat(formattedHeader, strcat(obj.varNames{ii}, ', '));
            end
            formattedHeader = strcat(formattedHeader, strcat(obj.varNames{ii + 1}, '\n'));
        end
        
        function formattedOutput = formatOutput(obj)
            timeStamp = obj.genZuluTimeString(obj.absTime, 0);
            formattedOutput = strcat(timeStamp, ',');
            
            for ii = 1 : obj.nVars - 1
                formattedOutput= strcat(formattedOutput, strcat(sprintf('%f', obj.output.(obj.varNames{ii}).V), ', '));
            end
            formattedOutput= strcat(formattedOutput, strcat(sprintf('%f', obj.output.(obj.varNames{ii + 1}).V), '\n'));
        end
    end
    
    methods (Static)
        function [minT, newIdx]=asyncStep(possibleNextTime, possibleStep, idx, idxNotAtEnd)
            % find the smallest of the available timestamp among the
            % variables and return this time (minT), and update indicies
            % for variables with this timestamp (newIdx).
            
            newIdx=idx;
            if any(idxNotAtEnd) % if there are indicies which can be updated
                minT=min(possibleNextTime(idxNotAtEnd)); % find min time
                pbt=possibleNextTime==minT;
                newIdx(idxNotAtEnd & pbt)=...
                    newIdx(idxNotAtEnd & pbt)+possibleStep(idxNotAtEnd & pbt); % iterate indicies which are not at end and equals minT
            else
                minT=max(possibleNextTime); % find min time
            end
        end
        
        function ts = genZuluTimeString(t, tOffset)
            % generate a time string in proper format, from serial time.
            ds = datevec(t+tOffset);
            ts = sprintf('%04g-%02g-%02gT%02g:%02g:%02gZ', ds);
        end
    end
    
end

