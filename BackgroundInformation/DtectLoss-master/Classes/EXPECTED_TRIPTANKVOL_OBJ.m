classdef EXPECTED_TRIPTANKVOL_OBJ < handle
% 
% this class calculates dill pipe area areaDP_m2 (open/closed end), so that:
% 
% d/dt volTripTank_m3(t) = areaDP_m2*velBlock_mph(t)/3600 
% to do that this class buffers data during tripping.

    properties(SetAccess = public)
        %Updated after every stand
        numStand;
        numOKStand;
        volTTLastStand_m3;
        depthLastStand_m;
        volSteelLastStand_m3;
        volTTdiff_m3;
        lengthLastStand_m;
        lengthStandAverage_m;
        areaDPLastStand_m2;
        areaDPAverage_m2;
        volTTcum_m3;
        volTTcumExp_m3;
        volTTcumErr_m3;
        timeLastStand_d;
        
        %Updated every new depth sample
        depthBitPrev_m;%previous value
        volTTPrev_m3; %previous value
        depthDelta_m;
        
        %Logic values
        stateTripping; % 1=RIH, 0=OnBottom/Steady, -1=POOH
        isOnBottom; 
        isPipeSteady; % 1=steady, 0=is moving
        isvolTTSteady; % 1=steady, 0=is changing
        isNewStandTripTank;        
        isOKLastStand;
        params; %Tuning params
        
        %Config params
        areaDPPOOH_m2; 
        areaDPRIH_m2; 
    end
    
    properties (SetAccess = private,GetAccess = private)
        volTripTankBufferObj;                      
        depthBitBufferObj;
        timePrevDepth_d;
        timePrevVol_d;
        dtDepth_s;
        dtVol_s;
        doDebugText;
    end
    
    properties (Constant)
        BUFFER_SIZE_SAMPLES                 = 1000;
        flowInMaxMinThreshold_lpm           = 100;
        NvaluesforSteady                    = 5;
        NvaluesforSteadyLong                = 15*60/2;
        depthMaxMinThreshold_m              = 23;%Minimum length since last calibration
        volMaxMinThreshold_m3               = 0.5*20*3.14/4*(5^2-4^2)*0.0254^2;%Minimum volume change since last calibration       
        factor_forget                       = 1/5;
        timeInjection_s                     = 6000;%600
        velMaxROP_mph                       = 100;%Tripping defined as bit depth changes faster than this + off bottom
        flowTTMax_lpm                       = 100;%Steady TT volume defined as less flow than this in/out of tank
    end
    
	methods
        function obj    = EXPECTED_TRIPTANKVOL_OBJ()
        end
        
        function obj    = initTrip(obj)
            obj.numStand                        = 0;
            obj.numOKStand                      = 0;
            obj.volTTLastStand_m3               = 0;
            obj.depthLastStand_m                = 0; 
            obj.volSteelLastStand_m3            = 0;
            obj.isOKLastStand                   = false;
%            obj.isNewStandTripTank              = false;
            obj.volTTdiff_m3                    = 0;
            obj.lengthLastStand_m               = 0;
            obj.areaDPLastStand_m2              = 0;
            obj.areaDPAverage_m2                = 0;
            obj.volTTcum_m3                     = 0;
            obj.volTTcumExp_m3                  = 0;
            obj.volTTcumErr_m3                  = 0;
            obj.timeLastStand_d                 = 0;           
            obj.lengthStandAverage_m            = 0;            
        end

        function obj    = setAreadp(obj, areaDPPOOH_m2, areaDPRIH_m2)
            obj.areaDPPOOH_m2                   = areaDPPOOH_m2; 
            obj.areaDPRIH_m2                    = areaDPRIH_m2; 
        end
        
        function obj    = initialize(obj, params)
            obj.params                          = params;    
            obj.areaDPPOOH_m2                   = 0.01;%Default values 
            obj.areaDPRIH_m2                    = 0.02;%Default values  
            %Updated every new depth sample
            obj.volTTPrev_m3                    = 0;
            obj.depthBitPrev_m                  = 0;
            obj.depthDelta_m                    = 0;
            obj.timePrevDepth_d                 = 0;
            obj.timePrevVol_d                   = 0;
            obj.dtDepth_s                       = 0;
            obj.dtVol_s                         = 0;
            obj.isOnBottom                      = false; 
            obj.isPipeSteady                    = true; % 1=steady, 0=is moving
            obj.isvolTTSteady                   = true;
            obj.stateTripping                   = 0; % 1=RIH, 0=OnBottom/Steady, -1=POOH
            obj.isNewStandTripTank              = false;
            ibj.isLastStandOK                   = false;
            %Updated every new stand
            obj                                 = obj.initTrip;            
            obj.depthBitBufferObj               = RINGBUFFER_OBJ(obj.BUFFER_SIZE_SAMPLES);
            obj.volTripTankBufferObj            = RINGBUFFER_OBJ(obj.BUFFER_SIZE_SAMPLES);

            obj.doDebugText                     = false;%false;            
        end

        
        function obj    = reset(obj)
            obj                                 = obj.initTrip;            
            obj.volTTPrev_m3                    = 0;
            obj.depthBitPrev_m                  = 0;
            obj.depthDelta_m                    = 0;
            obj.timePrevDepth_d                 = 0;
            obj.timePrevVol_d                   = 0;
            obj.dtDepth_s                       = 0;
            obj.dtVol_s                         = 0;
            obj.isOnBottom                      = false; 
            obj.isPipeSteady                    = true; % 1=steady, 0=is moving
            obj.isvolTTSteady                   = true;
            obj.stateTripping                   = 0; % 1=RIH, 0=OnBottom/Steady, -1=POOH
            obj.isNewStandTripTank              = false;
            ibj.isLastStandOK                   = false;
        end
        
        function obj    = resetTripVol(obj)
            obj.volTTcumExp_m3 = obj.volTTcum_m3;
            obj.volTTcumErr_m3 = 0;
        end
              
        function [obj, answer] = haveWeDrilledAnotherStandNow(obj, depthBit_m, volTripTank_m3)
            %1: Check if pipe is moving or not and if volume has changed
            depthVec      = obj.depthBitBufferObj.getLastNValues(obj.NvaluesforSteady);
            timeVec_d     = obj.depthBitBufferObj.getLastNTimes(obj.NvaluesforSteady);
            ixDepth       = find(depthVec > 0);
            depthVec      = depthVec(ixDepth);
            timeVec_d     = timeVec_d(ixDepth);
            volVec        = obj.volTripTankBufferObj.getLastNValues(obj.NvaluesforSteady);
            ixVol         = find(volVec > 0);
            volVec        = volVec(ixVol);
            depthMaxMin_m = max(depthVec)      - min(depthVec);
            volMaxMin_m3  = max(volVec)        - min(volVec);
            timeMaxMin_h  = 24*(max(timeVec_d) - min(timeVec_d));
            if (isempty(timeMaxMin_h) || isempty(depthMaxMin_m) || isempty(volMaxMin_m3)) 
                answer = false;
                return;                
            elseif (timeMaxMin_h > 0)
                obj.isPipeSteady   = (         depthMaxMin_m/timeMaxMin_h < obj.velMaxROP_mph);
                obj.isvolTTSteady  = (volMaxMin_m3*1000/(timeMaxMin_h*60) < obj.flowTTMax_lpm);
            else
                answer = false;
                return;
            end
            
            %2: Check if depth has changed more than 20m since last stand
            hasDepthChangedEnough = abs(depthBit_m     - obj.depthLastStand_m)  > obj.depthMaxMinThreshold_m;         
            hasVolTTChangedEnough = true;%XXX To be changed? abs(volTripTank_m3 - obj.volTTLastStand_m3) > obj.volMaxMinThreshold_m3;         
            answer                = (obj.isPipeSteady && hasDepthChangedEnough && hasVolTTChangedEnough); % && obj.isvolTTSteady
        end
                
        function TripTankOutput = setOutput(obj)
            TripTankOutput.numStand               = obj.numStand; 
            TripTankOutput.numOKStand             = obj.numOKStand;             
            TripTankOutput.volTTLastStand_m3      = obj.volTTLastStand_m3; 
            TripTankOutput.depthLastStand_m       = obj.depthLastStand_m; 
            TripTankOutput.isOKLastStand          = obj.isOKLastStand; 
            TripTankOutput.isNewStandTripTank     = obj.isNewStandTripTank;             
            TripTankOutput.volTTdiff_m3           = obj.volTTdiff_m3; 
            TripTankOutput.lengthLastStand_m      = obj.lengthLastStand_m;                 
            TripTankOutput.lengthStandAverage_m   = obj.lengthStandAverage_m;
            TripTankOutput.volSteelLastStand_m3   = obj.volSteelLastStand_m3;
            TripTankOutput.areaDPLastStand_m2     = obj.areaDPLastStand_m2; 
            TripTankOutput.areaDPAverage_m2       = obj.areaDPAverage_m2; 
            TripTankOutput.volTTcum_m3            = obj.volTTcum_m3; 
            TripTankOutput.volTTcumExp_m3         = obj.volTTcumExp_m3; 
            TripTankOutput.volTTcumErr_m3         = obj.volTTcumErr_m3; 
            TripTankOutput.timeLastStand_d        = obj.timeLastStand_d; 
            TripTankOutput.depthDelta_m           = obj.depthDelta_m;                
            TripTankOutput.stateTripping          = int32(obj.stateTripping); 
            TripTankOutput.isOnBottom             = obj.isOnBottom; 
            TripTankOutput.isPipeSteady           = obj.isPipeSteady; 
            TripTankOutput.isvolTTSteady          = obj.isvolTTSteady; 
        end                
        
        function [obj, TripTankOutput] = step(obj, depthBit_m, depthHole_m, volTripTank_m3, flowIn_lpm, timeNew_d)            
            %TripTankOutput = obj.setOutput();
            if (depthBit_m == -0.99925)
                TripTankOutput = obj.setOutput;
                return;
            end                       
            obj.dtDepth_s                              = (timeNew_d - obj.timePrevDepth_d)*86400;
            obj.depthDelta_m                           = depthBit_m - obj.depthBitPrev_m; 
            obj.timePrevDepth_d                        = timeNew_d;
            obj.depthBitPrev_m                         = depthBit_m;
            obj.dtVol_s                                = (timeNew_d - obj.timePrevVol_d)*86400;
            
            if not(volTripTank_m3 == -0.99925)
                obj.volTTPrev_m3                       = volTripTank_m3;
                obj.timePrevVol_d                      = timeNew_d;
            end

            persistent timeOld_d;
            if isempty(timeOld_d)
                timeOld_d = timeNew_d;
            end
            
            if abs(obj.dtDepth_s) > 60 || abs(obj.depthDelta_m) > 100
                %First sample
                obj.dtDepth_s     = 0;
                obj.depthDelta_m  = 0;
                TripTankOutput    = obj.setOutput;
                return;
            end
            
            obj.depthBitBufferObj        = obj.depthBitBufferObj.addDataToCircularBuffer(timeNew_d, depthBit_m);
            if not (volTripTank_m3 == -0.99925)
                obj.volTripTankBufferObj = obj.volTripTankBufferObj.addDataToCircularBuffer(timeNew_d, volTripTank_m3);
            end
           
            %State machine
            if obj.isOnBottom
                obj.isOnBottom = (depthHole_m - depthBit_m < 30); 
                if not(obj.isOnBottom) %Reset when we go off bottom
                    obj        = obj.initTrip;
                end
            else
                obj.isOnBottom = ((depthHole_m - depthBit_m < 30) && (flowIn_lpm > 200));
            end            
            
            if obj.isOnBottom
                obj.stateTripping = 0;
            end
            
            if obj.isOnBottom || (volTripTank_m3 == -0.99925)
                obj.isNewStandTripTank = false;                
            else
                [obj, obj.isNewStandTripTank] = obj.haveWeDrilledAnotherStandNow(depthBit_m, volTripTank_m3);
            end
            
            %State machine
            if obj.isNewStandTripTank
                obj.timeLastStand_d = timeNew_d;
                if (obj.numStand == 0) && (obj.depthLastStand_m == 0)%First stand
                    isnewtrip = true;
                    if (depthBit_m < obj.depthLastStand_m)
                        obj.stateTripping = -1;%POOH                    
                    elseif (depthBit_m > obj.depthLastStand_m)
                        obj.stateTripping = +1;%RIH                    
                    else
                        disp('?');
                    end
                elseif depthBit_m > obj.depthLastStand_m %RIH
                    isnewtrip = not(obj.stateTripping == +1);%Change of direction from POOH to RIH or from onBottom
                    obj.stateTripping = 1;%RIH
                    obj.isOKLastStand = (abs(depthBit_m  - obj.depthLastStand_m) < 40) && abs(volTripTank_m3 - obj.volTTLastStand_m3) < 3;%no empty TT && (volTripTank_m3 > obj.volTTLastStand_m3) 
                else %POOH
                    isnewtrip = not(obj.stateTripping == -1);%Change of direction from RIH to POOH or from onBottom
                    obj.stateTripping = -1;%POOH
                    obj.isOKLastStand = (abs(depthBit_m  - obj.depthLastStand_m) < 40) && abs(volTripTank_m3 - obj.volTTLastStand_m3) < 3;%no refill TT && (volTripTank_m3 < obj.volTTLastStand_m3) 
                end
                
                if isnewtrip
                    obj                    = obj.initTrip;
                    obj.depthLastStand_m   = depthBit_m;
                    obj.volTTLastStand_m3  = volTripTank_m3;
                    obj.timeLastStand_d    = timeNew_d;
                else %Update
                    obj.lengthLastStand_m               = abs(depthBit_m     - obj.depthLastStand_m);
                    obj.depthLastStand_m                = depthBit_m;
                    obj.volTTdiff_m3                    = (volTripTank_m3 - obj.volTTLastStand_m3);
                    obj.volTTLastStand_m3               = volTripTank_m3;
                    if obj.stateTripping == -1
                        obj.volSteelLastStand_m3        = obj.lengthLastStand_m*obj.areaDPPOOH_m2;
                    elseif obj.stateTripping == +1
                        obj.volSteelLastStand_m3        = obj.lengthLastStand_m*obj.areaDPRIH_m2;
                    end
                    obj.volTTcum_m3                     = obj.volTTcum_m3    + obj.isOKLastStand*obj.volTTdiff_m3;
                    obj.volTTcumExp_m3                  = obj.volTTcumExp_m3 + obj.isOKLastStand*obj.stateTripping*obj.volSteelLastStand_m3;
                    obj.volTTcumErr_m3                  = obj.volTTcum_m3    - obj.volTTcumExp_m3;                    

                    obj.areaDPLastStand_m2 = obj.stateTripping*obj.volTTdiff_m3/obj.lengthLastStand_m;
                    obj.numStand           = obj.numStand + 1;
                    
                    if obj.isOKLastStand
                        obj.numOKStand           = obj.numOKStand + 1;
                        forget                   = max(obj.factor_forget, 1/(obj.numOKStand));
                        obj.areaDPAverage_m2     = (1 - forget)*obj.areaDPAverage_m2     + forget*obj.areaDPLastStand_m2;
                        obj.lengthStandAverage_m = (1 - forget)*obj.lengthStandAverage_m + forget*obj.lengthLastStand_m;
                        if obj.lengthStandAverage_m < 23 || obj.lengthLastStand_m < 23
                            disp(['Lenght last stand =' num2str(obj.lengthLastStand_m,'%.1f') ', average = ' num2str(obj.lengthStandAverage_m,'%.1f')]);
                        end
                        if obj.doDebugText 
                            disp(['Updated average area of drill pipe = ' num2str(obj.areaDPLastStand_m2,'%.4f') 'm2, filtered value = ' num2str(obj.areaDPAverage_m2,'%.4f') 'm2']);
                            disp(['Updated average length of stand = '    num2str(obj.lengthLastStand_m,'%.1f') 'm, filtered value = ' num2str(obj. lengthStandAverage_m,'%.1f') 'm']);
                        end
                    end
                end
            else %Not new stand
                obj.volSteelLastStand_m3 = 0;
            end                       
            
            TripTankOutput = obj.setOutput;
            timeOld_d      = timeNew_d;            
        end
    end
end