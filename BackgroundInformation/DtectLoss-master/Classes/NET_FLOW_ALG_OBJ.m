classdef NET_FLOW_ALG_OBJ < EKD_ALGORITHM_OBJ
    %NET_FLOW_ALG_OBJ Calculate the net flow in the well
    %   For testing purposes
    
    properties
        params
        qIo % flow rate of in/outflux
        VIo % Volume of in/outflux
        sysState % state: not circulating, circulating, or drilling
    end
    
    methods
        function obj=NET_FLOW_ALG_OBJ()
            obj.params=[];
            obj.qIo=[];
            obj.sysState=SYSTEM_STATES_OBJ(); % the state machine
        end
        
        function obj=initialize(obj)
            obj.qIo=0;
            obj.VIo=0;
        end
        
        function obj=stepIt(obj, flowIn, flowOut, pitVol, holeDepth, bitDepth)
            obj.sysState=obj.sysState.stateMachine(flowIn,bitDepth,holeDepth);
            if obj.sysState.currentState==DrillingStates.notCirculating
                obj.qIo=0;
            else
                obj.qIo=flowOut-flowIn;
            end
        end
    end
    
end
