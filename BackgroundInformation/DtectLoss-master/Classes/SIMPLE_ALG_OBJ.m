% this is a very simple DtectLoss algorithm that is intended 
% to be just for illustration purposes and comparison.

classdef SIMPLE_ALG_OBJ < EKD_ALGORITHM_OBJ
    %DUMMY_ALG_OBJ Summary of this class goes here
    %   Detailed explanation goes here
    
    properties

    end
    
    properties (SetAccess = private)
        flowIo_lpm
        volIo_m3
        alarmState;
        params;
        timeCurrent_d;
        timeLast_d;
        dt_s;
        alarmStatus;
    end
    
    properties (Constant)
        timeStep_max_s  = 60;
    end
    
    methods
        function obj=SIMPLE_ALG_OBJ()
            obj.params      = [];
            obj.flowIo_lpm  = [];
            obj.volIo_m3    = [];
        end
        
        function obj=initialize(obj,params)
            obj.flowIo_lpm      = 0;
            obj.volIo_m3        = 0;
            obj.alarmState      = 0;
            obj.timeCurrent_d   = 0;
            obj.timeLast_d      = 0;
            obj.dt_s            = 0;
            obj.params      = params;           
        end
        
        function [obj, result]=stepIt(obj,  processData,expData,sysState, timeCurrent_d,dt_s)
            obj.timeCurrent_d   = timeCurrent_d;
            obj.dt_s            = dt_s;
           % obj.dt_s            = (obj.timeCurrent_d - obj.timeLast_d)*86400;
            obj.timeLast_d      = obj.timeCurrent_d;
            if (obj.dt_s <= 0),%do nothing
                obj.alarmStatus = ['Do nothing, time step = ' num2str(obj.dt_s,'%.2f') ' sec'];
            elseif obj.dt_s > obj.timeStep_max_s,%reset
                obj.alarmStatus = ['Reset, time step = ' num2str(obj.dt_s,'%.2f') ' sec'];
                obj             = initialize(obj,obj.params);
                obj.alarmState  = -1;
            else %ok - step alg            
                flowIn_lpm         = processData.flowIn_lpm;
                flowOut_lpm        = processData.flowOut_lpm;

                if(sysState.currentState == DrillingStates.Drilling )
                    LITERS_TO_M3    = 1/1000;
                    MIN_TO_SEC      = 1/60;
                    obj.flowIo_lpm  = flowOut_lpm - flowIn_lpm; 
                    if abs(obj.flowIo_lpm) > obj.params.flowDeadband_lpm,
                        obj.volIo_m3    = obj.volIo_m3 +obj.dt_s*obj.flowIo_lpm*LITERS_TO_M3*MIN_TO_SEC;% vIo[m3] = dt[s] *qIo[lpm]
                    else
                        obj.volIo_m3    = 0;
                    end
                else
                    obj.flowIo_lpm      = 0;
                    obj.volIo_m3        = 0;
                end
                obj                  = obj.setAlarmState(sysState);
            end
            [obj, result] = obj.returnResultStruct();
        end
        
        %determine if an alarm has occured
        function obj=setAlarmState(obj,sysState)
             if(sysState.currentState == DrillingStates.Drilling )
                if abs(obj.volIo_m3) > obj.params.volumeThresholdAlarm_m3,
                    obj.alarmState = AlarmState.Alarm;
                elseif abs(obj.volIo_m3) > obj.params.volumeThresholdWarning_m3,
                    obj.alarmState = AlarmState.Warning;
                else
                    obj.alarmState = AlarmState.Normal;
                end
             else
                 obj.alarmState     = AlarmState.Disabled;
             end
        end
        
        function  [obj, result ] = returnResultStruct(obj)
            result.flowIO_lpm   = obj.flowIo_lpm ;
            result.volIO_m3     = obj.volIo_m3;
            result.alarmState   = obj.alarmState;
        end
        
        
    end
    
end

