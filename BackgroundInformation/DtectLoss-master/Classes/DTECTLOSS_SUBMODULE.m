classdef (Abstract) DTECTLOSS_SUBMODULE
    %DTECTLOSS_SUBMODULE Abstract class describing functionality for the
    %subclasses used in DTECTLOSS_REALTIME_OBJ.
    %   Detailed explanation goes here
    
    methods (Abstract)
        obj = softReset(obj)
    end
    
end

