classdef DRILLSIMULATOR_OBJ
    %
    %   simulator class that sim solves dynamic equations for     
    %   conventional drilling incl ROP/WOB and with/without loss
    %   test  
    properties %states
        p_p_bar     ; % simulated state: pump pressure.
        p_bit_bar   ; % simulated state: pressure at bit
        p_bh_bar    ; % simulated state: pressure bottom hole
        p_cut_bar   ; % simulated state: weight of cuttings in annulus

        dp_dp_bar   ; % simulated state: pressure loss drill pipe
        dp_bit_bar  ; % simulated state: pressure loss bit  
        dp_ann_bar  ; % simulated state: pressure loss annulus

        q_p_lpm     ; % simulated state: flow from rig pumps - input
        q_bit_lpm   ; % simulated state: flow through bit
        q_top_lpm   ; % simulated state: flow out of annulus from well
        q_tt_lpm    ; % simulated state: flow from trip tank pump - input
        q_out_lpm   ; % simulated state: flow out to flow line and flow meter
        q_out_pc    ; % simulated state: paddle.
        q_pit_lpm   ; % simulated (derived) variable: flow from flowline to pit
        q_shaker_lpm; % simulated (derived) variable: flow from flowline to shakers (with cuttings)

        V_fl_m3     ; % simulated state: volume in the flowline
        V_well_m3   ; % simulated state: volume in the well
        V_pit_m3    ; % simulated state: volume in the pit 
        V_tt_m3     ; % simulated state: trip tank volume

        F_hl_ton    ; % simulated state: hookload
        F_wob_ton   ; % simulated state: weight on bit
        F_drag_ton  ; % simulated state: drag - friction force when moving pipe

        bitVel_mph  ; % simulated state: bit velocity/rop
        bitDepth_m  ; % simulated state: bit depth
        blockVel_mph; % simulated state: block velocity - input
        blockPos_m  ; % simulated state: block position
        holeDepth_m ; % simulated state: hole depth
        lastConnectionDepth_m; % simulated state: last depth where pipe was disconnected

        rotation_rpm; % simulated state: drill pipe rotation
        dbdV_barm3  ; % simulated state: mud compressibility divided by well volume 
        qIo_lpm     ; % simulated state: true influx/loss rate
        VIo_m3      ; % simulated state: true influx/loss volume

        onBottom_b  ; % simulated state: boolean, true if onbottom drilling
        drillState  ; % simulated state: drilling, pull off bottom, etc
        autoDrill   ; % simulated state: 0 = off, const bvel, 1 = const wob, 2=const pos
        ufl_b       ; % simulated state: flow line valve, 1=open normally, 0=closed during tripping and flow checks, when return flow is directed to trip tank
        vel_max_mph ; % simulated state: max tripping speed/rop
        
        seqNumber   ; % Number in sequence of simulated states to do
        
        ref_wob_ton ; % simulated state: reference for wob control/autodrill - input
        ref_wobf_ton; % simulated state: filtered reference for wob control/autodrill 
        ref_pos_m   ; % simulated state: reference for pos control/autodrill - input
        ref_posf_m  ; % simulated state: filtered reference for pos control/autodrill 
        err_wob_ton ; % simulated state: error wob control
        eI_wob_ton_s; % simulated state: integrated error wob control
        err_pos_m   ; % simulated state: error pos control
        eI_pos_m_s  ; % simulated state: integrated error pos control

        %Temporary variables
        target      ; % exit rule number
        timegelbreak_s; % waiting for gel to break before ramping up pump to full rate
        timeSinceResetTT_s;
        t_s         ; % simulation clock
        buffer      ;
        curBufferIdx            = 0;
    end
    
    properties (SetAccess = private)
        dt_s                ; % sampling time
        solver              ;
        %sys_states          ;
        simcase             ;
    end
             
    properties (Constant)           
            %Calc            
            TextArr={'Start drilling ';...
                'Pull off bottom ';...
                'Stop rotation ';...
                'Stop pumps ';...
                'Wait ';...
                'Start pumps ';...
                'Start rotation ';...
                'Go on bottom ';...
                'Pull 1 stand ';...
                'Flow check ';...
                'Run 1 stand ';...
                'End ';...
                'Add 1 stand tripping in ';...
                'Remove 1 stand tripping out ';...
                'Add 1 stand drilling '};
    end
    
    properties (SetAccess = private)
        isCurrentlyBuffering    = 0;
        valuesToBuffer          = 0;
        pressureNoiseAmpl_prc   = 0; 
        flowNoiseAmpl_prc       = 0;
    end
    
    methods
        function obj            = SIMULATOR_OBJ()
        end
        
        function obj                = initialize(obj,dt_s,solver,simcase)%Constructor
            obj.dt_s                = dt_s;
            obj.solver              = solver;

            obj.t_s                 = 0;
            obj.isCurrentlyBuffering= 0;
            obj.curBufferIdx        = 0; 
            obj.simcase             = simcase;
                        
            %Init
            obj.bitDepth_m          = simcase.holeDepth_m;
            obj.holeDepth_m         = simcase.holeDepth_m;
            obj.blockVel_mph        = simcase.rop_mph;
            obj.bitVel_mph          = simcase.rop_mph;
            obj.blockPos_m          = simcase.blockPos_m;
            obj.lastConnectionDepth_m = simcase.holeDepth_m;
            
            obj.rotation_rpm        = simcase.rotation_rpm;
            obj.dbdV_barm3          = simcase.beta_bar/simcase.Vtot_m3;
            obj.qIo_lpm             = 0;
            obj.VIo_m3              = 0;
            
            obj.F_wob_ton           = simcase.F_wob_ton;
            obj.F_drag_ton          = simcase.cdrag_ton_mph*simcase.rop_mph;
            obj.F_hl_ton            = simcase.mwet_ton - obj.F_wob_ton - obj.F_drag_ton;%ton
            
            obj.dp_dp_bar           = unified_friction_pipe(simcase.IDdp_in,simcase.q_drilling_lpm/60000,obj.bitDepth_m,simcase.PVmud_cP,simcase.YPmud_lb100sqft,simcase.tauymud_Pa,simcase.MWmud_sg*1000);
            obj.dp_bit_bar          = simcase.cbit_bar_lpm2*simcase.q_drilling_lpm^2;
            obj.dp_ann_bar          = unified_friction_ann(simcase.ODdp_in,simcase.IDCs_in,simcase.q_drilling_lpm/60000,obj.bitDepth_m,simcase.PVmud_cP,simcase.YPmud_lb100sqft,simcase.tauymud_Pa,simcase.MWmud_sg*1000);
            
            obj.p_cut_bar           = 10;
            obj.p_p_bar             = obj.dp_dp_bar+obj.dp_bit_bar+obj.dp_ann_bar+obj.p_cut_bar;
            obj.p_bit_bar           = simcase.MWmud_sg*1000*simcase.g_ms2*obj.bitDepth_m*1e-5+obj.dp_ann_bar+obj.p_cut_bar;
            obj.p_bh_bar            = obj.p_bit_bar;          
            
            obj.q_p_lpm             = simcase.q_drilling_lpm;
            obj.q_bit_lpm           = simcase.q_drilling_lpm;
            obj.q_tt_lpm            = 0;
            obj.q_top_lpm           = simcase.q_drilling_lpm+simcase.rop_mph/3600*simcase.Asdpopen_m2*60000;
            obj.q_out_lpm           = obj.q_top_lpm + obj.q_tt_lpm;
            obj.q_pit_lpm           = obj.q_out_lpm;
            obj.q_shaker_lpm        = obj.q_out_lpm;
            
            obj.V_fl_m3             = simcase.V_fl_m3; 
            obj.V_pit_m3            = simcase.V_pit_m3;
            obj.V_well_m3           = simcase.Vann_m3+simcase.Vidp_m3;   
            obj.V_tt_m3             = simcase.V_tt_m3;
            
            obj.ref_wob_ton         = simcase.F_wob_ton;
            obj.ref_wobf_ton        = simcase.F_wob_ton;
            obj.ref_pos_m           = obj.bitDepth_m;
            obj.ref_posf_m          = obj.bitDepth_m;
            obj.err_wob_ton         = 0;
            obj.eI_wob_ton_s        = simcase.Tiwob_s/simcase.Kcwob*simcase.rop_mph;
            obj.err_pos_m           = 0;
            obj.eI_pos_m_s          = simcase.Tipos_s/simcase.Kcpos*simcase.rop_mph;
            obj.vel_max_mph         = simcase.vel_max_rop_mph;
            
            obj.autoDrill           = 1;
            obj.onBottom_b          = true;
            obj.drillState          = simcase.drillState;
            obj.ufl_b               = true;
            obj.seqNumber           = 1;
            obj.timeSinceResetTT_s  = -0.99925;
            obj.target              = 0;
        end
       
        function obj = StartSavingIterations(obj,required_buffer_duration_s)
           obj.isCurrentlyBuffering  = 1;
           obj.valuesToBuffer        = required_buffer_duration_s/obj.dt_s;
           obj.curBufferIdx          = 0;
           obj.buffer.t_s            = nan(1, obj.valuesToBuffer);

           obj.buffer.p_p_bar        = nan(1, obj.valuesToBuffer);
           obj.buffer.p_bit_bar      = nan(1, obj.valuesToBuffer);
           obj.buffer.p_bh_bar       = nan(1, obj.valuesToBuffer);
           obj.buffer.p_cut_bar      = nan(1, obj.valuesToBuffer);
           obj.buffer.dp_dp_bar      = nan(1, obj.valuesToBuffer);           
           obj.buffer.dp_bit_bar     = nan(1, obj.valuesToBuffer);           
           obj.buffer.dp_ann_bar     = nan(1, obj.valuesToBuffer);           

           obj.buffer.q_p_lpm        = nan(1, obj.valuesToBuffer);%Input
           obj.buffer.q_bit_lpm      = nan(1, obj.valuesToBuffer);
           obj.buffer.q_top_lpm      = nan(1, obj.valuesToBuffer);
           obj.buffer.q_tt_lpm       = nan(1, obj.valuesToBuffer);%Input
           obj.buffer.q_out_lpm      = nan(1, obj.valuesToBuffer);
           obj.buffer.q_out_pc       = nan(1, obj.valuesToBuffer);
           obj.buffer.q_pit_lpm      = nan(1, obj.valuesToBuffer);
           obj.buffer.q_shaker_lpm   = nan(1, obj.valuesToBuffer);

           obj.buffer.V_fl_m3        = nan(1, obj.valuesToBuffer);
           obj.buffer.V_well_m3      = nan(1, obj.valuesToBuffer);
           obj.buffer.V_pit_m3       = nan(1, obj.valuesToBuffer);
           obj.buffer.V_tt_m3        = nan(1, obj.valuesToBuffer);

           obj.buffer.F_hl_ton       = nan(1, obj.valuesToBuffer);
           obj.buffer.F_wob_ton      = nan(1, obj.valuesToBuffer);
           obj.buffer.F_drag_ton     = nan(1, obj.valuesToBuffer);

           obj.buffer.bitVel_mph     = nan(1, obj.valuesToBuffer);
           obj.buffer.bitDepth_m     = nan(1, obj.valuesToBuffer);
           obj.buffer.blockVel_mph   = nan(1, obj.valuesToBuffer);           
           obj.buffer.blockPos_m     = nan(1, obj.valuesToBuffer); %input            
           obj.buffer.holeDepth_m    = nan(1, obj.valuesToBuffer);
           obj.buffer.lastConnectionDepth_m = nan(1, obj.valuesToBuffer);

           obj.buffer.rotation_rpm   = nan(1, obj.valuesToBuffer);           
           obj.buffer.dbdV_barm3     = nan(1, obj.valuesToBuffer);

           obj.buffer.qIo_lpm        = nan(1, obj.valuesToBuffer);
           obj.buffer.VIo_m3         = nan(1, obj.valuesToBuffer);

           obj.buffer.ref_wob_ton      = nan(1, obj.valuesToBuffer); %input           
           obj.buffer.ref_wobf_ton     = nan(1, obj.valuesToBuffer);           
           obj.buffer.ref_pos_m        = nan(1, obj.valuesToBuffer); %input           
           obj.buffer.ref_posf_m       = nan(1, obj.valuesToBuffer);           
           obj.buffer.err_wob_ton      = nan(1, obj.valuesToBuffer);           
           obj.buffer.eI_wob_ton_s     = nan(1, obj.valuesToBuffer);           
           obj.buffer.err_pos_m        = nan(1, obj.valuesToBuffer);           
           obj.buffer.eI_pos_m_s       = nan(1, obj.valuesToBuffer);           

           obj.buffer.onBottom_b       = nan(1, obj.valuesToBuffer);           
           obj.buffer.drillState       = nan(1, obj.valuesToBuffer);           
           obj.buffer.autoDrill        = nan(1, obj.valuesToBuffer);           
           obj.buffer.ufl_b            = nan(1, obj.valuesToBuffer);           
           obj.buffer.vel_max_mph      = nan(1, obj.valuesToBuffer);           
           obj.buffer.seqNumber        = nan(1, obj.valuesToBuffer);   
           obj.buffer.qout_pc          = nan(1, obj.valuesToBuffer);   
           %obj.buffer.sys_states       = nan(1, obj.valuesToBuffer);   
        end
        
        function obj = stepIt(obj)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            lpmTOm3ps         = 1/60000;% conversion from liters per minute to m3 per second
            m3psTOlpm         = 1/lpmTOm3ps;
            barToPa           = 100000;
            hTOs              = 3600;
            tonTOkg           = 1000;
          
            if not(obj.solver == SimulatorSolvers.FORWARD_EULER  ) 
                disp('SIMULATOR_OBJ.m: ERROR solver not supported');
            end
            obj.onBottom_b = (obj.holeDepth_m - obj.bitDepth_m < 0.01) & (obj.bitVel_mph >=0); 
           
           
            % Influx/loss
            ongoingEvent = false;
            for ii = 1:length(obj.simcase.InfluxVecRate_lpm)
                if obj.t_s > obj.simcase.InfluxVecTime_s(ii,1) && obj.t_s < obj.simcase.InfluxVecTime_s(ii,2)
                    obj.qIo_lpm = obj.simcase.InfluxVecRate_lpm(ii);
                    obj.VIo_m3  = obj.VIo_m3 + obj.qIo_lpm*obj.dt_s*lpmTOm3ps;
                    ongoingEvent = true;
                end
            end
            if not(ongoingEvent)
                obj.qIo_lpm = 0;
                obj.VIo_m3  = 0;
            end
            
            %Step in pit level
            if obj.t_s > obj.simcase.StepPitTime_s - obj.dt_s/2 && obj.t_s < obj.simcase.StepPitTime_s + obj.dt_s/2
                obj.V_pit_m3 = obj.V_pit_m3 + obj.simcase.StepPitDV_m3;
            end
            
            if obj.drillState == SimDrillingStates.Drilling        %Drill 1 stand (30m): ROP=30m/h
                obj.target       = obj.lastConnectionDepth_m+30;
                obj.ref_wob_ton  = obj.simcase.F_wob_ton;
                obj.ref_pos_m    = obj.bitDepth_m;
                obj.rotation_rpm = obj.simcase.rotation_rpm;
                obj.q_p_lpm      = obj.simcase.q_drilling_lpm;
                obj.q_tt_lpm     = 0;
                obj.autoDrill    = 1;
                obj.vel_max_mph  = obj.simcase.vel_max_rop_mph;
                obj.ufl_b        = true;%open flow line valve - valve to trip tank normally closed
            elseif obj.drillState == SimDrillingStates.PullOffBottom
%         %Connection: pull off bottom 2m, stop rotation, ramp down pumps, set in slips, disconnect,
%         %            add new pipe, connect, open slips, ramp up pumps, start rotation, go on bottom 
                obj.ref_wob_ton  = 0;
                obj.target       = obj.holeDepth_m - 2;%pull off bottom 2m
                obj.ref_pos_m    = obj.target;
                %obj.rotation_rpm = obj.simcase.rotation_rpm;
                %obj.q_p_lpm      = obj.simcase.q_drilling_lpm;
                obj.q_tt_lpm     = 0;
                obj.autoDrill    = 2;%pos control
                obj.vel_max_mph  = obj.simcase.vel_max_trip_mph;
                obj.ufl_b        = true;%open flow line valve - valve to trip tank normally closed
            elseif obj.drillState == SimDrillingStates.StopRotation    
                %stop rotation in 10 sec
                obj.ref_pos_m    = obj.bitDepth_m;
                obj.rotation_rpm = obj.rotation_rpm - obj.dt_s*obj.simcase.rotation_rpm/10;
                obj.q_p_lpm      = obj.simcase.q_drilling_lpm;
                obj.q_tt_lpm     = 0;
                obj.autoDrill    = 0;%manual
                obj.vel_max_mph  = 0;
                obj.ufl_b        = true;%open flow line valve - valve to trip tank normally closed
                obj.target       = 0;
            elseif obj.drillState == SimDrillingStates.StopPumps    
                %ramp down pumps in 30 sec
                obj.ref_pos_m    = obj.bitDepth_m;
                obj.rotation_rpm = 0;
                obj.q_p_lpm      = obj.q_p_lpm - obj.dt_s*obj.simcase.q_drilling_lpm/30;
                obj.q_tt_lpm     = 0;
                obj.autoDrill    = 0;%manual
                obj.vel_max_mph  = 0;
                obj.ufl_b        = true;%open flow line valve - valve to trip tank normally closed
                obj.target       = 0;
            elseif obj.drillState == SimDrillingStates.Wait    
            % Do nothing for 10 mins    
                if obj.target < obj.t_s - obj.dt_s
                    obj.target     = obj.t_s + 10*60;
                end
                obj.ref_pos_m    = obj.bitDepth_m;
                obj.rotation_rpm = 0;
                obj.q_p_lpm      = 0;
                obj.q_tt_lpm     = 0;
                obj.autoDrill    = 0;%manual
                obj.vel_max_mph  = 0;
                obj.autoDrill    = 0;%manual
                obj.ufl_b        = true;%open flow line valve - valve to trip tank normally closed
            elseif obj.drillState == SimDrillingStates.DrillAddStand    
                % Do nothing for 5 mins    
                if obj.target < obj.t_s - obj.dt_s
                    obj.target     = obj.t_s + 5*60;
                    obj.lastConnectionDepth_m = obj.bitDepth_m;
                    obj.blockPos_m = obj.blockPos_m - 30;%Add new pipe
                end
                obj.ref_pos_m    = obj.bitDepth_m;
                obj.rotation_rpm = 0;
                obj.q_p_lpm      = 0;
                obj.q_tt_lpm     = 0;
                obj.autoDrill    = 0;%manual
                obj.vel_max_mph  = 0;
                obj.autoDrill    = 0;%manual
                obj.ufl_b        = true;%open flow line valve - valve to trip tank normally closed
            elseif obj.drillState == SimDrillingStates.StartPumps
            % ramp up pumps,
                obj.ref_pos_m    = obj.bitDepth_m;
                obj.rotation_rpm = 0;
                if obj.q_p_lpm < obj.simcase.q_gelbreak_lpm
                    obj.q_p_lpm         = obj.q_p_lpm + obj.dt_s*obj.simcase.q_drilling_lpm/30; % Ramp up to low rate to break gel
                    obj.timegelbreak_s  = 0;
                elseif obj.timegelbreak_s <= obj.simcase.timegelbreak_s % Wait for gel to break
                    obj.q_p_lpm         = obj.q_p_lpm;
                    obj.timegelbreak_s  = obj.timegelbreak_s + obj.dt_s;
                else
                    obj.q_p_lpm         = min(obj.simcase.q_drilling_lpm,obj.q_p_lpm + obj.dt_s*obj.simcase.q_drilling_lpm/30); %Ramp up to drilling flow rate
                end
                obj.q_tt_lpm     = 0;
                obj.autoDrill    = 0;%manual
                obj.vel_max_mph  = 0;
                obj.target       = obj.simcase.q_drilling_lpm;
                obj.ufl_b        = true;%open flow line valve - valve to trip tank normally closed
            elseif obj.drillState == SimDrillingStates.StartRotation    
            %start rotation in 10 sec
                obj.ref_pos_m    = obj.bitDepth_m;
                obj.rotation_rpm = obj.rotation_rpm + obj.dt_s*obj.simcase.rotation_rpm/10;
                obj.q_p_lpm      = obj.simcase.q_drilling_lpm;
                obj.q_tt_lpm     = 0;
                obj.autoDrill    = 0;%manual
                obj.vel_max_mph  = 0;
                target           = obj.simcase.rotation_rpm;
                obj.ufl_b        = true;%open flow line valve - valve to trip tank normally closed
            elseif obj.drillState == SimDrillingStates.GoOnBottom %go on bottom 2m,
                obj.target       = obj.holeDepth_m;
                obj.ref_pos_m    = obj.target;
                obj.rotation_rpm = obj.simcase.rotation_rpm;
                obj.q_p_lpm      = obj.simcase.q_drilling_lpm;
                obj.q_tt_lpm     = 0;
                obj.autoDrill    = 2;%pos control
                obj.vel_max_mph  = obj.simcase.vel_max_trip_mph;
                obj.ufl_b        = true;%open flow line valve - valve to trip tank normally closed
            elseif obj.drillState == SimDrillingStates.Pull1Stand %pull 1 stand,
                obj.ufl_b        = false;%close flow line valve - open valve to trip tank
                %Ramp up trip tank pump
                obj.q_tt_lpm     = min(obj.simcase.q_tripping_lpm, obj.q_tt_lpm+obj.dt_s*obj.simcase.q_tripping_lpm/30);
                if obj.bitDepth_m > obj.lastConnectionDepth_m
                    obj.target       = obj.lastConnectionDepth_m;
                else
                    obj.target       = max(0, obj.lastConnectionDepth_m - 30);
                end
                obj.vel_max_mph  = obj.simcase.vel_max_trip_mph;
                obj.ref_pos_m    = obj.target;%max(target,xb(i)+dt*rvel(i)/60);
                obj.autoDrill    = 2;% pos control
                obj.rotation_rpm = 0;
                obj.q_p_lpm      = 0;                
            elseif obj.drillState == SimDrillingStates.FlowCheck
                if obj.target < obj.t_s
                    obj.target = obj.t_s + 5*60;
                end
                obj.ref_pos_m    = obj.bitDepth_m;
                obj.rotation_rpm = 0;
                obj.q_p_lpm      = 0;
                obj.q_tt_lpm     = obj.simcase.q_tripping_lpm;
                obj.autoDrill    = 0;%manual
                obj.vel_max_mph  = 0;
                obj.autoDrill    = 0;%manual
                obj.ufl_b        = false;%close flow line valve - valve to trip tank open
            elseif obj.drillState == SimDrillingStates.TripAddStand
                if obj.target < obj.t_s
                    obj.target = obj.t_s + 5*60;
                end
                obj.ref_pos_m    = obj.bitDepth_m;
                obj.rotation_rpm = 0;
                obj.q_p_lpm      = 0;
                obj.q_tt_lpm     = obj.simcase.q_tripping_lpm;
                obj.autoDrill    = 0;%manual
                obj.vel_max_mph  = 0;
                obj.autoDrill    = 0;%manual
                obj.ufl_b        = false;%close flow line valve - valve to trip tank open
                obj.lastConnectionDepth_m = obj.bitDepth_m;
                obj.blockPos_m   = obj.blockPos_m - 30;%Add new stand
            elseif obj.drillState == SimDrillingStates.TripRemoveStand
                if obj.target < obj.t_s
                    obj.target = obj.t_s + 5*60;
                end
                obj.ref_pos_m    = obj.bitDepth_m;
                obj.rotation_rpm = 0;
                obj.q_p_lpm      = 0;
                obj.q_tt_lpm     = obj.simcase.q_tripping_lpm;
                obj.autoDrill    = 0;%manual
                obj.vel_max_mph  = 0;
                obj.autoDrill    = 0;%manual
                obj.ufl_b        = false;%close flow line valve - valve to trip tank open
                obj.lastConnectionDepth_m = obj.bitDepth_m;
                obj.blockPos_m   = obj.blockPos_m + 30;%Remove stand
            elseif obj.drillState == SimDrillingStates.Run1Stand %run 1 stand,
                obj.ufl_b        = false;%close flow line valve - valve to trip tank open
                obj.q_tt_lpm     = obj.simcase.q_tripping_lpm;
                if obj.bitDepth_m < obj.lastConnectionDepth_m
                    obj.target       = obj.lastConnectionDepth_m;
                else
                    obj.target       = min(obj.lastConnectionDepth_m + 30, obj.holeDepth_m - 2); %Do not run closer than 2m from bottom of hole
                end
                obj.vel_max_mph  = obj.simcase.vel_max_trip_mph;
                obj.ref_pos_m    = obj.target;
                obj.autoDrill    = 2;% pos control
                obj.rotation_rpm = 0;
                obj.q_p_lpm      = 0;
            elseif obj.drillState == SimDrillingStates.EndSimulation
                %Ramp down trip tank pump
                obj.q_tt_lpm     = max(0,obj.q_tt_lpm - obj.dt_s*obj.simcase.q_tripping_lpm/30);
                obj.ref_pos_m    = obj.bitDepth_m;
                obj.ref_wob_ton  = 0;
                obj.autoDrill    = 0;% man control
                obj.rotation_rpm = 0;
                obj.q_p_lpm      = 0;
                obj.target       = 0;
            end
            
            %Steps in flow in
            for ii = 1:length(obj.simcase.DeltaQinVecTime_s)
                if obj.t_s > obj.simcase.DeltaQinVecTime_s(ii,1) && obj.t_s < obj.simcase.DeltaQinVecTime_s(ii,2)                    
                    obj.q_p_lpm = obj.simcase.q_drilling_lpm + obj.simcase.DeltaQinVecRate_lpm(ii)*min(1,(obj.t_s-obj.simcase.DeltaQinVecTime_s(ii,1))/10);                    
                end
            end

            
            %Controller autodrill
             obj.err_wob_ton = obj.ref_wobf_ton - obj.F_wob_ton;
             obj.err_pos_m   = obj.ref_posf_m   - obj.bitDepth_m;
             if obj.autoDrill == 1
                 obj.vel_max_mph  = obj.simcase.vel_max_rop_mph;
                 obj.blockVel_mph = max(0,min(obj.vel_max_mph,obj.simcase.Kcwob * obj.err_wob_ton + obj.simcase.Kcwob/obj.simcase.Tiwob_s*obj.eI_wob_ton_s));
                 obj.eI_wob_ton_s = obj.simcase.Tiwob_s/obj.simcase.Kcwob*(obj.blockVel_mph - obj.simcase.Kcwob*obj.err_wob_ton);
                 obj.eI_pos_m_s   = obj.simcase.Tipos_s/obj.simcase.Kcpos*(obj.blockVel_mph - obj.simcase.Kcpos*obj.err_pos_m);        
             elseif obj.autoDrill == 2
                 obj.vel_max_mph  = min(obj.simcase.vel_max_trip_mph,10+(obj.simcase.vel_max_trip_mph-10)/2*abs(obj.err_pos_m));
                 obj.blockVel_mph = obj.simcase.Kcpos*obj.err_pos_m+obj.simcase.Kcpos/obj.simcase.Tipos_s*obj.eI_pos_m_s;
                 if obj.blockVel_mph > obj.vel_max_mph
                     obj.blockVel_mph = obj.vel_max_mph;
                     obj.eI_pos_m_s   = obj.simcase.Tipos_s/obj.simcase.Kcpos*(obj.blockVel_mph - obj.simcase.Kcpos*obj.err_pos_m); 
                 elseif obj.blockVel_mph < -obj.vel_max_mph
                     obj.blockVel_mph = -obj.vel_max_mph;
                     obj.eI_pos_m_s   = obj.simcase.Tipos_s/obj.simcase.Kcpos*(obj.blockVel_mph - obj.simcase.Kcpos*obj.err_pos_m); 
                 end
                 obj.eI_wob_ton_s = obj.simcase.Tiwob_s/obj.simcase.Kcwob*(obj.blockVel_mph - obj.simcase.Kcwob*obj.err_wob_ton);
             else %obj.autoDrill == 0,
                 obj.eI_wob_ton_s = obj.simcase.Tiwob_s/obj.simcase.Kcwob*(obj.blockVel_mph - obj.simcase.Kcwob*obj.err_wob_ton);
                 obj.eI_pos_m_s   = obj.simcase.Tipos_s/obj.simcase.Kcpos*(obj.blockVel_mph - obj.simcase.Kcpos*obj.err_pos_m);     
                 obj.blockVel_mph = 0;
             end        
     
             %Trip steel area
             if (obj.q_p_lpm == 0 && obj.blockVel_mph > 1 && obj.ufl_b == 0)
                 areaDP_m2 = obj.simcase.Asdpclosed_m2; %tripping in
             else
                 areaDP_m2 = obj.simcase.Asdpopen_m2;
             end
             qsteel_lpm = obj.blockVel_mph/hTOs*areaDP_m2*60000;
             
             alpha=[0.8 0.8];%[0.7 0.3]weight on flow in vs flow out in friction calc
             qdp  = (alpha(1)* obj.q_p_lpm                               + (1-alpha(1))*obj.q_bit_lpm);
             qann = (alpha(2)*(obj.q_bit_lpm + obj.qIo_lpm + qsteel_lpm) + (1-alpha(2))*obj.q_top_lpm);%obj.q_out_lpm - obj.q_tt_lpm));
             obj.dp_dp_bar  = unified_friction_pipe(obj.simcase.IDdp_in,qdp*lpmTOm3ps,obj.bitDepth_m,obj.simcase.PVmud_cP,...
                                                    obj.simcase.YPmud_lb100sqft,obj.simcase.tauymud_Pa,obj.simcase.MWmud_sg*1000);
             obj.dp_bit_bar = obj.simcase.cbit_bar_lpm2*(obj.q_bit_lpm)^2;
             obj.dp_ann_bar = unified_friction_ann(obj.simcase.ODdp_in,obj.simcase.IDCs_in, qann*lpmTOm3ps, obj.bitDepth_m, obj.simcase.PVmud_cP,...
                                                   obj.simcase.YPmud_lb100sqft, obj.simcase.tauymud_Pa, obj.simcase.MWmud_sg*1000);
             
             obj.q_pit_lpm     =    obj.simcase.kpit_lpm_m3*obj.V_fl_m3^obj.simcase.alpha_fl;
             obj.q_shaker_lpm  = obj.simcase.kshaker_lpm_m3*obj.V_fl_m3^obj.simcase.alpha_fl;
             
             obj.F_drag_ton = obj.simcase.cdrag_ton_mph*obj.blockVel_mph;%ton

             %Dynamics
             if (obj.q_p_lpm == 0 && obj.blockVel_mph > 1 && obj.ufl_b == 0) %tripping in
                 obj.dp_dp_bar  = 0;
                 obj.dp_bit_bar = 0;
             end
             
             p_cut_dot_barps = obj.simcase.pcutdot*randn;
             p_p_dot_barps   = obj.simcase.beta_bar/obj.simcase.Vidp_m3*(obj.q_p_lpm - obj.q_bit_lpm)/60000;
             p_bh_dot_barps  = obj.simcase.beta_bar/obj.simcase.Vann_m3*(obj.q_bit_lpm + obj.qIo_lpm + qsteel_lpm - obj.q_top_lpm )/60000;

             p_cut_dot_barps = max(p_cut_dot_barps, -obj.p_cut_bar/obj.dt_s);
             obj.p_cut_bar   = obj.p_cut_bar      + obj.dt_s*p_cut_dot_barps;
             obj.p_p_bar     = max(0, obj.p_p_bar + obj.dt_s*p_p_dot_barps);
             obj.p_bh_bar    = obj.p_bh_bar + obj.dt_s*(p_bh_dot_barps + p_cut_dot_barps);
                
             dqmax_lpm       = 50;
             dqbit_lpm       = obj.dt_s/obj.simcase.Mdp_kgpm4*(obj.p_p_bar  - obj.dp_dp_bar - obj.dp_bit_bar + obj.simcase.MWmud_sg*1000*obj.simcase.g_ms2*obj.holeDepth_m*1e-5 - obj.p_bh_bar)*barToPa/lpmTOm3ps; 
             if abs(dqbit_lpm) > dqmax_lpm
                 dqbit_lpm = sign(dqbit_lpm)*dqmax_lpm;
             end
             obj.q_bit_lpm   = max(0, obj.q_bit_lpm + dqbit_lpm);
             %dqtop_bit_lpm   = obj.dt_s/(0.25*obj.simcase.Mann_kgpm4)*(obj.p_bh_bar - obj.dp_ann_bar - obj.p_cut_bar - obj.simcase.MWmud_sg*1000*obj.simcase.g_ms2*obj.holeDepth_m*1e-5)*barToPa/lpmTOm3ps;
             dqtop_bit_lpm   = obj.dt_s/(obj.simcase.Mann_kgpm4)*(obj.p_bh_bar - obj.dp_ann_bar - obj.p_cut_bar - obj.simcase.MWmud_sg*1000*obj.simcase.g_ms2*obj.holeDepth_m*1e-5)*barToPa/lpmTOm3ps;
             if abs(dqtop_bit_lpm) > dqmax_lpm
                 dqtop_bit_lpm = sign(dqtop_bit_lpm)*dqmax_lpm;
             end             
             obj.q_top_lpm   = obj.q_top_lpm + dqtop_bit_lpm;                 
             obj.q_out_lpm   = max(0, obj.q_top_lpm + obj.q_tt_lpm);
             obj.V_fl_m3     = max(0, obj.V_fl_m3  + obj.dt_s*(obj.ufl_b * obj.q_out_lpm - obj.q_pit_lpm - obj.q_shaker_lpm)*lpmTOm3ps);
             obj.V_tt_m3     = obj.V_tt_m3   + obj.dt_s*((1-obj.ufl_b)*obj.q_out_lpm - obj.q_tt_lpm)*lpmTOm3ps;
             
             if obj.V_tt_m3 > 10 && obj.timeSinceResetTT_s > 60
                 obj.V_tt_m3            = 1; %Empty trip tank when full
                 obj.timeSinceResetTT_s = 0;
             elseif obj.V_tt_m3 < 1 && obj.timeSinceResetTT_s > 60
                 obj.V_tt_m3 = 10; %Fill trip tank when empty
                 obj.timeSinceResetTT_s = 0;
             else
                 obj.timeSinceResetTT_s = obj.timeSinceResetTT_s + obj.dt_s;
             end
             
             obj.V_pit_m3   = obj.V_pit_m3  + obj.dt_s*(obj.q_pit_lpm - obj.q_p_lpm)*lpmTOm3ps;
             obj.V_well_m3  = obj.V_well_m3 + obj.dt_s*(obj.q_p_lpm + obj.q_tt_lpm + obj.qIo_lpm - obj.q_out_lpm)*lpmTOm3ps;

             obj.bitDepth_m   = obj.bitDepth_m + obj.dt_s/hTOs*obj.bitVel_mph;
             obj.blockPos_m   = obj.blockPos_m + obj.dt_s/hTOs*obj.blockVel_mph;
             obj.ref_wobf_ton = obj.ref_wobf_ton + obj.dt_s/obj.simcase.Tref_s*(obj.ref_wob_ton - obj.ref_wobf_ton);
             obj.ref_posf_m   = obj.ref_posf_m   + obj.dt_s/obj.simcase.Tref_s*(obj.ref_pos_m   - obj.ref_posf_m);
             obj.eI_wob_ton_s = obj.eI_wob_ton_s + obj.dt_s*obj.err_wob_ton;
             obj.eI_pos_m_s   = obj.eI_pos_m_s   + obj.dt_s*obj.err_pos_m;
             obj.F_hl_ton     = max(0, obj.F_hl_ton+obj.dt_s*obj.simcase.ks_ton/obj.simcase.holeDepth_m*(obj.bitVel_mph-obj.blockVel_mph)/hTOs);
             if (obj.onBottom_b)
                 obj.bitVel_mph   = (obj.simcase.mwet_ton - obj.F_hl_ton)/(obj.simcase.d_ton_mph*obj.rotation_rpm/obj.simcase.rotation_rpm + obj.simcase.cdrag_ton_mph);
                 obj.F_wob_ton    = (obj.simcase.d_ton_mph*obj.rotation_rpm/obj.simcase.rotation_rpm)*obj.bitVel_mph;
                 obj.holeDepth_m  = max(obj.holeDepth_m, obj.bitDepth_m);
             else
                 obj.F_wob_ton    = 0;
                 obj.bitVel_mph   = (obj.simcase.mwet_ton - obj.F_hl_ton)/(obj.simcase.cdrag_ton_mph);
                 obj.holeDepth_m  = obj.holeDepth_m;
             end

             obj.p_bit_bar = obj.p_bh_bar - obj.simcase.MWmud_sg*1000*obj.simcase.g_ms2*(obj.holeDepth_m - obj.bitDepth_m)*1e-5;
             oldState = obj.drillState; 
             obj = obj.statemachine(obj.simcase.StateVector(min(length(obj.simcase.StateVector),obj.seqNumber+1)));
             if not(obj.drillState == oldState)
                 disp([obj.TextArr(obj.simcase.StateVector(min(length(obj.simcase.StateVector),obj.seqNumber+1))), num2str(obj.t_s/60, '%.2f'), ' min']);
                 obj.seqNumber=obj.seqNumber+1;
             elseif obj.t_s<obj.dt_s
                 disp([obj.TextArr(obj.simcase.StateVector(1)), num2str(obj.t_s/60, '%.2f'), ' min']);        
             end
   
             %Generate noisy paddle measurement
             noise        = obj.simcase.q_out_noise_lpm*randn();% -50 - +50 lpm
             scalefac     = 1+obj.simcase.qpaddle_accuracy_pc/100*randn();% 0.9 - 1.1
             x            = scalefac*obj.q_out_lpm+noise;
             if obj.q_out_lpm < 400
                 obj.q_out_pc = 0;
             else
                 obj.q_out_pc = interp1(obj.simcase.PaddleVec_lpm,obj.simcase.PaddleVec_pc,x);
                 obj.q_out_pc = max(0,min(100,obj.q_out_pc));
             end
             
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            obj.t_s             = obj.t_s + obj.dt_s;
            if  ((obj.t_s/60 - floor(obj.t_s/60)) < obj.dt_s/60)
                if (obj.qIo_lpm > 0)
                    disp(['Sim time:' num2str(obj.t_s/60, '%.f') ' min: Influx ' num2str(obj.qIo_lpm,'%.0f') ' lpm']);
                elseif (obj.qIo_lpm < 0)
                    disp(['Sim time:' num2str(obj.t_s/60, '%.f') ' min: Loss '   num2str(obj.qIo_lpm,'%.0f') ' lpm']);
                else
                    disp(['Sim time:' num2str(obj.t_s/60, '%.f') ' min']);
                end
            end
            % buffer results if specified
            if (obj.isCurrentlyBuffering==1)
                obj.curBufferIdx    = obj.curBufferIdx + 1;
                if (obj.curBufferIdx>=obj.valuesToBuffer)
                    obj.isCurrentlyBuffering =0;
                    disp('Simulation finished buffering');
                else    
                    obj.buffer.t_s(obj.curBufferIdx)            = obj.t_s; 
                    % states/algebraic simualted variables
                    obj.buffer.p_p_bar (obj.curBufferIdx)       = obj.p_p_bar ;
                    obj.buffer.p_bit_bar (obj.curBufferIdx)     = obj.p_bit_bar ;
                    obj.buffer.p_bh_bar (obj.curBufferIdx)      = obj.p_bh_bar ;
                    obj.buffer.p_cut_bar (obj.curBufferIdx)     = obj.p_cut_bar ;

                    obj.buffer.dp_dp_bar (obj.curBufferIdx)     = obj.dp_dp_bar ;
                    obj.buffer.dp_bit_bar (obj.curBufferIdx)    = obj.dp_bit_bar ;
                    obj.buffer.dp_ann_bar (obj.curBufferIdx)    = obj.dp_ann_bar ;

                    obj.buffer.q_p_lpm(obj.curBufferIdx)        = obj.q_p_lpm ;
                    obj.buffer.q_bit_lpm(obj.curBufferIdx)      = obj.q_bit_lpm;
                    obj.buffer.q_tt_lpm(obj.curBufferIdx)       = obj.q_tt_lpm ;
                    obj.buffer.q_out_lpm(obj.curBufferIdx)      = obj.q_out_lpm ;
                    obj.buffer.q_out_pc(obj.curBufferIdx)       = obj.q_out_pc ;
                    obj.buffer.q_pit_lpm(obj.curBufferIdx)      = obj.q_pit_lpm;
                    obj.buffer.q_shaker_lpm(obj.curBufferIdx)   = obj.q_shaker_lpm;

                    obj.buffer.V_fl_m3 (obj.curBufferIdx)       = obj.V_fl_m3;
                    obj.buffer.V_well_m3 (obj.curBufferIdx)     = obj.V_well_m3;
                    obj.buffer.V_pit_m3(obj.curBufferIdx)       = obj.V_pit_m3;
                    obj.buffer.V_tt_m3 (obj.curBufferIdx)       = obj.V_tt_m3;

                    obj.buffer.F_hl_ton (obj.curBufferIdx)      = obj.F_hl_ton;
                    obj.buffer.F_wob_ton (obj.curBufferIdx)     = obj.F_wob_ton;
                    obj.buffer.F_drag_ton (obj.curBufferIdx)    = obj.F_drag_ton;
                    
                    obj.buffer.bitVel_mph (obj.curBufferIdx)    = obj.bitVel_mph;
                    obj.buffer.bitDepth_m (obj.curBufferIdx)    = obj.bitDepth_m;
                    obj.buffer.blockVel_mph (obj.curBufferIdx)  = obj.blockVel_mph;
                    obj.buffer.blockPos_m (obj.curBufferIdx)    = obj.blockPos_m;
                    obj.buffer.holeDepth_m (obj.curBufferIdx)   = obj.holeDepth_m;
                    obj.buffer.lastConnectionDepth_m (obj.curBufferIdx)   = obj.lastConnectionDepth_m;

                    obj.buffer.rotation_rpm (obj.curBufferIdx)  = obj.rotation_rpm;
                    obj.buffer.dbdV_barm3(obj.curBufferIdx)     = obj.dbdV_barm3;
                    obj.buffer.qIo_lpm (obj.curBufferIdx)       = obj.qIo_lpm;
                    obj.buffer.VIo_m3  (obj.curBufferIdx)       = obj.VIo_m3;

                    obj.buffer.ref_wob_ton (obj.curBufferIdx)   = obj.ref_wob_ton;
                    obj.buffer.ref_wobf_ton (obj.curBufferIdx)  = obj.ref_wobf_ton;
                    obj.buffer.ref_pos_m (obj.curBufferIdx)     = obj.ref_pos_m;
                    obj.buffer.ref_posf_m (obj.curBufferIdx)    = obj.ref_posf_m;
                    obj.buffer.err_wob_ton (obj.curBufferIdx)   = obj.err_wob_ton;
                    obj.buffer.eI_wob_ton_s (obj.curBufferIdx)  = obj.eI_wob_ton_s;
                    obj.buffer.err_pos_m (obj.curBufferIdx)     = obj.err_pos_m;
                    obj.buffer.eI_pos_m_s (obj.curBufferIdx)    = obj.eI_pos_m_s;

                    obj.buffer.onBottom_b (obj.curBufferIdx)    = obj.onBottom_b;
                    obj.buffer.drillState (obj.curBufferIdx)    = obj.drillState;
                    obj.buffer.autoDrill (obj.curBufferIdx)     = obj.autoDrill;
                    obj.buffer.ufl_b (obj.curBufferIdx)         = obj.ufl_b;
                    obj.buffer.vel_max_mph (obj.curBufferIdx)   = obj.vel_max_mph;
                    obj.buffer.seqNumber (obj.curBufferIdx)     = obj.seqNumber;
                end
            end
        end
        
        function obj = statemachine(obj, nextstate)
            prevstate = obj.drillState;
            time=obj.t_s;
            if prevstate==1 %drilling,
                if obj.holeDepth_m<obj.target
                    newstate = prevstate;%drilling;
                elseif nextstate == 2
                    newstate = nextstate;%pulloffbottom;
                    disp(['Pull off bottom ' num2str(time/60, '%.2f') ' min']);
                else
                    disp(['Error ' num2str(time/60, '%.2f') ' min']);
                    pause(10);
                end
            end
    
            if prevstate==2 %pulloffbottom,
                if obj.bitDepth_m > obj.target
                    newstate = prevstate;%pulloffbottom;
                elseif nextstate==3 %stoprotation
                    newstate = nextstate;%stoprotation;
                    disp(['Stop rotation ' num2str(time/60, '%.2f') ' min']);
                elseif nextstate == 12
                    newstate = nextstate;%endsimulation
                    disp(['End ' num2str(time/60, '%.2f') ' min']);
                else
                    disp(['Error ' num2str(time/60, '%.2f') ' min']);
                    pause(10);
                end
            end

            if prevstate==3 %stoprotation,
                if obj.rotation_rpm>0
                    newstate = prevstate;%stoprotation;
                elseif nextstate == 4
                    newstate = nextstate;%stoppumps;
                    disp(['Stop pumps ' num2str(time/60, '%.2f') ' min']);
                else
                    disp(['Error ' num2str(time/60, '%.2f') ' min']);
                    pause(10);
                end
            end

            if prevstate==4 %stoppumps,
                if obj.q_p_lpm > 0
                    newstate = prevstate;%stoppumps;
                elseif nextstate == 5 %wait
                    newstate = nextstate;%wait;
                    disp(['Wait ' num2str(time/60, '%.2f') ' min']);
                elseif nextstate == 15 %DrillAddstand
                    newstate = nextstate;%DrillddStand;
                    disp(['Drill Add Stand ' num2str(time/60, '%.2f') ' min']);
                else
                    disp(['Error ' num2str(time/60, '%.2f') ' min']);
                    pause(10);
                end
            end

            if prevstate==5 %wait,
                if obj.t_s<obj.target
                    newstate = prevstate;%wait;
                elseif nextstate==9 %pull1stand,%TB fixed
                    newstate=9;%pull1stand;
                    disp(['Pull 1 stand ' num2str(time/60, '%.2f') ' min']);
                elseif nextstate==11 %run1stand,%TB fixed
                    newstate=nextstate;%run1stand;
                    disp(['Run 1 stand ' num2str(time/60, '%.2f') ' min']);
                elseif nextstate==6 %startpumps,
                    newstate=nextstate;%startpumps;
                    disp(['Start pumps ' num2str(time/60, '%.2f') ' min']);
                else
                    disp(['Error ' num2str(time/60, '%.2f') ' min']);
                    pause(10);
                end
            end
            
            if prevstate==15 %DrillAddStand,
                if obj.t_s<obj.target 
                    newstate = prevstate;%DrillAddStand;
                elseif nextstate==6 %startpumps,
                    newstate=nextstate;%startpumps;
                    disp(['Start pumps ' num2str(time/60, '%.2f') ' min']);
                else
                    disp(['Error ' num2str(time/60, '%.2f') ' min']);
                    pause(10);
                end
            end

            
            if prevstate==6 %startpumps,
                if obj.q_p_lpm<obj.target
                    newstate=prevstate; %startpumps;
                elseif nextstate == 7
                    newstate=nextstate; %startrotation;
                    disp(['Start rotation ' num2str(time/60, '%.2f') ' min']);
                else
                    disp(['Error ' num2str(time/60, '%.2f') ' min']);
                    pause(10);
                end
            end

            if prevstate==7 %startrotation,
                if obj.rotation_rpm<obj.simcase.rotation_rpm
                    newstate=prevstate; %startrotation;
                elseif nextstate == 8
                    newstate=nextstate;%goonbottom;
                    disp(['Go on bottom ' num2str(time/60, '%.2f') ' min']);
                else
                    disp(['Error ' num2str(time/60, '%.2f') ' min']);
                    pause(10);
                end
            end

            if prevstate==8 %goonbottom,
                if obj.bitDepth_m < obj.holeDepth_m
                    newstate=prevstate;%goonbottom;
                elseif nextstate == 1
                    newstate=nextstate;%drilling;
                    disp(['Start drilling ' num2str(time/60, '%.2f') ' min']);
                else
                    disp(['Error ' num2str(time/60, '%.2f') ' min']);
                    pause(10);
                end
            end

            if prevstate==9 %pull1stand,
                if obj.bitDepth_m>obj.target
                    newstate=prevstate;%pull1stand;
                elseif nextstate==14
                    newstate=nextstate;%remove 1 stand;
                    disp(['Remove 1 stand ' num2str(time/60, '%.2f') ' min']);
                elseif nextstate == 10
                    newstate=nextstate;%flowcheck;
                    disp(['Flow check ' num2str(time/60, '%.2f') ' min']);
                else
                    disp(['Error ' num2str(time/60, '%.2f') ' min']);
                    pause(10);
                end
            end    

            if prevstate==10 %flowcheck,
                if obj.t_s > obj.target
                    newstate=prevstate;%flowcheck;
                elseif nextstate==9 %pull1stand,%TB fixed
                    newstate=9;%pull1stand;
                    disp(['Pull 1 stand ' num2str(time/60, '%.2f') ' min']);
                elseif nextstate == 11 %run1stand,%TB fixed
                    newstate=nextstate;%run1stand;
                    disp(['Run 1 stand ' num2str(time/60, '%.2f') ' min']);
                elseif nextstate == 12 %end
                    newstate=nextstate;%end;
                    disp(['End simulation ' num2str(time/60, '%.2f') ' min']);
                else
                    disp(['Error ' num2str(time/60, '%.2f') ' min']);
                    pause(10);
                end
            end    
            if prevstate==11 %run1stand,
                if obj.bitDepth_m<obj.target
                    newstate=prevstate;%run1stand;
                elseif nextstate==13
                    newstate=nextstate;%add 1 stand;
                    disp(['Add 1 stand ' num2str(time/60, '%.2f') ' min']);
                elseif nextstate==10 %flowcheck;
                    newstate=nextstate;%flowcheck;
                    disp(['Flow check ' num2str(time/60, '%.2f') ' min']);
                elseif nextstate == 12
                    newstate=nextstate;%end simulation;
                    disp(['End ' num2str(time/60, '%.2f') ' min']);
                else
                    disp(['Error ' num2str(time/60, '%.2f') ' min']);
                    pause(10);
                end
            end

            if prevstate==12 %endsimulation,
                newstate=prevstate;%endsimulation;
            end 
            
            if prevstate==13 %TripAddStand    
                if obj.t_s > obj.target
                    newstate=prevstate;%flowcheck;
                elseif nextstate == 11 %run1stand,%TB fixed
                    newstate=nextstate;%run1stand;
                    disp(['Run 1 stand ' num2str(time/60, '%.2f') ' min']);
                else
                    disp(['Error ' num2str(time/60, '%.2f') ' min']);
                    pause(10);
                end
            end    

            if prevstate==14 %TripRemoveStand    
                if obj.t_s > obj.target 
                    newstate=prevstate;%flowcheck;
                elseif nextstate == 9 %pull1stand,%TB fixed
                    newstate=nextstate;%pull1stand;
                    disp(['Pull 1 stand ' num2str(time/60, '%.2f') ' min']);
                else
                    disp(['Error ' num2str(time/60, '%.2f') ' min']);
                    pause(10);
                end
            end    
            
            obj.drillState = newstate;
        end
        
        function obj = writecsvfile(obj,filename,DT_S,sampleTimeFile_s)
            constant_sampleTimeonFile = false;%true;%false;
            sampleTimeVol_s           = 29;%30
            wtime                     = datenum(2016,1,1)+(obj.buffer.t_s-0*0.5)/24/60/60;
            ECD_sg                    = obj.buffer.p_bit_bar*1e5./(obj.buffer.bitDepth_m)/obj.simcase.g_ms2/1000;

            fHeader1='depthBit;depthBit;flowOut;flowOut;flowIn;flowIn;flowOutPercent;flowOutPercent;volPitDrill;volPitDrill;volGainLoss;volGainLoss;volTripTank;volTripTank;presSP;presSP;depthHole;depthHole;heightHook;heightHook;velRop;velRop;weightHookload;weightHookload;weightOnBit;weightOnBit;rpm;rpm;densECDMWD;densECDMWD;velBlock;velBlock';
            fHeader2='INDEX;VALUE;INDEX;VALUE;INDEX;VALUE;INDEX;VALUE;INDEX;VALUE;INDEX;VALUE;INDEX;VALUE;INDEX;VALUE;INDEX;VALUE;INDEX;VALUE;INDEX;VALUE;INDEX;VALUE;INDEX;VALUE;INDEX;VALUE;INDEX;VALUE;INDEX;VALUE';
            fHeader3='time;m;time;lpm;time;lpm;time;%;time;m3;time;m3;time;m3;time;bar;time;m;time;m;time;m/h;time;kN;time;kN;time;rpm;time;sg;time;m/s';
            
            tvec     = datevec(wtime);            
            gldrill  = obj.buffer.V_pit_m3-obj.buffer.V_pit_m3(1);%To Be Improved
            F_hl_kN  = obj.buffer.F_hl_ton*obj.simcase.g_ms2;
            F_wob_kN = obj.buffer.F_wob_ton*obj.simcase.g_ms2;
            
            data=[...
                obj.buffer.bitDepth_m;...
                obj.buffer.q_out_lpm;...
                obj.buffer.q_p_lpm;...
                obj.buffer.q_out_pc;...
                obj.buffer.V_pit_m3;...
                gldrill;...
                obj.buffer.V_tt_m3;...
                obj.buffer.p_p_bar;...
                obj.buffer.holeDepth_m;...
                obj.buffer.blockPos_m;...
                obj.buffer.bitVel_mph;...
                F_hl_kN;...
                F_wob_kN;...
                obj.buffer.rotation_rpm;...
                ECD_sg;...
                obj.buffer.blockVel_mph/3600];
            Ncol = 16;
            Nrow = length(obj.buffer.t_s)-1;
            L    = floor(sampleTimeFile_s/DT_S);
            if constant_sampleTimeonFile
                index = 1:L:Nrow;
            else
                indx  = find(L*rand(1,Nrow) < 1);
                ix    = 1;
                index = [];
                for k = 1:length(indx)
                   if ix == 1 || (seconds(wtime(indx(k)) - wtime(indx(ix)))*86400 >= seconds(1))
                       ix    = k;
                       index = [index, indx(ix)];
                   end
                end                     
            end
            
            %To remove 60Z in csv file
            indxaround60sec = find(tvec(index,6)>59);
            index           = setdiff(index,index(indxaround60sec));
            
            % open a file for writing
            fid = fopen(filename, 'w');
            
            if fid==-1
                disp(['Cannot open file ' filename ' for writing']);
            else
                disp(['Saving to file ' filename]);
                fprintf(fid, '%s\n',fHeader1);
                fprintf(fid, '%s\n',fHeader2);
                fprintf(fid, '%s\n',fHeader3);

                ivol = index(1);%0
                for i = index %time/row
                    for j=1:Ncol-1 %var
                        if (j>4  && j<8)
                            if j == 5 %move vol index to next vol meas
                                k = ivol;
                                while ((obj.buffer.t_s(k) - obj.buffer.t_s(ivol) < sampleTimeVol_s) && k <= max(index)) || (tvec(k,6) > 59)
                                    k = k+1;
                                end
                                ivol = k;
                            end
                            if ivol <= max(index)
                                fprintf(fid, '%4.0f-%02.0f-%02.0fT%02.0f:%02.0f:%02.0fZ;%.2f;', tvec(ivol,1), tvec(ivol,2),tvec(ivol,3),tvec(ivol,4),tvec(ivol,5),tvec(ivol,6),data(j,ivol));                                
                            else%empty
                                fprintf(fid, ';;');                                
                            end                                
                        else
                            fprintf(fid, '%4.0f-%02.0f-%02.0fT%02.0f:%02.0f:%02.0fZ;%.2f;', tvec(i,1), tvec(i,2),tvec(i,3),tvec(i,4),tvec(i,5),min(tvec(i,6)),data(j,i));
                        end
                    end
                    j=Ncol;
                    fprintf(fid, '%4.0f-%02.0f-%02.0fT%02.0f:%02.0f:%02.0fZ;%.2f', tvec(i,1), tvec(i,2),tvec(i,3),tvec(i,4),tvec(i,5),tvec(i,6),data(j,i));
                    fprintf(fid, '\n');
                end
                fclose(fid);
            end
        end
        
        function obj = writetruedatafile(obj,filename,DT_S,sampleTimeFile_s)
            wtime    = datenum(2016,1,1)+(obj.buffer.t_s-0.5)/24/60/60;

            fHeader1 = 'flowIO;flowIO;volIO;volIO';
            fHeader2 = 'INDEX;VALUE;INDEX;VALUE';
            fHeader3 = 'time;lpm;time;m3';          
            tvec     = datevec(wtime);
            data  = [...
                obj.buffer.qIo_lpm;...
                obj.buffer.VIo_m3;...
                ];
            Ncol  = 2;
            Nrow  = length(obj.buffer.t_s)-1;
            L     = floor(sampleTimeFile_s/DT_S);
            index = 1:L:Nrow;
            %To remove 60Z in csv file
            indxaround60sec = find(tvec(index,6)>59);
            index           = setdiff(index,index(indxaround60sec));

            % open a file for writing
            fid = fopen(filename, 'w');           
            if fid==-1
                disp(['Cannot open file ' filename ' for writing']);
            else
                disp(['Saving to file ' filename]);
                fprintf(fid, '%s\n',fHeader1);
                fprintf(fid, '%s\n',fHeader2);
                fprintf(fid, '%s\n',fHeader3);

                for i=index %time
                    for j=1:Ncol-1 %var
                        fprintf(fid, '%4.0f-%02.0f-%02.0fT%02.0f:%02.0f:%02.0fZ;%.2f;', tvec(i,1), tvec(i,2),tvec(i,3),tvec(i,4),tvec(i,5),tvec(i,6),data(j,i));
                    end
                    j=Ncol;
                    fprintf(fid, '%4.0f-%02.0f-%02.0fT%02.0f:%02.0f:%02.0fZ;%.2f', tvec(i,1), tvec(i,2),tvec(i,3),tvec(i,4),tvec(i,5),tvec(i,6),data(j,i));
                    fprintf(fid, '\n');
                end
                fclose(fid);
            end
        end
        
        function obj = plotsimres(obj)
            tm=obj.buffer.t_s/60;
            global fignr;
            figure(fignr);fignr=fignr+1;
            disp('Plotting simulation results...');
            ax(1)=subplot(421);
            plot(tm,obj.buffer.F_hl_ton,'LineWidth',2)
            ylabel('Load [ton]')
            legend('Hookload')
            grid            
            ax(3)=subplot(423);
            plot(tm,obj.buffer.F_wob_ton,tm,obj.buffer.ref_wob_ton,tm,obj.buffer.ref_wobf_ton,tm,obj.buffer.F_drag_ton,'LineWidth',2)
            ylabel('Load [ton]')
            legend('WOB','WOB ref','WOB ref filt','Drag')
            grid
            ax(5)=subplot(425);
            plot(tm,obj.buffer.bitVel_mph,tm,obj.buffer.blockVel_mph,tm,obj.buffer.vel_max_mph,'c--',tm,-obj.buffer.vel_max_mph,'c--','LineWidth',2)
            ylabel('Velocity [m/hr]')
            legend('ROP/bit vel','Block vel','Max vel','Min vel')
            grid
            ax(7)=subplot(427);
            plot(tm,obj.buffer.blockPos_m + obj.buffer.bitDepth_m(1),tm,obj.buffer.bitDepth_m,tm,obj.buffer.ref_pos_m,tm,obj.buffer.ref_posf_m,tm,obj.buffer.holeDepth_m,tm,obj.buffer.lastConnectionDepth_m,'LineWidth',2)
            grid
            ylabel('Position [m]')
            legend(['Blockpos + ' num2str(obj.buffer.bitDepth_m(1),'%.0f')],'Bit depth','Ref','Ref filt','Hole depth','Last connection')
            xlabel('Time [min]')
            ax(2)=subplot(422);
            plot(tm,obj.buffer.q_p_lpm,tm,obj.buffer.q_bit_lpm,tm,obj.buffer.q_top_lpm,tm,obj.buffer.q_out_lpm,tm,obj.buffer.q_pit_lpm,tm,obj.buffer.q_shaker_lpm,tm,obj.buffer.q_tt_lpm,tm,obj.buffer.qIo_lpm,'LineWidth',2)
            grid
            legend('Pump','Bit','Top annulus','Out','To pit','To shaker','Trip tank pump','True influx/loss')
            ylabel('Flow [lpm]')
            ax(4)=subplot(424);
            plot(tm,obj.buffer.dp_dp_bar,tm,obj.buffer.dp_bit_bar,tm,obj.buffer.dp_ann_bar,tm,obj.buffer.p_cut_bar,'LineWidth',2)
            grid
            legend('Fric DP','Fric bit','Fric ann','Cuttings')
            ylabel('Diff pressure [bar]')
            ax(6)=subplot(426);
            plot(tm,obj.buffer.p_p_bar,tm,obj.buffer.p_bit_bar,tm,obj.buffer.p_bh_bar,tm,obj.buffer.rotation_rpm,'LineWidth',2)
            grid
            legend('Pump pressure','Bottomhole pressure','Pressure at bit','Rotation')
            ylabel('Pressure [bar]/Rotation [rpm]')
            ax(8)=subplot(428);
            plot(tm,obj.buffer.V_fl_m3-obj.buffer.V_fl_m3(1),tm,obj.buffer.V_pit_m3-obj.buffer.V_pit_m3(1),tm,obj.buffer.V_well_m3-obj.buffer.V_well_m3(1),tm,obj.buffer.V_tt_m3-obj.buffer.V_tt_m3(1),tm,obj.buffer.VIo_m3,'LineWidth',2)
            grid
            legend('DVfl','DVpit','DVwell','DVtrip','True influx/loss')
            ylabel('Volume change [m3]')
            xlabel('Time [min]')

            figure(fignr);fignr=fignr+1;
            ax(9)=subplot(221);
            plot(tm,obj.buffer.err_wob_ton,'LineWidth',2)
            ylabel('Load [ton]')
            legend('eWOB')
            grid
            ax(10)=subplot(223);
            plot(tm,obj.buffer.err_pos_m,'LineWidth',2)
            ylabel('Position [m]')
            legend('ePOS')
            grid
            xlabel('Time [min]')
            ax(12)=subplot(222);
            plot(tm,obj.buffer.eI_wob_ton_s,'LineWidth',2)
            grid
            legend('eIWOB')
            ax(14)=subplot(224);
            plot(tm,obj.buffer.eI_pos_m_s,'LineWidth',2)
            grid
            legend('eIPOS')
            xlabel('Time [min]')

            figure(fignr);fignr=fignr+1;
            ax(15)=subplot(221);
            plot(tm,obj.buffer.drillState,'LineWidth',2)
            ylabel('Drillstate')
            legend('State')
            text(10,10,'start=0;drill=1;offbot=2;stoprot=3;stopp=4;wait=5')
            text(10, 8,'startp=6;startrot=7;goonbot=8;pull=9;fcheck=10,pull=11,end=12')
            grid
            ax(16)=subplot(222);
            plot(tm,obj.buffer.onBottom_b,tm,obj.buffer.autoDrill+0.1,tm,obj.buffer.seqNumber-0.1,'LineWidth',2)
            ylabel('State')
            text(10,14,'BadData(-1) Other(0) Drill(1) FCheck(2) Cond(3) Trip(4)')            
            legend('Onbottom=1, Offbottom=0','Autodrill: 0=man, 1=wob, 2=pos','Sequence')
            grid
            ax(17)=subplot(223);
            plot(tm,obj.buffer.dbdV_barm3,'LineWidth',2)
            ylabel('beta/Vol [bar/m3]')
            legend('True beta/Vol')
            grid
            xlabel('Time [min]')
            ax(18)=subplot(224);
            plot(tm,obj.buffer.qIo_lpm,tm,obj.buffer.VIo_m3*1000,'LineWidth',2)
            ylabel('[lpm] and [liter]')
            legend('qIo','VIo*1000')
            grid
            xlabel('Time [min]')

            figure(fignr);fignr=fignr+1;
            %ax(18)=subplot(111);
            plot(tm,obj.buffer.q_out_pc,'LineWidth',2)
            ylabel('[%]')
            xlabel('Time [min]')
            legend('Paddle flowOut')
            grid

            linkaxes(ax,'x') 
        end
        
    end
end


