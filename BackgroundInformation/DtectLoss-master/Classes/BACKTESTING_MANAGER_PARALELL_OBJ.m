% The bactesting manager is in charge of orchestrating the back tests
% of the DTECTLOSS_REALTIME class on historical data.
% the backtesting manager simulates a "live stream" of data by calling the
% FIELD_DATA_PLAYER and feeding the DTECTLOSS_REALTIME object one vector at
% a time. The backtesting manager runs through all the files of historical
% data in the data folder specified.

classdef BACKTESTING_MANAGER_PARALELL_OBJ < handle
    properties(SetAccess = private,GetAccess= public)
        numScenarios;
        simName             = [];
        didLoadOk           = 0;
        configCell;
        algResults          = [];
    end
    properties (Constant)
        fieldDataPlayerDT_s = 1;
        dispEveryXprc       = 1; % how often to display "percent complete" text
    end
    properties (SetAccess = private,GetAccess= private)
        processDataNames;
        bs              = [];       % simscenario handle: hold the saved simulation results of the method
        fdp             = [];       % fieldplayer handle: loads and runs a timeseries collection one step at a time.
        realtimeObj     = [];
        lastDispCompletionPrc =[];
        dT                          % timestep of current file
        pitThreshold
        flowDeadBand_lpm ;
        volumeThreshold_m3;
        rawDataUnits    = [];
        isDebug;
        dataFiles;   % cell with the name of all the files to run through
        nameDesiredProcessTags
        rootDir;
        buildNr ;
        doWrapperTest;% if set to 1, the wrapper functions of DTECTLOSS_REALTIME_OBJ are tested
        tagServerPorts;
        SIGMA_ip;
        doSigmaPlotter;
        stepCounter;
    end
    methods
        function obj                = BACKTESTING_MANAGER_PARALELL_OBJ()
            obj.dataFiles ;
            obj.processDataNames;
            obj.bs;
            obj.fdp;
            obj.realtimeObj;
            obj.flowDeadBand_lpm  ;
            obj.volumeThreshold_m3  ;
            obj.pitThreshold   ;
            obj.rootDir ;
            obj.configCell;
            obj.doWrapperTest;
            obj.tagServerPorts;
            obj.didLoadOk;
            obj.doSigmaPlotter;
        end
        
        
        function obj                = initalize(obj, root_dir,nameDesiredProcessTags,doWrapperTest,SIGMA_ip,isDebug,buildNr,doSigmaPlotter)
            obj.dataFiles           = returnEveryFileInDirectoryTree(root_dir,'.zip');
            obj.configCell          = readXLSconfig(root_dir);
            obj.nameDesiredProcessTags = nameDesiredProcessTags;
            obj.rootDir             = root_dir;
            obj.doWrapperTest       = doWrapperTest;
            obj.isDebug             = isDebug;
            obj.numScenarios        = (numel(obj.configCell));
            obj.SIGMA_ip            = SIGMA_ip;
            obj.buildNr             = buildNr;
            obj.doSigmaPlotter      = doSigmaPlotter;
            obj.stepCounter         = 0;
            obj.algResults          = struct;
        end
        
        % loads simulation, runs a specified percent of simulation, thens
        % cleans up and plots
        function [obj,didLoadOk,simDuration_s]       = RunScenarioToCompletionPrc(obj,scenarioIdx, prcToRun)
            [obj ,didLoadOk]           = obj.loadSim(scenarioIdx);
            obj.didLoadOk               = didLoadOk;
            if (didLoadOk==1)
                obj.lastDispCompletionPrc(scenarioIdx)=0;
                while (obj.fdp{scenarioIdx}.simProgress<prcToRun)
                    obj.StepAllAlgorithmsAndSave(scenarioIdx);
                    if  obj.fdp{scenarioIdx}.simProgress >= obj.lastDispCompletionPrc+obj.dispEveryXprc
                        fprintf('%s : %.0f percent complete\n', obj.simName{scenarioIdx},obj.fdp{scenarioIdx}.simProgress);
                        obj.lastDispCompletionPrc(scenarioIdx) = obj.fdp{scenarioIdx}.simProgress;
                    end
                end
                if (obj.SIGMA_ip>0)
                    obj.bs{scenarioIdx}.logoff();% kills tagserver and loggingclient for this instance.algorithmsToRun
                end
                simDuration_s = obj.fdp{scenarioIdx}.simDuration;
            else
                disp('skipping, error while loading....');
                simDuration_s = 0;
            end
        end
        
        % re-runnable/stateful runner of simulation, for injecting events
        % at certaint times(originally for testing reset function)
        function [obj, didLoadOk,simDuration_s] = RunScenarioAnAdditionalPrc(obj, scenarioIdx, prcToAddToRun)
            
            if not(obj.didLoadOk)
                [obj, didLoadOk] = obj.loadSim(scenarioIdx);
                obj.didLoadOk = didLoadOk;
                obj.lastDispCompletionPrc(scenarioIdx)=0;
            end
            if obj.didLoadOk
                initalSimProgress = obj.fdp{scenarioIdx}.simProgress;
                while obj.fdp{scenarioIdx}.simProgress - initalSimProgress < prcToAddToRun && obj.fdp{scenarioIdx}.simProgress < 100
                    obj.StepAllAlgorithmsAndSave(scenarioIdx);
                    if  obj.fdp{scenarioIdx}.simProgress >= obj.lastDispCompletionPrc + obj.dispEveryXprc
                        fprintf('%s : %.0f percent complete\n', obj.simName{scenarioIdx}, obj.fdp{scenarioIdx}.simProgress);
                        obj.lastDispCompletionPrc(scenarioIdx) = obj.fdp{scenarioIdx}.simProgress;
                    end
                end
                if obj.fdp{scenarioIdx}.simProgress >= 100 &&  obj.SIGMA_ip > 0
                    obj.bs{scenarioIdx}.logoff();% kills tagserver and loggingclient for this instance.algorithmsToRun
                end
                simDuration_s = obj.fdp{scenarioIdx}.simDuration;
            else
                disp('skipping, error while loading....');
                simDuration_s = 0;
            end
        end
        
        function [obj, dtectLossOutputStruct] = RunScenarioOneStepAnGetOutput(obj, scenarioIdx)
            [obj, dtectLossOutputStruct] = obj.StepAllAlgorithmsAndSave(scenarioIdx);
        end
        
        function [obj] = SendResetCommandToAlgorithm(obj)
            DTECTLOSS_REALTIME_OBJ_softreset();
        end
        
        %write statistics to comment.txt and makes backtesting graph in folder on g-disk
        function [obj, evaluateResults, alarmTypeCount] = evaluateAndFinalize(obj, scenarioIdx, FIELD_DATA_ROOT_DIR)
            comment = '';
            [obj.bs{scenarioIdx}, evaluateResults, alarmTypeCount] =...
                obj.bs{scenarioIdx}.evaluateCurrentRun(comment, obj.configCell(scenarioIdx).algorithmsToRun, FIELD_DATA_ROOT_DIR, obj.algResults);
        end
    end
    methods(Access= private)% *********** private methods ***********
        % can involve either loading a new file or loading the next time
        % series collection
        function [obj,didLoadOk]      = loadSim(obj,scenarioIdx)
            didLoadOk                 = 0;% status=0:error, status=1: loaded ok
            if obj.configCell(scenarioIdx).useForBacktesting ~=1
                fprintf('scenario %i ,useForBacktesting not set to 1, skipping.... \n',scenarioIdx);
                return;
            end
            curDataFileIdx      = nan;
            numFilesFound       = 0;
            for k= 1:1:numel(obj.dataFiles)
                if ~isempty( strfind(obj.dataFiles{k},sprintf('-%i.',scenarioIdx)))
                    curDataFileIdx = k;
                    numFilesFound = numFilesFound+1;
                end
            end
            if numFilesFound~=1
                fprintf('****Warning: found %i files with filename with index -%i., skipping.....*******************\n',numFilesFound,scenarioIdx);
                return;
            end
            obj.simName{scenarioIdx}= sprintf('%s',strrep(strrep(obj.dataFiles{curDataFileIdx},'.mat',''),'.zip',''));
            disp(['****************************' ,obj.simName{scenarioIdx} ,'****************************']);
            currentDataFile         = fullfile(obj.rootDir ,obj.dataFiles{curDataFileIdx});
            currentMatFile          = updateMatFile(currentDataFile);
            obj.fdp{scenarioIdx}    = FIELD_DATA_PLAYER_OBJ(currentMatFile,obj.fieldDataPlayerDT_s,obj.nameDesiredProcessTags);
            obj.fdp{scenarioIdx}    = obj.fdp{scenarioIdx}.initialize;
            
            unitsOfProvidedTags     = BACKTESTING_MANAGER_PARALELL_OBJ.setUnitsOfProvidedTags(obj.fdp{scenarioIdx}.rawDataUnits,obj.configCell(scenarioIdx));
            
            if ~isempty(obj.configCell(scenarioIdx).areaDPPOOH_m2) && ~isempty(obj.configCell(scenarioIdx).areaDPRIH_m2)
                unitsOfProvidedTags.areaDPPOOH_m2 = 'm2';
                unitsOfProvidedTags.areaDPRIH_m2  = 'm2';
                areaDPPOOH_m2 = obj.configCell(scenarioIdx).areaDPPOOH_m2;
                areaDPRIH_m2  = obj.configCell(scenarioIdx).areaDPRIH_m2;
            else
                unitsOfProvidedTags.areaDPPOOH_m2 = 'm2';
                unitsOfProvidedTags.areaDPRIH_m2  = 'm2';
                areaDPPOOH_m2 = 0.010;
                areaDPRIH_m2  = 0.015;
            end
            % TODO: Bytt rekkef�lge - tilpass bruk i dtectloss.statoil.no og p�
            % rigg
            DTECTLOSS_REALTIME_OBJ_init(obj.configCell(scenarioIdx).algorithmsToRun{1}, unitsOfProvidedTags, obj.fdp{scenarioIdx}.startTime, true);
            DTECTLOSS_REALTIME_OBJ_setAreadp(areaDPPOOH_m2, areaDPRIH_m2);
            if (obj.SIGMA_ip>0)
                obj.bs{scenarioIdx}      = BACKTEST_SCENARIO_OBJ ();
                obj.tagServerPorts{scenarioIdx} = 4655+scenarioIdx;%give each scenario each own tag server port based on its index
                obj.bs{scenarioIdx}      = obj.bs{scenarioIdx}.initalize(obj.simName{scenarioIdx},obj.tagServerPorts{scenarioIdx},obj.SIGMA_ip,obj.isDebug,obj.buildNr,obj.doSigmaPlotter);
            end
            didLoadOk               = 1;
        end
        %------------------------------------
        % run at each iteration to save the result of all algorithms
        %         function [obj]               = StepAllAlgorithmsAndSave(obj,scenarioIdx)
        %              obj.fdp{scenarioIdx}    = obj.fdp{scenarioIdx}.asyncPlay; % iterate field data player
        %              rawData                 = obj.fdp{scenarioIdx}.output;
        %              rawData                 = BACKTESTING_MANAGER_PARALELL_OBJ.ChooseTagsForRawData(rawData,obj.configCell(scenarioIdx));
        %              dtectLossOutput          = DTECTLOSS_REALTIME_OBJ_stepAlgorithm(rawData);
        %              obj.bs{scenarioIdx}.saveIteration(obj.fdp{scenarioIdx}.absTime,dtectLossOutput);
        %         end
        function [obj, dtectLossOutputStruct] = StepAllAlgorithmsAndSave(obj, scenarioIdx)
            obj.fdp{scenarioIdx} = obj.fdp{scenarioIdx}.asyncPlay; % iterate field data player
            rawData = obj.fdp{scenarioIdx}.output;
            rawData = BACKTESTING_MANAGER_PARALELL_OBJ.ChooseTagsForRawData(rawData, obj.configCell(scenarioIdx));
            [dtectLossOutputStruct, dtectLossOutputCell] = DTECTLOSS_REALTIME_OBJ_stepAlgorithm(rawData);
            if obj.SIGMA_ip > 0
                obj.bs{scenarioIdx}.saveIteration(obj.fdp{scenarioIdx}.absTime, dtectLossOutputStruct, dtectLossOutputCell);
            end
            
            % Save dtectLossOutputStruct to algResults timeseries
            obj.algResults.flowIO_lpm_voting.T(obj.stepCounter + 1) = obj.fdp{scenarioIdx}.absTime;
            obj.algResults.flowIO_lpm_voting.V(obj.stepCounter + 1) = dtectLossOutputStruct.flowIOflow_lpm_voting;
            obj.algResults.volIO_m3_voting.T(obj.stepCounter + 1) = obj.fdp{scenarioIdx}.absTime;
            obj.algResults.volIO_m3_voting.V(obj.stepCounter + 1) = dtectLossOutputStruct.volIO_m3_voting;
            obj.algResults.alarmState_voting.T(obj.stepCounter + 1) = obj.fdp{scenarioIdx}.absTime;
            obj.algResults.alarmState_voting.V(obj.stepCounter + 1) = dtectLossOutputStruct.alarmState_voting;
            obj.algResults.alarmType_voting.T(obj.stepCounter + 1) = obj.fdp{scenarioIdx}.absTime;
            obj.algResults.alarmType_voting.V(obj.stepCounter + 1) = dtectLossOutputStruct.alarmType_voting;
            
            
            
            obj.stepCounter = obj.stepCounter + 1;
        end
    end
    methods(Static)
        function unitsProvided = setUnitsOfProvidedTags(rawUnits,configCell)
            unitsProvided               = BACKTESTING_MANAGER_PARALELL_OBJ.chooseTagsInnerFunction(rawUnits,configCell);
        end
        function rawDataWithChosenTags   = ChooseTagsForRawData(rawData,configCell)
            rawDataWithChosenTags       = BACKTESTING_MANAGER_PARALELL_OBJ.chooseTagsInnerFunction(rawData,configCell);
        end
        function out = chooseTagsInnerFunction(in,configCell)
            out = in;
            if isfield(configCell,'usedTripTankMeasTagName')
                out.volTripTank = in.(configCell.('usedTripTankMeasTagName'));
            else
                error('missing config:usedTripTankMeasTagName ');
            end
            if isfield(configCell,'usedFlowMeasTagName')
                out.flowOut = in.(configCell.('usedFlowMeasTagName'));
            else
                error('missing config:usedFlowMeasTagName ');
            end
            if isfield(configCell,'usedPitVolMeasTagName')
                out.pitVolume = in.(configCell.('usedPitVolMeasTagName'));
                %out.volPit = in.(configCell.('usedPitVolMeasTagName'));
            else
                error('missing config:usedPitVolMeasTagName ');
            end
        end
    end
end













