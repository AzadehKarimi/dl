classdef VOTING_ALG_OBJ < DTECTLOSS_SUBMODULE
% generate alarms based on expected flow out, expected pump pressure and
% expected gain/loss volume.
% Voting: alarm if 2/3 are above thresholds

    properties (SetAccess = {?private, ?matlab.unittest.TestCase})
        flowIOflow_lpm;
        flowIOpres_lpm;
        flowIOpit_lpm;
        flowIOtrip_lpm;
        presIO_bar;
        volIOflow_m3;
        volIOpres_m3;
        volIOpit_m3;
        volIOtrip_m3;
        alarmState;
        alarmType;
        resetAlarm;
        recalibrateFlowOutNow;
        timeRecalibrateFlowOut_s;
        recalibratePresSPNow;
        timeRecalibratePresSP_s;
        alarmStatus;
        timeCurrent_d;
        timeLast_d;
        timeAlarmActive_s;
        timeSinceLastAlarm_s;
        timeSinceValid_s;
        runSoftresetnow;
        timeHighFlowError_s;
        timeHighVolError_s;
        volIO_m3;
        flowIO_lpm;
        dt_s;
        params;
        flowIOinj_lpm;%To remove slowly varying bias on flowmeter
        filtPitData;
        filtTripData;
    end
    
    properties (SetAccess = public)
        timeStep_max_s       = 60; 
        lengthOffBottomLim_m = 20;
        flowPumpsOnMin_lpm   = 100;
    end
    
    properties (Constant)
        dispDebug       = false;
        volGainDetectorThreshold_m3 = 0.35; % ensure that this limit is less than the volumeLimit_m3 in GAIN_DETECTOR_OBJ
        LPM_TO_M3PS        = 1/60000;
        MISSING_VAL_CODE   = -0.99925;
    end
    
  methods        
        function obj = VOTING_ALG_OBJ()
            obj.flowIOflow_lpm  = [];
            obj.flowIOpres_lpm  = [];
            obj.flowIOpit_lpm   = [];
            obj.flowIOtrip_lpm  = [];
            obj.presIO_bar      = [];
            obj.volIOflow_m3    = [];
            obj.volIOpres_m3    = [];
            obj.volIOpit_m3     = [];
            obj.volIOtrip_m3    = [];
            obj.volIO_m3        = [];
            obj.flowIO_lpm      = [];
            obj.filtPitData;
            obj.filtTripData;
        end
        
        function obj = initialize(obj, params)
            obj.flowIOflow_lpm  = 0;
            obj.flowIOpres_lpm  = 0;
            obj.flowIOpit_lpm   = 0;
            obj.flowIOtrip_lpm  = 0;
            obj.presIO_bar      = 0;
            obj.volIOflow_m3    = 0;
            obj.volIOpres_m3    = 0;
            obj.volIOpit_m3     = 0;
            obj.volIOtrip_m3    = 0;
            obj.volIO_m3        = 0;
            obj.flowIO_lpm      = 0;
            obj.alarmState      = AlarmState.Disabled;
            obj.alarmType       = AlarmType.Normal;
            obj.timeCurrent_d   = 0;
            obj.timeLast_d      = 0;
            obj.dt_s            = 0;
            obj.params          = params;
            obj.flowIOinj_lpm   = 0;
            obj.resetAlarm      = false;
            obj.recalibrateFlowOutNow       = false;
            obj.timeRecalibrateFlowOut_s    = 0;
            obj.recalibratePresSPNow        = false;
            obj.timeRecalibratePresSP_s     = 0;
            obj.timeAlarmActive_s           = 0;
            obj.timeSinceLastAlarm_s        = 0;
            obj.timeSinceValid_s            = -0.99925;
            obj.runSoftresetnow             = false;
            obj.timeHighFlowError_s         = 0;
            obj.timeHighVolError_s          = 0;
            obj.filtPitData                 = FILTERDERIVATIVE_OBJ(obj.params.timeFiltPitVol_s);
            obj.filtTripData                = FILTERDERIVATIVE_OBJ(obj.params.timeFiltPitVol_s);
        end

        function obj = softReset(obj)%Not in use
            obj.flowIOflow_lpm  = 0;
            obj.flowIOpres_lpm  = 0;
            obj.flowIOpit_lpm   = 0;
            obj.flowIOtrip_lpm  = 0;
            obj.presIO_bar      = 0;
            obj.volIOflow_m3    = 0;
            obj.volIOpres_m3    = 0;
            obj.volIOpit_m3     = 0;
            obj.volIOtrip_m3    = 0;
            obj.volIO_m3        = 0;
            obj.flowIO_lpm      = 0;
            obj.alarmState      = AlarmState.Disabled;
            obj.alarmType       = AlarmType.Normal;
            obj.timeCurrent_d   = 0;
            obj.timeLast_d      = 0;
            obj.dt_s            = 0;
            obj.flowIOinj_lpm   = 0;
            obj.resetAlarm      = false;
            obj.recalibrateFlowOutNow       = false;
            obj.timeRecalibrateFlowOut_s    = 0;
            obj.recalibratePresSPNow        = false;
            obj.timeRecalibratePresSP_s     = 0;
            obj.timeAlarmActive_s           = 0;
            obj.timeSinceLastAlarm_s        = 0;
            obj.timeHighFlowError_s         = 0;
            obj.timeHighVolError_s          = 0;
            obj.runSoftresetnow             = true;
        end
               
        function flowOut_lpm = setFlowOut(obj, processData)
            if not(processData.flowOutFiltered_lpm == obj.MISSING_VAL_CODE) && not(isnan(processData.flowOutFiltered_lpm))
                flowOut_lpm = processData.flowOutFiltered_lpm;
            elseif not(processData.flowOut_lpm == obj.MISSING_VAL_CODE)
                flowOut_lpm = processData.flowOut_lpm;
            else
                flowOut_lpm = obj.MISSING_VAL_CODE;
            end        
        end
        
        function obj = calcIOFlow(obj, algData, processData, sysState)
            flowOut_lpm = setFlowOut(obj, processData);            
            % check if algData.flowOutExpected_lpm is empty to avoid error
            if ~isempty(algData.flowOutExpected_lpm) && not(flowOut_lpm == obj.MISSING_VAL_CODE) && not(algData.isDownlinking)...
                    && algData.isCalibratedExpFlowOut && algData.isCalibratedMeasFlowOut && not(processData.flowIn_lpm == obj.MISSING_VAL_CODE)
                obj.flowIOflow_lpm      = flowOut_lpm - algData.flowOutExpected_lpm; 
            else
                obj.flowIOflow_lpm      = 0;
            end
            flowerracc_lpm = obj.flowIOflow_lpm + obj.flowIOinj_lpm; 
            if abs(flowerracc_lpm) > obj.params.flowDeadband_lpm && not(processData.flowIn_lpm == obj.MISSING_VAL_CODE) %Accumulate flow error to volume
                obj.volIOflow_m3    = obj.volIOflow_m3 + obj.dt_s*flowerracc_lpm*obj.LPM_TO_M3PS;% vIo[m3] = dt[s] *qIo[lpm]
            end
            if obj.timeRecalibrateFlowOut_s > 60 || algData.resetVolIOnow || not(sysState.isOnBottom)
                obj.flowIOflow_lpm  = 0;
                obj.volIOflow_m3    = 0;
            end
        end
        
        function obj = calcIOPres(obj, algData, processData, sysState)
            if not(processData.presSP_bar == obj.MISSING_VAL_CODE) && not(algData.isDownlinking) && algData.isCalibratedExpPresSP ...
                    && not(processData.flowIn_lpm == obj.MISSING_VAL_CODE)
                if processData.presSP_bar > 5 %Only use presSP when float is assumed open
                    obj.presIO_bar          = processData.presSP_bar - algData.presSPExpected_bar;
                else
                    obj.presIO_bar      = 0;
                end
                obj.flowIOpres_lpm      = obj.presIO_bar*obj.params.scalePresToFlow;
            else
                obj.flowIOpres_lpm      = 0;
            end
            if (abs(obj.flowIOpres_lpm) > obj.params.flowDeadband_lpm && algData.isCalibratedExpPresSP && not(processData.flowIn_lpm == obj.MISSING_VAL_CODE))    %Accumulate flow error to volume
                if not(algData.isDownlinking)  
                    obj.volIOpres_m3    = obj.volIOpres_m3 + obj.dt_s*obj.flowIOpres_lpm*obj.LPM_TO_M3PS;% vIo[m3] = dt[s] *qIo[lpm]
                else
                    obj.volIOpres_m3    = 0;
                end
            end
            if obj.timeRecalibratePresSP_s > 60 || algData.resetVolIOnow || not(sysState.isOnBottom)
                obj.flowIOpres_lpm  = 0;
                obj.volIOpres_m3    = 0;
            end
        end

        function obj = calcIOPit(obj, algData, processData, sysState)
            if not(processData.volGainLoss_m3 == obj.MISSING_VAL_CODE) && algData.isCalibratedExpPitVol && not(processData.flowIn_lpm == obj.MISSING_VAL_CODE)
                obj.volIOpit_m3                   = processData.volGainLoss_m3 - algData.volGainLossExpected_m3;
                [obj.filtPitData, flowIOpit_m3ps] = obj.filtPitData.smoothderivative2ndorder(obj.volIOpit_m3, obj.timeCurrent_d);              
                obj.flowIOpit_lpm = flowIOpit_m3ps/obj.LPM_TO_M3PS;
            elseif not(algData.isCalibratedExpPitVol) 
                obj.volIOpit_m3   = 0;
            end                                      
            if not(sysState.isOnBottom)
                obj.flowIOpit_lpm = 0;
                obj.volIOpit_m3   = 0;
            end                
        end
        
        function obj = calcIOTripTank(obj, algData, sysState)
            if not(sysState.isOnBottom)
                obj.volIOtrip_m3                    = algData.volTripTankCumError_m3;
                [obj.filtTripData, flowIOtrip_m3ps] = obj.filtTripData.smoothderivative2ndorder(obj.volIOtrip_m3, obj.timeCurrent_d);              
                obj.flowIOtrip_lpm                  = flowIOtrip_m3ps/obj.LPM_TO_M3PS;   
            else
                obj.volIOtrip_m3                    = 0;
                obj.flowIOtrip_lpm                  = 0;
            end    
        end
        
        function obj = setTimeHighFlowError(obj, algData, processData)
            if (~isempty(obj.flowIOflow_lpm) ...
                    && abs(obj.flowIOflow_lpm) > max(obj.params.flowIOAlarm_lpm, processData.flowIn_lpm*obj.params.flowIOAlarm_pc/100) ...
                    && algData.isCalibratedExpFlowOut  && algData.isCalibratedMeasFlowOut...
                    && not(processData.flowIn_lpm == obj.MISSING_VAL_CODE))
                obj.timeHighFlowError_s = obj.timeHighFlowError_s + obj.dt_s;
            else
                obj.timeHighFlowError_s = 0;
            end                           
        end
        
        function obj = setTimeHighVolError(obj, algData, processData)
            if algData.isRampingUp
                volPitOnlyAlarmLimit_m3 = obj.params.volIOAlarmPumpStart_m3;% Less accurate pitvol model when ramping up pump
            else
                volPitOnlyAlarmLimit_m3 = obj.params.volIOAlarm_m3;
            end
            if (abs(obj.volIOpit_m3) > volPitOnlyAlarmLimit_m3 && algData.isCalibratedExpPitVol && not(processData.flowIn_lpm == obj.MISSING_VAL_CODE))
                obj.timeHighVolError_s = obj.timeHighVolError_s + obj.dt_s;
            else
                obj.timeHighVolError_s = 0;
            end
        end
        
        function obj = setInjection(obj)
            flowerracc_lpm      = obj.flowIOflow_lpm + obj.flowIOinj_lpm; 
            obj.flowIOinj_lpm   = obj.flowIOinj_lpm - obj.dt_s*flowerracc_lpm/obj.params.timeConstInjFIO_s;%injection - to make DQ->0
            obj.flowIOinj_lpm   = min(obj.params.flowMaxInjection_lpm, max(-obj.params.flowMaxInjection_lpm, obj.flowIOinj_lpm));
            obj.volIOflow_m3    = obj.volIOflow_m3 - obj.dt_s*(sign(obj.volIOflow_m3)*min(obj.params.flowMaxInjection_lpm*obj.LPM_TO_M3PS,abs(obj.volIOflow_m3/obj.params.timeConstInjVIOF_s)));% injection - to make DV->0
            obj.volIOpres_m3    = obj.volIOpres_m3 - obj.dt_s*(sign(obj.volIOpres_m3)*min(obj.params.flowMaxInjection_lpm*obj.LPM_TO_M3PS,abs(obj.volIOpres_m3/obj.params.timeConstInjVIOP_s)));% injection - to make DV->0
        end
        
        function obj = updateTime(obj, timeCurrent_d, dt_s)
            obj.dt_s          = dt_s;
            obj.timeCurrent_d = timeCurrent_d;            
        end
        
        function [obj, result] = stepIt(obj, processData, algData, sysState, timeCurrent_d, dt_s)
            obj = updateTime(obj, timeCurrent_d, dt_s);
            
            if obj.dt_s > obj.timeStep_max_s %reset
                obj.alarmStatus = ['Reset, time step = ' num2str(obj.dt_s,'%.2f') ' sec'];
                obj             = softReset(obj);%initialize(obj,obj.params); 
                obj.timeLast_d  = obj.timeCurrent_d;
                [obj, result]   = returnResultStruct(obj, sysState);
                return;
            end
            
            obj.timeLast_d     = obj.timeCurrent_d;

            %debugging
            debugTime = datenum(2015, 10, 30, 04, 34, 56);
            %debugging obj.processObj.timeCurrent_d 
            
            obj = calcIOFlow(obj, algData, processData, sysState);
            obj = calcIOPres(obj, algData, processData, sysState);                       
            obj = calcIOPit(obj, algData, processData, sysState);                       
            obj = calcIOTripTank(obj, algData, sysState);  
            obj = setTimeHighFlowError(obj, algData, processData);
            obj = setTimeHighVolError(obj, algData, processData);
            obj = setInjection(obj);
            obj = setAlarmState(obj, algData, processData, sysState);
            [obj, result] = returnResultStruct(obj, sysState);
        end

        function obj = checkResetAlarm(obj)
            if  (obj.timeAlarmActive_s > obj.params.timeResetAlarm_s)
                obj.alarmState          = AlarmState.Normal;
                obj.alarmType           = AlarmType.Normal;
                obj.timeAlarmActive_s   = 0;
                obj.volIOflow_m3        = 0;
                obj.volIOpres_m3        = 0;
                obj.volIOtrip_m3        = 0;
                obj.volIO_m3            = 0;                
                obj.resetAlarm          = true;
                obj.recalibrateFlowOutNow = true;%XXX Test
                if obj.dispDebug
                    disp(['Alarm reset - ' num2str( obj.params.timeResetAlarm_s/60,'%.0f') ' min timeout']);
                end
            else
                obj.resetAlarm = false;
            end            
        end
                
        function obj = setAlarmStateTripping(obj)
            HighGainTrip     =  +obj.volIOtrip_m3 > obj.params.volumeThresholdTrippingAlarm_m3;
            HighLossTrip     =  -obj.volIOtrip_m3 > obj.params.volumeThresholdTrippingAlarm_m3;
            obj.volIO_m3     = obj.volIOtrip_m3;
            obj.volIOpit_m3  = 0;
            obj.volIOpres_m3 = 0;
            obj.volIOflow_m3 = 0;
            if (obj.alarmState == AlarmState.Alarm) %Hold alarm for 5 minutes
                obj.alarmState = AlarmState.Alarm;
            elseif (obj.timeSinceLastAlarm_s < 300) %Inhibit alarm first 5 minutes after reset
                obj.alarmState = AlarmState.Normal;
                obj.alarmType  = AlarmType.Normal;
            elseif (HighGainTrip)
                obj.alarmState = AlarmState.Alarm;%Influx Trip
                obj.alarmType  = AlarmType.TripOnlyInfluxAlarm;
            elseif (HighLossTrip)
                obj.alarmState = AlarmState.Alarm;%Loss Trip
                obj.alarmType  = AlarmType.TripOnlyLossAlarm;
            elseif (obj.alarmState == AlarmState.Alarm) %Old alarm
                obj.alarmState = AlarmState.Alarm;%Do nothing
            elseif ( (obj.volIOtrip_m3 > obj.params.volumeThresholdWarning_m3   && ...
                      obj.volIOtrip_m3 + obj.flowIOtrip_lpm/60000*obj.params.timeWarning_s > obj.params.volumeThresholdAlarm_m3) ) 
                  obj.alarmState = AlarmState.Warning;%Influx
                  obj.alarmType  = AlarmType.InfluxWarning;
            elseif ( (obj.volIOtrip_m3 < -obj.params.volumeThresholdWarning_m3   && ...
                      obj.volIOtrip_m3 + obj.flowIOtrip_lpm/60000*obj.params.timeWarning_s < -obj.params.volumeThresholdAlarm_m3) ) 
                  obj.alarmState = AlarmState.Warning;%Loss
                  obj.alarmType  = AlarmType.LossWarning;    
            else
                obj.alarmState = AlarmState.Normal;%normal
                obj.alarmType  = AlarmType.Normal;
                obj.volIO_m3   = 0; 
            end            
        end

        function AlarmConditions = setAlarmConditions(obj, processData, algData, badData)
            AlarmConditions = struct();
            AlarmConditions.HighGainVol  = not(badData.Vol ) && +obj.volIOpit_m3  > obj.params.volumeThresholdAlarm_m3 && algData.timeSinceResetGL_s > 300;%Inhbit 5 minutes after volume has been reset
            AlarmConditions.HighGainFlow = not(badData.Flow) && +obj.volIOflow_m3 > obj.params.volumeThresholdAlarm_m3;
            AlarmConditions.HighGainPres = not(badData.Pres) && +obj.volIOpres_m3 > obj.params.volumeThresholdAlarm_m3;
            AlarmConditions.HighLossVol  = not(badData.Vol ) && -obj.volIOpit_m3  > obj.params.volumeThresholdAlarm_m3 && algData.timeSinceResetGL_s > 300;%Inhbit 5 minutes after volume has been reset
            AlarmConditions.HighLossFlow = not(badData.Flow) && -obj.volIOflow_m3 > obj.params.volumeThresholdAlarm_m3;
            AlarmConditions.HighLossPres = not(badData.Pres) && -obj.volIOpres_m3 > obj.params.volumeThresholdAlarm_m3;       
            AlarmConditions.inhibit      = algData.numberPumpStops == 0 || (algData.numberPumpStops == 1 && algData.timePumpsOn_s < 10*60) || ...
                (obj.timeSinceLastAlarm_s < obj.params.timeInhibitAlarmAfterReset_s); %Inhibit alarm until 1st pump stop + 10 minutes and 5 minutes after last alarm 
            AlarmConditions.HoldAlarm    = obj.timeAlarmActive_s < obj.params.timeResetAlarm_s; %Hold alarm 5 minutes or until user has silenced it
            AlarmConditions.allowInfluxAlarm = not(algData.isRampingUp);
            AlarmConditions.startup          = (obj.alarmState == AlarmState.Disabled);
            AlarmConditions.SlowGain         = algData.volGainDetector_m3 > obj.volGainDetectorThreshold_m3 && not(algData.isRampingUp);
            AlarmConditions.HighGainVolOnly  = ( obj.volIOpit_m3 > 0     &&  obj.timeHighVolError_s > obj.params.timeVolOnlyAlarm_s) && not(algData.isRampingUp) && algData.timeSinceResetGL_s > 300;
            AlarmConditions.HighLossVolOnly  = (-obj.volIOpit_m3 > 0     &&  obj.timeHighVolError_s > obj.params.timeVolOnlyAlarm_s) && algData.timeSinceResetGL_s > 300;
            AlarmConditions.HighGainFlowOnly = ( obj.flowIOflow_lpm > 0  &&  obj.timeHighFlowError_s > obj.params.timeFlowOnlyAlarm_s ...
                &&  obj.timeRecalibrateFlowOut_s == 0 && processData.flowIn_lpm > obj.flowPumpsOnMin_lpm) && not(algData.isRampingUp);
            AlarmConditions.HighLossFlowOnly = (-obj.flowIOflow_lpm > 0  &&  obj.timeHighFlowError_s > obj.params.timeFlowOnlyAlarm_s ...
                &&  obj.timeRecalibrateFlowOut_s == 0 && processData.flowIn_lpm > obj.flowPumpsOnMin_lpm);                    
        end
        
        function obj = setVolIOAlarm(obj)
            switch (obj.alarmType)
                case {AlarmType.PitSlowGainAlarm, AlarmType.PitOnlyLossAlarm, AlarmType.PitOnlyInfluxAlarm}
                    obj.volIO_m3 = obj.volIOpit_m3;
                case {AlarmType.FlowOnlyLossAlarm, AlarmType.FlowOnlyInfluxAlarm}
                    obj.volIO_m3 = obj.volIOflow_m3;
                case AlarmType.FlowAndPressureLossAlarm
                    obj.volIO_m3 = max(obj.volIOflow_m3, obj.volIOpres_m3);                            
                case AlarmType.FlowAndPressureInfluxAlarm
                    obj.volIO_m3 = min(obj.volIOflow_m3, obj.volIOpres_m3);                            
                case AlarmType.FlowAndPitLossAlarm
                    obj.volIO_m3 = max(obj.volIOflow_m3, obj.volIOpit_m3);                            
                case AlarmType.FlowAndPitInfluxAlarm
                    obj.volIO_m3 = min(obj.volIOflow_m3, obj.volIOpit_m3);                            
                case AlarmType.PitAndPressureLossAlarm
                    obj.volIO_m3 = max(obj.volIOpit_m3, obj.volIOpres_m3);                            
                case AlarmType.PitAndPressureInfluxAlarm
                    obj.volIO_m3 = min(obj.volIOpit_m3, obj.volIOpres_m3);                            
            end
        end
        
        function obj = checkForNewAlarm(obj, AlarmConditions)            
            if (AlarmConditions.HighGainFlow && AlarmConditions.HighGainPres) && AlarmConditions.allowInfluxAlarm  
                obj.alarmState = AlarmState.Alarm;%Influx F+Pr
                obj.alarmType  = AlarmType.FlowAndPressureInfluxAlarm;
            elseif (AlarmConditions.HighGainFlow && AlarmConditions.HighGainVol) && AlarmConditions.allowInfluxAlarm
                obj.alarmState = AlarmState.Alarm;%Influx F+V
                obj.alarmType  = AlarmType.FlowAndPitInfluxAlarm;
            elseif (AlarmConditions.HighGainVol && AlarmConditions.HighGainPres) && AlarmConditions.allowInfluxAlarm
                obj.alarmState = AlarmState.Alarm;%Influx V+Pr
                obj.alarmType  = AlarmType.PitAndPressureInfluxAlarm;
            elseif (AlarmConditions.HighLossFlow && AlarmConditions.HighLossPres)
                obj.alarmState = AlarmState.Alarm;%Loss F+Pr
                obj.alarmType  = AlarmType.FlowAndPressureLossAlarm;
            elseif (AlarmConditions.HighLossFlow && AlarmConditions.HighLossVol)
                obj.alarmState = AlarmState.Alarm;%Loss F+V
                obj.alarmType  = AlarmType.FlowAndPitLossAlarm;
            elseif (AlarmConditions.HighLossVol && AlarmConditions.HighLossPres)
                obj.alarmState = AlarmState.Alarm;%Loss V+Pr
                obj.alarmType  = AlarmType.PitAndPressureLossAlarm;
            elseif AlarmConditions.SlowGain
                obj.alarmState = AlarmState.Alarm;%Influx: pit only alarm
                obj.alarmType  = AlarmType.PitSlowGainAlarm;
            elseif AlarmConditions.HighGainVolOnly
                obj.alarmState = AlarmState.Alarm;%Influx: pit only alarm
                obj.alarmType  = AlarmType.PitOnlyInfluxAlarm;
            elseif AlarmConditions.HighLossVolOnly 
                obj.alarmState = AlarmState.Alarm;%Loss: pit only alarm
                obj.alarmType  = AlarmType.PitOnlyLossAlarm;
            elseif AlarmConditions.HighGainFlowOnly 
                obj.alarmState = AlarmState.Alarm;%Influx: flow only alarm
                obj.alarmType  = AlarmType.FlowOnlyInfluxAlarm;
            elseif AlarmConditions.HighLossFlowOnly 
                obj.alarmState = AlarmState.Alarm;%Loss: flow only alarm
                obj.alarmType  = AlarmType.FlowOnlyLossAlarm;
            end
            
            if (obj.alarmState == AlarmState.Alarm)
                obj = setVolIOAlarm(obj);
            end
        end
        
        function WarningConditions = setWarningConditions(obj)
            WarningConditions = struct();
            %warning when alarm is expected timeWarning_s ahead
            WarningConditions.HighGainVol  =  +obj.volIOpit_m3 > obj.params.volumeThresholdWarning_m3 && ...
                obj.volIOpit_m3  + obj.flowIOpit_lpm/60000 *obj.params.timeWarning_s > obj.params.volumeThresholdAlarm_m3;
            WarningConditions.HighGainFlow = +obj.volIOflow_m3 > obj.params.volumeThresholdWarning_m3 && ...
                obj.volIOflow_m3 + obj.flowIOflow_lpm/60000*obj.params.timeWarning_s > obj.params.volumeThresholdAlarm_m3;
            WarningConditions.HighGainPres = +obj.volIOpres_m3 > obj.params.volumeThresholdWarning_m3 && ...;
                obj.volIOpres_m3 + obj.flowIOpres_lpm/60000*obj.params.timeWarning_s > obj.params.volumeThresholdAlarm_m3;
            WarningConditions.HighGainPitOnly = obj.volIOpit_m3  > obj.params.volumeThresholdWarning_m3   && ...
                  obj.volIOpit_m3  + obj.flowIOpit_lpm/60000 *obj.params.timeWarning_s > obj.params.volIOAlarm_m3;
            WarningConditions.HighGainFlowOnly = obj.volIOflow_m3  > obj.params.volumeThresholdWarning_m3   && ...
                  obj.volIOflow_m3  + obj.flowIOflow_lpm/60000 *obj.params.timeWarning_s > obj.params.volIOAlarm_m3;

            WarningConditions.HighLossVol  =  -obj.volIOpit_m3 > obj.params.volumeThresholdWarning_m3 && ...
                -obj.volIOpit_m3  - obj.flowIOpit_lpm/60000 *obj.params.timeWarning_s > obj.params.volumeThresholdAlarm_m3;
            WarningConditions.HighLossFlow = -obj.volIOflow_m3 > obj.params.volumeThresholdWarning_m3 && ...
                -obj.volIOflow_m3 - obj.flowIOflow_lpm/60000*obj.params.timeWarning_s > obj.params.volumeThresholdAlarm_m3;
            WarningConditions.HighLossPres = -obj.volIOpres_m3 > obj.params.volumeThresholdWarning_m3 && ...;
                -obj.volIOpres_m3 - obj.flowIOpres_lpm/60000*obj.params.timeWarning_s > obj.params.volumeThresholdAlarm_m3;
            WarningConditions.HighLossPitOnly = -obj.volIOpit_m3  > obj.params.volumeThresholdWarning_m3   && ...
                  -obj.volIOpit_m3  - obj.flowIOpit_lpm/60000 *obj.params.timeWarning_s > obj.params.volIOAlarm_m3;
            WarningConditions.HighLossFlowOnly = -obj.volIOflow_m3  > obj.params.volumeThresholdWarning_m3   && ...
                  -obj.volIOflow_m3  - obj.flowIOflow_lpm/60000 *obj.params.timeWarning_s > obj.params.volIOAlarm_m3;
        end
        
        function obj = checkForWarningOrNormal(obj)
            WarningConditions = setWarningConditions(obj);

            if (WarningConditions.HighGainVol && WarningConditions.HighGainPres)
                obj.alarmState = AlarmState.Warning;%Influx pit&pres
                obj.alarmType  = AlarmType.InfluxWarning;
                obj.volIO_m3   = min([obj.volIOpit_m3, obj.volIOpres_m3]);
            elseif (WarningConditions.HighGainFlow && WarningConditions.HighGainVol)
                obj.alarmState = AlarmState.Warning;%Influx flow&pit
                obj.alarmType  = AlarmType.InfluxWarning;
                obj.volIO_m3   = min([obj.volIOpit_m3, obj.volIOpit_m3]);
            elseif (WarningConditions.HighGainFlow && WarningConditions.HighGainPres)
                obj.alarmState = AlarmState.Warning;%Influx flow&pres
                obj.alarmType  = AlarmType.InfluxWarning;
                obj.volIO_m3   = min([obj.volIOflow_m3, obj.volIOpres_m3]);
            elseif WarningConditions.HighGainFlowOnly
                obj.alarmState = AlarmState.Warning;%Influx Flow only
                obj.alarmType  = AlarmType.InfluxWarning;
                obj.volIO_m3   = obj.volIOflow_m3;
            elseif WarningConditions.HighGainPitOnly
                obj.alarmState = AlarmState.Warning;%Influx Pit only
                obj.alarmType  = AlarmType.InfluxWarning;
                obj.volIO_m3   = obj.volIOpit_m3;                        
            elseif (WarningConditions.HighLossVol && WarningConditions.HighLossPres)
                obj.alarmState = AlarmState.Warning;%Influx pit&pres
                obj.alarmType  = AlarmType.LossWarning;
                obj.volIO_m3   = max([obj.volIOpit_m3, obj.volIOpres_m3]);
            elseif (WarningConditions.HighLossFlow && WarningConditions.HighLossVol)
                obj.alarmState = AlarmState.Warning;%Influx pit&pres
                obj.alarmType  = AlarmType.LossWarning;
                obj.volIO_m3   = max([obj.volIOpit_m3, obj.volIOflow_m3]);
            elseif (WarningConditions.HighLossFlow && WarningConditions.HighLossPres)
                obj.alarmState = AlarmState.Warning;%Loss flow & pres
                obj.alarmType  = AlarmType.LossWarning;
                obj.volIO_m3   = max([obj.volIOflow_m3, obj.volIOpres_m3]);
            elseif WarningConditions.HighLossFlowOnly
                obj.alarmState = AlarmState.Warning;%Loss Flow only
                obj.alarmType  = AlarmType.LossWarning;
                obj.volIO_m3   = obj.volIOflow_m3;
            elseif WarningConditions.HighLossPitOnly
                obj.alarmState = AlarmState.Warning;%Loss pit only
                obj.alarmType  = AlarmType.LossWarning;
                obj.volIO_m3   = obj.volIOpit_m3;
            else
                obj.alarmState = AlarmState.Normal;%normal
                obj.alarmType  = AlarmType.Normal;
                vol            = sort([obj.volIOflow_m3, obj.volIOpit_m3, obj.volIOpres_m3]); 
                obj.volIO_m3   = vol(2);%Use the median            
            end
        end
        
        function obj = checkRecalibrateFlowOut(obj, AlarmConditions)
            
            if not(obj.alarmState == AlarmState.Alarm) && ((AlarmConditions.HighGainFlow && not(AlarmConditions.HighGainVol)) || ...
                                                       (AlarmConditions.HighLossFlow && not(AlarmConditions.HighLossVol))) && ...
                                                       obj.timeHighFlowError_s == 0
                if (obj.timeRecalibrateFlowOut_s == 0) && obj.dispDebug
                    disp('Recalibrate flow out measurement next 60 sec as neither volume nor pressure indicate loss or influx');
                end
                obj.timeRecalibrateFlowOut_s     = obj.timeRecalibrateFlowOut_s + obj.dt_s;
                if obj.timeRecalibrateFlowOut_s  < obj.params.timeWaitRecalibratePaddle_s  %Wait 1 min in case of real loss
                    obj.recalibrateFlowOutNow    = false;
                elseif obj.timeRecalibrateFlowOut_s  < obj.params.timeWaitRecalibratePaddle_s + 60
                    obj.recalibrateFlowOutNow    = true;
                else
                    obj.timeRecalibrateFlowOut_s = 0;
                    obj.recalibrateFlowOutNow    = false;                    
                    obj.volIOflow_m3             = 0;
                end
            else
                obj.timeRecalibrateFlowOut_s     = 0;
                obj.recalibrateFlowOutNow        = false;                    
            end
        end
        
        function obj = checkRecalibratePresSP(obj, AlarmConditions)
            if not(obj.alarmState == AlarmState.Alarm) && (AlarmConditions.HighGainPres || AlarmConditions.HighLossPres)
               obj.timeRecalibratePresSP_s     = obj.timeRecalibratePresSP_s + obj.dt_s;
               if obj.timeRecalibratePresSP_s  < obj.params.timeWaitRecalibratePaddle_s  %Wait 1 min in case of real loss
                   obj.recalibratePresSPNow    = false;
               elseif obj.timeRecalibratePresSP_s  < obj.params.timeWaitRecalibratePaddle_s + 60
                   obj.recalibratePresSPNow    = true;
               else
                   if obj.dispDebug
                       disp('Reset volIOpres as neither volume nor flow out indicate loss or influx');
                   end
                   obj.timeRecalibratePresSP_s = 0;
                   obj.recalibratePresSPNow    = false;                    
                   obj.volIOpres_m3            = 0;
               end
            else
                obj.timeRecalibratePresSP_s     = 0;
                obj.recalibratePresSPNow        = false;                    
            end                
         end        
        
        function obj = setAlarmStateDrilling(obj, processData, algData, badData)
            %1. Check for alarms
            AlarmConditions = setAlarmConditions(obj, processData, algData, badData);
            if obj.alarmState == AlarmState.Alarm && AlarmConditions.HoldAlarm %Old alarm
                obj.alarmState = AlarmState.Alarm;%Do nothing                
                return;
            elseif AlarmConditions.startup || AlarmConditions.inhibit    
                obj.alarmState = AlarmState.Normal;
                obj.alarmType  = AlarmType.Normal;
                return;
            elseif not(obj.alarmState == AlarmState.Alarm) 
                obj = checkForNewAlarm(obj, AlarmConditions);
            end
            
            %2. Check for warnings or normal
            if not(obj.alarmState == AlarmState.Alarm) 
                obj = checkForWarningOrNormal(obj);
            end
                        
            obj = checkRecalibrateFlowOut(obj, AlarmConditions);
            obj = checkRecalibratePresSP(obj, AlarmConditions);
        end
        
        function obj = updateTimers(obj)
            if obj.alarmState == AlarmState.Alarm
                obj.timeAlarmActive_s    = obj.timeAlarmActive_s + obj.dt_s;
                obj.timeSinceLastAlarm_s = 0;
            else
                obj.timeSinceLastAlarm_s = obj.timeSinceLastAlarm_s  + obj.dt_s;
                obj.timeAlarmActive_s = 0;
            end
            obj.runSoftresetnow      = false;
            if obj.timeSinceValid_s == -0.99925
                %First
                if not(obj.alarmState == AlarmState.Disabled || obj.alarmState == AlarmState.InputError)
                    obj.timeSinceValid_s = 0;
                end
            elseif obj.timeSinceValid_s == 0
                %Was valid
                if (obj.alarmState == AlarmState.Disabled || obj.alarmState == AlarmState.InputError)
                    obj.timeSinceValid_s = obj.dt_s;
                end
            else
                %Was not valid
                if (obj.alarmState == AlarmState.Disabled || obj.alarmState == AlarmState.InputError)
                    obj.timeSinceValid_s = obj.timeSinceValid_s + obj.dt_s;
                else
                    if obj.timeSinceValid_s > 6*60*60
                        obj.runSoftresetnow = true;
                    end
                    obj.timeSinceValid_s = 0;
                end        
            end
        end

        %determine if an alarm has occured
        function obj = setAlarmState(obj, algData, processData, sysState)
            %Reset alarm to normal after 5 minutes
            obj = checkResetAlarm(obj);
            %Check if models have been calibrated
            badCalibration = obj.checkCalibration(algData, sysState);

            if badCalibration.Tot
                obj.alarmState = AlarmState.InputError;
                obj.alarmType  = AlarmType.Normal;
                obj.volIO_m3   = 0; 
            elseif not(obj.resetAlarm)
                if not(sysState.isOnBottom)%Tripping mode. Currently tripping mode is defined as not on bottom and not bad data
                    obj = setAlarmStateTripping(obj);                                
                elseif sysState.isOnBottom %true 
                    obj = setAlarmStateDrilling(obj, processData, algData, badCalibration);                
                else
                    obj.alarmState = AlarmState.Normal;
                    obj.alarmType  = AlarmType.Normal;
                end
            end
            obj = updateTimers(obj);
        end
        
        function  [obj, result ] = returnResultStruct(obj, varargin)% 
            result.flowIOflow_lpm   = obj.flowIOflow_lpm;
            result.flowIOpres_lpm   = obj.flowIOpres_lpm;
            result.flowIOpit_lpm    = obj.flowIOpit_lpm;
            result.flowIOtrip_lpm   = obj.flowIOtrip_lpm;
            result.presIO_bar       = obj.presIO_bar;
            result.volIOflow_m3     = obj.volIOflow_m3;
            result.volIOpres_m3     = obj.volIOpres_m3;
            result.volIOpit_m3      = obj.volIOpit_m3;
            result.volIOtrip_m3     = obj.volIOtrip_m3;
            result.flowIO_lpm       = obj.flowIOflow_lpm;
            result.volIO_m3         = obj.volIO_m3;
            result.alarmState       = int32(obj.alarmState);            
            result.alarmType        = int32(obj.alarmType);            
            result.flowIOinj_lpm    = obj.flowIOinj_lpm;
            result.recalibrateFlowOutNow = obj.recalibrateFlowOutNow;   
            result.timeSinceLastAlarm_s  = obj.timeSinceLastAlarm_s;
            result.timeSinceValid_s      = obj.timeSinceValid_s;            
            result.timeAlarmActive_s     = obj.timeAlarmActive_s; 
            result.timeHighFlowError_s   = obj.timeHighFlowError_s;
            result.timeHighVolError_s    = obj.timeHighVolError_s;
        end
        
  end
     
  methods (Static)
      function badCalibration = checkCalibration(algData, sysState)
          badCalibration      = struct();
          badCalibration.Vol  = not(algData.isCalibratedExpPitVol);
          badCalibration.Flow = not(algData.isCalibratedExpFlowOut && algData.isCalibratedMeasFlowOut);
          badCalibration.Pres = not(algData.isCalibratedExpPresSP);
          if sysState.isOnBottom
              badCalibration.Tot = (badCalibration.Vol + badCalibration.Flow + badCalibration.Pres > 1);
          else
              badCalibration.Tot = false;%badTripTank;
          end
      end
  end
end

