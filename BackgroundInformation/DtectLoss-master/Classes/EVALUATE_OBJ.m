classdef EVALUATE_OBJ < handle
    %   EVALUATE_OBJ Summary of this class goes here
    %   Read detection data from algorithm
    %   Calculate number of alarms. 
    %   Compare with true state if available: False alarm rate (FAR),
    %   Probability of detection (PD), Time-to-detect (TD),
    %   Volume-at-detection (VD), algorithm up-time (algUptime).
    
    properties (SetAccess = private)
        trueNoOfAlarms
        trueNoOfWarnings
        algNoOfCorrectAlarms
        algNoOfWarnings
        algNoOfFalseAlarms
        algLateAlarms % alarms issued either too late, or after correctly issuing an alarm
        algName
        rigName
        FAR % [false alarms per 12 h] false alarm rate
        PD % [-] probability of detection
        kpi% [-] key performance index for algorithm
        TD % [s] time to detection, an average if multiple events
        VD % [m3] mean absolute volume of actual influx at time of detection. 
        durationInHours % [hours] duration of simulation
        disabledTime % [days] time the algorithm is disabled
        algUpTime % [%]  how much of the time the algorithm is not disabled.
        algUpTimeInHours % [hours]  how much of the time the algorithm is not disabled.
        resultTxt % one-line of text that summarized the evaulation
    end
    
    properties (Constant)
        volThresWarning_m3 = 0.8;
        volThresAlarm_m3 = 1;
        detectionGracePeriod_s = 630; % [s] grace period for computing time of detection
    end
    
    methods
        function obj=EVALUATE_OBJ(algName, rigName)
            obj.algName=algName;
            obj.rigName=rigName;
            obj.trueNoOfAlarms=[];
            obj.trueNoOfWarnings=[];
            obj.algNoOfCorrectAlarms=[];
            obj.algNoOfWarnings=[];
            obj.algNoOfFalseAlarms=[];
            obj.algLateAlarms=[];
            obj.FAR=[];
            obj.PD=[];
            obj.kpi=[];
            obj.TD=[];
            obj.VD=[];
            obj.durationInHours=[];
            obj.disabledTime=[];
            obj.algUpTime=[];
            obj.algUpTimeInHours=[];
            obj.resultTxt=[];
        end
        
        function obj = initialize(obj)
            obj.trueNoOfAlarms=0;
            obj.trueNoOfWarnings=0;
            obj.algNoOfCorrectAlarms=0;
            obj.algNoOfWarnings=0;
            obj.algNoOfFalseAlarms=0;
            obj.algLateAlarms=0;
            obj.FAR=0;
            obj.PD=NaN;
            obj.kpi=NaN;
            obj.TD=NaN;
            obj.VD=NaN;
            obj.durationInHours=NaN;
            obj.disabledTime=0;
            obj.algUpTime=NaN;
            obj.algUpTimeInHours=NaN;
        end
        
        function [obj, nFalseAlarms, alarmTypes] = evaluate(obj, trueIgnore, trueVolIO,...
                algFlowIO, algVolIO, algAlarmState, algAlarmType)
            % The inputs are structs with time and value, e.g. trueIgnore.t
            % is the time and trueIgnore.V is the value.
            % trueIgnore [boolean]: flag saying if alarms issued during
            % this time-frame should be ignored, i.e. when trueIgnore.V == 1
            % trueVolIO [m3]  : actual accumulated volume to/from
            % reservoir, can simply be zero.
            % algFlowIO [lpm] : flow rate to/from reservoir computed by
            % algorithm
            % algVolIO [m3]   : accumulated volume to/from reservoir
            % computed by algorithm
            % alarmState [-]  : Alarm produced by algorithm
            
            % This function plays through all data and checks for alarms
            % and false alarms.
            
            % return with empty alarm count variables if called without
            % algAlarmType
            if nargin < 7
                algAlarmType = [];
            end

            trueAlarmStruct = obj.generateTrueAlarmStruct(trueIgnore, trueVolIO);
            [nFalseAlarms, alarmTypes]=obj.groupAlarms(algAlarmType, trueAlarmStruct);
            [obj, trueAlarmStruct] = obj.countIssuedAlarms(algFlowIO, algVolIO, algAlarmState, trueAlarmStruct);
            obj = obj.calculateTimeToDetection(trueAlarmStruct, trueVolIO);
            
            obj = obj.calcKeyPerformanceIndicies(algAlarmState.T(1), algAlarmState.T(end));
            
            obj = obj.makeResultText;
        end
        
        function trueAlarmStruct = generateTrueAlarmStruct(obj, trueIgnore, trueVolIO)
            trueAlarmStruct = struct('alarmTime', [],...
                'startEvent', [], 'stopEvent', [],...
                'ignore', false,...
                'detectionTime', [], 'volumeAtDetection', []);
            
            if ~isstruct(trueIgnore) || ~isstruct(trueVolIO)
                return
            end
            
            if isempty(fieldnames(trueIgnore)) && ~isempty(fieldnames(trueVolIO))
                trueIgnore.t = [trueVolIO.t(1); trueVolIO.t(end)];
                trueIgnore.V = [0; 0];
            elseif ~isempty(fieldnames(trueIgnore)) && isempty(fieldnames(trueVolIO))
                trueVolIO.t = [trueIgnore.t(1); trueIgnore.t(end)];
                trueVolIO.V = [0; 0];
            elseif  isempty(fieldnames(trueIgnore)) && isempty(fieldnames(trueVolIO))
                return
            end
            
            prevTrueAlarmState = AlarmState.Normal;
            
            trueNT = [numel(trueIgnore.t) numel(trueVolIO.t)]; % number of values
            trueIdx = ones(1,2); % index in use for [ trueIgnore trueVolIO ]
            trueCurrentT = [trueIgnore.t(1) trueVolIO.t(1)]; % time
            trueNextPossibleT = [trueIgnore.t(2) trueVolIO.t(2)]; % time
            trueV = [trueIgnore.V(1) trueVolIO.V(1)]; % value
            
            trueIdxNotAtEnd = trueIdx ~= trueNT; % logical array to determine if all columns has been played through.
            
            ignoreEvent = false;
            activeEvent = false;
            eventCounter = 0;
            
            % loop until all data has been played through
            while any(trueIdxNotAtEnd)
                
                % determine from true data if there is an alarm
                if ~ignoreEvent
                    if any(trueV) && ~activeEvent
                        eventCounter = eventCounter + 1;
                        if trueV(1)
                            trueAlarmStruct(eventCounter).ignore = true;
                            ignoreEvent = true;
                            trueAlarmStruct(eventCounter).startEvent = trueCurrentT(1);
                        else
                            trueAlarmStruct(eventCounter).ignore = false;
                            activeEvent = true;
                            trueAlarmStruct(eventCounter).startEvent = trueCurrentT(2);
                        end
                    elseif abs(trueV(2)) >= obj.volThresAlarm_m3 && (prevTrueAlarmState == AlarmState.Normal || ...
                            prevTrueAlarmState == AlarmState.Warning)
                        % alarm
                        prevTrueAlarmState = AlarmState.Alarm;
                        obj.trueNoOfAlarms = obj.trueNoOfAlarms + 1;
                        
                        trueAlarmStruct(eventCounter).alarmTime = trueCurrentT(2);
                    elseif trueV(2) == 0 && activeEvent
                        trueAlarmStruct(eventCounter).stopEvent = trueCurrentT(2);
                        activeEvent = false;
                    elseif abs(trueV(2)) < obj.volThresWarning_m3
                        % no alarm
                        prevTrueAlarmState = AlarmState.Normal;
                    elseif abs(trueV(2)) >= obj.volThresWarning_m3 && prevTrueAlarmState == AlarmState.Normal
                        % warning
                        prevTrueAlarmState = AlarmState.Warning;
                        obj.trueNoOfWarnings = obj.trueNoOfWarnings + 1;
                    end
                else
                    if ~trueV(1)
                        trueAlarmStruct(eventCounter).stopEvent = trueCurrentT(1);
                        ignoreEvent = false;
                    end
                end
            
                
                % determine which of the rows which are at the end.
                trueIdxNotAtEnd = trueIdx ~= trueNT;
                % step forward
                [trueNewT, trueNewIdx] = obj.step(trueNextPossibleT(trueIdxNotAtEnd),trueCurrentT(trueIdxNotAtEnd),trueIdx(trueIdxNotAtEnd));
                
                % make sure that the indicies not already at the end is
                % less than the available indicies.
                trueIdx(trueIdxNotAtEnd) = min([trueNewIdx; trueNT(trueIdxNotAtEnd)]);
                % only update current time with those not at the end
                trueCurrentT(trueIdxNotAtEnd) = trueNewT;
                
                % update value
                trueV = [trueIgnore.V(trueIdx(1)) trueVolIO.V(trueIdx(2))];
                
                % make sure that the next possible time is updated only if
                % necessary.
                if trueIdx(1)<trueNT(1)
                    trueNextPossibleT(1) = trueIgnore.t(trueIdx(1)+1);
                end
                if trueIdx(2)<trueNT(2)
                    trueNextPossibleT(2) = trueVolIO.t(trueIdx(2)+1);
                end
            end
        end
        
        function [obj, trueAlarmStruct] = countIssuedAlarms(obj, algFlowIO, algVolIO, algAlarmState, trueAlarmStruct)
            % algorithm values
            nT=[numel(algFlowIO.T) numel(algVolIO.T) numel(algAlarmState.T)]; % number of values
            idx=ones(1,3); % index in use for [ algFlowIO algVolIO algAlarmState ]
            currentT=[algFlowIO.T(1) algVolIO.T(1) algAlarmState.T(1)]; % time
            nextPossibleT=[algFlowIO.T(2) algVolIO.T(2) algAlarmState.T(2)]; % time
            v=[algFlowIO.V(1) algVolIO.V(1) algAlarmState.V(1)]; % value
            
            idxNotAtEnd=idx~=nT; % logical array to determine if all columns has been played through.
            
            prevAlgAlarmState=v(3); % prev state of the algorithm being evaluated.
            
            disableAlgStart=currentT(3);
            
            trueDataIsZero_bool = obj.trueDataIsZero(trueAlarmStruct);
            
            while any(idxNotAtEnd)
                
                % compare true data with field data and count alarms
                if v(3)==AlarmState.Normal
                    % no alarm from algorithm
                    if prevAlgAlarmState==AlarmState.Disabled
                        % count the time the algorithm has been disabled.
                        % note that the alarm state must pass through
                        % Normal for the time to be recorded.
                        obj.disabledTime=obj.disabledTime+currentT(3)-disableAlgStart;
                    end
                    prevAlgAlarmState=AlarmState.Normal;
                elseif v(3)==AlarmState.Alarm && trueDataIsZero_bool...
                        && prevAlgAlarmState~=AlarmState.Alarm
                    % false alarm from algorithm
                    obj.algNoOfFalseAlarms=obj.algNoOfFalseAlarms+1;
                    prevAlgAlarmState=AlarmState.Alarm;
                elseif v(3)==AlarmState.Alarm && ~trueDataIsZero_bool...
                        && (prevAlgAlarmState==AlarmState.Normal || prevAlgAlarmState==AlarmState.Warning)
                    % possible false alarm from algorithm. Need to check if
                    % it is issued within the grace period of an actual
                    % alarm
                    falseAlarm=true;
                    for ii=1:numel(trueAlarmStruct)
                        if currentT(3) >= trueAlarmStruct(ii).startEvent ...
                                && currentT(3) < trueAlarmStruct(ii).stopEvent
                            if ~trueAlarmStruct(ii).ignore
                                if isempty(trueAlarmStruct(ii).detectionTime)
                                    trueAlarmStruct(ii).detectionTime=currentT(3);
                                else
                                    obj.algLateAlarms=obj.algLateAlarms+1;
                                end
                            end
                            falseAlarm=false;
                        end
                    end
                    if falseAlarm
                        obj.algNoOfFalseAlarms=obj.algNoOfFalseAlarms+1;
                    end
                    prevAlgAlarmState=AlarmState.Alarm;
                elseif v(3)==AlarmState.Warning && prevAlgAlarmState~=AlarmState.Warning
                    % correct or incorrect warning from algorithm
                    obj.algNoOfWarnings=obj.algNoOfWarnings+1;
                    prevAlgAlarmState=AlarmState.Warning;
                elseif v(3)==AlarmState.Disabled && prevAlgAlarmState~=AlarmState.Disabled
                    % the algorithm is disabled
                    disableAlgStart=currentT(3);
                    prevAlgAlarmState=AlarmState.Disabled;
                end
                
                % determine which of the rows which are at the end.
                idxNotAtEnd=idx~=nT;
                % step forward
                [newT, newIdx]=obj.step(nextPossibleT(idxNotAtEnd),currentT(idxNotAtEnd),idx(idxNotAtEnd));
                
                % make sure that the indicies not already at the end is
                % less than the available indicies.
                idx(idxNotAtEnd)=min([newIdx;nT(idxNotAtEnd)]);
                % only update current time with those not at the end
                currentT(idxNotAtEnd)=newT;
                
                % update value vector depending on true data is 0 or a
                % struct.
                v=[algFlowIO.V(idx(1)) algVolIO.V(idx(2)) algAlarmState.V(idx(3))]; % value
                
                % make sure that the next possible time is updated only if
                % necessary.
                if idx(1)<nT(1)
                    nextPossibleT(1)=algFlowIO.T(idx(1)+1);
                end
                if idx(2)<nT(2)
                    nextPossibleT(2)=algVolIO.T(idx(2)+1);
                end
                if idx(3)<nT(3)
                    nextPossibleT(3)=algVolIO.T(idx(3)+1);
                end
            end
            
            if v(3)==AlarmState.Disabled
                obj.disabledTime=obj.disabledTime+currentT(3)-disableAlgStart;
            end
        end
        
        function obj = calculateTimeToDetection(obj, trueAlarmStruct, trueVolIO)
            if obj.trueDataIsZero(trueAlarmStruct)
                return
            end
            % time to detection
            TDVec=-999*ones(size(trueAlarmStruct));
            VDVec=-999*ones(size(trueAlarmStruct));
            for ii=1:max(size(trueAlarmStruct))
                if ~isempty(trueAlarmStruct(ii).detectionTime) && ~isempty(trueAlarmStruct(ii).alarmTime)
                    TDVec(ii)=(trueAlarmStruct(ii).detectionTime-trueAlarmStruct(ii).startEvent)*86400;
                    % only count alarm if it is given within the grace
                    % period
                    if TDVec(ii)<=obj.detectionGracePeriod_s
                        obj.algNoOfCorrectAlarms=obj.algNoOfCorrectAlarms+1;
                        idxDet=find(trueVolIO.t>=trueAlarmStruct(ii).detectionTime,1,'first');
                        if ~isempty(idxDet)
                            VDVec(ii)=trueVolIO.V(idxDet);
                        end
                    end
                    
                end
            end
            obj.TD=mean(TDVec(TDVec>-999 & TDVec<=obj.detectionGracePeriod_s));
            % volume at detection
            obj.VD=mean(VDVec(VDVec>-999));
        end
        
        function [nAlarms, nWarnings]=countTrueAlarms(obj,trueVolIO)
            % Count number of alarms and warnings in the true data set,
            % which depends on the flow rate threshold and volume threshold.
            
            nAlarms=0;
            nWarnings=0;
            
            prevTrueAlarmState=AlarmState.Normal; % assume normal state initially
            for ii=2:numel(trueVolIO.V)
                if abs(trueVolIO.V(ii))<obj.volThresWarning_m3
                    prevTrueAlarmState=AlarmState.Normal;
                elseif abs(trueVolIO.V(ii))>=obj.volThresWarning_m3 && prevTrueAlarmState==AlarmState.Normal
                    prevTrueAlarmState=AlarmState.Warning;
                    nWarnings=nWarnings+1;
                elseif abs(trueVolIO.V(ii))>=obj.volThresAlarm_m3 && (prevTrueAlarmState==AlarmState.Normal || ...
                         prevTrueAlarmState==AlarmState.Warning)
                     prevTrueAlarmState=AlarmState.Alarm;
                    nAlarms=nAlarms+1;
                end
            end
            
        end
        
        function obj = calcKeyPerformanceIndicies(obj, timeStart_d, timeEnd_d)
            % Long format to support required resolution when
            % calculating duration and FAR
            format long
            
            % calculate probability of detection
            if obj.trueNoOfAlarms ~= 0
                obj.PD = min(100, 100 * obj.algNoOfCorrectAlarms / obj.trueNoOfAlarms);
            end
            
            % calculate a KPI which is a sum of false alarms per hour and
            % probability of missing a true event. The lower the KPI, the
            % better.
            if ~isnan(obj.PD)
                probMissingTrueAlarm = (100 - obj.PD) / 100; % [-] probability of missing a true event
            else
                probMissingTrueAlarm = 0;
            end
            
            % false alarm rate in alarms per 12 hours
            obj.FAR = obj.algNoOfFalseAlarms / (timeEnd_d - timeStart_d) / 2;
            
            % kpi = |false alarms per hour| + |probability of missing a true alarm|
            obj.kpi = norm([obj.FAR/12 probMissingTrueAlarm], 1);
            
            obj.durationInHours = (timeEnd_d - timeStart_d) * 24;
            obj.algUpTime = 100 - obj.disabledTime / (timeEnd_d - timeStart_d) * 100;
            obj.algUpTimeInHours = (timeEnd_d - timeStart_d - obj.disabledTime) * 24;
        end
        
        function obj = makeResultText(obj)
            if obj.trueNoOfAlarms~=0
                obj.resultTxt = sprintf(['FAR: %.2f alarms/(12 h), PD: %.1f prc, TD: %.1f s, '...
                    'VD: %.3f m3, Up-time: %.2f prc, KPI: %.2f -, # late alarms: %g\n'],...
                    obj.FAR, obj.PD, obj.TD, obj.VD, obj.algUpTime, obj.kpi, obj.algLateAlarms);
            else
                obj.resultTxt = sprintf('FAR: %.2f alarms/(12 h), Up-time: %.2f prc, KPI: %.2f -\n',...
                    obj.FAR, obj.algUpTime, obj.kpi);
            end
            % disp info
            fprintf(1, obj.resultTxt);
        end
        
        function resultStruct = generateOutput(obj)
            resultStruct = struct(...
                'trueNoOfAlarms', obj.trueNoOfAlarms,...
                'trueNoOfWarnings', obj.trueNoOfWarnings,...
                'algNoOfCorrectAlarms', obj.algNoOfCorrectAlarms,...
                'algNoOfWarnings', obj.algNoOfWarnings,...
                'algNoOfFalseAlarms', obj.algNoOfFalseAlarms,...
                'algLateAlarms', obj.algLateAlarms,...
                'algName', obj.algName,...
                'rigName', obj.rigName,...
                'falseAlarmRate', obj.FAR,...
                'probabilityOfDetection', obj.PD,...
                'keyPerformanceIndex', obj.kpi,...
                'timeToDetection', obj.TD,...
                'accumulatedVolumeAtDetection', obj.VD,...
                'durationInHours', obj.durationInHours,...
                'algUpTime', obj.algUpTime,...
                'algUpTimeInHours', obj.algUpTimeInHours);            
        end
        
    end
    
    methods(Static)
        function [trueIgnore, trueVolIO] = qualifyTrueDataAndShiftTime(trueIgnore, trueVolIO, algFlowIO)
            if ~isstruct(trueVolIO)
                if trueVolIO==0 || trueVolIO==0
                    tDiff=0;
                end
            else
                tDiff=trueVolIO.t(1)-algFlowIO.T(1);
            end
            if round(86400*tDiff)~=0
                trueIgnore.t=trueIgnore.t-tDiff;
                trueVolIO.t=trueVolIO.t-tDiff;
                warning('EVALUATE_OBJ:differentStartTimeInTrueFileAndFieldData',...
                    'The start time in the true data set is different from that in the sim/field data');
            end
        end
        
        function [newT, newIdx]=step(possibleNextTime,currentTime,idx)
            % step time
            newT=currentTime;
            newIdx=idx;
            minT=min(possibleNextTime); % find min time
            for ii=1:numel(possibleNextTime)
                if possibleNextTime(ii)<=minT
                    newT(ii)=possibleNextTime(ii);
                    newIdx(ii)=newIdx(ii)+1;
                end
            end    
        end
        
        function [nFalseAlarms, alarmTypes]=groupAlarms(algAlarmType, ioEvent)
            nFalseAlarms=[];
            alarmTypes=[];
            if isempty(algAlarmType)
                return
            end

            if not(isstruct(ioEvent) && isfield(ioEvent, 'startEvent')...
                    && isfield(ioEvent(1), 'stopEvent') && ~isempty(ioEvent(1).startEvent)...
                    && ~isempty(ioEvent(1).stopEvent))
                warning('EVALUATE_OBJ:emptyIoEvent', 'The ioEvent struct is empty')
                ioEvent=[];
            else
                nEvents=size(ioEvent,2);
            end
            
            if isstruct(algAlarmType) && ~isempty(fieldnames(algAlarmType))
                alarmTypes=enumeration('AlarmType'); % types of alarms
                
                nAlarmTypes=size(alarmTypes,1); % number of alarm types
                nFalseAlarms=zeros(nAlarmTypes,1); % vector to store total number of alarms grouped by type
                
                nD=length(algAlarmType.T); % number of data points
                offset=abs(min(int32(alarmTypes))-1);
                
                prevAlarmType=algAlarmType.V(1); % store previous alarm type and compare with current
                nFalseAlarms = incrementNAlarms(nFalseAlarms, 1);
                                                
                % group type of alarms
                ignoreAlarm=false;
                for ii=2:nD
                    % decide to ignore alarms which are issued during an
                    % actual event.
                    if ~isempty(ioEvent)
                        for jj=1:nEvents
                            if algAlarmType.T(ii)<ioEvent(jj).stopEvent && algAlarmType.T(ii)>ioEvent(jj).startEvent
                                ignoreAlarm=true;
                                break
                            else
                                ignoreAlarm=false;
                            end
                        end
                    end
                    
                    if algAlarmType.V(ii)~=prevAlarmType && ~ignoreAlarm
                        nFalseAlarms = incrementNAlarms(nFalseAlarms, ii);
                    end
                    
                    prevAlarmType=algAlarmType.V(ii);
                end
            end
            
            function nFalseAlarms = incrementNAlarms(nFalseAlarms, alarmIndex)
                nFalseAlarms(offset+int32(algAlarmType.V(alarmIndex))) = nFalseAlarms(offset+int32(algAlarmType.V(alarmIndex)))+1;
            end
        end
        
        function [nAlarms, nWarnings]=countAlgAlarms(algAlarmState)
            % Count alarms and warnings based on the alarm state of the algorithm.
            % The input is a struct with time and value.
            % alarmState [-]  : Alarm produced by algorithm
            
            nAlarms=0;
            nWarnings=0;
            % count number of alarms produced by the detection algorithm
            prevAlgAlarmState=algAlarmState.V(1);
            for ii=2:numel(algAlarmState.V)
                if algAlarmState.V(ii)==AlarmState.Normal
                    prevAlgAlarmState=AlarmState.Normal;
                elseif algAlarmState.V(ii)==AlarmState.Warning && prevAlgAlarmState==AlarmState.Normal
                    prevAlgAlarmState=AlarmState.Warning;
                    nWarnings=nWarnings+1;
                elseif algAlarmState.V(ii)==AlarmState.Alarm && (prevAlgAlarmState==AlarmState.Normal || ...
                         prevAlgAlarmState==AlarmState.Warning)
                     prevAlgAlarmState=AlarmState.Alarm;
                    nAlarms=nAlarms+1;
                end
            end
        end
        
        function trueDataIsZero_bool = trueDataIsZero(trueAlarmStruct)
            if isempty(trueAlarmStruct(1).startEvent)
                trueDataIsZero_bool = true;
            else
                trueDataIsZero_bool = false;
            end 
        end
        
        function trueIgnoreStruct = determineIfIgnoreStructIsAvailable(trueData)
            if isstruct(trueData) && isfield(trueData, 'ignore')
                trueIgnoreStruct = trueData.ignore;
            else
                trueIgnoreStruct = struct();
            end
        end
        
        function plotResult(trueIgnore, trueVolIO, algFlowIO, algVolIO, algAlarmState)
            figure;
            if ~isstruct(trueIgnore)
                trueIgnore=struct('t', [algFlowIO.T(1) algFlowIO.T(end)],...
                    'V', [0 0]);
                trueVolIO=struct('t', [algVolIO.T(1) algVolIO.T(end)],...
                    'V', [0 0]);
            end
            ax(1)=subplot(311);
            plot(algFlowIO.T, algFlowIO.V, trueIgnore.t, trueIgnore.V*1000, 'x');
            title('Ignore alarm and Flow-rate')
            legend('algFlowIO', 'trueIgnore');
            ylabel('lpm');
            datetick('x');
            ax(2)=subplot(312);
            plot(trueVolIO.t, trueVolIO.V, algVolIO.T, algVolIO.V);
            title('Volume')
            legend('trueVolIO', 'algVolIO');
            ylabel('m3');
            datetick('x');
            ax(3)=subplot(313);
            plot(algAlarmState.T, algAlarmState.V);
            title('Algorithm state');
            datetick('x');
            zoom on;
            
            set(ax,'XGrid', 'on', 'YGrid', 'on');
            axis(ax,'tight')
            linkaxes(ax,'x');
        end
            
    end
end
