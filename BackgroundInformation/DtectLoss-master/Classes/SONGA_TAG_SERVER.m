classdef SONGA_TAG_SERVER
    %SONGA_TAG_SERVER emulates the tag server on the Songa rigs
    %   Detailed explanation goes here
    
    properties
        songaMeasTagsWithUnits
        songaValidTagsWithUnits
        songaSelectorTagsWithUnits
        nameSpace
    end
    
    properties (Constant)
        
        songaMeasTags = {...
            'volGainLoss',...
            'volPitDrill',...
            'depthBit',...
            'rpm',...
            'velBlock',...
            'depthHole',...
            'flowIn',...
            'flowOut',...
            'flowOut1',...
            'rop',...
            'presSP',...
            'presSP1',...
            'volTripTank',...
            };
        songaMeasUnits = {...
            'm3', 'm3', 'm', 'rpm', 'mph', 'm',...
            'lpm', 'lpm', 'lpm', 'mph', 'bar', 'bar', 'm3',...
            };
        
        songaValidTags = {...
            'validvolPitDrill',...
            'validdepthBit',...
            'validrpm',...
            'validvelBlock',...
            'validdepthHole',...
            'validflowIn',...
            'validflowOut',...
            'validflowOut1',...
            'validrop',...
            'validpresSP',...
            'validpresSP1',...
            'validvolTripTank',...
            };
        songaValidUnits = {'boolean', 'boolean', 'boolean', 'boolean', 'boolean', 'boolean',...
            'boolean', 'boolean', 'boolean', 'boolean', 'boolean', 'boolean'};
        
        songaSelectorTags = {'selectflowout', 'selectpresSP'};
        songaSelectorUnits = {'boolean', 'boolean'};
    end
    
    methods 
        function [obj, rwdu] = SONGA_TAG_SERVER(rawDataUnits)
            obj.songaMeasTagsWithUnits = concatTagAndUnit(obj.songaMeasTags, obj.songaMeasUnits);
            obj.songaValidTagsWithUnits = concatTagAndUnit(obj.songaValidTags, obj.songaValidUnits);
            obj.songaSelectorTagsWithUnits = concatTagAndUnit(obj.songaSelectorTags, obj.songaSelectorUnits);
            
            obj.nameSpace = cell(0);
            
            rwdu = rawDataUnits;
            
            fN = fieldnames(rawDataUnits);
            
            for jj = 1:numel(obj.songaMeasTags)
                foundTag = false;
                for kk = 1:numel(fN)
                    if strcmp(fN{kk}, obj.songaMeasTags{jj})
                        foundTag = true;
                        continue
                    end
                end
                if ~foundTag
                    rwdu.(obj.songaMeasTags{jj}) = obj.songaMeasUnits{jj};
                end
            end
            
            for mm = 1:numel(obj.songaValidTags)
                rwdu.(obj.songaValidTags{mm}) = obj.songaValidUnits{mm};
            end
            
            for nn = 1:numel(obj.songaSelectorTags)
                rwdu.(obj.songaSelectorTags{nn}) = obj.songaSelectorUnits{nn};
            end
            
            function tagAndUnit = concatTagAndUnit(tagList, unitList)
                tagAndUnit = cell(size(tagList));
                
                nTags = numel(tagList);
                for ii = 1:nTags
                    tagAndUnit{ii} = strcat(tagList{ii}, '_', unitList{ii});
                end
            end
        end
        
        function rawDataWithUnits = appendMissingData(obj, rawData)
            
            rawDataWithUnits = rawData;
            
            fN = fieldnames(rawData);
            for jj = 1:numel(obj.songaMeasTags)
                foundTag = false;
                for kk = 1:numel(fN)
                    if strcmp(fN{kk}, obj.songaMeasTags{jj})
                        foundTag = true;
                        continue
                    end
                end
                if ~foundTag
                    rawDataWithUnits.(obj.songaMeasTags{jj}).V = 0;
                    rawDataWithUnits.(obj.songaMeasTags{jj}).t = 0;
                end
            end
            
            for mm = 1:numel(obj.songaValidTags)
                rawDataWithUnits.(obj.songaValidTags{mm}).V = 1;
                rawDataWithUnits.(obj.songaValidTags{mm}).t = 0;
            end
            
            for nn = 1:numel(obj.songaSelectorTags)
                rawDataWithUnits.(obj.songaSelectorTags{nn}).V = 0;
                rawDataWithUnits.(obj.songaSelectorTags{nn}).t = 0;
            end
        end
    end
    
end

