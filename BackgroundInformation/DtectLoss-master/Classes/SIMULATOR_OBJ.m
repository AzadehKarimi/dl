classdef SIMULATOR_OBJ
    %
    %   simulator class that sim solves dynamic equations for     
    %   conventional drilling with/without loss
    %     
    properties
        p_p_bar     ; % simulated state: pump pressure.
        q_out_lpm   ; % simulated state: flow out.
        V_fl_m3     ; % simulated state: volume in the flowline
        V_pit_m3    ; % simualted state: volume in the pit 
        q_fl_lpm    ; % simulated (derived) variable: flowline flow
        t           ; % simualtion clock
        buffer      ;
        curBufferIdx            = 0;
    end
    
    properties (SetAccess = private)
        dt_s                ; % sampling time
        solver              ;
        sys_states          ;
        c_dp_bar_per_lpm2   ;
        c_ann_bar_per_lpm   ;
        M_kg_per_m4         ;
        A_dp_m2             ;
        A_ann_m2            ;
        V_m3                ;
        betaDivByV_bar_per_m3;
        K_fl_m3_per_lpm; % the "choke coeffiecient" in the equation V_fl_m3 =K* q_fl_lpm;
        
    end
             
    properties (Constant)
            beta_bar            = 13000;
            holeDepth_m         = 3000;
            deltap_ann_bar      = 10; % annular frictional pressure drop at drilling flow 
            deltap_dp_bar       = 50;  % frictional pressure drop at drillpipe
            
            % "nominal flows and volumes" 
            q_drilling_lpm   = 2000;
            V_fl0_m3            = 5;% inital volume in the flowline during drilling(used to calculate K_fl_m3_per_lpm)
            
            D_ann_m             = 8*0.0254;
            D_dpInner_m         = 5.5*0.0254;
            D_dpOuter_m         = 4.778*0.0254;
            rhoMud_kg_per_m3    = 1.63*1000; % 1.63 sg;
            alpha_fl            = 0.4;
    end
    
    properties (SetAccess = private)
        isCurrentlyBuffering    = 0;
        valuesToBuffer          = 0;
        pressureNoiseAmpl_prc   = 0; 
        flowNoiseAmpl_prc       = 0;
    end
    
    methods
        function obj            = SIMULATOR_OBJ()
        end
        function obj                = initialize(obj,dt_s,solver)
            obj.dt_s                = dt_s;
            obj.solver              = solver;
            obj.sys_states          = SYSTEM_STATES_OBJ();

            obj.t                   = 0;
            obj.isCurrentlyBuffering= 0;
            obj.curBufferIdx        = 0; 
            
            obj.A_dp_m2             = pi*(obj.D_dpInner_m/2)^2;
            obj.A_ann_m2            = pi*(obj.D_ann_m/2)^2 - pi*(obj.D_dpOuter_m/2)^2;
            obj.c_dp_bar_per_lpm2   = obj.deltap_dp_bar./(obj.q_drilling_lpm)^2;  % deltap_dp     = c_dp  * q_p^2
            obj.c_ann_bar_per_lpm   = obj.deltap_ann_bar./(obj.q_drilling_lpm);    % deltap_ann    = c_ann * q_o
            obj.M_kg_per_m4         = obj.holeDepth_m*obj.rhoMud_kg_per_m3.*(1./obj.A_dp_m2+1./obj.A_ann_m2);
            obj.V_m3                = (obj.A_dp_m2 + obj.A_ann_m2)*obj.holeDepth_m ;
            obj.betaDivByV_bar_per_m3 = obj.beta_bar/obj.V_m3;
            obj.K_fl_m3_per_lpm     = obj.q_drilling_lpm/(obj.V_fl0_m3)^obj.alpha_fl;
            
        end
       
        function obj = StartSavingIterations(obj,required_buffer_duration_s)
           obj.isCurrentlyBuffering  = 1;
           obj.valuesToBuffer        = required_buffer_duration_s/obj.dt_s;
           obj.curBufferIdx          = 0;
           obj.buffer.t              = nan(1, obj.valuesToBuffer);
           obj.buffer.p_p_bar        = nan(1, obj.valuesToBuffer);
           obj.buffer.qin_lpm        = nan(1, obj.valuesToBuffer);
           obj.buffer.qloss_lpm      = nan(1, obj.valuesToBuffer);
           obj.buffer.q_fl_lpm       = nan(1, obj.valuesToBuffer);
           obj.buffer.qout_lpm       = nan(1, obj.valuesToBuffer);
           obj.buffer.bitDepth_m     = nan(1, obj.valuesToBuffer);
           obj.buffer.holeDepth_m    = nan(1, obj.valuesToBuffer);
           obj.buffer.VdivbyBeta     = nan(1, obj.valuesToBuffer);
           obj.buffer.V_pit_m3       = nan(1, obj.valuesToBuffer);
           obj.buffer.V_fl_m3        = nan(1, obj.valuesToBuffer);
           obj.buffer.qIo_nodiff_lpm        = nan(1, obj.valuesToBuffer);
           obj.buffer.betaDivByV_bar_per_m3     = nan(1, obj.valuesToBuffer);
           obj.buffer.betaDivByV_e_bar_per_m3   = nan(1, obj.valuesToBuffer);
        end
        
        function obj = stepIt(obj, q_p_lpm, qloss_lpm, bitDepth_m, holeDepth_m)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            lpmTOm3ps         = 1/60000;% conversion from liters per minute to m3 per second
            m3psTOlpm         = 1/lpmTOm3ps;
            barToPa           = 100000;
            
            if (obj.t == 0)%set inital values
                obj.q_out_lpm = 2000;
                obj.p_p_bar   = 60;
                obj.V_fl_m3   = obj.V_fl0_m3;
                obj.V_pit_m3  = 5;
            end
            if (obj.solver == SimulatorSolvers.FORWARD_EULER  ) 
                % simplyfied steadified flow dynamics equation
                % M dotq_o      = p_p - deltap_dp -deltap_ann -delta rho*g*h  = 0 
                % ==>(neglecting cuttings)
                % M dotq_o      = p_p - c_ann*q_out^2 + c_dp*q_in
                fann_bar        = obj.c_ann_bar_per_lpm  * (obj.q_out_lpm);
                fdp_bar         = obj.c_dp_bar_per_lpm2  * (obj.q_out_lpm)^2;
                obj.q_out_lpm   = obj.q_out_lpm + obj.dt_s * (1/obj.M_kg_per_m4)*...
                    ( obj.p_p_bar - fann_bar - fdp_bar )*barToPa*m3psTOlpm;  
                % simplified bulk modulus equation for pressure dynamics
                obj.q_out_lpm  = max(obj.q_out_lpm,0);
                obj.p_p_bar    = obj.p_p_bar  + obj.dt_s * obj.betaDivByV_bar_per_m3 * ...
                    (q_p_lpm + qloss_lpm - obj.q_out_lpm)*lpmTOm3ps;
                obj.p_p_bar    = max(obj.p_p_bar,0);
        
                % flowline flow
                obj.q_fl_lpm    = (max(0.001,obj.V_fl_m3))^obj.alpha_fl*obj.K_fl_m3_per_lpm; 
                % flowline massbalance
                obj.V_fl_m3    = obj.V_fl_m3   + obj.dt_s *( obj.q_out_lpm - obj.q_fl_lpm  ) * lpmTOm3ps ;
                % pit massbalan ce
                obj.V_pit_m3   = obj.V_pit_m3  + obj.dt_s *( obj.q_fl_lpm  - q_p_lpm) * lpmTOm3ps ;
                
            else
                disp('SIMUALTOR_OBJ.m: ERROR solver not supported');
            end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            obj.sys_states      = obj.sys_states.stateMachine(q_p_lpm, bitDepth_m, holeDepth_m,150,1);

            obj.t               = obj.t + obj.dt_s;
            disp(sprintf('t:%.2f',obj.t));
            % buffer results if specified
            if (obj.isCurrentlyBuffering==1)
                obj.curBufferIdx    = obj.curBufferIdx + 1;
                if (obj.curBufferIdx>=obj.valuesToBuffer)
                    obj.isCurrentlyBuffering =0;
                    disp('Simulation finished buffering');
                else    
                    obj.buffer.t(obj.curBufferIdx)              = obj.t; 
                    % states/algebraic simualted variables
                    obj.buffer.p_p_bar (obj.curBufferIdx)       = obj.p_p_bar ;
                    obj.buffer.qout_lpm(obj.curBufferIdx)       = obj.q_out_lpm ;
                    obj.buffer.q_fl_lpm(obj.curBufferIdx)       = obj.q_fl_lpm;
                    obj.buffer.V_pit_m3(obj.curBufferIdx)       = obj.V_pit_m3;
                    obj.buffer.V_fl_m3 (obj.curBufferIdx)       = obj.V_fl_m3;
                    % input
                    obj.buffer.qin_lpm(obj.curBufferIdx)        = q_p_lpm;
                    obj.buffer.qloss_lpm(obj.curBufferIdx)      = qloss_lpm;
                    obj.buffer.bitDepth_m(obj.curBufferIdx)     = bitDepth_m;
                    obj.buffer.holeDepth_m(obj.curBufferIdx)    = holeDepth_m;
                    % param of interest
                    obj.buffer.betaDivByV_bar_per_m3(obj.curBufferIdx)     = obj.betaDivByV_bar_per_m3;
                end
            end
        end
    end
end

