classdef GAIN_DETECTOR_OBJ < DTECTLOSS_SUBMODULE
    
    properties (SetAccess = private)
        volGain_m3
        flowBasedOnDerivativeOfGainLoss_lpm
        ringBufferVolGainLoss
        ringBufferFlowIn
        volGainLossPrev_m3
        volGainLossOffset_m3
        linRegObj
        prevTime_d
    end
    
    properties (Constant)
        volumeLimit_m3 = 1;
        flowRateLim_lpm = 1000;
        volGainNegativeFlowRateReset_lpm = -10;
        flowInThreshold_lpm = 10;
        flowInRangeThreshold_lpm = 100;
        timeStepMax_s = 60;
        nSamples = 600;
        windowForLeastSquares_d = 600/86400;
        windowForFlowRange_d = 600/86400;
        fractionOfWindowNecessaryForLeastSquares = 0.5;
        bitOfBottomThreshold_m = 1.5;
        volGainLossThreshold_m3_per_time_step = 3;
        errVal = -0.99925;
        daysToSec = 86400;
        lpmToM3PerS = 1/60e3;
    end
    
    methods
        function obj = GAIN_DETECTOR_OBJ()
            obj.volGain_m3 = [];
            obj.flowBasedOnDerivativeOfGainLoss_lpm = [];
            obj.ringBufferVolGainLoss = [];
            obj.ringBufferFlowIn = [];
            obj.volGainLossPrev_m3 = [];
            obj.volGainLossOffset_m3 = [];
            obj.linRegObj = [];
            obj.prevTime_d = [];
        end
                
        
        function obj = initialize(obj)
            obj.volGain_m3 = 0;
            obj.flowBasedOnDerivativeOfGainLoss_lpm = 0;
            obj.ringBufferVolGainLoss = RINGBUFFER_OBJ(obj.nSamples);
            obj.ringBufferFlowIn = RINGBUFFER_OBJ(obj.nSamples);
            obj.volGainLossPrev_m3 = 0;
            obj.volGainLossOffset_m3 = 0;
            obj.linRegObj = LINREG_OBJ();
        end
        
        
        function obj = softReset(obj)
            obj.volGain_m3 = 0;
            obj.flowBasedOnDerivativeOfGainLoss_lpm = 0;
            obj.volGainLossPrev_m3 = 0;
            obj.volGainLossOffset_m3 = 0;
        end
        
        
        function obj = resetAfterAlarm(obj)
            obj.volGain_m3 = 0;
            obj = obj.initialize;
        end
        
        
        function obj = step(obj, time_d, volGainLoss_m3, depthBit_m, depthHole_m, flowIn_lpm)
            obj = obj.differentiateVolume(time_d, volGainLoss_m3, depthBit_m, depthHole_m, flowIn_lpm);
            
            if checkIfValidMeasAndDrilling()
                return
            end
            obj = obj.integrateVolume(time_d, obj.flowBasedOnDerivativeOfGainLoss_lpm);
            
            function validMeasAndDrilling = checkIfValidMeasAndDrilling()
                validMeasAndDrilling = (abs(depthHole_m - depthBit_m) > obj.bitOfBottomThreshold_m)...
                    || (depthBit_m == obj.errVal && depthHole_m == obj.errVal)...
                    || ~obj.checkIfMeasIsValid(obj.flowBasedOnDerivativeOfGainLoss_lpm);
            end
        end
                
        
        function obj = differentiateVolume(obj, time_d, volGainLoss_m3, depthBit_m, depthHole_m, flowIn_lpm)
            obj.flowBasedOnDerivativeOfGainLoss_lpm = obj.errVal;
                        
            if isNotValidMeas()
                return
            end
            
            obj.ringBufferFlowIn = obj.ringBufferFlowIn.addDataToCircularBuffer(time_d, flowIn_lpm);
            flowInRange_lpm = obj.ringBufferFlowIn.getValueRange(time_d - obj.windowForFlowRange_d);
            
            if isConnectionOrFlowInOutOfRange()
                return
            end
            
            if obj.checkIfMeasIsValid(volGainLoss_m3)
                [volGainLossWithoutOffset_m3, obj.volGainLossOffset_m3] = ...
                    removeOffset([obj.volGainLossPrev_m3 volGainLoss_m3],...
                    obj.volGainLossOffset_m3, obj.volGainLossThreshold_m3_per_time_step, flowIn_lpm);
                obj.volGainLossPrev_m3 = volGainLoss_m3;
            end
            
            obj.prevTime_d = obj.ringBufferVolGainLoss.getLastTime;
            obj.ringBufferVolGainLoss = obj.ringBufferVolGainLoss.addDataToCircularBuffer(time_d, volGainLossWithoutOffset_m3);
            
            flowRate_m3PerS = leastSquaresSlopeOfGainLossVolume;
            
            obj.flowBasedOnDerivativeOfGainLoss_lpm = flowRateLimiterAndConverter(flowRate_m3PerS);
            
            function isNotValidMeas_bool = isNotValidMeas()
               isNotValidMeas_bool = ~all([obj.checkIfMeasIsValid(volGainLoss_m3) obj.checkIfMeasIsValid(depthBit_m)...
                    obj.checkIfMeasIsValid(depthHole_m) obj.checkIfMeasIsValid(flowIn_lpm)]);
            end
            
            function isConnection_bool = isConnectionOrFlowInOutOfRange()
                isConnection_bool = (abs(depthHole_m - depthBit_m) > obj.bitOfBottomThreshold_m)...
                    || (depthBit_m == obj.errVal && depthHole_m == obj.errVal)...
                    || flowIn_lpm < obj.flowInThreshold_lpm...
                    || flowInRange_lpm > obj.flowInRangeThreshold_lpm;
            end
            
            function flowRate_m3PerS = leastSquaresSlopeOfGainLossVolume()
                flowRate_m3PerS = 0;
                timeStartOfWindow_d = time_d - obj.windowForLeastSquares_d;
                [t, v] = obj.ringBufferVolGainLoss.getWindowOfLastValues(timeStartOfWindow_d);
                tDiff = diff(t - t(1));
                tDiffLessThanTimeStepMax_bool = tDiff < obj.timeStepMax_s;
                tTotalValidTimeSteps_d = sum(tDiff(tDiffLessThanTimeStepMax_bool));
                if tTotalValidTimeSteps_d >= obj.windowForLeastSquares_d*obj.fractionOfWindowNecessaryForLeastSquares
                    tInSecWithStartTimeZero = (t - t(1))*obj.daysToSec;
                    obj.linRegObj = obj.linRegObj.fitData(tInSecWithStartTimeZero', v');
                    if ~isempty(obj.linRegObj.coeffs)
                        flowRate_m3PerS = obj.linRegObj.coeffs(1);
                    end
                end
            end
            
            function limitedFlowRate_lpm = flowRateLimiterAndConverter(flowRate_m3PerS)
                limitedFlowRate_lpm = max(min(obj.flowRateLim_lpm, flowRate_m3PerS / obj.lpmToM3PerS), -obj.flowRateLim_lpm);
            end
        end
        
        
        function obj = integrateVolume(obj, time_d, flowRate_lpm)
            if isempty(obj.prevTime_d)
                return
            elseif obj.prevTime_d == obj.errVal
                return
            elseif flowRate_lpm >= obj.volGainNegativeFlowRateReset_lpm && time_d ~= obj.errVal
                dt = (time_d - obj.prevTime_d)*obj.daysToSec;
                if dt > obj.timeStepMax_s
                    obj.volGain_m3 = 0;
                else
                    obj.volGain_m3 = max(min(obj.volGain_m3 + dt*flowRate_lpm*obj.lpmToM3PerS,...
                        obj.volumeLimit_m3), 0);
                end
            elseif flowRate_lpm < obj.volGainNegativeFlowRateReset_lpm
                obj.volGain_m3 = 0;
            end
        end
        
        
        function isValid = checkIfMeasIsValid(obj, meas)
            if meas == obj.errVal
                isValid = false;
            else
                isValid = true;
            end
        end
        
        
        function gainDetectorOutput = generateOutput(obj)
            gainDetectorOutput = struct('volGainDetector_m3', obj.volGain_m3, ...
                'flowDerGLAvg_lpm', obj.flowBasedOnDerivativeOfGainLoss_lpm);
        end
    end
end
