classdef STATE_MACHINE_OBJ < DTECTLOSS_SUBMODULE
    %STATE_MACHINE_OBJ is a the state machine class.
    % it can be updated with new data, and holds the current and previous
    % drilling states.
    %
    % the drilling states allowed are defined in the enum in DrillingStates.m.
    % for instance:
    % bad data, drilling, tripping, circulating, not circulating (flow check etc), other.
    %
    % Consider adding tripping as a new mode rather than a state.
    
    properties
        currentState;
        previousState;
        isOnBottom;
        isCirculating;
        isConstBitPos;
        % InSlips;
        isRotating;
        isDataValid;
        isPOOH;
        isRIH;
        % ConstFlowIn;
        timePumpsOff_s;
        timePipeNotMoving_s;
    end
    
    properties (Constant)
            flowThreshold_lpm   = 50; % [lpm] if the flow rate from the rig pumps are higher than this value -> circulating
            depthThreshold_m    = 10; % [m] if the bit close than this value to the holeDepth -> onBottom
            % hookloadThreshold=50; % [ton] if the hookload is small, then the drill pipe is in slips
            rpmThreshold        = 50; % [rpm] if the rpm is small, then the drill pipe is not rotating
            velThreshold_mph    = 80; % [m/hr] if the block vel is small, then we are not tripping
            % TimeThresholdTripping=1; %[min]
            % TimeThresholdOnBottom=6; %[min]
            depthBitAndHoleNoise_m = 0.01;
            timeThreshold_s     = 10*60; %6Time before state is changed from drilling to flow check (to avoid flow check mode during connections)
            %                         and from tripping to flow check (to avoid flow check mode during connections)
    end
    
    methods
        function obj = STATE_MACHINE_OBJ()
            obj.currentState        = DrillingStates.Other;
            obj.previousState       = DrillingStates.BadData;
            obj.isDataValid           = false;
            obj.isCirculating         = false;
            obj.isConstBitPos         = false;
            % obj.InSlips = false;
            obj.isRotating            = false;
            obj.isPOOH                  = false;
            obj.isRIH                = false;
            % obj.ConstFlowIn = false;
            obj.timePumpsOff_s      = 0; %Time since pumps were turned off
            obj.timePipeNotMoving_s = 0; %Time since pipe was moved
            
        end
        
        function obj = softReset(obj) %#ok<MANU>
            obj = STATE_MACHINE_OBJ();
        end
        
        function obj = changeState(obj, toState)
            obj.previousState   = obj.currentState;
            obj.currentState    = toState;
        end
        
        function obj = update(obj, washedData, dt_s)
            obj.isDataValid = (washedData.flowIn_lpm >= 0) && (washedData.depthBit_m>=0) && ...
                (washedData.depthHole_m >=0 ) ... %&& (washedData.rpm >= 0) ...
                && (washedData.depthHole_m + obj.depthBitAndHoleNoise_m >= washedData.depthBit_m); %& (hookload>=0)
            
            if obj.isOnBottom
                obj.isOnBottom = not(washedData.depthHole_m - washedData.depthBit_m >= obj.depthThreshold_m);
            else
                obj.isOnBottom = ((washedData.depthHole_m - washedData.depthBit_m < obj.depthThreshold_m) && (washedData.flowIn_lpm > obj.flowThreshold_lpm));
            end            

            if (washedData.flowIn_lpm >= obj.flowThreshold_lpm)
                obj.timePumpsOff_s = 0;
            else
                obj.timePumpsOff_s = obj.timePumpsOff_s + dt_s;
            end
            if (abs(washedData.velBlock_mph) > obj.velThreshold_mph)
                obj.timePipeNotMoving_s = 0;
            else
                obj.timePipeNotMoving_s = obj.timePipeNotMoving_s + dt_s;
            end
            
            obj.isCirculating    = (washedData.flowIn_lpm >= obj.flowThreshold_lpm);
            obj.isConstBitPos    = (abs(washedData.velBlock_mph) < obj.velThreshold_mph);%TODO
            %obj.InSlips = (hookload <= obj.hookloadThreshold);
            obj.isRotating       = (washedData.rpm > obj.rpmThreshold);
            obj.isPOOH           = (not(obj.isConstBitPos) & (washedData.velBlock_mph < 0));%TODO
            obj.isRIH            = (not(obj.isConstBitPos) & (washedData.velBlock_mph > 0));%TODO
            %obj.ConstFlowIn = false;%TODO
            
            if not(obj.isDataValid)
                obj = obj.changeState(DrillingStates.BadData); %
            elseif obj.isOnBottom
                if obj.isCirculating %Drilling or sliding
                    obj = obj.changeState(DrillingStates.Drilling);
                elseif (obj.timePumpsOff_s > obj.timeThreshold_s) %No flow
                    obj = obj.changeState(DrillingStates.FlowCheck);
                else
                    %Do nothing
                end
            else %Off bottom
                if not(obj.isConstBitPos)%tripping
                    obj = obj.changeState(DrillingStates.Tripping);
                elseif (obj.timePipeNotMoving_s > obj.timeThreshold_s) %No tripping
                    if obj.isCirculating %
                        obj = obj.changeState(DrillingStates.Conditioning);
                    else  %No flow
                        obj = obj.changeState(DrillingStates.FlowCheck);
                    end
                else
                    %Do nothing
                end %ConstBitPos
            end %DataValid
        end %stateMachine
    end %methods
end

