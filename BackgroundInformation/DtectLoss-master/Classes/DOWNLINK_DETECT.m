classdef DOWNLINK_DETECT < handle
    % Treat as downlink if pressure drops at least 20 bar with a few seconds,
    % then goes at least 20 bar back up again in a few more seconds, let flag
    % stay high until downlinking is done.
    %
    %
    %
    %
    properties(SetAccess = private)
        isDownlinking;
    end
    
    properties (SetAccess = private, GetAccess = private)
        presSPRingBufferObj;
        flowInRingBufferObj;
        T_prev_d;
        presSp_m_bar_prev;
        timeOfLastPosPresSPChangeAboveThreshold_d;
        timeOfLastNegPresSPChangeAboveThreshold_d;
    end
    
    properties (Constant)
        BUFFER_SIZE_SAMPLES = 10;
        ISDOWNLINKING_THRESHOLD_PRESSURE_CHANGE_BAR = 20;
        ISDOWNLINKING_THRESHOLD_PRESSURE_DERIVATIVE_BAR_PER_SEC = 10/3;
        ISDOWNLINKING_TIME_LIMIT_BETWEEN_UP_AND_DOWN_SPIKES_SEC = 20;
        ISDOWLINKING_RESET_WHEN_DELTAP_IN_BUFFER_BELOW_THRESHOLD = 5;
        FLOWRATE_RAMP_THRESHOLD_LPM = 200;
        PRESSURE_RAMP_THRESHOLD_BAR = 15; 
    end
    
    methods
        function obj    = DOWNLINK_DETECT()
            
            obj.presSPRingBufferObj = RINGBUFFER_OBJ(obj.BUFFER_SIZE_SAMPLES);
            obj.flowInRingBufferObj = RINGBUFFER_OBJ(obj.BUFFER_SIZE_SAMPLES);
            obj.isDownlinking = false;
            obj.presSp_m_bar_prev = 0;
            obj.T_prev_d = 0;
            obj.timeOfLastPosPresSPChangeAboveThreshold_d = 0;
            obj.timeOfLastNegPresSPChangeAboveThreshold_d = 0;
        end
        
        function obj = step(obj, presSp_m_bar, T_d, flowIn_lpm)
            
            isMissingFlowInMeas = false;
            
            if nargin < 4
                flowIn_lpm = 0;
                isMissingFlowInMeas = true;
            end
            
            obj.presSPRingBufferObj = obj.presSPRingBufferObj.addDataToCircularBuffer(T_d, presSp_m_bar);
            obj.flowInRingBufferObj = obj.flowInRingBufferObj.addDataToCircularBuffer(T_d, flowIn_lpm);
            [~, presSP_vec] = obj.presSPRingBufferObj.deringbuffer;
            [~, flowIn_vec] = obj.flowInRingBufferObj.deringbuffer;
            
            if  obj.isDownlinking == false
                if ~isMissingFlowInMeas
                    [isFlowInIncreasing, isFlowInDecreasing] = obj.checkIfChanging(flowIn_vec, obj.FLOWRATE_RAMP_THRESHOLD_LPM);
                    [isPresSPIncreasing, isPresSPDecreasing] = obj.checkIfChanging(presSP_vec, obj.PRESSURE_RAMP_THRESHOLD_BAR);
                    
                    isPumpProbablyRamping = (isFlowInIncreasing && isPresSPIncreasing) ...
                        || (isFlowInDecreasing && isPresSPDecreasing);
                else
                    isPumpProbablyRamping = false;
                end
                
                if isPumpProbablyRamping || max(presSP_vec) < 50
                    return
                end
                
                if T_d ~= obj.T_prev_d
                    dT_s = (T_d - obj.T_prev_d)*86400;
                    deltaP_deltaT = (presSp_m_bar - obj.presSp_m_bar_prev)/dT_s;
                else
                    deltaP_deltaT = 0;
                end
                
                if max(presSP_vec) - min(presSP_vec) > obj.ISDOWNLINKING_THRESHOLD_PRESSURE_CHANGE_BAR
                    pressureAbsChangeAboveThreshold = 1;
                else
                    pressureAbsChangeAboveThreshold = 0;
                end
                
                if deltaP_deltaT > obj.ISDOWNLINKING_THRESHOLD_PRESSURE_DERIVATIVE_BAR_PER_SEC
                    obj.timeOfLastPosPresSPChangeAboveThreshold_d = T_d;
                end
                
                if deltaP_deltaT < -obj.ISDOWNLINKING_THRESHOLD_PRESSURE_DERIVATIVE_BAR_PER_SEC
                    obj.timeOfLastNegPresSPChangeAboveThreshold_d = T_d;
                end
                
                timeBtwUpAndDownSPikes_s = (max(obj.timeOfLastPosPresSPChangeAboveThreshold_d, obj.timeOfLastNegPresSPChangeAboveThreshold_d)- ...
                    min(obj.timeOfLastPosPresSPChangeAboveThreshold_d, obj.timeOfLastNegPresSPChangeAboveThreshold_d))*86400;
                if timeBtwUpAndDownSPikes_s < obj.ISDOWNLINKING_TIME_LIMIT_BETWEEN_UP_AND_DOWN_SPIKES_SEC
                    spikesInBothDirectionWithinTimeLimit = 1;
                else
                    spikesInBothDirectionWithinTimeLimit = 0;
                end
                
                didSpikesHappenRecently = (T_d - obj.timeOfLastPosPresSPChangeAboveThreshold_d)*86400 < obj.ISDOWNLINKING_TIME_LIMIT_BETWEEN_UP_AND_DOWN_SPIKES_SEC;
                if pressureAbsChangeAboveThreshold  && spikesInBothDirectionWithinTimeLimit && didSpikesHappenRecently && ~isPumpProbablyRamping
                    obj.isDownlinking = true;
                end
            else
                if max(presSP_vec) - min(presSP_vec) < obj.ISDOWLINKING_RESET_WHEN_DELTAP_IN_BUFFER_BELOW_THRESHOLD
                    obj.isDownlinking = false;
                end
            end
            
            obj.presSp_m_bar_prev = presSp_m_bar;
            obj.T_prev_d          = T_d;
        end
    end
    
    methods (Static)
        function [isIncreasing, isDecreasing] = checkIfChanging(vec, threshold)
            
            isIncreasing = false;
            isDecreasing = false;
            
            if isempty(vec) || numel(vec) < 2
                return
            end

            if vec(end) - vec(1) > threshold
                isIncreasing = true;
                isDecreasing = false;
            elseif vec(1) - vec(end) > threshold
                isIncreasing = false;
                isDecreasing = true;
            end
        end
    end
end