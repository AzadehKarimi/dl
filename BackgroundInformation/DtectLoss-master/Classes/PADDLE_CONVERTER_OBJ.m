%
% The paddle converter class 
% converts the paddle measurement in percent to a flowOut measurement in
% lpm. The conversion exploits the flowIn measurement in lpm and
% re-tunes a correlation between these two measurements for each 
% stand pipe change:
%
% flowOut_lpm = K_lpm_per_prc * (flowOut_prc - Bias_prc),
% where K_lpm_per_prc is a scale factor and Bias_prc is an offset
% K_lpm_per_prc is calculated after every pump start up
% Bias_prc is calculated at the first two pump stops


classdef PADDLE_CONVERTER_OBJ < DTECTLOSS_SUBMODULE
	properties(SetAccess = private)
        flowOut_lpm;                % flow out to be estimated
        isCalibratingPaddleBias;    %
        isCalibratingPaddleK;
        isCalibratedPaddle;
        numberPumpStops;
        K_lpm_per_prc;              % the constant that gives flowOut_lpm = K_lpm_per_prc*(flowOut_prc - Bias_prc);
        Bias_prc;                   % Estimated bias so that  flowOut_prc = Bias_prc, when flowIn_lpm = 0
        isRampingUp;
        params;
        stateCalibrationPaddle;     % 0=normal, 1=gather data bias (zero flow), 2=gather data K (constant non-zero flow)
        timePumpsOn_s;              % Time with constant flow in
        timePumpsOff_s;             % Time with zero flow in
        timeCalibrating_s;          % Time calibrating bias/K
    end
    properties (SetAccess = private, GetAccess = private)
        PrevFilteredSignal;     % Internal state in LP filter
        FilteredSignal;         % Internal state in LP filter
        lpK;                    % low pass filter object
        lpBias;                 % low pass filter object
        filtData;
        flowDer_lpmpsec;
        RampUpFlowIn;
        RampDownFlowIn;
        ConstFlowIn;
        ZeroFlowIn;
        timeLast_d;
        flowOutBufferObj;
        flowInBufferObj;
    end
    
    properties (Constant)
         T_lp_s                          = 30; % lowpass filter time constant in seconds
         LPMtoM3S                        = 60000;
         flowIn_min_to_tune_K_lpm        = 900;% minimum flow before starting to tune K.
         flowCalibMin_lpm                = -1000; %Measurement is not valid if below this
         flowCalibMax_lpm                = +10000; %Measurement is not valid if above this
         errVal                          = -0.99925;
         flowderThreshold_lpmpsec        = 10; % [lpm/sec] if the flow rate from the rig pumps change slower than this value -> constant/zero flow
         TfiltFlowDer_s                  = 20; % Filter time constant for estimating time derivative of flow in from flow in
         timeWaitKCalibration_s          = 120; % [sec] 60 Waiting time after pumps are back on with constant flow in before calibration shall be initiated
         timeWaitBiasCalibration_s       = 60; % [sec] Waiting time after pumps are back on with constant flow in before calibration shall be initiated
         timeTuning_s                    = 60; % 60 for how long should K be updated after a startup?
         timestepmaxReset_s              = 60*60; %Reset (K=b=0) if no flow out meas for 60mins 
         numberPumpStopsBiasCalibration  = 3;
         BUFFER_SIZE_SAMPLES             = 200;
         showdisp                        = false;
         useRingBuffer                   = false;%true
    end
	methods
        function obj    = PADDLE_CONVERTER_OBJ()
            obj.K_lpm_per_prc;
		  	obj.Bias_prc; 
            obj.flowOut_lpm;
            obj.FilteredSignal;
            obj.PrevFilteredSignal;
        end
        
		function obj    = initalize(obj, params)
            obj.params              = params;
		  	obj.K_lpm_per_prc       = 0; 
		  	obj.Bias_prc            = 0; 
            obj.PrevFilteredSignal  = 0;
            obj.lpK                 = LOWPASS_OBJ(nan, obj.T_lp_s);
            obj.lpBias              = LOWPASS_OBJ(nan, obj.T_lp_s);
            obj.filtData            = FILTERDERIVATIVE_OBJ(obj.TfiltFlowDer_s);
            obj.flowOutBufferObj    = RINGBUFFER_OBJ(obj.BUFFER_SIZE_SAMPLES);
            obj.flowInBufferObj     = RINGBUFFER_OBJ(obj.BUFFER_SIZE_SAMPLES);

            obj.isCalibratedPaddle  = false;
            obj.RampUpFlowIn        = false;
            obj.RampDownFlowIn      = false;
            obj.ConstFlowIn         = false;
            obj.ZeroFlowIn          = false;           
            obj.flowDer_lpmpsec     = 0;
            obj.timePumpsOn_s       = 0; %Time since pumps were turned on
            obj.timePumpsOff_s      = 0; %Time since pumps were turned on
            obj.timeCalibrating_s   = 0;
            obj.timeLast_d          = 0;
            obj.numberPumpStops     = 0;
            obj.isCalibratingPaddleBias = false;
            obj.isCalibratingPaddleK    = false;
            obj.isRampingUp             = false;
            obj.stateCalibrationPaddle  = 0; %normal
            obj.flowOut_lpm             = obj.errVal;
        end
        
        function obj = softReset(obj)
            obj.K_lpm_per_prc       = 0;
		  	obj.Bias_prc            = 0; 
            obj.PrevFilteredSignal  = 0;
            
            obj.isCalibratedPaddle  = false;
            obj.RampUpFlowIn        = false;
            obj.RampDownFlowIn      = false;
            obj.ConstFlowIn         = false;
            obj.ZeroFlowIn          = false;           
            obj.flowDer_lpmpsec     = 0;
            obj.timePumpsOn_s       = 0; %Time since pumps were turned on
            obj.timePumpsOff_s      = 0; %Time since pumps were turned on
            obj.timeCalibrating_s   = 0;
            obj.timeLast_d          = 0;
            obj.numberPumpStops     = 0;
            obj.isCalibratingPaddleBias = false;
            obj.isCalibratingPaddleK    = false;
            obj.isRampingUp             = false;
            obj.stateCalibrationPaddle  = 0; %normal
            obj.flowOut_lpm             = obj.errVal;    
        end
        
        % run at each iteration to save the result of all algorithms 
        function obj = convert(obj, flowOut_prc, flowIn_lpm, isDownlinking, recalibrateNow, sys_state, timeNew_d)
            dt_s = (timeNew_d - obj.timeLast_d)*86400;
      
            if (isnan(flowOut_prc) || isnan(timeNew_d) || dt_s <= 0 || flowIn_lpm == obj.errVal) %do nothing
                obj.timeLast_d = timeNew_d;                
                obj.flowOut_lpm = obj.errVal;
            elseif dt_s > obj.timestepmaxReset_s %reset
                obj            = obj.initalize(obj.params);
                obj.timeLast_d = timeNew_d;
            else % OK
                 [obj.filtData, obj.flowDer_lpmpsec] = obj.filtData.smoothderivative2ndorder(flowIn_lpm, timeNew_d);
                 obj.timeLast_d = timeNew_d;
                 if obj.flowDer_lpmpsec > obj.flowderThreshold_lpmpsec
                     obj.RampUpFlowIn   = true;
                     obj.RampDownFlowIn = false;
                     obj.ConstFlowIn    = false;
                     obj.ZeroFlowIn     = false;
                 elseif obj.flowDer_lpmpsec < -obj.flowderThreshold_lpmpsec
                     obj.RampUpFlowIn   = false;
                     obj.RampDownFlowIn = true;
                     obj.ConstFlowIn    = false;
                     obj.ZeroFlowIn     = false;
                 else %steady flow in
                     if flowIn_lpm < 100
                         obj.ZeroFlowIn     = true;
                         obj.ConstFlowIn    = false;
                         obj.RampUpFlowIn   = false;
                         obj.RampDownFlowIn = false;
                     elseif flowIn_lpm > obj.flowIn_min_to_tune_K_lpm
                         obj.ZeroFlowIn     = false;
                         obj.ConstFlowIn    = true;
                         obj.RampUpFlowIn   = false;
                         obj.RampDownFlowIn = false;
                     else %low flow rate on way up used to break the gel                        
                         obj.ZeroFlowIn     = false;
                         obj.ConstFlowIn    = false;
                         obj.RampUpFlowIn   = true;
                         obj.RampDownFlowIn = false;
                     end                                          
                 end

                 if not(obj.ConstFlowIn)
                     obj.timePumpsOn_s = 0;
                 else
                     obj.timePumpsOn_s = obj.timePumpsOn_s + min(obj.filtData.dt_s, 30);
                 end
                 
                 if not(obj.ZeroFlowIn)
                     obj.timePumpsOff_s = 0;
                 else
                     if (obj.timePumpsOff_s == 0) && sys_state.isOnBottom
                         obj.numberPumpStops = obj.numberPumpStops + 1;
                         if obj.showdisp
                             disp(['Pump stop number ' num2str(obj.numberPumpStops,'%.0f'), ', scale K = ' num2str(obj.K_lpm_per_prc,'%.2e') ' lpm/%, bias = ' num2str(obj.Bias_prc,'%.3e') ' %']);
                         end
                     end
                     obj.timePumpsOff_s = obj.timePumpsOff_s + min(obj.filtData.dt_s, 30);
                 end
                 
                 obj.flowInBufferObj  = obj.flowInBufferObj.addDataToCircularBuffer(timeNew_d,flowIn_lpm);
                 obj.flowOutBufferObj = obj.flowOutBufferObj.addDataToCircularBuffer(timeNew_d,flowOut_prc);
                 
                 if obj.stateCalibrationPaddle == 0 %Normal
                     if obj.timePumpsOn_s > obj.timeWaitKCalibration_s && obj.timePumpsOn_s < obj.timeWaitKCalibration_s + obj.timeTuning_s
                         %Do not calibrate shortly after a pump start when flow out is transient
                         obj.stateCalibrationPaddle = 1; %Gather data for K calibration
                     elseif recalibrateNow && (flowIn_lpm > obj.flowIn_min_to_tune_K_lpm)
                         obj.stateCalibrationPaddle = 1; %Gather data for K calibration
                     elseif obj.timePumpsOff_s > obj.timeWaitBiasCalibration_s %obj.timePumpsOff_s < obj.timeWaitBiasCalibration_s + obj.timeTuning_s
                         %Pumps have been off for a while
                         obj.stateCalibrationPaddle = 2; %Gather data for bias calibration
                         %Do not calibrate shortly after a pump stop when flow out is transient
                     else
                         obj.stateCalibrationPaddle = 0;
                     end                         
                     obj.timeCalibrating_s       = 0;
                     obj.isCalibratingPaddleK    = false;
                     obj.isCalibratingPaddleBias = false;
                 elseif obj.stateCalibrationPaddle == 1 %Gather data for K calibration
                     if obj.timeCalibrating_s < obj.timeTuning_s 
                         obj.stateCalibrationPaddle = 1;
                         if (flowOut_prc - obj.Bias_prc > 0) && not(isDownlinking)
                             obj.lpK  = obj.lpK.filterForCustomDt(flowIn_lpm/(flowOut_prc-obj.Bias_prc),dt_s,0);
                         end
                         obj.isCalibratingPaddleK    = false;
                         obj.isCalibratingPaddleBias = false;
                         obj.timeCalibrating_s       = obj.timeCalibrating_s + obj.filtData.dt_s;
                     else
                         %Calibrate K
                         if obj.useRingBuffer
                            [timeFlowOut_vec, flowOut_vec] = obj.flowOutBufferObj.deringbuffer();
                            [timeFlowIn_vec, flowIn_vec]   = obj.flowInBufferObj.deringbuffer();
                            if true
                                %To do - only calibrate if no vol/pres alarm is given
                                [Bias, K] = obj.calcPaddleParams(timeFlowIn_vec, flowIn_vec, timeFlowOut_vec, flowOut_vec);
                                if ~isempty(Bias) && ~isempty(K)
                                    obj.Bias_prc      = Bias;
                                    obj.K_lpm_per_prc = K;
                                end
                            end
                         else
                             if ~isempty(obj.lpK.FilteredSignal) && ...
                                     (recalibrateNow && (flowIn_lpm > obj.flowIn_min_to_tune_K_lpm) || (obj.timeCalibrating_s >= obj.timeTuning_s))
                                 obj.K_lpm_per_prc        = obj.lpK.FilteredSignal;
                                 obj.isCalibratingPaddleK = true;
                             else
                                 obj.isCalibratingPaddleK = false;
                             end
                             %Calibrate bias
                             if ~isempty(obj.lpBias.FilteredSignal)
                                 obj.Bias_prc                = obj.lpBias.FilteredSignal;
                                 obj.isCalibratingPaddleBias = true;   
                             else
                                 obj.isCalibratingPaddleBias = false;
                             end
                         end
                         if (obj.numberPumpStops > 0) && not(obj.K_lpm_per_prc == 0)
                             obj.isCalibratedPaddle    = true;
                         end
                         obj.stateCalibrationPaddle  = 0;
                         obj.timeCalibrating_s = 0;
                     end                         
                 elseif obj.stateCalibrationPaddle == 2 %Gather data for bias calibration
                     if obj.ZeroFlowIn % obj.timeCalibrating_s < obj.timeTuning_s 
                         obj.stateCalibrationPaddle = 2;
                         if not(isDownlinking) %&& (obj.numberPumpStops <= obj.numberPumpStopsBiasCalibration)  %Calibrate only on first 2 pump stops. TO DO: improve!!
                             obj.lpBias          = obj.lpBias.filterForCustomDt(flowOut_prc,dt_s,0);
                         end
                         obj.timeCalibrating_s       = obj.timeCalibrating_s + obj.filtData.dt_s;
                     else
                         obj.stateCalibrationPaddle = 0;
                         %Calibrate bias
                         if false %~isempty(obj.lpBias.FilteredSignal)
                             obj.Bias_prc    = obj.lpBias.FilteredSignal;
                             obj.isCalibratingPaddleBias = true;   
                         else
                             obj.isCalibratingPaddleBias = false;   
                         end
                         obj.timeCalibrating_s       = 0;                         
                     end
                     
                 end

                 obj.isRampingUp = obj.RampUpFlowIn || (obj.timePumpsOn_s < obj.timeWaitKCalibration_s + obj.timeTuning_s && obj.timePumpsOn_s > 0);
                 if (flowOut_prc < 0 )
                     flowOut_prc = 0;
                 end
            end
            if obj.isCalibratedPaddle
                obj.flowOut_lpm = obj.K_lpm_per_prc*(flowOut_prc - obj.Bias_prc);%//obj.FilteredSignal;
                if obj.flowOut_lpm < obj.flowCalibMin_lpm || obj.flowOut_lpm > obj.flowCalibMax_lpm
                    obj.flowOut_lpm = obj.errVal;
                elseif obj.flowOut_lpm < 0
                    obj.flowOut_lpm = 0;
                end
            else
                obj.flowOut_lpm = obj.errVal;
            end
        end
        
        function [obj, Bias, K] = calcPaddleParams(obj, timeIn_vec, flowIn_vec, timeOut_vec, flowOut_vec)
            Bias = [];
            K    = [];
            %1. Find last periode when flowin was zero
            iEndZeroFlow   = find(flowIn_vec<50, 1, 'last' );
            iStartZeroFlow = find(flowIn_vec(1:iEndZeroFlow)>50, 1, 'last' );
            if isempty(iStartZeroFlow)
                return;
            end
            iStartBiasCalc = min(timeIn_vec - timeIn_vec(iStartZeroFlow) > obj.timeWaitBiasCalibration_s/86400);
            %2. Calculate bias as average flowOut in this period excl 1. minute
            Bias = mean(flowOut_vec(iStartBiasCalc:iEndZeroFlow));
            %3. Find periode after pump was back on
            iStartFullFlow = find(flowIn_vec - flowIn_vec(obj.BUFFER_SIZE_SAMPLES) > 50, 1, 'last' );
            iStartKCalc    = min(timeIn_vec - timeIn_vec(iStartFullFlow) > obj.timeWaitKCalibration_s/86400);
            %4. Calculate scale (gain)
            K              = (mean(flowOut_vec(iStartKCalc:obj.BUFFER_SIZE_SAMPLES)) - Bias)/mean(flowIn_vec(iStartKCalc:obj.BUFFER_SIZE_SAMPLES));
        end                       
    end
end