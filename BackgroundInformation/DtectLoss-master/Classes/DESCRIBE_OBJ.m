classdef DESCRIBE_OBJ < DTECTLOSS_SUBMODULE
    %   DESCRIBE_OBJ Summary of this class goes here
    %   Detailed explanation goes here
    %   Describe washed input data: check minimum data set available (flow in/out, pit vol, bit depth and hole depth)
    %   Statistics: system states (drilling, tripping, flow check, conditioning)
    %   TO DO: identify and count number of connections drilling and
    %   tripping
    
    properties         
        %Statistics
        numDataPoints       ;
        numPumpStarts       ;
        lengthTimeSeries_s  ;
        dt_s                ;
        timeBadData_s       ;
        timeDrilling_s      ;
        timeFlowCheck_s     ;
        timeTripping_s      ;
        timeConditioning_s  ;
        timeStep_max_s      ;
        timeStep_avg_s      ;
        depthBit_min_m      ;
        depthBit_max_m      ;
        depthHole_min_m     ;
        depthHole_max_m     ;
        velBlock_min_mph    ;
        velBlock_max_mph    ;
        rotation_max_rpm    ;
        flowOut_max_lpm     ;
        flowIn_max_lpm      ;
        volPit_min_m3       ;
        volPit_max_m3       ;
        volTripTank_min_m3  ;
        volTripTank_max_m3  ;
        presSP_max_bar      ;
        seriesOK            ;
        flowOutraw_min_lpm      ;
        flowOutraw_max_lpm      ;
        flowOutraw_min_percent  ;
        flowOutraw_max_percent  ;
        flowOutrawCoriolis_min_lpm ;
        flowOutrawCoriolis_max_lpm ;
        flowOutwashPercent_min_lpm  ;
        flowOutwashPercent_max_lpm  ;
        tempBHA_min_degC    ;
        tempBHA_max_degC    ;
    end
        
    properties (Constant)
        timeStep_reject_s  = 60;
    end
 
    methods
        
        function obj = DESCRIBE_OBJ()
            
        end
        
        function obj        = init(obj)
            obj.numDataPoints       = 0;
            obj.numPumpStarts       = 0;
            obj.lengthTimeSeries_s  = 0;
            obj.dt_s                = 0;
            obj.timeBadData_s       = 0;
            obj.timeDrilling_s      = 0;
            obj.timeFlowCheck_s     = 0;
            obj.timeTripping_s      = 0;
            obj.timeConditioning_s  = 0;
            obj.timeStep_max_s      = 0;
            obj.timeStep_avg_s      = 0;
            obj.flowOutraw_min_lpm      = 0;
            obj.flowOutraw_max_lpm      = 0;
            obj.flowOutraw_min_percent  = 0;
            obj.flowOutraw_max_percent  = 0;
            obj.depthBit_min_m      = 0;
            obj.depthBit_max_m      = 0;
            obj.depthHole_min_m     = 0;
            obj.depthHole_max_m     = 0;
            obj.velBlock_min_mph    = 0;
            obj.velBlock_max_mph    = 0;
            obj.rotation_max_rpm    = 0;
            obj.flowOut_max_lpm     = 0;
            obj.flowIn_max_lpm      = 0;
            obj.volPit_min_m3       = 0;
            obj.volPit_max_m3       = 0;
            obj.volTripTank_min_m3  = 0;
            obj.volTripTank_max_m3  = 0;
            obj.presSP_max_bar      = 0;
            obj.tempBHA_min_degC    = 0;
            obj.tempBHA_max_degC    = 0;

            obj.seriesOK            = 0;
        end
        
        function obj = softReset(obj)
            obj = obj.init();
        end
   
        function obj = describeData(obj, rawData, washedData, sysState, dt_s)
            obj.dt_s = dt_s; 
            if (obj.dt_s > 0 && obj.dt_s < obj.timeStep_reject_s)
                obj.numDataPoints = obj.numDataPoints + 1;
                obj.lengthTimeSeries_s = obj.lengthTimeSeries_s + dt_s;            
                switch sysState.currentState
                    case DrillingStates.BadData
                        obj.timeBadData_s       = obj.timeBadData_s + dt_s; 
                    case  DrillingStates.Drilling
                        obj.timeDrilling_s      = obj.timeDrilling_s + dt_s; 
                    case DrillingStates.Tripping
                        obj.timeTripping_s      = obj.timeTripping_s + dt_s; 
                    case DrillingStates.FlowCheck
                        obj.timeFlowCheck_s     = obj.timeFlowCheck_s + dt_s; 
                    case DrillingStates.Conditioning
                        obj.timeConditioning_s  = obj.timeConditioning_s + dt_s; 
                end                       
                if dt_s > obj.timeStep_max_s
                    obj.timeStep_max_s = dt_s;
                end
                obj.timeStep_avg_s = obj.lengthTimeSeries_s/obj.numDataPoints;
                
                if obj.depthBit_min_m == 0 || obj.depthBit_min_m > washedData.depthBit_m
                    obj.depthBit_min_m = washedData.depthBit_m;
                end
                if obj.depthBit_max_m == 0 || obj.depthBit_max_m < washedData.depthBit_m
                    obj.depthBit_max_m = washedData.depthBit_m;
                end
                if obj.depthHole_min_m == 0 || obj.depthHole_min_m > washedData.depthHole_m
                    obj.depthHole_min_m = washedData.depthHole_m;
                end
                if obj.depthHole_max_m == 0 || obj.depthHole_max_m < washedData.depthHole_m
                    obj.depthHole_max_m = washedData.depthHole_m;
                end
                if obj.flowIn_max_lpm == 0 || obj.flowIn_max_lpm < washedData.flowIn_lpm
                    obj.flowIn_max_lpm = washedData.flowIn_lpm;
                end
                if obj.flowOut_max_lpm == 0 || obj.flowOut_max_lpm < washedData.flowOut_lpm
                    obj.flowOut_max_lpm = washedData.flowOut_lpm;
                end
                if obj.flowOutraw_min_lpm == 0 || obj.flowOutraw_min_lpm > rawData.flowOut_lpm
                    obj.flowOutraw_min_lpm = rawData.flowOut_lpm;
                end
                if obj.flowOutraw_max_lpm == 0 || obj.flowOutraw_max_lpm < rawData.flowOut_lpm
                    obj.flowOutraw_max_lpm = rawData.flowOut_lpm;
                end
                if obj.volPit_min_m3 == 0 || obj.volPit_min_m3 > washedData.volGainLossWithoutOffset_m3
                    obj.volPit_min_m3 = washedData.volGainLossWithoutOffset_m3;
                end
                if obj.volPit_max_m3 == 0 || obj.volPit_max_m3 < washedData.volGainLossWithoutOffset_m3
                    obj.volPit_max_m3 = washedData.volGainLossWithoutOffset_m3;
                end
                if obj.volTripTank_min_m3 == 0 || obj.volTripTank_min_m3 > washedData.volTripTank_m3
                    obj.volTripTank_min_m3 = washedData.volTripTank_m3;
                end
                if obj.volTripTank_max_m3 == 0 || obj.volTripTank_max_m3 < washedData.volTripTank_m3
                    obj.volTripTank_max_m3 = washedData.volTripTank_m3;
                end
                if obj.presSP_max_bar == 0 || obj.presSP_max_bar < washedData.presSP_bar
                    obj.presSP_max_bar = washedData.presSP_bar;
                end
                if obj.velBlock_min_mph == 0 || obj.velBlock_min_mph > washedData.velBlock_mph
                    obj.velBlock_min_mph = washedData.velBlock_mph;
                end
                if obj.velBlock_max_mph == 0 || obj.velBlock_max_mph < washedData.velBlock_mph
                    obj.velBlock_max_mph = washedData.velBlock_mph;
                end                               
                if obj.rotation_max_rpm == 0 || obj.rotation_max_rpm < washedData.rpm
                    obj.rotation_max_rpm = washedData.rpm;
                end
                if isfield(rawData,'tempBHA_degC')
                    if obj.tempBHA_min_degC == 0 || obj.tempBHA_min_degC > washedData.tempBHA_degC
                        obj.tempBHA_min_degC = washedData.tempBHA_degC;
                    end
                    if obj.tempBHA_max_degC == 0 || obj.tempBHA_max_degC < washedData.tempBHA_degC
                        obj.tempBHA_max_degC = washedData.tempBHA_degC;
                    end
                end
            end
        end
        
        function obj =  checkDataSeries(obj)
            obj.seriesOK         = (obj.lengthTimeSeries_s > 3600) & (obj.flowIn_max_lpm > 0) & ...
                                   (obj.flowOut_max_lpm > 0)  & ...
                                   ( (obj.volPit_max_m3 - obj.volPit_min_m3 > 1) || (obj.volTripTank_max_m3 - obj.volTripTank_min_m3 > 0.1) );                               
        end

        function [textResult, obj] =  DispSeriesAnalysis(obj)
            if obj.seriesOK
                textResult = cellstr('Time series OK');
            else
                textResult = cellstr('Bad time series');
            end
            textResult = [textResult;['Length time series = ' num2str(obj.lengthTimeSeries_s/60,'%.0f') ' min with ' num2str(obj.numDataPoints,'%.0f') ' data points']]; 
            textResult = [textResult;['Time with bad data = '       num2str(obj.timeBadData_s/60,'%.0f')        ' min (' num2str(obj.timeBadData_s/obj.lengthTimeSeries_s*100,'%.2f') '%)']]; 
            textResult = [textResult;['Time spent drilling = '      num2str(obj.timeDrilling_s/60,'%.0f')       ' min (' num2str(obj.timeDrilling_s/obj.lengthTimeSeries_s*100,'%.2f') '%)']]; 
            textResult = [textResult;['Time spent tripping = '      num2str(obj.timeTripping_s/60,'%.0f')       ' min (' num2str(obj.timeTripping_s/obj.lengthTimeSeries_s*100,'%.2f') '%)']]; 
            textResult = [textResult;['Time spent flow checking = ' num2str(obj.timeFlowCheck_s/60,'%.0f')      ' min (' num2str(obj.timeFlowCheck_s/obj.lengthTimeSeries_s*100,'%.2f') '%)']]; 
            textResult = [textResult;['Time spent conditioning = '  num2str(obj.timeConditioning_s/60,'%.0f')   ' min (' num2str(obj.timeConditioning_s/obj.lengthTimeSeries_s*100,'%.2f') '%)']]; 
            textResult = [textResult;['Time step:     max = ' num2str(obj.timeStep_max_s,'%.2f')        ' sec,  average = ' num2str(obj.timeStep_avg_s,'%.2f') ' sec']]; 
            textResult = [textResult;['Bit depth:     min = ' num2str(obj.depthBit_min_m,'%.1f')        ' m,    max = ' num2str(obj.depthBit_max_m, '%.0f') ' m']]; 
            textResult = [textResult;['Hole depth:    min = ' num2str(obj.depthHole_min_m,'%.1f')       ' m,    max = ' num2str(obj.depthHole_max_m,'%.0f') ' m. Drilled ' num2str(obj.depthHole_max_m-obj.depthHole_min_m,'%.0f') ' m.']]; 
            textResult = [textResult;['Flow in:       max = ' num2str(obj.flowIn_max_lpm,  '%.1f')      ' lpm']]; 
            textResult = [textResult;['Flow out washed:      max = ' num2str(obj.flowOut_max_lpm,'%.1f')       ' lpm']]; 
            textResult = [textResult;['Flow out raw lpm:     min = ' num2str(obj.flowOutraw_min_lpm,'%.2f')         ' lpm, max = '  num2str(obj.flowOutraw_max_lpm, '%.2f')         ' lpm']];                        
            textResult = [textResult;['Pit volume:    min = ' num2str(obj.volPit_min_m3,'%.3f')         ' m3,   max = '  num2str(obj.volPit_max_m3,      '%.3f')  ' m3']]; 
            textResult = [textResult;['TT volume:     min = ' num2str(obj.volTripTank_min_m3,'%.3f')    ' m3,   max = '  num2str(obj.volTripTank_max_m3, '%.3f')  ' m3']]; 
            textResult = [textResult;['Pump pressure: max = ' num2str(obj.presSP_max_bar,     '%.1f')   ' bar']]; 
            if obj.tempBHA_max_degC > obj.tempBHA_min_degC
                textResult = [textResult;['BHA temp:      min = ' num2str(obj.tempBHA_min_degC,  '%.1f')    ' degC, max = '  num2str(obj.tempBHA_max_degC,  '%.1f')   ' degC']];
            end
            textResult = [textResult;['Block velocity:min = ' num2str(obj.velBlock_min_mph/60,  '%.1f') ' m/min, max = ' num2str(obj.velBlock_max_mph/60,  '%.1f')   ' m/min']];
            if obj.timeDrilling_s > 0
                textResult = [textResult;['ROP average:   net = ' num2str((obj.depthHole_max_m-obj.depthHole_min_m)/obj.timeDrilling_s*3600,'%.1f') ' m/hr,' ...
                                                       'gross = ' num2str((obj.depthHole_max_m-obj.depthHole_min_m)/obj.lengthTimeSeries_s*3600,'%.1f') ' m/hr']];            
            end
            textResult = [textResult;['Rotation:      max = ' num2str(obj.rotation_max_rpm,'%.1f')      ' rpm']]; 
            disp(textResult);
        end
    end
end

