% This DtectLoss kick/loss algorithm class applies an unscented Kalman filter 
% to estimate influx/loss rate in addition to loss rate on shakers, mud volume in flow line, 
% mud volume in pit, relative compressibility (beta/V), inverse inertia (1/M), flow line factor (k),
% relative friction (c/M), and calibration factor for paddle (s=qout/qpaddle) based on 
% measurements of flow in, pump pressure, flow out and pit volume:
% 
% d/dt ppump = beta/Vol ( qp + qio - qout) 
% d/dt qout  = 1/M (ppump - c*qpump)
% d/dt Vfl   = qout - qpit - qshaker 
% d/dt Vpit  = qpit - qshaker
%
% qpit       = k*Vfl
% qpaddle    = s*qout
%
%

classdef UKF_ALG_OBJ < EKD_ALGORITHM_OBJ   
    properties (SetAccess = private)       
        %Inputs
        flowIn_lpm                  %filter alt 1,2,3,4
        flowOut_lpm                 %filter alt 1,2,3,4
        %States
        volPit_m3                   %filter alt 1,2,3,4
        presSP_bar                  %filter alt   2,3,4 
        volFL_m3                    %filter alt     3,4
        flowOutx_lpm                %filter alt       4
        volTT_m3                    %filter alt         5
        
        %Params
        flowIO_lpm                  %filter alt 1,2,3,4
        betaperVol_barm3            %filter alt   2,3,4
        kpit_lpmpm3                 %filter alt     3,4
        kshaker_lpmpm3              %filter alt     3,4
        spaddle_lpmpc               %filter alt     3,4
        Minv_barsplpm               %filter alt       4
        cfrac_barplpm               %filter alt       4
        flowTT_lpm                  %filter alt         5
        areaDP_m2                   %filter alt         5
        valveTT_01                  %filter alt         5 Normal value 1, 0 when return is routed to trip tank (tripping or flow check)
        velBlock_mps                %filter alt         5
        
        %noise terms
        volPitderxnoise_m3ps        %filter alt 1,2,3,4
        presSPderxnoise_barps       %filter alt   2,3,4  
        volFLderxnoise_m3ps         %filter alt     3,4 
        flowOutderxnoise_lpmps      %filter alt       4        
        volTTderxnoise_m3ps         %filter alt         5 
        
        flowIOderwnoise_lpmps       %filter alt 1,2,3,4 
        betaprVolderwnoise_barm3ps  %filter alt   2,3,4
        kpitderwnoise_lpmpm3ps      %filter alt     3,4
        kshakerderwnoise_lpmpm3ps   %filter alt     3,4
        spaddlederwnoise_lpmpcps    %filter alt     3,4
        Minvderwnoise_barplpm       %filter alt       4
        cfracderwnoise_barplpmps    %filter alt       4
        flowTTderwnoise_lpmps       %filter alt         5
        areaDPderwnoise_m2ps        %filter alt         5
        
        volPitynoise_m3             %filter alt 1,2,3,4
        flowOutynoise_lpm           %filter alt 1,2,3,4         
        presSPynoise_bar            %filter alt   2,3,4
        volTTynoise_m3              %filter alt         5
        %Deducted var
        flowShaker_lpm              %filter alt     3,4
        flowPit_lpm                 %filter alt     3,4
        presFric_bar                %filter alt       4
        volIO_m3                    %filter alt 1,2,3,4
        flowSteel_lpm               %filter alt         5
        
        statex
        inputu
        measy
        paramw %unknown parameters
        paramp %known parameters
        
        edyn
        hmeas
        
        varxwQ
        varxwP
        varyR

        dt_s
        timeprev_d
        timestepmax_s
        params;             % tunable paramters struct releated to the algorithm

        timeLast_d;     
        timeCurrent_d;
        
        
 %       samplesSinceStartedDrilling;
        volumeThreshold_m3;
        alarmState;
        alarmStatus;
        comments;
    end
    
    properties (Constant)
        debug                   = false;%false;%true;
        filteralt               = 2;%1 = simple with 1 state & 1 param, 2 = include pdot eq and fixed beta/V, 3 = add flowline dyn and fixed k, 4 = add flow out as state, 5 = 2 + trip tank func
        betaperVol_barm3_min    = 5000/200;% maximum value for beta/V
        betaperVol_barm3_max    = 50000/10;% minimum value for beta/V
    end
    
    methods
        function obj        = UKF_ALG_OBJ()
            obj.params      = [];
            obj.flowIO_lpm  = [];
            obj.volIO_m3    = [];
            obj.alarmState  = 0; 
            obj.alarmStatus = '';
            obj.comments    = cellstr('');            
%            obj.samplesSinceStartedDrilling =[];
        end
        
        function obj        = initialize(obj,params)%doPcalc
            obj.params      = params;
            obj.flowIO_lpm  = 0; % w1
            obj.volIO_m3    = 0; %integrate flowIO 

            if obj.filteralt==1,
                obj.volPit_m3                   = 0; % x1
                obj.flowIn_lpm                  = 0; % u1
                obj.flowOut_lpm                 = 0; % u2
                %Tuning params
                obj.volPitderxnoise_m3ps        = 100/60000; % vx1
                obj.flowIOderwnoise_lpmps       = 100; % vw2
                obj.volPitynoise_m3             = 0.1; % vy1                
                obj.flowOutynoise_lpm           = 1;   % vy2
            elseif obj.filteralt==2,
                obj.volPit_m3                   = 0;   % x1
                obj.presSP_bar                  = 0;   % x2
                obj.betaperVol_barm3            = 15000/200; % w2
                obj.flowIn_lpm                  = 0;   % u1
                obj.flowOut_lpm                 = 0;   % u2
                %Tuning params
                obj.volPitderxnoise_m3ps        = 500/60000; % vx1 100/60000
                obj.presSPderxnoise_barps       = 1;   % vx2 1    
                obj.flowIOderwnoise_lpmps       = 200; % vw1 200
                obj.betaprVolderwnoise_barm3ps  = 0*1; % vw2
                obj.volPitynoise_m3             = 1; % vm1 0.1                
                obj.flowOutynoise_lpm           = 1;  % vm2 10
                obj.presSPynoise_bar            = 0.1;   % vm3 2              
            elseif obj.filteralt==3,
                obj.volPit_m3                   = 0;   % x1
                obj.presSP_bar                  = 0;   % x2
                obj.volFL_m3                    = 0;   % x3                
                obj.betaperVol_barm3            = 15000/156; % w2
                obj.kpit_lpmpm3                 = 1000;% w3
                obj.kshaker_lpmpm3              = 10;  % w4
                obj.spaddle_lpmpc               = 1;   % w5
                obj.flowIn_lpm                  = 0;   % u1
                obj.flowOut_lpm                 = 0;   % u2
                obj.flowPit_lpm                 = 0;
                obj.flowShaker_lpm              = 0;
                %Tuning params
                obj.volPitderxnoise_m3ps        = 100;  % vx1
                obj.presSPderxnoise_barps       = 1;    % vx2     
                obj.volFLderxnoise_m3ps         = 0.01; % vx3     
                obj.flowIOderwnoise_lpmps       = 50;   % vw1 200
                obj.betaprVolderwnoise_barm3ps  = 0*1;  % vw2
                obj.kpitderwnoise_lpmpm3ps      = 5;    % vw3
                obj.kshakerderwnoise_lpmpm3ps   = 1;    % vw4
                obj.spaddlederwnoise_lpmpcps    = 0;    % vw5
                obj.volPitynoise_m3             = 0.1;  % vy1                
                obj.flowOutynoise_lpm           = 10;   % vy2
                obj.presSPynoise_bar            = 2;    % vy3               
            elseif obj.filteralt==4,
                obj.volPit_m3                   = 0;   % x1
                obj.presSP_bar                  = 0;   % x2
                obj.volFL_m3                    = 0;   % x3 
                obj.flowOutx_lpm                = 0;   % x4
                obj.betaperVol_barm3            = 15000/156; % w2
                obj.kpit_lpmpm3                 = 1000;% w3
                obj.kshaker_lpmpm3              = 10;  % w4
                obj.spaddle_lpmpc               = 1;   % w5
                obj.Minv_barsplpm               = 4e8/60000; % w6
                obj.cfrac_barplpm               = 77/2000;   % w7
                obj.flowIn_lpm                  = 0;   % u1
                obj.flowOut_lpm                 = 0;   % u2
                obj.flowPit_lpm                 = 0;
                obj.flowShaker_lpm              = 0;
                obj.presFric_bar                = 0;
                %Tuning params
                obj.volPitderxnoise_m3ps        = 100;  % vx1
                obj.presSPderxnoise_barps       = 1;    % vx2     
                obj.volFLderxnoise_m3ps         = 0.01; % vx3     
                obj.flowOutderxnoise_lpmps      = 0.01; % vx4     
                obj.flowIOderwnoise_lpmps       = 50;   % vw1 200
                obj.betaprVolderwnoise_barm3ps  = 0*1;  % vw2
                obj.kpitderwnoise_lpmpm3ps      = 5;    % vw3
                obj.kshakerderwnoise_lpmpm3ps   = 1;    % vw4
                obj.spaddlederwnoise_lpmpcps    = 0;    % vw5
                obj.Minvderwnoise_barplpm       = 0;    % vw6
                obj.cfracderwnoise_barplpmps    = 0;    % vw7
                obj.volPitynoise_m3             = 0.1;  % vy1                
                obj.flowOutynoise_lpm           = 10;   % vy2
                obj.presSPynoise_bar            = 2;    % vy3               
            elseif obj.filteralt==5,
                obj.volPit_m3                   = 0;   % x1
                obj.presSP_bar                  = 0;   % x2
                obj.volTT_m3                    = 0;   % x3
                obj.betaperVol_barm3            = 15000/200; % w2
                obj.flowTT_lpm                  = 0;   % w3
                obj.areaDP_m2                   = 0.0038;   % w4
                obj.valveTT_01                  = 1; % return to pit, not to flow line and pit  
                obj.velBlock_mps                = 0;
                obj.flowIn_lpm                  = 0;   % u1
                obj.flowOut_lpm                 = 0;   % u2
                %Tuning params
                obj.volPitderxnoise_m3ps        = 100/60000; % vx1
                obj.presSPderxnoise_barps       = 1;   % vx2     
                obj.volTTderxnoise_m3ps         = 10/60000; % vx3
                obj.flowIOderwnoise_lpmps       = 200; % vw1
                obj.betaprVolderwnoise_barm3ps  = 0*1; % vw2
                obj.flowTTderwnoise_lpmps       = 1000;% vw3
                obj.areaDPderwnoise_m2ps        = 0;   % vw4
                obj.volPitynoise_m3             = 0.1; % vm1                
                obj.flowOutynoise_lpm           = 10;  % vm2
                obj.presSPynoise_bar            = 2;   % vm3               
                obj.volTTynoise_m3              = 0.1; % vm1                
            end
            obj.timeprev_d          = 0;
            obj.dt_s                = 0;
            obj.timestepmax_s       = 60;
            obj.timeLast_d          = 0;
            obj.timeCurrent_d       = 0;
            
%            obj.samplesSinceStartedDrilling = 0;

%            obj.sysState = SYSTEM_STATES_OBJ(); % the state machine

            %% UKF init
            if obj.filteralt == 1,%simple filter with 1 state & 1 param
                nx              = 1; % number of states x=Vpit
                nu              = 1; % number of inputs u=[qp;qout]
                nw              = 1; % number of parameters to be estimated w=[qres]
                ny              = 2; % number of measurements y=[Vpit;qout]
                obj.hmeas       = @(xw,u,p) [xw(1);u(1)+xw(2)] ; % function to obtain measured state
                obj.edyn        = @(xw,u,p,dt) [xw(1)+dt*(xw(2)/60000);xw(2)]; % extended dynamics x_k+1=f(xk,uk,pk,dt);w_k+1=w_k
                obj.varyR       = eye(ny); % covariance of measurement               
                obj.varxwQ      = eye(nx+nw);% apriori covariance of process     
                obj.varxwP      = obj.varxwQ;   % aposteriori covariance state
                obj.statex      = [obj.volPit_m3]; % initial state
                obj.inputu      = [obj.flowIn_lpm; obj.flowOut_lpm];
                obj.measy       = [obj.volPit_m3];
                obj.paramw      = [obj.flowIO_lpm];
                obj.paramp      = [];
            elseif obj.filteralt == 2,%filter with 2 state & 2 param
                nx              = 2; % number of states x=Vpit
                nu              = 2; % number of inputs u=[qp;qout]
                nw              = 2; % number of parameters to be estimated w=[qres]
                np              = 0; % number of fixed parameters
                ny              = 3; % number of measurements y=[Vpit;qout;psp]
                obj.hmeas       = @(xw,u,p) [xw(1);u(2);xw(2)] ; % function to obtain measured state
                obj.edyn        = @(xw,u,p,dt) [xw(1)+dt*(u(2)-u(1))/60000;xw(2)+dt*xw(4)*(u(1)+xw(3)-u(2))/60000;xw(3);xw(4)]; % extended dynamics x_k+1=f(xk,uk,pk,dt);w_k+1=w_k
                obj.varyR       = eye(ny); % covariance of measurement               
                obj.varxwQ      = eye(nx+nw);% apriori covariance of process     
                obj.varxwP      = obj.varxwQ;   % aposteriori covariance state
                obj.statex      = [obj.volPit_m3;obj.presSP_bar]; % initial state                
                obj.inputu      = [obj.flowIn_lpm; obj.flowOut_lpm];
                obj.measy       = [obj.volPit_m3;obj.flowOut_lpm;obj.presSP_bar];
                obj.paramw      = [obj.flowIO_lpm;obj.betaperVol_barm3];
                obj.paramp      = [];
            elseif obj.filteralt == 3,%filter with 3 states & 2 param
                nx              = 3; % number of states x=[Vpit psp Vfl]
                nu              = 2; % number of inputs u=[qp;qout]
                nw              = 5; % number of parameters to be estimated default w=[qres beta/v kpit kshaker spaddle]
                np              = 0; % number of fixed parameters default []
                ny              = 3; % number of measurements y=[Vpit;qout;psp]
                obj.statex      = [obj.volPit_m3;...        % xw1
                                   obj.presSP_bar;...       % xw2
                                   obj.volFL_m3];           % xw3 initial state                
                obj.paramw      = [obj.flowIO_lpm;...       % xw4
                                   obj.betaperVol_barm3;... % xw5
                                   obj.kpit_lpmpm3;...      % xw6
                                   obj.kshaker_lpmpm3;...   % xw7
                                   obj.spaddle_lpmpc];      % xw8
                obj.inputu      = [obj.flowIn_lpm; obj.flowOut_lpm];
                obj.measy       = [obj.volPit_m3;obj.flowOut_lpm;obj.presSP_bar];
                obj.paramp      = [];
                obj.edyn        = @(xw,u,p,dt) [xw(1)+dt*(xw(6)*xw(3)-u(1))/60000;...
                                                xw(2)+dt*xw(5)*(u(1)+xw(4)-xw(8)*u(2))/60000;...
                                                xw(3)+dt*(xw(8)*u(2)-(xw(6)+xw(7))*xw(3))/60000;...
                                                xw(4);...
                                                xw(5);...
                                                xw(6);...
                                                xw(7);...
                                                xw(8)]; % extended dynamics x_k+1=f(xk,uk,pk,dt);w_k+1=w_k
                obj.hmeas       = @(xw,u,p) [xw(1);u(2);xw(2)] ; % function to obtain measured state
                obj.varyR       = eye(ny); % covariance of measurement               
                obj.varxwQ      = eye(nx+nw);% apriori covariance of process     
                obj.varxwP      = obj.varxwQ;   % aposteriori covariance state
            elseif obj.filteralt == 4,
                nx              = 4; % number of states x=[Vpit psp Vfl qout]
                nu              = 1; % number of inputs u=[qp]
                nw              = 7; % number of parameters to be estimated default w=[qres beta/v kpit kshaker spaddle]
                np              = 0; % number of fixed parameters default []
                ny              = 3; % number of measurements y=[Vpit;qout;psp]
                obj.statex      = [obj.volPit_m3;...        % xw1
                                   obj.presSP_bar;...       % xw2
                                   obj.volFL_m3;...         % xw3
                                   obj.flowOutx_lpm];       % xw4 initial state                
                obj.paramw      = [obj.flowIO_lpm;...       % xw5
                                   obj.betaperVol_barm3;... % xw6
                                   obj.kpit_lpmpm3;...      % xw7
                                   obj.kshaker_lpmpm3;...   % xw8
                                   obj.spaddle_lpmpc;...    % xw9
                                   obj.Minv_barsplpm;...    % xw10
                                   obj.cfrac_barplpm];      % xw11

                obj.inputu      = [obj.flowIn_lpm];
                obj.measy       = [obj.volPit_m3;obj.flowOut_lpm;obj.presSP_bar];
                obj.paramp      = [];
                obj.edyn        = @(xw,u,p,dt) [xw(1)+dt*(xw(7)*xw(3)-u(1))/60000;...
                                                xw(2)+dt*xw(6)*(u(1)+xw(5)-xw(4))/60000;...
                                                xw(3)+dt*(xw(4)-(xw(7)+xw(8))*xw(3))/60000;...
                                                xw(4)+dt*xw(10)*(xw(2)-xw(11)*u(1));...
                                                xw(5);...
                                                xw(6);...
                                                xw(7);...
                                                xw(8);...
                                                xw(9);...
                                                xw(10);...
                                                xw(11)]; % extended dynamics x_k+1=f(xk,uk,pk,dt);w_k+1=w_k
                obj.hmeas       = @(xw,u,p) [xw(1);xw(9)*xw(4);xw(2)] ; % function to obtain measured state
                obj.varyR       = eye(ny);      % covariance of measurement               
                obj.varxwQ      = eye(nx+nw);   % apriori covariance of process     
                obj.varxwP      = obj.varxwQ;   % aposteriori covariance state
            elseif obj.filteralt == 5,%filter with 2 state & 2 param
                nx              = 3; % number of states x=Vpit psp Vtt
                nu              = 2; % number of inputs u=[qp;qout]
                nw              = 4; % number of parameters to be estimated w=[qIO beta/V qtt Adp]
                np              = 0; % number of fixed parameters
                ny              = 4; % number of measurements y=[Vpit;qout;psp;Vtt]
                obj.statex      = [obj.volPit_m3;...        % xw1
                                   obj.presSP_bar;...       % xw2
                                   obj.volTT_m3];           % xw3 initial state                
                obj.paramw      = [obj.flowIO_lpm;...       % xw4
                                   obj.betaperVol_barm3;... % xw5
                                   obj.flowTT_lpm;...       % xw6
                                   obj.areaDP_m2];          % xw7 initial param 
                obj.inputu      = [obj.flowIn_lpm; obj.flowOut_lpm];
                obj.measy       = [obj.volPit_m3;obj.flowOut_lpm;obj.presSP_bar;obj.volTT_m3];
                obj.paramp      = [obj.valveTT_01; obj.velBlock_mps];
                obj.hmeas       = @(xw,u,p) [xw(1);u(2);xw(2);xw(3)] ; % function to obtain measured state
                obj.edyn        = @(xw,u,p,dt) [xw(1)+dt*(p(1)*u(2)-u(1))/60000;...
                                                xw(2)+dt*xw(5)*(u(1)+xw(4)-u(2)+(1-p(1))*xw(6)+p(2)*xw(7)*60000)/60000;...
                                                xw(3)+dt*((1-p(1))*u(2)-xw(6))/60000;...
                                                xw(4);...
                                                xw(5);...
                                                xw(6);...
                                                xw(7)]; % extended dynamics x_k+1=f(xk,uk,pk,dt);w_k+1=w_k
                obj.varyR       = eye(ny); % covariance of measurement               
                obj.varxwQ      = eye(nx+nw);% apriori covariance of process     
                obj.varxwP      = obj.varxwQ;   % aposteriori covariance state
            end
        end
            
        %determine if an alarm has occured
        function obj=setAlarmState(obj)
            if abs(obj.volIO_m3) > obj.params.volumeThresholdAlarm_m3,
                obj.alarmState = 2; % Alarm
            elseif abs(obj.volIO_m3) > obj.params.volumeThresholdWarning_m3,
                obj.alarmState = 1; % Warning
            else
                obj.alarmState = 0; % Normal
            end
        end
        
        % this function is called for every vector of new data
        function [obj, result] = stepIt(obj, processData, expData, sysState,  timeCurrent_d,dt_s)
            obj.timeCurrent_d   = timeCurrent_d;
            obj.dt_s            = dt_s;%(obj.timeCurrent_d - obj.timeLast_d)*86400;
            obj.timeLast_d      = obj.timeCurrent_d;
            
            lpmToM3ps           = 1/60000;
            dayTos              = 1/86400;
            hourTos             = 1/3600;
            
            obj.alarmStatus     = 'Normal';
          %  obj.dt_s            = timeCurrent_d - obj.timeLast_d)*86400;
            
            if (obj.dt_s <= 0),%do nothing
                obj.alarmStatus = ['Do nothing, time step = ' num2str(obj.dt_s,'%.2f') ' sec'];
            elseif obj.dt_s > obj.timestepmax_s,%reset
                obj.alarmStatus = ['Reset, time step = ' num2str(obj.dt_s,'%.2f') ' sec'];
                obj             = initialize(obj,obj.params);
                obj.alarmState  = AlarmState.Disabled;
            elseif isempty(processData.flowIn_lpm) || isempty(processData.volGainLossWithoutOffset_m3) ...
                || isempty(processData.flowOut_lpm) || isempty(processData.presSP_bar),
                obj.alarmState  = AlarmState.InputError;
            else %ok - step ukf
                obj.timeprev_d  = obj.timeprev_d + obj.dt_s*dayTos;%newtime_d;  TO DO: input newtime, compute dt from prevtime          
                % ukf
                if obj.filteralt == 1,
                    obj.inputu          = [processData.flowIn_lpm];
                    obj.measy           = [processData.volGainLossWithoutOffset_m3;processData.flowOut_lpm];
                    obj.statex          = [obj.volPit_m3]; % state
                    obj.paramw          = [obj.flowIO_lpm];% param
                    obj.paramp          = [];
                    obj.varxwQ(1,1)     = (obj.volPitderxnoise_m3ps*obj.dt_s)^2; % Volpit
                    obj.varxwQ(2,2)     = (obj.flowIOderwnoise_lpmps*obj.dt_s)^2; % flowIO
                    obj.varyR(1,1)      = obj.volPitynoise_m3^2;
                    obj.varyR(2,2)      = obj.flowOutynoise_lpm^2;                    
                elseif obj.filteralt == 2,
                    obj.inputu      = [processData.flowIn_lpm; processData.flowOut_lpm];
                    obj.measy       = [processData.volGainLossWithoutOffset_m3;processData.flowOut_lpm;processData.presSP_bar];
                    obj.statex      = [obj.volPit_m3;obj.presSP_bar];      % state                
                    obj.paramw      = [obj.flowIO_lpm;obj.betaperVol_barm3]; % unknown param
                    obj.paramp      = []; % fixed known param
                    obj.varxwQ(1,1) = (obj.volPitderxnoise_m3ps*obj.dt_s)^2; % Volpit
                    obj.varxwQ(2,2) = (obj.presSPderxnoise_barps*obj.dt_s)^2; % presPump
                    obj.varxwQ(3,3) = (obj.flowIOderwnoise_lpmps*obj.dt_s)^2; % flowIO
                    obj.varxwQ(4,4) = (obj.betaprVolderwnoise_barm3ps*obj.dt_s)^2; % beta/Vol
                    obj.varyR(1,1)  = obj.volPitynoise_m3^2;
                    obj.varyR(2,2)  = obj.flowOutynoise_lpm^2;                    
                    obj.varyR(3,3)  = obj.presSPynoise_bar^2;                    
                elseif obj.filteralt == 3,
                    obj.inputu      = [processData.flowIn_lpm; processData.flowOut_lpm];
                    obj.measy       = [processData.volGainLossWithoutOffset_m3;processData.flowOut_lpm;processData.presSP_bar];
                    obj.statex      = [obj.volPit_m3;obj.presSP_bar;obj.volFL_m3];      % state                
                    obj.paramw      = [obj.flowIO_lpm;obj.betaperVol_barm3;obj.kpit_lpmpm3;obj.kshaker_lpmpm3;obj.spaddle_lpmpc]; % unknown param
                    obj.paramp      = []; % fixed known param
                    obj.varxwQ(1,1) = (obj.volPitderxnoise_m3ps*obj.dt_s)^2;        % Volpit
                    obj.varxwQ(2,2) = (obj.presSPderxnoise_barps*obj.dt_s)^2;       % presPump
                    obj.varxwQ(3,3) = (obj.volFLderxnoise_m3ps*obj.dt_s)^2;         % VolFL
                    obj.varxwQ(4,4) = (obj.flowIOderwnoise_lpmps*obj.dt_s)^2;       % flowIO
                    obj.varxwQ(5,5) = (obj.betaprVolderwnoise_barm3ps*obj.dt_s)^2; % beta/Vol
                    obj.varxwQ(6,6) = (obj.kpitderwnoise_lpmpm3ps*obj.dt_s)^2;     % kpit
                    obj.varxwQ(7,7) = (obj.kshakerderwnoise_lpmpm3ps*obj.dt_s)^2;  % kshaker
                    obj.varxwQ(8,8) = (obj.spaddlederwnoise_lpmpcps*obj.dt_s)^2;   % spaddle
                    obj.varyR(1,1)  = obj.volPitynoise_m3^2;
                    obj.varyR(2,2)  = obj.flowOutynoise_lpm^2;                    
                    obj.varyR(3,3)  = obj.presSPynoise_bar^2;                    
                elseif obj.filteralt == 4,
                    obj.inputu      = [processData.flowIn_lpm];
                    obj.measy       = [processData.volGainLossWithoutOffset_m3;processData.flowOut_lpm;processData.presSP_bar];
                    obj.statex      = [obj.volPit_m3;obj.presSP_bar;obj.volFL_m3;obj.flowOutx_lpm];      % state                
                    obj.paramw      = [obj.flowIO_lpm;obj.betaperVol_barm3;obj.kpit_lpmpm3;obj.kshaker_lpmpm3;obj.spaddle_lpmpc;obj.Minv_barsplpm;obj.cfrac_barplpm]; % unknown param
                    obj.paramp      = []; % fixed known param
                    obj.varxwQ(1,1) = (obj.volPitderxnoise_m3ps*obj.dt_s)^2;    % Volpit
                    obj.varxwQ(2,2) = (obj.presSPderxnoise_barps*obj.dt_s)^2;   % presPump
                    obj.varxwQ(3,3) = (obj.volFLderxnoise_m3ps*obj.dt_s)^2;     % VolFL
                    obj.varxwQ(4,4) = (obj.flowOutderxnoise_lpmps*obj.dt_s)^2;  % FlowOut
                    obj.varxwQ(5,5) = (obj.flowIOderwnoise_lpmps*obj.dt_s)^2;   % flowIO
                    obj.varxwQ(6,6) = (obj.betaprVolderwnoise_barm3ps*obj.dt_s)^2; % beta/Vol
                    obj.varxwQ(7,7) = (obj.kpitderwnoise_lpmpm3ps*obj.dt_s)^2;     % kpit
                    obj.varxwQ(8,8) = (obj.kshakerderwnoise_lpmpm3ps*obj.dt_s)^2;  % kshaker
                    obj.varxwQ(9,9) = (obj.spaddlederwnoise_lpmpcps*obj.dt_s)^2;   % spaddle
                    obj.varxwQ(10,10) = (obj.Minvderwnoise_barplpm*obj.dt_s)^2;    % Minv
                    obj.varxwQ(11,11) = (obj.cfracderwnoise_barplpmps*obj.dt_s)^2; % cfrac
                    obj.varyR(1,1)  = obj.volPitynoise_m3^2;
                    obj.varyR(2,2)  = obj.flowOutynoise_lpm^2;                    
                    obj.varyR(3,3)  = obj.presSPynoise_bar^2;                    
                elseif obj.filteralt == 5,
                    obj.inputu      = [processData.flowIn_lpm; processData.flowOut_lpm];
                    obj.measy       = [processData.volGainLossWithoutOffset_m3;processData.flowOut_lpm;processData.presSP_bar;processData.volTripTank_m3];
                    obj.statex      = [obj.volPit_m3;obj.presSP_bar;obj.volPit_m3];      % state                
                    obj.paramw      = [obj.flowIO_lpm;obj.betaperVol_barm3;obj.flowTT_lpm;obj.areaDP_m2]; % unknown param
                    obj.paramp      = [obj.valveTT_01; processData.velBlock_mph*hourTos]; % known param
                    obj.varxwQ(1,1) = (obj.volPitderxnoise_m3ps*obj.dt_s)^2; % Volpit
                    obj.varxwQ(2,2) = (obj.presSPderxnoise_barps*obj.dt_s)^2; % presPump
                    obj.varxwQ(3,3) = (obj.volTTderxnoise_m3ps*obj.dt_s)^2; % VolTT
                    obj.varxwQ(4,4) = (obj.flowIOderwnoise_lpmps*obj.dt_s)^2; % flowIO
                    obj.varxwQ(5,5) = (obj.betaprVolderwnoise_barm3ps*obj.dt_s)^2; % beta/Vol
                    obj.varxwQ(6,6) = (obj.flowTTderwnoise_lpmps*obj.dt_s)^2; % flowIO
                    obj.varxwQ(7,7) = (obj.areaDPderwnoise_m2ps*obj.dt_s)^2; % flowIO
                    obj.varyR(1,1)  = obj.volPitynoise_m3^2;
                    obj.varyR(2,2)  = obj.flowOutynoise_lpm^2;                    
                    obj.varyR(3,3)  = obj.presSPynoise_bar^2;                    
                    obj.varyR(4,4)  = obj.volTTynoise_m3^2;
                    obj.velBlock_mps=processData.velBlock_mph*hourTos; 
                end
                
                if (sysState.currentState == DrillingStates.Tripping || sysState.currentState == DrillingStates.FlowCheck),
                    if processData.volTripTank_m3 == -0.99925
                        obj.measy(1)    = 0;
                    else
                        obj.alarmStatus = ['Tripping, vol = ' num2str(processData.volTripTank_m3,'%.2f') ' m3'];
                    end
                    obj.valveTT_01 = 0; % Tripping/flow check
                else
                    obj.valveTT_01 = 1; % Not tripping/flow check 
                end
                
                 if (not(obj.filteralt == 5) && (sysState.previousState == DrillingStates.Tripping || sysState.previousState == DrillingStates.FlowCheck)...
                                          && not(sysState.currentState  == DrillingStates.Tripping || sysState.currentState  == DrillingStates.FlowCheck)),%reset
                     obj.alarmStatus = ['Reset after tripping, time = ', num2str(obj.timeprev_d*dayTos/60,'%.2f') ' min'];
                     %TO DO: reset with old state and param vector or increase uncertainty on vector by modifying Q/R matrices
                     obj            = initialize(obj,obj.params);
                 end        
                %UKF
                xw              = [obj.statex;obj.paramw];%Stack state and parameters to be estimated in one vector
                
                if (sum(isnan(obj.measy)) + sum(isnan(obj.inputu)) > 0),
                        obj.alarmState  = AlarmState.InputError; % Bad meas
                        obj.alarmStatus = ['Bad meas, UKF not running, time = ', num2str(obj.timeprev_d*dayTos/60,'%.2f') ' min'];                    
                elseif (obj.valveTT_01 == 1) %Normal: drilling, connection
                    [xw,obj.varxwP] = ukf(obj.edyn,obj.paramp,xw,obj.varxwP,obj.varxwQ,obj.hmeas,obj.measy,obj.varyR,obj.inputu,dt_s);                    
                    obj             = obj.setAlarmState();
                else %Tripping/flow check
                    if (obj.filteralt == 5), 
                        [xw,obj.varxwP] = ukf(obj.edyn,obj.paramp,xw,obj.varxwP,obj.varxwQ,obj.hmeas,obj.measy,obj.varyR,obj.inputu,dt_s);
                        obj             = obj.setAlarmState();
                    else
                        obj.alarmState  = AlarmState.InputError; % Bad
                        obj.alarmStatus = ['UKF not running, time = ', num2str(obj.timeprev_d*dayTos/60,'%.2f') ' min'];
                    end
                end
                
                obj.statex      = xw(1:length(obj.statex));
                obj.paramw      = xw(length(obj.statex)+1:length(obj.statex)+length(obj.paramw)); 
                if obj.filteralt == 1,
                    obj.volPit_m3          = obj.statex(1);
                    obj.flowIO_lpm         = obj.paramw(1);
                elseif obj.filteralt == 2,
                    obj.volPit_m3          = obj.statex(1);
                    obj.presSP_bar         = obj.statex(2);
                    obj.flowIO_lpm         = obj.paramw(1);
                    obj.betaperVol_barm3   = obj.paramw(2);
                    %Constrain values on states and parameters
                    obj.presSP_bar         = max(0, obj.presSP_bar);
                    obj.betaperVol_barm3   = max(obj.betaperVol_barm3_min, min(obj.betaperVol_barm3_max, obj.betaperVol_barm3));
                elseif obj.filteralt == 3,
                    obj.volPit_m3          = obj.statex(1);
                    obj.presSP_bar         = obj.statex(2);
                    obj.volFL_m3           = obj.statex(3);
                    obj.flowIO_lpm         = obj.paramw(1);
                    obj.betaperVol_barm3   = obj.paramw(2);
                    obj.kpit_lpmpm3        = obj.paramw(3);
                    obj.kshaker_lpmpm3     = obj.paramw(4);
                    obj.spaddle_lpmpc      = obj.paramw(5);
                    %Constrain values on states and parameters
                    obj.presSP_bar         = max(0, obj.presSP_bar);
                    obj.volFL_m3           = max(0, obj.volFL_m3);
                    obj.betaperVol_barm3   = max(obj.betaperVol_barm3_min, min(obj.betaperVol_barm3_max, obj.betaperVol_barm3));
                    obj.kpit_lpmpm3        = max(10, min(2000, obj.kpit_lpmpm3));
                    obj.kshaker_lpmpm3     = max(0, min(2000, obj.kshaker_lpmpm3));
                    obj.spaddle_lpmpc      = max(1, min(1, obj.spaddle_lpmpc));
                    obj.flowPit_lpm        = obj.volFL_m3*obj.kpit_lpmpm3;
                    obj.flowShaker_lpm     = obj.volFL_m3*obj.kshaker_lpmpm3;
                elseif obj.filteralt == 4,
                    obj.volPit_m3          = obj.statex(1);
                    obj.presSP_bar         = obj.statex(2);
                    obj.volFL_m3           = obj.statex(3);
                    obj.flowOutx_lpm       = obj.statex(4);
                    obj.flowIO_lpm         = obj.paramw(1);
                    obj.betaperVol_barm3   = obj.paramw(2);
                    obj.kpit_lpmpm3        = obj.paramw(3);
                    obj.kshaker_lpmpm3     = obj.paramw(4);
                    obj.spaddle_lpmpc      = obj.paramw(5);
                    obj.Minv_barsplpm      = obj.paramw(6);
                    obj.cfrac_barplpm      = obj.paramw(7);
                    %Constrain values on states and parameters
                    obj.presSP_bar         = max(0, obj.presSP_bar);
                    obj.volFL_m3           = max(0, obj.volFL_m3);
                    obj.flowOutx_lpm       = max(0, obj.flowOutx_lpm);
                    obj.betaperVol_barm3   = max(obj.betaperVol_barm3_min, min(obj.betaperVol_barm3_max, obj.betaperVol_barm3));
                    obj.kpit_lpmpm3        = max(10, min(2000, obj.kpit_lpmpm3));
                    obj.kshaker_lpmpm3     = max(0, min(2000, obj.kshaker_lpmpm3));
                    obj.spaddle_lpmpc      = max(1, min(1, obj.spaddle_lpmpc));
                    obj.Minv_barsplpm      = max(0.005, min(0.01, obj.Minv_barsplpm));                    
                    obj.cfrac_barplpm      = max(10/2000, min(200/2000, obj.cfrac_barplpm));
                    obj.flowPit_lpm        = obj.volFL_m3*obj.kpit_lpmpm3;
                    obj.flowShaker_lpm     = obj.volFL_m3*obj.kshaker_lpmpm3;
                    obj.presFric_bar       = obj.cfrac_barplpm*processData.flowIn_lpm;
                elseif obj.filteralt == 5,
                    obj.volPit_m3          = obj.statex(1);
                    obj.presSP_bar         = obj.statex(2);
                    obj.volTT_m3           = obj.statex(3);
                    obj.flowIO_lpm         = obj.paramw(1);
                    obj.betaperVol_barm3   = obj.paramw(2);
                    obj.flowTT_lpm         = obj.paramw(3);
                    obj.areaDP_m2          = obj.paramw(4);
                    %Constrain values on states and parameters
                    obj.presSP_bar         = max(0, obj.presSP_bar);
                    obj.betaperVol_barm3   = max(obj.betaperVol_barm3_min, min(obj.betaperVol_barm3_max, obj.betaperVol_barm3));
                    if obj.valveTT_01 == 1,
                        obj.flowTT_lpm         = 0;
                    else
                        obj.flowTT_lpm         = max(0, obj.flowTT_lpm);
                    end
                    obj.areaDP_m2          = max(0.001, min(0.005,obj.areaDP_m2));
                    obj.flowSteel_lpm      = obj.velBlock_mps*obj.areaDP_m2*60000;
                end
                
                if ( (obj.alarmState >= 0) && (abs(obj.flowIO_lpm) > obj.params.flowDeadband_lpm))
                    obj.volIO_m3      = obj.volIO_m3 + dt_s * obj.flowIO_lpm*lpmToM3ps;% vIo[m3] = dt[s] *qIo[lpm]
                else
                    obj.volIO_m3      = 0;
                end
            end
            if (not(strcmp(obj.alarmStatus,'Normal'))),
                if obj.debug,
                    disp(obj.alarmStatus);
                end
                obj.comments = [obj.comments;obj.alarmStatus];
            end
            [obj, result] = obj.returnResultStruct();
        end
        
        function  [obj, result ] = returnResultStruct(obj)
            result.volPit_m3            = obj.volPit_m3;
            result.flowIO_lpm           = obj.flowIO_lpm;
            result.volIO_m3             = obj.volIO_m3;                
            result.alarmState           = obj.alarmState;
            
            if obj.filteralt==2,
                result.presSP_bar           = obj.presSP_bar;
                result.betaperVol_barm3     = obj.betaperVol_barm3;                
            elseif obj.filteralt==3,
                result.presSP_bar           = obj.presSP_bar;
                result.volFL_m3             = obj.volFL_m3;
                result.betaperVol_barm3     = obj.betaperVol_barm3;                
                result.kpit_lpmpm3          = obj.kpit_lpmpm3; 
                result.kshaker_lpmpm3       = obj.kshaker_lpmpm3; 
                result.spaddle_lpmpc        = obj.spaddle_lpmpc; 
                result.flowPit_lpm          = obj.flowPit_lpm;
                result.flowShaker_lpm       = obj.flowShaker_lpm;
            elseif obj.filteralt==4, 
                result.presSP_bar           = obj.presSP_bar;
                result.volFL_m3             = obj.volFL_m3;
                result.flowOutx_lpm         = obj.flowOutx_lpm;
                result.betaperVol_barm3     = obj.betaperVol_barm3;                
                result.kpit_lpmpm3          = obj.kpit_lpmpm3; 
                result.kshaker_lpmpm3       = obj.kshaker_lpmpm3; 
                result.spaddle_lpmpc        = obj.spaddle_lpmpc; 
                result.Minv_barsplpm        = obj.Minv_barsplpm; 
                result.cfrac_barplpm        = obj.cfrac_barplpm; 
                result.flowPit_lpm          = obj.flowPit_lpm;
                result.flowShaker_lpm       = obj.flowShaker_lpm;
                result.presFric_bar         = obj.presFric_bar;                
            elseif obj.filteralt==5,
                result.presSP_bar           = obj.presSP_bar;
                result.volTT_m3             = obj.volTT_m3;
                result.betaperVol_barm3     = obj.betaperVol_barm3;                
                result.flowTT_lpm           = obj.flowTT_lpm;
                result.areaDP_m2            = obj.areaDP_m2;
                result.valveTT_01           = obj.valveTT_01;
                result.flowSteel_lpm        = obj.flowSteel_lpm;
                result.velBlock_mps         = obj.velBlock_mps;
            end
        end
    end
end
        
function [x,P] = ukf(f,p,x,P,Q,h,y,R,u,dt)% UKF   Unscented Kalman Filter for nonlinear dynamic systems
% for nonlinear dynamic system (for simplicity, noises are assumed as additive):
%           x_k+1 = f(p_k,x_k,u_k) + vp_k
%           y_k   = h(x_k) + vm_k
% where     process noise vp ~ N(0,Q), i.e. gaussian noise with covariance Q
%       measurement noise vm ~ N(0,R), i.e. gaussian noise with covariance R
% Inputs:   f: function handle for f(x,u,p,dt)
%           x: "a priori" state estimate
%           p: known parameters
%           w: unknow parameters (to be estimated)
%           P: "a priori" estimated state covariance
%           h: function handle for h(x,u,w)
%           y: current measurement
%           Q: process noise covariance 
%           R: measurement noise covariance
% Output:   x: "a posteriori" state estimate
%           P: "a posteriori" state covariance
    L               = numel(x);  %number of states
    m               = numel(y);  %number of measurements
    alpha           = 1e-3;      %default, tunable
    ki              = 0;         %default, tunable
    beta            = 2;         %default, tunable
    lambda          = alpha^2*(L+ki)-L; %scaling factor
    c               = L+lambda;         %scaling factor
    Wm              = [lambda/c 0.5/c+zeros(1,2*L)];  %weights for means
    Wc              = Wm;
    Wc(1)           = Wc(1)+(1-alpha^2+beta);         %weights for covariance
    c               = sqrt(c);
    X               = sigmas(x,P,c);                  %sigma points around xw
    [x1,X1,P1,X2]   = ut_state(f,p,u,X,Wm,Wc,L,Q,dt);  %unscented transformation of process
    % X1=sigmas(x1,P1,c);                       %sigma points around x1
    % X2=X1-x1(:,ones(1,size(X1,2)));           %deviation of X1
    [z1,Z1,P2,Z2]   = ut_meas(h,p,u,X1,Wm,Wc,m,R);       %unscented transformation of measurments
    P12=X2*diag(Wc)*Z2';                        %transformed cross-covariance
    K=P12/P2;
    x=x1+K*(y-z1);                              %state update
    P=P1-P12*K';  
end

function [y,Y,P,Y1]=ut_state(f,p,u,X,Wm,Wc,m,R,dt)
%Unscented Transformation
%Input:
%        f: nonlinear map
%        p: parameter struct
%        u: input vector
%        X: sigma points
%       Wm: weights for mean
%       Wc: weights for covraiance
%        m: number of outputs of f
%        R: additive covariance
%Output:
%        y: transformed mean
%        Y: transformed smapling points
%        P: transformed covariance
%       Y1: transformed deviations

    L = size(X,2);
    y = zeros(m,1);
    Y = zeros(m,L);
    for k=1:L,
        Y(:,k)=f(X(:,k),u,p,dt);
        y = y + Wm(k)*Y(:,k);       
    end
    Y1  = Y - y(:,ones(1,L));
    P   = Y1*diag(Wc)*Y1'+R;          
end

function [y,Y,P,Y1]=ut_meas(h,p,u,X,Wm,Wc,m,R)
%Unscented Transformation
%Input:
%    hmeas: nonlinear map of outputs
%        p: parameter struct
%        X: sigma points
%       Wm: weights for mean
%       Wc: weights for covraiance
%        m: numer of outputs of f
%        R: additive covariance
%Output:
%        y: transformed mean
%        Y: transformed smapling points
%        P: transformed covariance
%       Y1: transformed deviations

    L = size(X,2);
    y = zeros(m,1);
    Y = zeros(m,L);
    for k=1:L                   
        Y(:,k)  = h(X(:,k),u,p);
        y       = y + Wm(k)*Y(:,k);       
    end
    Y1 = Y - y(:,ones(1,L));
    P  = Y1*diag(Wc)*Y1'+R;          
end

function X = sigmas(x,P,c)
%Sigma points around reference point
%Inputs:
%       x: reference point
%       P: covariance
%       c: coefficient
%Output:
%       X: Sigma points
    A = c*chol(P)';
    Y = x(:,ones(1,numel(x)));
    X = [x Y+A Y-A]; 
end
