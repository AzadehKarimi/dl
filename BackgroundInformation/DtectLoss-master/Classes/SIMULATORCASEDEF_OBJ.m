classdef SIMULATORCASEDEF_OBJ
    %Define case for simulation
    
    properties
        g_ms2               = 9.81;
        beta_bar            = 15000;%15000
        Tref_s              = 10;   %s smoothen reference
        MWsteel_sg          = 7.859;%steel density sg
        Es_Pa               = 200e9;%Pa Youngs modulus elasticity 
        MWmud_sg            = 1.31;
        PVmud_cP            = 17;%17;
        YPmud_lb100sqft     = 23;%23;
        tauymud_Pa          = 7;%7
        alpha_fl            = 1; %0.4; % To calculate flow from flow line to pit
        IDCs_in             = 8.5;%10.5;%in
        ODdp_in             = 5.5;%in
        IDdp_in             = 4.778;%in
        tune_delay_wob_s    = 10;
        tune_delay_pos_s    = 0.5;
        shakerloss_frac     = 0.005; %Losing 10 lpm over shakers when pumping 2000 lpm 

        % Init states 
        holeDepth_m      = 3000;%3000
        blockPos_m       = 2;
        q_drilling_lpm   = 2000;
        q_gelbreak_lpm   = 500;
        timegelbreak_s   = 60;
        q_tripping_lpm   = 1200;% trip tank pump flowrate
        V_fl_m3          = 2;  % inital volume in the flowline during drilling(used to calculate K_fl_m3_per_lpm)
        V_pit_m3         = 5;
        V_tt_m3          = 3; % partially full trip tank
        
        rotation_rpm     = 150;% drill pipe rotation
        rop_mph          = 30; % nominal rop
        F_wob_ton        = 4;  % nominal wob
        dp_bit_bar       = 30; % friction loss bit at nominal flow

        cdrag_ton_mph    = 30/(50*60);%30 ton drag when tripping 50m/min
        vel_max_trip_mph = 10*60;%m/hr max trip vel
        vel_max_rop_mph  = 50;%m/hr max rop
        
        pcutdot          = 10/sqrt(3600);%10 bar/h =? 10/3600 bar/s
        
        D_ann_m         ;
        D_dpInner_m     ;
        D_dpOuter_m     ;
        Aidp_m2         ;
        Aann_m2         ;
        Asdpopen_m2     ;
        Asdpclosed_m2   ;
        Vidp_m3         ;
        Vann_m3         ;
        Vtot_m3         ;
        Mdp_kgpm4       ;
        Mann_kgpm4      ;
        Mtot_kgpm4      ;
        cbit_bar_lpm2   ;
        d_ton_mph       ;
        ks_ton          ;
        mdry_ton        ;
        mwet_ton        ;
        kpit_lpm_m3     ;
        kshaker_lpm_m3  ;
        dbdV_barm3      ;
        Twob_s          ;
        Tpos_s          ;
        Tcwob_s         ;
        Tcpos_s         ;
        Kcwob           ;
        Tiwob_s         ;
        Kcpos           ;
        Tipos_s         ; 

        
        NumStands;
        simulationDuration_s;
        StateVector=[...,
            SimDrillingStates.Drilling,...      %1
            SimDrillingStates.PullOffBottom,... %2
            SimDrillingStates.StopRotation,...  %3
            SimDrillingStates.StopPumps,...     %4
            SimDrillingStates.DrillAddStand,... %5
            SimDrillingStates.StartPumps,...    %6
            SimDrillingStates.StartRotation,... %7
            SimDrillingStates.GoOnBottom,...    %8
            SimDrillingStates.Drilling,...      %9
            SimDrillingStates.PullOffBottom,... %10
            SimDrillingStates.StopRotation,...  %11
            SimDrillingStates.StopPumps,...     %12
            SimDrillingStates.DrillAddStand,... %5
            SimDrillingStates.StartPumps,...    %6
            SimDrillingStates.StartRotation,... %7
            SimDrillingStates.GoOnBottom,...    %8
            SimDrillingStates.Drilling,...      %9
            SimDrillingStates.PullOffBottom,... %10
            SimDrillingStates.StopRotation,...  %11
            SimDrillingStates.StopPumps,...     %12
            SimDrillingStates.Wait,...          %13
            SimDrillingStates.Pull1Stand,...    %14
            SimDrillingStates.TripRemoveStand,... %15
            SimDrillingStates.Pull1Stand,...    %16
            SimDrillingStates.TripRemoveStand,... %17
            SimDrillingStates.Pull1Stand,...    %18
            SimDrillingStates.TripRemoveStand,... %19
            SimDrillingStates.Pull1Stand,...    %20
            SimDrillingStates.TripRemoveStand,... %21
            SimDrillingStates.Pull1Stand,...    %22
            SimDrillingStates.FlowCheck,...     %23
            SimDrillingStates.Run1Stand,...     %24
            SimDrillingStates.TripAddStand,...  %25
            SimDrillingStates.Run1Stand,...     %26
            SimDrillingStates.TripAddStand,...  %27
            SimDrillingStates.Run1Stand,...     %28
            SimDrillingStates.TripAddStand,...  %29
            SimDrillingStates.Run1Stand,...     %30
            SimDrillingStates.TripAddStand,...  %31
            SimDrillingStates.Run1Stand,...     %32
            SimDrillingStates.FlowCheck,...     %33
            SimDrillingStates.EndSimulation...  %34
            ];

        
        InfluxVecTime_s;
        InfluxVecRate_lpm;
        DeltaQinVecTime_s;
        DeltaQinVecRate_lpm;
        StepPitTime_s;
        StepPitDV_m3;
        PaddleVec_lpm;
        PaddleVec_pc;
        q_out_noise_lpm;
        qpaddle_accuracy_pc;% +-5% accuracy
        drillState;%StateVecto
    end
    
    methods
        
        function obj                = initialize(obj)%Constructor
            obj.NumStands            = 10;
            obj.simulationDuration_s = 900*60;%900*60;
            obj.InfluxVecTime_s      = [580 586; 660 672; 720 735; 765 769; 800 808; 830 842]*60;
            obj.InfluxVecRate_lpm    = [ -400;    300;      +100;     700;     300;     -200];
%             obj.DeltaQinVecTime_s    = [90 100; 110 120; 160 180; 230 250; 300 315; 380 390]*60;
%             obj.DeltaQinVecRate_lpm  = [  400;    200;    800;     400;     -300;     500];
            obj.DeltaQinVecTime_s    = [50 80; 90 100; 110 120; 160 180; 230 250; 300 315; 380 390]*60;
            obj.DeltaQinVecRate_lpm  = [-400;  400;    200;    800;     400;     -300;     500];
            obj.StepPitTime_s        = 700*60;
            obj.StepPitDV_m3         = 5;
            obj.PaddleVec_lpm        = [0 400 1000 3000 5000 8000];
            obj.PaddleVec_pc         = [0   0   24   83  100  100];
            obj.q_out_noise_lpm      = 50;
            obj.qpaddle_accuracy_pc  = 5;% +-5% accuracy
            obj.drillState           = SimDrillingStates.Drilling;%StateVector;%

            %Const calc
            obj.D_ann_m             = obj.IDCs_in*0.0254;
            obj.D_dpInner_m         = obj.IDdp_in*0.0254;
            obj.D_dpOuter_m         = obj.ODdp_in*0.0254;          
            obj.Aidp_m2             = pi/4*obj.D_dpInner_m^2;
            obj.Aann_m2             = pi/4*(obj.D_ann_m^2-obj.D_dpOuter_m^2);%Annulus area
            obj.Asdpopen_m2         = pi/4*(obj.D_dpOuter_m^2-obj.D_dpInner_m^2);%drillpipe cross section area open end
            obj.Asdpclosed_m2       = pi/4*(obj.D_dpOuter_m^2);%drillpipe cross section area closed end
            obj.Vidp_m3             = obj.Aidp_m2*obj.holeDepth_m;
            obj.Vann_m3             = obj.Aann_m2*obj.holeDepth_m;
            obj.Vtot_m3             = (obj.Aidp_m2 + obj.Aann_m2)*obj.holeDepth_m ;
            obj.Mdp_kgpm4           = obj.MWmud_sg*1000*obj.holeDepth_m/obj.Aidp_m2;%"mass" mud in dp for momentum balance
            obj.Mann_kgpm4          = obj.MWmud_sg*1000*obj.holeDepth_m/obj.Aann_m2;
            obj.Mtot_kgpm4          = obj.holeDepth_m*obj.MWmud_sg*1000*(1/obj.Aidp_m2+1/obj.Aann_m2);
            obj.cbit_bar_lpm2       = obj.dp_bit_bar/obj.q_drilling_lpm^2;%bar/lpm^2        
            obj.d_ton_mph           = obj.F_wob_ton/obj.rop_mph;%Constant for linear relation rop vs wob
            obj.ks_ton              = obj.Es_Pa*obj.Asdpopen_m2/obj.g_ms2/1000;%ton string constant ton Youngs modulus steel 200 GPa
            obj.mdry_ton            = obj.holeDepth_m*obj.Asdpopen_m2*obj.MWsteel_sg;%dry weight of drill pipe (steel)
            obj.mwet_ton            = obj.holeDepth_m*obj.Asdpopen_m2*(obj.MWsteel_sg-obj.MWmud_sg);%wet weight of drill pipe = dry weight - buoyancy
            k                       = obj.q_drilling_lpm/obj.V_fl_m3^obj.alpha_fl;
            obj.kpit_lpm_m3         = k*(1-obj.shakerloss_frac);
            obj.kshaker_lpm_m3      = k*obj.shakerloss_frac;
            
            obj.dbdV_barm3          = obj.beta_bar/obj.Vtot_m3;
            obj.Twob_s              = (obj.d_ton_mph+obj.cdrag_ton_mph)*obj.holeDepth_m/(obj.ks_ton)*3600;% Open loop time constant wob control sec
            obj.Tpos_s              = obj.cdrag_ton_mph*obj.holeDepth_m/obj.ks_ton*3600;% Open loop time constant vel control sec
            obj.Tcwob_s             = 1*obj.Twob_s;% Closed loop time const wob control
            obj.Tcpos_s             = 0.2*obj.Tpos_s;% Closed loop time constant pos control

            %SIMC tuning rules
            obj.Kcwob               = 1/(obj.d_ton_mph+obj.cdrag_ton_mph)*obj.Twob_s/(obj.Tcwob_s+obj.tune_delay_wob_s);
            obj.Tiwob_s             = min(obj.Twob_s,4*(obj.Tcwob_s+obj.tune_delay_wob_s));
            obj.Kcpos               = 1/(obj.Tcpos_s+obj.tune_delay_pos_s);
            obj.Tipos_s             = 4*(obj.Tcpos_s+obj.tune_delay_pos_s);  
                    
            obj.StateVector=[];
            Drill1Stand=[...,
                SimDrillingStates.Drilling,...      %1
                SimDrillingStates.PullOffBottom,... %2
                SimDrillingStates.StopRotation,...  %3
                SimDrillingStates.StopPumps,...     %4
                SimDrillingStates.DrillAddStand,... %5
                SimDrillingStates.StartPumps,...    %6
                SimDrillingStates.StartRotation,... %7
                SimDrillingStates.GoOnBottom];      %8
           DrillAndWait=[
                SimDrillingStates.Drilling,...      %9
                SimDrillingStates.PullOffBottom,... %10
                SimDrillingStates.StopRotation,...  %11
                SimDrillingStates.StopPumps,...     %12
                SimDrillingStates.Wait];            %13
           for i=1:obj.NumStands
               obj.StateVector = [obj.StateVector, Drill1Stand];
           end
           obj.StateVector = [obj.StateVector, DrillAndWait];
           for i=1:obj.NumStands
               obj.StateVector = [obj.StateVector, ...
                   SimDrillingStates.Pull1Stand,...    %14
                   SimDrillingStates.TripRemoveStand];
           end
           obj.StateVector = [obj.StateVector, ...
                SimDrillingStates.Pull1Stand,...    %22
                SimDrillingStates.FlowCheck];       %23
           for i=1:obj.NumStands
               obj.StateVector = [obj.StateVector, ...
                   SimDrillingStates.Run1Stand,...   %24
                   SimDrillingStates.TripAddStand];  %25
           end
           obj.StateVector = [obj.StateVector, ...
                SimDrillingStates.Run1Stand,...     %32
                SimDrillingStates.FlowCheck,...     %33
                SimDrillingStates.EndSimulation...  %34
                ];
            end
        
        
    end
    
end

