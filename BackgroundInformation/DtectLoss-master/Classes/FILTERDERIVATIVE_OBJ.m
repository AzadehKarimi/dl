classdef FILTERDERIVATIVE_OBJ
% filter data and estimate time derivative

    properties (SetAccess = private)
        Tfilt_s;
        damping;
        timestepmax_s;
    end
       
  properties (SetAccess = public)
        prevtime_d;
        dt_s;
        previnp;
        prevout;
        prevoutder;
    end

    methods
        
        function obj   = FILTERDERIVATIVE_OBJ(Tfilt_s)
            obj.prevtime_d    = 0;
            obj.dt_s          = 0;
            obj.previnp       = 0;
            obj.prevout       = 0;
            obj.prevoutder    = 0;
            obj.Tfilt_s       = Tfilt_s;
            obj.damping       = 1;
            obj.timestepmax_s = 60;%Reset filter if timestep is bigger than this
         end

        function [obj, newout] = smoothderivative1storder(obj, newinp, newtime_d)
            obj.dt_s           = (newtime_d - obj.prevtime_d)*86400;
            if (isnan(newinp) || isnan(newtime_d) || obj.dt_s <= 0) %do nothing
                newout         =  obj.prevout;
            elseif obj.dt_s < obj.timestepmax_s
                newout         = (1-obj.dt_s/obj.Tfilt_s)*obj.prevout + (newinp-obj.previnp)/obj.Tfilt_s;
            else %reset
                newout         = 0;
            end
            if isnan(newout)
                newout = obj.prevout;
            end
            obj.prevtime_d = newtime_d;
            obj.prevout    = newout;
            obj.previnp    = newinp;
        end
        
        function [obj, newout] = smoothderivative2ndorder(obj, newinp, newtime_d)
            obj.dt_s           = (newtime_d - obj.prevtime_d)*86400;
            if (isnan(newinp) || isnan(newtime_d) || obj.dt_s <= 0) %do nothing
                newoutder      = obj.prevoutder;  
                newout         = obj.prevout;
            elseif obj.dt_s < obj.timestepmax_s 
                newoutder      = (1-2*obj.damping*obj.dt_s/obj.Tfilt_s)*obj.prevoutder + (newinp-obj.previnp)/obj.Tfilt_s^2-obj.dt_s/obj.Tfilt_s^2*obj.prevout;
                newout         = obj.prevout + obj.dt_s*obj.prevoutder;
            else %reset
                newoutder      = 0;
                newout         = 0;
            end
            if isnan(newout) || isnan (newoutder)
                newout         = obj.prevout;
                newoutder      = obj.prevoutder;
            end

            obj.prevtime_d = newtime_d;
            obj.prevoutder = newoutder;
            obj.prevout    = newout;
            obj.previnp    = newinp;            
        end
    end    
end

