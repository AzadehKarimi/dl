classdef PIECEWISELINFIT_OBJ
    % partition input data, train samples in each grid and estimate output by
    % interpolation
    
    properties (SetAccess = private)
        inputMax;
        inputMin;
        Ninput;
        Nfilt;
    end

    properties (SetAccess = public)
        sizeGrid;
        inputArr;
        outputArr;
        numArr;
        inputArrMin;
        inputArrMax;
        isTrained;
    end

    methods
        function obj   = PIECEWISELINFIT_OBJ(inputMax, inputMin, Ninput, Nfilt)
            obj.inputMax  = inputMax;
            obj.inputMin  = inputMin;
            obj.Ninput    = Ninput;
            obj.sizeGrid  = (inputMax - inputMin)/Ninput;
            obj.Nfilt     = Nfilt;
            obj.inputArr  = zeros(1, obj.Ninput);
            obj.outputArr = zeros(1, obj.Ninput);        
            obj.numArr    = zeros(1, obj.Ninput);
            obj.inputArrMin = -0.99925;
            obj.inputArrMax = -0.99925;  
            obj.isTrained   = false;
        end

        function obj   = initialize(obj)
            obj.inputArr  = zeros(1, obj.Ninput);
            obj.outputArr = zeros(1, obj.Ninput);        
            obj.numArr    = zeros(1, obj.Ninput);        
            obj.isTrained = false;
        end

        function obj = train(obj, newinput, newoutput)
            newinput = max(newinput, 0);
            index = floor((newinput)/obj.sizeGrid) + 1;    
            if index > 0 && index <= obj.Ninput
                obj.numArr(index)    = obj.numArr(index)    + 1;
                obj.inputArr(index)  = obj.inputArr(index)  + 1/min(obj.numArr(index), obj.Nfilt)*(newinput  - obj.inputArr(index));
                obj.outputArr(index) = obj.outputArr(index) + 1/min(obj.numArr(index), obj.Nfilt)*(newoutput - obj.outputArr(index));                
            else
                disp('PIECEWISELINFIT_OBJ:train - Error');
            end
            nonzeroind    = find(obj.numArr > 5);
            obj.isTrained = (length(nonzeroind) > 1);
        end

        function [obj, expout] = interpol(obj, newinput)
            nonzeroind = find(obj.numArr > 5);
            if length(nonzeroind) > 2 
                for k=2:length(nonzeroind)%make array monotonely increasing
                    if obj.outputArr(nonzeroind(k)) < obj.outputArr(nonzeroind(k-1))
                        %Use number of instants to weigh average
                        N1 = min(100, obj.numArr(nonzeroind(k)));
                        N2 = min(100, obj.numArr(nonzeroind(k-1)));
                        
                        obj.outputArr(nonzeroind(k))   = (N1*obj.outputArr(nonzeroind(k)) +  N2*obj.outputArr(nonzeroind(k-1)))/(N1+N2);
                        obj.outputArr(nonzeroind(k-1)) = obj.outputArr(nonzeroind(k));
                    end
                end
                obj.inputArrMin = min(obj.inputArr(nonzeroind));
                obj.inputArrMax = max(obj.inputArr(nonzeroind));            
                arrx   = obj.inputArr(nonzeroind);
                arry   = obj.outputArr(nonzeroind);
                if sum(arry) == 0
                    expout = -0.99925;
                else
                    expout = interp1(arrx, arry, newinput,'linear','extrap');%'spline'
                    if numel(expout) > 1
                        expout = expout(1);
                    end
                    expout = min(1.1*max(arry),expout);
                    expout = max(0.9*min(arry),expout);
                end        
                if isnan(expout)
                    expout = -0.99925;
                elseif expout<0
                    expout = 0;
                end
            else
                expout = -0.99925;                
                %disp('Not trained yet');
            end

        end    
    end

end
