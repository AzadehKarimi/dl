% PROCESS_OBJ
% is in charging of washing data: convert units, adjust measurements, filtering
% it processes a struct of rawData in and converts it according to the
% unit specifier into the standard units
%
% this class is intended for running in real-time and processes one
% vector of data each time it is called
%
% process obj also adjusts for pit volume offests and converts
% paddle measurements from percent to lpm.
%

classdef PROCESS_OBJ < DTECTLOSS_SUBMODULE
    
    properties (SetAccess = private)
        rawDataUnits; % units for the raw data
        rawDataStdUnit_lastGoodValue;
        defaultDataUnits; % default units mapped using the defaultUnitsMapFile
        defaultDataUnitsFactor; % factors used to multiply raw data to obtain values in default unit
        paddleConvObj;
        filtHookHeight;
        filtBitDepth;
        volGainLossPrev_m3; %  previous measurement (from raw data, not washed)
        volGainLossOffset_m3;

        lowPassFlowInObj;
        
        flowOutType;
        tripTankTag;
        pitVolTag;
        configCell;
        timeLast_d;
        timeCurrent_d;
        dt_s;
        isDownlinking;
        downlinkObj;
        presSPdelayObj;
        kalmanFilter;
        
        availableRequiredTags; % logical array to keep track of which required tags which are avaialble
        availableOptionalTags; % logical array to keep track of which optional tags which are avaialble
        availableRequiredUnits; % logical array to keep track of which required tags which have valid unit conversion factor
        availableOptionalUnits; % logical array to keep track of which optional tags which have valid unit conversion factor
        
        availableSongaValidTags; % logical array to keep track of which of the Songa specific valid tags which are available
        availableSongaValidUnits;% logical array to keep track of which of the Songa specific valid unit conversion factors that are valid
            
        availableSongaSelectorTags; % logical array to keep track of which of the Songa specific measurement selectors which are available
        availableSongaSelectorUnits; % logical array to keep track of which of the Songa measurement selectors which have a correct unit.
        
        availableSongaMeasTags; % logical array to keep track of which of the Songa specific measurements which are available
        availableSongaMeasUnits; % logical array to keep track of which of the Songa measurement which have a correct unit.
        
        runSongaPreProcess; % boolean which is set true if all necessary Songa tags are available. Otherwise false.
        
        songaMeasTagsWithUnits;
        songaValidTagsWithUnits;
        songaSelectorTagsWithUnits;
        
        nameSpace; % cell which holds the combination of available tags with units and optional tags with units
        nameSpaceWithUnits; % same as namespace but with unit appended, e.g. tag_unit - flowIn_lpm
        songaNameSpaceWithUnits; % same as nameSpaceWithUnits, but specific for the Songa Enabler rig
        undefinedTags; % cell containing the tags which are not included in the name space but in the list of required
                       % tags, and optional tags.
    end
    properties (GetAccess = public, SetAccess = private)
        params;
    end
    properties (Constant)
        HEIGHTHOOK_DISCONNECT_LIMIT_FOR_VELBLOCK_EST_M  =  9;
        DEPTHBIT_RESET_LIMIT_FOR_VELBIT_EST_M           = 50;
        
        DO_DEBUG_DISP								= false; % more verbose, but may slow down running of alg
        MISSING_VAL_CODE                            = -0.99925; % that value that used to represent "missing" or "NaN" data
        ISENABLED_DELAYED_PRESSP                    = true;
        % init of variables necessary for removal of sudden changes in gain/loss
        volGainLossThreshold_m3_per_time_step       = 3;  % [m3/time-step]
        TfiltHookHeight_s                           = 20; % Filter time constant for estimating block velocity from hook height
        TfiltBitDepth_s                             = 20; % Filter time constant for estimating bit velocity from bit depth
        timeSlack_s                                 = 10; % 5 Allow up to 5 sec difference in time stamp, set meas to -0.99925 if bigger
        
        % tag list
        requiredTags = {'flowIn', 'depthBit', 'depthHole'};
        requiredUnits = {'lpm', 'm', 'm'}; % corresponding default units
        
        optionalTags = {'flowOut', 'flowOut1', 'flowOutCoriolis', 'presSP', 'presSP1', 'volGainLoss', 'volPitDrill', 'volTripTank', 'volTripTank2', 'heightHook', ...
            'rpm', 'torque', 'weightOnBit', 'rop'};
        optionalUnits = {'lpm', 'lpm', 'lpm', 'bar', 'bar', 'm3', 'm3', 'm3', 'm3', 'm', '', 'kNm', 'kN', 'mph'}; % corresponding default units
        
        calculatedTags = {'velBlock', 'velBit', 'velRop'};
        calculatedUnits = {'mph', 'mph', 'mph'}; % corresponding default units
        
        songaMeasTags = {...
            'volGainLoss',...
            'volPitDrill',...
            'depthBit',...
            'rpm',...
            'velBlock',...
            'depthHole',...
            'flowIn',...
            'flowOut',...
            'flowOut1',...
            'rop',...
            'presSP',...
            'presSP1',...
            'volTripTank',...
            };
        songaMeasUnits = {...
            'm3', 'm3', 'm', 'rpm', 'mph', 'm',...
            'lpm', 'lpm', 'lpm', 'mph', 'bar', 'bar', 'm3',...
            };
        
        songaValidTags = {...
            'validvolPitDrill',...
            'validdepthBit',...
            'validrpm',...
            'validvelBlock',...
            'validdepthHole',...
            'validflowIn',...
            'validflowOut',...
            'validflowOut1',...
            'validrop',...
            'validpresSP',...
            'validpresSP1',...
            'validvolTripTank',...
            };
        songaValidUnits = {'boolean', 'boolean', 'boolean', 'boolean', 'boolean', 'boolean',...
            'boolean', 'boolean', 'boolean', 'boolean', 'boolean', 'boolean'};
        
        songaSelectorTags = {'selectflowout', 'selectpresSP'};
        songaSelectorUnits = {'boolean', 'boolean'};
    end
    
    methods
        function obj                    = PROCESS_OBJ(params, initTime_d)
            obj.params                  = params;
            obj.downlinkObj             = DOWNLINK_DETECT();
            
            obj.paddleConvObj           = PADDLE_CONVERTER_OBJ();
            obj.paddleConvObj           = obj.paddleConvObj.initalize(params.paddle);
            
            obj.filtHookHeight          = FILTERDERIVATIVE_OBJ(obj.TfiltHookHeight_s);
            obj.filtBitDepth            = FILTERDERIVATIVE_OBJ(obj.TfiltBitDepth_s);
            
            obj.lowPassFlowInObj        = LOWPASS_OBJ(1, params.voting.flowInLowpassTc_s);   
            
            obj.presSPdelayObj          = STANDPIPE_PRESSURE_DELAY_OBJ(params);

            obj.volGainLossPrev_m3      = 0;
            obj.volGainLossOffset_m3    = 0;
            obj.defaultDataUnits        = [];
            obj.defaultDataUnitsFactor  = struct();
            obj.timeLast_d              = initTime_d;
            obj.timeCurrent_d           = initTime_d;
            obj.dt_s                    = 0;
            
            obj.kalmanFilter            = HYBRID_KALMAN_FILTER();
            obj.kalmanFilter            = obj.kalmanFilter.initialize(params.pitvol.flowstd_R_KF_lpm^2, 0, params.pitvol.flowstd_P0_KF_lpm^2);
            
            obj.availableRequiredTags   = false(size(obj.requiredTags));
            obj.availableRequiredUnits  = false(size(obj.requiredTags));
            
            obj.availableOptionalTags   = false(size(obj.optionalTags));
            obj.availableOptionalUnits  = false(size(obj.optionalTags));
            
            obj.availableSongaValidTags   = false(size(obj.songaValidTags));
            obj.availableSongaValidUnits  = false(size(obj.songaValidTags));
            
            obj.availableSongaSelectorTags   = false(size(obj.songaSelectorTags));
            obj.availableSongaSelectorUnits  = false(size(obj.songaSelectorTags));
            
            obj.availableSongaMeasTags   = false(size(obj.songaMeasTags));
            obj.availableSongaMeasUnits  = false(size(obj.songaMeasTags));
            
            obj.runSongaPreProcess = false;
            
            obj.songaMeasTagsWithUnits = concatTagAndUnit(obj.songaMeasTags, obj.songaMeasUnits);
            obj.songaValidTagsWithUnits = concatTagAndUnit(obj.songaValidTags, obj.songaValidUnits);
            obj.songaSelectorTagsWithUnits = concatTagAndUnit(obj.songaSelectorTags, obj.songaSelectorUnits);
            
            obj.nameSpace               = cell(0);
            obj.nameSpaceWithUnits      = cell(0);
            obj.songaNameSpaceWithUnits = cell(0);
            obj.undefinedTags           = cell(0);
            
            function tagAndUnit = concatTagAndUnit(tagList, unitList)
                tagAndUnit = cell(size(tagList));
                
                nTags = numel(tagList);
                for ii = 1:nTags
                    tagAndUnit{ii} = strcat(tagList{ii}, '_', unitList{ii});
                end
            end
        end
        
        function obj = softReset(obj)
            obj.kalmanFilter = obj.kalmanFilter.softReset();
            obj.paddleConvObj = obj.paddleConvObj.softReset();
            obj.presSPdelayObj = obj.presSPdelayObj.softReset();
        end
        
        function obj = setAvailableTags(obj, rawDataUnits)
            % indicate in the logical array if the tag is available
            
            fN = fieldnames(rawDataUnits);
            nF = numel(fN);
            
            obj.availableRequiredTags = loopThroughTagsAndMarkAvailable(obj.requiredTags);
            obj.availableOptionalTags = loopThroughTagsAndMarkAvailable(obj.optionalTags);
            obj.availableSongaMeasTags = loopThroughTagsAndMarkAvailable(obj.songaMeasTags);
            obj.availableSongaValidTags = loopThroughTagsAndMarkAvailable(obj.songaValidTags);
            obj.availableSongaSelectorTags = loopThroughTagsAndMarkAvailable(obj.songaSelectorTags);

            if all(obj.availableSongaValidTags) && all(obj.availableSongaSelectorTags)
                if all(obj.availableSongaMeasTags)
                    obj.runSongaPreProcess = true;
                else
                    error('PROCESS_OBJ:missingSongaMeasTags',...
                    'The necessary measurement tags have not been defined.');
                end
            elseif all(~obj.availableSongaValidTags)...
                    && all(~obj.availableSongaSelectorTags)
                % no Songa specific tags seems to be defined.
                return
            else
                error('PROCESS_OBJ:onlySomeSongaTagsDefined',...
                    'The necessary tags have not been defined correctly.');
            end
            
            function availableTags = loopThroughTagsAndMarkAvailable(tagList)
                nTags = numel(tagList);
                availableTags = false(size(tagList));
                for ii=1:nTags % loop through tags
                    for jj=1:nF % loop through tags in raw data units
                        if strcmpi(tagList{ii}, fN{jj})
                            % mark as true if found
                            availableTags(ii)=true;
                            continue
                        end
                    end
                end
            end
        end
        
        function obj = defineNameSpace(obj)
            % define the namespace as tags which are available with a
            % defined unit conversion
            
            if ~any(obj.availableRequiredTags & obj.availableRequiredUnits)
                % return with empty name space if no valid combination of
                % required tags and units
                return
            end
            
            requiredTagsWithUnits = obj.requiredTags(obj.availableRequiredTags & obj.availableRequiredUnits);
            optionalTagsWithUnits = obj.optionalTags(obj.availableOptionalTags & obj.availableOptionalUnits);
            
            % in case the flowOut tag is available but has a undefined
            % unit, make sure it is included in the namespace anyway. It
            % will be converted to lpm later. Put the remainder of the
            % optional and required tags in undefinedTags
            idxArrayFlow         = strcmp(obj.optionalTags, 'flowOut');
            idxFlowOut           = find(idxArrayFlow,1);
            idxArrayTripTank     = strcmp(obj.optionalTags, 'volTripTank');
            idxTripTank          = find(idxArrayTripTank,1);
            missingRequiredTags  = obj.requiredTags(not(obj.availableRequiredTags & obj.availableRequiredUnits));
            missingRequiredUnits = obj.requiredUnits(not(obj.availableRequiredTags & obj.availableRequiredUnits));
            
            if obj.availableOptionalTags(idxFlowOut) && ~obj.availableOptionalUnits(idxFlowOut)
                obj.nameSpace        = [requiredTagsWithUnits optionalTagsWithUnits 'flowOut']';
                missingOptionalTags  = obj.optionalTags(not(obj.availableOptionalTags...
                    & obj.availableOptionalUnits) & not(idxArrayFlow));
                missingOptionalUnits = obj.optionalUnits(not(obj.availableOptionalTags...
                    & obj.availableOptionalUnits) & not(idxArrayFlow));
            elseif obj.availableOptionalTags(idxTripTank) && ~obj.availableOptionalUnits(idxTripTank)
                obj.nameSpace        = [requiredTagsWithUnits optionalTagsWithUnits 'volTripTank']';
                missingOptionalTags  = obj.optionalTags(not(obj.availableOptionalTags...
                    & obj.availableOptionalUnits) & not(idxTripTank));
                missingOptionalUnits = obj.optionalUnits(not(obj.availableOptionalTags...
                    & obj.availableOptionalUnits) & not(idxTripTank));
            else
                obj.nameSpace        = [requiredTagsWithUnits optionalTagsWithUnits]';
                missingOptionalTags  = obj.optionalTags(not(obj.availableOptionalTags & obj.availableOptionalUnits));
                missingOptionalUnits = obj.optionalUnits(not(obj.availableOptionalTags...
                    & obj.availableOptionalUnits));
            end
            
            obj.nameSpaceWithUnits = appendUnits(obj.nameSpace, obj.defaultDataUnits);
            if obj.runSongaPreProcess
                allSongaTags = [obj.songaMeasTags, obj.songaValidTags, obj.songaSelectorTags];
                obj.songaNameSpaceWithUnits = appendUnits(allSongaTags, obj.defaultDataUnits);
            end
                        
            % fill cell with undefined/complement tags
            undefTags = [missingRequiredTags missingOptionalTags]';
            undefUnits = [missingRequiredUnits missingOptionalUnits]';
            obj.undefinedTags = cell(size(undefTags));
            for ii = 1:size(undefTags,1)
                if ~isempty(undefUnits{ii})
                    obj.undefinedTags{ii} = strcat(undefTags{ii}, '_', undefUnits{ii});
                else
                    obj.undefinedTags{ii} = undefTags{ii};
                end
            end
            
            
            function tagListWithUnits = appendUnits(tagList, unitList)
                tagListWithUnits = cell(size(tagList));
                for kk = 1:numel(tagList)
                    tag = tagList{kk};
                    if strcmpi(tag, 'rpm')
                        % no unit for rpm
                        tagListWithUnits{kk} = tag;
                    else
                        tagListWithUnits{kk} = strcat(tag, '_', unitList.(tag));
                    end
                end
            end
        end
        
        function obj = specifyUnits(obj, rawDataUnits)
            % specify the unit conversion factor for each tag.
            
            obj.rawDataUnits   = rawDataUnits;
            % read default unit map from file
            defUnitMap         = generateDefaultUnitsMap;
            varNamesInMap      = defUnitMap.Properties.RowNames;
            nVarNamesInMap     = size(varNamesInMap, 1);
            fN                 = fieldnames(rawDataUnits);
            defaultUnitDefined = false(size(fN));
            unitConvFactorDefined = false(size(fN));
            for ii=1:numel(fN)
                for jj=2:nVarNamesInMap
                    if strcmpi(fN{ii}, varNamesInMap{jj})
                        defaultUnitDefined(ii) = true;
                        [obj.defaultDataUnitsFactor.(fN{ii}), hasFoundConversionFactor] =...
                            findUnit(rawDataUnits.(fN{ii}), defUnitMap,jj);
                        if hasFoundConversionFactor
                            unitConvFactorDefined(ii) = true;
                        end
                        obj.defaultDataUnits.(fN{ii}) = strrep(defUnitMap{jj, 1}{1}, '/', 'p');
                        continue
                    end
                end
            end
            if ~all(defaultUnitDefined)
                warning('PROCESS_OBJ:variableNotInUnitMap',...
                    'The variable %s has no default unit. The factor 1 will be used for conversion.\n',...
                    fN{~defaultUnitDefined})
            end
            
            % loop through required and optional tags to check that all
            % have defined a unit conversion factor
            obj.availableRequiredUnits = loopThroughUnitsAndSetAvailable(obj.requiredTags);
            obj.availableOptionalUnits = loopThroughUnitsAndSetAvailable(obj.optionalTags);

            function availableUnit = loopThroughUnitsAndSetAvailable(tagList)
                availableUnit = false(size(tagList));
                for ll = 1:numel(fN)
                    for mm = 1:numel(tagList)
                        if strcmpi(fN{ll}, tagList{mm})
                            if unitConvFactorDefined(ll)
                                availableUnit(mm) = true;
                                continue
                            end
                        end
                    end
                end
            end
            
            function [convFactor, hasFoundConversionFactor] = findUnit(unitToLookUp, unitMap, rowNum)
                % find a conversion factor within the unit map at a given
                % row
                convFactor=1;
                if strcmpi(unitToLookUp, unitMap{rowNum,1})
                    % use one as conversion factor if already defined in
                    % std unit.
                    hasFoundConversionFactor = true;
                    return
                end
                
                hasFoundConversionFactor = false;
                nUnits = size(unitMap,2)-1;
                for kk = 2:nUnits + 1
                    if strcmpi(unitToLookUp, unitMap{1, kk})
                        if ~isempty(unitMap{rowNum, kk})
                            convFactor=str2double(unitMap{rowNum, kk});
                            hasFoundConversionFactor = true;
                            continue
                        end
                    end
                end
                if ~hasFoundConversionFactor
                    warning('PROCESS_OBJ:noConversionInUnitMap',...
                        'The variable %s is missing a conversion factor for %s. The factor 1 will be used.\n',...
                        varNamesInMap{rowNum}, unitToLookUp)
                end
            end
        end
        
        function obj = setFlowOutType(obj, flowOutTypeEnum)
            obj.flowOutType = flowOutTypeEnum;
        end
        
        function [obj, rawDataStdUnit, errorCode, warningCode] = songaPreProcess(obj, rawData_nonstandard)
            
            rawDataStdUnit = struct();
            [errorCode, warningCode] = obj.generateErrorAndWarningCode(rawData_nonstandard);
            if errorCode ~= DtectLossRealtimeErrorCodes.AllOk
                return
            end
            
            tagList = [obj.songaMeasTags, obj.songaValidTags, obj.songaSelectorTags];
            for ii = 1:numel(obj.songaNameSpaceWithUnits)
                tag = tagList{ii};
                tagWithUnit = obj.songaNameSpaceWithUnits{ii};
                rawDataStdUnit.(tagWithUnit) = rawData_nonstandard.(tag).V * obj.defaultDataUnitsFactor.(tag);
            end
        end

        function [washedData, errorCode] = songaWashData(obj, rawDataStdUnit)
            
            errorCode = DtectLossRealtimeErrorCodes.AllOk;
            washedData = rmfield(rawDataStdUnit, {'presSP1_bar', 'flowOut1_lpm'});
                        
            % set measured values of the tags which are not valid equal
            % missing value code.
            for ii = 1:numel(obj.songaValidTags)
                if washedData.(obj.songaValidTagsWithUnits{ii}) == 0
                    washedData.(obj.songaMeasTagsWithUnits{ii + 1}) = obj.MISSING_VAL_CODE;
                elseif washedData.(obj.songaValidTagsWithUnits{ii}) ~= 1
                    errorCode = DtectLossRealtimeErrorCodes.(strcat('BoolAsDoubleDifferentFromZeroOrOne_', obj.songaValidTags{ii}));
                    return
                end
            end
            
            if washedData.selectpresSP_boolean == 1
                washedData.presSP_bar = rawDataStdUnit.presSP1_bar;
            end
            
            if washedData.selectflowout_boolean == 1
                washedData.flowOut_lpm = rawDataStdUnit.flowOut1_lpm;
            end
        end
        
        function [obj, rawDataStdUnit, errorCode, warningCode] = preProcess(obj, rawData_nonstandard)
            % preprocess the data to make sure required tags are present
            % and that necessary units are present. Convert the raw data to
            % std units.
            
            rawDataStdUnit = struct();
            [errorCode, warningCode] = obj.generateErrorAndWarningCode(rawData_nonstandard);
            if errorCode ~= DtectLossRealtimeErrorCodes.AllOk
                return
            end
            
            % convert units of the tags defined in the name space
            for ii = 1:numel(obj.nameSpace)
                tag = obj.nameSpace{ii};
                if rawData_nonstandard.(tag).V  == obj.MISSING_VAL_CODE
                    rawDataStdUnit.(obj.nameSpaceWithUnits{ii}) = obj.MISSING_VAL_CODE; % keep the missing_val_code without conversion.
                else
                    rawDataStdUnit.(obj.nameSpaceWithUnits{ii}) = rawData_nonstandard.(tag).V*obj.defaultDataUnitsFactor.(tag);
                end
            end
            
            % store tags which are not in the name space for later use
            for ii = 1:numel(obj.undefinedTags)
                rawDataStdUnit.(obj.undefinedTags{ii}) = obj.MISSING_VAL_CODE;
            end
            
            %---------------- special calculated tags ------------
            if isfield(rawData_nonstandard,'velRop') && isfield(obj.defaultDataUnitsFactor,'velRop')
                if not(rawData_nonstandard.velRop.V == obj.MISSING_VAL_CODE)
                    rawDataStdUnit.velRop_mph = rawData_nonstandard.velRop.V*obj.defaultDataUnitsFactor.velRop;
                else
                    rawDataStdUnit.velRop_mph = obj.MISSING_VAL_CODE;
                end
            else
                rawDataStdUnit.velRop_mph = obj.MISSING_VAL_CODE;
            end
            
            % estimate block velcity using heightHook, if tag is available
            if isfield(rawDataStdUnit, 'heightHook_m') && isfield(rawData_nonstandard, 'heightHook')
                [obj,rawDataStdUnit.velBlock_mph] = obj.estimateVelBlockUsingHeightHookMeas(rawDataStdUnit.heightHook_m,...
                    rawData_nonstandard.heightHook.t);
            else
                rawDataStdUnit.velBlock_mph = obj.MISSING_VAL_CODE;
            end
            
            % estimate bit velcity using depthBit, if tag is available
            if isfield(rawDataStdUnit, 'depthBit_m')
                [obj,rawDataStdUnit.velBit_mph] = ...
                    estimateVelBitUsingDepthBitMeas(obj, rawDataStdUnit.depthBit_m, rawData_nonstandard.depthBit.t);
            else
                rawDataStdUnit.velBit_mph = obj.MISSING_VAL_CODE;
            end

        end
        
        % call this for each iteration to wash a single vector of inputs
        % according to the units stored in rawDataUnits
        function [obj, washedData, rawDataStdUnit, processOutput, errorCode, warningCode] = ...
                washData(obj, rawData_nonstandard, sysState, recalibratePaddleNow)
            
            processOutput = [];
            washedData    = [];
            
            

            
            [obj, rawData_unifiedTime] = obj.unifyTime(rawData_nonstandard);
            
            % preprocess data to make sure that required tags and units are
            % not missing
            if obj.runSongaPreProcess
                [obj, rawDataStdUnit, errorCode, warningCode] = obj.songaPreProcess(rawData_unifiedTime);
            else
                [obj, rawDataStdUnit, errorCode, warningCode] = obj.preProcess(rawData_unifiedTime);
            end
            
            if errorCode ~= DtectLossRealtimeErrorCodes.AllOk
                % if a tag is missing unit, or if a required tag is
                % missing, then return with a descriptive error code.
                return
            end
            
            % based on the unit, check that value is within range, and
            % check for inf. save the last good value for future
            % use.
            [obj, rawDataStdUnit] = obj.replaceBadValuesWithLastGoodValue(rawDataStdUnit);
            
            if obj.runSongaPreProcess
                [washedData, errorCode] = obj.songaWashData(rawDataStdUnit);
                if errorCode ~= DtectLossRealtimeErrorCodes.AllOk
                    % if a tag is missing unit, or if a required tag is
                    % missing, then return with a descriptive error code.
                    return
                end
            else
                % by default the washed data equals the scaled rawData
                washedData = rawDataStdUnit;
            end


            
            % handle flow in measurements
            if rawDataStdUnit.flowIn_lpm >= 0
                obj.lowPassFlowInObj      = obj.lowPassFlowInObj.filterForCustomDt(rawDataStdUnit.flowIn_lpm, obj.dt_s, 0);
                washedData.flowInfilt_lpm = obj.lowPassFlowInObj.FilteredSignal;
            else
                washedData.flowInfilt_lpm = obj.MISSING_VAL_CODE;
            end
            if obj.params.paddle.useFlowInFilt
                washedData.flowIn_lpm = washedData.flowInfilt_lpm;
            end
            % Temporary range check
            if washedData.flowIn_lpm < 0 || washedData.flowIn_lpm > 10000
                washedData.flowIn_lpm = obj.MISSING_VAL_CODE;
            end
            
            % wash standpipe pressure and update downlinkObj
            % handle that presSP is missing by doing nothing
            if obj.availableOptionalTags(3) % isfield(rawDataStdUnit, 'presSP_bar')
                obj.downlinkObj = ...
                    obj.downlinkObj.step(rawDataStdUnit.presSP_bar, obj.timeCurrent_d, rawDataStdUnit.flowIn_lpm);
                if not(obj.downlinkObj.isDownlinking) 
                    obj.presSPdelayObj = ...
                        obj.presSPdelayObj.updateTimeDelay(rawDataStdUnit.presSP_bar, washedData.flowInfilt_lpm, obj.timeCurrent_d );
                end
                if obj.ISENABLED_DELAYED_PRESSP && (obj.presSPdelayObj.timeDelayPresSP_s) > 0 && not(obj.downlinkObj.isDownlinking)
                    [obj.presSPdelayObj, washedData.presSP_bar] = obj.presSPdelayObj.getDelayedPresSP(obj.timeCurrent_d);
                else
                    washedData.presSP_bar        = rawDataStdUnit.presSP_bar;
                end
            end
            
            washedData.isDownlinking       = obj.downlinkObj.isDownlinking;
            obj.isDownlinking              = obj.downlinkObj.isDownlinking;
            
            % wash rop - use time derivative of bit depth if measured rop is not available
            if isfield(rawData_nonstandard,'velRop') 
                if (rawData_nonstandard.velRop.V == obj.MISSING_VAL_CODE) && isfield(rawDataStdUnit,'velBit_mph')
                    washedData.velRop_mph = rawDataStdUnit.velBit_mph;
                end
            elseif isfield(rawDataStdUnit,'velBit_mph')
                washedData.velRop_mph = rawDataStdUnit.velBit_mph;
            else
                washedData.velRop_mph = obj.MISSING_VAL_CODE;
            end
            
            % wash trip tank volume - use active volume if measured trip tank volume is not available
            if isfield(rawData_nonstandard,'volTripTank') 
                if (rawData_nonstandard.volTripTank.V == obj.MISSING_VAL_CODE) && isfield(rawDataStdUnit,'volTripTank_m3')
                    washedData.volTripTank_m3 = rawDataStdUnit.volTripTank_m3;
                end
            elseif isfield(rawDataStdUnit,'volGainLoss_m3')
                washedData.volTripTank_m3 = rawDataStdUnit.volGainLoss_m3;
            else
                washedData.volTripTank_m3 = obj.MISSING_VAL_CODE;
            end
            
            %debugging
            debugTime = datenum(2015, 10, 30, 04, 34, 56);
            %Condition:  obj.timeCurrent_d >= debugTime 
            
            % handle flow out measurements
            obj.paddleConvObj = obj.paddleConvObj.convert(...
                rawDataStdUnit.flowOut_lpm,...
                washedData.flowIn_lpm,...
                washedData.isDownlinking,...
                recalibratePaddleNow,...
                sysState,...
                obj.timeCurrent_d);           
            
            %Range check
            if washedData.depthBit_m < 0 || washedData.depthBit_m > 10000
                washedData.depthBit_m = obj.MISSING_VAL_CODE;
            end
              
            washedData.isCalibratingPaddleBias = obj.paddleConvObj.isCalibratingPaddleBias;
            washedData.isCalibratingPaddleK    = obj.paddleConvObj.isCalibratingPaddleK;
            washedData.isCalibratedMeasFlowOut = obj.paddleConvObj.isCalibratedPaddle;
            washedData.biasPaddle_prc          = obj.paddleConvObj.Bias_prc;
            washedData.KPaddle_lpm_per_prc     = obj.paddleConvObj.K_lpm_per_prc;
            washedData.isRampingUp             = obj.paddleConvObj.isRampingUp;
            washedData.stateCalibrationPaddle  = obj.paddleConvObj.stateCalibrationPaddle;
            washedData.timePumpsOn_s           = obj.paddleConvObj.timePumpsOn_s;
            washedData.timePumpsOff_s          = obj.paddleConvObj.timePumpsOff_s;
            washedData.timeCalibrating_s       = obj.paddleConvObj.timeCalibrating_s;           
            washedData.flowOut_lpm             = obj.paddleConvObj.flowOut_lpm ;
            washedData.numberPumpStops         = obj.paddleConvObj.numberPumpStops;
            
            % update filtered flow rate from Kalman filter
            % covariance computation
            obj.kalmanFilter        = obj.kalmanFilter.addFlowAndDepthMeas(obj.timeCurrent_d, washedData.flowOut_lpm,...
                washedData.depthBit_m, washedData.depthHole_m);
            % kalman filter for flow out
            obj.kalmanFilter        = obj.kalmanFilter.update(washedData.flowIn_lpm, washedData.flowOut_lpm, obj.dt_s);
            washedData.flowOutFiltered_lpm=obj.kalmanFilter.xK;
            
            % remove sudden changes in gain/loss due to reset to zero or
            % change in active pit volume.
            [washedData.volGainLossWithoutOffset_m3, obj.volGainLossOffset_m3] = removeOffset([obj.volGainLossPrev_m3 rawDataStdUnit.volGainLoss_m3],...
                obj.volGainLossOffset_m3, obj.volGainLossThreshold_m3_per_time_step, washedData.flowIn_lpm);
            if not(rawDataStdUnit.volGainLoss_m3 == obj.MISSING_VAL_CODE)
                obj.volGainLossPrev_m3            = rawDataStdUnit.volGainLoss_m3;
            end
            
            processOutput.timeDelayPresSP_s     = obj.presSPdelayObj.timeDelayPresSP_s;
            processOutput.isCalibratingSPdelay  = obj.presSPdelayObj.isCalibratingSPdelay;
        end
        
        function [obj,velBlock_mph] = estimateVelBlockUsingHeightHookMeas(obj,heightHook_m,heightHook_t)
            if (heightHook_m == obj.MISSING_VAL_CODE)
                velBlock_mph = obj.MISSING_VAL_CODE;
            else
                if abs(heightHook_m - obj.filtHookHeight.previnp) > obj.HEIGHTHOOK_DISCONNECT_LIMIT_FOR_VELBLOCK_EST_M %Assume connected/disconnected pipe i heightHook_m deviates more than 9m from previously
                    obj.filtHookHeight.previnp = heightHook_m;
                    obj.filtHookHeight.prevout = 0;
                end
                [obj.filtHookHeight, velBlock_mps] = obj.filtHookHeight.smoothderivative2ndorder(heightHook_m, heightHook_t);
                velBlock_mph  = velBlock_mps*3600;
                %disp(['Time: ' num2str(obj.filtHookHeight.prevtime_d*86400,'%.2f') ', velBlock_mph: ' num2str(rawDataStdUnit.velBlock_mph,'%.2f')]);
            end
        end
        
        function [obj,velBit_mph] = estimateVelBitUsingDepthBitMeas(obj,depthBit_m,depthBit_t)
            if (depthBit_m == obj.MISSING_VAL_CODE)
                velBit_mph = obj.MISSING_VAL_CODE;
            else
                if abs(depthBit_m - obj.filtBitDepth.previnp) > obj.DEPTHBIT_RESET_LIMIT_FOR_VELBIT_EST_M %Assume connected/disconnected pipe i heightHook_m deviates more than 9m from previously
                    obj.filtBitDepth.previnp = depthBit_m;
                    obj.filtBitDepth.prevout = 0;
                end
                [obj.filtBitDepth, velBit_mps] = obj.filtBitDepth.smoothderivative2ndorder(depthBit_m, depthBit_t);
                velBit_mph = velBit_mps*3600;
                %disp(['Time: ' num2str(obj.filtBitDepth.prevtime_d*86400,'%.2f') ', velBit_mph: ' num2str(rawDataStdUnit.velBit_mph,'%.2f')]);
            end
        end
        
        % protect against inf and out-of-range
        % TODO: exctend this class to explot the unit in the tag name to
        % check the range.
        function [obj,rawDataStdUnit] = replaceBadValuesWithLastGoodValue(obj,rawDataStdUnit)
            fieldVec = fields(rawDataStdUnit);
            for curField = 1:1:numel(fieldVec)
                % missing measurement: emtpy value
                % TODO: determine if is this how we want to treat missing
                % measurements?
                if isempty(rawDataStdUnit.(fieldVec{curField}))
                    continue;
                end
                
                % if current value is bad
                if isnan(rawDataStdUnit.(fieldVec{curField})) || isinf(rawDataStdUnit.(fieldVec{curField}))
                    %if a previous good value exists....
                    if isfield(obj.rawDataStdUnit_lastGoodValue,fieldVec{curField})
                        rawDataStdUnit.(fieldVec{curField}) = obj.rawDataStdUnit_lastGoodValue.(fieldVec{curField});
                    else
                        rawDataStdUnit.(fieldVec{curField})= obj.MISSING_VAL_CODE;
                    end
                else
                    obj.rawDataStdUnit_lastGoodValue.(fieldVec{curField}) = rawDataStdUnit.(fieldVec{curField});
                end
            end%for
        end
        
        function [convertedTagValue, errorCode] = convertUnitIfFields(obj, fieldName, rawData_nonstandard, errorCode)
            if isfield(rawData_nonstandard, fieldName)
                if isfield(obj.defaultDataUnitsFactor, fieldName)
                    convertedTagValue = rawData_nonstandard.(fieldName).V * obj.defaultDataUnitsFactor.(fieldName);
                else
                    errorField = ['NoUnit_', fieldName];
                    [~,s] = enumeration('DtectLossRealtimeErrorCodes');
                    if ~isempty(strncmp(errorField, s, 100))
                        errorCode = DtectLossRealtimeErrorCodes.(errorField);
                    else
                        errorCode = DtectLossRealtimeErrorCodes.MissingUnitNonSpecfic;
                    end
                    convertedTagValue = obj.MISSING_VAL_CODE;
                end
            else
                errorField = ['NoValue_', fieldName];
                [~,s] = enumeration('DtectLossRealtimeErrorCodes');
                if ~isempty(strncmp(errorField, s, 100))
                    errorCode = DtectLossRealtimeErrorCodes.(errorField);
                else
                    errorCode = DtectLossRealtimeErrorCodes.MissingValueNonSpecfic;
                end
                convertedTagValue = obj.MISSING_VAL_CODE;
            end
        end
        
        function [obj, rawData_nonstandard] = unifyTime(obj, rawData_nonstandard)
            % use the most recent time of the tags defined in the name
            % space as time base
            
            nTags = numel(obj.nameSpace);
            currentTime = zeros(nTags,1);
            
            % store all the times in an array, find the maximum, and set
            % tags older than the allowed time-slack to missing value code.
            for ii = 1:nTags
                currentTime(ii) = rawData_nonstandard.(obj.nameSpace{ii}).t;
            end
            timeBase_d = max(currentTime);
            
            for jj = 1:nTags
                tag=obj.nameSpace{jj};
                if (abs(timeBase_d - rawData_nonstandard.(tag).t)*86400 > obj.timeSlack_s)
                    rawData_nonstandard.(tag).V = obj.MISSING_VAL_CODE;
                end
            end
            
            % update the stored times and time-step
            obj.timeLast_d     = obj.timeCurrent_d;
            obj.timeCurrent_d  = timeBase_d;
            obj.dt_s           = (obj.timeCurrent_d - obj.timeLast_d)*86400;
        end
        
        function [errorCode, warningCode] = generateErrorAndWarningCode(obj, rawData_nonstandard)
            
            errorCode      = DtectLossRealtimeErrorCodes.AllOk;
            warningCode    = DtectLossRealtimeErrorCodes.AllOk;
    
            % check that the input contains data. If not, then return.
            if isempty(fieldnames(rawData_nonstandard))
                errorCode = DtectLossRealtimeErrorCodes.MissingAllValues;
                % return with empty struct if input is empty
                return
            end
            
            % check that the required tags are present. If not, then return
            % with errorCode indicating the first tag which is missing.
            if not(all(obj.availableRequiredTags))
                missingIdx = find(obj.availableRequiredTags == false,1);
                missingTag = obj.requiredTags{missingIdx};
                errorCode = obj.generateNoValueErrorCode(missingTag);
                return
            end
            
            % check that the required tags have a conversion factor. If
            % not, then return with an error code indictating the first tag
            % which is missing unit.
            if not(all(obj.availableRequiredUnits))
                missingIdx = find(obj.availableRequiredUnits == false,1);
                missingTag = obj.requiredTags{missingIdx};
                errorCode = obj.generateNoUnitErrorCode(missingTag);
                return
            end
        end
        
    end
    
    methods (Static)
        function errorCode = generateNoValueErrorCode(tagName)
            errorField = ['NoValue_', tagName];
            [~, s] = enumeration('DtectLossRealtimeErrorCodes');
            if any(strncmp(errorField, s, 100))
                errorCode = DtectLossRealtimeErrorCodes.(errorField);
            else
                errorCode = DtectLossRealtimeErrorCodes.MissingValueNonSpecfic;
            end
        end
        
        function errorCode = generateNoUnitErrorCode(tagName)
            errorField = ['NoUnit_', tagName];
            [~, s] = enumeration('DtectLossRealtimeErrorCodes');
            if any(strncmp(errorField, s, 100))
                errorCode = DtectLossRealtimeErrorCodes.(errorField);
            else
                errorCode = DtectLossRealtimeErrorCodes.MissingUnitNonSpecfic;
            end
        end
    end
end
