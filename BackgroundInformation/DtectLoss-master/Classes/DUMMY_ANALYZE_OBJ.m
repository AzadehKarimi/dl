


classdef DUMMY_ANALYZE_OBJ
    %DUMMY_ANALYZE_OBJ Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        flowDeadband
        volumeThreshold
        noOfAlarms
        alarmState
    end
    
    methods
        function obj=DUMMY_ANALYZE_OBJ(qdb,vdb)
            obj.flowDeadband=qdb;
            obj.volumeThreshold=vdb;
            obj.noOfAlarms      = 0;
            obj.alarmState      = 0;%0=no,1=yes 
        end
        
        function obj=analyze(obj,qIo,vIo)
            for ii=1:numel(qIo),
                if abs(vIo)>obj.volumeThreshold,
                   obj.noOfAlarms = obj.noOfAlarms+1;
                   obj.alarmState = 1;
                else
                   obj.alarmState = 0;
                end
            end
        end
        
    end
    
end

