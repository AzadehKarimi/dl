% The DTECTLOSS_REALTIME class is intended compiled as a jar file and
% supplied to GBS as a part of the production code of the web portal to be
% designed.
% This class encapsulates both washing of data, running of gainloss
% algorithms and detection/alarm generation.
% The class excepts raw data in and provides data back that is to be logged
% and plotted on the web portal. The backtesting manager also calls this
% class to do offline tests to verify the functionality of this class.


classdef DTECTLOSS_REALTIME_OBJ < handle
    properties % (public)
    end
    properties (SetAccess = private)% (public)
        dtectLossOutputStruct;
        dtectLossOutputCell;
        algShortName; % algorthm short names such as "simple" or "pdiff"
        describeObj; % decribe washed data
    end
    properties (GetAccess = private, SetAccess = private)
        t_s;
        sysState;
        flowDeadBand_lpm;
        pitThreshold;
        processObj;
        algObj;
        algParams;
        params;
        configCell;
        flowOutFlowInObj;
        expStandPipePresObj;
        expTripTankVolObj;
        flowlineAndPitObj;
        flowbackRecorderObj;
        spPresDelayObj;
        gainDetectorObj;
        prevGoodVolGainLoss_m3;
        recalibratePaddleNow;
        evaRealtime;
        algorithmName;
        rawDataUnits;
        backtestModeIsOn_bool;
        debugCounter;
    end
    
    properties (Constant)
        timeStepmax_s = 8*60*60; %Automatic reset if more than eight hours since last time step
        
        % flowline and pit paramters used for initialization
        V0 = 5; % [m3] Init only, not relevant for tuning
        kInjFlowLine = 0; % [1/s] Shall not be used
        x0=[5;0;0]; % [m3; m3; m3]
        
        % parameters for the flowback recorder
        nFlowbacks         =  100; % [int] not relevant for tuning
        minVolume_m3       =    1; % [m3] not relevant for tuning
        minFlowrate_lpm    =   50; % [lpm] threshold for flow-rate from rig pumps. qp<minFlowrate -> pumps off - not relevant for tuning
        flowTolerance_lpm  =  400; % [lpm] stddev which is tolerated as steady state for flow-rate from rig pumps - not relevant for tuning
        dtFlowrate_s       =    1; % [s] not relevant for tuning
        dtVolGL_s          =    1; % [s] not relevant for tuning
        timeDuration_s     = 1200; % [s] not relevant for tuning
    end
    
    methods (Access = public)
        function obj = DTECTLOSS_REALTIME_OBJ()
            obj.t_s ;
            obj.sysState;
            obj.algObj;
            obj.flowDeadBand_lpm;
            obj.pitThreshold;
            obj.algShortName;
            obj.processObj;
            obj.params;
            obj.algParams;
            obj.describeObj; % decribe washed data
            obj.configCell; % configuration, for backtest this is read from xls-file
            obj.flowOutFlowInObj;
            obj.expStandPipePresObj;
            obj.gainDetectorObj;
            obj.expTripTankVolObj;
            obj.flowlineAndPitObj;
            obj.flowbackRecorderObj;
            obj.prevGoodVolGainLoss_m3;
            obj.recalibratePaddleNow;
            obj.evaRealtime;
            obj.algorithmName;
            obj.rawDataUnits;
        end
        
        function obj = setAreadp(obj, areaDPPOOH_m2, areaDPRIH_m2)
            obj.expTripTankVolObj   = obj.expTripTankVolObj.setAreadp(areaDPPOOH_m2, areaDPRIH_m2);            
        end
        
        function obj = init(obj, algorithmName, rawDataUnits, initTime_d, backtestModeOn)
            if isstruct(rawDataUnits)
                if isempty(fieldnames(rawDataUnits))
                    warning('DTECTLOSS_REALTIME_OBJ:missingRawDataUnits',...
                        'The raw data units struct is empty.');
                end
            else
                error('DTECTLOSS_REALTIME_OBJ:rawDataUnitsNotAStruct',...
                    'The raw data units is not a struct.');
            end
            
            if nargin < 4
                initTime_d = [];
            end
                        
            if isempty(initTime_d) || initTime_d == 0
                error('DTECTLOSS_REALTIME_OBJ:missingInitTime',...
                    'Inititial time (initTime_d) has not been provided to the init-function.');
            end
            
            if nargin < 5
                obj.backtestModeIsOn_bool = false;
            else
                if islogical(backtestModeOn)
                    obj.backtestModeIsOn_bool = backtestModeOn;
                else
                    obj.backtestModeIsOn_bool = true;
                end
            end
            
            obj = obj.initializeParamters;
            
            obj.algorithmName = algorithmName;
            obj.rawDataUnits  = rawDataUnits;
            
            obj.processObj = PROCESS_OBJ(obj.params, initTime_d);
            obj = obj.updateTagList(rawDataUnits);
            
            obj.algShortName = getAlgorithmShortNamesFromClassName(algorithmName);
            obj.recalibratePaddleNow = false;
                    
            obj = obj.initializeSubmodules(algorithmName);
            debugCounter = 0;
        end
        
        function obj = initializeParamters(obj)
            global ParSet
            if not(isempty(ParSet))
                obj.params          = getparams(ParSet);
            else
                obj.params          = getparams(0);
            end
            obj.algParams           = obj.params.voting;
        end
        
        function obj = initializeSubmodules(obj, algorithmName)
            obj.sysState            = STATE_MACHINE_OBJ();
            
            obj.describeObj         = DESCRIBE_OBJ();
            obj.describeObj         = obj.describeObj.init();
            
            obj.algObj              = eval(algorithmName);
            obj.algObj              = obj.algObj.initialize(obj.algParams);
            
            obj.flowOutFlowInObj    = EXPECTED_FLOWOUT_FROM_FLOWIN_OBJ();
            obj.flowOutFlowInObj    = obj.flowOutFlowInObj.initalize(obj.params.flowout);
            
            obj.expStandPipePresObj = EXPECTED_STANDPIPE_PRES_OBJ();
            obj.expStandPipePresObj = obj.expStandPipePresObj.initalize(obj.params.pressp);
            
            obj.expTripTankVolObj   = EXPECTED_TRIPTANKVOL_OBJ();
            obj.expTripTankVolObj   = obj.expTripTankVolObj.initialize(obj.params.triptank);            
            
            obj.flowlineAndPitObj   = FLOWLINE_AND_PIT_OBJ();
            
            obj.gainDetectorObj     = GAIN_DETECTOR_OBJ();
            obj.gainDetectorObj     = obj.gainDetectorObj.initialize;
            
            obj.dtectLossOutputStruct = struct;
            obj.dtectLossOutputCell = cell(0);
            
            obj.flowlineAndPitObj = obj.flowlineAndPitObj.initialize(obj.x0, obj.V0, obj.kInjFlowLine, obj.params.pitvol.kInjGainLoss_hz);
            
            obj.flowbackRecorderObj = FLOWBACK_RECORDER_OBJ();
            obj.flowbackRecorderObj = obj.flowbackRecorderObj.initialize(obj.nFlowbacks, obj.minVolume_m3, obj.minFlowrate_lpm, obj.flowTolerance_lpm,...
                obj.dtFlowrate_s, obj.dtVolGL_s, obj.timeDuration_s, obj.params.pitvol);
            obj.prevGoodVolGainLoss_m3 = -0.99925;
            
            obj.evaRealtime = EVALUATE_REALTIME_OBJ;
        end
        
        function obj = softreset(obj)
            fprintf(1, 'SoftReset() called, time step = %.1f sec\n', obj.processObj.dt_s);
            
            obj.flowbackRecorderObj = obj.flowbackRecorderObj.softReset();
            obj.flowlineAndPitObj = obj.flowlineAndPitObj.softReset();
            obj.flowOutFlowInObj = obj.flowOutFlowInObj.softReset();
            obj.expStandPipePresObj = obj.expStandPipePresObj.softReset();
            obj.describeObj = obj.describeObj.softReset();
            obj.algObj = obj.algObj.softReset();
            obj.processObj = obj.processObj.softReset();
            obj.sysState = obj.sysState.softReset();
            obj.evaRealtime = obj.evaRealtime.softReset();
            obj.gainDetectorObj = obj.gainDetectorObj.softReset();
        end
        
        function obj = reset(obj, initTime_d)
            savedAlgorithmName = obj.algorithmName;
            savedRawDataUnits  = obj.rawDataUnits;
            obj                = DTECTLOSS_REALTIME_OBJ();
            obj = obj.init(savedAlgorithmName, savedRawDataUnits, initTime_d);
        end
        
        function obj = updateTagList(obj, rawDataUnits)
            obj.processObj = obj.processObj.specifyUnits(rawDataUnits);
            obj.processObj = obj.processObj.setAvailableTags(rawDataUnits);
            obj.processObj = obj.processObj.defineNameSpace;
        end
        
        function obj = stepAllAlgorithms(obj, rawData_nonstandardUnits)
            obj.debugCounter = obj.debugCounter + 1;
            prc = rawData_nonstandardUnits.flowOutPercent.V;
            
            % wash data: convert units, adjust measurements, filtering
            [obj.processObj, washedData, rawData, processOutput, errorCode, warningCode] =...
                obj.processObj.washData(rawData_nonstandardUnits, obj.sysState, obj.recalibratePaddleNow);
            if errorCode ~= DtectLossRealtimeErrorCodes.AllOk
                obj.dtectLossOutputStruct.errorCode     = int32(errorCode);
                obj.dtectLossOutputStruct.warningCode   = int32(warningCode);
                return;
            end
            errorAndWarningCodeCell = {'errorCode', int32(errorCode); 'warningCode', int32(warningCode)};
            
            % call the state machine
            [obj, sysStateCell] = obj.updateSysStateAndDescribeObj(rawData, washedData);
                
            [obj.flowOutFlowInObj, flowInOutOutput] = obj.flowOutFlowInObj.step(washedData.flowIn_lpm, washedData.flowOut_lpm,  washedData.isDownlinking, obj.algObj.alarmState, obj.processObj.timeCurrent_d);
            [obj.expStandPipePresObj, presExpOutput] = obj.expStandPipePresObj.step(washedData.flowInfilt_lpm, washedData.presSP_bar, washedData.isDownlinking, obj.algObj.alarmState, obj.processObj.timeCurrent_d);
            [obj.expTripTankVolObj, volExpTripOutput] = obj.expTripTankVolObj.step(washedData.depthBit_m, washedData.depthHole_m,  washedData.volTripTank_m3, washedData.flowInfilt_lpm, obj.processObj.timeCurrent_d);
            
            
            %debugging 04:34:55
            debugTime = datenum(2015, 10, 30, 04, 06, 20);
            %debugging obj.processObj.timeCurrent_d           
            
            [obj, volGainLossPredError] = obj.stepFlowbackRecorder(washedData);
            
            obj = obj.resetExpctedVolGainLoss(washedData);
            [obj, volGainLossPredModelled] = obj.stepFlowlineAndPitObj(flowInOutOutput, washedData);
            volGainLossExpected_m3 = volGainLossPredModelled + volGainLossPredError;
            
            isCalibratedExpPitVol_boolean = obj.isCalibratedExpPitVol();
            flowlineAndPitOutput = obj.flowlineAndPitObj.generateOutput(volGainLossExpected_m3, isCalibratedExpPitVol_boolean);
            
            obj.gainDetectorObj = obj.gainDetectorObj.step(obj.processObj.timeCurrent_d, washedData.volGainLoss_m3,...
                washedData.depthBit_m, washedData.depthHole_m, washedData.flowIn_lpm);
            gainDetectorOutput = obj.gainDetectorObj.generateOutput;
            
            isCalibrating = obj.expStandPipePresObj.isCalibratingExpPresSP || ...
                obj.processObj.paddleConvObj.isCalibratingPaddleBias || obj.processObj.paddleConvObj.isCalibratingPaddleK || ...
                obj.flowOutFlowInObj.isCalibratingFlowInFlowOut || obj.processObj.presSPdelayObj.isCalibratingSPdelay;
            isCalibratingCell = {'isCalibrating', isCalibrating};
            
            
            algData = struct('timeCurrent_d',             obj.processObj.timeCurrent_d,...
                             'flowOutExpected_lpm',       obj.flowOutFlowInObj.flowOutExpected_lpm,...
                             'presSPExpected_bar',        obj.expStandPipePresObj.presSPExpected_bar,...
                             'numberPumpStops',           washedData.numberPumpStops,...
                             'timePumpsOn_s',             washedData.timePumpsOn_s,...                             
                             'volTripTankCumMeas_m3',     obj.expTripTankVolObj.volTTcum_m3,...
                             'volTripTankCumExpected_m3', obj.expTripTankVolObj.volTTcumExp_m3,...
                             'volTripTankCumError_m3',    obj.expTripTankVolObj.volTTcumErr_m3,...
                             'timeSinceResetGL_s',        obj.flowlineAndPitObj.timeSinceResetGL_s,...
                             'volGainLossExpected_m3',    volGainLossExpected_m3,...
							 'volGainDetector_m3',        obj.gainDetectorObj.volGain_m3,...
                             'isCalibratedExpPresSP',     obj.expStandPipePresObj.isCalibratedExpPresSP,...
                             'isCalibratedExpPitVol',     isCalibratedExpPitVol_boolean,...
                             'isCalibratedExpFlowOut',    flowInOutOutput.isCalibratedExpFlowOut, ...
                             'isCalibratedMeasFlowOut',   washedData.isCalibratedMeasFlowOut, ...
                             'isCalibrating',             isCalibrating, ...
                             'isDownlinking',             washedData.isDownlinking, ...
                             'isRampingUp',               washedData.isRampingUp, ...
                             'resetVolIOnow',             washedData.isCalibratingPaddleK ...
                );
            
            if not(obj.sysState.isDataValid)
                [obj.algObj, algResult] = obj.algObj.returnResultStruct();
                % if no good data has become available since object was
                % created, variables will be emtpy, and the algorithms cannot run.
                evaRealtimeOutput = obj.evaRealtime.generateOutput;
            else
                [obj.algObj, algResult] = obj.algObj.stepIt(washedData, algData, obj.sysState, obj.processObj.timeCurrent_d, obj.processObj.dt_s);
                if isprop(obj.algObj, 'resetAlarm')
                    if obj.algObj.resetAlarm 
                        if washedData.volGainLoss_m3~=-0.99925
                            %disp('Reset');
                            obj.flowlineAndPitObj = obj.flowlineAndPitObj.setQuickConvergeNow(true);
                        end
                        obj.expTripTankVolObj = obj.expTripTankVolObj.resetTripVol();
                        obj.gainDetectorObj = obj.gainDetectorObj.resetAfterAlarm();
                    end
                end
                
                % evaluate realtime the number of alarms
                [obj.evaRealtime, evaRealtimeOutput] = obj.evaRealtime.step(obj.processObj.timeCurrent_d, obj.processObj.dt_s, algResult.alarmState, algResult.alarmType);
                                
                if obj.algObj.recalibrateFlowOutNow && washedData.flowOut_lpm ~= -0.99925
                    %disp('Reset');
                    obj.recalibratePaddleNow = true;
                else
                    obj.recalibratePaddleNow = false;
                end
            end

            realtimeOutputStatus.isCalibrating = isCalibrating;
            realtimeOutputStatus.volGainLossPredError = volGainLossPredError;
            realtimeOutputStatus.volGainLossPredModelled = volGainLossPredModelled;
            realtimeOutputStatus.volGainLossExpected_m3 = volGainLossExpected_m3;
            
            rawData_Nsu.flowIn          = rawData_nonstandardUnits.flowIn.V;
            rawData_Nsu.depthBit        = rawData_nonstandardUnits.depthBit.V;
            rawData_Nsu.depthHole       = rawData_nonstandardUnits.depthHole.V;
            rawData_Nsu.flowOut         = rawData_nonstandardUnits.flowOut.V;
            rawData_Nsu.flowOutPercent  = rawData_nonstandardUnits.flowOutPercent.V;
            rawData_Nsu.presSP          = rawData_nonstandardUnits.presSP.V;
            rawData_Nsu.volGainLoss     = rawData_nonstandardUnits.volGainLoss.V;
%             rawData_Nsu.volPitDrill     = rawData_nonstandardUnits.volPitDrill.V;
            rawData_Nsu.volTripTank     = rawData_nonstandardUnits.volTripTank.V;
            rawData_Nsu.heightHook      = rawData_nonstandardUnits.heightHook.V;
            rawData_Nsu.rpm             = rawData_nonstandardUnits.rpm.V;
%             rawData_Nsu.weightOnBit     = rawData_nonstandardUnits.weightOnBit.V;
            rawData_Nsu.pitVolume       = rawData_nonstandardUnits.pitVolume.V;
            
            
            washedDataCell = obj.appendSuffix(washedData, '_wash');
            algDataCell = obj.appendSuffix(algData, '_algdata');
            rawDataCell = obj.appendSuffix(rawData, '_raw');
            rawDataNsuCell = obj.appendSuffix(rawData_Nsu, '_nsu');
            flowInOutOutputCell = obj.appendSuffix(flowInOutOutput, '_stat');
            presExpOutputCell = obj.appendSuffix(presExpOutput, '_stat');
			volExpTripOutputCell= obj.appendSuffix(volExpTripOutput,'_stat');
            flowlineAndPitOutputCell = obj.appendSuffix(flowlineAndPitOutput, '_stat');
            processOutputCell = obj.appendSuffix(processOutput, '_stat');
            realtimeStatusOutputCell = obj.appendSuffix(realtimeOutputStatus, '_realtime');
            gainDetectorOutputCell = obj.appendSuffix(gainDetectorOutput, '_stat');
            algResultCell  = obj.appendSuffix(algResult, strcat('_', obj.algShortName));
            evaRealtimeOutputCell = obj.appendSuffix(evaRealtimeOutput,'_stat');
            
            outputCell = [...
                algDataCell;
                washedDataCell;
                rawDataCell;
                rawDataNsuCell;
                isCalibratingCell;
                flowInOutOutputCell;
                presExpOutputCell;
                volExpTripOutputCell;
                flowlineAndPitOutputCell;
                processOutputCell;
                realtimeStatusOutputCell;
                gainDetectorOutputCell;
                algResultCell;
                evaRealtimeOutputCell;
                errorAndWarningCodeCell;
                sysStateCell];
            
            if obj.backtestModeIsOn_bool
                obj.dtectLossOutputCell = outputCell;
            else
                obj.dtectLossOutputCell = obj.ignoreCertainVariablesAndCastToDouble(outputCell);
            end
            
            obj.dtectLossOutputStruct = cell2struct(obj.dtectLossOutputCell(:, 2), obj.dtectLossOutputCell(:, 1), 1);
        end
                
        function [obj, sysStateCell] = updateSysStateAndDescribeObj(obj, rawData, washedData)
            obj.sysState = obj.sysState.update(washedData, obj.processObj.dt_s); 
            obj.describeObj = obj.describeObj.describeData(rawData, washedData, obj.sysState, obj.processObj.dt_s);
            sysStateCell = {'sysState_e', int32(obj.sysState.currentState)};    
        end
        
        function [obj, volGainLossPredError] = stepFlowbackRecorder(obj, washedData)
            debugTime = datenum(2016, 01, 01, 02, 29, 04);
            
            obj.flowbackRecorderObj = obj.flowbackRecorderObj.addFlowToRingbuffer(obj.processObj.timeCurrent_d, washedData.flowIn_lpm);
            obj.flowbackRecorderObj = obj.flowbackRecorderObj.addPresToRingbuffer(obj.processObj.timeCurrent_d, washedData.presSP_bar);
            obj.flowbackRecorderObj = obj.flowbackRecorderObj.addVolToRingbuffer(obj.processObj.timeCurrent_d, washedData.volGainLoss_m3);
            obj.flowbackRecorderObj = obj.flowbackRecorderObj.addExpVolToRingbuffer(obj.flowlineAndPitObj.prevTimeStamp, obj.flowlineAndPitObj.x(2));
            obj.flowbackRecorderObj = obj.flowbackRecorderObj.updateCirculationState(washedData.flowIn_lpm, washedData.depthBit_m, washedData.depthHole_m);
            obj.flowbackRecorderObj = obj.flowbackRecorderObj.updateTimeSpentAtState(obj.processObj.dt_s);
            [obj.flowbackRecorderObj, obj.flowlineAndPitObj] = obj.flowbackRecorderObj.updateFap(obj.flowlineAndPitObj);
            obj.flowbackRecorderObj = obj.flowbackRecorderObj.updateConnectionData;
            
            volGainLossPredError = 0;
            if ~isempty(obj.flowbackRecorderObj.mdl)
                if ~isempty(obj.flowbackRecorderObj.mdl.coeffs)
                    volGainLossPredError = obj.flowbackRecorderObj.predictFromModel;
                end
            end
        end
        
        function obj = resetExpctedVolGainLoss(obj, washedData)
           % if gain/loss is reset, then reset expected volGainLoss.
            % initialize previous good value of volGainLoss, which will be
            % used for comparison with current volGainLoss
            if obj.prevGoodVolGainLoss_m3 == -0.99925 && washedData.volGainLoss_m3 ~= -0.99925
                obj.prevGoodVolGainLoss_m3 = washedData.volGainLoss_m3;
            end
            
            if washedData.volGainLoss_m3 ~= -0.99925
                obj.flowlineAndPitObj = obj.flowlineAndPitObj.resetGLWithSlidingWindowOfFlowIn(obj.prevGoodVolGainLoss_m3, washedData.volGainLoss_m3,...
                    obj.processObj.volGainLossThreshold_m3_per_time_step);
                obj.prevGoodVolGainLoss_m3 = washedData.volGainLoss_m3;
            end
            
            if obj.flowbackRecorderObj.isInit
                obj.flowlineAndPitObj = obj.flowlineAndPitObj.resetGL(obj.prevGoodVolGainLoss_m3);
            end
            
        end
        
        function [obj, volGainLossPredModelled] = stepFlowlineAndPitObj(obj, flowInOutOutput, washedData)
            obj.flowlineAndPitObj = obj.flowlineAndPitObj.stepIt(...
                struct('t', obj.processObj.timeCurrent_d, 'V', flowInOutOutput.flowOutExpected_lpm),... % expected flow rate out
                struct('t', obj.processObj.timeCurrent_d, 'V', washedData.flowIn_lpm),... % flow in
                struct('t', obj.processObj.timeCurrent_d, 'V', washedData.volGainLoss_m3),... % gain/loss volume which is reset
                obj.flowbackRecorderObj.pumpsOn); % boolean which indicate if output injection should be used. 
            
            volGainLossPredModelled = obj.flowlineAndPitObj.x(2);
        end
        
        function isCalibratedExpPitVol_boolean = isCalibratedExpPitVol(obj)
           isCalibratedExpPitVol_boolean = any(obj.flowbackRecorderObj.kStore > 0);
        end
        
        function [obj, textResult]  = checkData(obj)
            obj.describeObj         = obj.describeObj.checkDataSeries();
            textResult              = obj.describeObj.DispSeriesAnalysis();
        end
        
        % returns the fields of the dtectlossOutut struct and the units
        function [obj,outputFieldnames_full, outputFieldnames_noUnit, outputUnits] = getOutputFormat(obj)
            
            if isempty(obj.dtectLossOutputStruct)
                disp('ERROR: DTECTLOSS_REALTIME_OBJ.getOutputFormat() found and empty dtectLossOutputStruct struct, this is likely because you need to call stepAllAlgorithms first and have not done so yet' );
                outputUnits             = [];
                outputFieldnames_full   = [];
                outputFieldnames_noUnit  = [];
                return;
            end
            outputFieldnames_full_Cell       = fieldnames(obj.dtectLossOutputStruct);
            outputFieldnames_noUnit_Cell     = cell(numel(outputFieldnames_full_Cell),1);
            outputUnits_Cell                 = cell(numel(outputFieldnames_full_Cell),1);
            for k                       = 1:1:numel(outputFieldnames_full_Cell)
                splitFields                 = strsplit(outputFieldnames_full_Cell{k},'_');
                outputFieldnames_noUnit_Cell{k} = [splitFields{1},'_',splitFields{end}];
                if numel(splitFields)>=3
                    outputUnits_Cell{k} = splitFields{2};
                    ind = 3;
                    while ind<numel(splitFields)
                        outputUnits_Cell{k} = [outputUnits_Cell{k},'_',splitFields{ind}];
                        ind = ind+1;
                    end
                end
                if isempty(outputUnits_Cell{k})
                    outputUnits_Cell{k} = '';%for conversation to character array
                end
                % [outputFieldnames_full{k}, ' ',  outputFieldnames_noUnit{k},' ', outputUnits{k}];
            end%for
            outputFieldnames_full   = char(outputFieldnames_full_Cell);
            outputFieldnames_noUnit = char(outputFieldnames_noUnit_Cell);
            outputUnits             = char(outputUnits_Cell);
        end
        
        function backtestModeOn = getBacktestMode(obj)
            backtestModeOn = obj.backtestModeIsOn_bool;
        end
    end
    
    %************************** END OF PUBLIC INTERFACE *******************
    
    methods (Access = {?private, ?matlab.unittest.TestCase}, Static)
        
        
        function cellWithOnlyDoubles = ignoreCertainVariablesAndCastToDouble(inputCell)
            cellWithOnlyDoubles = inputCell;
            markForDeletion = false(size(inputCell, 1), 1);
            
            for ii = 1:size(inputCell, 1)
                ignoreVariable_boolean = DTECTLOSS_REALTIME_OBJ.checkIfVariableShouldBeIgnored(inputCell{ii, 1});
                if ignoreVariable_boolean
                    markForDeletion(ii) = true;
                else
                    cellWithOnlyDoubles{ii, 2} = double(inputCell{ii, 2});
                end
            end
            
            cellWithOnlyDoubles(markForDeletion, :) = [];
        end
        
        function cellWithTagAndValue = appendSuffix(outputVarsToAdd, suffix)
            if ~isstruct(outputVarsToAdd)
                cellWithTagAndValue = [];
                return;
            end
            
            outputFieldNames = fieldnames(outputVarsToAdd);
            outputFieldNamesWithSuffix = strcat(outputFieldNames, suffix);
            
            values = struct2cell(outputVarsToAdd);
            
            cellWithTagAndValue = {outputFieldNamesWithSuffix{:}; values{:}}';
        end
        
        function ignoreVariable_boolean = checkIfVariableShouldBeIgnored( str )
            
            ignoreVariable_boolean = true;
            
            variablesToIgnore = {'biasPaddle_prc';
%                 'flowInfilt_lpm';
                'flowPitDer_lpm';
                'isCalibratingSPdelay';
%                 'KPaddle_lpm_per_prc';
%                 'presSPDer_barpsec';
%                 'presSPErr_bar';
%                 'presSPExpWithoutInjection_bar';
%                 'pressSPInjection_bar';
                'rop_mph';
                'rpm';
                'selectflowout_boolean';
                'selectflowout_boolean';
                'selectpresSP_boolean';
                'selectpresSP_boolean';
                'stateCalibration';
                'stateMachine';
                'timeCalibrating_s';
                'timeConstFlowOut_s';
                'timeDelayFlowOut_s';
                'timeDelayPresSP_s';
                'timePumpsOff_s';
                'timePumpsOn_s';
                'validdepthBit_boolean';
                'validdepthBit_boolean';
                'validdepthHole_boolean';
                'validdepthHole_boolean';
                'validflowIn_boolean';
                'validflowIn_boolean';
                'validflowOut_boolean';
                'validflowOut_boolean';
                'validflowOut1_boolean';
                'validflowOut1_boolean';
                'validpresSP_boolean';
                'validpresSP_boolean';
                'validpresSP1_boolean';
                'validpresSP1_boolean';
                'validrop_boolean';
                'validrop_boolean';
                'validrpm_boolean';
                'validrpm_boolean';
                'validvelBlock_boolean';
                'validvelBlock_boolean';
                'validvolPitDrill_boolean';
                'validvolPitDrill_boolean';
                'validvolTripTank_boolean';
                'validvolTripTank_boolean';
                'velBlock_mph';
                'velBlock_mph';
                'vol0FlowOutPumpStart_m3';
%                 'volGainLossWithoutOffset_m3';
                'volPitDrill_m3'};
            
            for ii = 1:length(variablesToIgnore)
                if strcmp(variablesToIgnore{ii}, str)
                    return
                end
            end
            
            ignoreVariable_boolean = false;
        end
        
    end
end