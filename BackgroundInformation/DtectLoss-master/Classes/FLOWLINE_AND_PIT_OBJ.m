classdef FLOWLINE_AND_PIT_OBJ < DTECTLOSS_SUBMODULE
    %FLOWLINE_AND_PIT_OBJ Model of expeced flow line and pit dynamics.
    %   Detailed explanation goes here
    
    properties (SetAccess=private)
        x; % state [volFlowLine; volGainLoss; xInjInt], units [m3; m3; m3/s]
        volDrainback;% [m3] drainback from flow line into pits when stopping rig pumps
        kOutputInjectionFl; % [1/s] time-constant/gain for output injection
                            % term to compensate for difference in actual
                            % and estimate gain/loss volume. Used for flow
                            % line dynamics.
        kOutputInjectionGL; % [1/s] time-constant/gain for output injection 
                            % term to compensate for difference in actual
                            % and estimate gain/loss volume. Used for gain
                            % loss dynamics
        kIntOutputInjectionGL; % [1/s^2] time-constant/gain for integrated output injection
                            % term to compensate for difference in actual
                            % and estimate gain/loss volume. Used for gain
                            % loss dynamics
        totalInjectionRate_lpm; % [lpm] total injection flow-rate to track the
                                % gain/loss volume
        dT; % [s] duration of time-step
        prevTimeStamp; % [serial time] time instant of previous update
        td; % [s] time-delay from flowline through shakers to pit
        filterDer; % FILTERDERIVATIVE_OBJ for the flow-rate from the rig pumps.
        pitDer; % FILTERDERIVATIVE_OBJ for the pit volume
        flowPitDer_lpm; % estimated losses in well and shakers based on derivative of measured pit volume
        rbFlowIn; % ringbuffer for flow in
        % the flow rate from the flowline to the pit is modelled as
        % qFL = kFlowline*VFlowline + k2Flowline*qp
        kFlowline; % [1/s] constant for converting the volume in the flowline to flow-rate
        k2Flowline; % [-] constant used for flowline estimation.
        volGainLossPrevGood_m3; % [m3] previous measurement of volGainLoss which was good.
        quickConvergeNow_bool; % [bool] true if quick converge is active
        timeCounterQuickConverge_s; % [s] time counter for quick converge
        timeSinceResetGL_s; % time counter since last time GL was reset
    end
    
    properties (Constant)
        errVal = -0.99925;
        M3SToLPM = 60000;
        volStep_m3 = 3;
        upperInjLimit=300; % [lpm] upper limit of injection term
        lowerInjLimit=-300; % [lpm] lowerlimit of injection term
        pitDerivativeTimeConstant=900; % [s] time constant for the first 
                                       % order filter for finding derivative
                                       % of the pit volume
        flowInBufferSize_samples=540; % [samples] number of samples in ringbuffer for flow-in.
        maxDT=60; % [s] maximum allowed time-step. Reset if above.
        damping=1; % [-] damping for the gain/loss observer. Used to calculate tuning.
        flowInLowLimForGLReset_lpm = 400; % [lpm] limit which is used to determine if a reset should be performed.
        timeLimQuickConverge_s = 90; % [s] time-limit for quick convergence. On for this period of time.
        kVol_reset_hz = 1/60;
        kFlow_reset = -0.5;
        volDrainback_reset_m3 = 6;
    end
    
    methods
        function obj=FLOWLINE_AND_PIT_OBJ()
            obj.x=[];
            obj.volDrainback=[];
            obj.kOutputInjectionFl=[];
            obj.kOutputInjectionGL=[];
            obj.kIntOutputInjectionGL=[];
            obj.totalInjectionRate_lpm=[];
            obj.dT=[];
            obj.prevTimeStamp=[];
            obj.td=[];
            obj.filterDer=[];
            obj.pitDer=[];
            obj.flowPitDer_lpm=[];
            obj.rbFlowIn=[];
            obj.kFlowline=[];
            obj.k2Flowline=[];
            obj.volGainLossPrevGood_m3=[];
            obj.quickConvergeNow_bool=[];
            obj.timeCounterQuickConverge_s=[];
            obj.timeSinceResetGL_s=[];
        end
        
        function obj = softReset(obj)
            obj = obj.setQuickConvergeNow(true);
            obj.kFlowline = obj.kVol_reset_hz;
            obj.k2Flowline = obj.kFlow_reset;
            obj.volDrainback = obj.volDrainback_reset_m3;
            obj.timeSinceResetGL_s=0;
        end
        
        function obj=initialize(obj, x0, V0Fl, kOiFl, kOiGL, timeStep)
            obj.x=x0;
            obj.volDrainback=V0Fl;
            obj.kOutputInjectionFl=kOiFl;
            obj.kOutputInjectionGL=kOiGL;
            % tune observer for gain loss as a mass-spring-damper system
            w0=kOiGL/(2*obj.damping);
            obj.kIntOutputInjectionGL=w0^2;
            obj.td=0; % set time-delay to zero initially
            if nargin==6
                obj.dT=timeStep;
            end
            
            obj.totalInjectionRate_lpm = 0;
            
            obj.flowPitDer_lpm = 0;
            obj.pitDer = FILTERDERIVATIVE_OBJ(obj.pitDerivativeTimeConstant); %
            
            obj.rbFlowIn=RINGBUFFER_OBJ(obj.flowInBufferSize_samples);
            
            obj.kFlowline=obj.kVol_reset_hz;
            obj.k2Flowline=obj.kFlow_reset;
            obj.volGainLossPrevGood_m3=obj.x(2);
            
            obj.quickConvergeNow_bool = true;
            obj.timeCounterQuickConverge_s = 0;
            obj.timeSinceResetGL_s=0;
        end
        
        function obj=updateTimeStep(obj, qOutExp, qPump, volGL)
            % update time-step (dT) and time stamp based on time from
            % variables.
            
            currentTime=[qOutExp.t qPump.t volGL.t];
            if isempty(obj.prevTimeStamp)
                obj.prevTimeStamp=max(currentTime);
                obj.dT=0;
                return
            end
            
            newTimeStamp=max([currentTime obj.prevTimeStamp]);
            obj.dT=max(0, round((newTimeStamp-obj.prevTimeStamp)*86400));
            
            obj.prevTimeStamp=newTimeStamp;
        end
        
        function obj=setTimeStep(obj, timeStep)
            % timeStep should be in seconds
            obj.dT=max(0, min(obj.maxDT, timeStep));
        end
        
        function obj=updateState(obj, qOut_lpm, qPump_lpm, volGainLoss_m3, useOutputInjection, qPump_t)
            % This function updates the estimates of the volume in the flow
            % line and pit/(gain/loss). Input: qOut [lpm] expected flow
            % rate out of well and into flow line. qPump [lpm] flow rate
            % from rig pumps. volGainLoss [m3] gain/loss volume without
            % sudded jumps due to reset etc. useOutputInjection [boolean]
            % should be true when drilling in steady state, otherwise false.

            obj = obj.checkIfQuickConvergeShouldBeOn;
            if ~obj.quickConvergeNow_bool
                % step time normally
                [xDot, obj]=obj.dyn(qOut_lpm, qPump_lpm, volGainLoss_m3, useOutputInjection);
                obj.x=obj.x+obj.dT*xDot;
            else
                % reset states
                valFlowIn=obj.rbFlowIn.getLastNValues(10);
                validMeas = valFlowIn ~= obj.errVal;
                if any(validMeas)
                    obj.x(1)=mean(valFlowIn(validMeas))*(1-obj.k2Flowline)/obj.kFlowline/obj.M3SToLPM;
                end
                
                if volGainLoss_m3 ~= obj.errVal
                    obj.x(2) = volGainLoss_m3;
                else
                    obj.x(2) = obj.volGainLossPrevGood_m3;
                end
                
                obj.x(3) = 0;
            end
            obj.x(1)=max(obj.x(1),0); % ensure that volume in flowline is positive.
            obj.x(3)=min(max(obj.x(3), obj.lowerInjLimit/obj.M3SToLPM), obj.upperInjLimit/obj.M3SToLPM); % ensure that integrated injection is not too large

            if not(volGainLoss_m3==obj.errVal)
                if abs(volGainLoss_m3 - obj.pitDer.previnp/obj.M3SToLPM) > obj.volStep_m3
                    obj.pitDer.previnp = volGainLoss_m3*obj.M3SToLPM;
                else
                    [obj.pitDer, obj.flowPitDer_lpm]=obj.pitDer.smoothderivative1storder(volGainLoss_m3*obj.M3SToLPM, qPump_t);
                end
            end
        end

        function obj = checkIfQuickConvergeShouldBeOn(obj)
            
            obj.timeCounterQuickConverge_s = max(0, min(obj.timeCounterQuickConverge_s + obj.dT, ...
                obj.timeLimQuickConverge_s));
            
            if obj.timeCounterQuickConverge_s == obj.timeLimQuickConverge_s
                obj.quickConvergeNow_bool = false;
            end
            
        end
        
        function obj = setQuickConvergeNow(obj, desiredQuickConvergeNow_bool)
            
            if desiredQuickConvergeNow_bool
                obj.timeCounterQuickConverge_s = 0;
                obj.quickConvergeNow_bool = true;
            else
                obj.quickConvergeNow_bool = false;
            end
            
        end
        
        function [xDot, obj]=dyn(obj, qPumpDelayed_lpm, qPump_lpm, volGainLoss_m3, useOutputInjection)
            % calculation of xDot
                        
            % output injection terms.
            x1Inj=obj.kOutputInjectionFl*(volGainLoss_m3-obj.x(2));
            x2Inj=max(obj.lowerInjLimit/obj.M3SToLPM, min(obj.kOutputInjectionGL*(volGainLoss_m3-obj.x(2)), obj.upperInjLimit/obj.M3SToLPM));
            % double integrator for output injection term to track slope of
            % volGainLoss
            x2InjIntDot=obj.kIntOutputInjectionGL*sign(volGainLoss_m3-obj.x(2))*...
                max(abs(volGainLoss_m3-obj.x(2)), (volGainLoss_m3-obj.x(2))^2);
            
            % ensure that the flow-rates are positive
            qPump_m3s_pos=max(0, qPump_lpm)/obj.M3SToLPM;
            if qPumpDelayed_lpm~=obj.errVal
                qOutEst_m3PerS_pos=max(0, qPumpDelayed_lpm)/obj.M3SToLPM;
            else
                qOutEst_m3PerS_pos=qPump_m3s_pos;
            end
            
            % function for flow-rate from flowline
            qFl_m3PerS=max(0, obj.x(1)*obj.kFlowline)+obj.k2Flowline*qOutEst_m3PerS_pos;
            
            % calculate xDot
            if useOutputInjection
                % total flow-rate injected into estimated volGainLoss to track
                % the measured one.
                totalInjectionRate_m3PerS = x2Inj + obj.x(3);
                obj.totalInjectionRate_lpm = totalInjectionRate_m3PerS*obj.M3SToLPM;
                xDot=[qOutEst_m3PerS_pos - qFl_m3PerS - x1Inj;
                    qFl_m3PerS - qOutEst_m3PerS_pos + totalInjectionRate_m3PerS;
                    x2InjIntDot];
            else
                obj.totalInjectionRate_lpm = 0;
                xDot=[qOutEst_m3PerS_pos - qFl_m3PerS;
                    qFl_m3PerS - qOutEst_m3PerS_pos;
                    0];
            end
        end
        
        function obj=stepIt(obj, qOut, qPump_lpm, volGL, useOutputInjection)
            
            if volGL.V == obj.errVal
                volGainLoss_m3=obj.volGainLossPrevGood_m3;
            else
                volGainLoss_m3=volGL.V;
                obj.volGainLossPrevGood_m3=volGL.V;
            end
            
            obj=obj.updateTimeStep(qOut, qPump_lpm, volGL);
            obj.rbFlowIn=obj.rbFlowIn.addDataToCircularBuffer(qPump_lpm.t, qPump_lpm.V);
            qDelayed=obj.rbFlowIn.lookUp(qPump_lpm.t-obj.td/86400);
            if obj.dT>obj.maxDT
                obj=obj.resetState(volGainLoss_m3);
            elseif obj.dT>0
                obj=obj.updateState(qDelayed, qPump_lpm.V, volGainLoss_m3, useOutputInjection, qPump_lpm.t);
            end
        end
        
        function obj = resetGLIfAboveThresholds(obj, prevGoodVolGainLoss_m3, volGainLoss_m3, volGainLossThreshold_m3_per_time_step, flowIn_lpm)
            if flowIn_lpm > obj.flowInLowLimForGLReset_lpm
                if abs(prevGoodVolGainLoss_m3 - volGainLoss_m3) > volGainLossThreshold_m3_per_time_step/2
                    obj = obj.resetGL(volGainLoss_m3);
                else
                    obj.timeSinceResetGL_s = obj.timeSinceResetGL_s + obj.dT;                    
                end
            else
                if abs(prevGoodVolGainLoss_m3 - volGainLoss_m3) > volGainLossThreshold_m3_per_time_step
                    obj = obj.resetGL(volGainLoss_m3);
                else
                    obj.timeSinceResetGL_s = obj.timeSinceResetGL_s + obj.dT;                    
                end
            end
        end
        
        function obj = resetGLWithSlidingWindowOfFlowIn(obj, prevGoodVolGainLoss_m3, volGainLoss_m3, volGainLossThreshold_m3_per_time_step)
            
            flowInIsStableAndPumpsRunning = obj.checkIfFlowInInRingBufferIsStable;
            
            if flowInIsStableAndPumpsRunning
                if abs(prevGoodVolGainLoss_m3 - volGainLoss_m3) > volGainLossThreshold_m3_per_time_step/2
                    obj = obj.resetGL(volGainLoss_m3);
                else
                    obj.timeSinceResetGL_s = obj.timeSinceResetGL_s + obj.dT;                    
                end
            else
                if abs(prevGoodVolGainLoss_m3 - volGainLoss_m3) > volGainLossThreshold_m3_per_time_step
                    obj = obj.resetGL(volGainLoss_m3);
                else
                    obj.timeSinceResetGL_s = obj.timeSinceResetGL_s + obj.dT;                    
                end
            end
        end
        
        function flowInIsStableAndPumpsRunning = checkIfFlowInInRingBufferIsStable(obj)
            
            flowInIsStableAndPumpsRunning = false;
            
            [Tbuffer_d_straight, Vbuffer_straight, ~, validDataPoints] = obj.rbFlowIn.deringbuffer;
            
            if ~any(validDataPoints) || sum(validDataPoints) < obj.flowInBufferSize_samples/2
                % no, or not enough, valid data points
                return
            end
            
            if all(Vbuffer_straight(validDataPoints) < 50)
                % pumps are considered not running
                return
            end
            
            if abs(max(Vbuffer_straight(validDataPoints)) - min(Vbuffer_straight(validDataPoints))) > 500
                % flow is not considered stable
                return
            end
            
            if diff(Tbuffer_d_straight(validDataPoints))*86400 > obj.maxDT
                % there is a gap in the data larger than the maximum
                % time-step
                return
            end
            
            flowInIsStableAndPumpsRunning = true;
            
        end
        
        function obj=resetState(obj, volGL_m3)
            % reset all states
            obj.x=[obj.volDrainback; volGL_m3; 0];
        end
        
        function obj=resetGL(obj, volGL_m3)
            % reset only the gain/loss estimate
            obj.x(2)=volGL_m3;
            obj.timeSinceResetGL_s = 0;
        end
        
        function obj=resetInt(obj, volGL_m3)
            % reset gain/loss estimate and integrator for output injection
            obj.x(2)=volGL_m3;
            obj.x(3)=0;
        end
        
        function obj=setTimeDelay(obj, newTd)
            obj.td=min(600, max(newTd,0));
        end
        
        function obj=updateKFlowline(obj, newK)
            if newK<1 && newK>0.0001 % must be between 0.0001 and 1
                obj.kFlowline=newK;
            end
        end
        
        function obj=updateK2Flowline(obj, newK2)
            if newK2<10 && newK2>-10 % must be between -2 and 2
                obj.k2Flowline=newK2;
            end
        end
        
        function obj=updateVolDrainback(obj, newVolDrainback)
            if newVolDrainback<30 && newVolDrainback>0 % must be between 0 and 30 m3
%                 volDiff = obj.volDrainback - newVolDrainback;
%                 obj.x(3) = obj.x(3) - volDiff;
                obj.volDrainback=newVolDrainback;
            end
        end
        
        function flowlineAndPitOutput = generateOutput(obj, volGainLossExpected_m3, isCalibratedExpPitVol_boolean)
            flowlineAndPitOutput = struct(...
                'volFlExpected_m3', obj.x(1),... % estimated volume in flow line
                'volGLExpected_m3', volGainLossExpected_m3,...
                'flowPitDer_lpm', obj.flowPitDer_lpm,...
                'isCalibratedExpPitVol', isCalibratedExpPitVol_boolean);
        end
        
        function plotRingbufferValues(obj)
            
            [tF, vF, ~, vFDp] = obj.rbFlowIn.deringbuffer;
                        
            figure;
            
            plot(tF(vFDp), vF(vFDp));
            datetick('x')
            grid;
        end
        
    end
    
end
