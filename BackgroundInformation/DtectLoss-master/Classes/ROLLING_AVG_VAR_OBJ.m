classdef ROLLING_AVG_VAR_OBJ
    %ROLLING_AVG_VAR_OBJ Compute the rolling average and variance.
    %   The method is taken from: http://jonisalonen.com/2014/efficient-and-accurate-rolling-standard-deviation/
    %   The object must be initialized with an array of data.
    
    properties (SetAccess = private)
        N; % window size
        average;
        variance;
        stdDev;
    end
    
    methods
        function obj=ROLLING_AVG_VAR_OBJ(initData)
            % initialize object with the an 1D array and use built in
            % Matlab-functions to compute the average, variance, and
            % standard deviation.
            obj.N=numel(initData);
            obj.average=mean(initData);
            obj.variance=var(initData);
            obj.stdDev=std(initData);
        end
        
        function obj=update(obj, x0, xN)
            % update calcualtions with the value going out of the window,
            % x0, and the new value, xN, going into the window.
            oldAvg=obj.average;
            newAvg=oldAvg+(xN-x0)/obj.N;
            obj.average=newAvg;
            obj.variance=max(0,obj.variance+(xN-x0)*(xN-newAvg+x0-oldAvg)/(obj.N-1));
            obj.stdDev=sqrt(obj.variance);
        end
        
        function obj = reset(obj)
            obj.average = 0;
            obj.variance = 0;
            obj.stdDev = 0;
        end
    end
    
end

