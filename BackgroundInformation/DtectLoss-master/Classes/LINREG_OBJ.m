classdef LINREG_OBJ
    %LINREG_OBJ Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        coeffs
        SSE
        SSR
        SST
        RMSE
        RSquared
    end
    
    properties (Constant)
        doDebug = false;
    end
    
    methods
        function obj = LINREG_OBJ()
            obj.coeffs = [];
            obj.SSE = [];
            obj.SST = [];
            obj.RMSE = [];
            obj.RSquared = [];
        end
        
        function obj = fitData(obj, X, y)
            n = numel(y);
            if size(X, 2) + 1 < n
                obj.coeffs = [X ones(n, 1)]\y;
                obj = obj.calcSSE(X, y);
                obj = obj.calcRSquared(X, y);
                if obj.doDebug
                    fprintf('RMSE: %0.2f, RSquared: %0.2f\n', obj.RMSE, obj.RSquared);
                end
            end
        end

        function y = predictFromData(obj, X)
            y = [X ones(size(X,1),1)]*obj.coeffs;
        end
        
        function obj = calcSSE(obj, X, y)
            yEst = obj.predictFromData(X);
            obj.SSE = sum((y - yEst).^2);
            m = size(X,2) + 1;
            n = size(X,1);
            if  n > m
                v = n - m;
                obj.RMSE = obj.SSE/v;
            end
        end
        
        function obj = calcSSR(obj, X, y)
            yEst = obj.predictFromData(X);
            obj.SSR = sum((yEst - mean(y)).^2);
        end
        
        function obj = calcSST(obj, y)
            obj.SST = sum((y - mean(y)).^2);
        end
        
        function obj = calcRSquared(obj, X, y)
            obj = obj.calcSSR(X, y);
            obj = obj.calcSST(y);
            if ~isempty(obj.SSR) && ~isempty(obj.SST) && obj.SST > 0
                obj.RSquared = obj.SSR/obj.SST;
            end
        end
    end
    
end
