classdef FLOWBACK_RECORDER_OBJ < DTECTLOSS_SUBMODULE
    %FLOWBACK_RECORDER_OBJ Keep track of previous flowbacks and
    %corresponding flow-rates.
    %   A number of flowbacks and corresponding flow-rates can be stored.
    %   The volume of the flowback is computed as the difference between
    %   the gain/loss volume just before a pumps off event, and the volume
    %   just before the pumps are turned back on. The average flow-rate in
    %   a period before pumps off is used as the corresponding flow-rate.
    %   The flow-rate must be within a defined min/max window during a
    %   time-period for the flowback to be valid and recorded.
    
    properties
        nFbs; % [int] number of flowbacks to record
        minVol; % [m3] a minimum volume which the flow back must have to be recorded
        minFlow; % [lpm] threshold for when pumps are considered to be off.
        flowTol; % [lpm] the difference between min and max flow-rate which defines steady flow
        
        nRbFlow; % [int] number of samples to use for the ringbuffer for flow-rate
        nRbVol; % [int] number of samples to use for the ringbuffer for gain/loss volume
        rbFlowIn; % RINGBUFFER_OBJ for flow-rate from rig pumps
        rbVol; % RINGBUFFER_OBJ for gain/loss volume
        rbExpVol; % RINGBUFFER_OBJ for expected gain/loss volume
        rbPresSP;% RINGBUFFER_OBJ for standpipe pressure
        
        idxStore; % [int] index to current row where the flowback should be stored
        flowInStore; % [lpm], size: [nFbs x nRbFlow] matrix with previous flow-rates during flowback
        timeFlowInStore; % [serial time] size: [nFbs x nRbFlow] matrix with time during flowback for flow rate
        volStore; % [m3], size: [nFbs x nRbVol] matrix with previous volumes during flowback
        timeVolStore; % [serial time] size: [nFbs x nRbVol] matrix with time during flowback for volume
        expVolStore; % [m3], size: [nFbs x nRbFlow] matrix with previous expected volumes during flowback
        timeExpVolStore; % [serial time] size: [nFbs x nRbFlow] matrix with time during expected flowback for volume
        presSPStore; % [bar], size: [nFbs x nRbFlow] matrix with previous pressures during flowback
        timePresSPStore; % [serial time] size: [nFbs x nRbFlow] matrix with time during flowback for pressure
        kStore; % [1/(s*(m3)^(exponentM-1))] size: [nFbs x 1] vector for storing computed k's for flowback
        V0Store; % [m3] size: [nFbs x 1] vector for storing volumes of flowbacks
        expV0Store; % [m3] size: [nFbs x 1] vector for storing volumes of expected flowbacks
        qStore; % [lpm] size: [nFbs x 1] vector for storing mean flow rate from rig pumps before flowback
        pumpOffIdxStore; % [int] size: [nFbs x 1] index to when the pump off event has been identified.
        volOffIdxStore; % [int] size: [nFbs x 1] index to when the pump off event has been identified.
        expVolOffIdxStore; % [int] size: [nFbs x 1] index to when the pump off event has been identified.
        
        idxCon; % [int] index to current row where the connection should be stored
        flowInCon; % [lpm], size: [nConnections x nRbFlow] matrix with previous flow-rates during connection
        timeFlowInCon; % [serial time] size: [nConnections x nRbFlow] matrix with time during connection for flow rate
        volCon; % [m3], size: [nConnections x nRbFlow] matrix with volumes during connection
        timeVolCon; % [serial time] size: [nConnections x nRbFlow] matrix with time during connection for volume
        expVolCon; % [m3], size: [nConnections x nRbFlow] matrix with expected volume during connection
        timeExpVolCon; % [serial time] size: [nConnections x nRbFlow] matrix with time of expected volume during connection
        isValidConnection; % [boolean], size [nConnections x 1] logical array which indicate if the connection is valid and can be used for learning.
        nValidDataPoints; % [int], size [nConnections x 1]
        mdl; % [linear regression model object]
        
        yStore; % [-] size: [nFbs x nRbVol] normalized time constant related to the drainback
        tStore; % [-] size: [nFbs x nRbVol] time for recording normalized drainback
        
        isInit; % [boolean] true if the ringbuffers have not been filled up after init
        
        cs; % CIRCULATION_STATES_OBJ for determining if circulating, not circulating, or drilling
        stateAtPrevTime; % CirculationState (Enum) at previous time-step
        timeSpentAtState; % [s] time spent at current state.
        timeSpentOfFlowbackExceeded; % [boolean]
        prevTimeSpentOfFlowbackExceeded; % [boolean]
        timeSpentOfRampUpExceeded; % [boolean]
        prevTimeSpentOfRampUpExceeded; % [boolean]
        storeConnectionDataTrigger; % [boolean]
        updateFapTimeTrigger; % [boolean]
        pumpsOn; % [boolean] flag indicating if the pumps are running
        
        params; % tuning parameters
    end
    
    properties (Constant)
        errVal = -0.99925;
        
        flowbackTimerLim_s = 500; % [s] when the pumps have been shut down for this period, then it is time to store the flow back data.
        rampUpTimerLim_s = 600; % [s] wait this duration after pumps have been started before storing connection data.
        
        dbLim_m3 = 0.3; % [m3] limit for when drainback starts
        flowbackDurationLim_s = 100; % [s] lower limit for how long a flowback must be to be valid.
        volDiffLim_m3PerS = 10e3/60e3; % [m3/s] upper limit for change in volGainLoss. Flowbacks with a higher rate of change is not valid.
        timeVolSamplingLim_s = 5; % [s] if volGainLoss is sampled with less than this period, then allow a higher rate of change in volume of a flowback.
        multiplierHighSamplingOfVolGainLoss = 2; % [float]
        volChangeLim_m3 = 2; % [m3] upper limit for range of volGainLoss in the period before flowback starts
        volWindowDuration_d = 300/86400; % [days] window which is used to consider the range of volGainLoss before flowback starts.
        nMinFlowValuesForInit = 10; % [int] minimium number of datapoints necessary to store in the ringbuffer before considering it ready for use 
        nMinVolValuesForInit = 10;  % [int] minimium number of datapoints necessary to store in the ringbuffer before considering it ready for use
        
        % parameters for the analyzeRamp method
        nRunningAvgWin = 5; % [int] number of samples to use for the running avg filtering.
        flowChangeTol_lpmPerS = 32; % [lpm/s]
        maxNPlateaus = 2; % maximum number of plateaus before computing steady state flow-rate.
        plateauTimeDef_s = 10; % [s] duration which defines a plateau
        ssDuration_s = 180; % [s] duration with steady flow-rate defining steady state
        startSearch_lpm = 200; % [lpm] flow-rate threshold which defines the start for the search for a plateau
        nSamplesToDiscardAtEndOfFlowIn = 5; % [int] number of samples to discard at the end of a flowIn ramp down time series to avoid including some of the ramp up.
        
        % parameter used by the getFlowbackStatistics method
        qWin_lpm = 250; % [lpm] flow-rate window, qDrill_lpm +/- qWin_lpm
        
        % parameters used by the calcParams method
        leastSquareTimeLimit_s = 200; % [s] upper time limit for flowback data which shall be included in the least squares fit.
        correctionFactorMeanV0UpperLim = 1.25; % [-] upper limit for correction of meanV0
        correctionFactorMeanV0LowerLim = 0.75; % [-] lower limit for correction of meanV0
        
        % parameters for connection data and learning
        nConToRecord = 8; % number of connections which are recorded and used for machine learning
        timeSampling_s = 3; % [s]
        maxDelay_samples = 110; % [int]
%         delayArray_samples = flip([1:1:24  28:5:110]); % [array of int]
        delayArray_samples = flip(1:110); % [array of int]
        nDelays = 110; % [int]
        timeDiffBetweenStackedTimeSeries_s = 180; % [s]
        timeToIncludeBeforePumpShutDown_s = 400; % [s]
        flowInMaxLim_lpm = 10000; % [lpm]
        volGainLossErrorLim_m3 = 1; % [m3]
        removeOffsetThreshold = 3; % [m3/sample]
        deltaTimeForMissingData_s = 120; % [s]
        connectionDurationLim_s = 20; % [s]
        multiplierForVolDiffLimDuringConnection = 3; % [int] 
        dispDebug = false;
    end
    
    methods
        function obj=FLOWBACK_RECORDER_OBJ()
            obj.nFbs;
            obj.minVol;
            obj.minFlow;
            obj.flowTol;
            
            obj.nRbFlow;
            obj.nRbVol;
            obj.rbFlowIn;
            obj.rbVol;
            obj.rbExpVol;
            obj.rbPresSP;
            
            obj.idxStore;
            obj.flowInStore;
            obj.timeFlowInStore;
            obj.volStore;
            obj.timeVolStore;
            obj.expVolStore;
            obj.timeExpVolStore;
            obj.presSPStore;
            obj.timePresSPStore;
            
            obj.idxCon;
            obj.flowInCon;
            obj.timeFlowInCon;
            obj.volCon;
            obj.timeVolCon;
            obj.expVolCon;
            obj.timeExpVolCon;
            obj.isValidConnection;
            obj.nValidDataPoints;
            
            obj.kStore;
            obj.V0Store;
            obj.expV0Store;
            obj.qStore;
            obj.pumpOffIdxStore;
            obj.volOffIdxStore;
            obj.expVolOffIdxStore;
            
            obj.yStore;
            obj.tStore;
            
            obj.isInit;
                        
            obj.cs;
            obj.stateAtPrevTime;
            obj.timeSpentAtState;
            obj.timeSpentOfFlowbackExceeded;
            obj.prevTimeSpentOfFlowbackExceeded;
            obj.timeSpentOfRampUpExceeded;
            obj.prevTimeSpentOfRampUpExceeded;
            obj.storeConnectionDataTrigger;
            obj.updateFapTimeTrigger;
            obj.pumpsOn;
        end
        
        function obj = initialize(obj, nFlowbacks, minVolume, minFlowRate, flowTolerance, dtFlowrate, dtVolGL, timeDuration, params)
            % the number of samples for the ringbuffer for flow-rate is
            % computed using timeDuration (time-window for recording of
            % flow-rate and gain/loss volume) [s], and dtFlowrate (average
            % time-step for the flow-rate) [s]. Similar for the gain/loss
            % volume.
            
            obj.params = params;
            obj.nFbs = nFlowbacks;
            obj.minVol = minVolume;
            obj.minFlow = minFlowRate;
            obj.flowTol = flowTolerance;
            
            obj.nRbFlow = ceil(timeDuration/dtFlowrate);
            obj.nRbVol = ceil(timeDuration/dtVolGL);
            obj.rbFlowIn = RINGBUFFER_OBJ(obj.nRbFlow);
            obj.rbVol = RINGBUFFER_OBJ(obj.nRbVol);
            obj.rbPresSP = RINGBUFFER_OBJ(obj.nRbFlow);
            obj.rbExpVol = RINGBUFFER_OBJ(obj.nRbFlow);

            obj.idxStore = 1;
            
            obj.flowInStore = zeros(nFlowbacks, obj.nRbFlow);
            obj.timeFlowInStore = zeros(nFlowbacks, obj.nRbFlow);
            obj.volStore = zeros(nFlowbacks, obj.nRbVol);
            obj.timeVolStore = zeros(nFlowbacks, obj.nRbVol);
            obj.expVolStore = zeros(nFlowbacks, obj.nRbFlow);
            obj.timeExpVolStore = zeros(nFlowbacks, obj.nRbFlow);
            
            obj.idxCon = 1;
            obj.flowInCon = nan(obj.nConToRecord, obj.nRbFlow);
            obj.timeFlowInCon = nan(obj.nConToRecord, obj.nRbFlow);
            obj.volCon = nan(obj.nConToRecord, obj.nRbFlow);
            obj.timeVolCon = nan(obj.nConToRecord, obj.nRbFlow);
            obj.expVolCon = nan(obj.nConToRecord, obj.nRbFlow);
            obj.timeExpVolCon = nan(obj.nConToRecord, obj.nRbFlow);
            obj.isValidConnection = false(obj.nConToRecord, 1);
            obj.nValidDataPoints = zeros(obj.nConToRecord, 1);
                        
            obj.kStore = zeros(nFlowbacks, 1);
            obj.V0Store = zeros(nFlowbacks, 1);
            obj.expV0Store = zeros(nFlowbacks, 1);
            obj.qStore = zeros(nFlowbacks, 1);
            obj.pumpOffIdxStore = zeros(nFlowbacks, 1);
            obj.volOffIdxStore = zeros(nFlowbacks, 1);
            obj.expVolOffIdxStore = zeros(nFlowbacks, 1);
            
            obj.yStore = zeros(nFlowbacks, obj.nRbVol);
            obj.tStore = zeros(nFlowbacks, obj.nRbVol);
                        
            obj.isInit = true;
                        
            obj.cs = CIRCULATION_STATES_OBJ();
            obj.stateAtPrevTime = obj.cs.currentState;
            obj.timeSpentAtState = 0;
            obj.timeSpentOfFlowbackExceeded = false;
            obj.prevTimeSpentOfFlowbackExceeded = false;
            obj.updateFapTimeTrigger = false;
            obj.timeSpentOfRampUpExceeded = false;
            obj.prevTimeSpentOfRampUpExceeded = false;
            obj.storeConnectionDataTrigger = false;
            obj.pumpsOn = false;
            
            obj.mdl = [];
        end
        
        function obj = softReset(obj)
            
            obj.idxStore = 1;
            
            obj.flowInStore = zeros(obj.nFbs, obj.nRbFlow);
            obj.timeFlowInStore = zeros(obj.nFbs, obj.nRbFlow);
            obj.volStore = zeros(obj.nFbs, obj.nRbVol);
            obj.timeVolStore = zeros(obj.nFbs, obj.nRbVol);
            obj.expVolStore = zeros(obj.nFbs, obj.nRbFlow);
            obj.timeExpVolStore = zeros(obj.nFbs, obj.nRbFlow);
            
            obj.idxCon = 1;
            obj.flowInCon = nan(obj.nConToRecord, obj.nRbFlow);
            obj.timeFlowInCon = nan(obj.nConToRecord, obj.nRbFlow);
            obj.volCon = nan(obj.nConToRecord, obj.nRbFlow);
            obj.timeVolCon = nan(obj.nConToRecord, obj.nRbFlow);
            obj.expVolCon = nan(obj.nConToRecord, obj.nRbFlow);
            obj.timeExpVolCon = nan(obj.nConToRecord, obj.nRbFlow);
            obj.isValidConnection = false(obj.nConToRecord, 1);
            obj.nValidDataPoints = zeros(obj.nConToRecord, 1);
            
            obj.kStore = zeros(obj.nFbs, 1);
            obj.V0Store = zeros(obj.nFbs, 1);
            obj.expV0Store = zeros(obj.nFbs, 1);
            obj.qStore = zeros(obj.nFbs, 1);
            obj.pumpOffIdxStore = zeros(obj.nFbs, 1);
            obj.volOffIdxStore = zeros(obj.nFbs, 1);
            obj.expVolOffIdxStore = zeros(obj.nFbs, 1);
            
            obj.yStore = zeros(obj.nFbs, obj.nRbVol);
            obj.tStore = zeros(obj.nFbs, obj.nRbVol);
            
            obj.isInit = true;
            
            obj.cs = CIRCULATION_STATES_OBJ();
            obj.stateAtPrevTime = obj.cs.currentState;
            obj.timeSpentAtState = 0;
            obj.timeSpentOfFlowbackExceeded = false;
            obj.prevTimeSpentOfFlowbackExceeded = false;
            obj.updateFapTimeTrigger = false;
            obj.timeSpentOfRampUpExceeded = false;
            obj.prevTimeSpentOfRampUpExceeded = false;
            obj.storeConnectionDataTrigger = false;
            obj.pumpsOn = false;
            
            obj.mdl = [];
        end        
        
        function obj = addVolToRingbuffer(obj, volTime_d, vol_m3)
            % only add volume to ringbuffer is new timestamp is greater
            % than the previous.
            
            if vol_m3 ~= -0.99925 && volTime_d ~= -0.99925
                obj.rbVol = obj.rbVol.addDataToCircularBuffer(volTime_d, vol_m3);
                obj = obj.updateInitState;
            end
        end
        
        function obj = addExpVolToRingbuffer(obj, volTime_d, vol_m3)
            % only add expected volume to ringbuffer is new timestamp is
            % greater than the previous.
            
            if ~isempty(volTime_d) && ~isempty(vol_m3) && vol_m3 ~= -0.99925 && volTime_d ~= -0.99925
                obj.rbExpVol = obj.rbExpVol.addDataToCircularBuffer(volTime_d, vol_m3);
            end
        end
        
        function obj = addFlowToRingbuffer(obj, flowInTime_d, flowInRate_lpm)
            % only add flow-rate to ringbuffer is new timestamp is greater
            % than the previous.
            obj.rbFlowIn = obj.rbFlowIn.addDataToCircularBuffer(flowInTime_d, flowInRate_lpm);
            obj = obj.updateInitState;
        end
        
        function obj = addPresToRingbuffer(obj, presSPTime_d, presSP_bar)
            obj.rbPresSP = obj.rbPresSP.addDataToCircularBuffer(presSPTime_d, presSP_bar);
        end
        
        function plotRingbufferValues(obj)
            
            [tF, vF, ~, vFDp] = obj.rbFlowIn.deringbuffer;
            [tV, vV, ~, vVDp] = obj.rbVol.deringbuffer;
            [tExpV, vExpV, ~, vExpVDp] = obj.rbExpVol.deringbuffer;
            
            figure;
            
            ax1=subplot(211);
            plot(tF(vFDp), vF(vFDp));
            datetick('x')
            grid;
            ax2=subplot(212);
            stairs(tV(vVDp), vV(vVDp));
            hold on;
            stairs(tExpV(vExpVDp), vExpV(vExpVDp));
            datetick('x')
            grid;
            legend('Actual', 'Expected', 'Location', 'NorthWest')
            linkaxes([ax1, ax2], 'x');
        end
        
        function obj = updateCirculationState(obj, flowIn_lpm, depthBit_m, depthHole_m)
            % update state machine for circulation
            
            obj.stateAtPrevTime = obj.cs.currentState;
            obj.cs = obj.cs.stateMachine(flowIn_lpm, depthBit_m, depthHole_m);
            
            if obj.pumpHasBeenTurnedOn
                obj.pumpsOn = true;
            elseif obj.pumpHasBeenTurnedOff
                obj.pumpsOn = false;
            end
        end
        
        function pumpHasBeenTurnedOn_bool = pumpHasBeenTurnedOn(obj)
            
            pumpHasBeenTurnedOn_bool = obj.cs.currentState == CirculationStates.circulating...
                && obj.stateAtPrevTime == CirculationStates.notCirculating;
        end
        
        function pumpHasBeenTurnedOff_bool = pumpHasBeenTurnedOff(obj)
            
            pumpHasBeenTurnedOff_bool = ...
                obj.cs.currentState==CirculationStates.notCirculating...
                && (obj.stateAtPrevTime==CirculationStates.circulating...
                || obj.stateAtPrevTime==CirculationStates.drilling);
        end

        function obj = updateTimeSpentAtState(obj, timeStep_s)
            
            if obj.pumpHasBeenTurnedOn || obj.pumpHasBeenTurnedOff
                obj.timeSpentAtState = 0;
                obj.timeSpentOfFlowbackExceeded = false;
                obj.prevTimeSpentOfFlowbackExceeded = false;
                obj.timeSpentOfRampUpExceeded = false;
                obj.prevTimeSpentOfRampUpExceeded = false;
            else
                obj.timeSpentAtState = obj.timeSpentAtState + timeStep_s;
            end
            
            obj.prevTimeSpentOfFlowbackExceeded = obj.timeSpentOfFlowbackExceeded;
            if obj.timeSpentAtState > obj.flowbackTimerLim_s
                obj.timeSpentOfFlowbackExceeded = true;
            end
            
            obj.prevTimeSpentOfRampUpExceeded = obj.timeSpentOfRampUpExceeded;
            if obj.timeSpentAtState > obj.rampUpTimerLim_s
                obj.timeSpentOfRampUpExceeded = true;
            end
            
            obj.updateFapTimeTrigger = ~obj.prevTimeSpentOfFlowbackExceeded...
                && obj.timeSpentOfFlowbackExceeded...
                && obj.cs.currentState == CirculationStates.notCirculating;
            
            obj.storeConnectionDataTrigger = ~obj.prevTimeSpentOfRampUpExceeded...
                && obj.timeSpentOfRampUpExceeded...
                && obj.cs.currentState ~= CirculationStates.notCirculating;
        end
        
        function [obj, fap] = updateFap(obj, fap)
            % This function updates the flowline and pit object with new
            % values for kFlowLine and volDrainback, if there has been a
            % connection with a sufficently large flowback

            if (obj.cs.currentState == CirculationStates.circulating && obj.stateAtPrevTime == CirculationStates.notCirculating)...
                    || obj.updateFapTimeTrigger
                if ~obj.isInit
                    obj = obj.clearStoredDataAtCurrentIndex;
                    obj = obj.storeFlowbackData;
                    
                    % find when the pumps start to ramp down and the
                    % steady state flow-rate before ramping down.
                    [timeOfPumpRamp_d, qDrill_lpm] = obj.analyzeRamp(obj.timeFlowInStore(obj.idxStore,:), obj.flowInStore(obj.idxStore,:), 'down');
                    
                    obj = obj.storeShutDownIndicies(timeOfPumpRamp_d);
                    
                    [isValidFlowback, newV0] = obj.qualifyFlowback(qDrill_lpm);
                    
                    if isValidFlowback && ~isempty(timeOfPumpRamp_d) && ~isempty(qDrill_lpm)
                        idxVol = obj.getLastValidVolIdxAndKIdx;
                        
                        if ~isempty(idxVol)
                            [db, volTime] = obj.getDrainbackData(idxVol);
                            obj = obj.storeLeastSquaresData(db, volTime, idxVol);
                            
                            expDb = obj.getExpDrainbackData(idxVol);
                            if ~isempty(expDb)
                                obj.expV0Store(idxVol) = max(expDb);
                            end

                            % calculate new coefficient using least squares
                            [obj, fap] = obj.storeAndUpdateFlowlineParameters(fap, newV0, qDrill_lpm);
                            if obj.dispDebug
                                fprintf('Time: %s. New time-constant: %0.1f s, qDrill: %0.2f lpm, mean V0: %0.2f m3.\n',...
                                    datestr(obj.timeVolStore(obj.idxStore,obj.volOffIdxStore(obj.idxStore))),...
                                    1/fap.kFlowline, obj.qStore(obj.idxStore), fap.volDrainback);
                            end
                        end
                    end
                    % iterate the counter for storing snapshots from
                    % ringbuffers and constants
                    obj.idxStore = obj.idxStore+1;
                    if obj.idxStore>obj.nFbs
                        obj.idxStore = 1;
                    end
                end
            end
        end
        
        function obj = updateConnectionData(obj)
            
            if obj.storeConnectionDataTrigger && ~obj.isInit
                [isAValidConnection, timeBase_d, flowInCurrentConClipped,...
                    volCurrentConClipped, expVolCurrentConClipped] = obj.qualifyConnection;
                if ~isAValidConnection
                    return
                end
                
                obj = obj.clearStoredConnectionDataAtCurrentIndex;
                obj = obj.storeConnectionData(timeBase_d, flowInCurrentConClipped,...
                    volCurrentConClipped, expVolCurrentConClipped);
                obj = obj.fitConnectionData;
                
%                 plotConnectionDataDebug(obj, 0);
%                 pause
                
                obj.idxCon = obj.idxCon + 1;
                if obj.idxCon > obj.nConToRecord
                    obj.idxCon = 1;
                end
            end
        end
        
        function obj = fitConnectionData(obj)
            
            nC = sum(obj.isValidConnection);
            
            if nC == 0
                return
            end
            
            errorMat = nan(nC, obj.nRbFlow);
            flowInMat = nan(nC, obj.nRbFlow);
            deltaTimeMat = zeros(nC, obj.nRbFlow);
            rowCounter = 1;
            conRange = 1:obj.nConToRecord;
            conRange = conRange(obj.isValidConnection);
            for ii = conRange
                errorMat(rowCounter, :) = obj.volCon(ii, :) - obj.expVolCon(ii, :);
                flowInMat(rowCounter, :) = obj.flowInCon(ii, :);
                deltaTimeMat(rowCounter, 1:obj.nValidDataPoints(ii) - 1) = diff(obj.timeVolCon(ii, 1:obj.nValidDataPoints(ii)))*86400;
                deltaTimeMat(rowCounter, obj.nValidDataPoints(ii)) = obj.timeDiffBetweenStackedTimeSeries_s;
                rowCounter = rowCounter + 1;
            end
            
            flowArray = reshape(flowInMat', nC*obj.nRbFlow, 1);
            deltaTimeArray = reshape(deltaTimeMat', nC*obj.nRbFlow, 1);
            timeArray = [0; cumsum(deltaTimeArray(1:end-1))];
            errorArray = reshape(errorMat', nC*obj.nRbFlow, 1);
            
            noNanMask = ~isnan(errorArray) & ~isnan(flowArray) & ~isnan(timeArray);
            
            flowArrayWoNan = flowArray(noNanMask);
            timeArrayWoNan = timeArray(noNanMask);
            errorArrayWoNan = errorArray(noNanMask);
            
            timeBaseResampled = timeArrayWoNan(1):obj.timeSampling_s:timeArrayWoNan(end);
            
            flowArrayInterp = interp1(timeArrayWoNan, flowArrayWoNan, timeBaseResampled);
            errorArrayInterp = interp1(timeArrayWoNan, errorArrayWoNan, timeBaseResampled);
            
            noNanMask = ~isnan(errorArrayInterp);
            
            flowArrayWoNanInterp = flowArrayInterp(noNanMask);
            errorArrayWoNanInterp = errorArrayInterp(noNanMask);
            
            X = zeros(size(flowArrayWoNanInterp, 2) - obj.maxDelay_samples, obj.nDelays + 1);
            X(:, end) = flowArrayWoNanInterp(obj.maxDelay_samples + 1:end)';
            
            for jj = 1:obj.nDelays
                X(:,jj) = flowArrayWoNanInterp(obj.maxDelay_samples - obj.delayArray_samples(jj) + 1 :...
                    end - obj.delayArray_samples(jj));
            end
            
            y = errorArrayWoNanInterp(obj.maxDelay_samples + 1:end)';
            
            if obj.dispDebug
                tic
            end
            
            if isempty(obj.mdl)
                obj.mdl = LINREG_OBJ();
            end
            obj.mdl = obj.mdl.fitData(X, y);
            
            if obj.dispDebug
                toc
            end            
        end
        
        function yPred = predictFromModel(obj)
            [~, val] = obj.pickDelayedSamples;
            yPred = obj.mdl.predictFromData(min(max(val, 0), obj.flowInMaxLim_lpm));
        end
        
        function [time_d, val] = pickDelayedSamples(obj)
            
            [t, v, dt_avg_d, validDataPoints] = obj.rbFlowIn.deringbuffer();
            
            if ~all(validDataPoints)
                tValid_d = t(validDataPoints);
                vValid = v(validDataPoints);
                nInvalidDataPoints = sum(~validDataPoints);
                
                [timePadded, valuePadded] = obj.rbFlowIn.padMissingValues(tValid_d, vValid, dt_avg_d, nInvalidDataPoints, false);
                tPad = [timePadded tValid_d];
                vPad = [valuePadded vValid];
                
                time_d = [tPad(end) - obj.delayArray_samples*obj.timeSampling_s/86400 tPad(end)];
                val = interp1(tPad, vPad, time_d);
            else
                time_d = [t(end) - obj.delayArray_samples*obj.timeSampling_s/86400 t(end)];
                val = interp1(t, v, time_d);
            end
            
        end
        
        function [isValidConnection, timeBase_d, flowInCurrentConClipped,...
                volCurrentConClipped, expVolCurrentConClipped] = qualifyConnection(obj)
            
            isValidConnection = false;
            timeBase_d = [];
            flowInCurrentConClipped = [];
            volCurrentConClipped = [];
            expVolCurrentConClipped = [];
                        
            [timeFlowInCurrentCon_d, flowInCurrentCon] = obj.rbFlowIn.deringbufferAndResample(true);
            
            if max(diff(timeFlowInCurrentCon_d)*86400) > obj.deltaTimeForMissingData_s
                if obj.dispDebug
                    fprintf('Time: %s. Discarding connection due to missing data in buffer.\n',...
                        datestr(timeFlowInCurrentCon_d(end)));
                end
                return
            end
            
            connectionEndIdx = find(flowInCurrentCon <= obj.startSearch_lpm, 1, 'last');
            if isempty(connectionEndIdx)
                if obj.dispDebug
                    fprintf('Time: %s. Discarding connection because the end of the ramp down was not found.\n',...
                        datestr(timeFlowInCurrentCon_d(end)));
                end
                return
            end
            
            connectionStartIdx = find(flowInCurrentCon(1:max(1, connectionEndIdx-1)) > obj.startSearch_lpm, 1, 'last');
            if isempty(connectionStartIdx)
                if obj.dispDebug
                    fprintf('Time: %s. Discarding connection because the start of the ramp down was not found.\n',...
                        datestr(timeFlowInCurrentCon_d(connectionEndIdx)));
                end
                return
            end
            
            connectionDuration_s = (timeFlowInCurrentCon_d(connectionEndIdx) - timeFlowInCurrentCon_d(connectionStartIdx))*86400; % in seconds
            if connectionDuration_s <= obj.connectionDurationLim_s
                if obj.dispDebug
                    fprintf('Time: %s. Discarding connection due to short duration.\n',...
                        datestr(timeFlowInCurrentCon_d(connectionStartIdx)));
                end
                return
            end
            
            timeStartOfPumpRampDown =...
                obj.analyzeRamp(timeFlowInCurrentCon_d(1:connectionStartIdx), flowInCurrentCon(1:connectionStartIdx), 'down');
            if isempty(timeStartOfPumpRampDown)
                if obj.dispDebug
                    fprintf('Time: %s. Discarding connection since a period with stable flow prior to the connection was not found.\n',...
                        datestr(timeFlowInCurrentCon_d(connectionStartIdx)));
                end
                return
            end
                        
            % the mean absolute error between model and measured vol gain loss
            % must be less than a limit
            [timeVolCurrentCon_d, volCurrentCon] = obj.rbVol.deringbufferAndResample(true);
            [timeExpVolCurrentCon_d, expVolCurrentCon] = obj.rbExpVol.deringbufferAndResample(true);
%             volCurrentConWoOffset = obj.removeOffsetFromTimeseries(volCurrentCon, obj.removeOffsetThreshold);
%             expVolCurrentConWoOffset = obj.removeOffsetFromTimeseries(expVolCurrentCon, obj.removeOffsetThreshold);
            volCurrentConWoOffset = volCurrentCon;
            expVolCurrentConWoOffset = expVolCurrentCon;
            volCurrentConInterp = interp1(timeVolCurrentCon_d, volCurrentConWoOffset, timeExpVolCurrentCon_d ,'previous');
            
            noNanMask = ~isnan(volCurrentConInterp) & ~isnan(expVolCurrentConWoOffset) & ~isnan(timeExpVolCurrentCon_d);
            
            volCurrentConInterpWoNan = volCurrentConInterp(noNanMask);
            expVolCurrentConWoNan = expVolCurrentConWoOffset(noNanMask);
            timeExpVolCurrentConWoNan_d = timeExpVolCurrentCon_d(noNanMask);
            
            idxConStart = find(timeExpVolCurrentConWoNan_d >= timeStartOfPumpRampDown - obj.timeToIncludeBeforePumpShutDown_s/86400, 1);
            volCurrentConClipped = volCurrentConInterpWoNan(idxConStart:end);
            expVolCurrentConClipped = expVolCurrentConWoNan(idxConStart:end) + volCurrentConInterpWoNan(idxConStart) - expVolCurrentConWoNan(idxConStart);
            if mean(abs(volCurrentConClipped - expVolCurrentConClipped)) > obj.volGainLossErrorLim_m3 || isempty(obj.mdl)
                expVolCurrentConClipped = obj.scaleExpVolCurve(volCurrentConClipped, expVolCurrentConClipped);
            end
            
            % compute the maximum rate of change in gain/loss
            % volume [m3/s].
            timeBase_d = timeExpVolCurrentConWoNan_d(idxConStart:end);
            maxVolDiff = max(abs((diff(volCurrentConClipped)./diff(timeBase_d)/86400)));
            if isempty(maxVolDiff) || maxVolDiff >= obj.volDiffLim_m3PerS*obj.multiplierForVolDiffLimDuringConnection
                if obj.dispDebug
                    fprintf('Time: %s. Discarding connection since the change in flow rate (%0.2f lpm) into the pits is larger than %0.2f lpm.\n',...
                        datestr(timeFlowInCurrentCon_d(connectionStartIdx)), maxVolDiff*60e3, obj.multiplierForVolDiffLimDuringConnection*obj.volDiffLim_m3PerS*60e3);
                end
                return
            end
            flowInCurrentConWoNan = flowInCurrentCon(noNanMask);
            flowInCurrentConClipped = flowInCurrentConWoNan(idxConStart:end);
            
            isValidConnection = true;
        end
        
        function obj = clearStoredConnectionDataAtCurrentIndex(obj)
            
            obj.flowInCon(obj.idxCon, :) = nan(1, obj.nRbFlow);
            obj.timeFlowInCon(obj.idxCon, :) = nan(1, obj.nRbFlow);
            obj.volCon(obj.idxCon, :) = nan(1, obj.nRbFlow);
            obj.timeVolCon(obj.idxCon, :) = nan(1, obj.nRbFlow);
            obj.expVolCon(obj.idxCon, :) = nan(1, obj.nRbFlow);
            obj.timeExpVolCon(obj.idxCon, :) = nan(1, obj.nRbFlow);
            obj.isValidConnection(obj.idxCon) = false;
            obj.nValidDataPoints(obj.idxCon) = 0;
        end
        
        function obj = clearStoredDataAtCurrentIndex(obj)
            % make sure that the data storages are reset before storing any
            % new data
            
            obj.flowInStore(obj.idxStore,:) = zeros(1,obj.nRbFlow);
            obj.timeFlowInStore(obj.idxStore,:) = zeros(1,obj.nRbFlow);
            obj.volStore(obj.idxStore,:) = zeros(1,obj.nRbVol);
            obj.timeVolStore(obj.idxStore,:) = zeros(1,obj.nRbVol);
            obj.expVolStore(obj.idxStore,:) = zeros(1,obj.nRbFlow);
            obj.timeExpVolStore(obj.idxStore,:) = zeros(1,obj.nRbFlow);
            obj.kStore(obj.idxStore) = 0;
            obj.V0Store(obj.idxStore) = 0;
            obj.expV0Store(obj.idxStore) = 0;
            obj.qStore(obj.idxStore) = 0;
            obj.pumpOffIdxStore(obj.idxStore) = 0;
            obj.volOffIdxStore(obj.idxStore) = 0;
            obj.expVolOffIdxStore(obj.idxStore) = 0;
            obj.yStore(obj.idxStore,:) = zeros(1,obj.nRbVol);
            obj.tStore(obj.idxStore,:) = zeros(1,obj.nRbVol);
                        
        end
        
        function obj = storeFlowbackData(obj)
            
            [obj.timeFlowInStore(obj.idxStore,:), obj.flowInStore(obj.idxStore,:)] = storeOnlyNonEmptyData(obj.rbFlowIn);
            [obj.timeVolStore(obj.idxStore,:), obj.volStore(obj.idxStore,:)] = storeOnlyNonEmptyData(obj.rbVol);
            [obj.timeExpVolStore(obj.idxStore,:), obj.expVolStore(obj.idxStore,:)] = storeOnlyNonEmptyData(obj.rbExpVol);
            
            function [t, v] = storeOnlyNonEmptyData(ringBuffer)
                [tResampled, vResampled] = ringBuffer.deringbufferAndResample(true);
                if ~isempty(tResampled) || ~isempty(vResampled)
                    t = tResampled;
                    v = vResampled;
                else
                    t = zeros(1, ringBuffer.bufferSizeSamples);
                    v = zeros(1, ringBuffer.bufferSizeSamples);
                end
            end
        end
        
        function obj = storeConnectionData(obj, timeBase_d, flowInCurrentConClipped,...
                volCurrentConClipped, expVolCurrentConClipped)
            
            nDataPoints = numel(timeBase_d);
            
            obj.timeFlowInCon(obj.idxCon, 1:nDataPoints) = timeBase_d;
            obj.timeVolCon(obj.idxCon, 1:nDataPoints) = timeBase_d;
            obj.timeExpVolCon(obj.idxCon, 1:nDataPoints) = timeBase_d;
            
            obj.flowInCon(obj.idxCon, 1:nDataPoints) = flowInCurrentConClipped;
            obj.volCon(obj.idxCon, 1:nDataPoints) = volCurrentConClipped;
            obj.expVolCon(obj.idxCon, 1:nDataPoints) = expVolCurrentConClipped;

            obj.isValidConnection(obj.idxCon) = true;
            obj.nValidDataPoints(obj.idxCon) = nDataPoints;
        end
        
        function obj = storeShutDownIndicies(obj, timeOfPumpRamp)
            
            % find indicies to when pumps have been shut off.
            pumpOffIdx = find(obj.flowInStore(obj.idxStore,1:end-1)>obj.minFlow,1,'last');
            if isempty(pumpOffIdx), pumpOffIdx = 1; end
            pumpOffTime = obj.timeFlowInStore(obj.idxStore,pumpOffIdx);
            obj.pumpOffIdxStore(obj.idxStore) = pumpOffIdx;
            
            if ~isempty(timeOfPumpRamp)
                % index to volGainLoss where the rig pumps start to ramp
                % down
                volOffIdx = find(obj.timeVolStore(obj.idxStore,:) <= timeOfPumpRamp, 1, 'last');
                expVolOffIdx = find(obj.timeExpVolStore(obj.idxStore,:) <= timeOfPumpRamp, 1, 'last');
            else
                % index to pump off for volGainLoss
                volOffIdx = max(1,find(obj.timeVolStore(obj.idxStore,:) <= pumpOffTime, 1, 'last')-1);
                expVolOffIdx = max(1,find(obj.timeExpVolStore(obj.idxStore,:) <= pumpOffTime, 1, 'last')-1);
            end
            
            % if volOffIdx is empty, then the ramp down is not
            % recorded and the flowback data is not valid. The
            % index is anyhow stored to avoid errors later in the
            % code.
            if isempty(volOffIdx), volOffIdx = 1; end
            obj.volOffIdxStore(obj.idxStore) = volOffIdx;
            if isempty(expVolOffIdx), expVolOffIdx = 1; end
            obj.expVolOffIdxStore(obj.idxStore) = expVolOffIdx;
        end
        
        function [isValidFlowback, newV0] = qualifyFlowback(obj, qDrill_lpm)
            
            isValidFlowback = false;
            newV0 = [];
            
            if isempty(qDrill_lpm)
                return
            end
            
            volOffIdx = obj.volOffIdxStore(obj.idxStore);
            if volOffIdx <= 1
                return
            end
            
            % compute the maximum rate of change in gain/loss
            % volume [m3/s].
            dt = diff(obj.timeVolStore(obj.idxStore,volOffIdx:end));
            meanDt = mean(dt);
            maxVolDiff = max(abs((diff(obj.volStore(obj.idxStore,volOffIdx:end))./dt/86400)));
            if (meanDt > obj.timeVolSamplingLim_s && (isempty(maxVolDiff) || maxVolDiff >= obj.volDiffLim_m3PerS))...
                    || (meanDt <= obj.timeVolSamplingLim_s && (isempty(maxVolDiff)...
                    || maxVolDiff >= obj.multiplierHighSamplingOfVolGainLoss*obj.volDiffLim_m3PerS))
                if obj.dispDebug
                    fprintf('Time: %s. Discarding flowback due to too high rate of change.\n',...
                        datestr(obj.timeVolStore(obj.idxStore, obj.volOffIdxStore(obj.idxStore))));
                end
                return
            end
            
            % check that the change in gain/loss in a time period before
            % flow back starts is sufficiently small before changing any
            % parameters.
            volWindowIdx = find(obj.timeVolStore(obj.idxStore,:) < obj.timeVolStore(obj.idxStore,volOffIdx) - obj.volWindowDuration_d, 1, 'last');
            if isempty(volWindowIdx), volWindowIdx = 1; end
            if max(obj.volStore(obj.idxStore, volWindowIdx:volOffIdx))-min(obj.volStore(obj.idxStore, volWindowIdx:volOffIdx)) >= obj.volChangeLim_m3
                if obj.dispDebug
                    fprintf('Time: %s. Discarding flowback due to sudden change in volume before flowback started.\n',...
                        datestr(obj.timeVolStore(obj.idxStore, obj.volOffIdxStore(obj.idxStore))));
                end
                return
            end
            
            % compute the duration of the flowback. If it is too
            % short, then do not estimate any parameters.
            flowbackDuration_s = (obj.timeVolStore(obj.idxStore,end) - obj.timeVolStore(obj.idxStore,volOffIdx))*86400; % in seconds
            if flowbackDuration_s <= obj.flowbackDurationLim_s
                if obj.dispDebug
                    fprintf('Time: %s. Discarding flowback due to short duration of flowback.\n',...
                        datestr(obj.timeVolStore(obj.idxStore, obj.volOffIdxStore(obj.idxStore))));
                end
                return
            end
            
            % calculate new initial volume in flow line
            % before ramping down rig pumps.
            newV0 = max(obj.volStore(obj.idxStore,volOffIdx:end))-obj.volStore(obj.idxStore,volOffIdx);
            if newV0<=obj.minVol
                if obj.dispDebug
                    fprintf('Time: %s. Discarding flowback due to small flowback volume: %0.2f.\n',...
                        datestr(obj.timeVolStore(obj.idxStore, obj.volOffIdxStore(obj.idxStore))), newV0);
                end
                return
            end
            
            isVolumeWithinTolerance = obj.compareFlowbackVolumeWithPrev(newV0, qDrill_lpm);
            if ~isVolumeWithinTolerance
                if obj.dispDebug
                    fprintf('Time: %s. Discarding flowback since it is outside 3*stddev compared to the previous flowbacks.\n',...
                        datestr(obj.timeVolStore(obj.idxStore, obj.volOffIdxStore(obj.idxStore))));
                end
                return
            end
            
            % if the code reaches this line, then the flowback is
            % considered valid.
            isValidFlowback = true;
        end
        
        function isVolumeWithinTolerance = compareFlowbackVolumeWithPrev(obj, newV0, qDrill_lpm)
            
            isVolumeWithinTolerance = false;
            
            if isempty(qDrill_lpm)
                return
            end
            
            [meanV0, stdV0] = obj.getFlowbackStatistics(qDrill_lpm);
            
            if isempty(meanV0) || isempty(stdV0)
                % if there is nothing to compare with, we have to use this
                isVolumeWithinTolerance = true;
                return
            end
                        
            if newV0 < meanV0+3*stdV0 && newV0 > min(obj.minVol, meanV0-3*stdV0)
                isVolumeWithinTolerance = true;
            end
        end
        
        function [meanV0, stdV0] = getFlowbackStatistics(obj, qDrill_lpm)
            % retrive mean flowback volume and standard error, meanV0
            % [m3] and stdV0 [m3], respectively from the nTd flowbacks
            % where the flow rate is approximately qDrill_lpm
            
            meanV0 = [];
            stdV0 = [];
            
            nTdIdx = obj.getNTdLogicalIndexExcludingCurrent;
            
            % find indicies where the flow rate before starting the
            % flowback were qDrill_lpm +/- qWin_lpm
            qLowIdx = obj.qStore > qDrill_lpm - obj.qWin_lpm;
            qHighIdx = obj.qStore < qDrill_lpm + obj.qWin_lpm;
            
            qIdx = nTdIdx & qLowIdx & qHighIdx;
            
            if sum(qIdx) < 2
                % need at least two valid flowbacks to compute meaningful
                % mean and standard deviation
                return
            end
            
            flowBacks_m3 = obj.V0Store(qIdx);
            meanV0 = mean(flowBacks_m3);
            stdV0 = std(flowBacks_m3);
        end
        
        function [obj, fap] = storeAndUpdateFlowlineParameters(obj, fap, newV0, qDrill_lpm)
            
            obj.qStore(obj.idxStore) = qDrill_lpm;
            obj.V0Store(obj.idxStore) = newV0;
            [kVol, kFlow, meanV0] = obj.calcParams(qDrill_lpm);
            
            % update the flowlineAndPitObj
            if ~isempty(meanV0)
                fap = fap.updateVolDrainback(meanV0);
            end
            
            if ~isempty(kVol) && ~isempty(kFlow)
                fap = fap.updateKFlowline(kVol);
                fap = fap.updateK2Flowline(kFlow);
            end
            
            % save constant
            obj.kStore(obj.idxStore) = fap.kFlowline;
        end
        
        function obj = updateInitState(obj)
            
            if sum(obj.rbFlowIn.Tbuffer_d ~= obj.errVal) > obj.nMinFlowValuesForInit ...
                    || sum(obj.rbVol.Tbuffer_d ~= obj.errVal) > obj.nMinVolValuesForInit
                obj.isInit = false;
            else
                obj.isInit = true;
            end
        end
        
        function obj=resetInitState(obj)
            obj.isInit=true;
        end
        
        function obj=storeLeastSquaresData(obj, db, volTime, idxVol)
            % store data which is necessary for performing the least
            % squares calculation for finding the parameters related to the
            % flowline flow-rate.
            
           idxTd=find(db>obj.dbLim_m3,1,'first');
            % check if maybe it is better to use the previous index as
            % starting point, may be to conservative
            if idxTd>1
                v1=db(idxTd-1);
                v2=db(idxTd);
                if obj.dbLim_m3/(v2-v1)<0.5
                    idxTd=idxTd-1;
                end
            end
            % calculate y-term used for least squares
            t=volTime(idxTd:end)-volTime(idxTd);
            normDb=db(idxTd:end)/max(db);
            normDbIdx=normDb>0 & normDb<1 & t<300; % ensure that the normalized drainback is between 0 and 1, and use only the first five minutes of data.
            y=min(20,-log(1-normDb(normDbIdx)));
            nY=length(y);
            % save y and t for later use
            obj.yStore(idxVol,1:nY)=y;
            obj.tStore(idxVol,1:nY)=t(normDbIdx);  
        end
        
        function [db,volTime]=getDrainbackData(obj, idxVol)
            % retrieve drainback data for idxVol
            db=[];
            volTime=[];
            
            if not(idxVol>obj.nFbs || idxVol<1) && obj.volOffIdxStore(idxVol)>0 % avoid index out of bounds
                db=obj.volStore(idxVol,obj.volOffIdxStore(idxVol):end)-obj.volStore(idxVol,obj.volOffIdxStore(idxVol));
                volTime=(obj.timeVolStore(idxVol,obj.volOffIdxStore(idxVol):end)-obj.timeVolStore(idxVol,end))*86400;
            end
        end
        
        function [expDb,expVolTime]=getExpDrainbackData(obj, idxVol)
            % retrieve expected drainback data for idxVol
            expDb=[];
            expVolTime=[];
            
            if not(idxVol>obj.nFbs || idxVol<1) && obj.expVolOffIdxStore(idxVol)>0 % avoid index out of bounds
                expDb=obj.expVolStore(idxVol,obj.expVolOffIdxStore(idxVol):end)-obj.expVolStore(idxVol,obj.expVolOffIdxStore(idxVol));
                expVolTime=(obj.timeExpVolStore(idxVol,obj.expVolOffIdxStore(idxVol):end)-obj.timeExpVolStore(idxVol,end))*86400;
            end
        end
        
        function [kVol_hz, kFlow, meanV0_m3] = calcParams(obj, qDrill_lpm) 
            kVol_hz = [];
            kFlow = []; % unitless
            meanV0_m3=[];
            
            idx = getNTdLogicalIndex(obj);
            idx = idx & obj.kStore > 0;
            idx(obj.idxStore) = true; % use current dataset as a minimum.

            % stack data
            nD = sum(idx); % number of datasets
            yArray = reshape(obj.yStore(idx, :), [1,nD*obj.nRbVol]);
            yIdxGtZero = yArray > 0;
            tArray = reshape(obj.tStore(idx, :), [1,nD*obj.nRbVol]);
            tLtLim = tArray < obj.leastSquareTimeLimit_s; % consider only time less than specified by limit
            if any(yIdxGtZero) && any(tLtLim)
                % carry out least square fit to find the inverse of the
                % time-constant for the flowback
%                 kVol_hz = yArray(yIdxGtZero & tLtLim)/tArray(yIdxGtZero & tLtLim);
                kVol_hz = 1/60;
                % scatter(tArray(yIdxGtZero), yArray(yIdxGtZero))
                
                % carry out least squares fit to find the average flowback
                % volume (V0)
                idxValidFlowbacks = obj.V0Store > 0;
                nValidFlowbacks = sum(idxValidFlowbacks);
                diffValidFlowbacks = diff(obj.V0Store(idxValidFlowbacks));
                if any(diffValidFlowbacks ~= 0)
                    %myV0Store = obj.V0Store(idxValidFlowbacks)
                    %myqStore = obj.qStore(idxValidFlowbacks)
                    actually_used_meanV0Coeffs = obj.V0Store(idxValidFlowbacks)'/[obj.qStore(idxValidFlowbacks) ones(nValidFlowbacks, 1)]';
                    meanV0Coeffs = actually_used_meanV0Coeffs; %[0.1, 0.1];
                    kDb_m3PerLpm = meanV0Coeffs(1);
                    kV_m3 = meanV0Coeffs(2);
                    meanV0_m3 = kDb_m3PerLpm*qDrill_lpm + kV_m3;
                else
                    meanV0_m3 = obj.V0Store(obj.idxStore);
                end
                
                kFlow = 1 - kVol_hz*meanV0_m3/(qDrill_lpm/6e4);
            else
                % otherwise, return with empty kVol, kFlow, and V0.
                return
            end
        end
        
        function idx=getNTdLogicalIndex(obj)
            % make an array of logical indicies which can be used to look
            % up the nTd last recorded values, including the current one.
            
            idx=false(obj.nFbs,1); % logical indicies
            
            if obj.idxStore>=obj.params.nTd
                idx(obj.idxStore-obj.params.nTd+1:obj.idxStore)=true(obj.params.nTd,1);
            else
                idx(1:obj.idxStore)=true(obj.idxStore,1);
                idx(obj.nFbs-(obj.params.nTd-obj.idxStore)+1:end)=true(obj.params.nTd-obj.idxStore,1);
            end
        end
        
        function idx=getNTdLogicalIndexExcludingCurrent(obj)
            % make an array of logical indicies which can be used to look
            % up the nTd last recorded values, excluding the current one.
            
            idx=false(obj.nFbs,1); % logical indicies
            
            if obj.idxStore>obj.params.nTd
                idx(obj.idxStore-obj.params.nTd:obj.idxStore-1)=true(obj.params.nTd,1);
            elseif obj.idxStore == 1
                idx(obj.nFbs-obj.params.nTd+1:end)=true(obj.params.nTd,1);
            else
                idx(1:obj.idxStore-1)=true(obj.idxStore-1,1);
                idx(obj.nFbs-(obj.params.nTd-obj.idxStore):end)=true(obj.params.nTd-obj.idxStore+1,1);
            end
        end
        
        function [idxVol, kIdx]=getLastValidVolIdxAndKIdx(obj)
            % traverse backwards through flowbacks and find the first valid
            % one. Return last valid k-value and index to last volume,
            % idxVol.
            idxVol=[];
            kIdx=[];
            if obj.idxStore==obj.nFbs
                r=obj.nFbs:-1:1;
            else
                r=[obj.idxStore:-1:1 obj.nFbs:-1:obj.idxStore+1];
            end
            
            ii=1;
            while all(obj.volStore(r(ii),:)==0)
                ii=ii+1;
                if ii>obj.nFbs
                    % No valid flowback to use for computation
                    return
                end
            end
            
            idxVol=r(ii);
            
            jj=1;
            kVal=[];
            while obj.kStore(r(jj))==0 && isempty(kVal)
                jj=jj+1;
                if jj>obj.nFbs
                    % The k is not set
                    kVal=0;
                    jj=jj-1; % to avoid index out of bounds on next iteration
                end
            end
            if isempty(kVal)
                kIdx=r(jj);
            end
        end
        
        function [timeOfPumpRamp, qDrill_lpm, rIdx]=analyzeRamp(obj, flowInTime_d, flowIn_lpm, rampType)
            % analyze the rig pump flow rate time series which is stored
            % when starting to ramp up the rig pumps. Identify if it is
            % usable to calculate parameters for the flowback. If so, then
            % return with the index to when ramping down starts. If not,
            % then return empty rIdx. If the the sequence is valid, then
            % find the flow-rate used when drilling (qDrill [lpm]) before
            % ramping down the pumps.
            
            % filter with running avg of 5 samples on the flipped/reversed
            % flow-rate
            if strcmp(rampType, 'down')
                pumpRampDownFilt=filter(ones(1,obj.nRunningAvgWin)/obj.nRunningAvgWin, 1, flip(flowIn_lpm(1:end - obj.nSamplesToDiscardAtEndOfFlowIn)));
            else % it is ramp up and the flow-rate is not flipped.
                pumpRampDownFilt=filter(ones(1,obj.nRunningAvgWin)/obj.nRunningAvgWin, 1, flowIn_lpm);
            end
            n=length(pumpRampDownFilt);
            dt_s=abs(diff(flip(flowInTime_d)))*86400;
            meanDt_s=mean(dt_s);
            nSsSamples = ceil(obj.ssDuration_s/meanDt_s); % number of samples which defines steady state
            
            [counterPlateau, indexToMostRecentPlateau] = findMostRecentPlateau;
            
            [rIdx, qDrill_lpm]=isFlowStable(counterPlateau, indexToMostRecentPlateau);
            timeOfPumpRamp=flowInTime_d(rIdx);
            
            function [cPlateau, idxPlateauStart] = findMostRecentPlateau()
                % traverse look for a time-period where the
                % flow-rate has stabilized
                                
                nPlateaus=0; % [-] number of plateaus encountered
                cPlateau=0; % [samples] counter for which starts when encountering a plateau
                nDefPlateau=ceil(obj.plateauTimeDef_s/meanDt_s); % number of consecutive samples which defines a plateau
                idxPlateauStart=1; % index to when the plateau starts

                searchIdx=max(2,find(pumpRampDownFilt>obj.startSearch_lpm,1)); % index where search for plateau starts
                if isempty(searchIdx)
                    % if no pump shut-down in sequence, then return empty.
                    return;
                end
                
                % loop through the reversed and filtered ramp-down sequence.
                % End loop if: at end of sequence; if maximum number of
                % plateaus have been encountered; or if a plateau has lasted
                % long enough to consider the flow rate as steady
                while searchIdx <= n && nPlateaus <= obj.maxNPlateaus && cPlateau <= nSsSamples
                    % compute the change in flow rate per second
                    dfdt=diff(pumpRampDownFilt(searchIdx-1:searchIdx))/dt_s(searchIdx-1);
                    if abs(dfdt)<obj.flowChangeTol_lpmPerS
                        % start counting if the rate of change is small enough
                        cPlateau=cPlateau+1;
                    elseif dfdt<-obj.flowChangeTol_lpmPerS
                        % if the flow is ramped down again, then return empty
                        return;
                    else
                        % reset if the flow rate is not considered stable
                        cPlateau=0;
                    end
                    
                    % if enough samples are counted, then count this as a
                    % plateau
                    if cPlateau==nDefPlateau
                        nPlateaus=nPlateaus+1;
                        % store index to where the plateau started
                        idxPlateauStart=searchIdx-nDefPlateau;
                    end
                    searchIdx=searchIdx+1;
                end
            end
            
            function [rIdx, qDrill_lpm]=isFlowStable(cPlateau, idxPlateauStart)
                % only return with values if the flow-rate is considered to be
                % stable
                
                rIdx = [];
                qDrill_lpm = [];
                
                if cPlateau > nSsSamples
                    % reverse index back to actual index for the start of the pump
                    % ramp down.
                    if strcmp(rampType, 'down') 
                        rIdx=n-idxPlateauStart+1-obj.nSamplesToDiscardAtEndOfFlowIn;
                    else
                        rIdx=n-idxPlateauStart+1;
                    end
                    % the flow rate is the mean in the period considered as
                    % steady state.
                    qDrill_lpm=mean(flowIn_lpm(max(1,rIdx-nSsSamples):rIdx));
                end
            end
        end
    end
    
    methods (Static)
        
        function signalWoOffset = removeOffsetFromTimeseries(signal, threshold)
            
            offset = -signal(1);
            signalWoOffset = zeros(size(signal));
            for ii=2:numel(signal)
                [signalWoOffset(ii), offset]=removeOffset(signal(ii-1:ii), offset, threshold);
            end
            
        end
        
        function expVolCurrentConClippedAndScaled = scaleExpVolCurve(volCurrentConClipped, expVolCurrentConClipped)
            % use least squares to get the a factor and bias which scale
            % the expected vol curve to fit the measured.
            
            coeffs = volCurrentConClipped/[expVolCurrentConClipped;ones(1, numel(volCurrentConClipped))];
            expVolCurrentConClippedAndScaled = coeffs(1)*expVolCurrentConClipped + coeffs(2);
        end
        
    end
    
end

