%
%
% This DtectLoss algorithm class uses the derivative of the standpipe pressure
% "pdiff" in addition to the flow in and flow out in its gain/loss equation:
% 
% qio = (Va+Vd)/beta * d(p_sp)/dt + q_out-q_in 
%
% note that the term T_well = (Va+Vd)/beta is identified from data
% in this case.
% 
% d(p_sp(t))/dt  is found from p_sp(t) - lowpass(p_sp(t)) 
%
% The class uses a version of the bulk modulus estimation rutine 
% developed by Statoil for the IMPD project to find the V/beta parameter.
%
% The algorithm does not include:
% * the hysteresis/nonlinear viscocity of that has to be overwon during
% startup after a connection, will give a small false "kick" as flowIn is above zero while flowOut is not  
% 
% TODO: try to make another version of pdiff that
% - has a nonlinear function for (c0 + c1*q + c2*q^2) for calibrating "actual flow out" during a connection
% (try offline regression first), as there is often a steady state bias during
% connection of 100 lpm or more even when  accounting for the bias at full flow. 
% - try to make a version that of pdiff that re-calibrates beta and V during each
% connection, and otherwise is off, as there is no information during
% "steady flow", consider turning off pdiff during normal drilling,as there
% is no information on pdot then. (maybe just have a threshold on pdot for running inverted sums?)
% -there is also a transient in flowOut when stopping the pumps->add a time
% delay or time-constatnt to account for this?

classdef PDIFF_ALG_OBJ < EKD_ALGORITHM_OBJ
    properties

    end
    
    properties (SetAccess = private)
        betaperVol_e_barm3;
        flowBias_lpm;
        isOnBounds;
        
        flowIo_lpm ;
        flowIoFromPres_lpm;
        flowIoFromRatesWBias_lpm;
        volIo_m3;
        volIoAcc_m3;
        
        dpsp_dt_bar;
        params;             % tunable paramters struct releated to the algorithm
        samplesSinceStartedDrilling;
        doPcalc;
        volumeThreshold_m3;
        alarmState;
        dt_s;
        alarmStatus;
        flowOutExpected_lpm;
        
        timeCurrent_d;
        timeLast_d;
    end
    properties (GetAccess= private,SetAccess = private)
        lpPresSPObj  ;                % lowpass filter object
    end
    
    properties (Constant)
         lpmToM3ps           = 1/60000;
         M3PsTolpm           = 60000;
         TfiltPresSP_s       = 20;       % standpipe pressure filter time constant in seconds
         beta_divBY_V_min    = 5000/200;% maximum value for beta/V
         beta_divBY_V_max    = 50000/10;% minimum value for beta/V
         timeStep_max_s      = 60; % if no new data within this dt_s, the reset object
         flowBiasMax_lpm     = 500; % expect that you should have at least 10 lpm here
    end
    methods
        function obj        = PDIFF_ALG_OBJ()
            obj.params      = [];
            obj.flowIo_lpm  = [];
            obj.volIo_m3    = [];
            obj.dpsp_dt_bar = [];
            obj.samplesSinceStartedDrilling =[];
        end
        function obj        = initialize(obj,params,varargin)%doPcalc
            obj.params = params;
            if nargin ==2
                doPcalc_internal     =1;
            else
                doPcalc_internal     = varargin{1};
            end
            obj.flowIo_lpm  = 0;
            obj.volIo_m3    = 0;  
            obj.volIoAcc_m3 = 0;
            obj.dpsp_dt_bar = 0;
            obj.samplesSinceStartedDrilling =0;
            obj.doPcalc     = doPcalc_internal;
            if( obj.doPcalc == 1)
                obj.betaperVol_e_barm3 = 1;
            else
                obj.betaperVol_e_barm3 = inf; 
            end
            obj.lpPresSPObj     = LOWPASS_OBJ(nan,obj.TfiltPresSP_s);
            obj.alarmState      = 0;
             obj.timeCurrent_d   = 0;
             obj.timeLast_d      = 0;
            obj.dt_s            = 0;
            obj.flowOutExpected_lpm = 0;
        end
        %determine if an alarm has occured
        function obj=setAlarmState(obj,sysState)
             if(sysState.currentState == DrillingStates.Drilling )
                if abs(obj.volIo_m3) > obj.params.volumeThresholdAlarm_m3,
                    obj.alarmState = AlarmState.Alarm;
                elseif abs(obj.volIo_m3) > obj.params.volumeThresholdWarning_m3,
                    obj.alarmState = AlarmState.Warning;
                else
                    obj.alarmState = AlarmState.Normal;
                end
             else
                 obj.alarmState = AlarmState.Disabled;
             end
             if( obj.isOnBounds == 1 ) 
                obj.alarmState = AlarmState.Disabled;
             end
        end
        % this function is called for every vector of new data
        function [obj, result] = stepIt(obj, processData, expData, sysState, timeCurrent_d,dt_s)
     
            obj.timeCurrent_d   = timeCurrent_d;
            obj.dt_s            = dt_s;%(obj.timeCurrent_d - obj.timeLast_d)*86400;
            obj.timeLast_d      = obj.timeCurrent_d;
            flowIn_lpm          = processData.flowIn_lpm;
            flowOut_lpm         = processData.flowOut_lpm;
            presSP_bar          = processData.presSP_bar;
            if (obj.dt_s <= 0),%do nothing
                obj.alarmStatus = ['Do nothing, time step = ' num2str(obj.dt_s,'%.2f') ' sec'];
            elseif obj.dt_s > obj.timeStep_max_s,%reset
                obj.alarmStatus = ['Reset, time step = ' num2str(obj.dt_s,'%.2f') ' sec'];
                obj             = initialize(obj,obj.params);
                obj.alarmState  = AlarmState.Disabled;
            elseif flowOut_lpm== -0.99925 || flowIn_lpm == -0.99925 || presSP_bar == -0.99925 
                obj.alarmState  = AlarmState.InputError;
            else %ok - step alg            
                if(sysState.currentState == DrillingStates.Drilling )
                    obj.samplesSinceStartedDrilling = obj.samplesSinceStartedDrilling + 1;
                    % -------------- pdot -------------
                    obj.lpPresSPObj     = obj.lpPresSPObj .filterForCustomDt(presSP_bar,dt_s,0);
                    presSP_lowpass_bar  = obj.lpPresSPObj .FilteredSignal;       
                    if (obj.samplesSinceStartedDrilling*dt_s > obj.TfiltPresSP_s*5)
                        obj.dpsp_dt_bar  = (presSP_bar - presSP_lowpass_bar)/obj.TfiltPresSP_s ;
                    else
                        obj.dpsp_dt_bar  = 0;
                    end
                    if( obj.doPcalc == 1)
                        if obj.samplesSinceStartedDrilling<3
                            DO_RESET    = 1;
                        else
                            DO_RESET    = 0;
                        end
                         % ------------ "inverted sums" from impd bulk modulus estimation.
                        [obj.betaperVol_e_barm3, flowBias_m3ps, obj.isOnBounds]= invertedSums(presSP_bar,(flowOut_lpm-flowIn_lpm)*obj.lpmToM3ps,...
                             obj.beta_divBY_V_max,obj.beta_divBY_V_min,obj.flowBiasMax_lpm*obj.lpmToM3ps,dt_s,obj.TfiltPresSP_s,DO_RESET);
                         obj.flowBias_lpm = flowBias_m3ps*obj.M3PsTolpm;
                    end
                    % ---------------------------------------
                    obj.flowIoFromPres_lpm        = 1/obj.betaperVol_e_barm3 *obj.M3PsTolpm* obj.dpsp_dt_bar;
                    obj.flowIoFromRatesWBias_lpm  = flowOut_lpm - flowIn_lpm +obj.flowBias_lpm;% by definition flowOut-flowIn
                    obj.flowIo_lpm                = obj.flowIoFromPres_lpm  + obj.flowIoFromRatesWBias_lpm;
                    if abs(obj.flowIo_lpm)>obj.params.flowDeadband_lpm
                        obj.volIo_m3      = obj.volIo_m3 + obj.dt_s * obj.flowIo_lpm*obj.lpmToM3ps;% vIo[m3] = dt[s] *qIo[lpm]
                    else
                        obj.volIo_m3      = 0;%volIo is for sharp losses
                    end
                    obj.volIoAcc_m3         = obj.volIoAcc_m3 + obj.dt_s * obj.flowIo_lpm*obj.lpmToM3ps;% longer term trends, is not reset based on flowDeadband
                    obj                     = obj.setAlarmState(sysState);
                  %  disp(sprintf('dpsp_dt: %f  1/obj.betaDivByV_e_bar_per_m3 * dpsp_dt: %f',obj.dpsp_dt_bar,1/obj.betaDivByV_e_bar_per_m3 * obj.dpsp_dt_bar));

                    obj.flowOutExpected_lpm = flowIn_lpm - 1/obj.betaperVol_e_barm3 *obj.M3PsTolpm* obj.dpsp_dt_bar;
                    obj.flowIo_lpm          = flowOut_lpm - obj.flowOutExpected_lpm;
                    if abs(obj.flowIo_lpm)>obj.params.flowDeadband_lpm
                        obj.volIo_m3       = obj.volIo_m3 + obj.dt_s * obj.flowIo_lpm*obj.lpmToM3ps;% vIo[m3] = dt[s] *qIo[lpm]
                    else
                        obj.volIo_m3       = 0;
                    end
                    obj         = obj.setAlarmState(sysState);
                end
            end
            obj                 = obj.setAlarmState(sysState);
            [obj, result]       = obj.returnResultStruct();
        end
        
        function  [obj, result ]    = returnResultStruct(obj)
            result.flowIO_lpm       = obj.flowIo_lpm ;
            result.flowIoFromPres_lpm       = obj.flowIoFromPres_lpm;
            result.flowIoFromRatesWBias_lpm = obj.flowIoFromRatesWBias_lpm;    
            result.volIO_m3         = obj.volIo_m3;
            result.alarmState       = obj.alarmState;
            result.betaperVol_e_barm3= obj.betaperVol_e_barm3;
            result.flowBias_lpm     = obj.flowBias_lpm;
            result.isOnBounds       = obj.isOnBounds;
            result.flowOutExpected_lpm      = obj.flowOutExpected_lpm ;
        end
    end
end

