classdef RINGBUFFER_OBJ < handle
    
    properties(SetAccess = private)
        bufferSizeSamples;
        Vbuffer;
        Tbuffer_d;
        posBufferLastWriteIdx;
    end
    
    properties (SetAccess = private,GetAccess = private)
    end
    
    properties (Constant)
        errVal = -0.99925;
    end
    
    methods
        function obj = RINGBUFFER_OBJ(bufferSizeSamples)
            obj.bufferSizeSamples           = bufferSizeSamples;
            obj.Vbuffer                     = zeros(1,obj.bufferSizeSamples);
            obj.Tbuffer_d                   = ones(1,obj.bufferSizeSamples)*obj.errVal;
            obj.posBufferLastWriteIdx       = 1;
        end
        
        function obj = addDataToCircularBuffer(obj, timeNew_d, value)
            if (timeNew_d <= obj.Tbuffer_d(obj.posBufferLastWriteIdx) ) || (value == obj.errVal) || (timeNew_d == obj.errVal)
                % do not add meaningless values to buffer.
                return;
            end
            if obj.posBufferLastWriteIdx < obj.bufferSizeSamples
                obj.posBufferLastWriteIdx = obj.posBufferLastWriteIdx+1;
            else
                obj.posBufferLastWriteIdx = 1;
            end
            obj.Vbuffer(obj.posBufferLastWriteIdx) = value;
            obj.Tbuffer_d(obj.posBufferLastWriteIdx) = timeNew_d;
        end
        
        function avgBufferSampleTime_s = GetAvgBufferSampleTime(obj)
            if obj.posBufferLastWriteIdx== obj.bufferSizeSamples
                oldestIdx = 1;
            else
                oldestIdx = obj.posBufferLastWriteIdx+1;
            end
            if obj.Tbuffer_d(oldestIdx) == obj.errVal
                avgBufferSampleTime_s = obj.errVal;
            else
                avgBufferSampleTime_s =(obj.Tbuffer_d(obj.posBufferLastWriteIdx) -  obj.Tbuffer_d(oldestIdx))*86400/obj.bufferSizeSamples;
            end
        end
        
        function Vlast = getLastValue(obj)
            Vlast = obj.Vbuffer(obj.posBufferLastWriteIdx);
        end
        
        function VlastArr = getLastNValues(obj, N)
           if obj.posBufferLastWriteIdx - N > 0
                VlastArr = obj.Vbuffer(obj.posBufferLastWriteIdx-N:obj.posBufferLastWriteIdx);
           else
                VlastArr = obj.Vbuffer(1:obj.posBufferLastWriteIdx);
                VlastArr = [obj.Vbuffer(obj.bufferSizeSamples - N + obj.posBufferLastWriteIdx:obj.bufferSizeSamples) VlastArr];
           end
        end
        
        function TlastArr_d = getLastNTimes(obj, N)
            if obj.posBufferLastWriteIdx - N > 0
                TlastArr_d = obj.Tbuffer_d(obj.posBufferLastWriteIdx-N:obj.posBufferLastWriteIdx);
            else
                TlastArr_d = obj.Tbuffer_d(1:obj.posBufferLastWriteIdx);
                TlastArr_d = [obj.Tbuffer_d(obj.bufferSizeSamples - N + obj.posBufferLastWriteIdx:obj.bufferSizeSamples) TlastArr_d];
           end
        end
        
        function Voldest = getOldestValue(obj)
            if obj.posBufferLastWriteIdx== obj.bufferSizeSamples
                oldestIdx = 1;
            else
                oldestIdx = obj.posBufferLastWriteIdx+1;
            end
            Voldest = obj.Vbuffer(oldestIdx);
        end
        
        function Tlast = getLastTime(obj)
            Tlast = obj.Tbuffer_d(obj.posBufferLastWriteIdx);
        end
        
        function [timeWindow_d, values] = getWindowOfLastValues(obj, time_d)
            timeWindow_d = [];
            values = [];
            
            if time_d > obj.getLastTime
                return
            end
            
            [Tbuffer_d_straight, Vbuffer_straight, ~, validDataPoints] = obj.deringbuffer;
            
            timeWindow_d = Tbuffer_d_straight(validDataPoints);
            values = Vbuffer_straight(validDataPoints);
            
            if time_d > Tbuffer_d_straight(1)
                values = values(timeWindow_d > time_d);
                timeWindow_d = timeWindow_d(timeWindow_d > time_d);
            end
        end
        
        function valueRange = getValueRange(obj, startTime_d)
            
            valueRange = [];
            
            [~, values] = getWindowOfLastValues(obj, startTime_d);
            
            if ~isempty(values)
                valueRange = max(values) - min(values);
            end
        end
        
        function [Tbuffer_d_straight, Vbuffer_straight, dt_avg_d, validDataPoints] = deringbuffer(obj)
            Tbuffer_d_straight = [obj.Tbuffer_d(obj.posBufferLastWriteIdx+1:obj.bufferSizeSamples)...
                obj.Tbuffer_d(1:obj.posBufferLastWriteIdx)];
            
            Vbuffer_straight = [obj.Vbuffer(obj.posBufferLastWriteIdx+1:obj.bufferSizeSamples)...
                obj.Vbuffer(1:obj.posBufferLastWriteIdx)];
            
            validDataPoints = Tbuffer_d_straight ~= obj.errVal & Vbuffer_straight ~= obj.errVal;
            
            dt_avg_d = mean(diff(Tbuffer_d_straight(validDataPoints)));
        end
        
        function [Tbuffer_d_straight_resampled, Vbuffer_straight_resampled, dt_avg_d] = deringbufferAndResample(obj, doPadMissingValues)
            
            if nargin < 2
                doPadMissingValues = false;
            end
            
            [Tbuffer_d_straight, Vbuffer_straight, dt_avg_d, validDataPoints] = obj.deringbuffer();
            
           	if any(validDataPoints) && sum(validDataPoints) > 1
                tValid_d = Tbuffer_d_straight(validDataPoints);
                vValid = Vbuffer_straight(validDataPoints);
                
                nInvalidDataPoints = sum(~validDataPoints);
                
                if ~doPadMissingValues
                    missingVals = ones(1, nInvalidDataPoints)*obj.errVal;
                    Tbuffer_d_straight_resampled = [missingVals tValid_d(1):dt_avg_d:tValid_d(end)];
                    Vbuffer_straight_resampled = [missingVals interp1(tValid_d, vValid, Tbuffer_d_straight_resampled(validDataPoints))];
                else
                    Tbuffer_d_straight_resampled = linspace(tValid_d(1) - nInvalidDataPoints*dt_avg_d, tValid_d(end), numel(Tbuffer_d_straight));
                    Vbuffer_straight_resampled = [vValid(1)*ones(1, nInvalidDataPoints) interp1(tValid_d, vValid, Tbuffer_d_straight_resampled(validDataPoints))];
                end
            else
                Tbuffer_d_straight_resampled = [];
                Vbuffer_straight_resampled = [];
                dt_avg_d = [];
            end
		end
        
        function [timePadded, valuePadded] = padMissingValues(obj, tValid_d, vValid, dt_avg_d, nInvalidDataPoints, useErrValForPadding)
        
            if useErrValForPadding
                timePadded = ones(1, nInvalidDataPoints)*obj.errVal;
                valuePadded = timePadded;
            else
                timePadded = linspace(tValid_d(1) - nInvalidDataPoints*dt_avg_d, tValid_d(1) - dt_avg_d, nInvalidDataPoints);
                valuePadded = vValid(1)*ones(1, nInvalidDataPoints);
            end
        end
        
        function [Tbuffer_d_straight_resampled, Vbuffer_straight_resampled] = resampleData(obj, Tbuffer_d_straight, Vbuffer_straight, dt_avg_d, validDataPoints, doPadMissingValues)
            
            tValid_d = Tbuffer_d_straight(validDataPoints);
            vValid = Vbuffer_straight(validDataPoints);
            
            nInvalidDataPoints = sum(~validDataPoints);
            
            if ~doPadMissingValues
                missingVals = ones(1, nInvalidDataPoints)*obj.errVal;
                Tbuffer_d_straight_resampled = [missingVals tValid_d(1):dt_avg_d:tValid_d(end)];
                Vbuffer_straight_resampled = [zeros(1, nInvalidDataPoints) interp1(tValid_d, vValid, Tbuffer_d_straight_resampled(validDataPoints))];
            else
                Tbuffer_d_straight_resampled = linspace(tValid_d(1) - nInvalidDataPoints*dt_avg_d, tValid_d(end), numel(Tbuffer_d_straight));
                Vbuffer_straight_resampled = [vValid(1)*ones(1, nInvalidDataPoints) interp1(tValid_d, vValid, Tbuffer_d_straight_resampled(validDataPoints))];
            end
        end
        
        function [varCalc, Vbuffer_first_half] = varFirstHalf(obj)
            % compute covariance of the first half of the values stored in
            % the ringbuffer
            N1=floor(obj.bufferSizeSamples/2); % number of samples in the first half
            
            if obj.posBufferLastWriteIdx <= obj.bufferSizeSamples - N1
                Vbuffer_first_half=obj.Vbuffer(obj.posBufferLastWriteIdx+1:obj.posBufferLastWriteIdx+N1);
            else
                Vbuffer_first_half=[obj.Vbuffer(obj.posBufferLastWriteIdx+1:end)...
                    obj.Vbuffer(1:abs(obj.posBufferLastWriteIdx-N1-1))];
            end
            
            fbgz=Vbuffer_first_half>0; % values greater than zero
            if any(fbgz)
                varCalc=var(Vbuffer_first_half(fbgz));
            else
                varCalc=[];
            end
        end
        
        function [Vdelayed_lpm,dt_delayed_s] = getPastValue(obj,timeDelay_s,timeNew_d)
            persistent timeOld_d;
            if isempty(timeOld_d)
                timeOld_d = timeNew_d;
            end
            [Vdelayed_lpm,dt_delayed_s] = getPastValueWithExplicitLastCallTime(obj,timeDelay_s,timeNew_d,timeOld_d);
            timeOld_d = timeNew_d;

        end
      
        function [Vdelayed_lpm,dt_delayed_s] = getPastValueWithExplicitLastCallTime(obj,timeDelay_s,timeNew_d,timeOld_d)
            dt_delayed_s            = 1;
            idx                     = obj.posBufferLastWriteIdx;
            idxPast                 = obj.posBufferLastWriteIdx;
            deltaT_mostRecent_s    = timeNew_d*86400 - obj.Tbuffer_d(idx)*86400 ;
            a                       = 0;
            if timeDelay_s<deltaT_mostRecent_s
                isFound            = true;
            else
                isFound             = false;
            end
            whileCounter            = 0;
            deltaT_current_s        = 0;
            deltaT_past_s           = 0;
            while isFound==false
                whileCounter        = whileCounter+1;
                deltaT_current_s    = timeNew_d*86400 - obj.Tbuffer_d(idx)*86400 ;
                deltaT_past_s       = timeNew_d*86400 - obj.Tbuffer_d(idxPast)*86400 ;
                
                if deltaT_current_s >= timeDelay_s && deltaT_past_s <= timeDelay_s
                    isFound         = true;
                else
                    idxPast         = idx;
                    if idx>1
                        idx         = idx-1;
                    else
                        idx         = obj.bufferSizeSamples;
                    end
                end
                if whileCounter >= obj.bufferSizeSamples || obj.Tbuffer_d(idx) == obj.errVal || obj.Tbuffer_d(idxPast) == obj.errVal 
                    Vdelayed_lpm    = obj.errVal;
                    return;
                end   
            end
            if timeDelay_s>0
                 a                   = (timeDelay_s-deltaT_past_s)/(deltaT_current_s-deltaT_past_s);
                 a                   = max(0,a);
                 a                   = min(1,a);
                 Vdelayed_lpm        = obj.Vbuffer(idx)*a + obj.Vbuffer(idxPast)*(1-a);
             else 
                 Vdelayed_lpm        = obj.Vbuffer(idx);
            end
            dt_delayed_s            = timeNew_d*86400-timeOld_d*86400;
%             disp(sprintf('V:%f,a:%f,dt_delayed_s:%f',Vdelayed_lpm,a,dt_delayed_s));
        end
        
        
        function lookUpVal = lookUp(obj, time_d)
            % given a time in serial time (time_d), look up value in ring
            % buffer which corresponds to time and return this (lookUpVal).
            % If the time does not correspond exactly to a sampled value,
            % then interpolate between available values. If no value is
            % available, then return the error value obj.errVal.
            
            lookUpVal=obj.errVal;
            
            if all(obj.Tbuffer_d==obj.errVal)
                return
            end
            
            if time_d>=obj.getLastTime()
                lookUpVal=obj.getLastValue();
            else
                % traverse backwards through times to find closest matching time
                if obj.posBufferLastWriteIdx==obj.bufferSizeSamples
                    r=obj.bufferSizeSamples:-1:1;
                else
                    r=[obj.posBufferLastWriteIdx:-1:1 obj.bufferSizeSamples:-1:obj.posBufferLastWriteIdx+1];
                end
                
                ii=1;
                while obj.Tbuffer_d(r(ii))>time_d
                    ii=ii+1;
                    if ii>obj.bufferSizeSamples
                        % The look up time is not in the buffer
                        return
                    end
                end
                
                % in case the time in the buffer equals look-up time
                if obj.Tbuffer_d(r(ii))==time_d
                    if obj.Vbuffer(r(ii))~=obj.errVal
                        lookUpVal=obj.Vbuffer(r(ii));
                    end
                else % need to interpolate between values
                    % check that the values in the buffer are not error values
                    if any(obj.Tbuffer_d(r(ii-1:ii))==obj.errVal)
                        % return error value if one of the values used for
                        % interpolation is not valid
                        return
                    end
                    
                    % interpolate value as V0+dV/dt*timeDiff
                    dt=diff(obj.Tbuffer_d(r(ii-1:ii)));
                    dV=diff(obj.Vbuffer(r(ii-1:ii)));
                    lookUpVal=obj.Vbuffer(r(ii-1)) + (time_d-obj.Tbuffer_d(r(ii-1)))*dV/dt;
                end
            end
        end
    end
end
