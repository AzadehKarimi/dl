%The purpose of this class is to compensate for delays in the flowIn
%measurements. We have seen from field data that in some cases the
%standpipe pump pressure starts to drop before we can see a reduction in
%the pump flow. This is not physical, but due to issues in the pump flow
%sensor (mail Ben Hern)

classdef STANDPIPE_PRESSURE_DELAY_OBJ < DTECTLOSS_SUBMODULE

    properties(SetAccess = private)
        timeDelayPresSP_s;
        isCalibratingSPdelay;
    end
    properties (SetAccess = private,GetAccess = private)
        presSPringBufferObj;
        flowInringBufferObj;
        samplesSinceLastFullFlow;
        samplesSinceLastNoFlow;
        samplesSinceLastCalibration;
        timeDelayLPFilterFlowIn_s;
        factor_forget;
        timeDelay_max_s;
    end
    properties (Constant)
        BUFFER_SIZE_SAMPLES             = 20;% 20 should be even number!
        FULL_FLOW_THRESHOLD_LPM         = 1400;
        NO_FLOW_THRESHOLD_LPM           = 100;
        dispDebug                       = false;
    end
        
    methods
        function obj                    = STANDPIPE_PRESSURE_DELAY_OBJ(params)
            obj.presSPringBufferObj         = RINGBUFFER_OBJ(obj.BUFFER_SIZE_SAMPLES);
            obj.flowInringBufferObj         = RINGBUFFER_OBJ(obj.BUFFER_SIZE_SAMPLES);
            obj.samplesSinceLastFullFlow    = 0;
            obj.samplesSinceLastNoFlow      = 0;
            obj.samplesSinceLastCalibration = 0;
            obj.timeDelayPresSP_s           = 0;
            obj.isCalibratingSPdelay        = false;
            obj.factor_forget               = params.pressp.factor_forget;
            obj.timeDelay_max_s             = params.pressp.timeDelay_max_s;
        end
        
        function obj                    = softReset(obj)
            obj.samplesSinceLastFullFlow    = 0;
            obj.samplesSinceLastNoFlow      = 0;
            obj.samplesSinceLastCalibration = 0;
            obj.timeDelayPresSP_s           = 0;
            obj.isCalibratingSPdelay        = false;
        end
                
        function [obj, doCalibration] = shouldWeCalibrateNow(obj, flowIn_lpm)
             if flowIn_lpm > obj.FULL_FLOW_THRESHOLD_LPM
                 obj.samplesSinceLastFullFlow =0;
             else
                 obj.samplesSinceLastFullFlow = obj.samplesSinceLastFullFlow+1; 
             end
             if flowIn_lpm < obj.NO_FLOW_THRESHOLD_LPM
                obj.samplesSinceLastNoFlow = 0;
             else
                obj.samplesSinceLastNoFlow = obj.samplesSinceLastNoFlow+1;
             end

              if obj.samplesSinceLastFullFlow == obj.BUFFER_SIZE_SAMPLES/2 && ...
                 obj.samplesSinceLastNoFlow      < obj.BUFFER_SIZE_SAMPLES && ...
                 obj.samplesSinceLastCalibration > obj.BUFFER_SIZE_SAMPLES 
                doCalibration                   = true;
                obj.samplesSinceLastCalibration = 0;
             else
                 obj.samplesSinceLastCalibration = obj.samplesSinceLastCalibration+1;
                 doCalibration                   = false;
             end
        end
        
        function obj                    = calibrate(obj)
            [timevec_d, presSPvec]      = obj.presSPringBufferObj.deringbuffer();
            [~,         flowVec]        = obj.flowInringBufferObj.deringbuffer();
            isObjFunImproving           = 1;
            td_samplesBest              = 0;
            [obj, objFunBest]           = obj.calcObjFun(td_samplesBest,presSPvec,flowVec);
            td_samplesTemp              = 0;
            while(isObjFunImproving)
                td_samplesTemp          = td_samplesTemp+1;
                [obj,objFun]            = obj.calcObjFun(td_samplesTemp,presSPvec,flowVec);
                if objFun >= objFunBest 
                    isObjFunImproving   = 0;
                else% at least one of the objective improve on 
                    objFunBest          = objFun;
                    td_samplesBest      = td_samplesTemp;
                end
            end
            if td_samplesBest >= numel(timevec_d) 
                timeDelay_last_s            = 0;                
            else
                timeDelay_last_s            = (timevec_d(end)-timevec_d(max(1,end-td_samplesBest)))*86400;
                timeDelay_last_s            = min(obj.timeDelay_max_s,timeDelay_last_s);
                if obj.timeDelayPresSP_s == 0 %first time
                    obj.timeDelayPresSP_s = timeDelay_last_s;
                else
                    obj.timeDelayPresSP_s = (1 - obj.factor_forget)*obj.timeDelayPresSP_s + obj.factor_forget*timeDelay_last_s;
                end    
                if obj.dispDebug
                    fprintf('New time delay for delayed PresSP meas = %.2f sec, filtered value = %.2f sec\n',timeDelay_last_s,obj.timeDelayPresSP_s);                            
                end
            end
        end
            
        function [obj, objFun]          = calcObjFun(obj,td_samples,presSPvec,flowVec)
            flow_max                    = max(flowVec); 
            presSP_max                  = max(presSPvec);
            flow_min                    = min(flowVec); 
            presSP_min                  = min(presSPvec);
            flow_norm                   = flow_max - flow_min; 
            presSP_norm                 = presSP_max - presSP_min;
            N                           = numel(flowVec)-td_samples;
            presVecAdj                  = (presSPvec(1:end-td_samples) - presSP_min)/presSP_norm;
            flowVecAdj                  = (flowVec(1+td_samples:end) - flow_min)/flow_norm;
            objFun                      = 100*sqrt(1/N*sum((presVecAdj-flowVecAdj).^2 ));
%             if false
%                 close all
%                 figure(1)
%                 hold on
%                 plot(flowVecAdj);
%                 plot(presVecAdj) ;
%                 suptitle(sprintf('td:%i obj:%.2f',td_samples,objFun));
%                 legend('flow','pres');
%             end
        end
        
        function [obj, presSPdelayed_bar] = getDelayedPresSP(obj, timeNow_d)
            presSPdelayed_bar          = obj.presSPringBufferObj.getPastValue(obj.timeDelayPresSP_s, timeNow_d);
        end
        
        function obj = updateTimeDelay(obj, presSp_bar, flowIn_lpm, timeNow_d)
            obj.presSPringBufferObj     = obj.presSPringBufferObj.addDataToCircularBuffer(timeNow_d, presSp_bar);
            obj.flowInringBufferObj     = obj.flowInringBufferObj.addDataToCircularBuffer(timeNow_d, flowIn_lpm);
            
            [obj, doCalibration]        = obj.shouldWeCalibrateNow(flowIn_lpm);
            if doCalibration
                obj.isCalibratingSPdelay = 1;
                obj                      = obj.calibrate();
            else
                obj.isCalibratingSPdelay = 0;
            end
        end     
    end
end