classdef LOWPASS_OBJ
    % Simple lowpass filter
    %
    properties
        FilteredSignal;
    end
    
    properties (SetAccess = private)
            PrevFilteredSignal;
            dt_s ;% sampling time constant
            T_s ; % timeconstant of lowpass filter
    end
    methods
        %call before calling filter 
        function [obj]  = LOWPASS_OBJ(dt_s,T_s)
            obj.dt_s    = dt_s;
            obj.T_s     = T_s;
        end
        
        % main functionality is here.
        function [obj]  = filter(obj,Signal,reset)
            if  obj.dt_s >= obj.T_s
                a = 0;
            else
                a = 1/(1 + obj.dt_s / obj.T_s);
            end
            obj = obj.filter_internal(Signal,reset,a);
        end
        
        function [obj]  = filterForCustomDt(obj,Signal,dt_s,reset)
            if  dt_s >= obj.T_s
                a = 0;
            else
                a = 1/(1 + dt_s / obj.T_s);
            end
            obj = obj.filter_internal(Signal,reset,a);
        end
        
        % for a given value of "a" calculate the filtered signal        
        function [obj]  = filter_internal(obj,Signal,reset,a)
            if isempty(obj.PrevFilteredSignal) || (a == 0)%initalize on first call
               obj.FilteredSignal       = Signal; 
            else
               obj.FilteredSignal       = a *obj.PrevFilteredSignal  + (1- a)  * Signal ;
            end
            obj.PrevFilteredSignal      = obj.FilteredSignal;
            if (reset == 1 )
               obj.FilteredSignal       = Signal;
               obj.PrevFilteredSignal	= Signal;
            end
            
        end
        
        
        
    end
    
end

