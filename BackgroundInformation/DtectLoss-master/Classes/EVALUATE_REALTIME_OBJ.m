classdef EVALUATE_REALTIME_OBJ < DTECTLOSS_SUBMODULE
    %EVALUATE_REALTIME_OBJ Count alarms on the fly
    %   Detailed explanation goes here
    
    properties
        alarmTypeRingBuffer
        nTotalAlarms
        isInitialized
        upTime_d % time in days which the algorithm has been active
        downTime_d % time in days with AlarmState < 0
        totalTime_d
        nAlarmsPer12Hours % average computed for the entire time-span
        nAlarmsLast12Hours % number of alarms during the last 12 hours
        prevAlarmState
        alarmTypeCount
        alarmTypeIndexOffset
        alarmMask
    end
    
    properties (Constant)
        maxNumberOfAlarms = 99;
        nRingBuffer = 99;
    end
    
    methods
        function obj = EVALUATE_REALTIME_OBJ()
            obj.alarmTypeRingBuffer = RINGBUFFER_OBJ(obj.nRingBuffer);
            obj.isInitialized = false;
            obj.upTime_d = 0;
            obj.downTime_d = 0;
            obj.totalTime_d = 0;
            obj.nTotalAlarms = 0;
            obj.nAlarmsPer12Hours = 0;
            obj.nAlarmsLast12Hours = 0;
            obj = obj.generateAlarmParams;
        end
        
        function obj = generateAlarmParams(obj)
            [m, s] = enumeration('AlarmType');
            obj.alarmTypeIndexOffset = size(m, 1)/2;
            obj.alarmTypeCount = zeros(size(m));
            obj.alarmMask = false(size(m));
            cellsWithAlarm = strfind(s, 'Alarm');
            for ii = 1:size(m, 1)
                if ~isempty(cellsWithAlarm{ii})
                    obj.alarmMask(ii) = true;
                end
            end
        end
        
        function obj = initialize(obj, alarmState)
            obj.prevAlarmState = alarmState;
            obj.isInitialized = true;
        end
        
        function obj = softReset(obj)
            obj.isInitialized = false;
            obj.upTime_d = 0;
            obj.downTime_d = 0;
            obj.totalTime_d = 0;
            obj.nTotalAlarms = 0;
            obj.nAlarmsPer12Hours = 0;
            obj.nAlarmsLast12Hours = 0;
            obj = obj.generateAlarmParams;
        end
        
        function obj = updateTimeCount(obj, alarmState, timeStep_s)
            if alarmState < 0
                obj.downTime_d = obj.downTime_d + obj.convertSecToDays(timeStep_s);
            else
                obj.upTime_d = obj.upTime_d + obj.convertSecToDays(timeStep_s);
            end
            obj.totalTime_d = obj.totalTime_d + obj.convertSecToDays(timeStep_s);
        end
                
        function obj = addAlarmIfNotPitSlowGainAlarm(obj, currentTime_d, alarmType, alarmState)
            if obj.checkIfAlarmIsValid(alarmState, alarmType)
                obj.nTotalAlarms = obj.nTotalAlarms + 1;
                obj.alarmTypeRingBuffer = obj.alarmTypeRingBuffer.addDataToCircularBuffer(currentTime_d, alarmType);
                obj = obj.updateAlarmCount;
            end
        end
        
        function validAlarm_bool = checkIfAlarmIsValid(obj, alarmState, alarmType)
            validAlarm_bool = alarmState == int32(AlarmState.Alarm)...
                && obj.prevAlarmState ~= int32(AlarmState.Alarm)...
                && alarmType ~= AlarmType.PitSlowGainAlarm;
        end
        
        function obj = calculateNumberOfAlarmsPer12Hours(obj)
           if obj.upTime_d > 0
                obj.nAlarmsPer12Hours = min(obj.maxNumberOfAlarms, obj.nTotalAlarms/obj.upTime_d/2);
            end 
        end
        
        function obj = updateAlarmCount(obj)
                        
            alarmTypeIndex = obj.alarmTypeRingBuffer.getLastValue + obj.alarmTypeIndexOffset;
            obj.alarmTypeCount(alarmTypeIndex) = obj.alarmTypeCount(alarmTypeIndex) + 1;
        end
        
        function obj = countNAlarmsLast12Hours(obj, currentTime_d)
            
            time_d = obj.alarmTypeRingBuffer.Tbuffer_d;
            mask_timeGTZero = time_d > 0;
            mask_last12Hours = ((time_d - currentTime_d + 0.5) > 0) & mask_timeGTZero;
            
            obj.nAlarmsLast12Hours = sum(mask_last12Hours);    
        end
        
        function nAlarmTypes = sumUpAlarms(obj)
            nAlarmTypes = sum(obj.alarmTypeCount(obj.alarmMask));
        end
        
        function output = generateOutput(obj)
            
            if obj.totalTime_d > 0
                upTimeInPercent = obj.upTime_d/obj.totalTime_d*100;
            else
                upTimeInPercent = 0;
            end
            
            output = struct(...
                'numberOfAlarms', obj.nTotalAlarms,...
                'alarmsLast12Hours', obj.nAlarmsLast12Hours,...
                'alarmsPer12Hours', obj.nAlarmsPer12Hours,...
                'upTime_h', obj.upTime_d*24,...
                'totalTime_d', obj.totalTime_d,...
                'upTime_percent',upTimeInPercent,...
                'TripOnlyLossAlarm', obj.alarmTypeCount(1),...
                'PitOnlyLossAlarm', obj.alarmTypeCount(2),...
                'FlowOnlyLossAlarm', obj.alarmTypeCount(3),...
                'FlowAndPressureLossAlarm', obj.alarmTypeCount(4),...
                'FlowAndPitLossAlarm', obj.alarmTypeCount(5),...
                'PitAndPressureLossAlarm', obj.alarmTypeCount(6),...
                'PitAndPressureInfluxAlarm', obj.alarmTypeCount(10),...
                'FlowAndPitInfluxAlarm', obj.alarmTypeCount(11),...
                'FlowAndPressureInfluxAlarm', obj.alarmTypeCount(12),...
                'FlowOnlyInfluxAlarm', obj.alarmTypeCount(13),...
                'PitOnlyInfluxAlarm', obj.alarmTypeCount(14),...
                'TripOnlyInfluxAlarm', obj.alarmTypeCount(15),...
                'PitSlowGainAlarm', obj.alarmTypeCount(16));
        end
        
        function [obj, output] = step(obj, currentTime_d, timeStep_s, alarmState, alarmType)
            obj = obj.updateTimeCount(alarmState, timeStep_s);
            if obj.isInitialized
                obj = obj.addAlarmIfNotPitSlowGainAlarm(currentTime_d, alarmType, alarmState);
                obj = obj.countNAlarmsLast12Hours(currentTime_d);
                obj = calculateNumberOfAlarmsPer12Hours(obj);
                obj.prevAlarmState = alarmState;
            else
                obj = obj.initialize(alarmState);
            end
            output = obj.generateOutput();
        end
    end
    
    methods (Static)
        function time_d = convertSecToDays(time_s)
            time_d = time_s/86400;
        end
    end
    
end

