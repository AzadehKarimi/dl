classdef EKD_ALGORITHM_OBJ
    %EKD_ALGORITHM Abstract class which defines an EKD algorithm
    
    properties (Abstract)
       % qIo_lpm % influx/outflux flow rate. An influx is positive, outflux negative.
    end
    
    methods (Abstract)
        initialize(obj,params) % initialize the algorithm
        stepIt(obj, processData , sysState,timeCurrent_d,dt_s) % run computations when stepping in time
        [obj, result ] = returnResultStruct(obj)
    end
    
end

