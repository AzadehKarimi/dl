classdef JENKINS_REPORT_OBJ < handle
%
% class generates a log file in html with links to matlab diary, jar-file
% and the status of unit test, build number etc, to be served by a
% http-server to moniotor build progress on build machine
% 
% the html of the currently running build can be updated at stages through
% the build so that the table is current also during the build, not only
% after.
%
%
    properties(SetAccess = private, GetAccess= public)
        diaryFileName ;
        commitHash;
        buildNr;
        numUnitTestsFailed;
        timeStampTxt;
        totalDurationSec;
        timerHandle;
        buildJarStatusTxt;
        
        diaryFilename;
        diaryFilenameWithPath;
        jarFilePath;
        
        indexFileNameWithPath 
        indexFileTemplateNameWithPath;
        jarText;
        buildTrigger;
        % 
        logLineStartPos;
        logLineLength;
        simTestStausTxt;
        simKPI;
        simPD;
        simFAR;
        btStatusTxt;
        btKPI;
        btPD;
        btFAR;
        numberOfScenariosRun;
        totalScenarioDuration_s;
    end
    properties (Constant)
        inProgressStatusTxt     = 'IN_PROGRESS...'
        startedStatusTxt        = 'Started...';
        didNotFinishTxt         = 'Aborted';
        indexFileInsertionTag   = '<!-INSERT_NEW_ROW_HERE-!>';
        jenkinsReportFolderName = 'Jenkins_SharedFolder';
        indexFile               = 'index.html';
        indexTemplateFile       = 'index_template.html';
    end
    methods
        function obj            = JENKINS_REPORT_OBJ(buildNr,commitHash,buildTrigger)
            obj.buildTrigger    = strrep(buildTrigger,'TRIGGER','');
            obj.indexFileNameWithPath = [obj.jenkinsReportFolderName,'\',obj.indexFile];
            obj.indexFileTemplateNameWithPath = [obj.jenkinsReportFolderName,'\',obj.indexTemplateFile];
            obj.buildNr         = buildNr;
            obj.numUnitTestsFailed = NaN;
            obj.timeStampTxt    = datestr(now,'yyyy_mm_dd_HH_MM_SS');
            obj.timerHandle     = tic;
            obj.simTestStausTxt ='';
            obj.buildJarStatusTxt='';
            
            truncatedDigits     = 7;
            if numel(commitHash)>=truncatedDigits
               obj.commitHash   = commitHash(1:truncatedDigits);
            else
               obj.commitHash   = commitHash; 
            end    
            obj.jarFilePath     = '';

            obj.buildJarStatusTxt = 'NotYet';
            
            obj.jarText         = obj.inProgressStatusTxt;
            obj.logLineStartPos = NaN;
            obj.logLineLength   = NaN;
            obj.diaryFilename   = sprintf('diary%i.txt',buildNr);
            obj.diaryFilenameWithPath = [obj.jenkinsReportFolderName,'\',obj.diaryFilename];
            diary(obj.diaryFilenameWithPath);
            diary on
            
            obj.simTestStausTxt  = 'NotYet';
            obj.simKPI  = NaN;
            obj.simPD   = NaN;
            obj.simFAR  = NaN;
            
            obj.btStatusTxt  = 'NotYet';
            obj.btKPI  = NaN;
            obj.btPD   = NaN;
            obj.btFAR  = NaN;
            
            obj.numberOfScenariosRun= NaN;
            obj.totalScenarioDuration_s= NaN;
            
            obj.cleanOldBuilds();
            obj.updateReport();
        end
        
        % removes "in progress" or similar status texts that are present
        % from previous crashed/aborted runs.
        function cleanOldBuilds(obj)
               fileName         = [obj.jenkinsReportFolderName,'\index.html'];
               if (exist(fileName)==2)
                   fileTextStr      = fileread(fileName);
                   fileTextStr      = strrep(fileTextStr, obj.startedStatusTxt,    obj.didNotFinishTxt );
                   fileTextStr      = strrep(fileTextStr, obj.inProgressStatusTxt, obj.didNotFinishTxt );
                   fid = fopen(fileName,'w');
                   fprintf(fid,fileTextStr);
                   fclose(fid);
               end
        end
        
        
        function  [id_str] =getIdStr(obj)
            id_str = sprintf('%s_build_%i',obj.timeStampTxt,obj.buildNr);
        end
        function obj            = reportJarFilePath(obj,jarFilePath)
            fprintf('jar file path:%s\n',jarFilePath);
            obj.jarFilePath = jarFilePath;
            obj.updateReport();
        end
        function obj = truncateAndSaveCommitHash(obj,gitCommitHash)
             truncatedDigits     = 7;
            if numel(gitCommitHash)>=truncatedDigits
               obj.commitHash    = gitCommitHash(1:truncatedDigits);
            else
               obj.commitHash   = gitCommitHash; 
            end    
        end
        function obj            = reportUnitTestsFailed(obj,numUnitTestsFailed)
            obj.numUnitTestsFailed = numUnitTestsFailed;
             obj.updateReport();
        end
        function obj           = reportSimTestStarted(obj)
            obj.simTestStausTxt = obj.startedStatusTxt;
            obj.updateReport();
        end
        function obj           = reportSimTestResult(obj,statusTxt,KPI,PD,FAR)
            obj.simTestStausTxt = statusTxt;
            obj.simKPI          = KPI;
            obj.simPD           = PD;
            obj.simFAR          = FAR;
            obj.updateReport();
        end
        function obj           = reportBuildJarStatus(obj,buildJarStatusTxt)
            obj.buildJarStatusTxt   = buildJarStatusTxt;
            obj.updateReport();
        end
        function obj           = reportNumUnitTestsFailed(obj,numUnitTestsFailed)
            obj.numUnitTestsFailed   = numUnitTestsFailed;
            obj.updateReport();
        end
        function obj           = reportBackTestStarted(obj)
            obj.btStatusTxt     = obj.startedStatusTxt;
            obj.updateReport();
        end
        function obj           = reportBackTestResult(obj,statusTxt,KPI,PD,FAR,numberOfScenariosRun,totalScenarioDuration_s)
             obj.btStatusTxt    = statusTxt;
             obj.btKPI          = KPI;
             obj.btPD           = PD;
             obj.btFAR          = FAR;
             obj.numberOfScenariosRun       = numberOfScenariosRun;
             obj.totalScenarioDuration_s    = totalScenarioDuration_s;
             obj.updateReport();
        end
        function obj           = evaluateBuildAndCopyOrDeleteJar(obj)
           if strcmp(obj.buildJarStatusTxt,'OK') && strcmp(obj.simTestStausTxt,'OK') && obj.numUnitTestsFailed ==0 
                jarFileName     = strrep(obj.jarFilePath,'codegen\','');
                newJarPath      = [obj.jenkinsReportFolderName,'\',jarFileName];
                try
                    movefile (obj.jarFilePath, obj.jenkinsReportFolderName);
                    obj.jarText = sprintf('<a href="%s">JAR</a><button type="button" value="%s">Deploy</button>',jarFileName,jarFileName);
                catch ME
                     disp('evaluateBuildAndCopyOrDeleteJar: error moving file...');
                     disp(obj.jarFilePath)
                     disp(newJarPath);
                     disp([ME.identifier,':',ME.message]);
                    obj.jarText = 'ERROR_MOVING_FILE';
                end
           else
              disp('rejected build, deleting jar file.');
              try 
                delete(obj.jarFilePath);
              catch
              end
              obj.jarText= 'REJECTED'; 
           end
           obj.updateReport();
        end
           
       % call whenever the log-column should be updated based on new development in the build    
       % on the first call, it addes new line,
       % all subsequent calls updates the line.
       function obj           = updateReport(obj)
           obj.totalDurationSec = toc(obj.timerHandle);
           htmlStringToInsert =[... 
            '<tr>',...  
            sprintf('<td>%s</td>',obj.buildTrigger),...
            sprintf('<td>%i</td>',obj.buildNr),...
            sprintf('<td>%s</td>',obj.timeStampTxt),...
            sprintf('<td>%s</td>',obj.commitHash),...
            sprintf('<td>%s</td>',obj.buildJarStatusTxt),...
            sprintf('<td>%i</td>',obj.numUnitTestsFailed),...
            sprintf('<td>%s</td>',obj.simTestStausTxt),...
            sprintf('<td>%.4f</td>',obj.simKPI),...
            sprintf('<td>%.4f</td>',obj.simPD),...
            sprintf('<td>%.4f</td>',obj.simFAR),...
            sprintf('<td>%s</td>',obj.btStatusTxt),...
            sprintf('<td>%.4f</td>',obj.btKPI),...
            sprintf('<td>%.4f</td>',obj.btPD),...
            sprintf('<td>%.4f</td>',obj.btFAR),...  
            sprintf('<td>%i</td>',obj.numberOfScenariosRun),...
            sprintf('<td>%.1f</td>',obj.totalScenarioDuration_s/3600),...  
            sprintf('<td><a href="%s">link</a></td>',obj.diaryFilename),...
            sprintf('<td>%s</td>',obj.jarText),...
            sprintf('<td>%.1f</td>',obj.totalDurationSec),...
            '</tr>'...
           ];
           prevLogLineLength = obj.logLineLength;
       
           disp('text to be inserted:');
           disp(htmlStringToInsert);
           try
               if exist(obj.indexFileNameWithPath, 'file') ==0
                    copyfile(obj.indexFileTemplateNameWithPath,obj.indexFileNameWithPath);
               end
               tempFileWithPath = [obj.jenkinsReportFolderName,'\',sprintf('index%i.html',obj.buildNr)];
               copyfile(obj.indexFileNameWithPath,tempFileWithPath);
               oldFileText      = fileread(tempFileWithPath);
               posStart         = strfind(oldFileText,obj.indexFileInsertionTag);
               obj.logLineStartPos = posStart + numel(obj.indexFileInsertionTag);
               fid              = fopen(obj.indexFileNameWithPath,'w+');% open or create new text file for writing
               fprintf(fid,oldFileText(1:obj.logLineStartPos-1));
               obj.logLineLength = fprintf(fid,'%s\n',htmlStringToInsert);
               fprintf('wrote %i bytes',obj.logLineLength);
               if isnan(prevLogLineLength)% if first time calling update 
                    fprintf(fid,oldFileText(obj.logLineStartPos:end));
               else
                    fprintf(fid,oldFileText(obj.logLineStartPos+prevLogLineLength:end));
               end
               fclose(fid);
           catch e
               disp(getReport(e,'extended'));
           end
        end
    end
        
        
end