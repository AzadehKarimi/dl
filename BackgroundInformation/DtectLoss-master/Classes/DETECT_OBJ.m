classdef DETECT_OBJ
    %   DETECT_OBJ Summary of this class goes here
    %   Detailed explanation goes here
    %   Use washed drilling data
    %   Run EKD algorithm to detect inlfuxes and losses
    %   Calculate relative compressibility dbdV_e_barm3, influx/loss rate
    %   qIo_lpm, influx/loss volume, warning/alarm
    %   Save “detection” results data to csv file.
    
    properties
        numDataPoints              ;
        avg_dt_s                   ;
        flowDeadband_lpm           ;
        volumeThresholdWarning_m3  ;
        volumeThresholdAlarm_m3    ;

        buffer                     ;
    end
    
    methods
        function obj=DETECT_OBJ(flowDeadband_lpm, volumeThresholdWarning_m3, volumeThresholdAlarm_m3)
            obj.flowDeadband_lpm           = flowDeadband_lpm;
            obj.volumeThresholdWarning_m3  = volumeThresholdWarning_m3;
            obj.volumeThresholdAlarm_m3    = volumeThresholdAlarm_m3;
        end
               
        function obj =  initialize(obj, processobj)%Constructor
            obj.numDataPoints         = processobj.numDataPoints;
            obj.buffer.t_s            = nan(1, obj.numDataPoints);
            obj.buffer.sys_states     = nan(1, obj.numDataPoints);
            obj.buffer.dbdV_e_barm3   = nan(1, obj.numDataPoints);
            obj.buffer.dpsp_dt_bar    = nan(1, obj.numDataPoints);
            obj.buffer.qIo_pdiff_lpm  = nan(1, obj.numDataPoints);
            obj.buffer.qIo_nodiff_lpm = nan(1, obj.numDataPoints);
            obj.buffer.VIo_pdiff_m3   = nan(1, obj.numDataPoints);
            obj.buffer.VIo_nodiff_m3  = nan(1, obj.numDataPoints);
            obj.buffer.alarm_nodiff   = nan(1, obj.numDataPoints);
            obj.buffer.alarm_pdiff    = nan(1, obj.numDataPoints);
            obj.avg_dt_s              = processobj.avg_dt_s;
        end

        function obj =  runpdiff(obj, processobj, pdiff, pdiff_noP)
            qin_lp_Tc   = 1;
            qin_lp      = LOWPASS_OBJ(obj.avg_dt_s,qin_lp_Tc);

            VIo_pdiff_m3  = 0;
            VIo_nodiff_m3 = 0;
            alarm_pdiff   = 0;
            alarm_nodiff  = 0;

            i=1;
            while ( i < obj.numDataPoints ),%run EKD algorithm (iMPD inspired - delta flow only)
                pdiff       = pdiff.stepIt     (processobj.buffer.q_p_lpm(i), processobj.buffer.q_out_lpm(i) , processobj.buffer.p_p_bar (i),   processobj.buffer.sys_states (i));
                pdiff_noP   = pdiff_noP.stepIt (processobj.buffer.q_p_lpm(i), processobj.buffer.q_out_lpm(i) , processobj.buffer.p_p_bar (i),   processobj.buffer.sys_states (i));
            
                %To be improved
                if abs(pdiff.qIo_lpm) > obj.flowDeadband_lpm,
                    VIo_pdiff_m3  = VIo_pdiff_m3  + obj.avg_dt_s/60000*pdiff.qIo_lpm;%To be fixed
                end
                if abs(VIo_pdiff_m3) > obj.volumeThresholdAlarm_m3,
                    alarm_pdiff = 2;
                elseif abs(VIo_pdiff_m3) > obj.volumeThresholdWarning_m3,
                    alarm_pdiff = 1;
                else
                    alarm_pdiff = 0;
                end
                    
                %To be improved
                if abs(pdiff_noP.qIo_lpm) > obj.flowDeadband_lpm,
                    VIo_nodiff_m3 = VIo_nodiff_m3 + obj.avg_dt_s/60000*pdiff_noP.qIo_lpm;%To be fixed
                end
                if abs(VIo_nodiff_m3) > obj.volumeThresholdAlarm_m3,
                    alarm_nodiff = 2;
                elseif abs(VIo_nodiff_m3) > obj.volumeThresholdWarning_m3,
                    alarm_nodiff = 1;
                else
                    alarm_nodiff = 0;
                end
                
                % %                 
                obj.buffer.t_s(i)            = processobj.buffer.t_s(i);
                obj.buffer.sys_states(i)     = processobj.buffer.sys_states(i);
                obj.buffer.dbdV_e_barm3(i)   = pdiff.betaDivByV_e_bar_per_m3;
                obj.buffer.dpsp_dt_bar(i)    = pdiff.dpsp_dt_bar;
                obj.buffer.qIo_pdiff_lpm(i)  = pdiff.qIo_lpm;
                obj.buffer.qIo_nodiff_lpm(i) = pdiff_noP.qIo_lpm;
                obj.buffer.VIo_pdiff_m3(i)   = VIo_pdiff_m3;
                obj.buffer.VIo_nodiff_m3(i)  = VIo_nodiff_m3;
                obj.buffer.alarm_pdiff(i)    = alarm_pdiff;
                obj.buffer.alarm_nodiff(i)   = alarm_nodiff;
                i = i+1;
            end
        end
       
        function obj =  plotDetectionResults(obj)
            showlegend=false;
            tm=obj.buffer.t_s/60;
            tm=tm-tm(1);
            global fignr;
            figure(fignr);fignr=fignr+1;
            ax(1)=subplot(321);
            plot(tm,obj.buffer.sys_states,'LineWidth',2)
            ylabel('System state')
            legend('State')
            title('Drill(1);FCheck(2);Cond(3);Trip(4)')
            grid
            ax(2)=subplot(322);
            plot(tm,obj.buffer.dbdV_e_barm3,'LineWidth',2)
            ylabel('beta/Vol [bar/m3]')
            legend('Est beta/Vol')
            grid
            ax(3)=subplot(323);
            plot(tm,obj.buffer.qIo_pdiff_lpm,tm,obj.buffer.qIo_nodiff_lpm,'LineWidth',2)
            ylabel('[lpm]')
            legend('qIo pdiff','qIo nodiff')
            grid
            ax(4)=subplot(324);
            plot(tm,obj.buffer.dpsp_dt_bar,'LineWidth',2)
            ylabel('[bar/sec]')
            legend('dppdt')
            grid
            ax(5)=subplot(325);
            plot(tm,obj.buffer.VIo_pdiff_m3,tm,obj.buffer.VIo_nodiff_m3,'LineWidth',2)
            ylabel('[m3]')
            legend('VIo pdiff','VIo nodiff')
            grid
            ax(6)=subplot(326);
            plot(tm,obj.buffer.alarm_pdiff,tm,obj.buffer.alarm_nodiff,'LineWidth',2)
            title('0=normal,1=warning,2=alarm')
            ylabel('Alarm')
            legend('alarm pdiff','alarm nodiff')
            grid
            linkaxes(ax,'x') 

        end

        
        function obj = writerescsvfile(obj,filename)%save res to csv file
            fHeader1='TIME;SYSSTATE;dbdV_e_barm3;dpsp_dt_bar;qIo_pdiff_lpm;VIo_pdiff_m3;alarm_pdiff;qIo_nodiff_lpm;VIo_nodiff_m3;alarm_nodiff';
            fHeader2='sec;unitless;bar/m3;bar/s;lpm;m3;unitless;lpm;m3;unitless';

            data=[...
                obj.buffer.sys_states;...
                obj.buffer.dbdV_e_barm3;...
                obj.buffer.dpsp_dt_bar;...
                obj.buffer.qIo_pdiff_lpm;...
                obj.buffer.VIo_pdiff_m3;...
                obj.buffer.alarm_pdiff;...
                obj.buffer.qIo_nodiff_lpm;...
                obj.buffer.VIo_nodiff_m3;...
                obj.buffer.alarm_nodiff;...
                ];
             Ncol=9;
             Nrow=length(obj.buffer.t_s)-1;
             % open a file for writing
             fid = fopen(filename, 'w');
             if fid==-1,
                disp(['Cannot open file ' filename ' for writing']);
             else
                disp(['Saving to file ' filename]);
                fprintf(fid, '%s\n',fHeader1);
                fprintf(fid, '%s\n',fHeader2);
                tvec=datevec(obj.buffer.t_s/24/3600);

                for i=1:Nrow,%time
                    fprintf(fid, '%4.0f-%02.0f-%02.0fT%02.0f:%02.0f:%02.0fZ;%.2f', tvec(i,1), tvec(i,2),tvec(i,3),tvec(i,4),tvec(i,5),tvec(i,6));
                    for j=1:Ncol,%var
                        fprintf(fid, ';%.2f;',data(j,i));
                    end
                    fprintf(fid, '\n');
                end
                fclose(fid);
             end
        end
    end  
end

