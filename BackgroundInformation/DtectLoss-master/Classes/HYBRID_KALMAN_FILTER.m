classdef HYBRID_KALMAN_FILTER < DTECTLOSS_SUBMODULE
    %HYBRID_KALMAN_FILTER Hybrid Kalman filter for one state (scalar)
    %   https://en.wikipedia.org/wiki/Kalman_filter
    
    properties (SetAccess = private)
        r;
        xK;
        PK;
        xPrevPri;
        PPrevPri;
        rb;
        sleepModeActive;
        totalLossOfReturnFlowRate_boolean;
        timeDurationWithZeroReturnFlow_s;
    end
    
    properties (Constant)
        q = 50^2;
        a = -1/6;
        b = 1/6;
        c = 1;
        covarLag = 45; % samples for lagged computation of covariance of flow-out
        nBuffer = 100 + 45;
        timeStepUpperLim_s = 60; % [s] do not update if the time-step is larger than this limit
        errVal = -0.99925;
        distanceConsideredDrilling_m = 0.5; % [m]
        timeDurationConsideredAsLossOfReturnFlow_s = 20; % [s]
        flowOutLowLimit_lpm = 5;
    end
    
    methods
        
        function obj = HYBRID_KALMAN_FILTER()
            obj.r = [];
            obj.xK = [];
            obj.PK = [];
            obj.xPrevPri = [];
            obj.PPrevPri = [];
            obj.sleepModeActive = true;
            obj.totalLossOfReturnFlowRate_boolean = false;
            obj.timeDurationWithZeroReturnFlow_s = [];
        end
        
        function obj = initialize(obj, covMeas, x, P)
            obj.r = covMeas;
            obj.xK = x;
            obj.PK = P;
            obj.xPrevPri = x;
            obj.PPrevPri = P;
            obj.rb = RINGBUFFER_OBJ(obj.nBuffer); % ringbuffer
            if x <= 0
                obj.sleepModeActive = true;
            else
                obj.sleepModeActive = false;
            end
            obj.timeDurationWithZeroReturnFlow_s = 0;
        end
        
        function obj = softReset(obj)
            obj.xK = 0;
            obj.xPrevPri = 0;
            obj.sleepModeActive = true;
            obj.totalLossOfReturnFlowRate_boolean = false;
            obj.timeDurationWithZeroReturnFlow_s = 0;
        end
        
        function obj = addFlowAndDepthMeas(obj, flowOutTime_d, flowOut_lpm, depthBit_m, depthHole_m)
            if abs(depthBit_m - depthHole_m) < obj.distanceConsideredDrilling_m
                obj.rb = obj.rb.addDataToCircularBuffer(flowOutTime_d, flowOut_lpm);
                varCalc = obj.rb.varFirstHalf;
                if ~isempty(varCalc)
                    obj = obj.setMeasCovar(varCalc);
                end
            else
                obj = obj.setMeasCovar(obj.q);
            end
        end
        
        function obj = setMeasCovar(obj, measCovar)
            obj.r = measCovar;
        end
        
        function obj = update(obj, flowIn_lpm, flowOut_lpm, dt_s)
            % if time step is zero, do nothing.
            if dt_s == 0
                return
            end
            
            % if time step is too large, or measurment or input is missing,
            % set estimate to errVal.
            if dt_s > obj.timeStepUpperLim_s || flowOut_lpm == obj.errVal || flowIn_lpm == obj.errVal
                obj.xK = obj.errVal;
                return
            end
            
            % if the measured return flow-rate is different from zero,
            % deactivate sleep mode.
            if flowOut_lpm ~= 0
                obj.sleepModeActive = false;
            elseif all(obj.rb.Vbuffer == 0)
                obj.sleepModeActive = true;
            end
            
            % if the measured return flow-rate has not been above zero
            % since init, or if the measured return flow has been around
            % zero for a given time, then return with zero as predicted
            % return flow-rate.
            obj = checkForLossOfReturnFlow(obj, flowOut_lpm, dt_s);
            if obj.sleepModeActive || obj.totalLossOfReturnFlowRate_boolean
                obj.xK = 0;
                return
            end
            
            % Predict
            % a priori state estimate
            xKPri = obj.xPrevPri + dt_s*(obj.a*obj.xPrevPri + obj.b*flowIn_lpm);
            % a priori estimate covariance
            PKPri = obj.PPrevPri + dt_s*(obj.a^2*obj.PPrevPri + obj.q);
            
            % Update
            yErrK = flowOut_lpm - obj.c*xKPri;
            SK = obj.c^2*PKPri + obj.r;
            if SK == 0 % avoid division with zero
                SK = 1e-5;
            end
            KK = PKPri*obj.c/SK;
            obj.xK = xKPri + KK*yErrK;
            obj.PK = (1 - KK*obj.c)*PKPri;
            
            obj.xPrevPri = obj.xK;
            obj.PPrevPri = obj.PK;
        end
        
        function obj = checkForLossOfReturnFlow(obj, flowOut_lpm, timeStep_s)
            obj = zeroReturnFlowTimer(obj, flowOut_lpm, timeStep_s);
            if obj.timeDurationWithZeroReturnFlow_s > obj.timeDurationConsideredAsLossOfReturnFlow_s
                obj.totalLossOfReturnFlowRate_boolean = true;
            else
                obj.totalLossOfReturnFlowRate_boolean = false;
            end
        end
        
        function obj = zeroReturnFlowTimer(obj, flowOut_lpm, timeStep_s)
            if flowOut_lpm > obj.flowOutLowLimit_lpm || flowOut_lpm == obj.errVal
                obj.timeDurationWithZeroReturnFlow_s = 0;
            else
                obj.timeDurationWithZeroReturnFlow_s = obj.timeDurationWithZeroReturnFlow_s + timeStep_s;
            end
                
        end
    end
    
end

