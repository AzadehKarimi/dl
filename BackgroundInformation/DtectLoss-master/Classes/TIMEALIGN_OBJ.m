classdef TIMEALIGN_OBJ
    %TIMEALIGN_OBJ Finds timeconstant and timedelay to align two
    %time-series
    %   Detailed explanation goes here
 	properties(SetAccess = private)
        params;
        N_vec;
    end

    properties (Constant)
        doDebugPlots = false;%true;
        doDebugText  = false;%true;
    end
    
    methods 
        function obj    = TIMEALIGN_OBJ(params)
            obj.params = params;
            obj.N_vec  = params.N_vec;
        end

        % aligns values in inputSignal and refSignal, both values sharing the same 
        % time-vector timeVec_d
        % inputSignal: signal to be shifted and filtered to match refSignal
        % refSignal: reference signal that inputSignal should try to match
        % timeVec_d : vector of time in matlab-days
        function [obj, timeDelay_samples, timeConstant_s, timeDelay_s] = align(obj, inputSignal, refSignal, timeVec_d)
            if obj.doDebugText
                tic
            end
            N_samples         = numel(inputSignal);
            dt_avg_d          = mean(timeVec_d(end:-1:2)-timeVec_d(end-1:-1:1));
 
            timeDelay_samples     = 0;
            timeConstant_s        = 0;
            [obj, objFunLast]     = obj.calcDiffSum(timeDelay_samples,timeConstant_s,timeVec_d,inputSignal,refSignal);
            timeConstantBest      = timeConstant_s;
            timeDelay_samplesBest = timeDelay_samples;
            objFunBest            = objFunLast;            
            if obj.doDebugText
                fprintf('\nStart align \n');
            end
            %first iterate just on time-delay,Tc=0
            timeDelay_samples     = 1;
            isObjFunImproving     = true;
            while (isObjFunImproving && timeDelay_samples < N_samples)
                [obj, objFun]     = obj.calcDiffSum(timeDelay_samples, timeConstant_s, timeVec_d, inputSignal, refSignal);
                if obj.doDebugText
                   fprintf('Testing delay - timeDelay: %i samples | timeConstant: %.2f sec| obj: %.4f \n',timeDelay_samples,timeConstant_s,objFun);
                end
                if objFun < objFunLast
                    if (objFun < objFunBest)
                        objFunBest            = objFun;
                        timeDelay_samplesBest = timeDelay_samples;
                    end
                    % keep looking
                    timeDelay_samples = timeDelay_samples + 1;
                    objFunLast        = objFun;
                else
                    isObjFunImproving = false;
                end
            end
            timeDelay_samples = timeDelay_samplesBest;
            % part 2: try decreasing time-delay by multiples of dt_avg_d
            % and increasing Tc likewise, see if objective function
            % improves
            isObjFunImproving         = true;
            timeConstant_max_s        = 3*dt_avg_d*86400;
            timeConstant_resolution_s = timeConstant_max_s/obj.N_vec;
            while( isObjFunImproving && timeDelay_samples >= 1 )
                timeDelay_samples       = timeDelay_samples - 1;
                timeConstantVec_s       = timeConstantBest + (1:obj.N_vec)*timeConstant_resolution_s;
                % todo: you could improve the results somewhat by
                % generalizing this to a sort of binary search algorithm
                % for Tc
                
                objFun_Vec = zeros(obj.N_vec,1);
                for ii=1:obj.N_vec
                    [obj, objFun_Vec(ii)] = obj.calcDiffSum(timeDelay_samples, timeConstantVec_s(ii), timeVec_d, inputSignal, refSignal);
                    if obj.doDebugText
                        fprintf('Testing time Constant - timeDelay: %i samples | timeConstant: %.2f sec | obj: %.4f \n', timeDelay_samples, timeConstantVec_s(ii), objFun_Vec(ii));                    
                    end
                end
                if min(objFun_Vec) > objFunBest
                    isObjFunImproving = false;
                else
                    best_index            = find(objFun_Vec <= min(objFun_Vec), 1);
                    objFunBest            = objFun_Vec(best_index);
                    timeConstantBest      = timeConstantVec_s(best_index);
                    timeDelay_samplesBest = timeDelay_samples;
                    if obj.doDebugText
                        fprintf('Testing best so far: timeDelay: %i samples | timeConstant: %.2f sec | obj: %.4f \n', timeDelay_samplesBest, timeConstantBest, objFunBest);
                    end
                end
            end
            timeConstant_s     = timeConstantBest;
            timeDelay_samples  = timeDelay_samplesBest;
            timeDelay_s        = dt_avg_d*86400*timeDelay_samples;
            if obj.doDebugText
                fprintf(['Align function duration: '  num2str(toc,'%.3f') 'sec\n']);
            end
        end
        
        % inputSignal and refSignal are values of time-series to be calculated difference
        % for
        function [obj, resultingSum]= calcDiffSum(obj, timeDelay_samples, timeConstant_s, timeVec_d, inputSignal, refSignal)
            inputSignalShifted = inputSignal(1:(end - timeDelay_samples));
            % 
            deltaT_s           = (timeVec_d(2)-timeVec_d(1))*86400;
            if timeConstant_s > 0
                lp                               = LOWPASS_OBJ(deltaT_s, timeConstant_s);
                inputSignalShiftedAndFiltered    = zeros(1, numel(inputSignalShifted));
                lp                               = lp.filter(refSignal(1),1);
                inputSignalShiftedAndFiltered(1) = lp.FilteredSignal;
                for k = 2:1:numel(inputSignalShifted)
                    lp                               = lp.filter(inputSignalShifted(k),0);
                    inputSignalShiftedAndFiltered(k) = lp.FilteredSignal;
                end
            else
               inputSignalShiftedAndFiltered = inputSignalShifted;
            end
            refSignaladjusted = refSignal((1 + timeDelay_samples):end);
            N_obj             = numel(inputSignal) - timeDelay_samples;
            resultingSum      = sum((inputSignalShiftedAndFiltered - refSignaladjusted).^2./N_obj);
            
            if obj.doDebugText
                fprintf('calcDiffSum - timeDelay: %i samples (%.2f sec) | timeConstant: %.2f sec | obj: %.4f\n', timeDelay_samples, timeDelay_samples*deltaT_s, timeConstant_s, resultingSum);
            end          
            if obj.doDebugPlots
                text = sprintf('calcDiffSum - timeDelay: %i samples (%.2f sec) | timeConstant: %.2f sec | obj: %.4f',timeDelay_samples, timeDelay_samples*deltaT_s, timeConstant_s, resultingSum);                                
                figure(1)
                hold on
                grid on
                title(text);
                plot(inputSignalShiftedAndFiltered)
                plot(refSignaladjusted,'r');
                pause(0.2)
                hold off
             end
        end        
    end    
end

