classdef EXPECTED_FLOWOUT_FROM_FLOWIN_OBJ < DTECTLOSS_SUBMODULE
% 
% this class calculates timeconstant T and timedelay td between
% flowIn and flowOut so that:
% 
% flowOut(t) = 1/(1+timeConst_s*s)*flowIn(t-timeDelay_s) 
% 
% to do that this class buffers data during a connection.
% Note that for DeepSeaAtlantic-19 the flowOut_lpm_wash is low-pass
% filtered and takes three minutes to reach zero flow after 
% flowIn is stopped. To avoid false alarms, we would like flowOutExpected
% to also handle such cases.
    
    properties(SetAccess = private)
        timeConstFlowOut_s;
        timeDelayFlowOut_s ;
        timeDelayMin_s;
        timeDelayMax_s;
        flowOutExpected_lpm;
        vol0FlowOutPumpStart_m3;
        isCalibratingFlowInFlowOut;
        isCalibratedExpFlowOut;
        timealignobj;
        params; %Tuning params
    end
    
    properties (SetAccess = private, GetAccess = private)
        lowPassFlowObj;
        ringbufferObj;        
        
        samplesInBufferBelowShutdownThreshold;
        samplesInBufferAboveFullflowThreshold;
        samplesSinceLastCalibration;
        samplesSinceLastFullFlow;
        
        isDownlinkingBufferObj;
        flowInBufferObj;
        flowOutBufferObj;
        timePrev_d;
        doDebugPlots;
        doDebugText;
    end
    
    properties (Constant)
        %BUFFER_SIZE_SAMPLES*dt_s must be longer than it takes for flowOut to settle after pump shutdown
        BUFFER_SIZE_SAMPLES                 = 100;
        flowInMaxMinThreshold_lpm           = 100;
        flowOutMaxMinThreshold_lpm          = 300;
        NvaluesforSteady                    = 5;
    end
    
	methods
        function obj = EXPECTED_FLOWOUT_FROM_FLOWIN_OBJ()
            
        end
        
        function obj = initalize(obj, params)
            obj.params                      = params;
            obj.vol0FlowOutPumpStart_m3     = obj.params.vol0FlowOutPumpStartThreshold_m3;
            obj.timeConstFlowOut_s          = 0; 
            obj.timeDelayFlowOut_s          = 0;
            obj.flowOutExpected_lpm         = 0;
            obj.timePrev_d                  = 0;

            obj.isCalibratingFlowInFlowOut  = false;
            obj.isCalibratedExpFlowOut      = false;

            obj.lowPassFlowObj              = LOWPASS_OBJ(1,obj.timeConstFlowOut_s);
            obj.flowInBufferObj             = RINGBUFFER_OBJ(obj.BUFFER_SIZE_SAMPLES);
            obj.flowOutBufferObj            = RINGBUFFER_OBJ(obj.BUFFER_SIZE_SAMPLES);
            obj.isDownlinkingBufferObj      = RINGBUFFER_OBJ(obj.BUFFER_SIZE_SAMPLES);
            obj.timealignobj                = TIMEALIGN_OBJ(params);
            
            obj.samplesSinceLastCalibration = inf;
            obj.samplesSinceLastFullFlow    = inf;
            obj.samplesInBufferBelowShutdownThreshold = 0;
            obj.samplesInBufferAboveFullflowThreshold = 0;
            
            obj.timeDelayMin_s         = -0.99925;
            obj.timeDelayMax_s         = -0.99925;
  
            obj.doDebugPlots           = false;
            obj.doDebugText            = false;            
        end

        function obj = softReset(obj)
            obj.vol0FlowOutPumpStart_m3     = obj.params.vol0FlowOutPumpStartThreshold_m3;
            obj.timeConstFlowOut_s          = 0; 
            obj.timeDelayFlowOut_s          = 0;
            obj.flowOutExpected_lpm         = 0;
            obj.timePrev_d                  = 0;

            obj.isCalibratingFlowInFlowOut  = false;
            obj.isCalibratedExpFlowOut      = false;           
            obj.samplesSinceLastCalibration = inf;
            obj.samplesSinceLastFullFlow    = inf;
            obj.samplesInBufferBelowShutdownThreshold = 0;
            obj.samplesInBufferAboveFullflowThreshold = 0;
            
            obj.timeDelayMin_s         = -0.99925;
            obj.timeDelayMax_s         = -0.99925;
  
            obj.doDebugPlots           = false;
            obj.doDebugText            = false;            
        end
        
        % time-delay is found by direct search.
        function obj = identifyTimeConstantAndTimeDelayBasedOnBuffer(obj)
                        
            % part one: set Tc to zero and find time-delay
            [T1_d_raw, flowIn_lpm_raw,  ~]    = obj.flowInBufferObj.deringbufferAndResample();
            [~, flowOut_lpm_raw, dt_avg_d]    = obj.flowOutBufferObj.deringbufferAndResample();
            [~,isDownlinkingVec,~]            = obj.isDownlinkingBufferObj.deringbufferAndResample();
            ind                               = isDownlinkingVec==0;
             % if no data is valid, return without doing anything.
             if ~any(ind)
                 return
             end
             flowIn_lpm     = flowIn_lpm_raw(ind);
             flowOut_lpm    = flowOut_lpm_raw(ind);
             T1_d           = T1_d_raw(ind);
         
             % if T1_d is a scalar, then
             % calcSumOfSquaredDifferenceBtwBufferedFlowInAndFlowOut will
             % error. This is a quick fix.
             if numel(T1_d) < 2
                 return
             end
            [obj.timealignobj, td_samples, timeConst_s] = obj.timealignobj.align(flowIn_lpm,flowOut_lpm,T1_d);
            if td_samples < obj.BUFFER_SIZE_SAMPLES
                obj.timeDelayFlowOut_s = dt_avg_d*86400*td_samples;
                obj.timeDelayMin_s     = dt_avg_d*86400*(td_samples-0.99);
                obj.timeDelayMax_s     = dt_avg_d*86400*(td_samples+0.99);
                obj.timeConstFlowOut_s = timeConst_s;
                obj.lowPassFlowObj     = LOWPASS_OBJ(1,obj.timeConstFlowOut_s);
            end
        end
        
        function [obj, answer] = shouldWeDetermineTimeDelayNow(obj)
            if (obj.flowInBufferObj.getLastValue() < obj.params.flowInShutdownThreshold_lpm)
                obj.samplesInBufferBelowShutdownThreshold = obj.samplesInBufferBelowShutdownThreshold + 1;
            else
                obj.samplesInBufferBelowShutdownThreshold = 0;
            end
            if (obj.flowInBufferObj.getLastValue() > obj.params.flowInFullflowThreshold_lpm)   
                obj.samplesSinceLastFullFlow            = 0;
            else
                obj.samplesSinceLastFullFlow            = obj.samplesSinceLastFullFlow + 1;
            end
            avgBufferSampleTime_s                       = obj.flowInBufferObj.GetAvgBufferSampleTime();
            if avgBufferSampleTime_s == -0.99925
                doesBufferContainShutdownOverThresholdMinutes  = 0;
            else
                doesBufferContainShutdownOverThresholdMinutes  = obj.samplesInBufferBelowShutdownThreshold*avgBufferSampleTime_s > obj.params.flowInShutDownDurationThresholdToCalibrateFlow_s ;% this assumes that the shutdown length in time 
            end
            haveNotRecentlyDoneAcalibration = obj.samplesSinceLastCalibration > obj.BUFFER_SIZE_SAMPLES;
            fullFlowSeenRecently            = obj.samplesSinceLastFullFlow<obj.BUFFER_SIZE_SAMPLES;
            
            if doesBufferContainShutdownOverThresholdMinutes && haveNotRecentlyDoneAcalibration && fullFlowSeenRecently
               answer                                   = 1;
               obj.samplesSinceLastCalibration          = 0;
            else
               obj.samplesSinceLastCalibration          = obj.samplesSinceLastCalibration + 1; 
               answer                                   = 0;
            end
            obj.isCalibratingFlowInFlowOut              = answer;
        end
              
		function [obj, flowInOutOutput] = step(obj, flowIn_lpm, flowOut_lpm, isDownlinking, ~, timeNew_d)
            if flowIn_lpm == -0.99925
                flowInOutOutput.isCalibratingFlowInFlowOut  = obj.isCalibratingFlowInFlowOut; 
                flowInOutOutput.isCalibratedExpFlowOut      = obj.isCalibratedExpFlowOut; 
                flowInOutOutput.flowOutExpected_lpm         = obj.flowOutExpected_lpm; 
                flowInOutOutput.timeDelayFlowOut_s          = obj.timeDelayFlowOut_s; 
                flowInOutOutput.timeConstFlowOut_s          = obj.timeConstFlowOut_s; 
                flowInOutOutput.vol0FlowOutPumpStart_m3     = obj.vol0FlowOutPumpStart_m3; 
                return;
            end                       
            dt_s = (timeNew_d - obj.timePrev_d)*86400;
            obj.timePrev_d = timeNew_d;

            persistent timeOld_d;
            if isempty(timeOld_d)
                timeOld_d = timeNew_d;
            end
                       
            if flowIn_lpm  ~= -0.99925 && flowOut_lpm  ~= -0.99925
                obj.flowInBufferObj                 = obj.flowInBufferObj.addDataToCircularBuffer(timeNew_d,flowIn_lpm);
                obj.flowOutBufferObj                = obj.flowOutBufferObj.addDataToCircularBuffer(timeNew_d,flowOut_lpm);
                obj.isDownlinkingBufferObj          = obj.isDownlinkingBufferObj.addDataToCircularBuffer(timeNew_d,isDownlinking);
            end
            %if there is a shutdown in the centre of the buffered data,
            %then it do an estimation of the time delay
            if flowOut_lpm  == -0.99925 % missing flow out, for instance on StenaDon
                obj.flowOutExpected_lpm                 = flowIn_lpm;
            else
                [obj,doCalibration]                     = obj.shouldWeDetermineTimeDelayNow();
                if doCalibration
                     obj                                = obj.identifyTimeConstantAndTimeDelayBasedOnBuffer();
                end
                [flowInDelayed_lpm,dt_newestDatapoint_s]= obj.flowInBufferObj.getPastValueWithExplicitLastCallTime(obj.timeDelayFlowOut_s,timeNew_d,timeOld_d);
                if (flowInDelayed_lpm == -0.99925)
                    obj.flowOutExpected_lpm             = -0.99925;
                else
                   % disp(dt_newestDatapoint_s);
                    if obj.timeConstFlowOut_s > 0
                        if dt_newestDatapoint_s > 0 % depending on the sampling of the signal(non-synchronous) there may not be any newer datapoint, 
                            obj.lowPassFlowObj          = obj.lowPassFlowObj.filterForCustomDt(flowInDelayed_lpm,dt_newestDatapoint_s,0);
                            obj.flowOutExpected_lpm     = obj.lowPassFlowObj.FilteredSignal;
                        else
                            if ~isempty(obj.lowPassFlowObj.FilteredSignal)
                                obj.flowOutExpected_lpm     = obj.lowPassFlowObj.FilteredSignal;
                            else
                                obj.flowOutExpected_lpm     = flowInDelayed_lpm;
                            end
                        end
                    else
                        obj.flowOutExpected_lpm         = flowInDelayed_lpm;
                    end
                end
                
                if (dt_s > 30 || dt_s < 0) %bad time step
                    obj.vol0FlowOutPumpStart_m3 = 0;
                else
                    if (flowIn_lpm < 50) %Pump off
                        obj.vol0FlowOutPumpStart_m3 = 0;
                    elseif (obj.vol0FlowOutPumpStart_m3 < obj.params.vol0FlowOutPumpStartThreshold_m3)  %Pump running
                        obj.vol0FlowOutPumpStart_m3 = obj.vol0FlowOutPumpStart_m3 + dt_s*flowIn_lpm/60000;
                    end
                end        
                if (flowIn_lpm > 50 && ... %Pump running
                        flowOut_lpm < 100 && ... %No/low measured flow out
                        obj.vol0FlowOutPumpStart_m3 < obj.params.vol0FlowOutPumpStartThreshold_m3) %Accumulate void volume
                    obj.flowOutExpected_lpm = 0;%Set expected flow out to 0 until void volume is filled
                end                
                if not(obj.isCalibratedExpFlowOut) && (obj.timeConstFlowOut_s > 0 || dt_newestDatapoint_s > 0) 
                    obj.isCalibratedExpFlowOut = true;
                end                
            end
            flowInOutOutput.isCalibratingFlowInFlowOut  = obj.isCalibratingFlowInFlowOut; 
            flowInOutOutput.isCalibratedExpFlowOut      = obj.isCalibratedExpFlowOut; 
            flowInOutOutput.flowOutExpected_lpm         = obj.flowOutExpected_lpm; 
            flowInOutOutput.timeDelayFlowOut_s          = obj.timeDelayFlowOut_s; 
            flowInOutOutput.timeConstFlowOut_s          = obj.timeConstFlowOut_s; 
            flowInOutOutput.vol0FlowOutPumpStart_m3     = obj.vol0FlowOutPumpStart_m3; 
            timeOld_d                                   = timeNew_d;            
        end
    end
end