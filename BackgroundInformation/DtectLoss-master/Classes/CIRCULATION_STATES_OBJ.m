classdef CIRCULATION_STATES_OBJ
    %CIRCULATION_STATES_OBJ States which the circulation system can be in, e.g.
    %drilling, circulating, not circulating.
    
    % The available states are defined as enums in CirculationStates.m.
    
    properties
        currentState;
        previousState;
    end
    
    properties (Constant)
        MISSING_VAL_CODE = -0.99925;
        qThreshold = 50; % [lpm] threshold for flow-rate from rig pump. If the flow rate from the rig pumps are lower than this value -> not circulating
        depthThreshold = 0.5; % [m] threshold for when bit is on bottom. If the bit close than this value to the holeDepth -> drilling
    end
    
    methods
        function obj = CIRCULATION_STATES_OBJ()
            obj.currentState = CirculationStates.notCirculating;
            obj.previousState = CirculationStates.circulating;
        end
        
        function obj = changeState(obj, toState)
            obj.previousState = obj.currentState;
            obj.currentState = toState;
        end
        
        function obj = stateMachine(obj, qp_lpm, bitDepth_m, holeDepth_m)
            if qp_lpm < 0 || bitDepth_m < 0 || holeDepth_m < 0
                return
            end
            
            if qp_lpm <= obj.qThreshold
                obj = obj.changeState(CirculationStates.notCirculating); % from any state, go back to notCirculating if not flowing
            elseif qp_lpm > obj.qThreshold && obj.currentState == CirculationStates.notCirculating
                obj = obj.changeState(CirculationStates.circulating); % from notCirculating to circulating
            elseif bitDepth_m > holeDepth_m - obj.depthThreshold && obj.currentState == CirculationStates.circulating
                obj = obj.changeState(CirculationStates.drilling); % from circulating to drilling
            elseif bitDepth_m <= holeDepth_m - obj.depthThreshold && obj.currentState == CirculationStates.drilling
                obj = obj.changeState(CirculationStates.circulating); % from drilling back to circulating
            end
        end
    end
    
end
