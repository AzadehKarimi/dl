% 
%   this class calculates the standpipe pressure
%

classdef EXPECTED_STANDPIPE_PRES_OBJ < DTECTLOSS_SUBMODULE
    
	properties(SetAccess = private)
        presSPExpected_bar;             % The one to be used in voting algorithm
        presSPExpWithoutInjection_bar;  % The one calculated from pwlf        
        pressSPInjection_bar;           % Small additional term
        presSPErr_bar;
        isCalibratingExpPresSP;
        isCalibratedExpPresSP;
        isSteadyPresSP;
        params;
    end
    
    properties (SetAccess = private, GetAccess = private)       
        timePrev_d;
        
        presSPBufferObj;
        presSPExpectedBufferObj;
        flowInBufferObj;
        filtFlow;
        filtPres;
        filtPresErr;
        flowInDer_lpmpsec;
        presSPDer_barpsec;
        flowMaxMin_lpm;
        presMaxMin_bar;
        Pwlfobj;
        stateMachine;    % 0=no flow, 1=ramp up, 2=steady non-zero flow, 3=ramp down 
        isSteadyFlowIn;
        timesincePumpStart_s;
        flowInPrevious_lpm;
        presSPPrevious_bar;
        doDebugPlots;
        doDebugText;        
    end
    
    properties (Constant)
        BUFFER_SIZE_SAMPLES                              = 200;
        flowMaxMinThreshold_lpm                          = 100;
        presMaxMinThreshold_bar                          = 10;
        NvaluesforSteady                                 = 5;
        timeDelayMax_s                                   = 20;
        flowInMinLimit_lpm                                 = 0;
        flowInMaxLimit_lpm                                 = 10000;
        errVal                                           = -0.99925;
    end
    
    methods
        function obj = EXPECTED_STANDPIPE_PRES_OBJ()
            
        end
        
        function obj = initalize(obj, params)            
            obj.isCalibratingExpPresSP          = false;
            obj.presSPExpected_bar              = 0;  
            obj.presSPExpWithoutInjection_bar   = 0;
            obj.pressSPInjection_bar            = 0;
            obj.presSPErr_bar                   = 0;
            obj.params                          = params;
            
            obj.timePrev_d                      = 0;
            
            obj.flowInPrevious_lpm              = 0;
            obj.presSPPrevious_bar              = 0;
            obj.presSPBufferObj                 = RINGBUFFER_OBJ(obj.BUFFER_SIZE_SAMPLES);
            obj.presSPExpectedBufferObj         = RINGBUFFER_OBJ(obj.BUFFER_SIZE_SAMPLES);
            obj.flowInBufferObj                 = RINGBUFFER_OBJ(obj.BUFFER_SIZE_SAMPLES);
            obj.filtFlow                        = FILTERDERIVATIVE_OBJ(20);
            obj.filtPres                        = FILTERDERIVATIVE_OBJ(20);
            obj.filtPresErr                     = LOWPASS_OBJ(1,60);
            obj.flowInDer_lpmpsec               = 0;
            obj.presSPDer_barpsec               = 0;
            obj.flowMaxMin_lpm                  = 0;
            obj.presMaxMin_bar                  = 0;
            
            obj.doDebugPlots                    = false;
            obj.doDebugText                     = false;
            obj.isCalibratedExpPresSP           = false;
            obj.isCalibratingExpPresSP          = false;            
          
            obj.stateMachine                    = 0;
            obj.isSteadyPresSP                  = false;
            obj.isSteadyFlowIn                  = false;
            obj.Pwlfobj                         = PIECEWISELINFIT_OBJ(6000, 0, 60, 50);%6000, 0, 12, 50);%20, 100
            obj.flowMaxMin_lpm                  = 0;
            obj.presMaxMin_bar                  = 0;
            obj.timesincePumpStart_s            = 0;
        end
        
        function obj = softReset(obj)
            obj.isCalibratingExpPresSP          = false;
            obj.presSPExpected_bar              = 0;  
            obj.presSPExpWithoutInjection_bar   = 0;
            obj.pressSPInjection_bar            = 0;          
            obj.presSPErr_bar                   = 0;
            obj.timePrev_d                      = 0;            
            obj.flowInPrevious_lpm              = 0;
            obj.presSPPrevious_bar              = 0;
            obj.flowInDer_lpmpsec               = 0;
            obj.presSPDer_barpsec               = 0;
            obj.flowMaxMin_lpm                  = 0;
            obj.presMaxMin_bar                  = 0;
            
            obj.doDebugPlots                    = false;
            obj.doDebugText                     = false;
            obj.isCalibratedExpPresSP           = false;
            obj.isCalibratingExpPresSP          = false;            
          
            obj.stateMachine                    = 0;
            obj.isSteadyPresSP                  = false;
            obj.isSteadyFlowIn                  = false;
            obj.flowMaxMin_lpm                  = 0;
            obj.presMaxMin_bar                  = 0;
            obj.timesincePumpStart_s            = 0;
        end
 
		function [obj,presSPexpOutput] = step(obj, flowIn_lpm, presSP_bar, isDownlinking, alarmState, timeNew_d)
            %Assuming pre-filtered flow-in
            if flowIn_lpm < obj.flowInMinLimit_lpm || flowIn_lpm > obj.flowInMaxLimit_lpm
                presSPexpOutput = obj.generateOutput(presSP_bar);
                return;
            end                       
            
            dt_s                                        = (timeNew_d - obj.timePrev_d)*86400;
            obj.timePrev_d                              = timeNew_d;

            if not(isDownlinking) && flowIn_lpm < 10000 && presSP_bar < 1000%true,
                obj.flowInBufferObj                     = obj.flowInBufferObj.addDataToCircularBuffer(timeNew_d,flowIn_lpm);
                obj.presSPBufferObj                     = obj.presSPBufferObj.addDataToCircularBuffer(timeNew_d,presSP_bar);
            end
            
            flowVec            = obj.flowInBufferObj.getLastNValues(obj.NvaluesforSteady);
            presVec            = obj.presSPBufferObj.getLastNValues(obj.NvaluesforSteady);
            obj.flowMaxMin_lpm = max(flowVec) - min(flowVec);
            obj.presMaxMin_bar = max(presVec) - min(presVec);
            obj.isSteadyFlowIn = obj.flowMaxMin_lpm < obj.flowMaxMinThreshold_lpm;
            obj.isSteadyPresSP = obj.presMaxMin_bar < obj.presMaxMinThreshold_bar;
            
            [obj.filtFlow, obj.flowInDer_lpmpsec] = obj.filtFlow.smoothderivative2ndorder(flowIn_lpm, timeNew_d);
            [obj.filtPres, obj.presSPDer_barpsec] = obj.filtPres.smoothderivative2ndorder(presSP_bar, timeNew_d);
            
            flowZerolim_lpm = 100;
            if obj.stateMachine == 0 %Zero flow
                if obj.isSteadyFlowIn  %Steady
                    if flowIn_lpm > flowZerolim_lpm
                        obj.stateMachine = 2;
                    else
                        obj.stateMachine = 0;
                    end
                else
                    if flowIn_lpm > flowZerolim_lpm
                        obj.stateMachine = 1;
                    else
                        obj.stateMachine = 0;
                    end
                end                    
            elseif obj.stateMachine == 1 %Ramp up
                if obj.isSteadyFlowIn  %Steady
                    if flowIn_lpm > flowZerolim_lpm
                        obj.stateMachine = 2;%Steady non-zero flow
                        obj.timesincePumpStart_s = 0;
                    else
                        obj.stateMachine = 0;%Pumps off
                    end
                elseif obj.flowInDer_lpmpsec < 0
                    obj.stateMachine = 3;%Ramping down
                else
                    obj.stateMachine = 1;%Still ramping up
                end
            elseif obj.stateMachine == 2 %Steady flow
                if obj.isSteadyFlowIn %Steady
                    if flowIn_lpm > flowZerolim_lpm
                        obj.stateMachine = 2;%Steady non-zero flow
                        obj.timesincePumpStart_s = obj.timesincePumpStart_s + dt_s;
                    else
                        obj.stateMachine = 0;%Pumps off
                    end
                elseif obj.flowInDer_lpmpsec < 0
                    obj.stateMachine = 3;
                else 
                    obj.stateMachine = 1;
                end
            elseif obj.stateMachine == 3 %Ramp down
                if obj.isSteadyFlowIn 
                    if flowIn_lpm > flowZerolim_lpm
                        obj.stateMachine = 2;
                    else
                        obj.stateMachine = 0;
                    end
                elseif obj.flowInDer_lpmpsec < 0
                    obj.stateMachine = 3;%Ramping down
                else
                    obj.stateMachine = 1;%Still ramping up
                end
            end                   
                    
            [obj.Pwlfobj, obj.presSPExpWithoutInjection_bar]   = obj.Pwlfobj.interpol(flowIn_lpm);
            if obj.presSPExpWithoutInjection_bar == obj.errVal
                obj.presSPExpWithoutInjection_bar = presSP_bar;
                obj.isCalibratingExpPresSP        = true;
            else
                if not(isDownlinking) && (obj.stateMachine == 0 || obj.stateMachine == 2) && obj.isSteadyPresSP && (alarmState < 2)
                    preserr_bar                = obj.presSPExpWithoutInjection_bar + obj.pressSPInjection_bar - presSP_bar;
                    obj.filtPresErr            = obj.filtPresErr.filterForCustomDt(preserr_bar,dt_s,0);
                    obj.presSPErr_bar          = obj.filtPresErr.FilteredSignal;
                    timeConstant_s             = 120;
                    dP0dtmax_barps             = 1/60; %~1 bar/min 
                    p0injMax_bar               = 20;%10
                    if dt_s < 10
                        dP0dt_barps         = preserr_bar/timeConstant_s;
                        dP0dt_barps         = sign(dP0dt_barps)*min(dP0dtmax_barps, abs(dP0dt_barps)); 
                        obj.pressSPInjection_bar = max(-p0injMax_bar, min(p0injMax_bar, obj.pressSPInjection_bar - dt_s*dP0dt_barps));                     
                    end
                    obj.isCalibratingExpPresSP = (abs(obj.presSPErr_bar) >= obj.params.presDeadbandCalibrate_bar);
                else
                    obj.isCalibratingExpPresSP = false;
                    obj.presSPErr_bar          = 0;
                end
            end
            
            if obj.isCalibratingExpPresSP
                obj.Pwlfobj                = obj.Pwlfobj.train(flowIn_lpm, presSP_bar);
            end
            
            if obj.presSPExpWithoutInjection_bar > 0
                obj.presSPExpected_bar   = max(0, obj.presSPExpWithoutInjection_bar + obj.pressSPInjection_bar);
            else
                obj.presSPExpected_bar   = presSP_bar;
            end
            if obj.isCalibratedExpPresSP || obj.Pwlfobj.isTrained
                obj.isCalibratedExpPresSP  = true;
            else
                obj.isCalibratedExpPresSP  = false;
            end%if          
           
            if not(isDownlinking) && flowIn_lpm < obj.flowInMaxLimit_lpm && presSP_bar < 1000%true,
                obj.presSPExpectedBufferObj                 = obj.presSPExpectedBufferObj.addDataToCircularBuffer(timeNew_d,obj.presSPExpected_bar);
            end           

            presSPexpOutput = obj.generateOutput(presSP_bar);
        end
        
        function presSPexpOutput = generateOutput(obj, presSP_bar)
            presSPexpOutput.presSPExpected_bar              = obj.presSPExpected_bar; 
            presSPexpOutput.presSPExpWithoutInjection_bar   = obj.presSPExpWithoutInjection_bar; 
            presSPexpOutput.pressSPInjection_bar            = obj.pressSPInjection_bar;
            presSPexpOutput.presSPErr_bar                   = obj.presSPErr_bar;            
            presSPexpOutput.isCalibratedExpPresSP           = obj.isCalibratedExpPresSP;
            presSPexpOutput.isCalibratingExpPresSP          = obj.isCalibratingExpPresSP;
            presSPexpOutput.isSteadyPresSP                  = obj.isSteadyPresSP; 
            presSPexpOutput.isSteadyandNonZeroPresSP        = obj.isSteadyPresSP && (presSP_bar > 20);        
            presSPexpOutput.isSteadyFlowIn                  = obj.isSteadyFlowIn; 
            presSPexpOutput.stateMachine                    = obj.stateMachine; 
            presSPexpOutput.presSPDer_barpsec               = obj.presSPDer_barpsec;
        end
    end    
end