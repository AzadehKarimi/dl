classdef ANALYZE_OBJ
    %   ANALYZE_OBJ Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        t_s           ; % current values
        bitDepth_m    ;
        q_out_lpm     ;
        q_p_lpm       ;
        q_out_pc      ;
        V_pit_m3      ;
        gldrill_m3    ;
        V_tt_m3       ;
        p_p_bar       ;
        holeDepth_m   ;
        blockPos_m    ;
        bitVel_mph    ;
        F_hl_ton      ;
        F_wob_ton     ;
        rotation_rpm  ;
        ECD_sg        ;
        sys_states    ;
        buffer        ; % hold time series
        fig1          ;
        fig2          ;
        fig3          ;
    
        lengthTimeSeries    ;            
        max_dt_s            ;
        avg_dt_s            ;
        NaN_pc              ;
        bitDepth_min_m      ;
        bitDepth_max_m      ;
        q_out_max_lpm       ;
        q_out_max_pc        ;
        q_p_max_lpm         ;
        V_pit_min_m3        ;
        V_pit_max_m3        ;
        gldrill_min_m3      ;
        gldrill_max_m3      ;
        V_tt_min_m3         ;
        V_tt_max_m3         ;
        p_p_max_bar         ;
        holeDepth_min_m     ;
        holeDepth_max_m     ;
        bitVel_max_mph      ;
        rotation_max_rpm    ;
        seriesOK            ;

        flowDeadband
        volumeThreshold
        noOfAlarms
    end
    
    methods
        function obj=ANALYZE_OBJ(qdb,vdb)
            obj.flowDeadband=qdb;
            obj.volumeThreshold=vdb;
            obj.noOfAlarms=0;
        end
        
        function obj=analyze(obj,qIo,vIo)
            for ii=1:numel(qIo),
                if vIo>obj.volumeThreshold,
                   obj.noOfAlarms=obj.noOfAlarms+1;
                end
            end
        end
        
        function obj =  initialize(obj, data)%Constructor
            [numDataPoints,numVar]   = size(data);

            obj.buffer.t_s           = nan(1, numDataPoints);
            obj.buffer.p_p_bar       = nan(1, numDataPoints);
            obj.buffer.bitDepth_m    = nan(1, numDataPoints);
            obj.buffer.q_out_lpm     = nan(1, numDataPoints);
            obj.buffer.q_p_lpm       = nan(1, numDataPoints);
            obj.buffer.q_out_pc      = nan(1, numDataPoints);
            obj.buffer.V_pit_m3      = nan(1, numDataPoints);
            obj.buffer.gldrill_m3    = nan(1, numDataPoints);
            obj.buffer.V_tt_m3       = nan(1, numDataPoints);
            obj.buffer.p_p_bar       = nan(1, numDataPoints);
            obj.buffer.holeDepth_m   = nan(1, numDataPoints);
            obj.buffer.blockPos_m    = nan(1, numDataPoints);
            obj.buffer.bitVel_mph    = nan(1, numDataPoints);
            obj.buffer.F_hl_ton      = nan(1, numDataPoints);
            obj.buffer.F_wob_ton     = nan(1, numDataPoints);
            obj.buffer.rotation_rpm  = nan(1, numDataPoints);
            obj.buffer.ECD_sg        = nan(1, numDataPoints);
            obj.buffer.sys_states    = nan(1, numDataPoints);
            obj.buffer.dbdV_e_barm3  = nan(1, numDataPoints);
            obj.buffer.qIo_pdiff_lpm = nan(1, numDataPoints);

            obj.buffer.t_s          = data(:,1)*24*3600;
            obj.buffer.bitDepth_m   = data(:,2);
            obj.buffer.q_out_lpm    = data(:,3);
            obj.buffer.q_p_lpm      = data(:,4);            
            obj.buffer.q_out_pc     = data(:,5);
            obj.buffer.V_pit_m3     = data(:,6);
            obj.buffer.gldrill_m3   = data(:,7);
            obj.buffer.V_tt_m3      = data(:,8);
            obj.buffer.p_p_bar      = data(:,9);
            obj.buffer.holeDepth_m  = data(:,10);
            obj.buffer.blockPos_m   = data(:,11);
            obj.buffer.bitVel_mph   = data(:,12);
            obj.buffer.F_hl_ton     = data(:,13);
            obj.buffer.F_wob_ton    = data(:,14);
            obj.buffer.rotation_rpm = data(:,15);
            obj.buffer.ECD_sg       = data(:,16);
        end

        function obj =  DispSeriesAnalysis(obj)
            if obj.seriesOK,
                disp(['Time series OK']);
            else
                disp(['Bad time series']);
            end
            disp(['Length time series = ' num2str(obj.lengthTimeSeries/60,'%.0f') ' min']); 
            disp(['Time step:     max = ' num2str(obj.max_dt_s,'%.2f')         ' sec, average = ' num2str(obj.avg_dt_s,'%.2f') ' sec']); 
            disp(['NaN time instants  = ' num2str(obj.NaN_pc,'%.2f')           ' %']); 
            disp(['Bit depth:     min = ' num2str(obj.bitDepth_min_m,'%.1f')   ' m,  max = ' num2str(obj.bitDepth_max_m, '%.0f') ' m']); 
            disp(['Hole depth:    min = ' num2str(obj.holeDepth_min_m,'%.1f')  ' m,  max = ' num2str(obj.holeDepth_max_m,'%.0f') ' m']); 
            disp(['Flow in:       max = ' num2str(obj.q_p_max_lpm,  '%.1f')    ' lpm']); 
            disp(['Flow out:      max = ' num2str(obj.q_out_max_lpm,'%.1f')    ' lpm']); 
            disp(['Flow out:      max = ' num2str(obj.q_out_max_pc, '%.1f')    ' %']); 
            disp(['Pit volume:    min = ' num2str(obj.V_pit_min_m3,'%.1f')     ' m3, max = ' num2str(obj.V_pit_max_m3,  '%.0f')  ' m3']); 
            disp(['GL volume:     min = ' num2str(obj.gldrill_min_m3,'%.1f')   ' m3, max = ' num2str(obj.gldrill_max_m3,'%.0f')  ' m3']); 
            disp(['TT volume:     min = ' num2str(obj.V_tt_min_m3,   '%.1f')   ' m3, max = ' num2str(obj.V_tt_max_m3,   '%.0f')  ' m3']); 
            disp(['Pump pressure: max = ' num2str(obj.p_p_max_bar,     '%.1f') ' bar']); 
            disp(['Bit velocity:  max = ' num2str(obj.bitVel_max_mph,  '%.1f') ' m/hr']); 
            disp(['Rotation:      max = ' num2str(obj.rotation_max_rpm,'%.1f') ' rpm']); 
        end
        
        function obj =  plotSeriesAnalysis(obj)
            tm=obj.buffer.t_s/60;
            tm=tm-tm(1);
            figure(1)
            ax(1)=subplot(521);
            plot(tm(1:length(diff(obj.buffer.t_s))),diff(obj.buffer.t_s),'LineWidth',2)
            ylabel('Time resolution [s]')
            legend('DT')
            grid
            ax(2)=subplot(522);
            plot(tm,obj.buffer.bitDepth_m,tm,obj.buffer.holeDepth_m,'LineWidth',2)
            grid
            ylabel('Depth [m]')
            legend('Bit depth','Hole depth')
            xlabel('Time [min]')
            ax(3)=subplot(523);
            scalefactor=max(obj.buffer.q_out_lpm)/max(obj.buffer.q_out_pc,1);
            plot(tm,obj.buffer.q_p_lpm,tm,obj.buffer.q_out_lpm,tm,scalefactor*obj.buffer.q_out_pc,'LineWidth',2)
            grid
            title(['Scale factor flow out pc = ' num2str(scalefactor,'%.2f') 'lpm/%'])
            legend('Pump flow','Flow out lpm','Flow out pc scaled')
            ylabel('Flow [lpm]')
            ax(4)=subplot(524);
            plot(tm,obj.buffer.V_pit_m3,tm,obj.buffer.gldrill_m3,tm,obj.buffer.V_tt_m3,'LineWidth',2)
            grid
            legend('Vpit','Gain/Loss','Vtrip')
            ylabel('Volume [m3]')
            ax(5)=subplot(525);
            plot(tm,obj.buffer.p_p_bar,tm,obj.buffer.rotation_rpm,'LineWidth',2)
            grid
            legend('Pump pressure','Rotation')
            ylabel('Pressure [bar]/Rotation [rpm]')
            ax(6)=subplot(526);
            plot(tm,obj.buffer.blockPos_m,'LineWidth',2)
            grid
            ylabel('Block pos [m]')
            legend('Block pos')
            ax(6)=subplot(526);
            plot(tm,obj.buffer.bitVel_mph,'LineWidth',2)
            grid
            ylabel('Bit vel [m/hr]')
            legend('Bit vel/ROP')
            ax(6)=subplot(526);
            plot(tm,obj.buffer.F_hl_ton,'LineWidth',2)
            ylabel('Load [ton]')
            legend('Hookload')
            grid
            ax(7)=subplot(527);
            plot(tm,obj.buffer.F_wob_ton,'LineWidth',2)
            ylabel('Load [ton]')
            legend('WOB')
            grid          
            xlabel('Time [min]')
            ax(8)=subplot(528);
            plot(tm,obj.buffer.ECD_sg,'LineWidth',2)
            ylabel('ECD [sg]')
            legend('ECD')
            grid          
            xlabel('Time [min]')
            linkaxes(ax,'x')
        end

        function obj =  PlotResAnalysis(obj)
            tm=obj.buffer.t_s/60;
            tm=tm-tm(1);

%             figure(2)
%             ax(15)=subplot(221);
%             plot(tm,sim.buffer.drillState,'LineWidth',2)
%             ylabel('Drillstate')
%             legend('State')
%             text(10,10,'start=0;drill=1;offbot=2;stoprot=3;stopp=4;wait=5')
%             text(10, 8,'startp=6;startrot=7;goonbot=8;pull=9;fcheck=10,pull=11,end=12')
%             grid
%             ax(16)=subplot(222);
%             plot(tm,sim.buffer.onBottom_b,tm,sim.buffer.autoDrill+0.1,tm,sim.buffer.seqNumber-0.1,tm,sim.buffer.sys_states,'LineWidth',2)
%             ylabel('State')
%             text(10,14,'BadData(-1) Other(0) Drill(1) FCheck(2) Cond(3) Trip(4)')            
%             legend('Onbottom=1, Offbottom=0','Autodrill: 0=man, 1=wob, 2=pos','Sequence','State machine')
%             grid
%             ax(16)=subplot(223);
%             plot(tm,sim.buffer.dbdV_barm3,tm,sim.buffer.dbdV_e_barm3,'LineWidth',2)
%             ylabel('beta/Vol [bar/m3]')
%             legend('True beta/Vol', 'Est beta/Vol')
%             grid
%             ax(16)=subplot(224);
%             plot(tm,sim.buffer.qIo_nodiff_lpm,tm,sim.buffer.dpsp_dt_bar,'LineWidth',2)
%             ylabel('[lpm] and [bar/sec]')
%             legend('qIo_nodiff','dpsp_dt_bar')
%             grid
%             linkaxes(ax,'x')
        end
        
        function obj = writerescsvfile(obj,filename)%save res to csv file
            wtime=datetime-1+(sim.buffer.t_s)/24/60/60;
            fHeader1='TIME;SYSSTATE;dbdV_e_barm3;qIo_pdiff_lpm;VIo_pdiff_m3;alarm_pdiff;qIo_nodiff_lpm;VIo_nodiff_m3;alarm_nodiff;warning';
            fHeader2='sec;unitless;bar/m3;lpm;m3;lpm;m3;unitless;unitless';

            data=[...
                sim.buffer.t_s;...
                sim.buffer.sys_states;...
                sim.buffer.dbdV_e_barm3;...
                sim.buffer.qIo_pdiff_lpm;...
                sim.buffer.VIo_pdiff_m3;...
                sim.buffer.alarm_pdiff;...
                sim.buffer.qIo_nodiff_lpm;...
                sim.buffer.VIo_nodiff_m3;...
                sim.buffer.alarm_nodiff;...
                ];
             Ncol=9;
             Nrow=length(sim.buffer.t_s)-1;
             % open a file for writing
             fid = fopen(filename, 'w');
             fprintf(fid, '%s\n\n',fHeader1);
             fprintf(fid, '%s\n\n',fHeader2);
             tvec=datevec(sim.buffer.t_s);

             for i=1:L:Nrow,%time
                 for j=1:Ncol-1,%var
                     fprintf(fid, '%4.0f-%02.0f-%02.0fT%02.0f:%02.0f:%02.0fZ;%.2f;', tvec(i,1), tvec(i,2),tvec(i,3),tvec(i,4),tvec(i,5),tvec(i,6),data(j,i));
                 end
                 j=Ncol;
                 fprintf(fid, '%4.0f-%02.0f-%02.0fT%02.0f:%02.0f:%02.0fZ;%.2f', tvec(i,1), tvec(i,2),tvec(i,3),tvec(i,4),tvec(i,5),tvec(i,6),data(j,i));
                 fprintf(fid, '\n');
             end
             fclose(fid);
        end

    end  
end

