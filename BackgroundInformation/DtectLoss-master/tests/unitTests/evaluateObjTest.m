classdef evaluateObjTest < matlab.unittest.TestCase
    %EVALUATEOBJTEST unit tests for the evaluate object.
    
    properties
        trueData;
        falseData;
        simData;
        trueNoOfAlarms;
        trueNoOfFalseAlarms;
        trueNoOfFalseWarnings;
        trueNoOfWarnings;
        algName;
        rigName;
        eva; % evaluate object under test
        trueIgnore;
        trueVolIO;
        algAlarm;
        flowIO;
        volIO;
    end
    
    methods (TestClassSetup)
        function initClass(testCase)
            % load sd (simualation data) and td (true flow rate and
            % volume from reservoir)
            load(fullfile('helperFiles', 'simDataForEvaluateObjTest.mat'));
            testCase.simData=sd;
            testCase.trueData=td;
            testCase.falseData=fd; % alg called falg with 1 false alarm
            testCase.trueNoOfAlarms=2;
            testCase.trueNoOfFalseAlarms=0;
            testCase.trueNoOfFalseWarnings=1;
            testCase.trueNoOfWarnings=2;
            testCase.algName='ukf';
            testCase.rigName='simRig';
            
            secPerDay=86400;
            
            trueM=[...
                0  0 0.0 0;
                9  0 0.0 0;
                10 0 0.8 1;
                12 0 1.0 2;
                17 0 2.0 2;
                50 0 0.0 0];
            
            algM=[...
                0    0   0 0;
                11 500 0.5 0;
                12 500 0.8 1;
                13 500 1.0 2;
                14   0 0.0 0;
                15 500 0.6 0;
                16 500 0.8 1;
                17 500 1.0 2];
            
            
            testCase.trueIgnore=struct('t', trueM(:,1)/secPerDay, 'V', trueM(:,2));
            testCase.trueVolIO=struct('t', trueM(:,1)/secPerDay, 'V', trueM(:,3));
            testCase.algAlarm=struct('T', algM(:,1)/secPerDay, 'V', algM(:,4));
            testCase.flowIO=struct('T', algM(:,1)/secPerDay, 'V', algM(:,2));
            testCase.volIO=struct('T', algM(:,1)/secPerDay, 'V', algM(:,3));
            
        end
    end
    
    methods (TestMethodSetup)
        function initMethod(testCase)
            testCase.eva=EVALUATE_OBJ(testCase.algName, testCase.rigName);
            testCase.eva=testCase.eva.initialize();
            testCase.addTeardown(@clear, 'testCase.eva');
        end
    end
    
    methods (Test)
        function initialize_correctInit_correctOutput(testCase)
            testCase.verifyEqual(testCase.eva.trueNoOfAlarms, 0, 'Verify that n of alarms is zero');
            testCase.verifyEqual(testCase.eva.trueNoOfWarnings, 0, 'Verify that n of warnings is zero');
            testCase.verifyEqual(testCase.eva.algNoOfCorrectAlarms, 0, 'Verify that n of alarms is zero');
            testCase.verifyEqual(testCase.eva.algNoOfWarnings, 0, 'Verify that n of warnings is zero');
            testCase.verifyEqual(testCase.eva.algNoOfFalseAlarms, 0, 'Verify that n of false alarms is zero');
            testCase.verifyEqual(testCase.eva.FAR, 0, 'Verify that FAR is zero');
            testCase.verifyEqual(testCase.eva.PD, NaN, 'Verify that PD is NaN');
            testCase.verifyEqual(testCase.eva.kpi, NaN, 'Verify that kpi is NaN');
            testCase.verifyEqual(testCase.eva.TD, NaN, 'Verify that TD is NaN');
            testCase.verifyEqual(testCase.eva.VD, NaN, 'Verify that VD is NaN');
            testCase.verifyEqual(testCase.eva.algUpTime, NaN, 'Verify that algorithm up-time is NaN');
            testCase.verifyEqual(testCase.eva.algUpTimeInHours, NaN, 'Verify that algorithm up-time in hours is NaN');
        end
        
        function countAlgAlarms_simData_correctNoOfAlarmsAndWarnings(testCase)
            [actNAlarms, actNWarnings]=testCase.eva.countAlgAlarms(testCase.simData.alarmState_ukf);
            testCase.verifyEqual(actNAlarms, testCase.trueNoOfAlarms, 'Check on number of alarms in true data set');
            testCase.verifyEqual(actNWarnings, testCase.trueNoOfWarnings, 'Check on number of warnings in true data set');            
        end
        
        function countTrueAlarms_trueData_correctNoOfAlarmsAndWarnings(testCase)
            [actNAlarms, actNWarnings]=testCase.eva.countTrueAlarms(testCase.trueData.volIO);
            testCase.verifyEqual(actNAlarms, testCase.trueNoOfAlarms, 'Check on number of alarms in true data set');
            testCase.verifyEqual(actNWarnings, testCase.trueNoOfWarnings, 'Check on number of warnings in true data set');
        end
        
        function evaluate_noAlgAlarmType_returnEmpty(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            % suppress warning
            testCase.applyFixture(SuppressedWarningsFixture('EVALUATE_OBJ:differentStartTimeInTrueFileAndFieldData'));
            
            [testCase.eva, nAlarms, alarmTypes]=testCase.eva.evaluate(testCase.trueData.trueIgnore, testCase.trueData.volIO,...
                testCase.falseData.flowIO_lpm_falg, testCase.falseData.volIO_m3_falg,...
                testCase.falseData.alarmState_falg);
            
            testCase.verifyEmpty(nAlarms);
            testCase.verifyEmpty(alarmTypes);
        end
        
        function evaluate_trueDataComparison_correctResult(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            % suppress warning
            testCase.applyFixture(SuppressedWarningsFixture('EVALUATE_OBJ:differentStartTimeInTrueFileAndFieldData'));
            
            testCase.eva=testCase.eva.evaluate(testCase.trueData.trueIgnore, testCase.trueData.volIO,...
                testCase.falseData.flowIO_lpm_falg, testCase.falseData.volIO_m3_falg,...
                testCase.falseData.alarmState_falg);
            testCase.verifyEqual(testCase.eva.algNoOfCorrectAlarms, 2, 'Check on number of alarms produced by algorithm');
            testCase.verifyEqual(testCase.eva.algNoOfWarnings, testCase.trueNoOfWarnings+testCase.trueNoOfFalseWarnings,...
                'Check on number of warnings produced by algorithm');
            testCase.verifyEqual(testCase.eva.algNoOfFalseAlarms, testCase.trueNoOfFalseAlarms, 'Check on number of false alarms produced by algorithm');
        end
        
        function evaluate_trueDataComparison_correctResultForSimpleAlg(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            % suppress warning
            testCase.applyFixture(SuppressedWarningsFixture('EVALUATE_OBJ:differentStartTimeInTrueFileAndFieldData'));
            
            testCase.eva=testCase.eva.evaluate(testCase.trueData.trueIgnore, testCase.trueData.volIO,...
                testCase.simData.flowIO_lpm_simple, testCase.simData.volIO_m3_simple,...
                testCase.simData.alarmState_simple);
            testCase.verifyEqual(testCase.eva.algNoOfCorrectAlarms, 2, 'Check on number of alarms produced by algorithm');
            testCase.verifyEqual(testCase.eva.algNoOfFalseAlarms, 0, 'Check on number of false alarms produced by algorithm');
        end
     
        function evaluate_trueDataComparison_correctAlgorithmUpTime(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            % suppress warning
            testCase.applyFixture(SuppressedWarningsFixture('EVALUATE_OBJ:differentStartTimeInTrueFileAndFieldData'));
            
            disabledAlarmState=testCase.simData.alarmState_ukf;
            disabledAlarmState.V(3500:3800)=AlarmState.Disabled;
            disabledDuration=disabledAlarmState.T(3800)-disabledAlarmState.T(3499);
            expAlgUpTime=100-disabledDuration/(disabledAlarmState.T(end)-disabledAlarmState.T(1))*100;
            expAlgUpTimeInHours=(disabledAlarmState.T(end)-disabledAlarmState.T(1)-disabledDuration)*24;
            
            testCase.eva=testCase.eva.evaluate(testCase.trueData.trueIgnore, testCase.trueData.volIO,...
                testCase.simData.flowIO_lpm_ukf, testCase.simData.volIO_m3_ukf,...
                disabledAlarmState);
            testCase.verifyEqual(testCase.eva.algUpTime, expAlgUpTime, 'Check that the algorithm up-time is calculated correctly');
            testCase.verifyEqual(testCase.eva.algUpTimeInHours, expAlgUpTimeInHours, 'Check that the algorithm up-time is calculated correctly');
        end
        
        function evaluate_zeroInputForTrueData_correctNoOfAlarms(testCase)
            testCase.eva=testCase.eva.evaluate(0, 0,...
                testCase.falseData.flowIO_lpm_falg, testCase.falseData.volIO_m3_falg,...
                testCase.falseData.alarmState_falg);
            testCase.verifyEqual(testCase.eva.algNoOfCorrectAlarms, 0, 'Check on number of correct alarms produced by algorithm');
            testCase.verifyEqual(testCase.eva.algNoOfFalseAlarms, 3, 'Check on number of false alarms produced by algorithm');
        end
        
        function evaluate_multipleTrueAlarms_countTrueAlarmAsOne(testCase)
            % in the case where multiple alarms are issued subsequent to an
            % actual alarm, the alarms should be counted only one time.
            
            testCase.eva=testCase.eva.evaluate(testCase.trueIgnore, testCase.trueVolIO,...
                testCase.flowIO, testCase.volIO, testCase.algAlarm);
            
            testCase.verifyEqual(testCase.eva.algNoOfCorrectAlarms, 1, 'Check on number of correct alarms produced by algorithm');
            testCase.verifyEqual(testCase.eva.algNoOfFalseAlarms, 0, 'Check on number of false alarms produced by algorithm');
            testCase.verifyEqual(testCase.eva.algLateAlarms, 1, 'Check on number of late alarms produced by algorithm');
            testCase.verifyEqual(testCase.eva.PD, 100, 'The PD should be 100 even though there are 2 correct alarms but only one actual');
        end
        
        function step_correctInput_correctOutput(testCase)
            % input data
            t1=[1 2 3 4 5 7 9 10]/86400;
            t2=[1 2 4 5 7 8 9 10]/86400;
            t3=[1 3 5 8 11]/86400;
            
            % expected output from function 
            expT=[1 1 1; 2 2 1; 3 2 3; 4 4 3; 5 5 5; 7 7 5; 7 8 8; 9 9 8; 10 10 8; 10 10 11]/86400;
            expIdx =[1 1 1; 2 2 1; 3 2 2; 4 3 2; 5 4 3; 6 5 3; 6 6 4; 7 7 4; 8 8 4; 8 8 5];
            
            % init
            idx=ones(1,3);
            prevT=[t1(1) t2(1) t3(1)];
            nT=[numel(t1) numel(t2) numel(t3)];
            t=[t1(idx(1)+1) t2(idx(2)+1) t3(idx(3)+1)];
                        
            for ii=2:size(expT,1)-1
                [newT, newIdx]=testCase.eva.step(t,prevT,idx);
                testCase.verifyEqual(newT, expT(ii,:), 'Check time');
                testCase.verifyEqual(newIdx, expIdx(ii,:), 'Check index');
                idx=newIdx;
                prevT=newT;
                if idx(1)<nT(1)
                    t(1)=t1(idx(1)+1);
                end
                if idx(2)<nT(2)
                    t(2)=t2(idx(2)+1);
                end
                if idx(3)<nT(3)
                    t(3)=t3(idx(3)+1);
                end
            end
        end
        
        function groupAlarms_noInputData_returnEmpty(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            % suppress warning
            testCase.applyFixture(SuppressedWarningsFixture('EVALUATE_OBJ:emptyIoEvent'));
            
            % give in an empty struct
            [nAlarms, alarmTypes]=EVALUATE_OBJ.groupAlarms(struct(), struct());
            
            testCase.verifyEmpty(nAlarms);
            testCase.verifyEmpty(alarmTypes);
        end
        
        function groupAlarms_correctInputData_returnCorNAlarms(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            % suppress warning
            testCase.applyFixture(SuppressedWarningsFixture('EVALUATE_OBJ:emptyIoEvent'));
            
            % give in an empty struct
            load('tests/testdata/evaTestData.mat');
            [nAlarms, alarmTypes]=EVALUATE_OBJ.groupAlarms(retCell.alarmType_voting, struct());
            
            expAlarmTypes=enumeration('AlarmType');
            
            testCase.verifyEqual(nAlarms, [0 0 0 1 0 0 1 3 3 0 1 1 0 0 1 0]');
            testCase.verifyEqual(alarmTypes, expAlarmTypes);
        end
        
        function groupAlarms_IoEventEmpty_dispWarning(testCase)
            % give in an empty struct
            load('tests/testdata/evaTestData.mat');
            
            ioEvent=struct('alarmTime', [],...
                    'startEvent', [], 'stopEvent', [],...
                    'detectionTime', [], 'volumeAtDetection', []);
                
            testCase.verifyWarning(@() EVALUATE_OBJ.groupAlarms(retCell.alarmType_voting, ioEvent),...
                'EVALUATE_OBJ:emptyIoEvent');
        end
        
        function groupAlarms_oneIoEvent_disregardEvent(testCase)
            % give in an empty struct
            load('tests/testdata/evaTestData.mat');
            
            ioEvent=struct('alarmTime', [],...
                'startEvent', datenum('01-Jan-2016 10:37:39'), 'stopEvent', datenum('01-Jan-2016 12:13:03'),...
                'detectionTime', [], 'volumeAtDetection', []);
            
            [nAlarms]=EVALUATE_OBJ.groupAlarms(retCell.alarmType_voting, ioEvent);
            
            testCase.verifyEqual(nAlarms, [0 0 0 1 0 0 1 2 1 0 1 0 0 0 1 0]');
        end
        
        function groupAlarms_twoIoEvents_disregardEvents(testCase)
            % give in an empty struct
            load('tests/testdata/evaTestData.mat');
            
            ioEvent=struct('alarmTime', [],...
                'startEvent', datenum('01-Jan-2016 09:39:29'), 'stopEvent', datenum('01-Jan-2016 09:51:49'),...
                'detectionTime', [], 'volumeAtDetection', []);
            
            ioEvent(2).startEvent=datenum('01-Jan-2016 10:37:39');
            ioEvent(2).stopEvent=datenum('01-Jan-2016 12:13:03');
            
            
            nAlarms=EVALUATE_OBJ.groupAlarms(retCell.alarmType_voting, ioEvent);
            
            testCase.verifyEqual(nAlarms, [0 0 0 0 0 0 0 1 1 0 1 0 0 0 1 0]');
        end
        
        function groupAlarms_countPitSlowGainAlarm_alarmCounted(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            % suppress warning
            testCase.applyFixture(SuppressedWarningsFixture('EVALUATE_OBJ:emptyIoEvent'));
            
            alarmType_voting = struct('T', [], 'V', []);
            
            alarmType_voting.T(1) = datenum(2018, 01, 04, 10, 55, 00);
            alarmType_voting.T(2) = datenum(2018, 01, 04, 10, 55, 01);
            
            alarmType_voting.V(1) = int32(AlarmType.Normal);
            alarmType_voting.V(2) = int32(AlarmType.PitSlowGainAlarm);
            
            nAlarms = EVALUATE_OBJ.groupAlarms(alarmType_voting, struct());
            
            testCase.verifyEqual(nAlarms, [0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 1]')
        end
        
        function evaluate_noTrueFile_countAlarmTypes(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            % suppress warning
            testCase.applyFixture(SuppressedWarningsFixture('EVALUATE_OBJ:emptyIoEvent'));
            
            load('tests/testdata/evaTestData.mat');
            
            [~, nAlarms, ~]=testCase.eva.evaluate(0, 0, retCell.flowIO_lpm_voting, retCell.volIO_m3_voting,...
                retCell.alarmState_voting, retCell.alarmType_voting);
            
            testCase.verifyNotEmpty(nAlarms);
        end
        
        function generateOutput_noOutput_isStruct(testCase)
            output = testCase.eva.generateOutput();
            testCase.verifyClass(output, 'struct')
        end
        
        function generateOutput_noOutput_correctFieldnames(testCase)
            expFieldNames ={...
                'trueNoOfAlarms';
                'trueNoOfWarnings';
                'algNoOfCorrectAlarms';
                'algNoOfWarnings';
                'algNoOfFalseAlarms';
                'algLateAlarms';
                'algName'; 
                'rigName';
                'falseAlarmRate';
                'probabilityOfDetection';
                'keyPerformanceIndex';
                'timeToDetection';
                'accumulatedVolumeAtDetection';
                'durationInHours';
                'algUpTime';
                'algUpTimeInHours'};
            
            M = size(expFieldNames, 1);
            hasFoundExpectedName = false(M, 1);            
        
            output = testCase.eva.generateOutput();
            fn = fieldnames(output);
            
            N = size(fn, 1);
            for ii = 1:N
                for jj = 1:M
                    if strcmp(fn{ii}, expFieldNames{jj})
                        hasFoundExpectedName(jj) = true;
                        continue
                    end
                end
            end
            testCase.verifyTrue(all(hasFoundExpectedName), 'A field has changed in the output struct.')
        end
        
        function generateOutput_afterEvaluate_correctOutput(testCase)
            testCase.eva=testCase.eva.evaluate(testCase.trueIgnore, testCase.trueVolIO,...
                testCase.flowIO, testCase.volIO, testCase.algAlarm);
            output = testCase.eva.generateOutput();
            
            testCase.verifyEqual(output.algNoOfCorrectAlarms, 1, 'Check on number of correct alarms produced by algorithm');
            testCase.verifyEqual(output.algNoOfFalseAlarms, 0, 'Check on number of false alarms produced by algorithm');
            testCase.verifyEqual(output.algLateAlarms, 1, 'Check on number of late alarms produced by algorithm');
            testCase.verifyEqual(output.probabilityOfDetection, 100, 'The PD should be 100 even though there are 2 correct alarms but only one actual');

        end
        
        function generateTrueAlarmStruct_ignoreEvent_correctOutput(testCase)
            [trueAlarmStructExp, trueIgnoreStruct, trueVolIOStruct] = testCase.mockValuesForIgnoreTests;
            trueAlarmStructAct = testCase.eva.generateTrueAlarmStruct(trueIgnoreStruct, trueVolIOStruct);
            
            testCase.verifyEqual(trueAlarmStructAct, trueAlarmStructExp)
        end
        
        function generateTrueAlarmStruct_emptyTrueIgnoreStruct_noError(testCase)
            [trueAlarmStructExp, ~, trueVolIOStruct] = testCase.mockValuesForIgnoreTests;
            trueAlarmStructExp(2) = [];
            trueAlarmStructExp(1).startEvent = 3;
            trueAlarmStructExp(1).stopEvent = 5;
            trueAlarmStructExp(1).alarmTime = 4;
            trueAlarmStructExp(1).ignore = false;
            trueAlarmStructAct = testCase.eva.generateTrueAlarmStruct(struct(), trueVolIOStruct);
            
            testCase.verifyEqual(trueAlarmStructAct, trueAlarmStructExp)
        end
        
        
%         function plotResult_nicePlot(testCase)
%             
%             testCase.eva.plotResult(testCase.trueData.trueIgnore, testCase.trueData.volIO,...
%                 testCase.simData.flowIO_lpm_simple, testCase.simData.volIO_m3_simple,...
%                 testCase.simData.alarmState_simple);
%         end
    end
    
    methods (Static)
        function [trueAlarmStructExp, trueIgnoreStruct, trueVolIOStruct] = mockValuesForIgnoreTests()
            
            dataPoints = [...
                0 0 0;
                1 0 0;
                2 1 0;
                3 1 2;
                4 1 2;
                5 1 0;
                6 1 0;
                7 0 0;
                8 0 0;
                9 0 0;
                10 1 0;
                11 1 0;
                12 0 0;
                13 0 0;
                14 0 2
                15 0 2;
                16 0 0;
                17 0 0];
            
            time = dataPoints(:, 1);
            ignoreVals = dataPoints(:, 2);
            volIOVals = dataPoints(:, 3);
            
            trueIgnoreStruct = struct('t', time, 'V', ignoreVals);
            trueVolIOStruct = struct('t', time, 'V', volIOVals);
            trueAlarmStructExp = struct('alarmTime', [],...
                'startEvent', [], 'stopEvent', [],...
                'ignore', false,...
                'detectionTime', [], 'volumeAtDetection', []);
            
            trueAlarmStructExp(1).startEvent = 2;
            trueAlarmStructExp(1).stopEvent = 7;
            trueAlarmStructExp(1).ignore = true;
            
            trueAlarmStructExp(2).startEvent = 10;
            trueAlarmStructExp(2).stopEvent = 12;
            trueAlarmStructExp(2).ignore = true;
            
            trueAlarmStructExp(3).startEvent = 14;
            trueAlarmStructExp(3).stopEvent = 16;
            trueAlarmStructExp(3).alarmTime = 15;
            trueAlarmStructExp(3).ignore = false;
            
        end
    end
    
end

