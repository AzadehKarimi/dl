classdef ringbufferObjTest < matlab.unittest.TestCase
    %ringbufferObjTest testing of the ringbuffer with methods
    
    properties
        nSamples; % number of samples when init buffer
        rbObj;
        errVal;
    end
    
    methods (TestClassSetup)
        function initFunc(testCase)
            testCase.nSamples = 10;
            testCase.errVal = -0.99925;
        end
    end
    
    methods (TestMethodSetup)
        function clearFixture(testCase)
            testCase.rbObj=RINGBUFFER_OBJ(testCase.nSamples);
            testCase.addTeardown(@clear, 'testCase.rbObj');
        end
    end
    
    methods (Test)
        function lookUp_timeDelayZero_returnLastValue(testCase)
            
            % fill up ringbuffer
            startTime=datenum(1981, 09, 26, 01, 00, 02);
            for ii=0:testCase.nSamples-1
                testCase.rbObj=testCase.rbObj.addDataToCircularBuffer(startTime+ii/86400, 2*ii);
            end
            
            [Vdelayed]=testCase.rbObj.lookUp(startTime+ii/86400);
            testCase.verifyEqual(Vdelayed, 2*ii, 'Value should be same as last put in buffer');
        end
        
        function lookUp_noValueInBuffer_returnErrval(testCase)
            
            
            [Vdelayed]=testCase.rbObj.lookUp(now);
            testCase.verifyEqual(Vdelayed, -0.99925, 'Value should be error value');
        end
        
        function lookUp_timeDelayGreaterThanAvailableValue_returnErrval(testCase)
            
            % fill up ringbuffer
            startTime=datenum(1981, 09, 26, 01, 00, 02);
            for ii=0:testCase.nSamples-1
                testCase.rbObj=testCase.rbObj.addDataToCircularBuffer(startTime+ii/86400, 2*ii);
            end
            
            [Vdelayed]=testCase.rbObj.lookUp(startTime-ii/86400);
            testCase.verifyEqual(Vdelayed, -0.99925, 'Value should be error value');
        end
        
        function lookUp_timeDelayMatchAPrevValExactly_retCorVal(testCase)
            
            % fill up ringbuffer
            startTime=datenum(1981, 09, 26, 01, 00, 02);
            for ii=0:testCase.nSamples-1
                testCase.rbObj=testCase.rbObj.addDataToCircularBuffer(startTime+ii/86400, 2*ii);
            end
            
            [Vdelayed]=testCase.rbObj.lookUp(startTime+4/86400);
            testCase.verifyEqual(Vdelayed, 8, 'Value should be 8.');
        end
        
        function lookUp_needToInterpolate_retCorVal(testCase)
            import matlab.unittest.constraints.IsEqualTo
            import matlab.unittest.constraints.AbsoluteTolerance
            numTol=2e-5; % absolute error which is tolerated
            
            
            % fill up ringbuffer
            startTime=datenum(1981, 09, 26, 01, 00, 02);
            for ii=0:testCase.nSamples-1
                testCase.rbObj=testCase.rbObj.addDataToCircularBuffer(startTime+ii/86400, 2*ii);
            end
            
            % look up value in between sampled values
            [Vdelayed]=testCase.rbObj.lookUp(startTime+4.5/86400);
            
            testCase.verifyThat(Vdelayed, IsEqualTo(9,...
                    'Within', AbsoluteTolerance(numTol)), 'Interpolated value should be 9.');
        end
        
        function lookUp_missingValueWhenInterpolating_retCorVal(testCase)
            import matlab.unittest.constraints.IsEqualTo
            import matlab.unittest.constraints.AbsoluteTolerance
            numTol=2e-5; % absolute error which is tolerated
            
            
            % fill up ringbuffer
            startTime=datenum(1981, 09, 26, 01, 00, 02);
            for ii=0:testCase.nSamples-1
                if mod(ii,2)==0
                    testCase.rbObj=testCase.rbObj.addDataToCircularBuffer(startTime+ii/86400, 2*ii);
                else % the following value will not be added to the ringbuffer.
                    testCase.rbObj=testCase.rbObj.addDataToCircularBuffer(startTime+ii/86400, -0.99925);
                end
            end
            
            [Vdelayed]=testCase.rbObj.lookUp(startTime+4.5/86400);
            testCase.verifyThat(Vdelayed, IsEqualTo(9,...
                    'Within', AbsoluteTolerance(numTol)), 'Interpolated value should be 9.');
        end
        
        function lookUp_onlyOneValueInBuffer_retErrVal(testCase)
            
            % fill up ringbuffer
            startTime=datenum(1981, 09, 26, 01, 00, 02);
            testCase.rbObj=testCase.rbObj.addDataToCircularBuffer(startTime+1/86400, 3);
            
            [Vdelayed]=testCase.rbObj.lookUp(startTime); % look up value not in buffer
            testCase.verifyEqual(Vdelayed, -0.99925, 'Return error value when value not found.');
        end
        
        function lookUp_lookUpTimeGreaterThanNewestTime_correctInterpolation(testCase)
            
            % fill up ringbuffer
            startTime=datenum(1981, 09, 26, 01, 00, 02);
            for ii=0:testCase.nSamples-1
                    testCase.rbObj=testCase.rbObj.addDataToCircularBuffer(startTime+ii/86400, 2*ii);
            end
            
            [Vdelayed]=testCase.rbObj.lookUp(startTime+(ii+1)/86400);
            testCase.verifyEqual(Vdelayed, 18);
        end
        
        function deringbuffer_missingValuesInBuffer_returnCorrectDtAvg_d(testCase)
            
            fraction = 2;
            [testCase, tStore] = testCase.fillFractionOfRingBuffer(fraction);
                                    
            [~, ~, dtAct] = testCase.rbObj.deringbuffer;
            dtExp = mean(diff(tStore));
                        
            testCase.verifyEqual(dtAct, dtExp);
        end
        
        function deringbufferAndResample_missingValuesInBuffer_keepMissingVals(testCase)
            import matlab.unittest.constraints.IsEqualTo
            import matlab.unittest.constraints.RelativeTolerance
            numTol=3e-5; % relative error which is tolerated
            
            fraction = 2;
            [testCase, tStore, vStore] = testCase.fillFractionOfRingBuffer(fraction);
            
            N = sum(tStore > 0);
            
            [tAct, vAct] = testCase.rbObj.deringbufferAndResample;
            tExp = [testCase.errVal*ones(1, testCase.nSamples - N) tStore];
            vExp = [testCase.errVal*ones(1, testCase.nSamples - N) vStore];
            
            testCase.verifyThat(tAct, IsEqualTo(tExp, 'Within', RelativeTolerance(numTol*1e-5)));
            testCase.verifyThat(vAct, IsEqualTo(vExp, 'Within', RelativeTolerance(numTol)));
        end
        
        function deringbufferAndResample_missingValuesInBuffer_padIfSecondArg(testCase)
            import matlab.unittest.constraints.IsEqualTo
            import matlab.unittest.constraints.RelativeTolerance
            numTol=1e-5; % relative error which is tolerated
            
            fraction = 2;
            [testCase, tStore, vStore] = testCase.fillFractionOfRingBuffer(fraction);
            
            N = sum(tStore > 0);
            
            tMissingStart = tStore(1) - 1/86400*N;
                        
            tPadded = [tMissingStart:1/86400:tStore(1) tStore(2:end)];
            
            doPadMissingValues = true;
            [tAct, vAct] = testCase.rbObj.deringbufferAndResample(doPadMissingValues);
            tExp = tPadded;
            vExp = [ones(1, testCase.nSamples - N) vStore];
            
            testCase.verifyThat(tAct, IsEqualTo(tExp, 'Within', RelativeTolerance(numTol*1e-5)));
            testCase.verifyThat(vAct, IsEqualTo(vExp, 'Within', RelativeTolerance(numTol)));
        end
        
        function deringbufferAndResample_singleValue_returnEmpty(testCase)
            rb = RINGBUFFER_OBJ(5);
            rb.addDataToCircularBuffer(2, now);
            
            doPadMissingValues = true;
            [Tbuffer_d_straight_resampled, Vbuffer_straight_resampled, dt_avg_d] = rb.deringbufferAndResample(doPadMissingValues);
            testCase.verifyEmpty(Tbuffer_d_straight_resampled)
            testCase.verifyEmpty(Vbuffer_straight_resampled)
            testCase.verifyEmpty(dt_avg_d)
        end
        
        function padMissingValues_useErrValForPadding_correctOutput(testCase)
            fraction = 2;
            [testCase, tStore] = testCase.fillFractionOfRingBuffer(fraction);
            
            N = sum(tStore > 0);
            
            [Tbuffer_d_straight, Vbuffer_straight, dt_avg_d, validDataPoints] = testCase.rbObj.deringbuffer();
            
            tValid_d = Tbuffer_d_straight(validDataPoints);
            vValid = Vbuffer_straight(validDataPoints);
            nInvalidDataPoints = sum(~validDataPoints);
            useErrValForPadding = true;
            
            [tAct, vAct] = testCase.rbObj.padMissingValues(tValid_d, vValid, dt_avg_d, nInvalidDataPoints, useErrValForPadding);
            
            tExp = testCase.errVal*ones(1, N);
            vExp = tExp;
            
            testCase.verifyEqual(tAct, tExp);
            testCase.verifyEqual(vAct, vExp);
        end
        
        function padMissingValues_doNotUseErrValForPadding_correctOutput(testCase)
            fraction = 2;
            [testCase, tStore] = testCase.fillFractionOfRingBuffer(fraction);
            
            N = sum(tStore > 0);
            
            [Tbuffer_d_straight, Vbuffer_straight, dt_avg_d, validDataPoints] = testCase.rbObj.deringbuffer();
            
            tValid_d = Tbuffer_d_straight(validDataPoints);
            vValid = Vbuffer_straight(validDataPoints);
            nInvalidDataPoints = sum(~validDataPoints);
            useErrValForPadding = false;
            
            [tAct, vAct] = testCase.rbObj.padMissingValues(tValid_d, vValid, dt_avg_d, nInvalidDataPoints, useErrValForPadding);
            
            tExp = zeros(1, testCase.nSamples - N);
            for jj = 1:testCase.nSamples - N
                tExp(jj) = datenum(1981, 09, 26, 01, 00, 44 + jj);
            end
            vExp = ones(1, N);
            
            testCase.verifyEqual(datestr(tAct), datestr(tExp));
            testCase.verifyEqual(vAct, vExp);
        end

		function deringbufferAndResample_noValidDataPoints_returnEmpty(testCase)
                        
            doPadMissingValues = true;
            [tAct, vAct] = testCase.rbObj.deringbufferAndResample(doPadMissingValues);
                        
            testCase.verifyEmpty(tAct);
            testCase.verifyEmpty(vAct);
        end
        
        function getWindowOfLastValues_timeNewerThanLastValInBuffer_returnEmpty(testCase)
            fraction = 1;
            testCase = testCase.fillFractionOfRingBuffer(fraction);
            
            tooOldTime_d = datenum(2017, 09, 26, 01, 00, 50);
            [time_d, val] = testCase.rbObj.getWindowOfLastValues(tooOldTime_d);
            testCase.verifyEmpty(time_d)
            testCase.verifyEmpty(val)
        end
        
        function getWindowOfLastValues_emptyBuffer_returnEmpty(testCase)
            desiredTime_d = datenum(1981, 08, 26, 01, 00, 50);
            [time_d, val] = testCase.rbObj.getWindowOfLastValues(desiredTime_d);
            testCase.verifyEmpty(time_d)
            testCase.verifyEmpty(val)
        end
        
        function getWindowOfLastValues_timeOlderThanBuffer_returnValidVals(testCase)
            fraction = 2;
            [testCase, tStore, vStore] = testCase.fillFractionOfRingBuffer(fraction);
            expTime = tStore(tStore > 0);
            expVal = vStore(tStore > 0);
            
            oldTime_d = datenum(1981, 08, 26, 01, 00, 50);
            [time_d, val] = testCase.rbObj.getWindowOfLastValues(oldTime_d);
            testCase.verifyEqual(time_d, expTime)
            testCase.verifyEqual(val, expVal)
        end
        
        function getWindowOfLastValues_timeInBuffer_returnValidVals(testCase)
            fraction = 1;
            [testCase, tStore, vStore] = testCase.fillFractionOfRingBuffer(fraction);
            askingTime_d = datenum(1981, 09, 26, 01, 00, 56);
            expTime = tStore(tStore > askingTime_d);
            expVal = vStore(tStore > askingTime_d);
            
            [time_d, val] = testCase.rbObj.getWindowOfLastValues(askingTime_d);
            testCase.verifyEqual(time_d, expTime)
            testCase.verifyEqual(val, expVal)
        end

        function varExtraFirstHalfWithFullBufferOnFirstIndex(testCase)
            fraction = 0.7;
            [testCase, ~, ~] = testCase.fillFractionOfRingBuffer(fraction);
            
            [varCalc, ~] = testCase.rbObj.varFirstHalf();
            testCase.verifyEqual(varCalc, 6.666666666666667)
        end    
        
        
        function varFirstHalfWithFullBufferOnFirstIndex(testCase)
            fraction = 1;
            [testCase, ~, ~] = testCase.fillFractionOfRingBuffer(fraction);
            
            [varCalc, ~] = testCase.rbObj.varFirstHalf();
            testCase.verifyEqual(varCalc, 10)

        end    
        
        
        function varFirstHalfWithHalfBufferOnHalfIndex(testCase)
            fraction = 2;
            [testCase, ~, ~] = testCase.fillFractionOfRingBuffer(fraction);
            
            [varCalc, ~] = testCase.rbObj.varFirstHalf();
            testCase.verifyEmpty(varCalc)
        end    

        
        function getLastNValues2Ok(testCase)
            fraction = 5;
            [testCase, ~, values] = testCase.fillFractionOfRingBuffer(fraction);
            result = testCase.rbObj.getLastNValues(2);
            testCase.verifyEqual(result(1), 0);            
            testCase.verifyEqual(result(2), values(1));
            testCase.verifyEqual(result(3), values(2));            
        end

        function getLastNValues2of9_Ok(testCase)
            fraction = 1.2;
            [testCase, ~, values] = testCase.fillFractionOfRingBuffer(fraction);
            result = testCase.rbObj.getLastNValues(2);
            testCase.verifyEqual(result(1), values(7));
            testCase.verifyEqual(result(2), values(8));
            testCase.verifyEqual(result(3), values(9));            
        end

        function getLastNValues2of10_Ok(testCase)
            fraction = 1;
            [testCase, ~, values] = testCase.fillFractionOfRingBuffer(fraction);
            result = testCase.rbObj.getLastNValues(2);
            testCase.verifyEqual(result(1), values(8));
            testCase.verifyEqual(result(2), values(9));
            testCase.verifyEqual(result(3), values(10));            
        end
        
        function getLastNValues5of10_Ok(testCase)
            fraction = 1;
            [testCase, ~, values] = testCase.fillFractionOfRingBuffer(fraction);
            result = testCase.rbObj.getLastNValues(5);
            testCase.verifyEqual(result(1), values(5));
            testCase.verifyEqual(result(2), values(6));
            testCase.verifyEqual(result(3), values(7));            
            testCase.verifyEqual(result(4), values(8));            
            testCase.verifyEqual(result(5), values(9));                        
            testCase.verifyEqual(result(6), values(10));                                    
        end

        function getLastNTimes2Ok(testCase)
            fraction = 5;
            [testCase, times, ~] = testCase.fillFractionOfRingBuffer(fraction);
            result = testCase.rbObj.getLastNTimes(2);
            testCase.verifyEqual(result(1), testCase.errVal);            
            testCase.verifyEqual(result(2), times(1));
            testCase.verifyEqual(result(3), times(2));            
        end
        
        function getLastNTimes2of9_Ok(testCase)
            fraction = 1.2;
            [testCase, times, ~] = testCase.fillFractionOfRingBuffer(fraction);
            result = testCase.rbObj.getLastNTimes(2);
            testCase.verifyEqual(result(1), times(7));
            testCase.verifyEqual(result(2), times(8));
            testCase.verifyEqual(result(3), times(9));            
        end

        function getLastNTimes2of10_Ok(testCase)
            fraction = 1;
            [testCase, times, ~] = testCase.fillFractionOfRingBuffer(fraction);
            result = testCase.rbObj.getLastNTimes(2);
            testCase.verifyEqual(result(1), times(8));
            testCase.verifyEqual(result(2), times(9));
            testCase.verifyEqual(result(3), times(10));            
        end
        
        function getLastNTimes5of10_Ok(testCase)
            fraction = 1;
            [testCase, times, ~] = testCase.fillFractionOfRingBuffer(fraction);
            result = testCase.rbObj.getLastNTimes(5);
            testCase.verifyEqual(result(1), times(5));
            testCase.verifyEqual(result(2), times(6));
            testCase.verifyEqual(result(3), times(7));            
            testCase.verifyEqual(result(4), times(8));            
            testCase.verifyEqual(result(5), times(9));                        
            testCase.verifyEqual(result(6), times(10));                                    
        end
        
        
    end
    
    methods
        function [testCase, tStore, vStore] = fillFractionOfRingBuffer(testCase, fraction)
            % fill up ringbuffer
            startTime = datenum(1981, 09, 26, 01, 00, 50);
            N = ceil(testCase.nSamples/fraction);
            tStore = zeros(1, N);
            vStore = zeros(1, N);
            dt_d = 1/86400;
            for ii=0:N - 1
                t = startTime+ii*dt_d;
                v = 2*ii + 1 ;
                tStore(ii+1) = t;
                vStore(ii+1) = v;
                testCase.rbObj=testCase.rbObj.addDataToCircularBuffer(t, v);
            end
        end
    end
end
