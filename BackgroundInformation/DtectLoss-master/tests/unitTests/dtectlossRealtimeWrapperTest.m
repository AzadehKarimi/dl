classdef dtectlossRealtimeWrapperTest < matlab.unittest.TestCase
    %DTECTLOSSREALTIMEOBJTEST unit tests of the dtectlossRealtime class with
    %methods
    
    properties
        rwdu; % raw data units
        dr; % dtectloss realtime object
        requiredTags = {'flowIn', 'depthBit', 'depthHole'};
        optionalTags = {'flowOut', 'presSP', 'volGainLoss', 'volPitDrill', 'volTripTank', 'heightHook', ...
            'rpm', 'torque', 'weightOnBit'};
        initTime_d = datenum(2017, 06, 05, 12, 30, 30);
    end
    
    methods (TestClassSetup)
        function initFunc(testCase)
            testCase.rwdu=struct(); % raw data units
            testCase.rwdu.flowIn='m3/s';
            testCase.rwdu.flowOut='m3/s';
            testCase.rwdu.flowOutPercent='%';
            testCase.rwdu.rpm='rpm';
            testCase.rwdu.velRop='m/s';
            testCase.rwdu.presSP='kPa';
            testCase.rwdu.depthHole='m';
            testCase.rwdu.depthBit='m';
            testCase.rwdu.heightHook='m';
            testCase.rwdu.volGainLoss='m3';
            testCase.rwdu.tempBHA='K';
            testCase.rwdu.volTripTank='m3';
        end
    end
    
    methods (TestMethodSetup)
        function initMethod(testCase)
            clearvars �global
        end
    end
    
    methods (Test)
        function init_correctInit_returnObj(testCase)
            DTECTLOSS_REALTIME_OBJ_init('VOTING_ALG_OBJ', testCase.rwdu, testCase.initTime_d);
            global GLOBAL_DTECTLOSS_REALTIME_OBJ_obj;
            testCase.verifyClass(GLOBAL_DTECTLOSS_REALTIME_OBJ_obj, 'DTECTLOSS_REALTIME_OBJ');
        end
        
        function init_emptyUnit_issueWarning(testCase)
            testCase.verifyWarning(@()DTECTLOSS_REALTIME_OBJ_init('VOTING_ALG_OBJ', struct(), testCase.initTime_d),...
                'DTECTLOSS_REALTIME_OBJ:missingRawDataUnits');
        end
        
        function init_unitNotAStruct_issueError(testCase)
            testCase.verifyError(@()DTECTLOSS_REALTIME_OBJ_init('VOTING_ALG_OBJ', [], testCase.initTime_d),...
                'DTECTLOSS_REALTIME_OBJ:rawDataUnitsNotAStruct');
        end
        
        function init_noInitTime_issueError(testCase)
            testCase.verifyError(@()DTECTLOSS_REALTIME_OBJ_init('VOTING_ALG_OBJ', testCase.rwdu),...
                'DTECTLOSS_REALTIME_OBJ:missingInitTime');
        end
        
        function init_emptyInitTime_issueError(testCase)
            testCase.verifyError(@()DTECTLOSS_REALTIME_OBJ_init('VOTING_ALG_OBJ', testCase.rwdu, []),...
                'DTECTLOSS_REALTIME_OBJ:missingInitTime');
        end
        
        function init_zeroInitTime_issueError(testCase)
            testCase.verifyError(@()DTECTLOSS_REALTIME_OBJ_init('VOTING_ALG_OBJ', testCase.rwdu, 0),...
                'DTECTLOSS_REALTIME_OBJ:missingInitTime');
        end
        
        function init_backtestModeNotGiven_backtestModeFalse(testCase)
            DTECTLOSS_REALTIME_OBJ_init('VOTING_ALG_OBJ', testCase.rwdu, testCase.initTime_d);
            global GLOBAL_DTECTLOSS_REALTIME_OBJ_obj
            testCase.verifyFalse(GLOBAL_DTECTLOSS_REALTIME_OBJ_obj.getBacktestMode);
        end
        
        function init_backtestModeGiven_backtestModeTrue(testCase)
            DTECTLOSS_REALTIME_OBJ_init('VOTING_ALG_OBJ', testCase.rwdu, testCase.initTime_d, true);
            global GLOBAL_DTECTLOSS_REALTIME_OBJ_obj
            testCase.verifyTrue(GLOBAL_DTECTLOSS_REALTIME_OBJ_obj.getBacktestMode);
        end
        
        function init_backtestModeGivenAsFalse_backtestModeFalse(testCase)
            DTECTLOSS_REALTIME_OBJ_init('VOTING_ALG_OBJ', testCase.rwdu, testCase.initTime_d, false);
            global GLOBAL_DTECTLOSS_REALTIME_OBJ_obj
            testCase.verifyFalse(GLOBAL_DTECTLOSS_REALTIME_OBJ_obj.getBacktestMode);
        end
        
        function softreset_backtestModeNotGiven_noError(testCase)
            DTECTLOSS_REALTIME_OBJ_init('VOTING_ALG_OBJ', testCase.rwdu, testCase.initTime_d, false);
            global GLOBAL_DTECTLOSS_REALTIME_OBJ_obj
            testCase.assertFalse(GLOBAL_DTECTLOSS_REALTIME_OBJ_obj.getBacktestMode);
            
            DTECTLOSS_REALTIME_OBJ_softreset;
        end
        
        function reset_backtestModeNotGiven_noError(testCase)
            DTECTLOSS_REALTIME_OBJ_init('VOTING_ALG_OBJ', testCase.rwdu, testCase.initTime_d, false);
            global GLOBAL_DTECTLOSS_REALTIME_OBJ_obj
            testCase.assertFalse(GLOBAL_DTECTLOSS_REALTIME_OBJ_obj.getBacktestMode);
            
            DTECTLOSS_REALTIME_OBJ_reset(testCase.initTime_d);
        end
        
        function stepAllAlgorithms_allRequiredAndAllOptional_noError(testCase)
            DTECTLOSS_REALTIME_OBJ_init('VOTING_ALG_OBJ', testCase.rwdu, testCase.initTime_d);
                    
            testTags = [testCase.requiredTags testCase.optionalTags];
            for k= 1:1:numel(testTags)
                inputVec.(testTags{k}).t=0;
                inputVec.(testTags{k}).V=0;
            end
            dtectLossOutput = DTECTLOSS_REALTIME_OBJ_stepAlgorithm(inputVec);
            testCase.verifyEqual(dtectLossOutput.errorCode, 1);
        end
        
        function stepAllAlgorithms_onlyRequiredTags_noError(testCase)
            rdu=struct('flowIn', 'lpm', 'depthBit', 'm', 'depthHole', 'm');
            DTECTLOSS_REALTIME_OBJ_init('VOTING_ALG_OBJ', rdu, testCase.initTime_d);
            testTags = testCase.requiredTags;
            for k= 1:1:numel(testTags)
                inputVec.(testTags{k}).t=0;
                inputVec.(testTags{k}).V=0;
            end
            dtectLossOutput = DTECTLOSS_REALTIME_OBJ_stepAlgorithm(inputVec);
            testCase.verifyEqual(dtectLossOutput.errorCode, 1);
        end
        
        function stepAllAlgorithms_noTags_returnErrorCode(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            testCase.applyFixture(...
                SuppressedWarningsFixture('DTECTLOSS_REALTIME_OBJ:missingRawDataUnits'));
            
            DTECTLOSS_REALTIME_OBJ_init('VOTING_ALG_OBJ', struct(), testCase.initTime_d);
            dtectLossOutput = DTECTLOSS_REALTIME_OBJ_stepAlgorithm(struct());
            testCase.verifyTrue(dtectLossOutput.errorCode<0);
        end
        
        function stepAllAlgorithms_oneRequiredTagMissing_returnErrorCode(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            testCase.applyFixture(...
                SuppressedWarningsFixture('PROCESS_OBJ:noConversionInUnitMap'));
            
            for caseIdx = 1:numel(testCase.requiredTags)
                inputTags = testCase.requiredTags([1:caseIdx-1 caseIdx+1:end]);
                rd=struct();
                rdu=struct();
                for k= 1:1:numel(inputTags)
                    rdu.(inputTags{k})='unit';
                    rd.(inputTags{k}).t=0;
                    rd.(inputTags{k}).V=0;
                end
                
                DTECTLOSS_REALTIME_OBJ_init('VOTING_ALG_OBJ', rdu, testCase.initTime_d);
                dtectLossOutput= DTECTLOSS_REALTIME_OBJ_stepAlgorithm(rd);
                testCase.verifyTrue(dtectLossOutput.errorCode<0 && dtectLossOutput.errorCode>-1000,...
                    ['removed:',testCase.requiredTags{caseIdx} ,' did not give alarm']);
            end
        end
        
        function stepAllAlgotrithms_addNewTagWhileRunning_noError(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            testCase.applyFixture(...
                SuppressedWarningsFixture('DTECTLOSS_REALTIME_OBJ:missingRawDataUnits'));
            
            rawDataNonStdUnits=struct(); % raw data units
            rawDataNonStdUnits.flowIn='m3/s'; % 
            rawDataNonStdUnits.flowOut='m3/s';
            rawDataNonStdUnits.flowOutPercent='%';
            rawDataNonStdUnits.rpm='rpm';
            rawDataNonStdUnits.velRop='m/s';
            rawDataNonStdUnits.presSP='kPa';
            rawDataNonStdUnits.depthHole='m';
            rawDataNonStdUnits.depthBit='m';
            rawDataNonStdUnits.volGainLoss='m3';
            rawDataNonStdUnits.volTripTank='m3';
            
            rawData_nonStd=struct();
            rawData_nonStd.flowIn.V=0.15;
            rawData_nonStd.flowIn.t=now;
            rawData_nonStd.flowOut.V=0.15;
            rawData_nonStd.flowOut.t=now;
            rawData_nonStd.flowOutPercent.V=15;
            rawData_nonStd.flowOutPercent.t=now;
            rawData_nonStd.rpm.V=200;
            rawData_nonStd.rpm.t=now;
            rawData_nonStd.velRop.V=0.01;
            rawData_nonStd.velRop.t=now;
            rawData_nonStd.presSP.V=5000;
            rawData_nonStd.presSP.t=now;
            rawData_nonStd.depthHole.V=3000;
            rawData_nonStd.depthHole.t=now;
            rawData_nonStd.depthBit.V=2999;
            rawData_nonStd.depthBit.t=now;
            rawData_nonStd.volGainLoss.V=3;
            rawData_nonStd.volGainLoss.t=now;
            rawData_nonStd.volTripTank.V=2;
            rawData_nonStd.volTripTank.t=now;
            
            DTECTLOSS_REALTIME_OBJ_init('VOTING_ALG_OBJ', rawDataNonStdUnits, testCase.initTime_d);
            DTECTLOSS_REALTIME_OBJ_stepAlgorithm(rawData_nonStd);
            
            % now add heightHook as new tag and step all algs again
            rawDataNonStdUnits.heightHook='m';
            rawData_nonStd.heightHook.V=2;
            rawData_nonStd.heightHook.t=now;
            DTECTLOSS_REALTIME_OBJ_updateTagList(rawDataNonStdUnits);
            dtectLossOutput = DTECTLOSS_REALTIME_OBJ_stepAlgorithm(rawData_nonStd);
            
            testCase.verifyEqual(dtectLossOutput.errorCode,  DtectLossRealtimeErrorCodes.AllOk);
        end
        
        function stepAllAlgotrithms_removeTagWhileRunning_noError(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            testCase.applyFixture(...
                SuppressedWarningsFixture('DTECTLOSS_REALTIME_OBJ:missingRawDataUnits'));
            
            rawDataNonStdUnits=struct(); % raw data units
            rawDataNonStdUnits.flowIn='m3/s'; % 
            rawDataNonStdUnits.flowOut='m3/s';
            rawDataNonStdUnits.flowOutPercent='%';
            rawDataNonStdUnits.rpm='rpm';
            rawDataNonStdUnits.velRop='m/s';
            rawDataNonStdUnits.presSP='kPa';
            rawDataNonStdUnits.depthHole='m';
            rawDataNonStdUnits.depthBit='m';
            rawDataNonStdUnits.volGainLoss='m3';
            rawDataNonStdUnits.volTripTank='m3';
            
            rawData_nonStd=struct();
            rawData_nonStd.flowIn.V=0.15;
            rawData_nonStd.flowIn.t=now;
            rawData_nonStd.flowOut.V=0.15;
            rawData_nonStd.flowOut.t=now;
            rawData_nonStd.flowOutPercent.V=15;
            rawData_nonStd.flowOutPercent.t=now;
            rawData_nonStd.rpm.V=200;
            rawData_nonStd.rpm.t=now;
            rawData_nonStd.velRop.V=0.01;
            rawData_nonStd.velRop.t=now;
            rawData_nonStd.presSP.V=5000;
            rawData_nonStd.presSP.t=now;
            rawData_nonStd.depthHole.V=3000;
            rawData_nonStd.depthHole.t=now;
            rawData_nonStd.depthBit.V=2999;
            rawData_nonStd.depthBit.t=now;
            rawData_nonStd.volGainLoss.V=3;
            rawData_nonStd.volGainLoss.t=now;
            rawData_nonStd.volTripTank.V=2;
            rawData_nonStd.volTripTank.t=now;
            
            DTECTLOSS_REALTIME_OBJ_init('VOTING_ALG_OBJ', rawDataNonStdUnits, testCase.initTime_d);
            DTECTLOSS_REALTIME_OBJ_stepAlgorithm(rawData_nonStd);
            
            % now add heightHook as new tag and step all algs again
            rawDataNonStdUnits=rmfield(rawDataNonStdUnits, 'flowOutPercent');
            rawData_nonStd=rmfield(rawData_nonStd, 'flowOutPercent');
            DTECTLOSS_REALTIME_OBJ_updateTagList(rawDataNonStdUnits);
            dtectLossOutput= DTECTLOSS_REALTIME_OBJ_stepAlgorithm(rawData_nonStd);
            
            testCase.verifyEqual(dtectLossOutput.errorCode,  DtectLossRealtimeErrorCodes.AllOk);
        end
    end
    
end

