classdef votingObjTest < matlab.unittest.TestCase
    %triptankObjTest testing of the EXPECTED_TRIPTANKVOL_OBJ.
    
    properties
        votingObj;
         volIOflow_m3;
         volIOpres_m3;
         volIOpit_m3;
         volIOtrip_m3;
         filtPitData;
    end
    
    methods (TestClassSetup)
        function clearFixture(testCase)
             testCase.votingObj = VOTING_ALG_OBJ();
             params = getparams(0);
             testCase.votingObj = initialize(testCase.votingObj, params.voting);
             testCase.addTeardown(@clear, 'testCase.votingObj');
        end

        function initFunc(testCase)            
             testCase.volIOflow_m3;
             testCase.volIOpres_m3;
             testCase.volIOpit_m3;
             testCase.volIOtrip_m3;
             testCase.filtPitData;
         end
    end
    
    
    methods (Test)
         function verifySetInjection(testCase)
              testCase.votingObj = setInjection(testCase.votingObj);
              testCase.verifyEqual(testCase.votingObj.volIOflow_m3, 0);         
         end
         
         function verifyCalcIOFlow(testCase)
             processData = struct();
             algData     = struct();
             sysState    = struct();
             algData.flowOutExpected_lpm = 2000;
             algData.isDownlinking = false;
             algData.isCalibratedExpFlowOut = true;
             algData.isCalibratedMeasFlowOut = true;
             algData.resetVolIOnow = false;
             processData.flowIn_lpm = 2000;
             processData.flowOut_lpm = 2101;
             processData.flowOutFiltered_lpm = 2150;
             processData.depthHole_m = 5000;
             processData.depthBit_m  = 5000 - 1;
             sysState.isOnBottom = true;

             dt_s = 2;
             testCase.votingObj = updateTime(testCase.votingObj, now, dt_s);
             testCase.votingObj = calcIOFlow(testCase.votingObj, algData, processData, sysState);
             testCase.verifyEqual(testCase.votingObj.flowIOflow_lpm, processData.flowOutFiltered_lpm - algData.flowOutExpected_lpm);             
             testCase.verifyEqual(testCase.votingObj.volIOflow_m3, dt_s*(processData.flowOutFiltered_lpm - algData.flowOutExpected_lpm)/60000);                          
             
             processData.flowOutFiltered_lpm = -0.99925;
             V0 = testCase.votingObj.volIOflow_m3;
             testCase.votingObj = updateTime(testCase.votingObj, now, dt_s);
             testCase.votingObj = calcIOFlow(testCase.votingObj, algData, processData, sysState);
             testCase.verifyEqual(testCase.votingObj.flowIOflow_lpm, processData.flowOut_lpm - algData.flowOutExpected_lpm);             
             testCase.verifyEqual(testCase.votingObj.volIOflow_m3, V0 + dt_s*(processData.flowOut_lpm - algData.flowOutExpected_lpm)/60000);                          
         end

         function verifyCalcIOPres(testCase)
             processData = struct();
             algData     = struct();
             sysState    = struct();
             algData.presSPExpected_bar = 200;
             algData.isDownlinking = false;
             algData.isCalibratedExpPresSP = true;
             algData.resetVolIOnow = false;
             processData.flowIn_lpm = 2000;
             processData.presSP_bar = 203;
             processData.depthHole_m = 5000;
             processData.depthBit_m  = 5000 - 1;
             processData.flowIn_lpm  = 3000;
             sysState.isOnBottom = true;

             dt_s = 2;
             testCase.votingObj = updateTime(testCase.votingObj, now, dt_s);
             testCase.votingObj = calcIOPres(testCase.votingObj, algData, processData, sysState);
             testCase.verifyEqual(testCase.votingObj.presIO_bar, processData.presSP_bar - algData.presSPExpected_bar);             
             testCase.verifyEqual(testCase.votingObj.flowIOpres_lpm, testCase.votingObj.presIO_bar*testCase.votingObj.params.scalePresToFlow);             
             testCase.verifyEqual(testCase.votingObj.volIOpres_m3, dt_s*testCase.votingObj.flowIOpres_lpm/60000);                          
         end

         function verifyCalcIOPit(testCase)
             processData = struct();
             algData     = struct();
             sysState    = struct();
             algData.volGainLossExpected_m3 = 21;
             algData.isDownlinking = false;
             algData.isCalibratedExpPitVol = true;
             algData.resetVolIOnow = false;
             processData.flowIn_lpm = 2000;
             processData.volGainLoss_m3 = 20;
             processData.depthHole_m = 5000;
             processData.depthBit_m  = 5000 - 1;
             processData.flowIn_lpm  = 3000;
             sysState.isOnBottom = true;
             dt_s = 2;
             timeNow_d = now;
             testCase.votingObj = updateTime(testCase.votingObj, timeNow_d, dt_s);
             testCase.votingObj = calcIOPit(testCase.votingObj, algData, processData, sysState);
             testCase.votingObj.volIOpit_m3
             testCase.votingObj.flowIOpit_lpm
             testCase.verifyEqual(testCase.votingObj.volIOpit_m3, processData.volGainLoss_m3 - algData.volGainLossExpected_m3);                          
             testCase.verifyEqual(testCase.votingObj.flowIOpit_lpm, 0);
             flowIO_lpm = -130;
             Vinc_m3 = (1:100)*flowIO_lpm/60000*dt_s;
             for i = 1:length(Vinc_m3)
                 algData.volGainLossExpected_m3 = 21 + Vinc_m3(i);
                 processData.volGainLoss_m3 = 20 + 2*Vinc_m3(i);
                 timeNow_d = timeNow_d + dt_s/86400;
                 testCase.votingObj = updateTime(testCase.votingObj, timeNow_d, dt_s);
                 testCase.votingObj = calcIOPit(testCase.votingObj, algData, processData, sysState);
             end
             testCase.verifyEqual(testCase.votingObj.volIOpit_m3, processData.volGainLoss_m3 - algData.volGainLossExpected_m3);                          
             testCase.verifyTrue(abs(testCase.votingObj.flowIOpit_lpm/flowIO_lpm - 1) < 0.01);             
         end

         function verifySetTimeHighFlowError(testCase)
             processData = struct();
             algData     = struct();
             sysState    = struct();
             algData.isCalibratedExpFlowOut = true;
             algData.isCalibratedMeasFlowOut = true;
             algData.flowOutExpected_lpm = 2000;
             algData.isDownlinking = false;
             algData.isCalibratedExpFlowOut = true;
             algData.isCalibratedMeasFlowOut = true;
             algData.resetVolIOnow = false;
             processData.flowIn_lpm = 2000;
             processData.flowOut_lpm = 2101;
             processData.flowOutFiltered_lpm = 2150;
             processData.depthHole_m = 5000;
             processData.depthBit_m  = 5000 - 1;
             sysState.isOnBottom = true;

             dt_s = 2;
             testCase.votingObj = updateTime(testCase.votingObj, now, dt_s);
             testCase.votingObj = calcIOFlow(testCase.votingObj, algData, processData, sysState);
             testCase.votingObj = setTimeHighFlowError(testCase.votingObj, algData, processData);
             testCase.verifyEqual(testCase.votingObj.timeHighFlowError_s, 0);   
             
             processData.flowIn_lpm = 300;
             algData.flowOutExpected_lpm = processData.flowIn_lpm;
             testCase.votingObj = calcIOFlow(testCase.votingObj, algData, processData, sysState);
             testCase.votingObj = setTimeHighFlowError(testCase.votingObj, algData, processData);
             testCase.verifyEqual(testCase.votingObj.timeHighFlowError_s, dt_s);   
         end
         
         function verifySetTimeHighVolError(testCase)
             processData = struct();
             algData     = struct();
             sysState    = struct();
             algData.volGainLossExpected_m3 = 20;
             algData.isDownlinking = false;
             algData.isCalibratedExpPitVol = true;
             algData.resetVolIOnow = false;
             algData.isRampingUp = false;
             processData.flowIn_lpm = 2000;
             processData.volGainLoss_m3 = 20.3;
             processData.depthHole_m = 5000;
             processData.depthBit_m  = 5000 - 1;
             sysState.isOnBottom = true;

             dt_s = 2;
             testCase.votingObj = updateTime(testCase.votingObj, now, dt_s);
             testCase.votingObj = calcIOPit(testCase.votingObj, algData, processData, sysState);
             testCase.votingObj = setTimeHighVolError(testCase.votingObj, algData, processData);
             testCase.verifyEqual(testCase.votingObj.timeHighVolError_s, 0);   
             
             processData.volGainLoss_m3 = 23;
             testCase.votingObj = calcIOPit(testCase.votingObj, algData, processData, sysState);
             testCase.votingObj = setTimeHighVolError(testCase.votingObj, algData, processData);
             testCase.verifyEqual(testCase.votingObj.timeHighVolError_s, dt_s);   
         end

         
         function verifyCheckForNewAlarm(testCase)
             AlarmConditions = struct();
             AlarmConditions.HighGainVol  = true;
             AlarmConditions.HighGainFlow = false;
             AlarmConditions.HighGainPres = false;
             AlarmConditions.HighLossVol  = false;
             AlarmConditions.HighLossFlow = false;
             AlarmConditions.HighLossPres = false;
             AlarmConditions.inhibit          = false;
             AlarmConditions.allowInfluxAlarm = false;
             AlarmConditions.startup          = false;
             AlarmConditions.SlowGain         = false;
             AlarmConditions.HighGainVolOnly  = false;
             AlarmConditions.HighLossVolOnly  = false;
             AlarmConditions.HighGainFlowOnly = false;
             AlarmConditions.HighLossFlowOnly = false;
             testCase.votingObj = checkForNewAlarm(testCase.votingObj, AlarmConditions);
             testCase.verifyEqual(testCase.votingObj.alarmState, AlarmState.Disabled);         
             testCase.verifyEqual(testCase.votingObj.alarmType, AlarmType.Normal); 

             AlarmConditions.HighGainVol  = true;
             AlarmConditions.HighGainFlow = true;
             AlarmConditions.HighGainPres = true;
             AlarmConditions.HighLossVol  = false;
             AlarmConditions.HighLossFlow = false;
             AlarmConditions.HighLossPres = false;
             AlarmConditions.inhibit          = true;
             AlarmConditions.allowInfluxAlarm = true;
             AlarmConditions.startup          = true;
             AlarmConditions.SlowGain         = true;
             AlarmConditions.HighGainVolOnly  = true;
             AlarmConditions.HighLossVolOnly  = true;
             AlarmConditions.HighGainFlowOnly = true;
             AlarmConditions.HighLossFlowOnly = true;

             testCase.votingObj = checkForNewAlarm(testCase.votingObj, AlarmConditions);
             testCase.verifyEqual(testCase.votingObj.alarmState, AlarmState.Alarm);         
             testCase.verifyEqual(testCase.votingObj.alarmType, AlarmType.FlowAndPressureInfluxAlarm); 
             AlarmConditions.HighGainPres = false;
             testCase.votingObj = checkForNewAlarm(testCase.votingObj, AlarmConditions);
             testCase.verifyEqual(testCase.votingObj.alarmState, AlarmState.Alarm);         
             testCase.verifyEqual(testCase.votingObj.alarmType, AlarmType.FlowAndPitInfluxAlarm); 
             AlarmConditions.HighGainPres = true;             
             AlarmConditions.HighGainFlow = false;
             testCase.votingObj = checkForNewAlarm(testCase.votingObj, AlarmConditions);
             testCase.verifyEqual(testCase.votingObj.alarmState, AlarmState.Alarm);         
             testCase.verifyEqual(testCase.votingObj.alarmType, AlarmType.PitAndPressureInfluxAlarm); 
             AlarmConditions.HighGainVol  = false;
             AlarmConditions.HighGainPres = false;             
             AlarmConditions.HighGainFlow = false;
             AlarmConditions.HighLossVol  = true;
             AlarmConditions.HighLossFlow = true;
             AlarmConditions.HighLossPres = true;
             testCase.votingObj = checkForNewAlarm(testCase.votingObj, AlarmConditions);
             testCase.verifyEqual(testCase.votingObj.alarmState, AlarmState.Alarm);         
             testCase.verifyEqual(testCase.votingObj.alarmType, AlarmType.FlowAndPressureLossAlarm); 
             AlarmConditions.HighLossPres = false;
             testCase.votingObj = checkForNewAlarm(testCase.votingObj, AlarmConditions);
             testCase.verifyEqual(testCase.votingObj.alarmState, AlarmState.Alarm);         
             testCase.verifyEqual(testCase.votingObj.alarmType, AlarmType.FlowAndPitLossAlarm); 

         end
         
         function verifySetVolIOAlarm(testCase)
%              obj.volIOpit_m3  = -2;
%              obj.volIOflow_m3 = 1;
%              obj.volIOpres_m3 = 3;
%              obj.alarmType    = AlarmType.PitSlowGainAlarm;
%              testCase.votingObj = setVolIOAlarm(testCase.votingObj);
%              testCase.verifyEqual(obj.volIO_m3, obj.volIOpit_m3);
%              obj.alarmType    = AlarmType.PitOnlyLossAlarm;
%              testCase.votingObj = setVolIOAlarm(testCase.votingObj);
%              testCase.verifyEqual(obj.volIO_m3, obj.volIOpit_m3);
%              obj.alarmType    = AlarmType.PitOnlyInfluxAlarm;
%              obj = setVolIOAlarm(obj);
%              testCase.verifyEqual(obj.volIO_m3, obj.volIOpit_m3);
             
         end
         
             
     end
end
