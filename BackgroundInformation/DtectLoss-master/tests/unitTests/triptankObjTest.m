classdef triptankObjTest < matlab.unittest.TestCase
    %triptankObjTest testing of the EXPECTED_TRIPTANKVOL_OBJ.
    
    properties
        numStand;
        numOKStand;
        volTTLastStand_m3;
        depthLastStand_m;
        volSteelLastStand_m3;
        volTTdiff_m3;
        lengthLastStand_m;
        lengthStandAverage_m;
        areaDPLastStand_m2;
        areaDPAverage_m2;
        volTTcum_m3;
        volTTcumExp_m3;
        volTTcumErr_m3;
        timeLastStand_d;
        
        %Updated every new depth sample
        depthBitPrev_m;%previous value
        volTTPrev_m3; %previous value
        depthDelta_m;
        
        %Logic values
        stateTripping; % 1=RIH, 0=OnBottom/Steady, -1=POOH
        isOnBottom; 
        isPipeSteady; % 1=steady, 0=is moving
        isvolTTSteady; % 1=steady, 0=is changing
        isNewStandTripTank;        
        isOKLastStand;
        params; %Tuning params
        
        areaDPPOOH_m2; 
        areaDPRIH_m2; 
        volTripTankBufferObj;                      
        depthBitBufferObj;
        timePrevDepth_d;
        timePrevVol_d;
        dtDepth_s;
        dtVol_s;

        ttObj; % trip tank  obj

    end
    
    methods (TestClassSetup)
        function initFunc(testCase)            
            testCase.numStand                        = 10;
            testCase.numOKStand                      = 12;
            testCase.volTTLastStand_m3               = 1;
            testCase.depthLastStand_m                = 3000; 
            testCase.volSteelLastStand_m3            = 0.01*30;
            testCase.isOKLastStand                   = true;
            testCase.isNewStandTripTank              = false;
            testCase.volTTdiff_m3                    = 0.04;
            testCase.lengthLastStand_m               = 29;
            testCase.areaDPLastStand_m2              = 0.012;
            testCase.areaDPAverage_m2                = 0.013;
            testCase.volTTcum_m3                     = 3;
            testCase.volTTcumExp_m3                  = 3.1;
            testCase.volTTcumErr_m3                  = 0.1;
            testCase.timeLastStand_d                 = 0;           
            testCase.lengthStandAverage_m            = 30;  
            testCase.params                          = getparams(0);
            testCase.areaDPPOOH_m2                   = 0.005; 
            testCase.areaDPRIH_m2                    = 0.015; 
        end
    end
    
    methods (TestMethodSetup)
        function clearFixture(testCase)
             testCase.ttObj = EXPECTED_TRIPTANKVOL_OBJ();
             testCase.addTeardown(@clear, 'testCase.ttObj');
        end
    end
    
    methods (Test)
        function constructor_correctConstruct_correctOutput(testCase)           
            testCase.verifyEmpty(testCase.ttObj.numStand, 'Verify empty state');
            testCase.verifyEmpty(testCase.ttObj.depthBitPrev_m);
        end
        
        function initialize_correctInit_correctOutput(testCase)           
            testCase.ttObj=testCase.ttObj.initialize(testCase.params);
            testCase.verifyEqual(testCase.ttObj.numStand, 0);
        end
        
        function initialize_setAreadp_correctOutput(testCase)            
            testCase.ttObj=testCase.ttObj.initialize(testCase.params);
            testCase.ttObj=testCase.ttObj.setAreadp(testCase.areaDPPOOH_m2, testCase.areaDPRIH_m2);
            testCase.verifyEqual(testCase.ttObj.areaDPPOOH_m2, testCase.areaDPPOOH_m2);
            testCase.verifyEqual(testCase.ttObj.areaDPRIH_m2, testCase.areaDPRIH_m2);
        end
        
         function reset_correctOutput(testCase)
            testCase.ttObj=testCase.ttObj.reset();
            testCase.verifyEqual(testCase.ttObj.timeLastStand_d, 0);
            testCase.verifyEqual(testCase.ttObj.numStand, 0);             
         end
         
         function resetTripVol_correctOutput(testCase)
            testCase.ttObj.volTTcumExp_m3 = 1;
            testCase.ttObj.volTTcum_m3    = 2;
            testCase.volTTcumErr_m3       = testCase.volTTcum_m3 - testCase.volTTcumExp_m3; 
            testCase.ttObj=testCase.ttObj.resetTripVol();
            testCase.verifyEqual(testCase.ttObj.volTTcumExp_m3, testCase.ttObj.volTTcum_m3);
            testCase.verifyEqual(testCase.ttObj.volTTcumErr_m3, 0);             
         end

         function haveWeDrilledAnotherStandNow_correctOutput(testCase)
            numstandtrue   = 4;
            numtrip        = 100;
            numconn        = 50;
            num            = numstandtrue*(numtrip + numconn);
            lengthStand_m  = 29;
            areaDP_m2      = 0.01;
            volStand_m3    = lengthStand_m*areaDP_m2;
            dd_m           = lengthStand_m/numtrip;
            dV_m3          = volStand_m3/numtrip;
            dt_d           = 2/3600/24;
            depthBit_m     = [1000 : dd_m : 1000+lengthStand_m, (1000+lengthStand_m)*ones(1,numconn)];               
            volTripTank_m3 = [   5 : dV_m3: 5+volStand_m3,         (5+volStand_m3)*ones(1,numconn)]; 
            if numstandtrue > 1
                for ii = 1:numstandtrue-1
                    depthBit_m     = [depthBit_m,     1000+ii*lengthStand_m : dd_m : 1000+(ii+1)*lengthStand_m, (1000+(ii+1)*lengthStand_m)*ones(1,numconn)];  
                    volTripTank_m3 = [volTripTank_m3,    5+ii*volStand_m3   : dV_m3: 5+(ii+1)*volStand_m3,      (   5+(ii+1)*volStand_m3)*ones(1,numconn)]; 
                end
            end
            timeNew_d      = now: dt_d : now+num*dt_d; 
            depthHole_m    = 2000;
            flowIn_lpm     = 0;
            testCase.ttObj = EXPECTED_TRIPTANKVOL_OBJ();
            testCase.ttObj = testCase.ttObj.initialize(testCase.params);
            testCase.ttObj.depthLastStand_m = depthBit_m(1);
            numStands = 0;
            for ii = 1:num
                [testCase.ttObj, TripTankOutput] = testCase.ttObj.step(depthBit_m(ii), depthHole_m, volTripTank_m3(ii), flowIn_lpm, timeNew_d(ii));
                if testCase.ttObj.isNewStandTripTank
                    numStands = numStands + 1;
                end                   
            end
            testCase.verifyEqual(numStands, numstandtrue);
         end
    end
end
