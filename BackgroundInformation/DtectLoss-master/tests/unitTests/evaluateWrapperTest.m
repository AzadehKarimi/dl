classdef evaluateWrapperTest < matlab.unittest.TestCase
    %EVALUATEOBJTEST unit tests for wrapper functions using the evaluate object.
    
    properties
        trueData;
        falseData;
        simData;
        trueNoOfAlarms;
        trueNoOfFalseAlarms;
        trueNoOfFalseWarnings;
        trueNoOfWarnings;
        algName;
        rigName;
        eva; % evaluate object under test
        trueIgnore;
        trueVolIO;
        algAlarm;
        flowIO;
        volIO;
    end
    
    methods (TestClassSetup)
        function initClass(testCase)
            load(fullfile('helperFiles', 'simDataForEvaluateObjTest.mat'));
            testCase.simData=sd;
            testCase.trueData=td;
            testCase.falseData=fd; % alg called falg with 1 false alarm
            testCase.trueNoOfAlarms=2;
            testCase.trueNoOfFalseAlarms=0;
            testCase.trueNoOfFalseWarnings=1;
            testCase.trueNoOfWarnings=2;
            testCase.algName='ukf';
            testCase.rigName='simRig';
            
            secPerDay=86400;
            
            trueM=[...
                0  0 0.0 0;
                9  0 0.0 0;
                10 0 0.8 1;
                12 0 1.0 2;
                17 0 2.0 2;
                50 0 0.0 0];
            
            algM=[...
                0    0   0 0;
                11 500 0.5 0;
                12 500 0.8 1;
                13 500 1.0 2;
                14   0 0.0 0;
                15 500 0.6 0;
                16 500 0.8 1;
                17 500 1.0 2];
            
            
            testCase.trueIgnore=struct('t', trueM(:,1)/secPerDay, 'V', trueM(:,2));
            testCase.trueVolIO=struct('t', trueM(:,1)/secPerDay, 'V', trueM(:,3));
            testCase.algAlarm=struct('T', algM(:,1)/secPerDay, 'V', algM(:,4));
            testCase.flowIO=struct('T', algM(:,1)/secPerDay, 'V', algM(:,2));
            testCase.volIO=struct('T', algM(:,1)/secPerDay, 'V', algM(:,3));
        end
    end
    
    methods (TestMethodSetup)
        function initMethod(testCase)
            clearvars -global
            EVALUATE_OBJ_init(testCase.algName, testCase.rigName);
        end
    end
    
    methods (Test)
        function init_correctInit_returnObj(testCase)
            global EVALUATE_OBJ_obj;
            testCase.verifyClass(EVALUATE_OBJ_obj, 'EVALUATE_OBJ');
        end
        
        function initialize_correctInit_correctOutput(testCase)
            global EVALUATE_OBJ_obj;
            testCase.verifyEqual(EVALUATE_OBJ_obj.trueNoOfAlarms, 0, 'Verify that n of alarms is zero');
            testCase.verifyEqual(EVALUATE_OBJ_obj.trueNoOfWarnings, 0, 'Verify that n of warnings is zero');
            testCase.verifyEqual(EVALUATE_OBJ_obj.algNoOfCorrectAlarms, 0, 'Verify that n of alarms is zero');
            testCase.verifyEqual(EVALUATE_OBJ_obj.algNoOfWarnings, 0, 'Verify that n of warnings is zero');
            testCase.verifyEqual(EVALUATE_OBJ_obj.algNoOfFalseAlarms, 0, 'Verify that n of false alarms is zero');
            testCase.verifyEqual(EVALUATE_OBJ_obj.FAR, 0, 'Verify that FAR is zero');
            testCase.verifyEqual(EVALUATE_OBJ_obj.PD, NaN, 'Verify that PD is NaN');
            testCase.verifyEqual(EVALUATE_OBJ_obj.kpi, NaN, 'Verify that kpi is NaN');
            testCase.verifyEqual(EVALUATE_OBJ_obj.TD, NaN, 'Verify that TD is NaN');
            testCase.verifyEqual(EVALUATE_OBJ_obj.VD, NaN, 'Verify that VD is NaN');
            testCase.verifyEqual(EVALUATE_OBJ_obj.algUpTime, NaN, 'Verify that algorithm up-time is NaN');
            testCase.verifyEqual(EVALUATE_OBJ_obj.algUpTimeInHours, NaN, 'Verify that algorithm up-time in hours is NaN');
        end
        
        function evaluate_noAlgAlarmType_returnEmpty(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            % suppress warning
            testCase.applyFixture(SuppressedWarningsFixture('EVALUATE_OBJ:differentStartTimeInTrueFileAndFieldData'));
            
            [nAlarms, alarmTypes]=EVALUATE_OBJ_evaluate(testCase.trueData.flowIO, testCase.trueData.volIO,...
                testCase.falseData.flowIO_lpm_falg, testCase.falseData.volIO_m3_falg,...
                testCase.falseData.alarmState_falg);
            
            testCase.verifyEmpty(nAlarms);
            testCase.verifyEmpty(alarmTypes);
        end
        
        function evaluate_trueDataComparison_correctResult(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            % suppress warning
            testCase.applyFixture(SuppressedWarningsFixture('EVALUATE_OBJ:differentStartTimeInTrueFileAndFieldData'));
            
            global EVALUATE_OBJ_obj;
            EVALUATE_OBJ_evaluate(testCase.trueData.trueIgnore, testCase.trueData.volIO,...
                testCase.falseData.flowIO_lpm_falg, testCase.falseData.volIO_m3_falg,...
                testCase.falseData.alarmState_falg);
            testCase.verifyEqual(EVALUATE_OBJ_obj.algNoOfCorrectAlarms, 2, 'Check on number of alarms produced by algorithm');
            testCase.verifyEqual(EVALUATE_OBJ_obj.algNoOfWarnings, testCase.trueNoOfWarnings+testCase.trueNoOfFalseWarnings,...
                'Check on number of warnings produced by algorithm');
            testCase.verifyEqual(EVALUATE_OBJ_obj.algNoOfFalseAlarms, testCase.trueNoOfFalseAlarms, 'Check on number of false alarms produced by algorithm');
        end
        
        function evaluate_trueDataComparison_correctResultForSimpleAlg(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            % suppress warning
            testCase.applyFixture(SuppressedWarningsFixture('EVALUATE_OBJ:differentStartTimeInTrueFileAndFieldData'));
            
            global EVALUATE_OBJ_obj;
            EVALUATE_OBJ_evaluate(testCase.trueData.trueIgnore, testCase.trueData.volIO,...
                testCase.simData.flowIO_lpm_simple, testCase.simData.volIO_m3_simple,...
                testCase.simData.alarmState_simple);
            testCase.verifyEqual(EVALUATE_OBJ_obj.algNoOfCorrectAlarms, 2, 'Check on number of alarms produced by algorithm');
            testCase.verifyEqual(EVALUATE_OBJ_obj.algNoOfFalseAlarms, 0, 'Check on number of false alarms produced by algorithm');
        end
        
        function evaluate_trueDataComparison_correctAlgorithmUpTime(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            % suppress warning
            testCase.applyFixture(SuppressedWarningsFixture('EVALUATE_OBJ:differentStartTimeInTrueFileAndFieldData'));
            
            disabledAlarmState=testCase.simData.alarmState_ukf;
            disabledAlarmState.V(3500:3800)=AlarmState.Disabled;
            disabledDuration=disabledAlarmState.T(3800)-disabledAlarmState.T(3499);
            expAlgUpTime=100-disabledDuration/(disabledAlarmState.T(end)-disabledAlarmState.T(1))*100;
            expAlgUpTimeInHours=(disabledAlarmState.T(end)-disabledAlarmState.T(1)-disabledDuration)*24;
            
            global EVALUATE_OBJ_obj;
            EVALUATE_OBJ_evaluate(testCase.trueData.trueIgnore, testCase.trueData.volIO,...
                testCase.simData.flowIO_lpm_ukf, testCase.simData.volIO_m3_ukf,...
                disabledAlarmState);
            testCase.verifyEqual(EVALUATE_OBJ_obj.algUpTime, expAlgUpTime, 'Check that the algorithm up-time is calculated correctly');
            testCase.verifyEqual(EVALUATE_OBJ_obj.algUpTimeInHours, expAlgUpTimeInHours, 'Check that the algorithm up-time is calculated correctly');
        end
        
        function evaluate_zeroInputForTrueData_correctNoOfAlarms(testCase)
            global EVALUATE_OBJ_obj;
            EVALUATE_OBJ_evaluate(0, 0,...
                testCase.falseData.flowIO_lpm_falg, testCase.falseData.volIO_m3_falg,...
                testCase.falseData.alarmState_falg);
            testCase.verifyEqual(EVALUATE_OBJ_obj.algNoOfCorrectAlarms, 0, 'Check on number of correct alarms produced by algorithm');
            testCase.verifyEqual(EVALUATE_OBJ_obj.algNoOfFalseAlarms, 3, 'Check on number of false alarms produced by algorithm');
        end
        
        function evaluate_multipleTrueAlarms_countTrueAlarmAsOne(testCase)
            % in the case where multiple alarms are issued subsequent to an
            % actual alarm, the alarms should be counted only one time.
            
            global EVALUATE_OBJ_obj;
            EVALUATE_OBJ_evaluate(testCase.trueIgnore, testCase.trueVolIO,...
                testCase.flowIO, testCase.volIO, testCase.algAlarm);
            
            testCase.verifyEqual(EVALUATE_OBJ_obj.algNoOfCorrectAlarms, 1, 'Check on number of correct alarms produced by algorithm');
            testCase.verifyEqual(EVALUATE_OBJ_obj.algNoOfFalseAlarms, 0, 'Check on number of false alarms produced by algorithm');
            testCase.verifyEqual(EVALUATE_OBJ_obj.algLateAlarms, 1, 'Check on number of late alarms produced by algorithm');
            testCase.verifyEqual(EVALUATE_OBJ_obj.PD, 100, 'The PD should be 100 even though there are 2 correct alarms but only one actual');
        end
        
        
        function evaluate_noTrueFile_countAlarmTypes(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            % suppress warning
            testCase.applyFixture(SuppressedWarningsFixture('EVALUATE_OBJ:emptyIoEvent'));
            
            load('tests/testdata/evaTestData.mat');
            
            nAlarms = EVALUATE_OBJ_evaluate(0, 0, retCell.flowIO_lpm_voting, retCell.volIO_m3_voting,...
                retCell.alarmState_voting, retCell.alarmType_voting);
            
            testCase.verifyNotEmpty(nAlarms);
        end
        
        function generateOutput_noOutput_isStruct(testCase)
            output = EVALUATE_OBJ_generateOutput();
            testCase.verifyClass(output, 'struct')
        end
        
        function generateOutput_noOutput_correctFieldnames(testCase)
            expFieldNames ={...
                'trueNoOfAlarms';
                'trueNoOfWarnings';
                'algNoOfCorrectAlarms';
                'algNoOfWarnings';
                'algNoOfFalseAlarms';
                'algLateAlarms';
                'algName'; 
                'rigName';
                'falseAlarmRate';
                'probabilityOfDetection';
                'keyPerformanceIndex';
                'timeToDetection';
                'accumulatedVolumeAtDetection';
                'durationInHours';
                'algUpTime';
                'algUpTimeInHours'};
            
            M = size(expFieldNames, 1);
            hasFoundExpectedName = false(M, 1);            
            
            output = EVALUATE_OBJ_generateOutput();
            
            fn = fieldnames(output);
            
            N = size(fn, 1);
            for ii = 1:N
                for jj = 1:M
                    if strcmp(fn{ii}, expFieldNames{jj})
                        hasFoundExpectedName(jj) = true;
                        continue
                    end
                end
            end
            testCase.verifyTrue(all(hasFoundExpectedName))
        end
        
        function generateOutput_afterEvaluate_correctOutput(testCase)
            EVALUATE_OBJ_evaluate(testCase.trueIgnore, testCase.trueVolIO,...
                testCase.flowIO, testCase.volIO, testCase.algAlarm);
            output = EVALUATE_OBJ_generateOutput();
                        
            testCase.verifyEqual(output.algNoOfCorrectAlarms, 1, 'Check on number of correct alarms produced by algorithm');
            testCase.verifyEqual(output.algNoOfFalseAlarms, 0, 'Check on number of false alarms produced by algorithm');
            testCase.verifyEqual(output.algLateAlarms, 1, 'Check on number of late alarms produced by algorithm');
            testCase.verifyEqual(output.probabilityOfDetection, 100, 'The PD should be 100 even though there are 2 correct alarms but only one actual');

        end
        
    end
    
end

