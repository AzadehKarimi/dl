classdef rollingAvgVarTest < matlab.unittest.TestCase
    %ROLLINGAVGVARTEST unit tests of the rollingAvgVar class with
    %methods
    
    properties
        dataVec % data vector
        rollingAvgExp % expected average produced by Matlab's mean function
        rollingVarExp % expected variance produced by Matlab's var function
        rollingStdDevExp% expected standard deviation produced by Matlab's std function
        rollingAvgAct % actual average produced by algorithm to be tested
        rollingVarAct % actual variance produced by algorithm to be tested
        rollingStdDevAct % actual standard deviation produced by algorithm to be tested
        nWin % rolling window size
        N % number of elements in test-dataset
        rav % rolling avereage variance object which is subject to test
    end
    
    methods (TestClassSetup)
        function initFunc(testCase)
            rng default % to obtain reproducible results
            testCase.N=100;
            testCase.dataVec=rand(testCase.N,1);
            testCase.nWin=10;
            testCase.rollingAvgExp=zeros(testCase.N,1);
            testCase.rollingVarExp=zeros(testCase.N,1);
            testCase.rollingStdDevExp=zeros(testCase.N,1);
            
            for ii=1:testCase.N
                if ii<=testCase.nWin
                    testCase.rollingAvgExp(ii)=mean(testCase.dataVec(1:ii));
                    testCase.rollingVarExp(ii)=var(testCase.dataVec(1:ii));
                    testCase.rollingStdDevExp(ii)=std(testCase.dataVec(1:ii));
                else
                    testCase.rollingAvgExp(ii)=mean(testCase.dataVec(ii-testCase.nWin+1:ii));
                    testCase.rollingVarExp(ii)=var(testCase.dataVec(ii-testCase.nWin+1:ii));
                    testCase.rollingStdDevExp(ii)=std(testCase.dataVec(ii-testCase.nWin+1:ii));
                end
            end
        end
    end
    
    methods (TestMethodSetup)
        function initBeforeEachTest(testCase)
            testCase.rollingAvgAct=zeros(testCase.N,1);
            testCase.rollingVarAct=zeros(testCase.N,1);
            testCase.rollingStdDevAct=zeros(testCase.N,1);
            testCase.rav=ROLLING_AVG_VAR_OBJ(testCase.dataVec(1:testCase.nWin)); % init with data vector
            testCase.addTeardown(@clear, 'testCase.rav');
        end
    end
    
    methods (Test)
        function update_correctInit_correctOutput(testCase)
            import matlab.unittest.constraints.IsEqualTo
            import matlab.unittest.constraints.RelativeTolerance
            numTol=1e-10; % relative error which is tolerated
            
            testCase.rollingAvgAct(testCase.nWin)=testCase.rav.average;
            testCase.rollingVarAct(testCase.nWin)=testCase.rav.variance;
            testCase.rollingStdDevAct(testCase.nWin)=testCase.rav.stdDev;
            for ii=testCase.nWin+1:testCase.N
                testCase.rav=testCase.rav.update(testCase.dataVec(ii-testCase.nWin), testCase.dataVec(ii));
                testCase.rollingAvgAct(ii)=testCase.rav.average;
                testCase.rollingVarAct(ii)=testCase.rav.variance;
                testCase.rollingStdDevAct(ii)=testCase.rav.stdDev;
            end
            
            % verify with a numerical tolerance
            testCase.verifyThat(testCase.rollingAvgAct(testCase.nWin:testCase.N),...
                IsEqualTo(testCase.rollingAvgExp(testCase.nWin:testCase.N),...
                'Within', RelativeTolerance(numTol)), 'Average value should be the same');
            testCase.verifyThat(testCase.rollingVarAct(testCase.nWin:testCase.N),...
                IsEqualTo(testCase.rollingVarExp(testCase.nWin:testCase.N),...
                'Within', RelativeTolerance(numTol)), 'Variance should be the same');
            testCase.verifyThat(testCase.rollingStdDevAct(testCase.nWin:testCase.N),...
                IsEqualTo(testCase.rollingStdDevExp(testCase.nWin:testCase.N),...
                'Within', RelativeTolerance(numTol)), 'Standard deviation should be the same');
        end
    end
    
end

