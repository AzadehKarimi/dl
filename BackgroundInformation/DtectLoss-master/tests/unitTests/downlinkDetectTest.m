classdef downlinkDetectTest < matlab.unittest.TestCase
    
    properties
        dd
        missingValCode
        initTime_d
    end
    
    methods (TestMethodSetup)
        function initFunc(testCase)
            testCase.dd = DOWNLINK_DETECT();
            testCase.initTime_d = datenum(2017, 03, 27, 09, 49, 00);
            testCase.missingValCode = -0.99925;
            testCase.addTeardown(@clear, 'testCase.dd');
        end
    end
    
    methods (Test)
        function step_actualDownlinking_similarResultAsInitialVersion(testCase)
            
            load(fullfile('tests', 'unitTests', 'helperFiles', 'downlinking_actualDownlink.mat'));
            
            t = timeDownlink_d + testCase.initTime_d;
            n = numel(t);
            actIsDownlinking = false(n,1);
            for ii = 1:n
                testCase.dd = testCase.dd.step(XDownlink(ii, 2), t(ii), XDownlink(ii, 1)); %#ok<NODEF>
                actIsDownlinking(ii) = testCase.dd.isDownlinking;
            end
            
            %actAndExpDifference = logical(XDownlink(:,6)) - actIsDownlinking;
            actAndExpDifference = logical(XDownlink(:,6)) - actIsDownlinking;
            testCase.verifyLessThan(sum(actAndExpDifference), 5);
            
%             figure;
%             ax(1) = subplot(311);
%             plot(t, XDownlink(:, 1));
%             datetick('x');
%             ax(2) = subplot(312);
%             plot(t, XDownlink(:, 2));
%             datetick('x');
%             ax(3) = subplot(313);
%             plot(t, [actIsDownlinking, logical(XDownlink(:,6))]);
%             datetick('x');
%             linkaxes(ax, 'x'); 
%             zoom on;
            
        end
        
        function step_actualDownlinkingNoFlowInMeas_sameResultAsInitialVersion(testCase)
            
            load(fullfile('tests', 'unitTests', 'helperFiles', 'downlinking_actualDownlink.mat'));
            
            t = timeDownlink_d + testCase.initTime_d;
            n = numel(t);
            actIsDownlinking = false(n,1);
            for ii = 1:n
                testCase.dd = testCase.dd.step(XDownlink(ii, 2), t(ii)); %#ok<NODEF>
                actIsDownlinking(ii) = testCase.dd.isDownlinking;
            end
            
            testCase.verifyEqual(actIsDownlinking, logical(XDownlink(:,6))); 
        end
        
        function step_rampUp_notDownlinking(testCase)
            
            load(fullfile('tests', 'unitTests', 'helperFiles', 'downlinking_rampUp.mat'));
            
            t = timeRampUp_d + testCase.initTime_d;
            n = numel(t);
            actIsDownlinking = false(n,1);
            for ii = 1:n
                testCase.dd = testCase.dd.step(XRampUp(ii, 2), t(ii), XRampUp(ii, 1)); 
                actIsDownlinking(ii) = testCase.dd.isDownlinking;
            end
            
            testCase.verifyFalse(any(actIsDownlinking));
            
%             figure;
%             ax(1) = subplot(311);
%             plot(t, XRampUp(:, 1));
%             datetick('x');
%             ax(2) = subplot(312);
%             plot(t, XRampUp(:, 2));
%             datetick('x');
%             ax(3) = subplot(313);
%             plot(t, [actIsDownlinking, logical(XRampUp(:,6))]);
%             datetick('x');
%             linkaxes(ax, 'x');
%             zoom on;
        end
        
        function step_rampDown_notDownlinking(testCase)
            
            load(fullfile('tests', 'unitTests', 'helperFiles', 'downlinking_rampDown.mat'));
            
            t = timeRampDown_d + testCase.initTime_d;
            n = numel(t);
            actIsDownlinking = false(n,1);
            for ii = 1:n
                testCase.dd = testCase.dd.step(XRampDown(ii, 2), t(ii), XRampDown(ii, 1)); 
                actIsDownlinking(ii) = testCase.dd.isDownlinking;
            end
            
            testCase.verifyFalse(any(actIsDownlinking));
%             
%             figure;
%             ax(1) = subplot(311);
%             plot(t, XRampDown(:, 1));
%             datetick('x');
%             ax(2) = subplot(312);
%             plot(t, XRampDown(:, 2));
%             datetick('x');
%             ax(3) = subplot(313);
%             plot(t, [actIsDownlinking, logical(XRampDown(:,6))]);
%             datetick('x');
%             linkaxes(ax, 'x');
%             zoom on;
        end
        
    end
end