classdef hybridKalmanFilterTest < matlab.unittest.TestCase
    %hybridKalmanFilterTest unit tests of the HYBRID_KALMAN_FILTER class with
    %methods
    
    properties
        hkf
    end
    
    properties (Constant)
        measCovariance = 50;
        PInit = 50;
        xInit = 0;
        initTime_d = datenum(2017);
    end
    
    methods (TestMethodSetup)
        function initBeforeEachTest(testCase)
            testCase.hkf = HYBRID_KALMAN_FILTER();
            testCase.addTeardown(@clear, 'testCase.hkf');
        end
    end
    
    methods (Test)
        function update_measFlowOutZero_sleepModeActiveTrue(testCase)
            dt_s = 2;
            t_d = testCase.initTime_d + (1:dt_s);
            
            testCase.hkf = testCase.hkf.initialize(testCase.measCovariance,...
                testCase.xInit, testCase.PInit);
            
            flowIn_lpm = 0;
            flowOut_lpm = 0;
            depthBitAndHole_m = 2000;
            for ii = 1:numel(t_d)
                testCase.hkf = testCase.hkf.addFlowAndDepthMeas(t_d(ii), flowOut_lpm,...
                    depthBitAndHole_m, depthBitAndHole_m);
                testCase.hkf = testCase.hkf.update(flowIn_lpm, flowOut_lpm, dt_s);
                testCase.verifyTrue(testCase.hkf.sleepModeActive);
            end
        end
        
        function update_measFlowOutZeroAndFlowInPositive_flowOutPredZero(testCase)
            dt_s = 2;
            t_d = testCase.initTime_d + (1:dt_s);
            
            testCase.hkf = testCase.hkf.initialize(testCase.measCovariance,...
                testCase.xInit, testCase.PInit);
            
            flowIn_lpm = 2000;
            flowOut_lpm = 0;
            depthBitAndHole_m = 2000;
            for ii = 1:numel(t_d)
                testCase.hkf = testCase.hkf.addFlowAndDepthMeas(t_d(ii), flowOut_lpm,...
                    depthBitAndHole_m, depthBitAndHole_m);
                testCase.hkf = testCase.hkf.update(flowIn_lpm, flowOut_lpm, dt_s);
                testCase.verifyEqual(testCase.hkf.xK, 0);
            end
        end
        
        function update_measFlowOutPositiveAndFlowInPositive_sleepModeNotActive(testCase)
            dt_s = 2;
            t_d = testCase.initTime_d + (1:dt_s);
            
            testCase.hkf = testCase.hkf.initialize(testCase.measCovariance,...
                testCase.xInit, testCase.PInit);
            
            flowIn_lpm = 2000;
            flowOut_lpm = 2000;
            depthBitAndHole_m = 2000;
            for ii = 1:numel(t_d)
                testCase.hkf = testCase.hkf.addFlowAndDepthMeas(t_d(ii), flowOut_lpm,...
                    depthBitAndHole_m, depthBitAndHole_m);
                testCase.hkf = testCase.hkf.update(flowIn_lpm, flowOut_lpm, dt_s);
                testCase.verifyFalse(testCase.hkf.sleepModeActive);
            end
        end
        
        function update_longPeriodWOFlowOut_sleepModeActive(testCase)
            % in case the return flow has been positive for a while, but
            % then is zero for a long period, e.g. due to tripping, then
            % the filter should go into sleep mode again.
            
            dt_s = 2;
            t_d = testCase.initTime_d + (1:dt_s:testCase.hkf.nBuffer*dt_s + 5);
            
            testCase.hkf = testCase.hkf.initialize(testCase.measCovariance,...
                testCase.xInit, testCase.PInit);
            
            flowIn_lpm = 2000;
            flowOut_lpm = 2000;
            
            depthBitAndHole_m = 2000;
            for ii = 1:2
                testCase.hkf = testCase.hkf.addFlowAndDepthMeas(t_d(ii), flowOut_lpm,...
                    depthBitAndHole_m, depthBitAndHole_m);
                testCase.hkf = testCase.hkf.update(flowIn_lpm, flowOut_lpm, dt_s);
                testCase.assertFalse(testCase.hkf.sleepModeActive);
            end
            
            flowOut_lpm = 0;
            for ii = 3:testCase.hkf.nBuffer + 3
                testCase.hkf = testCase.hkf.addFlowAndDepthMeas(t_d(ii), flowOut_lpm,...
                    depthBitAndHole_m, depthBitAndHole_m);
                testCase.hkf = testCase.hkf.update(flowIn_lpm, flowOut_lpm, dt_s);
            end
            testCase.verifyTrue(testCase.hkf.sleepModeActive);
        end
        
        function checkForLossOfReturnFlow_flowGTZero_returnFalse(testCase)
            dt_s = 2;
            duration_s = 6;
            t_d = testCase.initTime_d + (1:dt_s:duration_s)/86400;
            
            testCase.hkf = testCase.hkf.initialize(testCase.measCovariance,...
                testCase.xInit, testCase.PInit);
            
            flowOut_lpm = 2000;
            for ii = 1:numel(t_d)
                testCase.hkf = testCase.hkf.checkForLossOfReturnFlow(flowOut_lpm, dt_s);
            end
            
            testCase.verifyFalse(testCase.hkf.totalLossOfReturnFlowRate_boolean)
        end
        
        function checkForLossOfReturnFlow_flowZero_returnTrue(testCase)
            dt_s = 2;
            duration_s = 30;
            t_d = testCase.initTime_d + (0:dt_s:duration_s)/86400;
            
            testCase.hkf = testCase.hkf.initialize(testCase.measCovariance,...
                testCase.xInit, testCase.PInit);
            
            flowOut_lpm = 2000;
            for ii = 1:numel(t_d)
                testCase.hkf = testCase.hkf.checkForLossOfReturnFlow(flowOut_lpm, dt_s);
                if t_d(ii) > t_d(1) + 6/86400
                    flowOut_lpm = 0;
                end
            end
            
            testCase.verifyTrue(testCase.hkf.totalLossOfReturnFlowRate_boolean)
        end
        
        function zeroReturnFlowTimer_returnFlowZero_accumulateTime(testCase)
            dt_s = 2;
            duration_s = 6;
            t_d = testCase.initTime_d + (1:dt_s:duration_s)/86400;
            
            testCase.hkf = testCase.hkf.initialize(testCase.measCovariance,...
                testCase.xInit, testCase.PInit);
            
            flowOut_lpm = 0;
            for ii = 1:numel(t_d)
                testCase.hkf = testCase.hkf.zeroReturnFlowTimer(flowOut_lpm, dt_s);
            end
            
            testCase.verifyEqual(testCase.hkf.timeDurationWithZeroReturnFlow_s, duration_s)
        end
        
        function zeroReturnFlowTimer_returnFlowGTZero_accumulatedTimeZero(testCase)
            dt_s = 2;
            duration_s = 8;
            t_d = testCase.initTime_d + (0:dt_s:duration_s)/86400;
            
            testCase.hkf = testCase.hkf.initialize(testCase.measCovariance,...
                testCase.xInit, testCase.PInit);
            
            flowOut_lpm = 0;
            for ii = 1:numel(t_d)
                testCase.hkf = testCase.hkf.zeroReturnFlowTimer(flowOut_lpm, dt_s);
                if t_d(ii) > t_d(1)
                    flowOut_lpm = 3000;
                end
            end
            
            testCase.verifyEqual(testCase.hkf.timeDurationWithZeroReturnFlow_s, 0)
        end
    end
    
end

