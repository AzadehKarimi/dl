classdef piecewiselinfitTest < matlab.unittest.TestCase
    %testing the piecewise linear fit algorithm
       
    properties
        xVec    % input signal
        yVec    % output signal
        Pwlf    % Piecewise linear fit signal produced by algorithm to be tested
        Nfilt   % filter time constant
        dt_s    % (nominal) time step in time series
        t_s     % time
        N       % number of elements in test-dataset
        dobj    % object subject to test
        testEx  % test
        par
        filtIn;
        filtOut;
        drift;
        InMinMax;
        OutMinMax;
        InBufferObj;
        OutBufferObj;
        InMaxMinThreshold;
        OutMaxMinThreshold;
    end
    
    methods (TestClassSetup)
        function initFunc(testCase)
            testCase.testEx  = 1;%1=quadratic, 2=other
            
            tend_s           = 1400;
            testCase.dt_s    = 3;%Time step in simulation
            testCase.t_s     = testCase.dt_s:testCase.dt_s:tend_s;
            testCase.N       = length(testCase.t_s);
            switch testCase.testEx
                case 1 %sinus
                    amp             = 4000;
                    tper_s          = 200;
                    testCase.xVec   = amp*(sin(2*pi*testCase.t_s/tper_s).^2)+50*randn(1,testCase.N);
                    testCase.par    = [7.37 -0.0002 9e-6];
                    testCase.yVec   = testCase.par(1) + testCase.par(2)*testCase.xVec + testCase.par(3)*testCase.xVec.^2 +1*randn(1,testCase.N);                    
                 case 2 %ramp up & down x2
                    testCase.InBufferObj    = RINGBUFFER_OBJ(100);
                    testCase.OutBufferObj   = RINGBUFFER_OBJ(100);

                    qdrill          = 2000;
                    testCase.drift  = zeros(1,testCase.N);
                    testCase.InMinMax  = zeros(1,testCase.N);                    
                    testCase.OutMinMax = zeros(1,testCase.N);                    
                    testCase.par    = [7.37 -0.0004 25e-6];
                    tramp           = 30;%Ramp up/down time
                    t0              = 60;%Wait at zero flow
                    t1              = 30;%Wait at low flow
                    t2              = 150;%Wait at high flow
                    tvec(1)         = t0;
                    tvec(2)         = tvec(1)  + tramp/4;
                    tvec(3)         = tvec(2)  + t1;
                    tvec(4)         = tvec(3)  + tramp*3/4;
                    tvec(5)         = tvec(4)  + t2;
                    tvec(6)         = tvec(5)  + tramp;
                    tvec(7)         = tvec(6)  + t0;
                    tvec(8)         = tvec(7)  + tramp/4;
                    tvec(9)         = tvec(8)  + t1;
                    tvec(10)        = tvec(9)  + tramp*3/4;
                    tvec(11)        = tvec(10) + t2;
                    tvec(12)        = tvec(11) + t2;
                    tvec(13)        = tvec(12) + t2;                    
                    tvec(14)        = tvec(13) + tramp;
                    tvec(15)        = tvec(14) + t0;
                    tvec(16)        = tvec(15) + tramp/4;
                    tvec(17)        = tvec(16) + t1;
                    tvec(18)        = tvec(17)  + tramp*3/4;
                    tvec(19)        = tvec(18) + t2;
                    tvec(20)        = tvec(19) + t2;
                    tvec(21)        = tvec(20) + t2;                    
                    
                    for i = 1:testCase.N
                        testCase.drift(i) = testCase.drift(max(1,i-1)) + 0.1*testCase.dt_s*randn(1,1);
                        if testCase.t_s(i) < tvec(1)
                            qin = 0;
                        elseif testCase.t_s(i) < tvec(2)
                            qin = qdrill*(testCase.t_s(i) - tvec(1))/tramp;
                        elseif testCase.t_s(i) < tvec(3)
                            qin = qdrill/4;
                        elseif testCase.t_s(i) < tvec(4)
                            qin = qdrill/4 + qdrill*(testCase.t_s(i) - tvec(3))/tramp;
                        elseif testCase.t_s(i) < tvec(5)
                            qin = qdrill;
                        elseif testCase.t_s(i) < tvec(6)
                            qin = qdrill - qdrill*(testCase.t_s(i) - tvec(5))/tramp;
                        elseif testCase.t_s(i) < tvec(7)
                            qin = 0;
                        elseif testCase.t_s(i) < tvec(8)
                            qin = qdrill*(testCase.t_s(i) - tvec(7))/tramp;
                        elseif testCase.t_s(i) < tvec(9)
                            qin = qdrill/4;
                        elseif testCase.t_s(i) < tvec(10)
                            qin = qdrill/4 + qdrill*(testCase.t_s(i) - tvec(9))/tramp;
                        elseif testCase.t_s(i) < tvec(11)
                            qin = qdrill;
                        elseif testCase.t_s(i) < tvec(12)
                            qin = qdrill*1.2;
                        elseif testCase.t_s(i) < tvec(13)
                            qin = qdrill;
                        elseif testCase.t_s(i) < tvec(14)
                            qin = qdrill - qdrill*(testCase.t_s(i) - tvec(13))/tramp;
                        elseif testCase.t_s(i) < tvec(15)
                            qin = 0;
                        elseif testCase.t_s(i) < tvec(16)
                            qin = qdrill*(testCase.t_s(i) - tvec(15))/tramp;
                        elseif testCase.t_s(i) < tvec(17)
                            qin = qdrill/4;
                        elseif testCase.t_s(i) < tvec(18)
                            qin = qdrill/4 + qdrill*(testCase.t_s(i) - tvec(17))/tramp;
                        elseif testCase.t_s(i) < tvec(19)
                            qin = qdrill;
                        elseif testCase.t_s(i) < tvec(20)
                            qin = qdrill*1.2;
                        else
                            qin = qdrill;
                        end
                        testCase.xVec(i) = qin;
                    end
                    testCase.xVec    = max(0,testCase.xVec + 20*randn(1,testCase.N));
                    testCase.yVec    = testCase.par(1) + testCase.par(2)*testCase.xVec + testCase.par(3)*testCase.xVec.^2 +0.5*randn(1,testCase.N) + testCase.drift;                    
            end
            figure(1)
            subplot(311)
            plot(testCase.t_s,testCase.xVec,'LineWidth',2);
            grid
            title(['Input time series']);
            xlabel('Time')
            subplot(312)
            plot(testCase.t_s,testCase.yVec,'LineWidth',2);
            grid
            title('Output time series')
            xlabel('Time')
            subplot(313)
            plot(testCase.xVec,testCase.yVec,'*','LineWidth',2);
            grid
            title('Output vs input')
            xlabel('In signal')
            ylabel('Out signal')
        end
    end
    
    methods (TestMethodSetup)
        function initBeforeEachTest(testCase)
            testCase.dobj = PIECEWISELINFIT_OBJ(6000, 0, 60, 50); % 10 init with data vector
            testCase.Pwlf = zeros(1,testCase.N);
            testCase.addTeardown(@clear, 'testCase.dobj');
        end
    end
       
    methods (Test)
        function piecewiselinfit_correctInit_correctOutput(testCase)
            import matlab.unittest.constraints.IsLessThan
            import matlab.unittest.constraints.IsEqualTo
            import matlab.unittest.constraints.RelativeTolerance
            import matlab.unittest.constraints.AbsoluteTolerance
            numTol = 0.10; % relative error which is tolerated
            testCase.InMaxMinThreshold   = 100;
            testCase.OutMaxMinThreshold  = 10;

            if testCase.testEx  == 1
                startindex = 30;
                for ii = 1:testCase.N
                    [testCase.dobj, testCase.Pwlf(ii)] = testCase.dobj.interpol(testCase.xVec(ii));
                    testCase.dobj = testCase.dobj.train(testCase.xVec(ii), testCase.yVec(ii));
                end
            else
                startindex       = 200/2;
                NvaluesforSteady = 5;
                for ii = 1:testCase.N

                    [testCase.dobj, testCase.Pwlf(ii)] = testCase.dobj.interpol(testCase.xVec(ii));
                    if testCase.Pwlf(ii) == -0.99925
                        testCase.Pwlf(ii) = testCase.yVec(ii);
                    end
                    timeNew_d = 0*round(now)+testCase.t_s(ii)/86400;
                    testCase.InBufferObj   = testCase.InBufferObj.addDataToCircularBuffer( timeNew_d, testCase.xVec(ii));
                    testCase.OutBufferObj  = testCase.OutBufferObj.addDataToCircularBuffer(timeNew_d, testCase.yVec(ii));
                    testCase.InMinMax(ii)  = max(testCase.InBufferObj.getLastNValues( NvaluesforSteady))-min(testCase.InBufferObj.getLastNValues( NvaluesforSteady));
                    testCase.OutMinMax(ii) = max(testCase.OutBufferObj.getLastNValues(NvaluesforSteady))-min(testCase.OutBufferObj.getLastNValues(NvaluesforSteady));
                    isSteadyIn             = testCase.InMinMax(ii)  < testCase.InMaxMinThreshold;
                    isSteadyOut            = testCase.OutMinMax(ii) < testCase.OutMaxMinThreshold;

                    if isSteadyIn && isSteadyOut
                        testCase.dobj = testCase.dobj.train(testCase.xVec(ii), testCase.yVec(ii));
                    end                    
                end
                testIn  = 1:3000;
                testOut = zeros(3000,1);
                for ii = 1:3000
                    [testCase.dobj, testOut(ii)] = testCase.dobj.interpol(testIn(ii));
                end
                testTrueOut    = testCase.par(1) + testCase.par(2)*testIn + testCase.par(3)*testIn.^2;                    
            end
            rangeTrue  = max(testCase.yVec) - min(testCase.yVec);
            Err        = testCase.yVec-testCase.Pwlf;
            trueOut    = testCase.par(1) + testCase.par(2)*testCase.dobj.inputArr + testCase.par(3)*testCase.dobj.inputArr.^2;                    

            % verify with a numerical tolerance
            figure(2)
            subplot(411)
            plot(testCase.t_s,testCase.xVec,'LineWidth',2);
            grid
            legend('In signal')
            xlabel('Time')
            subplot(412)
            plot(testCase.t_s,testCase.yVec, testCase.t_s, testCase.Pwlf,'LineWidth',2);
            grid
            legend('True out signal','Expected out signal')
            xlabel('Time')
            subplot(413)
%            plot(testCase.t_s(startindex:testCase.N),ErrSPpres(startindex:testCase.N),'LineWidth',2);
            plot(testCase.t_s,Err,'LineWidth',2);
            grid
            legend('Error = True - Expected')
            xlabel('Time')
            subplot(414)
            plot(testCase.xVec,testCase.yVec,'x',testCase.dobj.inputArr,testCase.dobj.outputArr,'*',testCase.dobj.inputArr,trueOut,'+','LineWidth',2);
            grid
            title('Output vs input')
            xlabel('In signal')
            ylabel('Out signal')
            
            figure(3)
            subplot(311)
            plot(testIn,testOut,testIn,testTrueOut,testCase.dobj.inputArr,testCase.dobj.outputArr,'*','LineWidth',2);
            grid
            legend('Out signal','True Out')
            xlabel('In')
            ylabel('Out')
            subplot(312)
            plot(testCase.t_s,testCase.InMinMax,testCase.t_s,ones(testCase.N)*testCase.InMaxMinThreshold,'r--','LineWidth',2);
            grid
            legend('In max-min','Threshold')
            xlabel('Time')
            subplot(313)
            plot(testCase.t_s,testCase.OutMinMax,testCase.t_s,ones(testCase.N)*testCase.OutMaxMinThreshold,'r--','LineWidth',2);
            grid
            legend('Out max-min','Threshold')
            xlabel('Time')
            

            if testCase.testEx  == 1
                testCase.verifyThat(mean(testCase.Pwlf(startindex:testCase.N)),...
                    IsEqualTo(mean(testCase.yVec(startindex:testCase.N)),...
                    'Within', AbsoluteTolerance(numTol*rangeTrue)), 'Mean value should be the same');
                testCase.verifyThat(var(testCase.Pwlf(startindex:testCase.N)),...
                    IsEqualTo(var(testCase.yVec(startindex:testCase.N)),...
                    'Within', RelativeTolerance(numTol)), 'Variance value should be the same');                   
                testCase.verifyThat(var(testCase.yVec(startindex:testCase.N) - testCase.Pwlf(startindex:testCase.N)),...
                    IsEqualTo(0,...
                    'Within', AbsoluteTolerance(numTol*rangeTrue)), 'Error std should be small');
            else         
                m1 = mean(testCase.Pwlf(startindex:testCase.N));
                m2 = mean(testCase.yVec(startindex:testCase.N));
                tol = numTol*rangeTrue;
                disp(['Mean test: trained = ' num2str(m1,'%.1f') ' vs true = ' num2str(m2,'%.1f') '. Tol = ' num2str(tol,'%.1f')]);
                v1 = var(testCase.Pwlf(startindex:testCase.N));
                v2 = var(testCase.yVec(startindex:testCase.N));
                disp(['Relative variance test: trained = ' num2str(v1,'%.1f') ' vs true = ' num2str(v2,'%.1f') '. Compare ' num2str(abs(v1-v2)/v1,'%.4f') ' with Tol = ' num2str(numTol,'%.4f')]);
                s = sqrt(var(testCase.yVec(startindex:testCase.N)- testCase.Pwlf(startindex:testCase.N)));
                disp(['Std error test: error std = ' num2str(s,'%.1f') ' vs tol = ' num2str(tol,'%.1f')]);
                testCase.verifyThat(mean(testCase.Pwlf(startindex:testCase.N)),...
                    IsEqualTo(mean(testCase.yVec(startindex:testCase.N)),...
                    'Within', AbsoluteTolerance(numTol*rangeTrue)), 'Mean value should be the same');
                testCase.verifyThat(var(testCase.Pwlf(startindex:testCase.N)),...
                    IsEqualTo(var(testCase.yVec(startindex:testCase.N)),...
                    'Within', RelativeTolerance(numTol)), 'Variance value should be the same');                   
                testCase.verifyThat(sqrt(var(testCase.yVec(startindex:testCase.N) - testCase.Pwlf(startindex:testCase.N))),...
                    IsEqualTo(0,...
                    'Within', AbsoluteTolerance(numTol*rangeTrue)), 'Error std should be small');
            end
        end
    end    
end

