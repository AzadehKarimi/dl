classdef paddleConverterObjTest < matlab.unittest.TestCase
    
    properties
        paddleConverterObj;
        flowIn_lpm;
        flowOut_lpm;
        
    end
    
    methods (TestClassSetup)
        function initFunc(testCase)            
            testCase.flowIn_lpm;
            testCase.flowOut_lpm;
         end
    end

    
    methods (TestMethodSetup)
        function clearFixture(testCase)
             testCase.paddleConverterObj = PADDLE_CONVERTER_OBJ();
             testCase.addTeardown(@clear, 'testCase.paddleConverterObjTest');
        end
    end
    
    
    
    methods (Test)
         function verifySetInjection(testCase)
              %testCase.paddleConverterObj = setInjection(testCase.paddleConverterObj);
              testCase.verifyEqual(testCase.paddleConverterObj.flowIn_lpm, 0);         
         end
         
         
         
         function verifyConvertStateTransition(testCase)
%              processData = struct();
%              algData     = struct();
%              sysState    = struct();
%              algData.flowOutExpected_lpm = 2000;
%              algData.isDownlinking = false;
%              algData.isCalibratedExpFlowOut = true;
%              algData.isCalibratedMeasFlowOut = true;
%              algData.resetVolIOnow = false;
%              processData.flowIn_lpm = 2000;
%              processData.flowOut_lpm = 2101;
%              processData.flowOutFiltered_lpm = 2150;
%              processData.depthHole_m = 5000;
%              processData.depthBit_m  = 5000 - 1;
%              sysState.isOnBottom = true;
% 
%              dt_s = 2;
%              testCase.votingObj = updateTime(testCase.votingObj, now, dt_s);
%              testCase.votingObj = calcIOFlow(testCase.votingObj, algData, processData, sysState);
%              testCase.verifyEqual(testCase.votingObj.flowIOflow_lpm, processData.flowOutFiltered_lpm - algData.flowOutExpected_lpm);             
%              testCase.verifyEqual(testCase.votingObj.volIOflow_m3, dt_s*(processData.flowOutFiltered_lpm - algData.flowOutExpected_lpm)/60000);                          
%              
%              processData.flowOutFiltered_lpm = -0.99925;
%              V0 = testCase.votingObj.volIOflow_m3;
%              testCase.votingObj = updateTime(testCase.votingObj, now, dt_s);
%              testCase.votingObj = calcIOFlow(testCase.votingObj, algData, processData, sysState);
%              testCase.verifyEqual(testCase.votingObj.flowIOflow_lpm, processData.flowOut_lpm - algData.flowOutExpected_lpm);             
             testCase.verifyEqual(1, 1);                          
         end


         
             
     end
end
