classdef gainDetectorObjTest < matlab.unittest.TestCase
    
    properties
        gdo % gainDetector object
    end
    
    methods (TestMethodSetup)
        function initBeforeEachTest(testCase)
            testCase.gdo = GAIN_DETECTOR_OBJ;
            testCase.gdo = testCase.gdo.initialize;

            testCase.addTeardown(@clear, 'testCase.gdo');
        end
    end
    
    methods (Test)
        function initialize_correctInit_correctOutput(testCase)
            testCase.verifyEqual(testCase.gdo.ringBufferVolGainLoss.Vbuffer, zeros(1, testCase.gdo.nSamples));
        end
        
        function integrateVolume_addFirstValue_doNotAccVolume(testCase)
            flow_rate_lpm = 1000;
            time_d = datenum(2017, 11, 21, 13, 00, 00);
            testCase.gdo = testCase.gdo.integrateVolume(time_d, flow_rate_lpm);
            
            testCase.verifyEqual(testCase.gdo.volGain_m3, 0);
        end
        
        function integrateVolume_addSecondPositiveValue_doAccVolume(testCase)
            import matlab.unittest.constraints.IsEqualTo
            import matlab.unittest.constraints.RelativeTolerance
            numTol=1e-5; % relative error which is tolerated
            
            volGainLoss_m3 = 0;
            depthBit_m = 1000;
            depthHole_m = 1000;
            flowIn_lpm = 2500;
            volGain_m3PerSec = 0.2/60;
            
            for ii = 1:303
                volGainLoss_m3 = volGainLoss_m3 + volGain_m3PerSec;
                time_d = datenum(2017, 11, 21, 13, 00, ii);
                testCase.gdo = testCase.gdo.step(time_d, volGainLoss_m3, depthBit_m, depthHole_m, flowIn_lpm);
            end
            
            % verify with a numerical tolerance
            testCase.verifyThat(testCase.gdo.volGain_m3,...
                IsEqualTo( volGain_m3PerSec*3,...
                'Within', RelativeTolerance(numTol)), 'The accumulated volume should be correct.');
        end

        function step_negativeAverage_resetGainedVolume_m3(testCase)
            volGainLoss_m3 = 0;
            depthBit_m = 1000;
            depthHole_m = 1000;
            flowIn_lpm = 2500;
            volGain_m3PerSec = 0.2/60;
            
            for ii = 1:310
                volGainLoss_m3 = volGainLoss_m3 + volGain_m3PerSec;
                time_d = datenum(2017, 11, 21, 13, 00, ii);
                testCase.gdo = testCase.gdo.step(time_d, volGainLoss_m3, depthBit_m, depthHole_m, flowIn_lpm);
            end
            testCase.assertGreaterThan(testCase.gdo.volGain_m3, 0);
            
            for ii = 1:310
                volGainLoss_m3 = volGainLoss_m3 - volGain_m3PerSec;
                time_d = datenum(2017, 11, 21, 13, 30, ii);
                testCase.gdo = testCase.gdo.step(time_d, volGainLoss_m3, depthBit_m, depthHole_m, flowIn_lpm);
            end
            testCase.assertLessThan(testCase.gdo.flowBasedOnDerivativeOfGainLoss_lpm, 0)
            
            testCase.verifyEqual(testCase.gdo.volGain_m3, 0);
        end
        
        function step_bitOnBottom_addFlowRate(testCase)
            volGainLoss_m3 = 1;
            flowIn_lpm = 1000;
            depthBit_m = 1000;
            depthHole_m = 1000;
            for ii = 1:700
                time_d = datenum(2017, 11, 21, 13, 00, ii);
                testCase.gdo = testCase.gdo.step(time_d, volGainLoss_m3 + ii, depthBit_m, depthHole_m, flowIn_lpm);
            end
            
            testCase.verifyGreaterThan(testCase.gdo.volGain_m3, 0);
        end
        
        function step_bitOffBottom_doNotAddFlowRate(testCase)
            volGainLoss_m3 = 1;
            depthBit_m = 998;
            depthHole_m = 1000;
            flowIn_lpm = 2500;
            for ii = 1:2
                time_d = datenum(2017, 11, 21, 13, 00, ii);
                testCase.gdo = testCase.gdo.step(time_d, volGainLoss_m3 + ii, depthBit_m, depthHole_m, flowIn_lpm);
            end
            
            testCase.verifyEqual(testCase.gdo.volGain_m3, 0);
        end
        
        function step_timeStepAboveMax_resetVolume(testCase)
            volGainLoss_m3 = 1;
            depthBit_m = 1000;
            depthHole_m = 1000;
            flowIn_lpm = 2500;
            for ii = 1:310
                time_d = datenum(2017, 11, 21, 13, 00, ii);
                testCase.gdo = testCase.gdo.step(time_d, volGainLoss_m3 + ii, depthBit_m, depthHole_m, flowIn_lpm);
            end
            testCase.assertGreaterThan(testCase.gdo.volGain_m3, 0);
            
            time_d = time_d + (testCase.gdo.timeStepMax_s + 1)/86400;
            testCase.gdo = testCase.gdo.step(time_d, volGainLoss_m3, depthBit_m, depthHole_m, flowIn_lpm);
                        
            testCase.verifyEqual(testCase.gdo.volGain_m3, 0);    
        end

        
    end
    
end

