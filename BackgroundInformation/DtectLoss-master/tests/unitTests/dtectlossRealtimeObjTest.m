classdef dtectlossRealtimeObjTest < matlab.unittest.TestCase
    %DTECTLOSSREALTIMEOBJTEST unit tests of the dtectlossRealtime class with
    %methods
    
    properties
        rwdu; % raw data units
        dr; % dtectloss realtime object
        requiredTags = {'flowIn', 'depthBit', 'depthHole'};
        optionalTags = {'flowOut', 'flowOut1', 'presSP', 'presSP1',...
            'volGainLoss', 'volPitDrill', 'volTripTank', 'volTripTank2',...
            'heightHook', 'rpm', 'torque', 'weightOnBit', 'rop'};
        initTime_d = datenum(2017, 06, 05, 12, 30, 30);
    end
    
    methods (TestClassSetup)
        function initFunc(testCase)
            testCase.rwdu=struct(); % raw data units
            testCase.rwdu.flowIn='m3/s';
            testCase.rwdu.flowOut='m3/s';
            testCase.rwdu.flowOutPercent='%';
            testCase.rwdu.rpm='rpm';
            testCase.rwdu.velRop='m/s';
            testCase.rwdu.presSP='kPa';
            testCase.rwdu.depthHole='m';
            testCase.rwdu.depthBit='m';
            testCase.rwdu.heightHook='m';
            testCase.rwdu.volGainLoss='m3';
            testCase.rwdu.tempBHA='K';
            testCase.rwdu.volTripTank='m3';
        end
    end
    
    methods (TestMethodSetup)
        function initBeforeEachTest(testCase)
            testCase.dr=DTECTLOSS_REALTIME_OBJ();
            testCase.addTeardown(@clear, 'testCase.dr');
        end
    end
    
    methods (Test)
        function init_correctInit_returnObj(testCase)
            testCase.dr=testCase.dr.init('VOTING_ALG_OBJ', testCase.rwdu, testCase.initTime_d);
            testCase.verifyClass(testCase.dr, 'DTECTLOSS_REALTIME_OBJ');
        end
        
        function init_emptyUnit_issueWarning(testCase)
            testCase.verifyWarning(@()testCase.dr.init('VOTING_ALG_OBJ', struct(), testCase.initTime_d),...
                'DTECTLOSS_REALTIME_OBJ:missingRawDataUnits');
        end
        
        function init_unitNotAStruct_issueError(testCase)
            testCase.verifyError(@()testCase.dr.init('VOTING_ALG_OBJ', [], testCase.initTime_d),...
                'DTECTLOSS_REALTIME_OBJ:rawDataUnitsNotAStruct');
        end
        
        function init_noInitTime_issueError(testCase)
            testCase.verifyError(@()testCase.dr.init('VOTING_ALG_OBJ', testCase.rwdu),...
                'DTECTLOSS_REALTIME_OBJ:missingInitTime');
        end
        
        function init_emptyInitTime_issueError(testCase)
            testCase.verifyError(@()testCase.dr.init('VOTING_ALG_OBJ', testCase.rwdu, []),...
                'DTECTLOSS_REALTIME_OBJ:missingInitTime');
        end
        
        function init_zeroInitTime_issueError(testCase)
            testCase.verifyError(@()testCase.dr.init('VOTING_ALG_OBJ', testCase.rwdu, 0),...
                'DTECTLOSS_REALTIME_OBJ:missingInitTime');
        end
        
        function init_backtestModeNotGiven_backtestModeFalse(testCase)
            testCase.dr = testCase.dr.init('VOTING_ALG_OBJ', testCase.rwdu, testCase.initTime_d);
            
            testCase.verifyFalse(testCase.dr.getBacktestMode);
        end
        
        function init_backtestModeGiven_backtestModeTrue(testCase)
            testCase.dr = testCase.dr.init('VOTING_ALG_OBJ', testCase.rwdu, testCase.initTime_d, true);
            
            testCase.verifyTrue(testCase.dr.getBacktestMode);
        end
        
        function init_backtestModeGivenAsFalse_backtestModeFalse(testCase)
            testCase.dr = testCase.dr.init('VOTING_ALG_OBJ', testCase.rwdu, testCase.initTime_d, false);
            
            testCase.verifyFalse(testCase.dr.getBacktestMode);
        end
        
        function softreset_backtestModeNotGiven_noError(testCase)
            testCase.dr = testCase.dr.init('VOTING_ALG_OBJ', testCase.rwdu, testCase.initTime_d, false);
            
            testCase.assertFalse(testCase.dr.getBacktestMode);
            
            testCase.dr = testCase.dr.softreset();
        end
        
        function stepAllAlgorithms_allRequiredAndAllOptional_noError(testCase)
            testCase.dr=testCase.dr.init('VOTING_ALG_OBJ', testCase.rwdu, testCase.initTime_d);
            testTags = [testCase.requiredTags testCase.optionalTags];
            for k= 1:1:numel(testTags)
                inputVec.(testTags{k}).t=0;
                inputVec.(testTags{k}).V=0;
            end
            ret = testCase.dr.stepAllAlgorithms(inputVec);
            testCase.verifyEqual(ret.dtectLossOutputStruct.errorCode, 1);
        end
        
        function stepAllAlgorithms_onlyRequiredTags_noError(testCase)
            rdu=struct('flowIn', 'lpm', 'depthBit', 'm', 'depthHole', 'm');
            testCase.dr=testCase.dr.init('VOTING_ALG_OBJ', rdu, testCase.initTime_d);
            testTags = testCase.requiredTags;
            for k= 1:1:numel(testTags)
                inputVec.(testTags{k}).t=0;
                inputVec.(testTags{k}).V=0;
            end
            ret = testCase.dr.stepAllAlgorithms(inputVec);
            testCase.verifyEqual(ret.dtectLossOutputStruct.errorCode, 1);
        end
        
        function stepAllAlgorithms_noTags_returnErrorCode(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            testCase.applyFixture(...
                SuppressedWarningsFixture('DTECTLOSS_REALTIME_OBJ:missingRawDataUnits'));
            
            testCase.dr=testCase.dr.init('VOTING_ALG_OBJ', struct(), testCase.initTime_d);
            ret = testCase.dr.stepAllAlgorithms(struct());
            testCase.verifyTrue(ret.dtectLossOutputStruct.errorCode<0);
        end
        
        function stepAllAlgorithms_oneRequiredTagMissing_returnErrorCode(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            testCase.applyFixture(...
                SuppressedWarningsFixture('PROCESS_OBJ:noConversionInUnitMap'));
            
            for caseIdx = 1:numel(testCase.requiredTags)
                inputTags = testCase.requiredTags([1:caseIdx-1 caseIdx+1:end]);
                rd=struct();
                rdu=struct();
                for k= 1:1:numel(inputTags)
                    rdu.(inputTags{k})='unit';
                    rd.(inputTags{k}).t=0;
                    rd.(inputTags{k}).V=0;
                end
                
                testCase.dr=testCase.dr.init('VOTING_ALG_OBJ', rdu, testCase.initTime_d);
                ret = testCase.dr.stepAllAlgorithms(rd);
                testCase.verifyTrue(ret.dtectLossOutputStruct.errorCode<0 && ret.dtectLossOutputStruct.errorCode>-1000,...
                    ['removed:',testCase.requiredTags{caseIdx} ,' did not give alarm']);
            end
        end
        
        function stepAllAlgotrithms_addNewTagWhileRunning_noError(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            testCase.applyFixture(...
                SuppressedWarningsFixture('DTECTLOSS_REALTIME_OBJ:missingRawDataUnits'));
            
            rawDataNonStdUnits=struct(); % raw data units
            rawDataNonStdUnits.flowIn='m3/s'; %
            rawDataNonStdUnits.flowOut='m3/s';
            rawDataNonStdUnits.flowOutPercent='%';
            rawDataNonStdUnits.rpm='rpm';
            rawDataNonStdUnits.velRop='m/s';
            rawDataNonStdUnits.presSP='kPa';
            rawDataNonStdUnits.depthHole='m';
            rawDataNonStdUnits.depthBit='m';
            rawDataNonStdUnits.volGainLoss='m3';
            rawDataNonStdUnits.volTripTank='m3';
            
            rawData_nonStd=struct();
            rawData_nonStd.flowIn.V=0.15;
            rawData_nonStd.flowIn.t=now;
            rawData_nonStd.flowOut.V=0.15;
            rawData_nonStd.flowOut.t=now;
            rawData_nonStd.flowOutPercent.V=15;
            rawData_nonStd.flowOutPercent.t=now;
            rawData_nonStd.rpm.V=200;
            rawData_nonStd.rpm.t=now;
            rawData_nonStd.velRop.V=0.01;
            rawData_nonStd.velRop.t=now;
            rawData_nonStd.presSP.V=5000;
            rawData_nonStd.presSP.t=now;
            rawData_nonStd.depthHole.V=3000;
            rawData_nonStd.depthHole.t=now;
            rawData_nonStd.depthBit.V=2999;
            rawData_nonStd.depthBit.t=now;
            rawData_nonStd.volGainLoss.V=3;
            rawData_nonStd.volGainLoss.t=now;
            rawData_nonStd.volTripTank.V=2;
            rawData_nonStd.volTripTank.t=now;
            
            testCase.dr=testCase.dr.init('VOTING_ALG_OBJ', rawDataNonStdUnits, testCase.initTime_d);
            testCase.dr = testCase.dr.stepAllAlgorithms(rawData_nonStd);
            
            % now add heightHook as new tag and step all algs again
            rawDataNonStdUnits.heightHook='m';
            rawData_nonStd.heightHook.V=2;
            rawData_nonStd.heightHook.t=now;
            testCase.dr = testCase.dr.updateTagList(rawDataNonStdUnits);
            testCase.dr = testCase.dr.stepAllAlgorithms(rawData_nonStd);
            
            testCase.verifyEqual(testCase.dr.dtectLossOutputStruct.errorCode,  DtectLossRealtimeErrorCodes.AllOk);
        end
        
        function stepAllAlgotrithms_removeTagWhileRunning_noError(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            testCase.applyFixture(...
                SuppressedWarningsFixture('DTECTLOSS_REALTIME_OBJ:missingRawDataUnits'));
            
            rawDataNonStdUnits=struct(); % raw data units
            rawDataNonStdUnits.flowIn='m3/s'; %
            rawDataNonStdUnits.flowOut='m3/s';
            rawDataNonStdUnits.flowOutPercent='%';
            rawDataNonStdUnits.rpm='rpm';
            rawDataNonStdUnits.velRop='m/s';
            rawDataNonStdUnits.presSP='kPa';
            rawDataNonStdUnits.depthHole='m';
            rawDataNonStdUnits.depthBit='m';
            rawDataNonStdUnits.volGainLoss='m3';
            rawDataNonStdUnits.volTripTank='m3';
            
            rawData_nonStd=struct();
            rawData_nonStd.flowIn.V=0.15;
            rawData_nonStd.flowIn.t=now;
            rawData_nonStd.flowOut.V=0.15;
            rawData_nonStd.flowOut.t=now;
            rawData_nonStd.flowOutPercent.V=15;
            rawData_nonStd.flowOutPercent.t=now;
            rawData_nonStd.rpm.V=200;
            rawData_nonStd.rpm.t=now;
            rawData_nonStd.velRop.V=0.01;
            rawData_nonStd.velRop.t=now;
            rawData_nonStd.presSP.V=5000;
            rawData_nonStd.presSP.t=now;
            rawData_nonStd.depthHole.V=3000;
            rawData_nonStd.depthHole.t=now;
            rawData_nonStd.depthBit.V=2999;
            rawData_nonStd.depthBit.t=now;
            rawData_nonStd.volGainLoss.V=3;
            rawData_nonStd.volGainLoss.t=now;
            rawData_nonStd.volTripTank.V=2;
            rawData_nonStd.volTripTank.t=now;
            
            testCase.dr=testCase.dr.init('VOTING_ALG_OBJ', rawDataNonStdUnits, testCase.initTime_d);
            testCase.dr = testCase.dr.stepAllAlgorithms(rawData_nonStd);
            
            % now add heightHook as new tag and step all algs again
            rawDataNonStdUnits=rmfield(rawDataNonStdUnits, 'flowOutPercent');
            rawData_nonStd=rmfield(rawData_nonStd, 'flowOutPercent');
            testCase.dr = testCase.dr.updateTagList(rawDataNonStdUnits);
            testCase.dr = testCase.dr.stepAllAlgorithms(rawData_nonStd);
            
            testCase.verifyEqual(testCase.dr.dtectLossOutputStruct.errorCode,  DtectLossRealtimeErrorCodes.AllOk);
        end
        
        function ignoreCertainVariablesAndCastToDouble_castBoolToDouble(testCase)
            testCase.dr = testCase.dr.init('VOTING_ALG_OBJ', testCase.rwdu, testCase.initTime_d, true);
            
            inputCell = {...
                'isOnBottom_stat', false;
                'isPipeSteady_stat', true;
                'volFlExpected_m3_stat', 5};
            
            expOutputCell = {...
                'isOnBottom_stat', 0.0;
                'isPipeSteady_stat', 1.0;
                'volFlExpected_m3_stat', 5};
            
            actOutputCell = testCase.dr.ignoreCertainVariablesAndCastToDouble(inputCell);
            
            testCase.verifyEqual(actOutputCell, expOutputCell);
        end
        
        function ignoreCertainVariablesAndCastToDouble_ignoreCertainTag(testCase)
            testCase.dr = testCase.dr.init('VOTING_ALG_OBJ', testCase.rwdu, testCase.initTime_d, true);
            
            tagToIgnore = 'biasPaddle_prc';
            
            inputCell = {...
                'isOnBottom_stat', false;
                'isPipeSteady_stat', true;
                'volFlExpected_m3_stat', 5
                tagToIgnore, 8};
            
            expOutputCell = {...
                'isOnBottom_stat', 0.0;
                'isPipeSteady_stat', 1.0;
                'volFlExpected_m3_stat', 5};
            
            actOutputCell = testCase.dr.ignoreCertainVariablesAndCastToDouble(inputCell);
            
            testCase.verifyEqual(actOutputCell, expOutputCell);
        end
    end
    
end
