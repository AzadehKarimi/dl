classdef lowpassfilterTest < matlab.unittest.TestCase
    %testing the lowpass filter algorithm on a sinus
       
    properties
        dataVec % input signal
        lpAlg   % lowpass filter signal produced by algorithm to be tested
        Tfilt_s % filter time constant
        dt_s    % (nominal) time step in time series
        t_s     % time
        N       % number of elements in test-dataset
        dobj    % object subject to test
        testEx  % test
        constant_dt
    end
    
    methods (TestClassSetup)
        function initFunc(testCase)
%            testCase.constant_dt = true;
            testCase.constant_dt = false;
            testCase.testEx  = 1;%1=sinus, 2=ramp
            
            tend_s           = 100;
            testCase.Tfilt_s = 0.5;
            testCase.dt_s    = 0.01;%Time step in simulation
            if testCase.constant_dt,
                testCase.t_s     = testCase.dt_s:testCase.dt_s:tend_s;
            else
                Nt               = floor(tend_s/testCase.dt_s);
                testCase.t_s     = zeros(1,2*Nt);  
                ii               = 1;
                while testCase.t_s(ii) < tend_s,
                    dt_s                = testCase.dt_s*2*rand;
                    if dt_s < 0.2*testCase.dt_s,
                        dt_s = 0;
                    end
                    testCase.t_s(ii+1)  = testCase.t_s(ii) + dt_s;   
                    ii                  = ii + 1;
                end
                testCase.t_s    = testCase.t_s(1:ii-1);
            end
            testCase.N       = length(testCase.t_s);
            switch testCase.testEx,
                 case 1,
                    amp              = 1;
                    tper_s           = 20;
                    testCase.dataVec = amp*sin(2*pi*testCase.t_s/tper_s);
                case 2,
                    rop_mps          = 30/3600;
                    testCase.dataVec = rop_mps*testCase.t_s;
            end
            figure(1)
            subplot(221)
            plot(testCase.t_s,testCase.dataVec);
            grid
            title(['Input time series']);
            ylabel('Amp []')
            subplot(222)
            plot(testCase.t_s(1:testCase.N-1),diff(testCase.t_s));
            grid
            title('Time step')
            xlabel('time')
            ylabel('Time step [sec]')

        end
    end
    
    methods (TestMethodSetup)
        function initBeforeEachTest(testCase)
            testCase.dobj    = LOWPASS_OBJ(testCase.dt_s,testCase.Tfilt_s); % init with data vector
            testCase.lpAlg   = zeros(1,testCase.N);
            testCase.addTeardown(@clear, 'testCase.dobj');
        end
    end
       
    methods (Test)
        function filterForCustomDt_correctInit_correctOutput(testCase)
            import matlab.unittest.constraints.IsLessThan
            import matlab.unittest.constraints.IsEqualTo
            import matlab.unittest.constraints.RelativeTolerance
            import matlab.unittest.constraints.AbsoluteTolerance
            numTol=0.1; % relative error which is tolerated
            rangeTrue = max(testCase.dataVec) - min(testCase.dataVec);
            
            for ii = 1:testCase.N-1,
                if testCase.constant_dt,
                    [testCase.dobj] = testCase.dobj.filter(testCase.dataVec(ii+1), 0);                    
                else
                    dt_s = (testCase.t_s(ii+1) - testCase.t_s(ii));
                    [testCase.dobj] = testCase.dobj.filterForCustomDt(testCase.dataVec(ii+1), dt_s, 0);
                end
                testCase.lpAlg(ii+1) = testCase.dobj.FilteredSignal;
            end
            
            % verify with a numerical tolerance
            startindex  = round(50/testCase.dt_s);
            
            delaytime_s = findDelay (testCase.dataVec,testCase.lpAlg,testCase.dt_s);
            delayindex  = floor(delaytime_s/testCase.dt_s);
            delayTrue   = testCase.dataVec;
            delayTrue(delayindex:testCase.N-1) = delayTrue(1:testCase.N-delayindex);            
            ampfilter   = std(testCase.lpAlg)/std(delayTrue(delayindex:testCase.N-1));           
            subplot(223)
            plot(testCase.t_s,testCase.dataVec,testCase.t_s,testCase.lpAlg, testCase.t_s, delayTrue);
            grid
            legend('True signal','Lowpass filter','Phase shifted true signal')
            ylabel('Amp []')
            subplot(224)
            plot(testCase.t_s,testCase.dataVec-testCase.lpAlg, testCase.t_s, delayTrue-testCase.lpAlg);
            grid
            legend('True signal - Lowpass filter','Phase shifted true signal - Lowpass filter')
            ylabel('Error []')

            if testCase.testEx==1,
                testCase.verifyThat(mean(testCase.lpAlg(startindex:testCase.N)),...
                    IsEqualTo(mean(testCase.dataVec(startindex:testCase.N)),...
                    'Within', AbsoluteTolerance(numTol*rangeTrue)), 'Mean value should be the same');
                testCase.verifyThat(var(testCase.lpAlg(startindex:testCase.N)),...
                    IsEqualTo(var(testCase.dataVec(startindex:testCase.N)),...
                    'Within', RelativeTolerance(numTol)), 'Variance value should be the same');           
                    testCase.verifyThat(delaytime_s,IsLessThan(5), 'Delay should be less than 5 second');
                    testCase.verifyThat(ampfilter, IsEqualTo(1,'Within', RelativeTolerance(numTol)), 'Filter gain should be 1');
                title(['Calculated delay lowpass filter = ' num2str(delaytime_s,'%.2f') ' sec, filter gain = ' num2str(ampfilter,'%.2f')]);
            else
                testCase.verifyThat(mean(testCase.lpAlg(startindex:testCase.N)),...
                    IsEqualTo(mean(testCase.dataVec(startindex:testCase.N)),...
                    'Within', RelativeTolerance(numTol)), 'Mean value should be the same');
                title(['Lowpass filter']);
            end
        end
    end    
end

function delaytime = findDelay(ser1, ser2, dt_s)
    Ns=length(ser1);
    delayindex = 0;
    mindevmean = mean(abs(ser1-ser2));
    L = round(20/dt_s);
    for ii=1:L,
        ts1=ser1(1:Ns-L);
        ts2=ser2(ii:Ns-L+ii-1);
        devmean = mean(abs(ser1(1:Ns-L)-ser2(ii:Ns-L+ii-1)));
        if devmean < mindevmean,
            delayindex = ii;
            mindevmean = devmean;
        end
    end
    delaytime = delayindex*dt_s;
end
