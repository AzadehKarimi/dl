
classdef timeAlignTest < matlab.unittest.TestCase
    %testing the pdiff algorithm in a scenario
    
    properties
    end
    
    methods (TestMethodSetup)
    end
    
    methods (Test)
        function calcDiffSum_twoequalseriesNoShift_zerosum(testCase)
            close all
            T   = 1:1:100;
            s1  = 2:2:200;
            s2  = s1;
            params       = getparams(0);
            timealignobj = TIMEALIGN_OBJ(params.flowout);
            [timealignobj, sumDiff] = timealignobj.calcDiffSum(0,0,T,s1,s2);
            testCase.verifyEqual(sumDiff, 0,'two equal series should have diffsum zero');%value was  9.6910e+03 at run on may 3. 2017
        end
        
        function calcDiffSum_shiftedbyone_nonzerosum(testCase)
            T   = 1:1:100;
            s1  = 1:1:100;
            s2  = 2:1:101;
            params       = getparams(0);
            timealignobj = TIMEALIGN_OBJ(params.flowout);
            [timealignobj, sumDiff] = timealignobj.calcDiffSum(0,0,T,s1,s2);
            testCase.verifyGreaterThan(sumDiff, 0.99,sprintf('the two nonequal series should have diffsum of exactly 1:%f',sumDiff));%value was  9.6910e+03 at run on may 3. 2017
            testCase.verifyLessThan(sumDiff, 1.01,sprintf('the two nonequal series should have diffsum of exactly 1:%f',sumDiff));%value was  9.6910e+03 at run on may 3. 2017
        end
        
        function calcDiffSum_shiftedbyoneandtimedelayadjusted_zerosum(testCase)
            close;
            T   = 1:1:100;
            s2  = 1:1:100;
            s1  = 2:1:101;
            params       = getparams(0);
            timealignobj = TIMEALIGN_OBJ(params.flowout);
            [timealignobj, sumDiff] = timealignobj.calcDiffSum(1,0,T,s1,s2);
            testCase.verifyEqual(sumDiff, 0,sprintf('the sum of differences of the two shifted time-series should be zero when compensated for with time-delay:%f',sumDiff));%value was  9.6910e+03 at run on may 3. 2017
        end
        
        function align_equalseries_zerodelayandtimeconst(testCase)
            close;
            deltaT_d = 2/86400;%0.01;
            T_d(1) = datenum('2000-01-01');
            s1(1) =1;
            s2(1) =1;
            for k=2:1:100
                T_d(k)   = T_d(k-1)+deltaT_d;
                s1(k)  = s1(k-1)+1;
                s2(k)  = s2(k-1)+1;
            end
            params       = getparams(0);
            timealignobj = TIMEALIGN_OBJ(params.flowout);
            [timealignobj, td_samples, Tc_s,td_s] = timealignobj.align(s1,s2,T_d);
            testCase.verifyEqual(Tc_s, 0,sprintf('two equal series should produce zero Tc:%f',Tc_s));
            testCase.verifyEqual(td_samples, 0,sprintf('two equal series should produce zero td_samples:%f',td_samples));
            testCase.verifyEqual(td_s, 0,sprintf('two equal series should produce zero td_s:%f',td_s));
        end
        
        
        function align_shifteseries_correcttimedelayandnotimeconst(testCase)
            close;
            deltaT_d = 3/86400;%0.01;
            T_d(1) = datenum('2000-01-01');
            s1(1) =2;
            s2(1) =1;
            for k=2:1:100
                T_d(k)   = T_d(k-1)+deltaT_d;
                if k<50
                    s1(k)  = s1(k-1)+1;
                    s2(k)  = s2(k-1)+1;
                else
                    s2(k)= s2(k-1);
                    s1(k)= s2(k-1);
                end
            end
            s1(49)=s2(49);
            %              figure
            %              hold on
            %              plot(s1)
            %              plot(s2,'r')
            
            params       = getparams(0);
            timealignobj = TIMEALIGN_OBJ(params.flowout);
            [timealignobj, td_samples, Tc_s,td_s]      = timealignobj.align(s1,s2,T_d);
            testCase.verifyEqual(Tc_s, 0,sprintf('shifted series should produce zero Tc:%f',Tc_s));
            testCase.verifyEqual(td_samples, 1,sprintf('shifted series should produce one td_samples:%f',td_samples));
            testCase.verifyLessThan((td_s-deltaT_d*86400), deltaT_d*86400/100, sprintf('shifted series should produce sec equal to one sample td_s:%f, td_true:%f',td_s,deltaT_d));
        end
        
        function align_biggershiftedseries_correcttimedelayandnotimeconst(testCase)
            close;
            deltaT_d = 2/86400;%0.01;
            offset   = 4;
            T_d(1)   = datenum('2000-01-01');
            s1(1)    = 5;
            s2(1)    = s1(1) - offset;
            for k=2:1:100
                T_d(k) = T_d(k-1)+deltaT_d;
                s1(k)  = s1(k-1)+1;
                s2(k)  = s2(k-1)+1;
            end
            params       = getparams(0);
            timealignobj = TIMEALIGN_OBJ(params.flowout);
            [timealignobj, td_samples, Tc_s, td_s] = timealignobj.align(s1,s2,T_d);
            testCase.verifyEqual(Tc_s, 0, sprintf('test should produce zero Tc:%f',Tc_s));
            testCase.verifyEqual(td_samples, offset,sprintf('shifted time-series should produce one td_samples:%f',td_samples));
            testCase.verifyLessThan((td_s-deltaT_d*86400*offset), deltaT_d*86400/100, sprintf('shifted time-series should produce sec equal to one sample td_s:%f, td_true:%f',td_s,deltaT_d));
        end
        
        function align_lowpassfilteredseries_correcttimeconstandnodelay(testCase)
            deltaT_d   = 2/86400;%0.01;
            T_d(1)     = datenum('2000-01-01');
            s1(1)      = 1;
            s2(1)      = 1;
            TcTrue_s   = 6;%0.03*86400;
            lp         = LOWPASS_OBJ(deltaT_d*86400,TcTrue_s);
            for k=2:1:100
                T_d(k)   = T_d(k-1)+deltaT_d;
                if k<20
                    s1(k) = s1(k-1);
                else
                    s1(k) = 5;
                end
                lp  = lp.filter(s1(k),0);
                s2(k) = lp.FilteredSignal;
            end
            params       = getparams(0);
            timealignobj = TIMEALIGN_OBJ(params.flowout);
            [timealignobj, td_samples, Tc_s,td_s]      = timealignobj.align(s1,s2,T_d);
            testCase.verifyLessThan(Tc_s-TcTrue_s, deltaT_d*0.5, sprintf(' Tc:%f,TcTrue:%f', Tc_s, TcTrue_s));
            testCase.verifyEqual(td_samples, 0, sprintf('two equal series should produce one td_samples:%f',td_samples));
            testCase.verifyEqual(td_s, 0, sprintf('no timedelay in this testcase td_s:%f:%f',td_s));
        end
        
        function align_lowpassfilteredandshiftedseries_bothTcanddelayCorrect(testCase)
            close;
            deltaT_d       = 5/86400;
            T_d(1)         = datenum('2000-01-01');
            TdTrue_samples = 5;
            TcTrue_s       = 7;
            s1(1)          = 10;
            s2(1)          = 10;
            for k= 2:1:TdTrue_samples+1
                s1(k)      = s1(k-1);
                s2(k)      = s2(k-1);
                T_d(k)     = T_d(k-1) + deltaT_d;
            end
            lp         = LOWPASS_OBJ(deltaT_d*86400, TcTrue_s);
            for k=(TdTrue_samples+2):1:200
                T_d(k) = T_d(k-1) + deltaT_d;
                if k < 50
                    s1(k) = s1(k-1);
                else
                    s1(k) = 0;
                end
                lp  = lp.filter(s1(k - TdTrue_samples),0);
                s2(k) = lp.FilteredSignal;
            end
%             figure
%             hold on
%             plot(s1)
%             plot(s2,'r')
            params       = getparams(0);
            timealignobj = TIMEALIGN_OBJ(params.flowout);
            [timealignobj, sum] = timealignobj.calcDiffSum(TdTrue_samples,TcTrue_s,T_d,s1,s2);
            testCase.verifyLessThan(sum, 0.01, 'when using the correct values, obj fun should be zero');
            
            [timealignobj, td_samples,Tc_s,~]      = timealignobj.align(s1,s2,T_d);
            testCase.verifyLessThan(Tc_s-TcTrue_s, deltaT_d*0.3*86400, sprintf('Tc:%f, TcTrue:%f', Tc_s, TcTrue_s));
            testCase.verifyEqual(td_samples, TdTrue_samples,...
                sprintf('two equal series should produce %f samples delay, but actually td_samples: %f',TdTrue_samples, td_samples));
        end
    end
    
end
