classdef flowlineAndPitObjTest < matlab.unittest.TestCase
    %flowlineAndPitObjTest testing of the flow line and pit object.
    
    properties
        x0; % initial state wo time-delay [2x1] vector
        V0Fl;% [m3] drainback from flow line into pits when stopping rig pumps
        kOiFl; % [1/s] time-constant/gain for output injection for flowline
        kOiGL; % [1/s] time-constant/gain for output injection for gain/loss
        timeStep; % [s] duration of time-step
        flowOutSS; % [lpm] steady state flow rate out
        vLow; % [m3] volume in flowline which causes low flow-rate (qLow)
        qLow; % [lpm] flow-rate when volume in flowline is below vLow
        vDrill; % [m3] volume in flowline when drilling
        timeDelay; % [s] time-delay from flowline, through shakers, to pit
        kF; % [1/s]
        k2F; % [s]
        fapObj; % flow line and pit obj
    end
    
    methods (TestClassSetup)
        function initFunc(testCase)
            testCase.V0Fl=4.5;
            testCase.flowOutSS=2340;
            testCase.vLow=1.5;
            testCase.qLow=10;
            testCase.vDrill=testCase.vLow+testCase.V0Fl;
            testCase.x0=[testCase.vDrill; 0; 0];
            testCase.k2F=-0.5;
            testCase.kF=testCase.flowOutSS/60e3*(1-testCase.k2F)/testCase.vDrill;
            testCase.kOiFl=1e-2;
            testCase.kOiGL=1e-2;
            testCase.timeStep=1;
            testCase.timeDelay=15;
        end
    end
    
    methods (TestMethodSetup)
        function clearFixture(testCase)
             testCase.fapObj=FLOWLINE_AND_PIT_OBJ();
             testCase.addTeardown(@clear, 'testCase.fapObj');
        end
    end
    
    methods (Test)
        function constructor_correctInit_correctOutput(testCase)
           
            testCase.verifyEmpty(testCase.fapObj.x, 'Verify empty state');
            testCase.verifyEmpty(testCase.fapObj.volDrainback);
            testCase.verifyEmpty(testCase.fapObj.kOutputInjectionFl);
            testCase.verifyEmpty(testCase.fapObj.kOutputInjectionGL);
            testCase.verifyEmpty(testCase.fapObj.dT);
            testCase.verifyEmpty(testCase.fapObj.prevTimeStamp);
            testCase.verifyEmpty(testCase.fapObj.td);
            testCase.verifyEmpty(testCase.fapObj.filterDer);
            testCase.verifyEmpty(testCase.fapObj.kFlowline);
            testCase.verifyEmpty(testCase.fapObj.k2Flowline);
        end
        
        function initialize_correctInitNoTimeDelay_correctOutput(testCase)
            
            testCase.fapObj=testCase.fapObj.initialize(testCase.x0, testCase.V0Fl,...
                testCase.kOiFl, testCase.kOiGL, testCase.timeStep);
            testCase.verifyEqual(testCase.fapObj.x, testCase.x0);
            testCase.verifyEqual(testCase.fapObj.volDrainback, testCase.V0Fl);
            testCase.verifyEqual(testCase.fapObj.kOutputInjectionFl, testCase.kOiFl);
            testCase.verifyEqual(testCase.fapObj.kOutputInjectionGL, testCase.kOiGL);
            testCase.verifyEqual(testCase.fapObj.dT, testCase.timeStep);
            testCase.verifyEmpty(testCase.fapObj.prevTimeStamp);
            testCase.verifyEqual(testCase.fapObj.td, 0);
        end
        
        function initialize_noTimeStamp_correctOutput(testCase)
            
            testCase.fapObj=testCase.fapObj.initialize(testCase.x0, testCase.V0Fl,...
                testCase.kOiFl, testCase.kOiGL);
            testCase.verifyEqual(testCase.fapObj.x, testCase.x0);
                        testCase.verifyEqual(testCase.fapObj.volDrainback, testCase.V0Fl);
            testCase.verifyEqual(testCase.fapObj.kOutputInjectionFl, testCase.kOiFl);
            testCase.verifyEqual(testCase.fapObj.kOutputInjectionGL, testCase.kOiGL);
            testCase.verifyEmpty(testCase.fapObj.dT);
            testCase.verifyEmpty(testCase.fapObj.prevTimeStamp);
        end
        
        function dyn_noFlowChange_noVolChange(testCase)
            import matlab.unittest.constraints.IsEqualTo
            import matlab.unittest.constraints.AbsoluteTolerance
            numTol=1e-15; % absolute error which is tolerated
            
            
            testCase.fapObj=testCase.fapObj.initialize(testCase.x0, testCase.V0Fl,...
                testCase.kOiFl, testCase.kOiGL, testCase.timeStep);
            testCase.fapObj=testCase.fapObj.updateKFlowline(testCase.kF);
            testCase.fapObj=testCase.fapObj.updateK2Flowline(testCase.k2F);
            qOut_lpm=testCase.flowOutSS;
            qPump_lpm=qOut_lpm;
            volGainLoss_m3=0;
            useOutputInjection=false;
            
            xDotExp=[0;0;0];
            xDotActual=testCase.fapObj.dyn(qOut_lpm, qPump_lpm, volGainLoss_m3, useOutputInjection);
            
            % verify with a numerical tolerance
            testCase.verifyThat(xDotActual,...
                IsEqualTo(xDotExp,...
                'Within', AbsoluteTolerance(numTol)), 'xDot should be zero');
        end
        
        function dyn_noOutputInjection_noVolChange(testCase)
            import matlab.unittest.constraints.IsEqualTo
            import matlab.unittest.constraints.AbsoluteTolerance
            numTol=1e-15; % absolute error which is tolerated
            
            
            testCase.fapObj=testCase.fapObj.initialize(testCase.x0, testCase.V0Fl,...
                testCase.kOiFl, testCase.kOiGL, testCase.timeStep);
            testCase.fapObj=testCase.fapObj.updateKFlowline(testCase.kF);
            testCase.fapObj=testCase.fapObj.updateK2Flowline(testCase.k2F);
            qOut_lpm=testCase.flowOutSS;
            qPump_lpm=qOut_lpm;
            volGainLoss_m3=10;
            useOutputInjection=false;
            
            xDotExp=[0;0;0];
            xDotActual=testCase.fapObj.dyn(qOut_lpm, qPump_lpm, volGainLoss_m3, useOutputInjection);
            
            % verify with a numerical tolerance
            testCase.verifyThat(xDotActual,...
                IsEqualTo(xDotExp,...
                'Within', AbsoluteTolerance(numTol)), 'xDot should be zero');
        end
        
        function updateTimeStep_prevTimeStampNotSet_prevTimeSet(testCase)
            
            testCase.fapObj=testCase.fapObj.initialize(testCase.x0, testCase.V0Fl,...
                testCase.kOiFl, testCase.kOiGL);
            testCase.fapObj=testCase.fapObj.updateKFlowline(testCase.kF);
            testCase.fapObj=testCase.fapObj.updateK2Flowline(testCase.k2F);
            
            qOutExp=struct('t', datenum(1981, 09, 26, 01, 00, 02),...
                'V', 2500);
            qPump=struct('t', datenum(1981, 09, 26, 01, 00, 03),...
                'V', 2510);
            volGL=struct('t', datenum(1981, 09, 26, 01, 00, 01),...
                'V', -1);
            
            testCase.fapObj=testCase.fapObj.updateTimeStep(qOutExp,qPump,volGL);
            
            testCase.verifyEqual(testCase.fapObj.prevTimeStamp, qPump.t, 'Time stamp should be set.');
            testCase.verifyEqual(testCase.fapObj.dT, 0, 'Time-step should be zero.');
        end
        
        function updateTimeStep_negativeDt_noChangeInDt(testCase)
            
            testCase.fapObj=testCase.fapObj.initialize(testCase.x0, testCase.V0Fl,...
                testCase.kOiFl, testCase.kOiGL);
            testCase.fapObj=testCase.fapObj.updateKFlowline(testCase.kF);
            testCase.fapObj=testCase.fapObj.updateK2Flowline(testCase.k2F);
            
            qOutExp=struct('t', datenum(1981, 09, 26, 01, 00, 02),...
                'V', 2500);
            qPump=struct('t', datenum(1981, 09, 26, 01, 00, 03),...
                'V', 2510);
            volGL=struct('t', datenum(1981, 09, 26, 01, 00, 01),...
                'V', -1);
            
            testCase.fapObj=testCase.fapObj.updateTimeStep(qOutExp,qPump,volGL);
            testCase.assertEqual(datestr(testCase.fapObj.prevTimeStamp), datestr(qPump.t), 'Time stamp should be set.');
            testCase.assertEqual(testCase.fapObj.dT, 0, 'Time-step should be zero.');
            
            qOutExpNew=struct('t', datenum(1981, 09, 26, 01, 00, 00),...
                'V', 2500);
            qPumpNew=struct('t', datenum(1981, 09, 26, 01, 00, 00),...
                'V', 2510);
            volGLNew=struct('t', datenum(1981, 09, 26, 01, 00, 01),...
                'V', -1);
            
            testCase.fapObj=testCase.fapObj.updateTimeStep(qOutExpNew,qPumpNew,volGLNew);
            testCase.verifyEqual(datestr(testCase.fapObj.prevTimeStamp), datestr(qPump.t), 'Time stamp should be set as the previous value.');
            testCase.verifyEqual(testCase.fapObj.dT, 0, 'No change in time step');
        end
        
        function updateTimeStep_correctUpdate_changeInDtAndPrevTime(testCase)
            
            testCase.fapObj=testCase.fapObj.initialize(testCase.x0, testCase.V0Fl,...
                testCase.kOiFl, testCase.kOiGL);
            testCase.fapObj=testCase.fapObj.updateKFlowline(testCase.kF);
            testCase.fapObj=testCase.fapObj.updateK2Flowline(testCase.k2F);
            
            qOutExp=struct('t', datenum(1981, 09, 26, 01, 00, 02),...
                'V', 2500);
            qPump=struct('t', datenum(1981, 09, 26, 01, 00, 02),...
                'V', 2510);
            volGL=struct('t', datenum(1981, 09, 26, 01, 00, 01),...
                'V', -1);
            
            testCase.fapObj=testCase.fapObj.updateTimeStep(qOutExp,qPump,volGL);
            testCase.assertEqual(datestr(testCase.fapObj.prevTimeStamp), datestr(qPump.t), 'Time stamp should be set.');
            testCase.assertEqual(testCase.fapObj.dT, 0, 'Time-step should be zero.');
            
            qOutExpNew=struct('t', datenum(1981, 09, 26, 01, 00, 04),...
                'V', 2500);
            qPumpNew=struct('t', datenum(1981, 09, 26, 01, 00, 04),...
                'V', 2510);
            volGLNew=struct('t', datenum(1981, 09, 26, 01, 00, 01),...
                'V', -1);
            
            testCase.fapObj=testCase.fapObj.updateTimeStep(qOutExpNew,qPumpNew,volGLNew);
            testCase.verifyEqual(datestr(testCase.fapObj.prevTimeStamp), datestr(qPumpNew.t), 'Time stamp should be set as the new value.');
            testCase.verifyEqual(testCase.fapObj.dT, 2, 'The time step should be two seconds.');
        end
        
        function stepIt_timeStepAboveMaxDt_reset(testCase)
            
            testCase.fapObj=testCase.fapObj.initialize(testCase.x0, testCase.V0Fl,...
                testCase.kOiFl, testCase.kOiGL);
            testCase.fapObj=testCase.fapObj.updateKFlowline(testCase.kF);
            testCase.fapObj=testCase.fapObj.updateK2Flowline(testCase.k2F);
            
            qOutExp=struct('t', datenum(1981, 09, 26, 01, 00, 02),...
                'V', 2500);
            qPump=struct('t', datenum(1981, 09, 26, 01, 00, 02),...
                'V', 2510);
            volGL=struct('t', datenum(1981, 09, 26, 01, 00, 01),...
                'V', -1);
            
            testCase.fapObj=testCase.fapObj.updateTimeStep(qOutExp,qPump,volGL);
            testCase.assertEqual(datestr(testCase.fapObj.prevTimeStamp), datestr(qPump.t), 'Time stamp should be set.');
            testCase.assertEqual(testCase.fapObj.dT, 0, 'Time-step should be zero.');
            
            qOutExpNew=struct('t', datenum(1981, 09, 26, 01, 00, 04),...
                'V', 2540);
            qPumpNew=struct('t', datenum(1981, 09, 26, 01, 00, 04),...
                'V', 2610);
            volGLNew=struct('t', datenum(1981, 09, 26, 01, 00, 01),...
                'V', -1.5);
            
            testCase.fapObj=testCase.fapObj.stepIt(qOutExpNew,qPumpNew,volGLNew, true);
            testCase.assertEqual(datestr(testCase.fapObj.prevTimeStamp), datestr(qPumpNew.t), 'Time stamp should be set.');
            testCase.assertEqual(testCase.fapObj.dT, 2, 'Time-step should be two seconds.');
            
            qOutExpNew=struct('t', datenum(1981, 09, 26, 01, 02, 04),...
                'V', 2500);
            qPumpNew=struct('t', datenum(1981, 09, 26, 01, 02, 04),...
                'V', 2510);
            volGLNew=struct('t', datenum(1981, 09, 26, 01, 02, 01),...
                'V', -2);
            
            testCase.fapObj=testCase.fapObj.stepIt(qOutExpNew,qPumpNew,volGLNew, true);
            testCase.assertEqual(datestr(testCase.fapObj.prevTimeStamp), datestr(qPumpNew.t), 'Time stamp should be set as the new value.');
            testCase.assertEqual(testCase.fapObj.dT, 120, 'The time step should be 120 seconds.');
            testCase.verifyEqual(testCase.fapObj.x(2), volGLNew.V, 'The estimated gain/loss should be set to the measured value.');
            testCase.verifyEqual(testCase.fapObj.x(3), 0, 'The integral of the gain/loss error should be reset.');
        end
        
        function stepIt_timeStepAboveMaxDtNewGLIsErrVal_resetToPrevGoodVal(testCase)
            
            testCase.fapObj=testCase.fapObj.initialize(testCase.x0, testCase.V0Fl,...
                testCase.kOiFl, testCase.kOiGL);
            testCase.fapObj=testCase.fapObj.updateKFlowline(testCase.kF);
            testCase.fapObj=testCase.fapObj.updateK2Flowline(testCase.k2F);
            
            qOutExp0=struct('t', datenum(1981, 09, 26, 01, 00, 02),...
                'V', 2500);
            qPump0=struct('t', datenum(1981, 09, 26, 01, 00, 02),...
                'V', 2510);
            volGL0=struct('t', datenum(1981, 09, 26, 01, 00, 01),...
                'V', -1);
            
            testCase.fapObj=testCase.fapObj.updateTimeStep(qOutExp0,qPump0,volGL0);
            testCase.assertEqual(datestr(testCase.fapObj.prevTimeStamp), datestr(qPump0.t), 'Time stamp should be set.');
            testCase.assertEqual(testCase.fapObj.dT, 0, 'Time-step should be zero.');
            
            qOutExp1=struct('t', datenum(1981, 09, 26, 01, 00, 04),...
                'V', 2540);
            qPump1=struct('t', datenum(1981, 09, 26, 01, 00, 04),...
                'V', 2610);
            volGL1=struct('t', datenum(1981, 09, 26, 01, 00, 01),...
                'V', -1.5);
            
            testCase.fapObj=testCase.fapObj.stepIt(qOutExp1,qPump1,volGL1, true);
            testCase.assertEqual(datestr(testCase.fapObj.prevTimeStamp), datestr(qPump1.t), 'Time stamp should be set.');
            testCase.assertEqual(testCase.fapObj.dT, 2, 'Time-step should be two seconds.');
            
            qOutExp2=struct('t', datenum(1981, 09, 26, 01, 02, 04),...
                'V', 2500);
            qPump2=struct('t', datenum(1981, 09, 26, 01, 02, 04),...
                'V', 2510);
            volGL2=struct('t', datenum(1981, 09, 26, 01, 02, 01),...
                'V', -0.99925);
            
            testCase.fapObj=testCase.fapObj.stepIt(qOutExp2,qPump2,volGL2, true);
            testCase.assertEqual(datestr(testCase.fapObj.prevTimeStamp), datestr(qPump2.t), 'Time stamp should be set as the new value.');
            testCase.assertEqual(testCase.fapObj.dT, 120, 'The time step should be 120 seconds.');
            testCase.verifyEqual(testCase.fapObj.x(2), volGL1.V, 'The estimated gain/loss should be set to the previous measured value.');
            testCase.verifyEqual(testCase.fapObj.x(3), 0, 'The integral of the gain/loss error should be reset.');
        end
        
        function updateVolDrainback_newV0_V0SetCorrectly(testCase)
            
            testCase.fapObj=testCase.fapObj.initialize(testCase.x0, testCase.V0Fl,...
                testCase.kOiFl, testCase.kOiGL);
            testCase.fapObj=testCase.fapObj.updateKFlowline(testCase.kF);
            testCase.fapObj=testCase.fapObj.updateK2Flowline(testCase.k2F);
            
            negativeV0=-1;
            zeroV0=0;
            unphysicalV0=1000;
            realV0=5.5;
            
            prevV0=testCase.fapObj.volDrainback;
            fapNeg=testCase.fapObj.updateVolDrainback(negativeV0);
            testCase.verifyEqual(fapNeg.volDrainback, prevV0);
            
            fapZero=testCase.fapObj.updateVolDrainback(zeroV0);
            testCase.verifyEqual(fapZero.volDrainback, prevV0);
            
            fapUnphysical=testCase.fapObj.updateVolDrainback(unphysicalV0);
            testCase.verifyEqual(fapUnphysical.volDrainback, prevV0);
            
            fapReal=testCase.fapObj.updateVolDrainback(realV0);
            testCase.verifyEqual(fapReal.volDrainback, realV0);
        end
        
        function updateState_pumpOffNoTimedelay_flowlineVolToPit(testCase)
            % test that when shutting down rig pumps, the volume in the
            % flowline is transferred to the pits.
            
            import matlab.unittest.constraints.IsEqualTo
            import matlab.unittest.constraints.AbsoluteTolerance
            numTol=1e-2; % absolute error which is tolerated
            
            
            testCase.fapObj=testCase.fapObj.initialize(testCase.x0, testCase.V0Fl,...
                testCase.kOiFl, testCase.kOiGL);
            testCase.fapObj = testCase.fapObj.setQuickConvergeNow(false);
            testCase.fapObj=testCase.fapObj.updateKFlowline(testCase.kF);
            testCase.fapObj=testCase.fapObj.updateK2Flowline(testCase.k2F);
            dt=10; 
            t=0:dt:2500; % simulate 1500 seconds
            testCase.fapObj=testCase.fapObj.setTimeStep(dt);
            
            testCase.assertEqual(testCase.fapObj.dT, dt);
            
            for ii=t
                testCase.fapObj=testCase.fapObj.updateState(0, 0, 0, false, 0);
            end
            
            % verify with a numerical tolerance
            testCase.verifyThat(testCase.fapObj.x, IsEqualTo([0; testCase.vDrill; 0],...
                'Within', AbsoluteTolerance(numTol)),'The pit volume should equal the flowback volume');
        end
        
        function updateState_pumpOnNoTimedelay_pitVolToFlowline(testCase)
            % test that when ramping up rig pumps, the volume in the
            % in the pits is transferred to the flowline.
            
            import matlab.unittest.constraints.IsEqualTo
            import matlab.unittest.constraints.AbsoluteTolerance
            numTol=1e-2; % absolute error which is tolerated
            
            testCase.fapObj=testCase.fapObj.initialize([testCase.vLow; testCase.vDrill-testCase.vLow; 0], testCase.V0Fl,...
                testCase.kOiFl, testCase.kOiGL);
            testCase.fapObj = testCase.fapObj.setQuickConvergeNow(false);
            testCase.fapObj=testCase.fapObj.updateKFlowline(testCase.kF);
            testCase.fapObj=testCase.fapObj.updateK2Flowline(testCase.k2F);
            dt=10; 
            t=0:dt:1200; % simulate 600 seconds
            testCase.fapObj=testCase.fapObj.setTimeStep(dt);
            
            testCase.assertEqual(testCase.fapObj.dT, dt);
            
            qOut_lpm=testCase.flowOutSS;
            qPump_lpm=qOut_lpm;
            for ii=t
                testCase.fapObj=testCase.fapObj.updateState(qOut_lpm, qPump_lpm, 0, false, 0);
            end
            
            % verify with a numerical tolerance
            testCase.verifyThat(testCase.fapObj.x, IsEqualTo(testCase.x0,...
                'Within', AbsoluteTolerance(numTol)),'The pit should be empty and volume transferred back to flowline');
        end
        
        %     function updateState_timeDelay
        %     end

    end
end
