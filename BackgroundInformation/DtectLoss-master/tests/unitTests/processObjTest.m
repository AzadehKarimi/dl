classdef processObjTest < matlab.unittest.TestCase
    %PROCESSOBJTEST unit testing of the process object.
    
    properties
        rawDataUnits
        expDefaultDataUnits
        expDefaultDataUnitsFactor
        rawData_nonstandard
        expRawDataStdUnit
        missingValCode
        tuningParams
        proObj
        initTime_d = datenum(2017, 06, 06, 12, 30, 30);
	end
    
    properties (Constant)
        songaMeasTagsWithUnit = {...
            'volGainLoss', 'm3';...
            'volPitDrill', 'm3';...
            'depthBit', 'm';...
            'rpm', 'rpm';...
            'velBlock', 'm/s';...
            'depthHole', 'm';...
            'flowIn', 'm3/s';...
            'flowOut', 'lpm';...
            'flowOut1', 'lpm';...
            'rop', 'm/h';...
            'presSP', 'kPa';...
            'presSP1', 'kPa';...
            'volTripTank', 'm3';...
            };
        songaValidTagsWithUnit = {...
            'validvolPitDrill', 'boolean';...
            'validdepthBit', 'boolean';...
            'validrpm', 'boolean';...
            'validvelBlock', 'boolean';...
            'validdepthHole', 'boolean';...
            'validflowIn', 'boolean';...
            'validflowOut', 'boolean';...
            'validflowOut1', 'boolean';...
            'validrop', 'boolean';...
            'validpresSP', 'boolean';...
            'validpresSP1', 'boolean';...
            'validvolTripTank', 'boolean'};
        songaSelectorTagsWithUnit = {...
            'selectflowout', 'boolean';...
            'selectpresSP', 'boolean';...
            };
        
        songaMeas= {...
            'volGainLoss', 10;...
            'volPitDrill', 20;...
            'depthBit', 2003;...
            'rpm', 180;...
            'velBlock', 0;...
            'depthHole', 2005;...
            'flowIn', 2400/60e3;...
            'flowOut', 2350;...
            'flowOut1', 2330;...
            'rop', 40;...
            'presSP', 30000;...
            'presSP1', 30002';...
            'volTripTank', 2;...
            };
        songaValid = {...
            'validvolPitDrill', 1;...
            'validdepthBit', 1;...
            'validrpm', 1;...
            'validvelBlock', 1;...
            'validdepthHole', 1;...
            'validflowIn', 1;...
            'validflowOut', 1;...
            'validflowOut1', 1;...
            'validrop', 1;...
            'validpresSP', 1;...
            'validpresSP1', 1;...
            'validvolTripTank', 1};
        songaSelector = {...
            'selectflowout', 0;...
            'selectpresSP', 0;...
            };
        
        defaultValues = struct(...
                'volGainLoss_m3', 10,...
                'volPitDrill_m3', 20,...
                'depthBit_m', 2003,...
                'rpm', 180,...
                'velBlock_mps', 0,...
                'depthHole_m', 2005,...
                'flowIn_lpm', 2400,...
                'flowOut_lpm', 2350,...
                'flowOut1_lpm', 2330,...
                'rop_mph', 40,...
                'presSP_bar', 300,...
                'presSP1_bar', 300.0200,...
                'volTripTank_m3', 2,...
                'validvolPitDrill_boolean', 1,...
                'validdepthBit_boolean', 1,...
                'validrpm_boolean', 1,...
                'validvelBlock_boolean', 1,...
                'validdepthHole_boolean', 1,...
                'validflowIn_boolean', 1,...
                'validflowOut_boolean', 1,...
                'validflowOut1_boolean', 1,...
                'validrop_boolean', 1,...
                'validpresSP_boolean', 1,...
                'validpresSP1_boolean', 1,...
                'validvolTripTank_boolean', 1,...
                'selectflowout_boolean', 0,...
                'selectpresSP_boolean', 0);
    end
    
    methods (TestMethodSetup)
        function initFunc(testCase)
            testCase.rawDataUnits=struct('flowIn', 'm3/s', 'depthBit', 'm',...
                'presSP', 'kPa', 'weightOnBit', 'kkgf', 'flowOutPercent', 'Euc',...
                'torque', 'kN.m', 'velRop', 'm/s', 'tempBHA', 'K');
            testCase.expDefaultDataUnits=struct('flowIn', 'lpm', 'depthBit', 'm',...
                'presSP', 'bar', 'weightOnBit', 'kN', 'flowOutPercent', '%',...
                'torque', 'kNm', 'velRop', 'mph', 'tempBHA', 'degC');
            testCase.expDefaultDataUnitsFactor=struct('flowIn', 60000, 'depthBit', 1,...
                'presSP', 0.01, 'weightOnBit', 9.08665, 'flowOutPercent', 100,...
                'torque', 1, 'velRop', 3600, 'tempBHA', 1);
            testCase.rawData_nonstandard=struct('flowIn', 0.1, 'depthBit', 1000,...
                'presSP', 5000, 'weightOnBit', 1, 'flowOutPercent', 20,...
                'torque', 50, 'velRop', 10/3600, 'tempBHA', 300);
            testCase.expRawDataStdUnit=struct('flowIn', 6000, 'depthBit', 1000,...
                'presSP', 50, 'weightOnBit', 9.08665, 'flowOutPercent', 20,...
                'torque', 50, 'velRop', 10, 'tempBHA', 300-273.15);
            testCase.missingValCode=-0.99925;
            testCase.tuningParams=getparams(0);
            
            testCase.proObj=PROCESS_OBJ(testCase.tuningParams, testCase.initTime_d);
            testCase.addTeardown(@clear, 'testCase.proObj');
        end
    end
    
    methods (Test)
        function specifyUnits_correctInit_correctOutput(testCase)
            
            testCase.proObj=testCase.proObj.specifyUnits(testCase.rawDataUnits);
            fN=fieldnames(testCase.proObj.rawDataUnits);
            for ii=1:numel(fN)
                testCase.verifyEqual(testCase.proObj.defaultDataUnits.(fN{ii}),...
                    testCase.expDefaultDataUnits.(fN{ii}),...
                    sprintf('Checking correct mapping to default unit for var %s', fN{ii}));
                testCase.verifyEqual(testCase.proObj.defaultDataUnitsFactor.(fN{ii}),...
                    testCase.expDefaultDataUnitsFactor.(fN{ii}),...
                    sprintf('Checking correct conversion factor for var %s', fN{ii}));
            end
        end
        
        function specifyUnits_varNotInUnitMap_issueWarning(testCase)
            
            rawDataWithMissingVar=struct('flowIn', 'm3/s', 'depthBit', 'm',...
                'presSP', 'kPa', 'weightOnBit', 'kkgf', 'flowOutPercent', 'Euc',...
                'torque', 'kN.m', 'velRop', 'm/s', 'missingVar', 'unitless');
            testCase.verifyWarning(@()testCase.proObj.specifyUnits(rawDataWithMissingVar),...
                'PROCESS_OBJ:variableNotInUnitMap')
        end
        
        function specifyUnits_varWoConvFactor_issueWarning(testCase)
            
            rawDataWithWoConvFactor=struct('flowIn', 'm3/s', 'depthBit', 'm',...
                'presSP', 'kPa', 'weightOnBit', 'kkgf', 'flowOutPercent', 'Euc',...
                'torque', 'kN.m', 'velRop', 'm/s', 'testUnitWoConv', 'unitless');
            testCase.verifyWarning(@()testCase.proObj.specifyUnits(rawDataWithWoConvFactor),...
                'PROCESS_OBJ:noConversionInUnitMap')
        end
        
        function specifyUnits_varMissingUnitInMap_issueWarning(testCase)
            
            rawDataWithMissingUnitInMap=struct('flowIn', 'm3/s', 'depthBit', 'm',...
                'presSP', 'psig', 'weightOnBit', 'kkgf', 'flowOutPercent', 'Euc',...
                'torque', 'kN.m', 'velRop', 'm/s');
            testCase.verifyWarning(@()testCase.proObj.specifyUnits(rawDataWithMissingUnitInMap),...
                'PROCESS_OBJ:noConversionInUnitMap')
        end
        
        function specifyUnits_requiredVarHasUnit_allTrue(testCase)
            
            rwdu=struct(); % raw data units
            rwdu.flowIn='m3/s';
            rwdu.flowOut='m3/s';
            rwdu.flowOutPercent='%';
            rwdu.rpm='rpm';
            rwdu.velRop='m/s';
            rwdu.presSP='kPa';
            rwdu.depthHole='m';
            rwdu.depthBit='m';
            rwdu.heightHook='m';
            rwdu.volGainLoss='m3';
            rwdu.tempBHA='K';
            rwdu.volTripTank='m3';
                        
            testCase.proObj=testCase.proObj.specifyUnits(rwdu);
            
            testCase.verifyTrue(all(testCase.proObj.availableRequiredUnits));
        end
        
        function specifyUnits_requiredVarMissingUnit_returnFalse(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            testCase.applyFixture(...
                SuppressedWarningsFixture('PROCESS_OBJ:noConversionInUnitMap'));
            
            rwdu=struct(); % raw data units
            rwdu.flowIn='wrongUnit';
            rwdu.flowOut='m3/s';
            rwdu.flowOutPercent='%';
            rwdu.rpm='rpm';
            rwdu.velRop='m/s';
            rwdu.presSP='kPa';
            rwdu.depthHole='m';
            rwdu.depthBit='m';
            rwdu.heightHook='m';
            rwdu.volGainLoss='m3';
            rwdu.tempBHA='K';
            rwdu.volTripTank='m3';
                        
            testCase.proObj=testCase.proObj.specifyUnits(rwdu);
            
            expAru=true(size(testCase.proObj.availableRequiredUnits));
            expAru(1)=false;
            
            testCase.verifyEqual(testCase.proObj.availableRequiredUnits, expAru);
        end
        
        function specifyUnits_optionalVarMissingUnit_returnFalse(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            testCase.applyFixture(...
                SuppressedWarningsFixture('PROCESS_OBJ:noConversionInUnitMap'));
            
            rwdu=struct(); % raw data units
            rwdu.flowIn='lpm';
            rwdu.flowOut='m3/s';
            rwdu.flowOutPercent='%';
            rwdu.rpm='rpm';
            rwdu.torque='kNm';
            rwdu.velRop='m/s';
            rwdu.presSP='kPa';
            rwdu.depthHole='m';
            rwdu.depthBit='m';
            rwdu.heightHook='wrongUnit';
            rwdu.volGainLoss='m3';
            rwdu.tempBHA='K';
            rwdu.volTripTank='m3';
                        
            testCase.proObj=testCase.proObj.specifyUnits(rwdu);
            
            expAru=true(size(testCase.proObj.availableOptionalUnits));
            expAru([2 4 6 8 9 12 13]) = false(1, 7);
                        
            testCase.verifyEqual(testCase.proObj.availableOptionalUnits, expAru);
        end
        
        function specifyUnits_songaTags_noWarnings(testCase)
            allTagsWithUnits = [...
                testCase.songaMeasTagsWithUnit;...
                testCase.songaValidTagsWithUnit;...
                testCase.songaSelectorTagsWithUnit];
            
            rwdu = cell2struct(allTagsWithUnits(:,2), allTagsWithUnits(:,1), 1);
            
            testCase.verifyWarningFree(@()testCase.proObj.specifyUnits(rwdu));
        end
        
        function preProcess_emptyInput_emptyOutput(testCase)
            
            rwdu=struct(); % raw data units
            rwdu.flowIn='m3/s';
            rwdu.flowOut='m3/s';
            rwdu.flowOutPercent='%';
            rwdu.rpm='rpm';
            rwdu.velRop='m/s';
            rwdu.presSP='kPa';
            rwdu.depthHole='m';
            rwdu.depthBit='m';
            rwdu.heightHook='m';
            rwdu.volGainLoss='m3';
            rwdu.tempBHA='K';
            rwdu.volTripTank='m3';
            
            testCase.proObj=testCase.proObj.specifyUnits(rwdu);
            testCase.proObj=testCase.proObj.setAvailableTags(rwdu);
            
            rawData_nonStd=struct();
            
            [~, rawDataStdUnit, errorCode, ~] =...
                testCase.proObj.preProcess(rawData_nonStd);
            
            testCase.verifyEmpty(fieldnames(rawDataStdUnit));
            % return with error code indicating that all data is missing.
            testCase.verifyEqual(errorCode, DtectLossRealtimeErrorCodes.MissingAllValues);
        end
        
        
        function preProcess_missingRequiredTag_returnErrorCode(testCase)
            
            rwdu=struct(); % raw data units
            % rwdu.flowIn='m3/s'; % cruicial tag is missing
            rwdu.flowOut='m3/s';
            rwdu.flowOutPercent='%';
            rwdu.rpm='rpm';
            rwdu.velRop='m/s';
            rwdu.presSP='kPa';
            rwdu.depthHole='m';
            rwdu.depthBit='m';
            rwdu.volGainLoss='m3';
            rwdu.volTripTank='m3';
            
            testCase.proObj=testCase.proObj.specifyUnits(rwdu);
            testCase.proObj=testCase.proObj.setAvailableTags(rwdu);
            
            rawData_nonStd=struct();
            rawData_nonStd.flowOut.V=0.15;
            rawData_nonStd.flowOut.t=now;
            rawData_nonStd.flowOutPercent.V=15;
            rawData_nonStd.flowOutPercent.t=now;
            rawData_nonStd.rpm.V=200;
            rawData_nonStd.rpm.t=now;
            rawData_nonStd.velRop.V=0.01;
            rawData_nonStd.velRop.t=now;
            rawData_nonStd.presSP.V=5000;
            rawData_nonStd.presSP.t=now;
            rawData_nonStd.depthHole.V=3000;
            rawData_nonStd.depthHole.t=now;
            rawData_nonStd.depthBit.V=2999;
            rawData_nonStd.depthBit.t=now;
            rawData_nonStd.volGainLoss.V=3;
            rawData_nonStd.volGainLoss.t=now;
            rawData_nonStd.volTripTank.V=2;
            rawData_nonStd.volTripTank.t=now;
            
            [~, ~, errorCode, ~] =...
                testCase.proObj.preProcess(rawData_nonStd);
            
            % return with error code indicating that flowIn is missing
            testCase.verifyEqual(errorCode, DtectLossRealtimeErrorCodes.NoValue_flowIn);
        end
        
        function preProcess_missingRequiredUnit_returnErrorCode(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            testCase.applyFixture(...
                SuppressedWarningsFixture('PROCESS_OBJ:noConversionInUnitMap'));
            
            
            rwdu=struct(); % raw data units
            rwdu.flowIn='wrongUnit'; % tag has unknown unit
            rwdu.flowOut='m3/s';
            rwdu.flowOutPercent='%';
            rwdu.rpm='rpm';
            rwdu.velRop='m/s';
            rwdu.presSP='kPa';
            rwdu.depthHole='m';
            rwdu.depthBit='m';
            rwdu.volGainLoss='m3';
            rwdu.volTripTank='m3';
            
            testCase.proObj=testCase.proObj.specifyUnits(rwdu);
            testCase.proObj=testCase.proObj.setAvailableTags(rwdu);
            
            rawData_nonStd=struct();
            rawData_nonStd.flowOut.V=0.15;
            rawData_nonStd.flowOut.t=now;
            rawData_nonStd.flowOutPercent.V=15;
            rawData_nonStd.flowOutPercent.t=now;
            rawData_nonStd.rpm.V=200;
            rawData_nonStd.rpm.t=now;
            rawData_nonStd.velRop.V=0.01;
            rawData_nonStd.velRop.t=now;
            rawData_nonStd.presSP.V=5000;
            rawData_nonStd.presSP.t=now;
            rawData_nonStd.depthHole.V=3000;
            rawData_nonStd.depthHole.t=now;
            rawData_nonStd.depthBit.V=2999;
            rawData_nonStd.depthBit.t=now;
            rawData_nonStd.volGainLoss.V=3;
            rawData_nonStd.volGainLoss.t=now;
            rawData_nonStd.volTripTank.V=2;
            rawData_nonStd.volTripTank.t=now;
            
            [~, ~, errorCode, ~] =...
                testCase.proObj.preProcess(rawData_nonStd);
            
            % return with error code indicating that flowIn is missing
            testCase.verifyEqual(errorCode, DtectLossRealtimeErrorCodes.NoUnit_flowIn);
        end
        
        function preProcess_noVelRop_missingValCode(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            testCase.applyFixture(...
                SuppressedWarningsFixture('PROCESS_OBJ:noConversionInUnitMap'));
            
            rwdu=struct(); % raw data units
            rwdu.flowIn='lpm';
            rwdu.depthHole='m';
            rwdu.depthBit='m';
            rwdu.volTripTank='m3';
            
            testCase.proObj=testCase.proObj.specifyUnits(rwdu);
            testCase.proObj=testCase.proObj.setAvailableTags(rwdu);
            
            rawData_nonStd=struct();
            rawData_nonStd.flowIn.V=3200;
            rawData_nonStd.flowIn.t=now;
            rawData_nonStd.depthHole.V=3000;
            rawData_nonStd.depthHole.t=now;
            rawData_nonStd.depthBit.V=2999;
            rawData_nonStd.depthBit.t=now;
            rawData_nonStd.volTripTank.V=2;
            rawData_nonStd.volTripTank.t=now;
            
            [~, rdsu, ~, ~] =...
                testCase.proObj.preProcess(rawData_nonStd);
            
            % return with error code indicating that flowIn is missing
            testCase.verifyEqual(rdsu.velRop_mph, testCase.missingValCode);
        end
        
        function preProcess_noFlowOut_addTagAndMissingValCode(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            testCase.applyFixture(...
                SuppressedWarningsFixture('PROCESS_OBJ:noConversionInUnitMap'));
            
            rwdu=struct(); % raw data units
            rwdu.flowIn='lpm';
            rwdu.depthHole='m';
            rwdu.depthBit='m';
            rwdu.volTripTank='m3';
            
            testCase.proObj=testCase.proObj.specifyUnits(rwdu);
            testCase.proObj=testCase.proObj.setAvailableTags(rwdu);
            testCase.proObj=testCase.proObj.defineNameSpace;
            
            rawData_nonStd=struct();
            rawData_nonStd.flowIn.V=3200;
            rawData_nonStd.flowIn.t=now;
            rawData_nonStd.depthHole.V=3000;
            rawData_nonStd.depthHole.t=now;
            rawData_nonStd.depthBit.V=2999;
            rawData_nonStd.depthBit.t=now;
            rawData_nonStd.volTripTank.V=2;
            rawData_nonStd.volTripTank.t=now;
            
            [~, rdsu, ~, ~] =...
                testCase.proObj.preProcess(rawData_nonStd);
            
            % append flowOut_lpm to rdsu, but with error code.
            testCase.verifyEqual(rdsu.flowOut_lpm, testCase.missingValCode);
        end
        
        function preProcess_onlyRequiredTags_returnCompleteNameSpace(testCase)
            % when only the required tags are defined the preProcess
            % function shall still return a rawDataStdUnit struct with the
            % tags defined in required and optional tags. But the optional
            % tags should have the value -0.99925.
            
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            testCase.applyFixture(...
                SuppressedWarningsFixture('PROCESS_OBJ:noConversionInUnitMap'));
            
            rwdu=struct(); % raw data units
            rwdu.flowIn='lpm';
            rwdu.depthHole='m';
            rwdu.depthBit='m';
            rwdu.volTripTank='m3';
            
            testCase.proObj=testCase.proObj.specifyUnits(rwdu);
            testCase.proObj=testCase.proObj.setAvailableTags(rwdu);
            testCase.proObj=testCase.proObj.defineNameSpace;
            
            rawData_nonStd=struct();
            rawData_nonStd.flowIn.V=3200;
            rawData_nonStd.flowIn.t=now;
            rawData_nonStd.depthHole.V=3000;
            rawData_nonStd.depthHole.t=now;
            rawData_nonStd.depthBit.V=2999;
            rawData_nonStd.depthBit.t=now;
            rawData_nonStd.volTripTank.V=2;
            rawData_nonStd.volTripTank.t=now;
            
            expRdsu=struct('flowIn_lpm', 3200, 'depthBit_m', 2999,...
                'depthHole_m', 3000, 'volTripTank_m3', 2,...
                'flowOut_lpm', testCase.missingValCode,...
                'flowOut1_lpm', testCase.missingValCode,...
                'presSP_bar', testCase.missingValCode,...
                'presSP1_bar', testCase.missingValCode,...
                'volGainLoss_m3', testCase.missingValCode,...
                'volPitDrill_m3', testCase.missingValCode,...
                'volTripTank2_m3', testCase.missingValCode,...
                'heightHook_m', testCase.missingValCode,...
                'rpm', testCase.missingValCode, ...
                'torque_kNm', testCase.missingValCode, ...
                'weightOnBit_kN', testCase.missingValCode, ...
                'rop_mph', testCase.missingValCode, ...
                'velRop_mph', testCase.missingValCode,...
                'velBlock_mph', testCase.missingValCode,...
                'velBit_mph', testCase.missingValCode);
            
            [~, rdsu, ~, ~] = testCase.proObj.preProcess(rawData_nonStd);
            
            fNExpRdsu=fieldnames(expRdsu);
            fNRdsu=fieldnames(rdsu);
            
            testCase.verifyEqual(fNRdsu, fNExpRdsu);
            
            % loop 
            for ii=1:numel(expRdsu)
                tag=fNExpRdsu{ii};
                testCase.verifyEqual(rdsu.(tag), expRdsu.(tag));
            end
        end
        
        function songaPreProcess_allSongaMeasTag_correctRawDataSdtUnit(testCase)
            allTagsWithUnits = [...
                testCase.songaMeasTagsWithUnit;...
                testCase.songaValidTagsWithUnit;...
                testCase.songaSelectorTagsWithUnit];
            
            rawDataNonStdCell = [...
                testCase.songaMeas;...
                testCase.songaValid;...
                testCase.songaSelector];
            
            rwdu = cell2struct(allTagsWithUnits(:,2), allTagsWithUnits(:,1), 1);
            rawDataNonStd = processObjTest.makeRawDataStructFromCell(rawDataNonStdCell);
            
            testCase.proObj=testCase.proObj.specifyUnits(rwdu);
            testCase.proObj=testCase.proObj.setAvailableTags(rwdu);
            testCase.proObj=testCase.proObj.defineNameSpace;
            
            testCase.assertTrue(testCase.proObj.runSongaPreProcess);
            
            [~, rawDataStdUnit] =...
                testCase.proObj.songaPreProcess(rawDataNonStd);
            
            % expected values
            expVal = testCase.defaultValues;
            
            expFieldNames = fieldnames(expVal);
            actFieldNames = fieldnames(rawDataStdUnit);
            
            for ii = numel(expFieldNames)
                testCase.verifyEqual(actFieldNames{ii}, expFieldNames{ii});
                testCase.verifyEqual(rawDataStdUnit.(actFieldNames{ii}),...
                    expVal.(expFieldNames{ii}));
            end
        end
        
        function songaWashData_correctInput_correctOutput(testCase)
            
            allTagsWithUnits = [...
                testCase.songaMeasTagsWithUnit;...
                testCase.songaValidTagsWithUnit;...
                testCase.songaSelectorTagsWithUnit];
            
            rwdu = cell2struct(allTagsWithUnits(:,2), allTagsWithUnits(:,1), 1);
                        
            testCase.proObj=testCase.proObj.specifyUnits(rwdu);
            testCase.proObj=testCase.proObj.setAvailableTags(rwdu);
            testCase.proObj=testCase.proObj.defineNameSpace;
            
            testCase.assertTrue(testCase.proObj.runSongaPreProcess);
            
            rawDataStdUnit = testCase.defaultValues;
            rawDataStdUnit.validdepthHole_boolean = 0; % this is set to wrong value
            
            washedData = testCase.proObj.songaWashData(rawDataStdUnit);
            
            fn = fieldnames(rawDataStdUnit);
            nTags = numel(fn);
            
            fnWashedData = fieldnames(washedData);
            nWashedDataTags = numel(fnWashedData);
            
            for ii = 1: nTags
                for jj = 1: nWashedDataTags
                    if strcmp(fnWashedData{jj}, fn{ii})
                        if ~strcmp(fn{ii}, 'depthHole_m')
                            testCase.verifyEqual(washedData.(fnWashedData{jj}), rawDataStdUnit.(fn{ii}));
                        else
                            testCase.verifyEqual(washedData.depthHole_m, testCase.missingValCode);
                        end
                    end
                end
            end
        end
        
        function songaWashData_wrongInput_correctErrorCode(testCase)
            
            allTagsWithUnits = [...
                testCase.songaMeasTagsWithUnit;...
                testCase.songaValidTagsWithUnit;...
                testCase.songaSelectorTagsWithUnit];
            
            rwdu = cell2struct(allTagsWithUnits(:,2), allTagsWithUnits(:,1), 1);
                        
            testCase.proObj=testCase.proObj.specifyUnits(rwdu);
            testCase.proObj=testCase.proObj.setAvailableTags(rwdu);
            testCase.proObj=testCase.proObj.defineNameSpace;
            
            testCase.assertTrue(testCase.proObj.runSongaPreProcess);
            
            rawDataStdUnit = testCase.defaultValues;
            rawDataStdUnit.validdepthHole_boolean = 2; % this is set to wrong value
            
            [~, errorCode] = testCase.proObj.songaWashData(rawDataStdUnit);
            
            testCase.verifyEqual(errorCode, DtectLossRealtimeErrorCodes.BoolAsDoubleDifferentFromZeroOrOne_validdepthHole);
        end
        
        function songaWashData_selectPresSP_correctWashDataStruct(testCase)
            
            allTagsWithUnits = [...
                testCase.songaMeasTagsWithUnit;...
                testCase.songaValidTagsWithUnit;...
                testCase.songaSelectorTagsWithUnit];
            
            rwdu = cell2struct(allTagsWithUnits(:,2), allTagsWithUnits(:,1), 1);
                        
            testCase.proObj=testCase.proObj.specifyUnits(rwdu);
            testCase.proObj=testCase.proObj.setAvailableTags(rwdu);
            testCase.proObj=testCase.proObj.defineNameSpace;
            
            testCase.assertTrue(testCase.proObj.runSongaPreProcess);
            
            rawDataStdUnit = testCase.defaultValues;
                        
            washedData = testCase.proObj.songaWashData(rawDataStdUnit);
            testCase.assertEqual(rawDataStdUnit.selectpresSP_boolean, 0); % presSP should be selected
            testCase.verifyEqual(washedData.presSP_bar, rawDataStdUnit.presSP_bar);
        end
        
        function songaWashData_selectPresSP1_correctWashDataStruct(testCase)
            
            allTagsWithUnits = [...
                testCase.songaMeasTagsWithUnit;...
                testCase.songaValidTagsWithUnit;...
                testCase.songaSelectorTagsWithUnit];
            
            rwdu = cell2struct(allTagsWithUnits(:,2), allTagsWithUnits(:,1), 1);
                        
            testCase.proObj=testCase.proObj.specifyUnits(rwdu);
            testCase.proObj=testCase.proObj.setAvailableTags(rwdu);
            testCase.proObj=testCase.proObj.defineNameSpace;
            
            testCase.assertTrue(testCase.proObj.runSongaPreProcess);
            
            rawDataStdUnit = testCase.defaultValues;
            rawDataStdUnit.selectpresSP_boolean = 1;
                        
            washedData = testCase.proObj.songaWashData(rawDataStdUnit);
            testCase.assertEqual(rawDataStdUnit.selectpresSP_boolean, 1); % presSP1 should be selected
            testCase.verifyEqual(washedData.presSP_bar, rawDataStdUnit.presSP1_bar);
        end
        
        function songaWashData_selectflowOut_correctWashDataStruct(testCase)
            
            allTagsWithUnits = [...
                testCase.songaMeasTagsWithUnit;...
                testCase.songaValidTagsWithUnit;...
                testCase.songaSelectorTagsWithUnit];
            
            rwdu = cell2struct(allTagsWithUnits(:,2), allTagsWithUnits(:,1), 1);
                        
            testCase.proObj=testCase.proObj.specifyUnits(rwdu);
            testCase.proObj=testCase.proObj.setAvailableTags(rwdu);
            testCase.proObj=testCase.proObj.defineNameSpace;
            
            testCase.assertTrue(testCase.proObj.runSongaPreProcess);
            
            rawDataStdUnit = testCase.defaultValues;
                        
            washedData = testCase.proObj.songaWashData(rawDataStdUnit);
            testCase.assertEqual(rawDataStdUnit.selectflowout_boolean, 0);
            testCase.verifyEqual(washedData.flowOut_lpm, rawDataStdUnit.flowOut_lpm);
        end
        
        function songaWashData_selectflowOut1_correctWashDataStruct(testCase)
            
            allTagsWithUnits = [...
                testCase.songaMeasTagsWithUnit;...
                testCase.songaValidTagsWithUnit;...
                testCase.songaSelectorTagsWithUnit];
            
            rwdu = cell2struct(allTagsWithUnits(:,2), allTagsWithUnits(:,1), 1);
                        
            testCase.proObj=testCase.proObj.specifyUnits(rwdu);
            testCase.proObj=testCase.proObj.setAvailableTags(rwdu);
            testCase.proObj=testCase.proObj.defineNameSpace;
            
            testCase.assertTrue(testCase.proObj.runSongaPreProcess);
            
            rawDataStdUnit = testCase.defaultValues;
            rawDataStdUnit.selectflowout_boolean = 1;
                        
            washedData = testCase.proObj.songaWashData(rawDataStdUnit);
            testCase.assertEqual(rawDataStdUnit.selectflowout_boolean, 1);
            testCase.verifyEqual(washedData.flowOut_lpm, rawDataStdUnit.flowOut1_lpm);
        end
        
        
        function setAvailableTags_correctInput_correctOutput(testCase)
            
            rwdu=struct(); % raw data units
            rwdu.flowIn='m3/s';
            rwdu.flowOut='m3/s';
            rwdu.flowOutPercent='%';
            rwdu.rpm='rpm';
            rwdu.velRop='m/s';
            rwdu.presSP='kPa';
            rwdu.depthHole='m';
            rwdu.depthBit='m';
            rwdu.heightHook='m';
            rwdu.volGainLoss='m3';
            rwdu.volTripTank='m3';
            rwdu.weightOnBit='kN';
            
            testCase.proObj=testCase.proObj.setAvailableTags(rwdu);
            
            expAvailableRequiredTags=true(1, 3);
            
            expAvailableOptionalTags=true(1, 13);
            expAvailableOptionalTags([2 4 6 8 11 13])=false(1, 6);
            
            testCase.verifyEqual(testCase.proObj.availableRequiredTags, expAvailableRequiredTags);
            testCase.verifyEqual(testCase.proObj.availableOptionalTags, expAvailableOptionalTags);
        end
        
        function setAvailableTags_noSongaTags_warningFree(testCase)
            
            rwdu=struct(); % raw data units
            rwdu.flowIn='m3/s';
            rwdu.flowOut='m3/s';
            rwdu.flowOutPercent='%';
            rwdu.rpm='rpm';
            rwdu.velRop='m/s';
            rwdu.presSP='kPa';
            rwdu.depthHole='m';
            rwdu.depthBit='m';
            rwdu.heightHook='m';
            rwdu.volGainLoss='m3';
            rwdu.volTripTank='m3';
            rwdu.weightOnBit='kN';

            testCase.verifyWarningFree(@()testCase.proObj.setAvailableTags(rwdu));
        end
        
        function setAvailableTags_someSongaTagsButNotAll_issueError(testCase)
            
            rwdu=struct(); % raw data units
            rwdu.flowIn='m3/s';
            rwdu.flowOut='m3/s';
            rwdu.flowOutPercent='%';
            rwdu.rpm='rpm';
            rwdu.velRop='m/s';
            rwdu.presSP='kPa';
            rwdu.depthHole='m';
            rwdu.depthBit='m';
            rwdu.heightHook='m';
            rwdu.volGainLoss='m3';
            rwdu.volTripTank='m3';
            rwdu.weightOnBit='kN';
            rwdu.validdepthHole='boolean';
            rwdu.selectflowout='boolean';
            
            testCase.verifyError(@()testCase.proObj.setAvailableTags(rwdu),...
                'PROCESS_OBJ:onlySomeSongaTagsDefined');
        end
        
        function setAvailableTags_missingSongaMeasTag_issueError(testCase)
            allTagsWithUnits = [...
                testCase.songaMeasTagsWithUnit;...
                testCase.songaValidTagsWithUnit;...
                testCase.songaSelectorTagsWithUnit];
            
            % remove a row with meas tag
            allTagsWithUnits(1,:) = [];
            
            rwdu = cell2struct(allTagsWithUnits(:,2), allTagsWithUnits(:,1), 1);
            
            testCase.verifyError(@()testCase.proObj.setAvailableTags(rwdu),...
                'PROCESS_OBJ:missingSongaMeasTags');
        end
        
        function setAvailableTags_allSongaMeasTag_setRunSongaPreProcess(testCase)
            allTagsWithUnits = [...
                testCase.songaMeasTagsWithUnit;...
                testCase.songaValidTagsWithUnit;...
                testCase.songaSelectorTagsWithUnit];
            
            rwdu = cell2struct(allTagsWithUnits(:,2), allTagsWithUnits(:,1), 1);
            
            testCase.proObj=testCase.proObj.setAvailableTags(rwdu);
            
            testCase.verifyTrue(testCase.proObj.runSongaPreProcess);
        end

        
        function washData_missingRequiredTag_returnErrorCode(testCase)
            
            rwdu=struct(); % raw data units
            % rwdu.flowIn='m3/s'; % cruicial tag is missing
            rwdu.flowOut='m3/s';
            rwdu.flowOutPercent='%';
            rwdu.rpm='rpm';
            rwdu.velRop='m/s';
            rwdu.presSP='kPa';
            rwdu.depthHole='m';
            rwdu.depthBit='m';
            rwdu.volGainLoss='m3';
            rwdu.volTripTank='m3';
            
            testCase.proObj=testCase.proObj.specifyUnits(rwdu);
            testCase.proObj=testCase.proObj.setAvailableTags(rwdu);
            testCase.proObj=testCase.proObj.defineNameSpace();
            
            rawData_nonStd=struct();
            rawData_nonStd.flowOut.V=0.15;
            rawData_nonStd.flowOut.t=now;
            rawData_nonStd.flowOutPercent.V=15;
            rawData_nonStd.flowOutPercent.t=now;
            rawData_nonStd.rpm.V=200;
            rawData_nonStd.rpm.t=now;
            rawData_nonStd.velRop.V=0.01;
            rawData_nonStd.velRop.t=now;
            rawData_nonStd.presSP.V=5000;
            rawData_nonStd.presSP.t=now;
            rawData_nonStd.depthHole.V=3000;
            rawData_nonStd.depthHole.t=now;
            rawData_nonStd.depthBit.V=2999;
            rawData_nonStd.depthBit.t=now;
            rawData_nonStd.volGainLoss.V=3;
            rawData_nonStd.volGainLoss.t=now;
            rawData_nonStd.volTripTank.V=2;
            rawData_nonStd.volTripTank.t=now;
            
            [~, ~, ~,~,errorCode,~] = ...
                testCase.proObj.washData(rawData_nonStd, STATE_MACHINE_OBJ(), false);
            
            % return with error code indicating that flowIn is missing
            testCase.verifyEqual(errorCode, DtectLossRealtimeErrorCodes.NoValue_flowIn);
        end
        
        function washData_correctInit_correctConversion(testCase)
            
            rwdu=struct(); % raw data units
            rwdu.flowIn='m3/s';
            rwdu.flowOut='m3/s';
            rwdu.flowOutPercent='%';
            rwdu.rpm='rpm';
            rwdu.velRop='m/s';
            rwdu.presSP='kPa';
            rwdu.depthHole='m';
            rwdu.depthBit='m';
            rwdu.heightHook='m';
            rwdu.volGainLoss='m3';
            rwdu.tempBHA='K';
            rwdu.volTripTank='m3';
            
            testCase.proObj=testCase.proObj.specifyUnits(rwdu);
            testCase.proObj=testCase.proObj.setAvailableTags(rwdu);
            testCase.proObj=testCase.proObj.defineNameSpace();
            
            sysState=STATE_MACHINE_OBJ(); % the state machine
            
            rawData_nonStd=struct();
            rawData_nonStd.flowIn.V=0.1;
            rawData_nonStd.flowIn.t=now;
            rawData_nonStd.flowOut.V=0.15;
            rawData_nonStd.flowOut.t=now;
            rawData_nonStd.flowOutPercent.V=15;
            rawData_nonStd.flowOutPercent.t=now;
            rawData_nonStd.rpm.V=200;
            rawData_nonStd.rpm.t=now;
            rawData_nonStd.velRop.V=0.01;
            rawData_nonStd.velRop.t=now;
            rawData_nonStd.presSP.V=5000;
            rawData_nonStd.presSP.t=now;
            rawData_nonStd.depthHole.V=3000;
            rawData_nonStd.depthHole.t=now;
            rawData_nonStd.depthBit.V=2999;
            rawData_nonStd.depthBit.t=now;
            rawData_nonStd.heightHook.V=29;
            rawData_nonStd.heightHook.t=now;
            rawData_nonStd.volGainLoss.V=3;
            rawData_nonStd.volGainLoss.t=now;
            rawData_nonStd.volTripTank.V=2;
            rawData_nonStd.volTripTank.t=now;
            rawData_nonStd.tempBHA.V=273.15;
            rawData_nonStd.tempBHA.t=now;
            
            expRwdsu=struct(); % expected raw data std unit
            expRwdsu.flowIn_lpm=6000;
            expRwdsu.flowOut_lpm=9000;
            expRwdsu.rpm=200;
            expRwdsu.velRop_mph=36;
            expRwdsu.presSP_bar=50;
            expRwdsu.depthHole_m=3000;
            expRwdsu.depthBit_m=2999;
            expRwdsu.heightHook_m=29;
            expRwdsu.volGainLoss_m3=3;
            expRwdsu.volTripTank_m3=2;
            
            [~, ~, rwdsu] = testCase.proObj.washData(rawData_nonStd,sysState,false);
            
            fN=fieldnames(expRwdsu);
            for ii=1:numel(fN)
                testCase.verifyEqual(rwdsu.(fN{ii}),...
                    expRwdsu.(fN{ii}),...
                    sprintf('Checking correct mapping to default unit for var %s', fN{ii}));
            end
        end
        
        % write test for convertUnitIfFields
        % write test for unifyTime
        
        function generateNoValueErrorCode_tagNotDefined_returnNonSpecific(testCase)
            ec=PROCESS_OBJ.generateNoValueErrorCode('tagNotDefined');
            
            testCase.verifyEqual(ec,DtectLossRealtimeErrorCodes.MissingValueNonSpecfic);
        end
        
        function generateNoValueErrorCode_tagDefined_returnCorrectEnum(testCase)
            ec=PROCESS_OBJ.generateNoValueErrorCode('flowIn');
            
            testCase.verifyEqual(ec,DtectLossRealtimeErrorCodes.NoValue_flowIn);
        end
        
        function generateNoUnitErrorCode_tagNotDefined_returnNonSpecific(testCase)
            ec=PROCESS_OBJ.generateNoUnitErrorCode('tagNotDefined');
            
            testCase.verifyEqual(ec,DtectLossRealtimeErrorCodes.MissingUnitNonSpecfic);
        end
        
        function generateNoUnitErrorCode_tagDefined_returnCorrectEnum(testCase)
            ec=PROCESS_OBJ.generateNoUnitErrorCode('flowIn');
            
            testCase.verifyEqual(ec,DtectLossRealtimeErrorCodes.NoUnit_flowIn);
        end
        
        function defineNamespace_noValidCombination_returnEmpty(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            testCase.applyFixture(...
                SuppressedWarningsFixture('PROCESS_OBJ:noConversionInUnitMap'));
            
            rwdu=struct(); % raw data units
            rwdu.flowIn='wrongUnit';
            rwdu.tempBHA='K';
            rwdu.volTripTank='wrongUnit';
            
            testCase.proObj=testCase.proObj.specifyUnits(rwdu);
            testCase.proObj=testCase.proObj.setAvailableTags(rwdu);
            testCase.proObj=testCase.proObj.defineNameSpace();
            
            testCase.verifyEmpty(testCase.proObj.nameSpace)
        end
        
        function defineNamespace_validCombination_returnCorrectNamespace(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            testCase.applyFixture(...
                SuppressedWarningsFixture('PROCESS_OBJ:noConversionInUnitMap'));
            
            rwdu=struct(); % raw data units
            rwdu.flowIn='m3/s';
            rwdu.flowOut='m3/s';
            rwdu.flowOutPercent='%';
            rwdu.rpm='rpm';
            rwdu.velRop='cm/s'; % not valid unit
            rwdu.presSP='kPa';
            rwdu.depthHole='m';
            rwdu.depthBit='m';
            rwdu.heightHook='m';
            rwdu.volGainLoss='m3';
            rwdu.tempBHA='K';
            rwdu.volTripTank='m3';
            
            testCase.proObj=testCase.proObj.specifyUnits(rwdu);
            testCase.proObj=testCase.proObj.setAvailableTags(rwdu);
            testCase.proObj=testCase.proObj.defineNameSpace();
            
            expNameSpace={'flowIn', 'depthBit', 'depthHole',...
                'flowOut', 'presSP', 'volGainLoss', 'volTripTank', 'heightHook', ...
                'rpm'}';
                            
            testCase.verifyEqual(testCase.proObj.nameSpace, expNameSpace);
        end
        
        function defineNamespace_wrongUnitFlowOut_returnFlowOutAnyway(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            testCase.applyFixture(...
                SuppressedWarningsFixture('PROCESS_OBJ:noConversionInUnitMap'));
            
            rwdu=struct(); % raw data units
            rwdu.flowIn='m3/s';
            rwdu.flowOut='%';
            rwdu.depthHole='m';
            rwdu.depthBit='m';
            
            testCase.proObj=testCase.proObj.specifyUnits(rwdu);
            testCase.proObj=testCase.proObj.setAvailableTags(rwdu);
            testCase.proObj=testCase.proObj.defineNameSpace();
            
            expNameSpace={'flowIn', 'depthBit', 'depthHole', 'flowOut'}';
            expUndefinedTags={...
            'flowOut1_lpm';
            'presSP_bar';
            'presSP1_bar';
            'volGainLoss_m3';
            'volPitDrill_m3';
            'volTripTank_m3';
            'volTripTank2_m3';
            'heightHook_m';
            'rpm';
            'torque_kNm';
            'weightOnBit_kN';
            'rop_mph'};
                                        
            testCase.verifyEqual(testCase.proObj.nameSpace, expNameSpace);
            testCase.verifyEqual(testCase.proObj.undefinedTags, expUndefinedTags);
        end
        
        function defineNamespace_correctInit_setUndefinedTags(testCase)
            import matlab.unittest.fixtures.SuppressedWarningsFixture
            testCase.applyFixture(...
                SuppressedWarningsFixture('PROCESS_OBJ:noConversionInUnitMap'));
            
            rwdu=struct(); % raw data units
            rwdu.flowIn='m3/s';
            rwdu.flowOut='lpm';
            rwdu.depthHole='m';
            rwdu.depthBit='m';
            
            testCase.proObj=testCase.proObj.specifyUnits(rwdu);
            testCase.proObj=testCase.proObj.setAvailableTags(rwdu);
            testCase.proObj=testCase.proObj.defineNameSpace();
            
            expNameSpace={'flowIn', 'depthBit', 'depthHole', 'flowOut'}';
            expUndefinedTags={...
            'flowOut1_lpm';
            'presSP_bar';
            'presSP1_bar';
            'volGainLoss_m3';
            'volPitDrill_m3';
            'volTripTank_m3';
            'volTripTank2_m3';
            'heightHook_m';
            'rpm';
            'torque_kNm';
            'weightOnBit_kN';
            'rop_mph'};
                            
            testCase.verifyEqual(testCase.proObj.nameSpace, expNameSpace);
            testCase.verifyEqual(testCase.proObj.undefinedTags, expUndefinedTags);
        end
        
        function unifyTime_presSPNewest_usePresSPTimeAsTimebase(testCase)
            
            rwdu=struct(); % raw data units
            rwdu.flowIn='m3/s';
            rwdu.presSP='kPa';
            rwdu.depthHole='m';
            
            testCase.proObj=testCase.proObj.specifyUnits(rwdu);
            testCase.proObj=testCase.proObj.setAvailableTags(rwdu);
            testCase.proObj=testCase.proObj.defineNameSpace();
            
            t=datenum(2017, 02, 20, 13, 17, 10);
            rawData_nonStd=struct();
            rawData_nonStd.flowIn.V=0.1;
            rawData_nonStd.flowIn.t=t;
            rawData_nonStd.presSP.V=5000;
            rawData_nonStd.presSP.t=t+2/86400;
            rawData_nonStd.depthHole.V=3000;
            rawData_nonStd.depthHole.t=t;
            rawData_nonStd.depthBit.V=2999;
            rawData_nonStd.depthBit.t=t;
            
            testCase.proObj=testCase.proObj.unifyTime(rawData_nonStd);
            
            expTimebase=t+2/86400;
            
            testCase.verifyEqual(testCase.proObj.timeCurrent_d, expTimebase);
        end
        
        function unifyTime_oldTimeForDepthHole_setMissingValCode(testCase)
            
            rwdu=struct(); % raw data units
            rwdu.flowIn='m3/s';
            rwdu.presSP='kPa';
            rwdu.depthHole='m';
            
            testCase.proObj=testCase.proObj.specifyUnits(rwdu);
            testCase.proObj=testCase.proObj.setAvailableTags(rwdu);
            testCase.proObj=testCase.proObj.defineNameSpace();
            
            t=datenum(2017, 02, 20, 13, 17, 10);
            rawData_nonStd=struct();
            rawData_nonStd.flowIn.V=0.1;
            rawData_nonStd.flowIn.t=t;
            rawData_nonStd.presSP.V=5000;
            rawData_nonStd.presSP.t=t+2/86400;
            rawData_nonStd.depthHole.V=3000;
            rawData_nonStd.depthHole.t=t-600/86400; % ten min and 2 sec older than newest tag
            rawData_nonStd.depthBit.V=2999;
            rawData_nonStd.depthBit.t=t;
            
            [~, rd]=testCase.proObj.unifyTime(rawData_nonStd);
            
            testCase.verifyEqual(rd.depthHole.V, testCase.missingValCode);
        end
    end
    
    methods (Static)
        function rawData = makeRawDataStructFromCell(dataCell)
            rawData = struct();
            time_d = datenum(2017, 06, 06, 12, 30, 30);
            
            for ii = 1:size(dataCell, 1)
                rawData.(dataCell{ii, 1}).V = dataCell{ii, 2};
                rawData.(dataCell{ii, 1}).t = time_d;
            end
        end
    end
end
