classdef removeOffsetTest < matlab.unittest.TestCase
    %REMOVEOFFSETTEST unit tests of the removeOffset-function
    
    properties
       threshold;
       offset;
       errVal;
       flowInLowLim_lpm;
    end
    
    methods (TestMethodSetup)
        function initFunc(testCase)
            testCase.threshold=5;
            testCase.offset=-2;
            testCase.errVal=-0.99925;
            testCase.flowInLowLim_lpm = 400;
        end
    end
    
    methods (Test)
        function noChangeInInput_correctOutput(testCase)
            yIn=[0 0];
            [yOutAct,offsetOutAct]=removeOffset(yIn,testCase.offset, testCase.threshold);
            yOutExp=-2;
            offsetOutExp=testCase.offset;
            testCase.verifyEqual(yOutAct,yOutExp, 'yOut is not as expected');
            testCase.verifyEqual(offsetOutAct,offsetOutExp, 'The offset is not computed correctly');
        end
        
        function positiveChangeWithinThresholdInInput_correctOutput(testCase)
            yIn=[0 2];
            [yOutAct,offsetOutAct]=removeOffset(yIn,testCase.offset, testCase.threshold);
            yOutExp=0;
            offsetOutExp=testCase.offset;
            testCase.verifyEqual(yOutAct,yOutExp, 'yOut is not as expected');
            testCase.verifyEqual(offsetOutAct,offsetOutExp, 'The offset is not computed correctly');
        end
        
        function negativeChangeWithinThresholdInInput_correctOutput(testCase)
            yIn=[0 -2];
            [yOutAct,offsetOutAct]=removeOffset(yIn,testCase.offset, testCase.threshold);
            yOutExp=-4;
            offsetOutExp=testCase.offset;
            testCase.verifyEqual(yOutAct,yOutExp, 'yOut is not as expected');
            testCase.verifyEqual(offsetOutAct,offsetOutExp, 'The offset is not computed correctly');
        end
        
        function positiveChangeGreaterThanThresholdInInput_correctOutput(testCase)
            yIn=[0 6];
            [yOutAct,offsetOutAct]=removeOffset(yIn,testCase.offset, testCase.threshold);
            yOutExp=-2;
            offsetOutExp=-8;
            testCase.verifyEqual(yOutAct,yOutExp, 'yOut is not as expected');
            testCase.verifyEqual(offsetOutAct,offsetOutExp, 'The offset is not computed correctly');
        end
        
        function negativeChangeGreaterThanThresholdInInput_correctOutput(testCase)
            yIn=[0 -6];
            [yOutAct,offsetOutAct]=removeOffset(yIn,testCase.offset, testCase.threshold);
            yOutExp=-2;
            offsetOutExp=4;
            testCase.verifyEqual(yOutAct,yOutExp, 'yOut is not as expected');
            testCase.verifyEqual(offsetOutAct,offsetOutExp, 'The offset is not computed correctly');
        end
        
        function missingValue_returnErrorVal(testCase)
            yIn=[10 testCase.errVal];
            [yOutAct,offsetOutAct]=removeOffset(yIn,testCase.offset, testCase.threshold);
            yOutExp=testCase.errVal;
            offsetOutExp=testCase.offset;
            testCase.verifyEqual(yOutAct,yOutExp, 'yOut is not as expected');
            testCase.verifyEqual(offsetOutAct,offsetOutExp, 'The offset is not computed correctly');
        end
        
        function flowInErrVal_correctOutput(testCase)
            yIn=[0 -6];
            [yOutAct,offsetOutAct]=removeOffset(yIn,testCase.offset, testCase.threshold, testCase.errVal);
            yOutExp=-2;
            offsetOutExp=4;
            testCase.verifyEqual(yOutAct,yOutExp, 'yOut is not as expected');
            testCase.verifyEqual(offsetOutAct,offsetOutExp, 'The offset is not computed correctly');
        end
        
        function flowInLessThanLimit_useOriginalThreshold(testCase)
            yIn=[0 -6];
            [yOutAct,offsetOutAct]=removeOffset(yIn,testCase.offset, testCase.threshold, testCase.flowInLowLim_lpm - 10);
            yOutExp=-2;
            offsetOutExp=4;
            testCase.verifyEqual(yOutAct,yOutExp, 'yOut is not as expected');
            testCase.verifyEqual(offsetOutAct,offsetOutExp, 'The offset is not computed correctly');
        end
        
        function flowInGreaterThanLimit_useHalfOfOriginalThreshold(testCase)
            yIn=[0 -3];
            [yOutAct, offsetOutAct]=removeOffset(yIn, testCase.offset, testCase.threshold, testCase.flowInLowLim_lpm + 10);
            yOutExp=-2;
            offsetOutExp=1;
            testCase.verifyEqual(yOutAct,yOutExp, 'yOut is not as expected');
            testCase.verifyEqual(offsetOutAct,offsetOutExp, 'The offset is not computed correctly');
        end
    end
    
end

