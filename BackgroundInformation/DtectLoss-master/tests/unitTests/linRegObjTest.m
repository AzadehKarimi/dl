classdef linRegObjTest < matlab.unittest.TestCase
   
    properties
        lr
    end
    
    methods (TestMethodSetup)
        function initFunc(testCase)
            testCase.lr = LINREG_OBJ();
%             testCase.initTime_d = datenum(2017, 03, 27, 09, 49, 00);
%             testCase.missingValCode = -0.99925;
%             testCase.addTeardown(@clear, 'testCase.dd');
        end
    end
    
    methods (Test)
      
        function correctCoeffFitLineV2(testCase)
            
            X = [43 21 25 42 57 59];
            y = [99 65 79 75 87 81];
            expectedSlope = 0.385225;
            result = testCase.lr.fitData(X',y');
            testCase.verifyEqual(result(1), expectedSlope); 
        end
        
        
    end
end

