classdef circulationStatesTest < matlab.unittest.TestCase
    %SYSTEMSTATETEST testing of the SYSTEM_STATES_OBJ
    
    properties
        circState
        missingValCode
    end
    
    methods (TestMethodSetup)
        function initFunc(testCase)
            testCase.circState=CIRCULATION_STATES_OBJ();
            testCase.missingValCode=-0.99925;
        end
    end
    
    methods (Test)
        function correctInit_correctSystemState(testCase)
            testCase.verifyInstanceOf(testCase.circState.currentState, 'CirculationStates');
            testCase.verifyInstanceOf(testCase.circState.previousState, 'CirculationStates');
            testCase.verifyEqual(testCase.circState.currentState, CirculationStates.notCirculating);
            testCase.verifyEqual(testCase.circState.previousState, CirculationStates.circulating);
        end
        
        function changeState_correctStateChange(testCase)
            testCase.circState=testCase.circState.changeState(CirculationStates.drilling);
            testCase.verifyEqual(testCase.circState.currentState,CirculationStates.drilling);
        end
        
        function stateMachine_missingFlowIn_doNothing(testCase)
            % test transition from notCirculating to circulating
            testCase.assertEqual(testCase.circState.currentState,CirculationStates.notCirculating);
            testCase.circState=testCase.circState.stateMachine(100,50,100);
            testCase.assertEqual(testCase.circState.currentState,CirculationStates.circulating);
            testCase.circState=testCase.circState.stateMachine(testCase.missingValCode,50,100);
            testCase.verifyEqual(testCase.circState.currentState,CirculationStates.circulating);
        end
        
        function stateMachine_missingDepthBit_doNothing(testCase)
            % test transition from notCirculating to circulating
            testCase.assertEqual(testCase.circState.currentState,CirculationStates.notCirculating);
            testCase.circState=testCase.circState.stateMachine(100,100,100);
            testCase.assertEqual(testCase.circState.currentState,CirculationStates.circulating);
            testCase.circState=testCase.circState.stateMachine(100,100,100);
            testCase.assertEqual(testCase.circState.currentState,CirculationStates.drilling);
            testCase.circState=testCase.circState.stateMachine(100,testCase.missingValCode,100);
            testCase.verifyEqual(testCase.circState.currentState,CirculationStates.drilling);
        end
        
        function stateMachine_missingDepthHole_doNothing(testCase)
            % test transition from notCirculating to circulating
            testCase.assertEqual(testCase.circState.currentState,CirculationStates.notCirculating);
            testCase.circState=testCase.circState.stateMachine(100,100,100);
            testCase.assertEqual(testCase.circState.currentState,CirculationStates.circulating);
            testCase.circState=testCase.circState.stateMachine(100,100,testCase.missingValCode);
            testCase.verifyEqual(testCase.circState.currentState,CirculationStates.circulating);
        end
        
        function changeState_correctStateMachineBehaviour(testCase)
            % test transition from notCirculating to circulating
            testCase.assertEqual(testCase.circState.currentState,CirculationStates.notCirculating);
            testCase.circState=testCase.circState.stateMachine(100,50,100);
            testCase.verifyEqual(testCase.circState.currentState,CirculationStates.circulating);
            % test transition from circulating to drilling
            testCase.assertEqual(testCase.circState.currentState,CirculationStates.circulating);
            testCase.circState=testCase.circState.stateMachine(100,99.6,100);
            testCase.verifyEqual(testCase.circState.currentState,CirculationStates.drilling);
            % test transition from drilling to circulating
            testCase.assertEqual(testCase.circState.currentState,CirculationStates.drilling);
            testCase.circState=testCase.circState.stateMachine(100,99.4,100);
            testCase.verifyEqual(testCase.circState.currentState,CirculationStates.circulating);
            % test transition from circulating to NotCirculating
            testCase.assertEqual(testCase.circState.currentState,CirculationStates.circulating);
            testCase.circState=testCase.circState.stateMachine(0,98.9,100);
            testCase.verifyEqual(testCase.circState.currentState,CirculationStates.notCirculating);
            % change state back to drilling and test transition from drilling to NotCirculating
            testCase.circState=testCase.circState.changeState(CirculationStates.drilling);
            testCase.assertEqual(testCase.circState.currentState,CirculationStates.drilling);
            testCase.circState=testCase.circState.stateMachine(0,98.9,100);
            testCase.verifyEqual(testCase.circState.currentState,CirculationStates.notCirculating);
        end
        
    end
end