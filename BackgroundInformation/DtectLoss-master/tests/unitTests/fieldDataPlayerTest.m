classdef fieldDataPlayerTest < matlab.unittest.TestCase
    %FIELDDATAPLAYERTEST testing of the field data player object.
    
    properties
        fileName
        matName
        fileNameZipped
        varNames
        units
        dT
        fdp
    end
    
    methods (TestMethodSetup)
        function clearFixture(testCase)
            testCase.fileName=fullfile('helperFiles', 'smallCurves.csv');
            testCase.matName=fullfile('helperFiles', 'smallCurves.mat');
            testCase.fileNameZipped=fullfile('helperFiles', 'smallCurvesZipped.zip');
            testCase.dT=2; % [s] time-step
            testCase.varNames={'depthBit' 'flowIn' 'flowOut' 'volPitDrill'};
            testCase.units={'m' 'm3/s' 'm3/s' 'm3'};
            testCase.fdp=FIELD_DATA_PLAYER_OBJ(testCase.fileName,testCase.dT, testCase.varNames);
            testCase.addTeardown(@clear, 'testCase.fdp');
        end
    end
    
    methods (Test)
        function constructor_correctInit_correctFieldDataPlayer(testCase) % unzipped version
            
            testCase.verifyInstanceOf(testCase.fdp, 'FIELD_DATA_PLAYER_OBJ');
            testCase.verifyInstanceOf(testCase.fdp.ds, 'struct');
            testCase.verifyInstanceOf(testCase.fdp.varNames, 'cell');
            testCase.verifyEqual(testCase.fdp.nVars,4,'Testing number of variables');
            testCase.verifyEqual(testCase.fdp.nDataPoints,[179 179 179 17]', 'Testing number of datapoints');
            testCase.verifyEqual(testCase.fdp.simTime,0,'Testing current time');
            testCase.verifyEqual(testCase.fdp.currentIdx,ones(4,1));
            testCase.verifyEqual(testCase.fdp.currentTime,zeros(4,1));
            testCase.verifyEqual(testCase.fdp.varNames,testCase.varNames);
            testCase.verifyEmpty(testCase.fdp.output);
            testCase.verifyEmpty(testCase.fdp.startTime);
            testCase.verifyEmpty(testCase.fdp.absTime);
            testCase.verifyEmpty(testCase.fdp.asyncDT);
        end
        
        function constructor_correctInit_correctFieldDataPlayerZipped(testCase) % zipped version
            testCase.fdp=FIELD_DATA_PLAYER_OBJ(testCase.fileNameZipped,testCase.dT, testCase.varNames);
            testCase.verifyInstanceOf(testCase.fdp, 'FIELD_DATA_PLAYER_OBJ');
            testCase.verifyInstanceOf(testCase.fdp.ds, 'struct');
            testCase.verifyInstanceOf(testCase.fdp.varNames, 'cell');
            testCase.verifyEqual(testCase.fdp.nVars,4,'Testing number of variables');
            testCase.verifyEqual(testCase.fdp.nDataPoints,[179 179 179 17]', 'Testing number of datapoints');
            testCase.verifyEqual(testCase.fdp.simTime,0,'Testing current time');
            testCase.verifyEqual(testCase.fdp.currentIdx,ones(4,1));
            testCase.verifyEqual(testCase.fdp.currentTime,zeros(4,1));
            testCase.verifyEqual(testCase.fdp.varNames,testCase.varNames);
            testCase.verifyEmpty(testCase.fdp.output);
            testCase.verifyEmpty(testCase.fdp.startTime);
            testCase.verifyEmpty(testCase.fdp.absTime);
            testCase.verifyEmpty(testCase.fdp.asyncDT);
        end
        
        function constructor_missingVarName_issueWarning(testCase)
            testCase.verifyWarning(@()FIELD_DATA_PLAYER_OBJ(testCase.fileName,testCase.dT, {'depthBit' 'mwdMem'}),...
                'FIELD_DATA_PLAYER_OBJ:desiredVariableNotAvailable')
        end
        
        function constructor_missingUnit_issueWarning(testCase)
            testCase.verifyWarning(@()FIELD_DATA_PLAYER_OBJ(testCase.fileName,testCase.dT, {'depthBit' 'flowOutPercent'}),...
                'FIELD_DATA_PLAYER_OBJ:missingUnit')
        end
        
        function constructor_correctInit_correctUnits(testCase)
            
            counter=1;
            for vars=testCase.fdp.varNames
                testCase.verifyEqual(testCase.fdp.rawDataUnits.(vars{1}), testCase.units{counter}, 'Verifying correct unit')
                counter=counter+1;
            end
        end
        
        function initialize_correctInit_correctOutput(testCase)
            
            testCase.fdp=testCase.fdp.initialize;
            
            expOutputBitDepthV=1783.8;
            expOutputFlowInV=0.017671363;
            expOutputFlowOutV=0;
            expOutputPitDrillV=76.571633;
            
            expOutputBitDepthT=736471;
            expOutputFlowInT=736471;
            expOutputFlowOutT=736471;
            expOutputPitDrillT=736471;
            
            testCase.verifyEqual(testCase.fdp.output.depthBit.t,expOutputBitDepthT);
            testCase.verifyEqual(testCase.fdp.output.flowIn.t,expOutputFlowInT);
            testCase.verifyEqual(testCase.fdp.output.flowOut.t,expOutputFlowOutT);
            testCase.verifyEqual(testCase.fdp.output.volPitDrill.t,expOutputPitDrillT);
            
            testCase.verifyEqual(testCase.fdp.output.depthBit.V,expOutputBitDepthV);
            testCase.verifyEqual(testCase.fdp.output.flowIn.V,expOutputFlowInV);
            testCase.verifyEqual(testCase.fdp.output.flowOut.V,expOutputFlowOutV);
            testCase.verifyEqual(testCase.fdp.output.volPitDrill.V,expOutputPitDrillV);
            
            expStartTime=736471;
            expAbsTime=736471;
            testCase.verifyEqual(testCase.fdp.startTime,expStartTime);
            testCase.verifyEqual(testCase.fdp.absTime,expAbsTime);
        end
        
        function initialize_readMatFile_correctOutput(testCase)
            testCase.fdp=FIELD_DATA_PLAYER_OBJ(testCase.matName,testCase.dT, testCase.varNames);
            testCase.fdp=testCase.fdp.initialize;
            
            expOutputBitDepthV=1783.8;
            expOutputFlowInV=0.017671363;
            expOutputFlowOutV=0;
            expOutputPitDrillV=76.571633;
            
            expOutputBitDepthT=736471;
            expOutputFlowInT=736471;
            expOutputFlowOutT=736471;
            expOutputPitDrillT=736471;
            
            testCase.verifyEqual(testCase.fdp.output.depthBit.t,expOutputBitDepthT);
            testCase.verifyEqual(testCase.fdp.output.flowIn.t,expOutputFlowInT);
            testCase.verifyEqual(testCase.fdp.output.flowOut.t,expOutputFlowOutT);
            testCase.verifyEqual(testCase.fdp.output.volPitDrill.t,expOutputPitDrillT);
            
            testCase.verifyEqual(testCase.fdp.output.depthBit.V,expOutputBitDepthV);
            testCase.verifyEqual(testCase.fdp.output.flowIn.V,expOutputFlowInV);
            testCase.verifyEqual(testCase.fdp.output.flowOut.V,expOutputFlowOutV);
            testCase.verifyEqual(testCase.fdp.output.volPitDrill.V,expOutputPitDrillV);
            
            expStartTime=736471;
            expAbsTime=736471;
            testCase.verifyEqual(testCase.fdp.startTime,expStartTime);
            testCase.verifyEqual(testCase.fdp.absTime,expAbsTime);
        end
        
        function play_sixtySeconds_correctOutput(testCase)
            
            testCase.fdp=testCase.fdp.initialize;
            expOutput=readDataFromCsv(fullfile('helperFiles', 'expectedOutputForUnitTestOfFDP.csv'));
            for ii=1:size(expOutput.flowIn.t,1)
                testCase.fdp=testCase.fdp.play;
                testCase.fatalAssertEqual(testCase.fdp.simTime,ii*testCase.fdp.dT, 'Current time is not correct');
                for kk=1:testCase.fdp.nVars
                    
                    testCase.fatalAssertEqual(testCase.fdp.output.(testCase.fdp.varNames{kk}).t,...
                        expOutput.(testCase.fdp.varNames{kk}).t(ii),...
                        sprintf('Verification of time failed at idx %g and var number %g.\nTagTime %s expTime %s',ii,kk,...
                        datestr(testCase.fdp.output.(testCase.fdp.varNames{kk}).t),...
                        datestr(expOutput.(testCase.fdp.varNames{kk}).t(ii)) ));
                    
                    testCase.fatalAssertEqual(testCase.fdp.output.(testCase.fdp.varNames{kk}).V,...
                        expOutput.(testCase.fdp.varNames{kk}).V(ii),sprintf('Verification of value failed at idx %g and var number %g',ii,kk));
                end
            end
        end
        
        function asyncPlay_sixtySeconds_correctOutput(testCase)
            
            testCase.fdp=testCase.fdp.initialize;
            expOutput=readDataFromCsv(fullfile('helperFiles', 'expectedOutputAsyncPlay.csv'));
            for ii=1:size(expOutput.flowIn.t,1)
                testCase.fdp=testCase.fdp.asyncPlay;
                for kk=1:testCase.fdp.nVars
                    
                    testCase.fatalAssertEqual(testCase.fdp.output.(testCase.fdp.varNames{kk}).t,...
                        expOutput.(testCase.fdp.varNames{kk}).t(ii),...
                        sprintf('Verification of time failed at idx %g and var number %g.\nTagTime %s expTime %s',ii,kk,...
                        datestr(testCase.fdp.output.(testCase.fdp.varNames{kk}).t),...
                        datestr(expOutput.(testCase.fdp.varNames{kk}).t(ii)) ));
                    
                    testCase.fatalAssertEqual(testCase.fdp.output.(testCase.fdp.varNames{kk}).V,...
                        expOutput.(testCase.fdp.varNames{kk}).V(ii),sprintf('Verification of value failed at idx %g and var number %g',ii,kk));
                end
            end
        end
        
        function play_entireFile_noError(testCase)
            
            testCase.fdp=testCase.fdp.initialize;
            ii=1;
            while ii<400 && max(testCase.fdp.currentIdx)<=max(testCase.fdp.nDataPoints)
                ii=ii+1; % avoid infinite loop
                testCase.fdp=testCase.fdp.play;
            end
        end
        
        function asyncPlay_entireFile_noError(testCase)
            
            testCase.fdp=testCase.fdp.initialize;
            ii=1;
            while ii<400 && max(testCase.fdp.currentIdx)<=max(testCase.fdp.nDataPoints)
                ii=ii+1; % avoid infinite loop
                testCase.fdp=testCase.fdp.asyncPlay;
            end
        end
        
        function play_entireFile_reportStatus(testCase)
            
            testCase.fdp=testCase.fdp.initialize;
            testCase.verifyEqual(testCase.fdp.simProgress,0, 'The simulation progress should be 0 after init');
            testCase.verifyEqual(testCase.fdp.simDuration,498, 'The simulation duration is  498 s for the data set');
            ii=1;
            while ii<400 && max(testCase.fdp.currentIdx)<max(testCase.fdp.nDataPoints)
                ii=ii+1; % avoid infinite loop
                testCase.fdp=testCase.fdp.play;
            end
            testCase.verifyEqual(testCase.fdp.simProgress,100, 'The simulation progress should be 100 when finished');
        end
        
         function asyncPlay_entireFile_reportStatus(testCase)
            
            testCase.fdp=testCase.fdp.initialize;
            testCase.verifyEqual(testCase.fdp.simProgress,0, 'The simulation progress should be 0 after init');
            testCase.verifyEqual(testCase.fdp.simDuration,498, 'The simulation duration is  498 s for the data set');
            ii=1;
            while ii<500 && max(testCase.fdp.currentIdx)<=max(testCase.fdp.nDataPoints)
                ii=ii+1; % avoid infinite loop
                testCase.fdp=testCase.fdp.asyncPlay;
            end
            testCase.verifyEqual(testCase.fdp.simProgress,100, 'The simulation progress should be 100 when finished');
         end
        
         function asyncPlay_absTime_noDuplicates(testCase)             
             testCase.fdp=testCase.fdp.initialize;
             
             ii=1;
             prevAbsTime=testCase.fdp.absTime;
             while testCase.fdp.simProgress<100 && ii<2e5
                 testCase.fdp=testCase.fdp.asyncPlay;
                     testCase.fatalAssertGreaterThan(testCase.fdp.absTime,prevAbsTime,...
                         sprintf('Abs time is equal to prev abs time for idx %g, time: %s', ii, datestr(prevAbsTime)));
                 ii=ii+1; % avoid infinite loop
                 prevAbsTime=testCase.fdp.absTime;
             end
         end
         
         function formatHeader_correctFormat(testCase)
             testCase.fdp=testCase.fdp.initialize;
             
             actHeader = testCase.fdp.formatHeader();
             expHeader = 'time,depthBit,flowIn,flowOut,volPitDrill\n';
             
             testCase.verifyEqual(actHeader, expHeader);
         end
         
         function formatOutput_correctFormat(testCase)
             testCase.fdp=testCase.fdp.initialize;
             
             actOutput= testCase.fdp.formatOutput();
             expOutput = '2016-05-21T00:00:00Z,1783.800000,0.017671,0.000000,76.571633\n';
             
             testCase.verifyEqual(actOutput, expOutput);
         end
    end
end
