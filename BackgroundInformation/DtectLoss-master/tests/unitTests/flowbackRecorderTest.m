classdef flowbackRecorderTest < matlab.unittest.TestCase
    %FLOWBACKRECORDERTEST unit tests of the flowbackRecorder class with
    %methods
    
    properties
        nFlowbacks
        minVolume
        minFlowRate
        flowTolerance
        dtFlowrate
        dtVolGL
        timeDuration
        tuningParams
        fbr
    end
    
    methods (TestClassSetup)
        function initFunc(testCase)
            testCase.nFlowbacks=16; % [int]
            testCase.minVolume=1; % [m3]
            testCase.minFlowRate=50; % [lpm]
            testCase.flowTolerance=400; % [lpm]
            testCase.dtFlowrate=3; % [s]
            testCase.dtVolGL=30; % [s]
            testCase.timeDuration=900; % [s]
            testCase.tuningParams=struct('nTd',4);
        end
    end
    
    methods (TestMethodSetup)
        function createFixture(testCase)
            % make sure that the flowbackRecorder object is cleared between
            % tests.
            testCase.fbr=FLOWBACK_RECORDER_OBJ();
            testCase.fbr=testCase.fbr.initialize(testCase.nFlowbacks, testCase.minVolume, testCase.minFlowRate,...
                testCase.flowTolerance, testCase.dtFlowrate, testCase.dtVolGL, testCase.timeDuration,...
                testCase.tuningParams);
            testCase.addTeardown(@clear, 'testCase.fbr');
        end
    end
    
    methods (Test)
        function intialize_correctInit_correctOutput(testCase)
               
            testCase.verifyEqual(testCase.fbr.nFbs, testCase.nFlowbacks);
            testCase.verifyEqual(testCase.fbr.minVol, testCase.minVolume);
            testCase.verifyEqual(testCase.fbr.minFlow, testCase.minFlowRate);
            testCase.verifyEqual(testCase.fbr.flowTol, testCase.flowTolerance);
            testCase.verifyEqual(testCase.fbr.nRbFlow, 300);
            testCase.verifyEqual(testCase.fbr.nRbVol, 30);
            testCase.verifyClass(testCase.fbr.rbFlowIn,'RINGBUFFER_OBJ');
            testCase.verifyClass(testCase.fbr.rbVol,'RINGBUFFER_OBJ');
            testCase.verifyClass(testCase.fbr.cs,'CIRCULATION_STATES_OBJ');
            testCase.verifyEqual(testCase.fbr.stateAtPrevTime, CirculationStates.notCirculating);
            testCase.verifyEqual(testCase.fbr.pumpsOn, false);
            testCase.verifyEqual(testCase.fbr.idxStore,1);
            testCase.verifyEqual(testCase.fbr.flowInStore, zeros(testCase.nFlowbacks,testCase.fbr.nRbFlow));
            testCase.verifyEqual(testCase.fbr.timeFlowInStore, zeros(testCase.nFlowbacks,testCase.fbr.nRbFlow));
            testCase.verifyEqual(testCase.fbr.volStore, zeros(testCase.nFlowbacks,testCase.fbr.nRbVol));
            testCase.verifyEqual(testCase.fbr.timeVolStore, zeros(testCase.nFlowbacks,testCase.fbr.nRbVol));
            testCase.verifyEqual(testCase.fbr.kStore, zeros(testCase.nFlowbacks,1));
            testCase.verifyEqual(testCase.fbr.V0Store, zeros(testCase.nFlowbacks,1));
            testCase.verifyEqual(testCase.fbr.yStore, zeros(testCase.nFlowbacks,testCase.fbr.nRbVol));
            testCase.verifyEqual(testCase.fbr.pumpOffIdxStore, zeros(testCase.nFlowbacks,1));
            testCase.verifyEqual(testCase.fbr.volOffIdxStore, zeros(testCase.nFlowbacks,1));
            testCase.verifyEqual(testCase.fbr.expVolOffIdxStore, zeros(testCase.nFlowbacks,1));
        end
        
        function softReset_clearSomeData(testCase)
            
            testCase.fbr.idxStore = 2;
            testCase.fbr.idxCon = 2;
            testCase.fbr.isInit = false;
            
            testCase.fbr = testCase.fbr.softReset;
            
            testCase.verifyEqual(testCase.fbr.idxStore, 1);
            testCase.verifyEqual(testCase.fbr.idxCon, 1);
            testCase.verifyTrue(testCase.fbr.isInit);
        end
        
        function addVolToRingbuffer_correctInput_correctOutput(testCase)
   
            volTime_d=datenum(2016, 10, 24, 12, 00, 00);
            vol_m3=-4;
            testCase.fbr=testCase.fbr.addVolToRingbuffer(volTime_d, vol_m3);
            
            testCase.verifyEqual(datestr(testCase.fbr.rbVol.getLastTime), datestr(volTime_d));
            testCase.verifyEqual(testCase.fbr.rbVol.getLastValue, vol_m3);
        end
        
        function addExpVolToRingbuffer_correctInput_correctOutput(testCase)
  
            volTime_d=datenum(2016, 10, 24, 12, 00, 00);
            vol_m3=-4;
            testCase.fbr=testCase.fbr.addExpVolToRingbuffer(volTime_d, vol_m3);
            
            testCase.verifyEqual(datestr(testCase.fbr.rbExpVol.getLastTime), datestr(volTime_d));
            testCase.verifyEqual(testCase.fbr.rbExpVol.getLastValue, vol_m3);
        end
        
        function addFlowToRingbuffer_correctInput_correctOutput(testCase)
    
            flowInTime_d=datenum(2016, 10, 24, 12, 00, 00);
            flowIn_lpm=2400;
            testCase.fbr=testCase.fbr.addFlowToRingbuffer(flowInTime_d, flowIn_lpm);
            
            testCase.verifyEqual(datestr(testCase.fbr.rbFlowIn.getLastTime), datestr(flowInTime_d));
            testCase.verifyEqual(testCase.fbr.rbFlowIn.getLastValue, flowIn_lpm);
        end
        
        function updateCirculationState_correctInput_correctStateChange(testCase)

            flowIn_lpm=2000;
            depthBit_m=1000;
            depthHole_m=2000;
            
            testCase.verifyEqual(testCase.fbr.stateAtPrevTime, CirculationStates.notCirculating);
            testCase.verifyEqual(testCase.fbr.cs.currentState, CirculationStates.notCirculating);
            testCase.verifyFalse(testCase.fbr.pumpsOn);
            testCase.fbr=testCase.fbr.updateCirculationState(flowIn_lpm, depthBit_m, depthHole_m);
            testCase.verifyEqual(testCase.fbr.stateAtPrevTime, CirculationStates.notCirculating);
            testCase.verifyEqual(testCase.fbr.cs.currentState, CirculationStates.circulating);
            testCase.verifyTrue(testCase.fbr.pumpsOn);
        end
        
        function updateTimeSpentAtState_noStateChange_update(testCase)
            testCase.assertEqual(testCase.fbr.stateAtPrevTime, CirculationStates.notCirculating);
            testCase.assertEqual(testCase.fbr.cs.currentState, CirculationStates.notCirculating);
            testCase.assertEqual(testCase.fbr.timeSpentAtState, 0);
            
            timeStep = 1;
            testCase.fbr=testCase.fbr.updateTimeSpentAtState(timeStep);
            
            testCase.verifyEqual(testCase.fbr.timeSpentAtState, timeStep);
        end
        
        function updateTimeSpentAtState_stateChange_reset(testCase)
            testCase.assertEqual(testCase.fbr.stateAtPrevTime, CirculationStates.notCirculating);
            testCase.assertEqual(testCase.fbr.cs.currentState, CirculationStates.notCirculating);
            testCase.assertEqual(testCase.fbr.timeSpentAtState, 0);
            
            flowIn_lpm=2000;
            depthBit_m=1000;
            depthHole_m=2000;
            
            testCase.fbr=testCase.fbr.updateCirculationState(flowIn_lpm,depthBit_m, depthHole_m);
            
            timeStep = 1; 
            testCase.fbr=testCase.fbr.updateTimeSpentAtState(timeStep);
            
            testCase.verifyEqual(testCase.fbr.timeSpentAtState, 0);
        end
        
        function updateFap_isInit_doNotUpdateFap(testCase)

            flowIn_lpm=2000;
            depthBit_m=1000;
            depthHole_m=2000;
            
            % mock object for pit and flowline volume
            fap=FLOWLINE_AND_PIT_OBJ();
            fap=fap.initialize([1; 0],6,0,1,0);
            
            prevIdxStore=testCase.fbr.idxStore;
            testCase.fbr=testCase.fbr.updateCirculationState(flowIn_lpm,depthBit_m, depthHole_m);
            testCase.fbr=testCase.fbr.updateFap(fap);
            testCase.verifyEqual(testCase.fbr.stateAtPrevTime, CirculationStates.notCirculating);
            testCase.verifyEqual(testCase.fbr.cs.currentState, CirculationStates.circulating);
            testCase.verifyTrue(testCase.fbr.pumpsOn);
            testCase.verifyEqual(testCase.fbr.idxStore, prevIdxStore, 'The index counter for flowbacks should not be iterated when still in init state.')
        end
        
        function updateFap_pumpStart_identifyCorrectPumpOffIdx(testCase)
            
            flowIn_lpm=2000;
            depthBit_m=1000;
            depthHole_m=2000;
            
            pumpOffTime=300;
            expPumpOffIdx=100;
            expVolOffIdx=9;
            rampDownTime=20;
            volTimeConstant=120;
            tVol=0:testCase.dtVolGL:testCase.timeDuration-testCase.dtVolGL;
            vol=6*(1-exp(min(0,-(tVol-pumpOffTime)/volTimeConstant)));
            tFlow=0:testCase.dtFlowrate:testCase.timeDuration-testCase.dtFlowrate;
            expVol=6*(1-exp(min(0,-(tFlow-pumpOffTime)/volTimeConstant)));
            flow=max(0,flowIn_lpm*(1-max(0, (tFlow-(pumpOffTime-rampDownTime))/rampDownTime)));
            noisyFlow=flow;
            
            % add data to ring buffers
            timeStart_d=datenum(2016, 10, 24, 12, 00, 00);
            for ii=1:testCase.fbr.nRbFlow
                flowTime_d=timeStart_d+tFlow(ii)/86400;
                if tFlow(ii)>=pumpOffTime
                    flow_lpm=flow(ii); % no noise after pump shut down
                else
                    flow_lpm=flow(ii)+20*randn; % flow rate with noise
                    noisyFlow(ii)=flow_lpm;
                end
                
                testCase.fbr=testCase.fbr.addFlowToRingbuffer(flowTime_d, flow_lpm);
                testCase.fbr=testCase.fbr.addExpVolToRingbuffer(flowTime_d, expVol(ii));
            end
            
            for ii=1:testCase.fbr.nRbVol
                volTime_d=timeStart_d+tVol(ii)/86400;
                vol_m3=vol(ii);
                testCase.fbr=testCase.fbr.addVolToRingbuffer(volTime_d, vol_m3);
            end
            
%             figure('Name', 'Plot of synthetic flowback data for test');
%             subplot(211);
%             plot(tFlow, noisyFlow);
%             subplot(212);
%             plot(tVol, vol);
            
            % mock object for pit and flowline volume
            fap=FLOWLINE_AND_PIT_OBJ();
            fap=fap.initialize([1; 0],6,0,1,0);
                        
            testCase.assertEqual(testCase.fbr.stateAtPrevTime, CirculationStates.notCirculating);
            testCase.assertEqual(testCase.fbr.cs.currentState, CirculationStates.notCirculating);
            testCase.assertFalse(testCase.fbr.pumpsOn);
            testCase.assertFalse(testCase.fbr.isInit);
            testCase.fbr=testCase.fbr.updateCirculationState(flowIn_lpm,depthBit_m, depthHole_m);
            testCase.fbr=testCase.fbr.updateFap(fap);
            pumpOffIdx=testCase.fbr.pumpOffIdxStore(testCase.fbr.idxStore-1);
            volOffIdx=testCase.fbr.volOffIdxStore(testCase.fbr.idxStore-1);
            testCase.verifyEqual(pumpOffIdx, expPumpOffIdx, 'The pumpOffIdx should be found correctly.')
            testCase.verifyEqual(volOffIdx, expVolOffIdx);
        end
        
        function updateFap_pumpStartNoisyFlowMeas_doNotUpdateK(testCase)

            flowIn_lpm=2000;
            depthBit_m=1000;
            depthHole_m=2000;
            
            pumpOffTime=300;
            rampDownTime=20;
            volTimeConstant=120;
            tVol=0:testCase.dtVolGL:testCase.timeDuration-testCase.dtVolGL;
            vol=6*(1-exp(min(0,-(tVol-pumpOffTime)/volTimeConstant)));
            tFlow=0:testCase.dtFlowrate:testCase.timeDuration-testCase.dtFlowrate;
            expVol=6*(1-exp(min(0,-(tFlow-pumpOffTime)/volTimeConstant)));
            flow=max(0,flowIn_lpm*(1-max(0, (tFlow-(pumpOffTime-rampDownTime))/rampDownTime)));
            noisyFlow=flow;
            
            % add data to ring buffers
            timeStart_d=datenum(2016, 10, 24, 12, 00, 00);
            rng('default'); % control randomness
            for ii=1:testCase.fbr.nRbFlow
                flowTime_d=timeStart_d+tFlow(ii)/86400;
                if tFlow(ii)>=pumpOffTime
                    flow_lpm=flow(ii); % no noise after pump shut down
                else
                    flow_lpm=flow(ii)+500*randn; % flow rate with noise
                    noisyFlow(ii)=flow_lpm;
                end
                
                testCase.fbr=testCase.fbr.addFlowToRingbuffer(flowTime_d, flow_lpm);
                testCase.fbr=testCase.fbr.addExpVolToRingbuffer(flowTime_d, expVol(ii));
            end
            
            for ii=1:testCase.fbr.nRbVol
                volTime_d=timeStart_d+tVol(ii)/86400;
                vol_m3=vol(ii);
                testCase.fbr=testCase.fbr.addVolToRingbuffer(volTime_d, vol_m3);
            end
            
%             figure('Name', 'Plot of synthetic flowback data for test');
%             subplot(211);
%             plot(tFlow, noisyFlow);
%             subplot(212);
%             plot(tVol, vol);
            
            % mock object for pit and flowline volume
            fap=FLOWLINE_AND_PIT_OBJ();
            fap=fap.initialize([1; 0],6,0,1,0);
            
            testCase.assertEqual(testCase.fbr.stateAtPrevTime, CirculationStates.notCirculating);
            testCase.assertEqual(testCase.fbr.cs.currentState, CirculationStates.notCirculating);
            testCase.assertFalse(testCase.fbr.pumpsOn);
            testCase.assertFalse(testCase.fbr.isInit);
            testCase.fbr=testCase.fbr.updateCirculationState(flowIn_lpm,depthBit_m, depthHole_m);
            testCase.fbr=testCase.fbr.updateFap(fap);
            testCase.assertTrue(testCase.fbr.pumpsOn);
            testCase.verifyEqual(testCase.fbr.kStore(testCase.fbr.idxStore-1), 0, 'The kFlowback constant should not have been updated');
        end
        
        function updateFap_pumpRampDown_doUpdateK(testCase)

            flowIn_lpm=2000;
            depthBit_m=1000;
            depthHole_m=2000;
            
            pumpOffTime=300;
            rampDownTime=20;
            volTimeConstant=120;
            tVol=0:testCase.dtVolGL:testCase.timeDuration-testCase.dtVolGL;
            vol=6*(1-exp(min(0,-(tVol-pumpOffTime)/volTimeConstant)));
            tFlow=0:testCase.dtFlowrate:testCase.timeDuration-testCase.dtFlowrate;
            expVol=6*(1-exp(min(0,-(tFlow-pumpOffTime)/volTimeConstant)));
            flow=max(0,flowIn_lpm*(1-max(0, (tFlow-(pumpOffTime-rampDownTime))/rampDownTime)));
            noisyFlow=flow;
            
            % add data to ring buffers
            timeStart_d=datenum(2016, 10, 24, 12, 00, 00);
            for ii=1:testCase.fbr.nRbFlow
                flowTime_d=timeStart_d+tFlow(ii)/86400;
                if tFlow(ii)>=pumpOffTime
                    flow_lpm=flow(ii); % no noise after pump shut down
                else
                    flow_lpm=flow(ii)+20*randn; % flow rate with noise
                    noisyFlow(ii)=flow_lpm;
                end
                
                testCase.fbr=testCase.fbr.addFlowToRingbuffer(flowTime_d, flow_lpm);
                testCase.fbr=testCase.fbr.addExpVolToRingbuffer(flowTime_d, expVol(ii));
            end
            
            for ii=1:testCase.fbr.nRbVol
                volTime_d=timeStart_d+tVol(ii)/86400;
                vol_m3=vol(ii);
                testCase.fbr=testCase.fbr.addVolToRingbuffer(volTime_d, vol_m3);
            end
            
%             figure('Name', 'Plot of synthetic flowback data for test');
%             subplot(211);
%             plot(tFlow, noisyFlow);
%             subplot(212);
%             plot(tVol, vol);
            
            % mock object for pit and flowline volume
            fap=FLOWLINE_AND_PIT_OBJ();
            fap=fap.initialize([1; 0],6,0,1,0);
            
            testCase.assertEqual(testCase.fbr.stateAtPrevTime, CirculationStates.notCirculating);
            testCase.assertEqual(testCase.fbr.cs.currentState, CirculationStates.notCirculating);
            testCase.assertFalse(testCase.fbr.pumpsOn);
            testCase.assertFalse(testCase.fbr.isInit);
            testCase.fbr=testCase.fbr.updateCirculationState(flowIn_lpm,depthBit_m, depthHole_m);
            testCase.fbr=testCase.fbr.updateFap(fap);
            testCase.assertTrue(testCase.fbr.pumpsOn);
            testCase.verifyGreaterThan(testCase.fbr.kStore(testCase.fbr.idxStore-1), 0, 'The kFlowback constant should have been updated');
        end
        
        function storeFlowbackData_missingData_correctPadding(testCase)
            import matlab.unittest.constraints.IsEqualTo
            import matlab.unittest.constraints.RelativeTolerance
            numTol=1e-5; % relative error which is tolerated
            
            flowIn_lpm = 2000;
            
            pumpOffTime = 120;
            rampDownTime = 20;
            volTimeConstant = 60;
            
            tEnd = ceil(testCase.timeDuration/3*2);
            
            tVol = 0:testCase.dtVolGL:tEnd;
            vol = 6*(1-exp(min(0,-(tVol-pumpOffTime)/volTimeConstant)));
            tFlow = 0:testCase.dtFlowrate:tEnd;
            expVol = 6*(1-exp(min(0,-(tFlow-pumpOffTime)/volTimeConstant))) + 0.5;
            flow = max(0,flowIn_lpm*(1-max(0, (tFlow-(pumpOffTime-rampDownTime))/rampDownTime)));
            noisyFlow = flow;
            
            % add data to ring buffers
            timeStart_d = datenum(2016, 10, 24, 12, 00, 00);
            flowTimeStore_d = zeros(1, numel(tFlow));
            for ii = 1:numel(tFlow)
                flowTime_d = timeStart_d+tFlow(ii)/86400;
                if tFlow(ii) >= pumpOffTime
                    flow_lpm = flow(ii); % no noise after pump shut down
                else
                    flow_lpm = flow(ii)+20*randn; % flow rate with noise
                    noisyFlow(ii) = flow_lpm;
                end
                flowTimeStore_d(ii) = flowTime_d;
                testCase.fbr = testCase.fbr.addFlowToRingbuffer(flowTime_d, flow_lpm);
                testCase.fbr = testCase.fbr.addExpVolToRingbuffer(flowTime_d, expVol(ii));
            end
            
            volTimeStore_d = zeros(1, numel(tVol));
            for ii = 1:numel(tVol)
                volTime_d = timeStart_d+tVol(ii)/86400;
                volTimeStore_d(ii) = volTime_d;
                vol_m3 = vol(ii);
                testCase.fbr = testCase.fbr.addVolToRingbuffer(volTime_d, vol_m3);
            end
            
%             figure('Name', 'Plot of synthetic flowback data for test');
%             subplot(211);
%             plot(tFlow, noisyFlow);
%             subplot(212);
%             plot(tVol, vol, tFlow, expVol);
%             legend('vol', 'expVol');
            
            testCase.fbr = testCase.fbr.storeFlowbackData;
            
            tFInAct = testCase.fbr.timeFlowInStore(1,:);
            fInAct = testCase.fbr.flowInStore(1,:);
            tVolAct = testCase.fbr.timeVolStore(1,:);
            volAct = testCase.fbr.volStore(1,:);
            tExpVolAct = testCase.fbr.timeExpVolStore(1,:);
            expVolAct = testCase.fbr.expVolStore(1,:);
            
            nValidVolSamples = numel(tVol);
            nValidFlowSamples = numel(tFlow);
            nInvalidVolSamples = testCase.fbr.nRbVol - nValidVolSamples;
            nInvalidFlowSamples = testCase.fbr.nRbFlow - nValidFlowSamples;
            
            tMissingVolStart = volTimeStore_d(1) - testCase.dtVolGL/86400*nInvalidVolSamples;
            tMissingFlowStart = flowTimeStore_d(1) - testCase.dtFlowrate/86400*nInvalidFlowSamples;
            
            tFInExp = [tMissingFlowStart:testCase.dtFlowrate/86400:flowTimeStore_d(1) flowTimeStore_d(2:end)];
            fInExp = [ones(1, nInvalidFlowSamples)*noisyFlow(1) noisyFlow];
            tVolExp = [tMissingVolStart:testCase.dtVolGL/86400:volTimeStore_d(1) volTimeStore_d(2:end)];
            volExp = [ones(1, nInvalidVolSamples)*vol(1) vol];
            tExpVolExp = [tMissingFlowStart:testCase.dtFlowrate/86400:flowTimeStore_d(1) flowTimeStore_d(2:end)];
            expVolExp = [ones(1, nInvalidFlowSamples)*expVol(1) expVol];
            
            testCase.verifyThat(tFInAct, IsEqualTo(tFInExp, 'Within', RelativeTolerance(numTol)));
            testCase.verifyThat(fInAct, IsEqualTo(fInExp, 'Within', RelativeTolerance(numTol)));
            testCase.verifyThat(tVolAct, IsEqualTo(tVolExp, 'Within', RelativeTolerance(numTol)));
            testCase.verifyThat(volAct, IsEqualTo(volExp, 'Within', RelativeTolerance(numTol)));
            testCase.verifyThat(tExpVolAct, IsEqualTo(tExpVolExp, 'Within', RelativeTolerance(numTol)));
            testCase.verifyThat(expVolAct, IsEqualTo(expVolExp, 'Within', RelativeTolerance(numTol)));
        end
        
        function updateInitState_inInit_isInitTrue(testCase)
            
            testCase.fbr=testCase.fbr.updateInitState;
            
            testCase.verifyTrue(testCase.fbr.isInit, 'Should still be in init state');
        end
        
        function updateInitState_doneInit_counterUpdatedAndDoneInit(testCase)

            % Fill up ringbuffers
            for ii=1:testCase.fbr.nRbFlow
                testCase.fbr=testCase.fbr.addFlowToRingbuffer(ii,ii);
                testCase.fbr=testCase.fbr.addVolToRingbuffer(ii,ii);
                testCase.fbr=testCase.fbr.addExpVolToRingbuffer(ii, ii);
            end
            
%             testCase.fbr=testCase.fbr.updateInitCounter;
            
            testCase.verifyFalse(testCase.fbr.isInit, 'Should not be in init state');
        end
        
        function calcParams_noData_retEmptyParams(testCase)

            k=testCase.fbr.calcParams;
            testCase.verifyEmpty(k);
        end
        
        function calcParams_oneDataSet_retCorParams(testCase)
            import matlab.unittest.constraints.IsEqualTo
            import matlab.unittest.constraints.RelativeTolerance
            numTol=20e-2; % relative error which is tolerated

            flowIn_lpm=2000;
            depthBit_m=1000;
            depthHole_m=2000;
            
            pumpOffTime=297;
            timeDelay=20;
            rampDownTime=20;
            volTimeConstant=60;
            V0Exp=6;
            tVol=0:testCase.dtVolGL:testCase.timeDuration-testCase.dtVolGL;
            vol=V0Exp*(1-exp(min(0,-(tVol-pumpOffTime-timeDelay)/volTimeConstant)));

            tFlow=0:testCase.dtFlowrate:testCase.timeDuration-testCase.dtFlowrate;
            expVol=V0Exp*(1-exp(min(0,-(tFlow-pumpOffTime-timeDelay)/volTimeConstant)));
            
            flow=max(0,flowIn_lpm*(1-max(0, (tFlow-(pumpOffTime-rampDownTime))/rampDownTime)));
            noisyFlow=flow;
            
            rng('default'); % control randomness
            % add data to ring buffers
            timeStart_d=datenum(2016, 10, 24, 12, 00, 00);
            for ii=1:testCase.fbr.nRbFlow
                flowTime_d=timeStart_d+tFlow(ii)/86400;
                if tFlow(ii)>=pumpOffTime
                    flow_lpm=flow(ii); % no noise after pump shut down
                else
                    flow_lpm=flow(ii)+20*randn; % flow rate with noise
                    noisyFlow(ii)=flow_lpm;
                end
                
                testCase.fbr=testCase.fbr.addFlowToRingbuffer(flowTime_d, flow_lpm);
                testCase.fbr=testCase.fbr.addExpVolToRingbuffer(flowTime_d, expVol(ii));
            end
            
            for ii=1:testCase.fbr.nRbVol
                volTime_d=timeStart_d+tVol(ii)/86400;
                vol(ii)=vol(ii)+0.1*randn;
                vol_m3=vol(ii);
                testCase.fbr=testCase.fbr.addVolToRingbuffer(volTime_d, vol_m3);
            end
            
%             figure('Name', 'Plot of synthetic flowback data for test');
%             subplot(211);
%             plot(tFlow, noisyFlow);
%             subplot(212);
%             plot(tVol, vol);
            
             % mock object for pit and flowline volume
            fap=FLOWLINE_AND_PIT_OBJ();
            fap=fap.initialize([1; 0],6,0,1,0);
            
            testCase.assertEqual(testCase.fbr.stateAtPrevTime, CirculationStates.notCirculating);
            testCase.assertEqual(testCase.fbr.cs.currentState, CirculationStates.notCirculating);
            testCase.assertFalse(testCase.fbr.pumpsOn);
            testCase.assertFalse(testCase.fbr.isInit);
            testCase.fbr=testCase.fbr.updateCirculationState(flowIn_lpm,depthBit_m, depthHole_m);
            [testCase.fbr, fap]=testCase.fbr.updateFap(fap);
            testCase.assertTrue(testCase.fbr.pumpsOn);
            
            k=fap.kFlowline;
            % verify with a numerical tolerance
%             testCase.verifyThat(1/k, IsEqualTo(cal;,...
%                 'Within', RelativeTolerance(numTol)),'Time-constant should be reasonable.');
        end
        
        function calcParams_singlePrecision_noWarning(testCase)
            flowIn_lpm=2000;
            depthBit_m=1000;
            depthHole_m=2000;
            
            pumpOffTime=297;
            timeDelay=20;
            rampDownTime=20;
            volTimeConstant=120;
            V0Exp=6;
            tVol=0:testCase.dtVolGL:testCase.timeDuration-testCase.dtVolGL;
            vol=V0Exp*(1-exp(min(0,-(tVol-pumpOffTime-timeDelay)/volTimeConstant)));
            tFlow=0:testCase.dtFlowrate:testCase.timeDuration-testCase.dtFlowrate;
            expVol=V0Exp*(1-exp(min(0,-(tFlow-pumpOffTime-timeDelay)/volTimeConstant)));
            flow=max(0,flowIn_lpm*(1-max(0, (tFlow-(pumpOffTime-rampDownTime))/rampDownTime)));
            noisyFlow=flow;
            
            % mock object for pit and flowline volume
            fap=FLOWLINE_AND_PIT_OBJ();
            fap=fap.initialize([1; 0],6,0,1,0);
            
            rng('default'); % control randomness
            % add data to ring buffers
            timeStart_d=datenum(2016, 10, 24, 12, 00, 00);
            for jj=1:2
                for ii=1:testCase.fbr.nRbFlow
                    flowTime_d=timeStart_d+tFlow(ii)/86400;
                    if tFlow(ii)>=pumpOffTime
                        flow_lpm=flow(ii); % no noise after pump shut down
                    else
                        flow_lpm=flow(ii)+20*randn; % flow rate with noise
                        noisyFlow(ii)=flow_lpm;
                    end
                    
                    testCase.fbr=testCase.fbr.addFlowToRingbuffer(flowTime_d, flow_lpm);
                    testCase.fbr=testCase.fbr.addExpVolToRingbuffer(flowTime_d, expVol(ii));
                end
                
                for ii=1:testCase.fbr.nRbVol
                    volTime_d=timeStart_d+tVol(ii)/86400;
                    vol(ii)=vol(ii)+0.1*randn;
                    vol_m3=vol(ii);
                    testCase.fbr=testCase.fbr.addVolToRingbuffer(volTime_d, vol_m3);
                end
                testCase.fbr=testCase.fbr.updateCirculationState(0, depthBit_m, depthHole_m);
                [testCase.fbr, ~]=testCase.verifyWarningFree(@() testCase.fbr.updateFap(fap));
            end
        end
        
        function getNTdLogicalIndex_currentIdxIsOne_returnCorrectIndicies(testCase)

            testCase.assertEqual(testCase.fbr.idxStore,1);
            idx=testCase.fbr.getNTdLogicalIndex;
            
            expIdx=false(testCase.fbr.nFbs,1);
            expIdx(1)=true;
            expIdx(end-testCase.fbr.params.nTd+2:end)=true(testCase.fbr.params.nTd-1,1);
            
            testCase.verifyEqual(idx,expIdx);
        end
        
        function getNTdLogicalIndex_currentIdxIsNTd_returnCorrectIndicies(testCase)

            testCase.fbr.idxStore=testCase.fbr.params.nTd;
            testCase.assertEqual(testCase.fbr.idxStore,testCase.fbr.params.nTd);
            idx=testCase.fbr.getNTdLogicalIndex;
            
            expIdx=false(testCase.fbr.nFbs,1);
            expIdx(1:testCase.fbr.params.nTd)=true(testCase.fbr.params.nTd,1);
            
            testCase.verifyEqual(idx,expIdx);
        end
        
        function getNTdLogicalIndexExcludingCurrent_curIdxIsOne_retCorIndicies(testCase)

            testCase.assertEqual(testCase.fbr.idxStore,1);
            idx=testCase.fbr.getNTdLogicalIndexExcludingCurrent;
            
            expIdx=false(testCase.fbr.nFbs,1);
            expIdx(end-testCase.fbr.params.nTd+1:end)=true(testCase.fbr.params.nTd,1);
            
            testCase.verifyEqual(idx,expIdx);
        end
        
        function getNTdLogicalIndexExcludingCurrent_curIdxNTd_retCorIndicies(testCase)
            testCase.fbr.idxStore=testCase.fbr.params.nTd;
            testCase.assertEqual(testCase.fbr.idxStore,testCase.fbr.params.nTd);
            idx=testCase.fbr.getNTdLogicalIndexExcludingCurrent;
            
            expIdx=false(testCase.fbr.nFbs,1);
            expIdx(1:testCase.fbr.params.nTd-1)=true(testCase.fbr.params.nTd-1,1);
            expIdx(end)=true;
            
            testCase.verifyEqual(idx,expIdx);
        end

        function getNTdLogicalIndexExcludingCurrent_gtNTd_retCorIndicies(testCase)
            
            testCase.fbr.idxStore=testCase.fbr.params.nTd+2;
            testCase.assertEqual(testCase.fbr.idxStore,testCase.fbr.params.nTd+2);
            idx=testCase.fbr.getNTdLogicalIndexExcludingCurrent;
            
            expIdx=false(testCase.fbr.nFbs,1);
            expIdx(2:testCase.fbr.params.nTd+1)=true(testCase.fbr.params.nTd,1);
                        
            testCase.verifyEqual(idx,expIdx);
        end
        
        function getExpDrainbackData_noDataAtIdxVol_returnEmpty(testCase)

            idxVol=1;
            [expVol, expVolTime]=testCase.fbr.getExpDrainbackData(idxVol);
            
            testCase.verifyEmpty(expVol);
            testCase.verifyEmpty(expVolTime);
        end
        
        function getExpDrainbackData_idxOutOfBounds_returnEmpty(testCase)

            idxVol=1000;
            [expVol, expVolTime]=testCase.fbr.getExpDrainbackData(idxVol);
            
            testCase.verifyEmpty(expVol);
            testCase.verifyEmpty(expVolTime);
        end
        
        function analyzeRamp_noRampDown_returnEmpty(testCase)

            fi=zeros(1,300);
            fi(end)=30;
            
            rIdx=testCase.fbr.analyzeRamp(0,fi, 'down');
            
            testCase.verifyEmpty(rIdx);
        end
        
        function analyzeRamp_singleRampWoNoise_returnCorRIdxAndQDrill(testCase)
            
            n=ceil(testCase.timeDuration/testCase.dtFlowrate); % n samples
            fiTime_d=zeros(1,n);
            
            pumpOffTime=297;
            rampDownTime=20;
            flowIn_lpm=3000;
            tFlow=0:testCase.dtFlowrate:testCase.timeDuration-testCase.dtFlowrate;
            flow=max(0,flowIn_lpm*(1-max(0, (tFlow-(pumpOffTime-rampDownTime))/rampDownTime)));
            expPumpOffIdx=find(flow<flowIn_lpm,1)-1;
            
            % add data to ring buffers
            timeStart_d=datenum(2016, 10, 24, 12, 00, 00);
            for ii=1:n
                fiTime_d(ii)=timeStart_d+tFlow(ii)/86400;
            end
            fi_lpm=flow; % flow-rate without noise
            
            [~, qDrill_lpm, rIdx]=testCase.fbr.analyzeRamp(fiTime_d,fi_lpm, 'down');
            
            testCase.verifyGreaterThanOrEqual(rIdx, expPumpOffIdx-10);
            testCase.verifyLessThanOrEqual(rIdx, expPumpOffIdx);
            testCase.verifyEqual(qDrill_lpm, flowIn_lpm);
        end
        
        function analyzeRamp_singleRampWithNoise_returnCorRIdx(testCase)
            
            n=ceil(testCase.timeDuration/testCase.dtFlowrate); % n samples
            fi_lpm=zeros(1,n);
            fiTime_d=zeros(1,n);
            
            pumpOffTime=297;
            rampDownTime=20;
            flowIn_lpm=3000;
            tFlow=0:testCase.dtFlowrate:testCase.timeDuration-testCase.dtFlowrate;
            flow=max(0,flowIn_lpm*(1-max(0, (tFlow-(pumpOffTime-rampDownTime))/rampDownTime)));
            expPumpOffIdx=find(flow<flowIn_lpm,1)-1;
            
            rng('default'); % control randomness
            % add data to ring buffers
            timeStart_d=datenum(2016, 10, 24, 12, 00, 00);
            for ii=1:n
                fiTime_d(ii)=timeStart_d+tFlow(ii)/86400;
                if tFlow(ii)>=pumpOffTime
                    fi_lpm(ii)=flow(ii); % no noise after pump shut down
                else
                    fi_lpm(ii)=flow(ii)+20*randn; % flow rate with noise
                end
            end
            
            [~, qDrill_lpm, rIdx]=testCase.fbr.analyzeRamp(fiTime_d,fi_lpm, 'down');
            
            testCase.verifyGreaterThanOrEqual(rIdx, expPumpOffIdx-10);
            testCase.verifyLessThanOrEqual(rIdx, expPumpOffIdx);
            % allow +/- 50 lpm difference in qDrill from actual flow-rate.
            testCase.verifyGreaterThanOrEqual(qDrill_lpm, flowIn_lpm-50);
            testCase.verifyLessThanOrEqual(qDrill_lpm, flowIn_lpm+50);
        end
        
        function analyzeRamp_singleRampNotSS_retEmpty(testCase)
            
            n=ceil(testCase.timeDuration/testCase.dtFlowrate); % n samples
            fi_lpm=zeros(1,n);
            fiTime_d=zeros(1,n);
            
            pumpOffTime=60;
            rampDownTime=20;
            flowIn_lpm=3000;
            tFlow=0:testCase.dtFlowrate:testCase.timeDuration-testCase.dtFlowrate;
            flow=max(0,flowIn_lpm*(1-max(0, (tFlow-(pumpOffTime-rampDownTime))/rampDownTime)));
            
            rng('default'); % control randomness
            % add data to ring buffers
            timeStart_d=datenum(2016, 10, 24, 12, 00, 00);
            for ii=1:n
                fiTime_d(ii)=timeStart_d+tFlow(ii)/86400;
                if tFlow(ii)>=pumpOffTime
                    fi_lpm(ii)=flow(ii); % no noise after pump shut down
                else
                    fi_lpm(ii)=flow(ii)+20*randn; % flow rate with noise
                end
            end
            
            [~, qDrill_lpm, rIdx]=testCase.fbr.analyzeRamp(fiTime_d,fi_lpm, 'down');
            
            testCase.verifyEmpty(rIdx);
            testCase.verifyEmpty(qDrill_lpm);
        end
        
        function analyzeRamp_stageDownWoNoise_returnCorRIdxAndQDrill(testCase)
            
            n=ceil(testCase.timeDuration/testCase.dtFlowrate); % n samples
            fiTime_d=zeros(1,n);
            
            pumpOffTime=297;
            rampDownSlope=100; % [lpm/s]
            flowIn_lpm=3000;
            tFlow=0:testCase.dtFlowrate:testCase.timeDuration-testCase.dtFlowrate;
            fi_lpm=ones(size(tFlow))*flowIn_lpm;
            
            % add data to ring buffers
            timeStart_d=datenum(2016, 10, 24, 12, 00, 00);
            fiTime_d(1)=timeStart_d+tFlow(1)/86400;
            for ii=2:n
                fiTime_d(ii)=timeStart_d+tFlow(ii)/86400;
                dt_s=diff(tFlow(ii-1:ii));
                if tFlow(ii)>=pumpOffTime && fi_lpm(ii-1) > 1500 || tFlow(ii) >= pumpOffTime+60
                    fi_lpm(ii)=max(0,fi_lpm(ii-1)-rampDownSlope*dt_s);
                else
                    fi_lpm(ii)=fi_lpm(ii-1);
                end
            end
            
            expPumpOffIdx=find(fi_lpm<flowIn_lpm,1)-1;
            
            [~, qDrill_lpm, rIdx]=testCase.fbr.analyzeRamp(fiTime_d,fi_lpm, 'down');
            
            testCase.verifyGreaterThanOrEqual(rIdx, expPumpOffIdx-10);
            testCase.verifyLessThanOrEqual(rIdx, expPumpOffIdx);
            testCase.verifyEqual(qDrill_lpm, flowIn_lpm);
        end
        
        function analyzeRamp_stageDownWithNoise_returnCorRIdx(testCase)
            
            n=ceil(testCase.timeDuration/testCase.dtFlowrate); % n samples
            fiTime_d=zeros(1,n);
            
            pumpOffTime=297;
            rampDownSlope=100; % [lpm/s]
            flowIn_lpm=3000;
            tFlow=0:testCase.dtFlowrate:testCase.timeDuration-testCase.dtFlowrate;
            fi_lpm=ones(size(tFlow))*flowIn_lpm;
            
            % add data to ring buffers
            timeStart_d=datenum(2016, 10, 24, 12, 00, 00);
            fiTime_d(1)=timeStart_d+tFlow(1)/86400;
            for ii=2:n
                fiTime_d(ii)=timeStart_d+tFlow(ii)/86400;
                dt_s=diff(tFlow(ii-1:ii));
                if tFlow(ii)>=pumpOffTime && fi_lpm(ii-1) > 1500 || tFlow(ii) >= pumpOffTime+60
                    fi_lpm(ii)=max(0,fi_lpm(ii-1)-rampDownSlope*dt_s);
                else
                    fi_lpm(ii)=fi_lpm(ii-1);
                end
            end
            
            % add noise to flow meas.
            rng('default');
            mask=fi_lpm>0;
            fi_lpm(mask)=fi_lpm(mask)+20*randn(1,sum(mask));
            
            expPumpOffIdx=find(fi_lpm<(flowIn_lpm-100),1)-1;
            
            [~, qDrill_lpm, rIdx]=testCase.fbr.analyzeRamp(fiTime_d,fi_lpm, 'down');
            
            testCase.verifyGreaterThanOrEqual(rIdx, expPumpOffIdx-10);
            testCase.verifyLessThanOrEqual(rIdx, expPumpOffIdx);
            testCase.verifyGreaterThanOrEqual(qDrill_lpm, flowIn_lpm-50);
            testCase.verifyLessThanOrEqual(qDrill_lpm, flowIn_lpm+50);
        end
        
        function analyzeRamp_longStageDownWithNoise_returnCorRIdx(testCase)
            
            n=ceil(testCase.timeDuration/testCase.dtFlowrate); % n samples
            fiTime_d=zeros(1,n);
            
            pumpOffTime=50;
            rampDownSlope=100; % [lpm/s]
            flowIn_lpm=3000;
            stageFlow_lpm=1500;
            tFlow=0:testCase.dtFlowrate:testCase.timeDuration-testCase.dtFlowrate;
            fi_lpm=ones(size(tFlow))*flowIn_lpm;
            
            % add data to ring buffers
            timeStart_d=datenum(2016, 10, 24, 12, 00, 00);
            fiTime_d(1)=timeStart_d+tFlow(1)/86400;
            for ii=2:n
                fiTime_d(ii)=timeStart_d+tFlow(ii)/86400;
                dt_s=diff(tFlow(ii-1:ii));
                if tFlow(ii)>=pumpOffTime && fi_lpm(ii-1) > stageFlow_lpm || tFlow(ii) >= pumpOffTime+360
                    fi_lpm(ii)=max(0,fi_lpm(ii-1)-rampDownSlope*dt_s);
                else
                    fi_lpm(ii)=fi_lpm(ii-1);
                end
            end
            
            % add noise to flow meas.
            mask=fi_lpm>0;
            fi_lpm(mask)=fi_lpm(mask)+20*randn(1,sum(mask));
            
            expPumpOffIdx=find(fi_lpm<(stageFlow_lpm-100),1)-2;
            
            [~, qDrill_lpm, rIdx]=testCase.fbr.analyzeRamp(fiTime_d,fi_lpm, 'down');
            
            testCase.verifyGreaterThanOrEqual(rIdx, expPumpOffIdx-10);
            testCase.verifyLessThanOrEqual(rIdx, expPumpOffIdx);
            testCase.verifyGreaterThanOrEqual(qDrill_lpm, stageFlow_lpm-50);
            testCase.verifyLessThanOrEqual(qDrill_lpm, stageFlow_lpm+50);
        end
        
        function analyzeRamp_smallHumpWithNoise_returnEmpty(testCase)
            
            n=ceil(testCase.timeDuration/testCase.dtFlowrate); % n samples
            fiTime_d=zeros(1,n);
            
            pumpOnTime=700;
            pumpOnDuration=100;
            rampSlope=100; % [lpm/s]
            flowIn_lpm=500;
            tFlow=0:testCase.dtFlowrate:testCase.timeDuration-testCase.dtFlowrate;
            fi_lpm=zeros(size(tFlow));
            
            % add data to ring buffers
            timeStart_d=datenum(2016, 10, 24, 12, 00, 00);
            fiTime_d(1)=timeStart_d+tFlow(1)/86400;
            for ii=2:n
                fiTime_d(ii)=timeStart_d+tFlow(ii)/86400;
                dt_s=diff(tFlow(ii-1:ii));
                if tFlow(ii)>=pumpOnTime && tFlow(ii)<pumpOnTime+pumpOnDuration && fi_lpm(ii-1) < flowIn_lpm
                    fi_lpm(ii)=min(flowIn_lpm,fi_lpm(ii-1)+rampSlope*dt_s);
                elseif tFlow(ii)>=pumpOnTime+pumpOnDuration
                    fi_lpm(ii)=max(0,fi_lpm(ii-1)-rampSlope*dt_s);
                else
                    fi_lpm(ii)=fi_lpm(ii-1);
                end
            end
            
            % add noise to flow meas.
            mask=fi_lpm>0;
            fi_lpm(mask)=fi_lpm(mask)+20*randn(1,sum(mask));
            
            [~, qDrill_lpm, rIdx]=testCase.fbr.analyzeRamp(fiTime_d,fi_lpm, 'down');
            
            testCase.verifyEmpty(rIdx);
            testCase.verifyEmpty(qDrill_lpm);
        end
        
        function getFlowbackStatistics_corInput_corOutput(testCase)
            qDrill_lpm = 3000;
            volFlowback = 3;
            nFbs = 6;
            for ii=1:nFbs
                testCase.fbr.qStore(ii) = qDrill_lpm;
                testCase.fbr.V0Store(ii) = volFlowback;
            end
            testCase.fbr.idxStore = nFbs + 1;
            [meanV0, stdV0] = testCase.fbr.getFlowbackStatistics(qDrill_lpm);
            
            testCase.verifyEqual(meanV0, volFlowback);
            testCase.verifyEqual(stdV0, 0);
        end
        
        function getFlowbackStatistics_noMatchingFlowRate_returnEmpty(testCase)
            qDrill_lpm = 3000;
            volFlowback = 3;
            nFbs = 6;
            for ii=1:nFbs
                testCase.fbr.qStore(ii) = qDrill_lpm + 400;
                testCase.fbr.V0Store(ii) = volFlowback;
            end
            testCase.fbr.idxStore = nFbs + 1;
            [meanV0, stdV0] = testCase.fbr.getFlowbackStatistics(qDrill_lpm);
            
            testCase.verifyEmpty(meanV0);
            testCase.verifyEmpty(stdV0);
        end
        
        function getFlowbackStatistics_oneOutOfBoundFlow_retCorVal(testCase)
            qDrill_lpm = 3000;
            nFbs = 6;
            
            testCase.fbr.qStore(1:nFbs) = ones(1, nFbs)*qDrill_lpm;
            testCase.fbr.V0Store(1:nFbs) = 2:nFbs+1; % [2 3 4 5 6 7]
            
            testCase.fbr.qStore(nFbs-1) = qDrill_lpm - 2*testCase.fbr.qWin_lpm; % this should be neglected
            
            testCase.fbr.idxStore = nFbs + 1;
            [meanV0, stdV0] = testCase.fbr.getFlowbackStatistics(qDrill_lpm);
            
            expMeanV0 = mean([4 5 7]);
            expStdV0 = std([4 5 7]);
            testCase.verifyEqual(meanV0, expMeanV0);
            testCase.verifyEqual(stdV0, expStdV0);
        end
        
        function compareFlowbackVolumeWithPrev_noPrevFb_returnTrue(testCase)
            qDrill_lpm = 3000;
            newV0 = 6;
                        
            testCase.fbr.idxStore = 1;
            isVolumeWithinTolerance = testCase.fbr.compareFlowbackVolumeWithPrev(newV0, qDrill_lpm);
            
            testCase.verifyTrue(isVolumeWithinTolerance);
        end
        
        function pickDelayedSamples_unevenSampling_interpolate(testCase)
            
            flow_lpm = 2000;
            
            timeStart_d = datenum(2017, 03, 31, 14, 50, 00);
            dtFlow = ones(1, testCase.fbr.nRbFlow);
            dtFlow(1:3:end) = 2;
            tFlow = cumsum(dtFlow);
            
            for ii=1:testCase.fbr.nRbFlow
                flowTime_d=timeStart_d+tFlow(ii)/86400;
                testCase.fbr=testCase.fbr.addFlowToRingbuffer(flowTime_d, flow_lpm);
            end
            
            nDelays = numel(testCase.fbr.delayArray_samples);
            expFlowTime_d = zeros(1, nDelays + 1);
            
            for jj = 1:nDelays
                expFlowTime_d(jj) = flowTime_d - testCase.fbr.delayArray_samples(jj)*testCase.fbr.timeSampling_s/86400;
            end
            expFlowTime_d(end) = flowTime_d;
            
            [actTime_d, ~] = testCase.fbr.pickDelayedSamples;
            testCase.verifyEqual(datestr(actTime_d), datestr(expFlowTime_d));
        end

        function verifyExpVolCurrentConClippedAndScaled(testCase)
            volConClipped = [0.1 2 3];
            expVolCurrentConClipped = [1 2 3];
            result = FLOWBACK_RECORDER_OBJ.scaleExpVolCurve(volConClipped, expVolCurrentConClipped);
            testCase.verifyEqual(result(1), 0);
            
        end
        
    
    end
    
end

