classdef filterderivativeTest < matlab.unittest.TestCase
    %testing the filter derivative algorithm on a sinus
       
    properties
        dataVec % input signal
        derTrue % true derivative
        derAlg  % derivative produced by algorithm to be tested
        Tfilt_s % filter time constant
        dt_s    % time step in time series
        t_s     % time
        N       % number of elements in test-dataset
        dobj    % object subject to test
        testEx  % test 
        constant_dt
    end
    
    methods (TestClassSetup)
        function initFunc(testCase)
            testCase.constant_dt = true;
%            testCase.constant_dt = false;
            testCase.testEx  = 1;%1=sinus, 2=ramp
            
            tend_s           = 100;
            testCase.Tfilt_s = 0.5;
            testCase.dt_s    = 0.01;%Time step in simulation
            if testCase.constant_dt,
                testCase.t_s     = testCase.dt_s:testCase.dt_s:tend_s;
            else
                Nt               = floor(tend_s/testCase.dt_s);
                testCase.t_s     = zeros(1,2*Nt);  
                ii               = 1;
                while testCase.t_s(ii) < tend_s,
                    dt_s                = testCase.dt_s*2*rand;
                    if dt_s < 0.2*testCase.dt_s,
                        dt_s = 0;
                    end
                    testCase.t_s(ii+1)  = testCase.t_s(ii) + dt_s;   
                    ii                  = ii + 1;
                end
                testCase.t_s    = testCase.t_s(1:ii-1);
            end
            testCase.N       = length(testCase.t_s);
            switch testCase.testEx,
                 case 1,
                    amp              = 1;
                    tper_s           = 20;
                    testCase.dataVec = amp*sin(2*pi*testCase.t_s/tper_s);
                    testCase.derTrue = amp*2*pi/tper_s*cos(2*pi*testCase.t_s/tper_s);
                case 2,
                    rop_mps          = 30/3600;
                    testCase.dataVec = rop_mps*testCase.t_s;
                    testCase.derTrue = rop_mps*ones(1,testCase.N);                    
            end
            figure(1)
            subplot(321)
            plot(testCase.t_s,testCase.dataVec);
            grid
            title(['Input time series']);
            ylabel('Amp []')
            subplot(322)
            plot(testCase.t_s(2:testCase.N),diff(testCase.t_s));
            grid
            title('Time step')
            xlabel('Time')
            ylabel('Time step [sec]')
        end
    end
    
    methods (TestMethodSetup)
        function initBeforeEachTest(testCase)
            testCase.dobj    = FILTERDERIVATIVE_OBJ(testCase.Tfilt_s); % init with data vector
            testCase.derAlg  = zeros(1,testCase.N);
            testCase.addTeardown(@clear, 'testCase.dobj');
        end
    end
       
    methods (Test)
        function smoothderivative1storder_correctInit_correctOutput(testCase)
            import matlab.unittest.constraints.IsLessThan
            import matlab.unittest.constraints.IsEqualTo
            import matlab.unittest.constraints.RelativeTolerance
            import matlab.unittest.constraints.AbsoluteTolerance
            numTol=0.1; % relative error which is tolerated
            rangeTrue = max(testCase.derTrue) - min(testCase.derTrue);
            
            for ii = 1:testCase.N-1
                [testCase.dobj,testCase.derAlg(ii+1)] = testCase.dobj.smoothderivative1storder(testCase.dataVec(ii+1), testCase.t_s(ii+1)/86400);
            end
            
            % verify with a numerical tolerance
            startindex  = round(50/testCase.dt_s);
            
            delaytime_s = findDelay (testCase.derTrue,testCase.derAlg,testCase.dt_s);
            delayindex  = floor(delaytime_s/testCase.dt_s);
            delayTrue   = testCase.derTrue;
            delayTrue(delayindex:testCase.N-1) = delayTrue(1:testCase.N-delayindex);            
            ampfilter   = std(testCase.derAlg)/std(delayTrue(delayindex:testCase.N-1));           
            subplot(323)
            plot(testCase.t_s,testCase.derTrue,testCase.t_s,testCase.derAlg, testCase.t_s, delayTrue);
            grid
            legend('True der','1st order filter','Phase shifted true der')
            ylabel('Amp []')
            subplot(325)
            plot(testCase.t_s,testCase.derTrue - testCase.derAlg, testCase.t_s, delayTrue - testCase.derAlg);
            grid
            legend('True der - 1st order filter','Phase shifted true der - 1st order filter')
            ylabel('Error []')
            subplot(323)

            if testCase.testEx==1,
                testCase.verifyThat(mean(testCase.derAlg(startindex:testCase.N)),...
                    IsEqualTo(mean(testCase.derTrue(startindex:testCase.N)),...
                    'Within', AbsoluteTolerance(numTol*rangeTrue)), 'Mean value should be the same');
                testCase.verifyThat(var(testCase.derAlg(startindex:testCase.N)),...
                    IsEqualTo(var(testCase.derTrue(startindex:testCase.N)),...
                    'Within', RelativeTolerance(numTol)), 'Variance value should be the same');           
                    testCase.verifyThat(delaytime_s,IsLessThan(5), 'Delay should be less than 5 second');
                    testCase.verifyThat(ampfilter, IsEqualTo(1,'Within', RelativeTolerance(numTol)), 'Filter gain should be 1');
                title(['Calculated delay 1st order filter = ' num2str(delaytime_s,'%.2f') ' sec, filter gain = ' num2str(ampfilter,'%.2f')]);
            else
                testCase.verifyThat(mean(testCase.derAlg(startindex:testCase.N)),...
                    IsEqualTo(mean(testCase.derTrue(startindex:testCase.N)),...
                    'Within', RelativeTolerance(numTol)), 'Mean value should be the same');
                title(['1st order filter']);
            end
        end

        function smoothderivative2ndorder_correctInit_correctOutput(testCase)
            import matlab.unittest.constraints.IsLessThan
            import matlab.unittest.constraints.IsEqualTo
            import matlab.unittest.constraints.RelativeTolerance
            import matlab.unittest.constraints.AbsoluteTolerance
            numTol      = 0.1; % relative error which is tolerated
            rangeTrue   = max(testCase.derTrue) - min(testCase.derTrue);
            
            for ii = 1:testCase.N-1
                [testCase.dobj,testCase.derAlg(ii+1)] = testCase.dobj.smoothderivative2ndorder(testCase.dataVec(ii+1), testCase.t_s(ii+1)/86400);
            end
            
            % verify with a numerical tolerance
            startindex  = round(50/testCase.dt_s);
            delaytime_s = findDelay (testCase.derTrue,testCase.derAlg,testCase.dt_s);
            delayindex  = floor(delaytime_s/testCase.dt_s);
            delayTrue   = testCase.derTrue;
            delayTrue(delayindex:testCase.N-1) = delayTrue(1:testCase.N-delayindex);
            ampfilter   = std(testCase.derAlg)/std(delayTrue(delayindex:testCase.N-1));
            subplot(324)
            plot(testCase.t_s,testCase.derTrue,testCase.t_s,testCase.derAlg, testCase.t_s, delayTrue);
            grid
            legend('True der','2nd order filter','Phase shifted true der')
            xlabel('Time [sec]')
            ylabel('Amp []')
            subplot(326)
            plot(testCase.t_s,testCase.derTrue-testCase.derAlg, testCase.t_s, delayTrue-testCase.derAlg);
            grid
            legend('True signal - Lowpass filter','Phase shifted true signal - Lowpass filter')
            ylabel('Error []')
            subplot(324)

            if testCase.testEx==1,
                testCase.verifyThat(mean(testCase.derAlg(startindex:testCase.N)),...
                    IsEqualTo(mean(testCase.derTrue(startindex:testCase.N)),...
                    'Within', AbsoluteTolerance(numTol*rangeTrue)), 'Mean value should be the same');
                testCase.verifyThat(var(testCase.derAlg(startindex:testCase.N)),...
                    IsEqualTo(var(testCase.derTrue(startindex:testCase.N)),...
                    'Within', RelativeTolerance(numTol)), 'Variance value should be the same');
                testCase.verifyThat(delaytime_s,IsLessThan(5), 'Delay should be less than 5 second');
                testCase.verifyThat(ampfilter, IsEqualTo(1,'Within', RelativeTolerance(numTol)), 'Filter gain should be 1');
                title(['Calculated delay 2nd order filter = ' num2str(delaytime_s,'%.2f') ' sec, filter gain = ' num2str(ampfilter,'%.2f')]);
            else
                testCase.verifyThat(mean(testCase.derAlg(startindex:testCase.N)),...
                    IsEqualTo(mean(testCase.derTrue(startindex:testCase.N)),...
                    'Within', RelativeTolerance(numTol)), 'Mean value should be the same');
                title(['2nd order filter']);
            end    
        end
    end    
end

function delaytime = findDelay(ser1, ser2, dt_s)
    Ns=length(ser1);
    delayindex = 0;
    mindevmean = mean(abs(ser1-ser2));
    L = round(20/dt_s);
    for ii=1:L,
        ts1=ser1(1:Ns-L);
        ts2=ser2(ii:Ns-L+ii-1);
        devmean = mean(abs(ser1(1:Ns-L)-ser2(ii:Ns-L+ii-1)));
        if devmean < mindevmean,
            delayindex = ii;
            mindevmean = devmean;
        end
    end
    delaytime = delayindex*dt_s;
end
