
% Define vectors
qStore =[513.9; 514.9; 2327.9; 2328.5; 2413.1; 2003.4; 2004.3; 1587.9; 1818.4; 2003.1; 2010.7; 2191.6; 2379.8; 2373.0; 2375.4; 2412.8; 2003.6; 2265.2]
V0Store = [8.6614; 8.6614; 6.1254; 2.1830; 6.1353; 5.8042; 5.8077; 3.2843; 4.1804; 4.9082; 4.8493; 4.8341; 4.4742; 5.3761; 4.2060; 4.7376; 3.4607; 5.4029]

actually_used_meanV0Coeffs = V0Store'/[qStore ones(length(qStore), 1)]'
meanV0Coeffs = actually_used_meanV0Coeffs %[0.1, 0.1];
kDb_m3PerLpm = meanV0Coeffs(1)
kV_m3 = meanV0Coeffs(2)
