classdef evaluateRealtimeObjTest < matlab.unittest.TestCase
    
    properties
        er
        missingValCode
        initTime_d
    end
    
    methods (TestMethodSetup)
        function initFunc(testCase)
            
            testCase.er = EVALUATE_REALTIME_OBJ();
            testCase.initTime_d = datenum(2017, 03, 27, 09, 49, 00);
            testCase.er = testCase.er.initialize(int32(AlarmState.InputError));
            testCase.missingValCode = -0.99925;
            testCase.addTeardown(@clear, 'testCase.er');
        end
    end
    
    methods (Test)
        function updateAlarmCount_onlyInit_noAlarmCount(testCase)
            
            testCase.er = testCase.er.updateAlarmCount;
            testCase.verifyEqual(testCase.er.nTotalAlarms, 0);
        end
        
        function addAlarm_oneAlarmAfterInit_ret99(testCase)
            
            timeStep_s = 5;
            alarmState = int32(AlarmState.Alarm);
            currentTime_d = testCase.initTime_d + timeStep_s/86400;
            
            testCase.er = testCase.er.updateTimeCount(alarmState, timeStep_s);
            testCase.er = testCase.er.addAlarmIfNotPitSlowGainAlarm(currentTime_d, int32(AlarmType.PitOnlyInfluxAlarm), alarmState);
            testCase.er = testCase.er.calculateNumberOfAlarmsPer12Hours;
                        
            testCase.verifyEqual(testCase.er.nTotalAlarms, 1);
            testCase.verifyEqual(testCase.er.nAlarmsPer12Hours, testCase.er.maxNumberOfAlarms);
        end
        
        function addAlarm_addPitInfluxAlarm_countOnePitOnlyInfluxAlarm(testCase)
            
            timeStep_s = 5;
            alarmState = int32(AlarmState.Alarm);
            currentTime_d = testCase.initTime_d + timeStep_s/86400;
            alarmEnumNumber = int32(AlarmType.PitOnlyInfluxAlarm);
            
            testCase.er = testCase.er.updateTimeCount(alarmState, timeStep_s);
            testCase.er = testCase.er.addAlarmIfNotPitSlowGainAlarm(currentTime_d, alarmEnumNumber, alarmState);
            testCase.er = testCase.er.calculateNumberOfAlarmsPer12Hours;
                        
            alarmTypeIndex = 14;
            testCase.verifyEqual(testCase.er.alarmTypeCount(alarmTypeIndex), 1)
        end
        
        function addAlarm_addPitLossAlarm_countOnePitOnlyLossAlarm(testCase)
            
            timeStep_s = 5;
            alarmState = int32(AlarmState.Alarm);
            currentTime_d = testCase.initTime_d + timeStep_s/86400;
            alarmEnumNumber = int32(AlarmType.PitOnlyLossAlarm);
            
            testCase.er = testCase.er.updateTimeCount(alarmState, timeStep_s);
            testCase.er = testCase.er.addAlarmIfNotPitSlowGainAlarm(currentTime_d, alarmEnumNumber, alarmState);
            testCase.er = testCase.er.calculateNumberOfAlarmsPer12Hours;
                    
            alarmTypeIndex = 2;
            testCase.verifyEqual(testCase.er.alarmTypeCount(alarmTypeIndex), 1)
        end
        
        function updateTimecount_noActivity_returnZero(testCase)
        
            currentTime_d = testCase.initTime_d + 1;
            testCase.er = testCase.er.updateTimeCount(int32(AlarmState.Disabled), currentTime_d);
            testCase.verifyEqual(testCase.er.upTime_d, 0)
        end
        
        function updateTimeCount_oneTimeStep_incrementTotalTime(testCase)
            
            timeStep_s = 86400;
            alarmState = int32(AlarmState.Normal);
            testCase.er = testCase.er.updateTimeCount(alarmState, timeStep_s);
            testCase.verifyEqual(testCase.er.totalTime_d, timeStep_s/86400);
        end
                
        function updateTimeCount_oneTimeStepDownTime_incrementDownTime(testCase)
            
            timeStep_s = 86400;
            alarmState = int32(AlarmState.Disabled);
            testCase.er = testCase.er.updateTimeCount(alarmState, timeStep_s);
            testCase.verifyEqual(testCase.er.downTime_d, timeStep_s/86400);
        end
        
        function updateTimeCount_oneTimeStepUpTime_incrementUpTime(testCase)
            
            timeStep_s = 86400;
            alarmState = int32(AlarmState.Normal);
            testCase.er = testCase.er.updateTimeCount(alarmState, timeStep_s);
            testCase.verifyEqual(testCase.er.upTime_d, timeStep_s/86400);
        end
        
        function generateOutput_noChangeInAlarmType_corMapOfAlarmTypeInOutput(testCase)
            % this test will fail if the AlarmType enum class has changed.
            
            output = testCase.er.generateOutput;
            fNames = fieldnames(output);
            fNames(1:6) = [];
            nFields = size(fNames, 1);
                                    
            [m, s] = enumeration('AlarmType');
            cellsWithAlarm = strfind(s, 'Alarm');
            alarmInOutput = false(size(m));
            for ii = 1:size(m, 1)
                if ~isempty(cellsWithAlarm{ii})
                    for jj = 1:nFields
                        if strcmp(s{ii}, fNames{jj})
                            alarmInOutput(ii) = true;
                        end
                    end
                else
                    alarmInOutput(ii) = true;
                end
            end
            
            testCase.verifyTrue(all(alarmInOutput), 'The AlarmType enumeration class has changed.')
        end
        
        function step_twoAlarmsIn24Hours_returnCorrectNumber(testCase)
            
            timeStep_s = 5;
            alarmState = int32(AlarmState.Alarm);
            currentTime_d = testCase.initTime_d + timeStep_s/86400;
            alarmEnumNumber = int32(AlarmType.PitOnlyInfluxAlarm);
            
            testCase.er = testCase.er.step(currentTime_d, timeStep_s, alarmState, alarmEnumNumber); % alarm at start
            currentTime_d = currentTime_d + timeStep_s/86400;
            testCase.er = testCase.er.step(currentTime_d, timeStep_s, int32(AlarmState.Normal), int32(AlarmType.Normal));
            largeTimeStep_s = 86000;
            currentTime_d = currentTime_d + largeTimeStep_s/86400;
            testCase.er = testCase.er.step(currentTime_d, largeTimeStep_s, alarmState, alarmEnumNumber); % alarm at end
            
            testCase.assertEqual(testCase.er.nTotalAlarms, 2);
            testCase.verifyEqual(testCase.er.nAlarmsLast12Hours, 1);
        end
        
        function step_PitSlowGainAlarm_doNotCount(testCase)
            
            timeStep_s = 5;
            alarmState = int32(AlarmState.Alarm);
            currentTime_d = testCase.initTime_d + timeStep_s/86400;
            alarmEnumNumber = int32(AlarmType.PitSlowGainAlarm);
            
            testCase.er = testCase.er.step(currentTime_d, timeStep_s, alarmState, alarmEnumNumber); % alarm at start
                        
            testCase.verifyEqual(testCase.er.nTotalAlarms, 0);
        end
    end
end
