%Simulate flow back, flow line volume and pit volume
clear
close all

Nconn=10;
Time1Conn=30*60;
tend=Nconn*Time1Conn;%sec
dt=1;%0.05;%sec
t=dt:dt:tend;
N=length(t);

%Case parameters
L=5000;%m
MWmud_sg            = 1.31;
PVmud_cP            = 17;
YPmud_lb100sqft     = 0.1*23;
tauymud_Pa          = 0.5*7;
ID_dp=4.8;%in
OD_dp=5.5;%in
ID_cs=10;%in
Adp=pi/4*(ID_dp*0.0254)^2;%m2
Aann=pi/4*((ID_cs*0.0254)^2-(OD_dp*0.0254)^2);
beta=15000;%bar
vol_dp=Adp*L;%m3
vol_ann=Aann*L;%m3
vol=vol_dp+vol_ann;%m3
Mdp=MWmud_sg*1000*L/Adp;%kg/m4
Mann=MWmud_sg*1000*L/Aann;%kg/m4
M=Mdp+Mann;%kg/m4
velc=sqrt(beta*1e5/(MWmud_sg*1000));
delay_s=2*L/velc;
delay_i=floor(delay_s/dt);
tinit=10*60;
qdrill=  [ 1  1  1 1.5 2  1  1 1.5 2  1]'*2000;%lpm
tramp=   [30 20 10 40 30 30 30 20 60 30]';%Ramp time pumps
twait=60*[10  5  5  5  5  8 12 10  9 12]';%Time with zero flow in
twait2=  [60 40 50 60 35 45 55 70 30 60]';%Time to break gel
twait3=Time1Conn*ones(Nconn,1)-tinit-2*tramp-twait2;
tvec=zeros(Nconn,6);
for k=1:Nconn,
    tvec(k,1)=(k-1)*Time1Conn+tinit;
    tvec(k,2)=tvec(k,1)+tramp(k);
    tvec(k,3)=tvec(k,2)+twait(k);
    tvec(k,4)=tvec(k,3)+tramp(k)/4;
    tvec(k,5)=tvec(k,4)+twait2(k);
    tvec(k,6)=k*Time1Conn;
end
%True parameters
Vfb=3;%m3
kpit=qdrill(1)/Vfb;%lpm/m3
kshaker=10/Vfb;%lpm/m3


dq=1;
qi=2000:-dq:100;
Nq=length(qi);
pfa=zeros(Nq,1);
pfd=zeros(Nq,1);
dpb=zeros(Nq,1);
pfr=zeros(Nq,1);
i=1;
pfa(i) = unified_friction_ann(OD_dp,ID_cs,qi(i)/60000,L,PVmud_cP,YPmud_lb100sqft/2.08854342,tauymud_Pa,MWmud_sg*1000);
pfd(i) = unified_friction_pipe(ID_dp,qi(i)/60000,L,PVmud_cP,YPmud_lb100sqft/2.08854342,tauymud_Pa,MWmud_sg*1000);
dpb(1) = 1*pfd(1);
cbit   = dpb(1)/qi(1)^2;
for i=1:Nq,
    pfa(i) = unified_friction_ann(OD_dp,ID_cs,qi(i)/60000,L,PVmud_cP,YPmud_lb100sqft/2.08854342,tauymud_Pa,MWmud_sg*1000);
    pfd(i) = unified_friction_pipe(ID_dp,qi(i)/60000,L,PVmud_cP,YPmud_lb100sqft/2.08854342,tauymud_Pa,MWmud_sg*1000);
    dpb(i) = cbit*qi(i)^2;
    pfr(i) = pfa(i)+pfd(i)+dpb(i);
end

figure(1)
subplot(211)
c1=max(pfr)/max(qi)/4;%bar/lpm
c2=2*(max(pfr))/max(qi)^2;%bar/lpm^2
% pest=c2/2*qi.^2;
% plot(qi,pfa,qi,pfd,qi,dpb,qi,pfr,qi,pest,'LineWidth',2);
% legend('Annulus','Drill pipe','Bit','Total','Est')
% ylabel('Pressure [bar]')
% title('Pressure loss')
% xlabel('Flow in [lpm]')
% grid
% subplot(212)
% plot(qi(1:Nq-1),-diff(pfr)',qi,c2.*qi,'LineWidth',2);
% legend('Total','Est')
% ylabel('Pressure/flow [bar/lpm]')
% xlabel('Flow in [lpm]')
% grid


%Allocate
qp    = zeros(N,1);
p     = zeros(N,1);
pfricd= zeros(N,1);
pfrica= zeros(N,1);
dpbit = zeros(N,1);
pfric = zeros(N,1);
qout  = zeros(N,1);
volFL = zeros(N,1);
volPit= zeros(N,1);
qshaker=zeros(N,1);
qpit  = zeros(N,1);
qest1 = zeros(N,1);
pest1 = zeros(N,1);
qest2 = zeros(N,1);
pest2 = zeros(N,1);
volFLest = zeros(N,1);
volPitest= zeros(N,1);
qshakerest=zeros(N,1);
qpitest  = zeros(N,1);

qout(1) =qdrill(1);
qpit(1) =qdrill(1);
volFL(1)=Vfb;
qpit(1)=kpit*volFL(1);
qshaker(1)=kshaker*volFL(1);
qest1(1)=qdrill(1);
qest2(1)=qdrill(1);
i=1;
pfrica(i) = unified_friction_ann(OD_dp,ID_cs,qout(i)/60000,L,PVmud_cP,YPmud_lb100sqft/2.08854342,tauymud_Pa,MWmud_sg*1000);
pfricd(i) = unified_friction_pipe(ID_dp,qout(i)/60000,L,PVmud_cP,YPmud_lb100sqft/2.08854342,tauymud_Pa,MWmud_sg*1000);
dpbit(1)  = 1*pfricd(1);
cbit      = dpbit(1)/qdrill(1)^2;
pfric(i)  = pfrica(i)+pfricd(i)+dpbit(i);
p(i)      = pfric(i);
pest1(1)  = p(1);
pest2(1)  = p(1);

for i=1:N-1,
    k = 1+floor(t(i)/Time1Conn);
    if t(i)<tvec(k,1),
        qp(i)=qdrill(k);
    elseif t(i)<tvec(k,2),
        qp(i)=qp(i-1)-qdrill(k)/tramp(k)*dt;
    elseif t(i)<tvec(k,3),
        qp(i)=0;
    elseif t(i)<tvec(k,4),
        qp(i)=qp(i-1)+qdrill(k)/tramp(k)*dt;
    elseif t(i)<tvec(k,5),
        qp(i)=qp(i-1);
    elseif t(i)<=tvec(k,6),
        qp(i)=min(qdrill(k),qp(i-1)+qdrill(k)/tramp(k)*dt);
    else
        disp('Err');
    end
    
    p(i+1)    = p(i)+dt*beta/vol*(qp(i)-qout(max(1,i-delay_i)))/60000;
    pfrica(i) = unified_friction_ann(OD_dp,ID_cs,qout(i)/60000,L,PVmud_cP,YPmud_lb100sqft/2.08854342,tauymud_Pa,MWmud_sg*1000);
    pfricd(i) = unified_friction_pipe(ID_dp,qout(i)/60000,L,PVmud_cP,YPmud_lb100sqft/2.08854342,tauymud_Pa,MWmud_sg*1000);
    dpbit(i)  = cbit*qout(i)^2;
    pfric(i)  = pfrica(i)+pfricd(i)+dpbit(i);
    qout(i+1) = qout(i)+dt/M*(p(i)-pfric(i))*1e5*60000;
    qpit(i)   = kpit*volFL(i);
    qshaker(i)= kshaker*volFL(i);
    volFL(i+1)= volFL(i)+dt*(qout(i)-qpit(i)-qshaker(i))/60000;
    volPit(i+1)= volPit(i)+dt*(qpit(i)-qp(i))/60000;
    
    qest1(i+1) = qest1(i)+dt*beta/(2*vol*c1*60000)*(qp(i)-qest1(i));
    pest1(i+1) = c1*qest1(i+1);
    qest2(i+1) = max(0.1,qest2(i)+dt*beta/(vol*c2*60000)*(qp(i)/qest2(i)-1));
    pest2(i+1) = c2/2*qest2(i+1)^2;
end
qp(N)=qp(N-1);
qpit(N)=qpit(N-1);
qshaker(N)=qshaker(N-1);
pfric(N)=pfric(N-1);
pfrica(N)=pfrica(N-1);
pfricd(N)=pfricd(N-1);
dpbit(N)=dpbit(N-1);

figure(2)
subplot(231)
plot(t,qp,t,qout,t,qshaker,t,qpit,t,qest1,t,qest2,'LineWidth',2);
legend('qp','qout','qshaker','qpit','Estimate 1 of qout','Estimate 2 of qout')
ylabel('Flow [lpm]')
xlabel('Time [sec]')
grid
subplot(234)
plot(t,qout-qest1,t,qout-qest2,'LineWidth',2);
legend('Error estimate 1','Error estimate 2')
ylabel('Flow [lpm]')
xlabel('Time [sec]')
grid
subplot(232)
plot(t,p,t,pest1,t,pest2,'LineWidth',2);
legend('Pump pressure','Estimate 1','Estimate 2')
ylabel('Pressure [bar]')
xlabel('Time [sec]')
grid
subplot(235)
plot(t,p-pest1,t,p-pest2,'LineWidth',2);
legend('Error estimate 1','Error estimate 2')
ylabel('Pressure [bar]')
xlabel('Time [sec]')
grid
subplot(233)
plot(t,volFL,t,volPit,'LineWidth',2);
legend('Flow line volume','Pit volume')
ylabel('Volume [m3]')
xlabel('Time [sec]')
grid
figure(3)
subplot(311)
plot(t,qp,t,qout,'LineWidth',2);
legend('qp','qout')
ylabel('Flow [lpm]')
xlabel('Time [sec]')
grid
subplot(312)
plot(t,p,'LineWidth',2);
legend('Pump pressure')
ylabel('Pressure [bar]')
xlabel('Time [sec]')
grid
subplot(313)
plot(t,volPit,'LineWidth',2);
legend('Pit volume')
ylabel('Volume [m3]')
xlabel('Time [sec]')
grid


sampleTimeFile_s=2;
constant_sampleTimeonFile = false;%true;%false;
wtime=datetime(2016,1,1)+(t-0.5)/24/60/60;

fHeader1='time;flowOut;flowIn;volPitDrill;presSP';
fHeader2='INDEX;VALUE;VALUE;VALUE;VALUE';
fHeader3='time;lpm;lpm;m3;bar';

tvecw   = datevec(wtime);
data    = [qout';qp';volPit';p'];
noise   = [randn(size(qout'))*200;randn(size(qp'))*10;randn(size(volPit'))*0.1;randn(size(p'))*5];

datawnoise = data+noise;
Ncol    = 4;
Nrow    = length(t)-1;
L       = floor(sampleTimeFile_s/dt);
Lvol    = floor(30/dt); 
if constant_sampleTimeonFile,
    index = 1:L:Nrow;
else
    index = find(L*rand(1,Nrow) < 1);
end
% open a file for writing
filename = 'simdatajmnoise.csv';
fid      = fopen(filename, 'w');

if fid==-1,
    disp(['Cannot open file ' filename ' for writing']);
else
    disp(['Saving to file ' filename]);
    fprintf(fid, '%s\n',fHeader1);
    fprintf(fid, '%s\n',fHeader2);
    fprintf(fid, '%s\n',fHeader3);

    for i = index,%time
        fprintf(fid, '%4.0f-%02.0f-%02.0fT%02.0f:%02.0f:%02.0fZ;', tvecw(i,1), tvecw(i,2),tvecw(i,3),tvecw(i,4),tvecw(i,5),tvecw(i,6));        
        fprintf(fid, '%.2f;',max(0,datawnoise(1,i)));
        fprintf(fid, '%.2f;',max(0,datawnoise(2,i)));
        if rand > 2/30,
            datawnoise(3,i) = -0.99925;
        end
        fprintf(fid, '%.2f;',datawnoise(3,i));
        fprintf(fid, '%.2f', max(0,datawnoise(4,i)));
        fprintf(fid, '\n');
    end
end
fclose(fid);

qoutw   = datawnoise(1,index);
qpw     = datawnoise(2,index);
volPitw = datawnoise(3,index);
pw      = datawnoise(4,index);
tw      = t(index);
figure(4)
subplot(311)
plot(tw,qpw,tw,qoutw,'LineWidth',2);
legend('qp','qout')
ylabel('Flow [lpm]')
xlabel('Time [sec]')
grid
subplot(312)
plot(tw,pw,'LineWidth',2);
legend('Pump pressure')
ylabel('Pressure [bar]')
xlabel('Time [sec]')
grid
subplot(313)
plot(tw,volPitw,'LineWidth',2);
legend('Pit volume')
ylabel('Volume [m3]')
xlabel('Time [sec]')
grid


