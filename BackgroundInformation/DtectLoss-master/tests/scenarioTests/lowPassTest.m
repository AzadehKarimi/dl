classdef lowPassTest < matlab.unittest.TestCase
    %testing the pdiff algorithm in a scenario
    
    properties
    end
    
    methods (TestMethodSetup)
    end
    
    methods (Test)  
        function obj =  Test1(obj)
            
               Tc1       = 5;  
               Tc2       = 10;  
               dt_s     = 0.1;
               lp1      = LOWPASS_OBJ(dt_s,Tc1);
               lp2      = LOWPASS_OBJ(dt_s,Tc2);
               
               signal   = [zeros(1,50/dt_s) ones(1,50/dt_s) zeros(1,50/dt_s) zeros(1,50/dt_s)] ;
               NUMT     = numel(signal);
               t        = dt_s:dt_s:dt_s*NUMT;
               simlp1   = zeros(1,NUMT);
               simlp2   = zeros(1,NUMT);
              
               reset    = 0;
               for k= 1:1:NUMT
                  lp1   = lp1.filter(signal(k),reset) ;
                  lp2   = lp2.filter(signal(k),reset) ;
                  simlp1(k) = lp1.FilteredSignal;
                  simlp2(k) = lp2.FilteredSignal;
               end
               
            figure 
            hold on
            plot(t,signal,'r');
            plot(t,simlp1,'g');
            plot(t,simlp2,'k');
            legend('signal','Tc=5','Tc=10');
        end
    end
end