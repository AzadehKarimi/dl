
classdef resetAlgorithmTest < matlab.unittest.TestCase
    %testing the pdiff algorithm in a scenario
    
    properties
    end
    
    methods (TestMethodSetup)
    end

    methods (Test)  
        function obj =  softResetOnSimData(obj)
            FIELD_DATA_ROOT_DIR         = '..\..\simData\';
            buildNr                     = 0;
            isDebugMode                 = 0;
            doWrapperTest               = 1;
            
            doSigmaPlotter              = 0;%1;%% set to one for debugging
            SIGMA_ip                    = 0;%start_SIGMA_processes;%%comment in to debug
            
            nameDesiredProcessTags   = {'flowIn', 'depthBit', 'depthHole', ...
                'flowOut', 'flowOutPercent', 'presSP', 'volGainLoss', 'volPitDrill', 'volTripTank', 'volTripTankIn', 'volTripTankOut', 'heightHook', ...
                'rpm', 'torque', 'weightOnBit'};
            scenarioIdx = 1;
            bm  = BACKTESTING_MANAGER_PARALELL_OBJ();
            bm  = bm.initalize(FIELD_DATA_ROOT_DIR,nameDesiredProcessTags,doWrapperTest,SIGMA_ip,isDebugMode,buildNr,doSigmaPlotter);
            
            bm = bm.RunScenarioAnAdditionalPrc(scenarioIdx,10);%10prc enough to get a tuned model for simtest
            %
            [bm, dtectLossOutput] =bm.RunScenarioOneStepAnGetOutput(scenarioIdx);
            assert(dtectLossOutput.alarmState_voting ==0,'after first connection, the algorithm should be operational(alarmState 0)');
            %
            bm   = bm.SendResetCommandToAlgorithm();
            %
            [bm, dtectLossOutput] =bm.RunScenarioOneStepAnGetOutput(scenarioIdx);
            assert(dtectLossOutput.alarmState_voting<0,'after soft reset, the algorithm should revert to "uncalibrated" state');
            % 
            bm = bm.RunScenarioAnAdditionalPrc(scenarioIdx,10);
            %
            [bm, dtectLossOutput] =bm.RunScenarioOneStepAnGetOutput(scenarioIdx);
            assert(dtectLossOutput.alarmState_voting ==0,'after connection after reset, the algorithm should be operational(alarmState 0)');
        end
        
          function obj =  softResetOnFieldData(obj)
            FIELD_DATA_ROOT_DIR         = '..\..\fieldData\';
            buildNr                     = 0;
            isDebugMode                 = 0;
            doWrapperTest               = 1;
            
            doSigmaPlotter              = 0;%1;%% set to one for debugging
            SIGMA_ip                    = 0;%start_SIGMA_processes;%start_SIGMA_processes;%%comment in to debug
            
            nameDesiredProcessTags   = {'flowIn', 'depthBit', 'depthHole', ...
                'flowOut', 'flowOutPercent', 'presSP', 'volGainLoss', 'volPitDrill', 'volTripTank', 'volTripTankIn', 'volTripTankOut', 'heightHook', ...
                'rpm', 'torque', 'weightOnBit'};
            scenarioIdx = 7;
            bm  = BACKTESTING_MANAGER_PARALELL_OBJ();
            bm  = bm.initalize(FIELD_DATA_ROOT_DIR,nameDesiredProcessTags,doWrapperTest,SIGMA_ip,isDebugMode,buildNr,doSigmaPlotter);
            
            bm = bm.RunScenarioAnAdditionalPrc(scenarioIdx,5);%10prc enough to get a tuned model for simtest
            %
            [bm, dtectLossOutput] =bm.RunScenarioOneStepAnGetOutput(scenarioIdx);
            assert(dtectLossOutput.alarmState_voting>=0,'after first connection, the algorithm should be operational(alarmState 0 or above)');
            %
            bm   = bm.SendResetCommandToAlgorithm();
            %
            [bm, dtectLossOutput] =bm.RunScenarioOneStepAnGetOutput(scenarioIdx);
            assert(dtectLossOutput.alarmState_voting<0,'after soft reset, the algorithm should revert to "uncalibrated" state');
            % 
            bm = bm.RunScenarioAnAdditionalPrc(scenarioIdx,5);
            %
            [bm, dtectLossOutput] =bm.RunScenarioOneStepAnGetOutput(scenarioIdx);
            assert(dtectLossOutput.alarmState_voting >=0,'after connection after reset, the algorithm should be operational(alarmState 0  or above)');
        end
        
        
        
        
        
    end
end
