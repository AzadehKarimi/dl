close all; clc; clear;

% load(fullfile('MachineLearningData', 'STENADON-10.mat'));
% load(fullfile('MachineLearningData', 'HeidrunTLP-34.mat'));
load(fullfile('MachineLearningData', 'GullfaksC-24.mat'));
% load(fullfile('MachineLearningData', 'SongaEnabler-21.mat'));

dd = DOWNLINK_DETECT();

t = simTime/86400 + datenum(2017, 03, 27, 09, 49, 00);
n = numel(t);

actIsDownlinking = false(n,1);

for ii = 1:n
    dd = dd.step(X(ii, 2), t(ii), X(ii, 1));
    actIsDownlinking(ii) = dd.isDownlinking;
end

%%

figure;
ax(1) = subplot(311);
plot(simTime, X(:, 1));
ax(2) = subplot(312);
plot(simTime, X(:, 2), simTime(actIsDownlinking), X(actIsDownlinking, 2), 'r.');
ax(3) = subplot(313);
plot(simTime, actIsDownlinking);
zoom on;

linkaxes(ax, 'x');
axis tight
zoom on;
