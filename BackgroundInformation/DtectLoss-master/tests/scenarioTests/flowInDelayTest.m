
classdef flowInDelayTest < matlab.unittest.TestCase
    %testing in a scenario
    
    properties
    end
    
    methods (TestMethodSetup)
    end

    methods (Test)  
        function obj =  Scenario1(obj)
            clc
            timedelayFlowIn_true_s      = 12.3; %True delay on flowin meas
            timedelayFlowOut_true_s     = 16.7;%True delay on flowout meas
            timeconstFlowOut_true_s     = 8; %Time constant for well - flow out meas
            timeconstPres_true_s        = 3;

            deltaT_random_s         = 2;
            deltaT_steady_s         = 1;
            presSP_p0               = 12;%True model pressure
            presSP_C                = 1;% 1=>100 bar at 2000 lpm
            
            DrillingDuration_s      = 2000;
            NoFlowDuration_s        = 200;
            LowFlowStabilization_s  = 10;
            FLOW_IN_NOISE_PRC       = 5;
            FLOW_OUT_NOISE_PRC      = 10;
            PRES_NOISE_PRC          = 10;

            %***********************
            rng(41)
            close all

            params                = getparams(0);
            objFlowOutExp         = EXPECTED_FLOWOUT_FROM_FLOWIN_OBJ();
            objFlowOutExp         = objFlowOutExp.initalize(params.flowout);
            objPresSPexp          = EXPECTED_STANDPIPE_PRES_OBJ();
            objPresSPexp          = objPresSPexp.initalize(params.pressp);
            objPresDelay          = STANDPIPE_PRESSURE_DELAY_OBJ();
            lpFlowOut             = LOWPASS_OBJ(1,timeconstFlowOut_true_s);
            lpPres                = LOWPASS_OBJ(1,timeconstPres_true_s);

            timesim_d(1)             = 0;
            flowDrilling_lpm         = 2000;
            presSP_drilling_bar      = presSP_C*100*(flowDrilling_lpm/2000)^2 + presSP_p0;
            flowIn_true_lpm(1)       = flowDrilling_lpm;
            flowIn_meas_lpm(1)       = flowDrilling_lpm;
            flowOut_meas_lpm(1)      = flowDrilling_lpm;
            presSP_meas_bar(1)       = presSP_drilling_bar;
            presSP_true_bar(1)       = presSP_drilling_bar;
            
            objFlowOutExp.step(flowIn_true_lpm(1),flowOut_meas_lpm(1),0,0,timesim_d(1));
            %
            timeRamp_s = 20;
            Duration_s_Value_Table = ...
            [   DrillingDuration_s       flowDrilling_lpm;...
                NoFlowDuration_s         100;...
                LowFlowStabilization_s   500;...
                DrillingDuration_s       flowDrilling_lpm;...
                NoFlowDuration_s         50;...
                LowFlowStabilization_s   500;...
                DrillingDuration_s       1600; ...
                DrillingDuration_s       1800; ...
                NoFlowDuration_s         100;...
                LowFlowStabilization_s   500;...
                DrillingDuration_s/2     2200; ...
                DrillingDuration_s/2     180; ...
                NoFlowDuration_s         10;...
                LowFlowStabilization_s   470;...
                DrillingDuration_s/4     2200; ...
                DrillingDuration_s/4     2100; ... 
                DrillingDuration_s/4     2000; ...    
                DrillingDuration_s/4     1900; ...    
                DrillingDuration_s/4     1800; ...  
                DrillingDuration_s/4     1700 ...  
                ];

            timeOfLastChange_s  = timesim_d(1);
            k                   = 1;
            endReached          = 0;
            curRow              = 1;
            while (curRow<=size(Duration_s_Value_Table,1))
               k                   = k+1;
               dt_s                = (rand*deltaT_random_s+deltaT_steady_s);
               timesim_d(k)        = timesim_d(k-1)+dt_s/86400;
               time_now_s          = timesim_d(k)*86400;
               if curRow == 1
                   flowIn_true_lpm(k)  = Duration_s_Value_Table(curRow,2);                   
               elseif Duration_s_Value_Table(curRow,2) > Duration_s_Value_Table(curRow-1,2) %ramp up flow
                   flowIn_true_lpm(k)  = min(flowIn_true_lpm(k-1) + dt_s/timeRamp_s*flowDrilling_lpm, Duration_s_Value_Table(curRow,2));
               else %Ramp down                  
                   flowIn_true_lpm(k)  = max(flowIn_true_lpm(k-1) - dt_s/timeRamp_s*flowDrilling_lpm, Duration_s_Value_Table(curRow,2));
               end
               presSP_true_bar(k)  = presSP_C*100*(flowIn_true_lpm(k)/2000)^2 + presSP_p0;

               if  time_now_s - timeOfLastChange_s > Duration_s_Value_Table(curRow,1)
                   curRow             = curRow+1;
                   timeOfLastChange_s = time_now_s;
               end
               %1. Generate delayed flow in meas
               qIn_lpm = interp1(timesim_d,flowIn_true_lpm,timesim_d(k) - timedelayFlowIn_true_s/86400,'linear');
               if isnan(qIn_lpm)
                   qIn_lpm  = flowIn_true_lpm(k);
               end
               flowIn_meas_lpm(k)  = qIn_lpm*(1+(rand-0.5)*FLOW_IN_NOISE_PRC/100); 
               %2. Generate delayed and filtered flow out meas
               qOut_lpm = interp1(timesim_d,flowIn_true_lpm,timesim_d(k) - timedelayFlowOut_true_s/86400,'linear');
               if isnan(qOut_lpm)
                   qOut_lpm  = flowIn_true_lpm(k);
               end
               lpFlowOut = lpFlowOut.filterForCustomDt(qOut_lpm,(timesim_d(k)-timesim_d(k-1))*86400,0);
               flowOut_meas_lpm(k)  = lpFlowOut.FilteredSignal*(1+(rand-0.5)*FLOW_OUT_NOISE_PRC/100);
               %3. Generate filtered pressure measurement
               pout_bar           = presSP_C*100* (flowIn_true_lpm(k)/2000)^2+ presSP_p0;
               lpPres             = lpPres.filterForCustomDt(pout_bar,(timesim_d(k)-timesim_d(k-1))*86400,0);
               presSP_meas_bar(k) = lpPres.FilteredSignal*(1+(rand-0.5)*PRES_NOISE_PRC/100);
            end
            
            %Init
            time_estVec_d(1)              = timesim_d(1);
            timedelayPresSPJM_estVec_s(1)   = objPresSPexp.timeDelayPresSP_s;
            timedelayPresSPSE_estVec_s(1)   = objPresSPexp.timeDelayPresSP_s;
            timedelayFlowOut_estVec_s(1)  = objFlowOutExp.timeDelay_s;
            timeconstFlowOut_estVec_s(1)  = objFlowOutExp.timeConst_s;
            timedelay_estMaxVec(1)        = nan;
            timedelay_estMinVec(1)        = nan;
            FlowOutExp_lpm(1)             = nan;
            isCalibratingFlowInFlowOut(1) = nan;
            isCalibratingSPDelayJM(1)     = nan;
            isCalibratingSPDelaySE(1)     = nan;
            isPresCalibrated(1)           = nan;
            presSPexp_bar(1)              = nan;
            presSPDelayedJM_bar(1)          = nan;
            presSPDelayedSE_bar(1)          = nan;
            presSPLow_C (1)               = nan;
            presSPLow_p0(1)               = nan;
            presSPLow_b(1)                = nan;
            presSPHigh_C (1)              = nan;
            presSPHigh_p0(1)              = nan;
            presSPHigh_b(1)               = nan;
            presSP_p0inj(1)               = nan;

            for k = 2:1:numel(flowIn_true_lpm)
              % disp(sprintf('timestep:%i',k));
               [objFlowOutExp flowInOutOutput] = objFlowOutExp.step(flowIn_meas_lpm(k),flowOut_meas_lpm(k),0,0,timesim_d(k));
               objPresDelay                    = objPresDelay.updateTimeDelay(presSP_meas_bar(k), flowIn_meas_lpm(k), timesim_d(k) );
               [objPresDelay, presDelay_bar]   = objPresDelay.getDelayedPresSP(timesim_d(k));

               [objPresSPexp presSPExpOutput]  = objPresSPexp.step(flowIn_meas_lpm(k),presSP_meas_bar(k),0,0,timesim_d(k));

               time_estVec_d              = [time_estVec_d timesim_d(k)];
               timedelayFlowOut_estVec_s  = [timedelayFlowOut_estVec_s objFlowOutExp.timeDelay_s];
               timeconstFlowOut_estVec_s  = [timeconstFlowOut_estVec_s objFlowOutExp.timeConst_s];
               timedelay_estMaxVec        = [timedelay_estMaxVec objFlowOutExp.timeDelayMax_s];
               timedelay_estMinVec        = [timedelay_estMinVec objFlowOutExp.timeDelayMin_s];
               FlowOutExp_lpm             = [FlowOutExp_lpm flowInOutOutput.flowOutExpected_lpm];
               isCalibratingFlowInFlowOut = [isCalibratingFlowInFlowOut flowInOutOutput.isCalibratingFlowInFlowOut];
               presSPexp_bar              = [presSPexp_bar, presSPExpOutput.presSPExpected_bar ];
               presSPDelayedJM_bar          = [presSPDelayedJM_bar, presSPExpOutput.presSPDelayed_bar];
               timedelayPresSPJM_estVec_s   = [timedelayPresSPJM_estVec_s objPresSPexp.timeDelayPresSP_s];
               presSPDelayedSE_bar          = [presSPDelayedSE_bar, presDelay_bar];
               timedelayPresSPSE_estVec_s   = [timedelayPresSPSE_estVec_s, objPresDelay.timeDelayPresSP_s];
               
               %presSPLow_C  = [presSPLow_C ,presSPExpOutput.cLowPresSPExp];
               presSPLow_b      = [presSPLow_b ,presSPExpOutput.bLowPresSPExp];
               presSPLow_p0     = [presSPLow_p0,presSPExpOutput.p0LowPresSPExp];
               %presSPHigh_C     = [presSPHigh_C ,presSPExpOutput.cHighPresSPExp];
               presSPHigh_b     = [presSPHigh_b ,presSPExpOutput.bHighPresSPExp];
               presSPHigh_p0    = [presSPHigh_p0,presSPExpOutput.p0HighPresSPExp];

               presSP_p0inj     = [presSP_p0inj,presSPExpOutput.p0injPresSPExp];

               isPresCalibrated          = [isPresCalibrated presSPExpOutput.isCalibratedExpPresSP];
               isCalibratingSPDelayJM    = [isCalibratingSPDelayJM, objPresSPexp.isCalibratingSPDelay];   
               isCalibratingSPDelaySE    = [isCalibratingSPDelaySE, objPresDelay.isCalibratingSPdelay];   
            end
            
            ax =[];
            timeax = 1;%0=day, 1=sec
            if timeax==0
                timesim_plot = timesim_d;
                timeest_plot = time_estVec_d;
            else
                timesim_plot = timesim_d*86400;
                timeest_plot = time_estVec_d*86400;
            end
            figure(1) 
            set(gcf, 'Position', get(0,'Screensize')); % Maximize figure.
            hold on 
            subplot(6,1,1)
            ax = [ax gca];
            hold on
            plot(timesim_plot,flowIn_true_lpm,'LineWidth',2);
            plot(timesim_plot,flowIn_meas_lpm,'LineWidth',2);
            plot(timesim_plot,flowOut_meas_lpm,'LineWidth',2);
            plot(timeest_plot,FlowOutExp_lpm,'--','LineWidth',2)
            legend('FlowIn_{true}','FlowIn_{meas}','FlowOut_{meas}','FlowOutExp');
            grid
            if timeax==0 
                datetick('x','HH:MM:SS') 
            end
            axis tight
            subplot(6,1,2)
            ax = [ax gca];
            hold on
            plot(timesim_plot,presSP_true_bar,'LineWidth',2);
            plot(timesim_plot,presSP_meas_bar,'LineWidth',2);
            plot(timeest_plot,presSPexp_bar,'--','LineWidth',2);
            plot(timeest_plot,presSPDelayedJM_bar,'LineWidth',2);
            plot(timeest_plot,presSPDelayedSE_bar,'LineWidth',2);
            grid
            legend('presSP_{true}','presSP_{meas}','presSPexp','presSPDelayedJM','presSPDelayedSE');
            if timeax==0 
                datetick('x','HH:MM:SS') 
            end
            axis tight
            subplot(6,1,3)
            ax = [ax gca];
            hold on
            plot(timeest_plot,timedelayPresSPJM_estVec_s,'--','LineWidth',2);
            plot(timeest_plot,timedelayPresSPSE_estVec_s,'--','LineWidth',2);
            plot([timeest_plot(1) timeest_plot(end)],timedelayFlowIn_true_s*[1, 1],'LineWidth',2)
            grid
            if timeax==0 
                datetick('x','HH:MM:SS') 
            end
            axis tight
            legend('timedelayPresSPJM_{est}','timedelayPresSPSE_{est}','timedelayPresSP_{true}');
            subplot(6,1,4)
            ax = [ax gca];
            hold on
            plot(timeest_plot,timedelayFlowOut_estVec_s,'--','LineWidth',2);
            plot([timeest_plot(1) timeest_plot(end)],(timedelayFlowOut_true_s-timedelayFlowIn_true_s)*[1, 1],'LineWidth',2)
            grid
            legend('timedelayFlowOut_{est}','timedelayFlowOut_{true}');
            if timeax==0 
                datetick('x','HH:MM:SS') 
            end
            axis tight
            subplot(6,1,5)
            ax = [ax gca];
            hold on
            plot(timeest_plot,timeconstFlowOut_estVec_s,'--','LineWidth',2);
            plot([timeest_plot(1) timeest_plot(end)],timeconstFlowOut_true_s*[1, 1],'LineWidth',2)
            grid
            legend('timeconstFlowOut_{est}','timeconstFlowOut_{true}');
            if timeax==0 
                datetick('x','HH:MM:SS') 
            end
            axis tight
            subplot(6,1,6)
            ax = [ax gca];
            hold on
            plot(timeest_plot,isCalibratingFlowInFlowOut,'LineWidth',2);
            plot(timeest_plot,isCalibratingSPDelayJM,'LineWidth',2);
            plot(timeest_plot,isCalibratingSPDelaySE,'LineWidth',2);
            legend('isCalibratingFlowInFlowOut','isCalibratingSPDelayJM','isCalibratingSPDelaySE')
            if timeax==0 
                datetick('x','HH:MM:SS') 
            end
            axis tight
            grid
            %
            
            figure(2)
            subplot(2,1,1)
            ax = [ax gca];
            hold on
            plot(timesim_plot,flowOut_meas_lpm-FlowOutExp_lpm,'LineWidth',2)
            legend('Flow out deviation');
            grid
            if timeax==0 
                datetick('x','HH:MM:SS') 
            end
            axis tight
            subplot(2,1,2)
            ax = [ax gca];
            hold on
            plot(timeest_plot,presSPexp_bar-presSPDelayedJM_bar,'LineWidth',2);
            plot(timeest_plot,presSPexp_bar-presSPDelayedSE_bar,'LineWidth',2);
            grid
            legend('presSP deviation JM', 'presSP deviation SE');
            if timeax==0 
                datetick('x','HH:MM:SS') 
            end
            axis tight
            
            linkaxes(ax,'x')
            
            %
            %       Assert part
            %
            index_assert = find(time_estVec_d > max(time_estVec_d)/3);
            pressumJM = 100*nansum(abs(presSPDelayedJM_bar(index_assert)-presSPexp_bar(index_assert)))/(presSP_drilling_bar*numel(index_assert));
            pressumSE = 100*nansum(abs(presSPDelayedSE_bar(index_assert)-presSPexp_bar(index_assert)))/(presSP_drilling_bar*numel(index_assert));
            flowsum = 100*nansum(abs(flowOut_meas_lpm(index_assert)-FlowOutExp_lpm(index_assert)))/(flowDrilling_lpm*numel(index_assert));
            fprintf('flowInDelayTest.Scenario1: pressurediffsum JM = %.2f pc\n',pressumJM);
            fprintf('flowInDelayTest.Scenario1: pressurediffsum SE = %.2f pc\n',pressumSE);
            fprintf('flowInDelayTest.Scenario1: flowdiffsum = %.2f pc\n',flowsum);
            
            assert(pressumJM<2.5,'offset btw JM pressure and expected pressure has increased!');%value was  9.6910e+03 at run on may 3. 2017
            assert(pressumSE<2.5,'offset btw SE pressure and expected pressure has increased!');%value was  9.6910e+03 at run on may 3. 2017
            assert(flowsum<2.5,'offset btw flowout and expected flowout has increased!'); % value was  9.8688e+04 at run on may 3. 2017
        end
    end
end
