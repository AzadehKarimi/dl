classdef pdiffTest < matlab.unittest.TestCase
    %testing the pdiff algorithm in a scenario
    
    properties
        test=1;
        
    end
    
    methods (TestMethodSetup)
    end
    
    methods (Test)  
        function obj        =  Test1(obj)
            close all
            
            DT_S            = 1;
            solver          = SimulatorSolvers.FORWARD_EULER;
            pdiff           = PDIFF_ALG_OBJ();
            pdiff           = pdiff.initialize(DT_S,1);
            pdiff_noP       = PDIFF_ALG_OBJ();
            pdiff_noP       = pdiff_noP.initialize(DT_S,0);
            
            sim             = SIMULATOR_OBJ();
            sim             = sim.initialize(DT_S,solver);
            simulationDuration_s = 900;
            sim             = sim.StartSavingIterations(simulationDuration_s);
            
            bitDepth_m      = 3000;
            holeDepth_m     = 3000;
            
            qin_lp_Tc   = 1;
            qin_lp      = LOWPASS_OBJ(DT_S,qin_lp_Tc);
            
            while ( sim.t < simulationDuration_s )
                qin_lpm         = 2000;
                qloss_real_lpm  = 0;
                % first shutdown and startup
                if (sim.t>20)
                     qin_lpm    = 2000*(1-(sim.t-20)/20);
                end
                if (sim.t>40)
                     qin_lpm = 0;
                end    
                
                if (sim.t>300)
                     qin_lpm    = 2000*min(1,(sim.t-300)/20);
                end
                if (sim.t>305)
                     qin_lpm = 500;
                end    
                if (sim.t>325)
                     qin_lpm    = 2000*min(1,(sim.t-320)/20);
                end   
                % second shutdown and startup
                if (sim.t>450)
                     qin_lpm    = 2000*(1-(sim.t-450)/20);
                end
                
                if (sim.t>470)
                     qin_lpm = 0;
                end    
                
                if (sim.t>600)
                     qin_lpm    = 2000*min(1,(sim.t-600)/20);
                end
                if (sim.t>605)
                     qin_lpm = 500;
                end    
                if (sim.t>625)
                     qin_lpm    = 2000*min(1,(sim.t-620)/20);
                end    
                 % loss on and off
                
                if (sim.t>750)
                     qloss_real_lpm = 200;
                end
                if (sim.t>770)
                     qloss_real_lpm = 0;
                end
                
                qin_lp      = qin_lp.filter(qin_lpm,0);
                qin_lpm     = qin_lp.FilteredSignal;
                
                sim         = sim.stepIt        (qin_lpm, qloss_real_lpm,   bitDepth_m,    holeDepth_m);
                pdiff       = pdiff.stepIt      (qin_lpm, sim.q_out_lpm ,   sim.p_p_bar,   sim.sys_states);
                pdiff_noP   = pdiff_noP.stepIt  (qin_lpm, sim.q_out_lpm ,   sim.p_p_bar,   sim.sys_states);
                
                sim.buffer.betaDivByV_e_bar_per_m3(sim.curBufferIdx) = pdiff.betaDivByV_e_bar_per_m3;
                sim.buffer.qIo_pdiff_lpm(sim.curBufferIdx)           = pdiff.qIo_lpm;
                sim.buffer.qIo_nodiff_lpm(sim.curBufferIdx)          = pdiff_noP.qIo_lpm;
                sim.buffer.dpsp_dt_bar(sim.curBufferIdx )            = pdiff.dpsp_dt_bar;
            end
            disp(sprintf('volume of well:%f',sim.V_m3)); 
            m3Tolpm = 60000;
            new_subplot({{1},{2};{3,4,5,7,8,6,9},{};{10,11},{};{12,13,14},{}},...
                {sim.buffer.t,sim.buffer.p_p_bar,       'p_p [bar](sim)'},...
                {sim.buffer.t,sim.buffer.dpsp_dt_bar,   'dpsp/dt [bar/s](sim)'},...
                {sim.buffer.t,sim.buffer.qin_lpm,       'q_{in} [lpm](sim)'},...
                {sim.buffer.t,sim.buffer.qout_lpm,      'q_{out} [lpm](sim)'},...
                {sim.buffer.t,sim.buffer.qloss_lpm,     'q_{loss} [lpm](sim)'},...
                {sim.buffer.t,sim.buffer.q_fl_lpm,      'q_{fl} [lpm](sim)'},...
                {sim.buffer.t,sim.buffer.qIo_pdiff_lpm, 'q_{loss}^{pdiff} [lpm](est)'},...
                {sim.buffer.t,sim.buffer.qIo_nodiff_lpm,'q_{loss}^{nodiff} [lpm](est)'},...
                {sim.buffer.t,-sim.buffer.dpsp_dt_bar/120*m3Tolpm + sim.buffer.qin_lpm ,'q_{out}^{exp} [lpm](est)'},... %  
                {sim.buffer.t,sim.buffer.betaDivByV_bar_per_m3, 'beta/V [bar/m3] (sim)'},...
                {sim.buffer.t,sim.buffer.betaDivByV_e_bar_per_m3, 'beta/V [bar/m3] (est)'},...
                {sim.buffer.t,sim.buffer.V_fl_m3,       'V_{fl}[m3](sim)'},...
                {sim.buffer.t,sim.buffer.V_pit_m3,      'V_{pit} [m3](sim)'},...
                {sim.buffer.t,15- sim.buffer.V_pit_m3-sim.buffer.V_fl_m3,  '\Delta V_{well} [m3](sim)'}...
            );
            new_subplot({{1},{}},...
             {sim.buffer.t,sim.buffer.betaDivByV_e_bar_per_m3, 'beta/V [bar/m3] (est)'}...
               );
        
        
        end
    end
    
end