classdef evaluateTest < matlab.unittest.TestCase
    %testing the analysis of a time series
       
properties
end

methods (TestMethodSetup)
end
    
methods (Test)  
    function obj =  Test1(obj, detectobj)
        %read data from file
        %data = readcsvfile('simfile.csv');
        %[numDataPoints,numVar]   = size(data);

        obj = EVALUATE_OBJ();            
        obj = obj.initialize(data);

        qin_lp_Tc   = 1;
        qin_lp      = LOWPASS_OBJ(obj.avg_dt_s,qin_lp_Tc);

        i=1;
        while ( i < numDataPoints ),
             i = i+1;
        end
        
        %report alarms, etc
        
        %plot results
        obj = obj.PlotResAnalysis();
        %write res to csv file
        obj = obj.writerescsvfile('evaluation.csv');
    end
    
end

end