classdef simdrillTest < matlab.unittest.TestCase
    %testing the simdrill algorithm in a scenario
       
    properties
    end
    
    methods (TestMethodSetup)
    end
    
    methods (Test)               
        function obj =  Test1(obj)%Run batchwise
            DT_S            = 0.5;%0.1Time step in simulation
            sampleTimeFile_s= 2.0;%Time step saved on csv file
            
            simcase         = SIMULATORCASEDEF_OBJ();
            simcase         = simcase.initialize();           
            solver          = SimulatorSolvers.FORWARD_EULER;
            sim             = DRILLSIMULATOR_OBJ();
            sim             = sim.initialize(DT_S,solver,simcase);
            sim             = sim.StartSavingIterations(simcase.simulationDuration_s);           
                                
            while ( sim.t_s < simcase.simulationDuration_s )
                sim         = sim.stepIt();
            end
            
            sim = sim.plotsimres();
%             filenamesim  = 'C:\appl\Source\DtectLoss\simData\SimRig-1.csv';
%             filenametrue = 'C:\appl\Source\DtectLoss\simData\True_SimRig-1.csv';
%             filenamezip  = 'C:\appl\Source\DtectLoss\simData\SimRig-1.zip';
            filenamesim  = 'C:\Development\DtectLoss\BackgroundInformation\DtectLoss-master\simData\SimRig-2.csv';
            filenametrue = 'C:\Development\DtectLoss\BackgroundInformation\DtectLoss-master\simData\True_SimRig-2.csv';
            filenamezip  = 'C:\Development\DtectLoss\BackgroundInformation\DtectLoss-master\simData\SimRig-2.zip';
            sim = sim.writecsvfile(filenamesim,DT_S,sampleTimeFile_s);    
            sim = sim.writetruedatafile(filenametrue,DT_S,sampleTimeFile_s);
            zip(filenamezip, {filenamesim, filenametrue});
            delete(filenamesim);
            delete(filenametrue);
        end
        
        function obj =  Test2(obj)%Run each sample through all steps: simulate - process - detect - evaluate
            DT_S            = 0.5;%Time step in simulation
            sampleTimeFile_s= 2.0;%Time step process & detect
            
            simcase         = SIMULATORCASEDEF_OBJ();
            simcase         = simcase.initialize();           
            solver          = SimulatorSolvers.FORWARD_EULER;
            sim             = DRILLSIMULATOR_OBJ();
            sim             = sim.initialize(DT_S,solver,simcase);
            sim             = sim.StartSavingIterations(simcase.simulationDuration_s);           

            process         = PROCESS_OBJ();            
            %process         = process.initialize('simfile.csv');%read data from file

            %read data from file
%             detect      = DETECT_OBJ(100, 0.5, 1);%Flow dead band 100lpm, volume threshold warning 0.5m3, alarm 1m3            
%             detect      = detect.initialize(process);

            %run EKD alg
%             solver      = SimulatorSolvers.FORWARD_EULER;
%             pdiff       = PDIFF_ALG_OBJ();
%             pdiff       = pdiff.initialize(obj.detect.avg_dt_s,1);
%             pdiff_noP   = PDIFF_ALG_OBJ();
%             pdiff_noP   = pdiff_noP.initialize(obj.detect.avg_dt_s,0);
 
            
            while ( sim.t_s < simcase.simulationDuration_s )
                sim         = sim.stepIt();
                %MAKE functions for one sample
                %process    = process.checkdata();   
                %detect     =  detect.runpdiff(processobj, pdiff, pdiff_noP);

            end
            
            sim = sim.plotsimres();                        
            sim = sim.writecsvfile('simfile.csv',DT_S,sampleTimeFile_s);    
            sim = sim.writetruedatafile('simtruedata.csv',DT_S,sampleTimeFile_s);                
            
            process  = process.DispSeriesAnalysis();
            process  = process.plotSeriesAnalysis();        
            process  = process.writerescsvfile('processres.csv');%write res to csv file

        end
        
    end    
end