classdef analyseTest < matlab.unittest.TestCase
    %TO BE DELETED - replaced by EVALUATE
    %testing the analysis of a time series
    
       
properties
end

methods (TestMethodSetup)
end
    
methods (Test)  
    function obj =  Test1(obj)
        %read data from file
        data = readcsvfile('simfile.csv');
        [numDataPoints,numVar]   = size(data);

        obj = ANALYZE_OBJ();            
        obj = obj.initialize(data);

        %check time columns for nan
        badrows = isnan (obj.buffer.t_s);

        obj.lengthTimeSeries = max(obj.buffer.t_s) - min(obj.buffer.t_s);           
        obj.max_dt_s         = max(diff(obj.buffer.t_s));
        obj.avg_dt_s         = mean(diff(obj.buffer.t_s));
        obj.NaN_pc           = sum(badrows)/numDataPoints*100;
        obj.bitDepth_min_m   = min(obj.buffer.bitDepth_m);
        obj.bitDepth_max_m   = max(obj.buffer.bitDepth_m);   
        obj.q_out_max_lpm    = max(obj.buffer.q_out_lpm);   
        obj.q_out_max_pc     = max(obj.buffer.q_out_pc);
        obj.q_p_max_lpm      = max(obj.buffer.q_p_lpm); 
        obj.V_pit_min_m3     = min(obj.buffer.V_pit_m3); 
        obj.V_pit_max_m3     = max(obj.buffer.V_pit_m3);
        obj.gldrill_min_m3   = min(obj.buffer.gldrill_m3);
        obj.gldrill_max_m3   = max(obj.buffer.gldrill_m3);
        obj.V_tt_min_m3      = min(obj.buffer.V_tt_m3);
        obj.V_tt_max_m3      = max(obj.buffer.V_tt_m3);
        obj.p_p_max_bar      = max(obj.buffer.p_p_bar);
        obj.holeDepth_min_m  = min(obj.buffer.holeDepth_m);
        obj.holeDepth_max_m  = max(obj.buffer.holeDepth_m);
        obj.bitVel_max_mph   = max(obj.buffer.bitVel_mph);
        obj.rotation_max_rpm = max(obj.buffer.rotation_rpm);

        %check req var: flow in, flow out, pit vol
        obj.seriesOK         = (obj.lengthTimeSeries > 10) & (obj.NaN_pc < 50) & (obj.q_p_max_lpm > 0) & ...
                               ( (obj.q_out_max_lpm > 0) | (obj.q_out_max_pc > 0) ) & ...
                               ( (obj.V_pit_max_m3 - obj.V_pit_min_m3 > 1) | (obj.gldrill_max_m3 - obj.gldrill_min_m3 > 1) ); 
        obj = obj.DispSeriesAnalysis();
        obj = obj.plotSeriesAnalysis();

        %sys states: stats
        obj.sys_states       = SYSTEM_STATES_OBJ();

        %run EKD alg
        solver      = SimulatorSolvers.FORWARD_EULER;
        pdiff       = PDIFF_ALG_OBJ();
        pdiff       = pdiff.initialize(obj.avg_dt_s,1);
        pdiff_noP   = PDIFF_ALG_OBJ();
        pdiff_noP   = pdiff_noP.initialize(obj.avg_dt_s,0);

        qin_lp_Tc   = 1;
        qin_lp      = LOWPASS_OBJ(obj.avg_dt_s,qin_lp_Tc);

        i=1;
        while ( i < numDataPoints ),%run EKD algorithm (iMPD inspired - delta flow only)
             obj.sys_states = obj.sys_states.stateMachine(obj.buffer.q_p_lpm(i), obj.buffer.bitDepth_m (i), obj.buffer.holeDepth_m (i),...
                                                          obj.buffer.rotation_rpm (i),obj.buffer.bitVel_mph (i));

             pdiff       = pdiff.stepIt     (obj.buffer.q_p_lpm(i), obj.buffer.q_out_lpm(i) , obj.buffer.p_p_bar (i),   obj.buffer.sys_states (i));
             pdiff_noP   = pdiff_noP.stepIt (obj.buffer.q_p_lpm(i), obj.buffer.q_out_lpm(i) , obj.buffer.p_p_bar (i),   obj.buffer.sys_states (i));
    % %                 
             obj.buffer.dbdV_e_barm3(i)   = pdiff.betaDivByV_e_bar_per_m3;
             obj.buffer.qIo_pdiff_lpm(i)  = pdiff.qIo_lpm;
             obj.buffer.qIo_nodiff_lpm(i) = pdiff_noP.qIo_lpm;
             obj.buffer.dpsp_dt_bar(i)    = pdiff.dpsp_dt_bar;
             obj.buffer.sys_states (i)    = obj.sys_states.currentState;
             i = i+1;
        end
        
        %report alarms, etc
        
        %plot results
        obj = obj.PlotResAnalysis();
        %write res to csv file
        obj = obj.writerescsvfile('analysis.csv');
    end
    
end

end