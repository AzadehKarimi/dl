classdef processTest < matlab.unittest.TestCase
    %testing the processing of a time series
       
properties
    process    ;
end

methods (TestMethodSetup)
end
    
methods (Test)  
    function obj =  Test1(obj)
        obj.process  = PROCESS_OBJ();            
        obj.process  = obj.process.initialize('C:\appl\Source\StopLoss\tests\testdata\SimRig_SimWell_FromTime2016_01_01_00_00_00.csv');%read data from file
        obj.process  = obj.process.checkdatabatch();
        obj.process  = obj.process.DispSeriesAnalysis();
        obj.process  = obj.process.plotSeriesAnalysis();        
        obj.process  = obj.process.writerescsvfile('C:\appl\Source\StopLoss\tests\testdata\Washed_SimRig_SimWell_FromTime2016_01_01_00_00_00.csv');%write res to csv file
    end    
end

end