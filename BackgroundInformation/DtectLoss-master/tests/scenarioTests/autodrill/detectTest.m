classdef detectTest < matlab.unittest.TestCase
    %testing the influx/loss detction of a time series
       
properties
    detect;
end

methods (TestMethodSetup)
end
    
methods (Test)  
    function obj =  Test1(obj, processobj)
        %read data from file
        obj.detect = DETECT_OBJ(100, 0.5, 1);%Flow dead band 100lpm, volume threshold warning 0.5m3, alarm 1m3            
        obj.detect = obj.detect.initialize(processobj);

        %run EKD alg
        solver      = SimulatorSolvers.FORWARD_EULER;
        pdiff       = PDIFF_ALG_OBJ();
        pdiff       = pdiff.initialize(obj.detect.avg_dt_s,1);
        pdiff_noP   = PDIFF_ALG_OBJ();
        pdiff_noP   = pdiff_noP.initialize(obj.detect.avg_dt_s,0);

        obj.detect =  obj.detect.runpdiff(processobj, pdiff, pdiff_noP);
        %report alarms, etc
        
        %plot results
        obj.detect = obj.detect.plotDetectionResults();
        %write res to csv file
        obj.detect = obj.detect.writerescsvfile('detection.csv');
    end
    
end

end