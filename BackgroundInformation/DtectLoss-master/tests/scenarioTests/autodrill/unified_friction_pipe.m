function dp = unified_friction_pipe(Dd_i,q_SI,L_SI,PV,YP_SI,tau_y,rho_SI)
% UNIFIED_FRICTION_PIPE returns the frictional pressure drop dp [bar] in a
% pipe based on its diameter Dd_i [in], flow rate q [m3/s], length L [m], and
% the rheology parameters plastic viscosity PV [cP], yield point YP [Pa],
% yield stress tau_y [lp/(100 ft^2)], and density rho [kg/m^3]. The
% calculations are based on Zamora2005 (AADE-05-NTCE-27).

q_SI_abs=abs(q_SI);

%% multipliers
in2m=0.0254; % multiplier from in to m
m2ft=(3.048e-1)^-1; % multiplier from m to ft

%% parameters
% well parameters
L=L_SI*m2ft; % [ft] length

% Area in SI-units
Ad_i_SI=(Dd_i*in2m)^2*pi/4; % [m2] inner area of drillstring

% flow velocity
v_d_SI=q_SI_abs/Ad_i_SI; % [m/s] velocity inside drillstring
V_d_m=v_d_SI*m2ft*60; % [ft/min] velocity inside drillstring

% rheology parameters
rho=rho_SI/119.826427; % [kg/m3]/conv=[lb/gal] density
YP=YP_SI*2.08854342; % [Pa]*conv=[lb/100 ft^2] Bingham yield point
n=3.322*log10((2*PV+YP-tau_y)/(PV+YP-tau_y));
k=(PV+YP-tau_y)/511^n;

G_pipe=(3*n+1)/(4*n);
gamma_w_pipe=1.6*G_pipe*V_d_m/Dd_i;
tau_w_pipe_turb=( (4/3)^n*tau_y+k*gamma_w_pipe.^n );

% friction calculation
NReG_pipe=rho*V_d_m.^2./(19.36*tau_w_pipe_turb);

f_lam_pipe=16./NReG_pipe;

f_trans_pipe=16*NReG_pipe/(3470-1370*n)^2;

n_p=3.32*log10( (2*PV+YP)/(PV+YP) );
a=(log10(n_p)+3.93)/50;
b=(1.75-log10(n_p))/7;
f_turb_pipe=a./(NReG_pipe.^b);

f_int_pipe=(f_trans_pipe.^-8+f_turb_pipe.^-8).^(-1/8);
f_pipe=(f_int_pipe.^12+f_lam_pipe.^12).^(1/12);

% calculation of the frictional pressure drop
dp=sign(q_SI)*1.076*rho*V_d_m.^2.*f_pipe*L/(Dd_i*1e5)/14.5037738;
qmin=100/60000;%20
if q_SI_abs<qmin
    dpmin=unified_friction_pipe(Dd_i,qmin,L_SI,PV,YP_SI,tau_y,rho_SI);
    dp=sign(q_SI)*sqrt(q_SI_abs/qmin)*dpmin;
end
%dp=real(dp);
