function dp = unified_friction_ann(Dd_o,Da,q_SI,L_SI,PV,YP_SI,tau_y,rho_SI)
% UNIFIED_FRICTION_PIPE returns the frictional pressure drop dp [bar] in an
% annulus based on its inner diameter Dd_o [in], outer diameter Da [in],
% flow rate q [m3/s], length L [m], and the rheology parameters plastic
% viscosity PV [cP], yield point YP [Pa], yield stress tau_y [lp/(100
% ft^2)], and density rho [kg/m^3]. The calculations are based on
% Zamora2005 (AADE-05-NTCE-27).% Plotting frictional pressure drop with the
% unified model from Zamora2002

q_SI_abs=abs(q_SI);

%% multipliers
in2m=0.0254; % multiplier from in to m
m2ft=(3.048e-1)^-1; % multiplier from m to ft

%% parameters
% well parameters
L=L_SI*m2ft; % [ft] length

% Area in SI-units
Ad_o_SI=(Dd_o*in2m)^2*pi/4; % [m2] outer area of drillstring
Aa_SI=(Da*in2m)^2*pi/4-Ad_o_SI;  % [m2] area of annulus

% flow velocity
v_a_SI=q_SI_abs/Aa_SI; % [m/s] velocity in annulus
V_a_m=v_a_SI*m2ft*60; % [ft/min] velocity in annulus

% rheology parameters
rho=rho_SI/119.826427; % [kg/m3]/conv=[lb/gal] density
YP=YP_SI*2.08854342; % [Pa]*conv=[lb/100 ft^2] Bingham yield point
n=3.322*log10((2*PV+YP-tau_y)/(PV+YP-tau_y));
k=(PV+YP-tau_y)/511^n;

G_ann=(2*n+1)/(3*n)*3/2;
gamma_w_ann=1.6*G_ann*V_a_m/(Da-Dd_o);
tau_w_ann_turb=( (3/2)^n*tau_y+k*gamma_w_ann.^n );

% friction calculation
NReG_ann=rho*V_a_m.^2./(19.36*tau_w_ann_turb);

f_lam_ann=16./NReG_ann;

f_trans_ann=16*NReG_ann/(3470-1370*n)^2;

n_p=3.32*log10( (2*PV+YP)/(PV+YP) );
a=(log10(n_p)+3.93)/50;
b=(1.75-log10(n_p))/7;
f_turb_ann=a./(NReG_ann.^b);

f_int_ann=(f_trans_ann.^-8+f_turb_ann.^-8).^(-1/8);
f_ann=(f_int_ann.^12+f_lam_ann.^12).^(1/12);

% frictional pressure drop in bar
dp=sign(q_SI)*1.076*rho*V_a_m.^2.*f_ann*L/((Da-Dd_o)*1e5)/14.5037738;

qmin=100/60000;%100
if q_SI_abs<qmin
    dpmin=unified_friction_ann(Dd_o,Da,qmin,L_SI,PV,YP_SI,tau_y,rho_SI);
    dp=sign(q_SI)*sqrt(q_SI_abs/qmin)*dpmin;
end

%dp=real(dp);
