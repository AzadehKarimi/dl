function data = readcsvfile(filename)
fid = fopen(filename, 'r');

tline = fgetl(fid);
%fHeader1='BITDEP;BITDEP;FLOWOUT;FLOWOUT;FLOWIN;FLOWIN;FLOWOUTPC;FLOWOUTPC;PIT_DRILL;PIT_DRILL;GL_DRILL;GL_DRILL;TRIPTANK;TRIPTANK;SPP;SPP;HOLEDEP;HOLEDEP;HOOKHEIGHT;HOOKHEIGHT;ROP;ROP;HOOKLOAD;HOOKLOAD;WOB;WOB;RPM;RPM;ECD;ECD';
%matches = strfind(tline, ';');
%num = length(matches);
%parnames=[];
%for i=1:2:num-1,
%   parnames=[parnames;char(tline(matches(i)+1:matches(i+1)-1))];
%end
tline = fgetl(fid);
tline = fgetl(fid);
%fHeader2='INDEX;VALUE;INDEX;VALUE;INDEX;VALUE;INDEX;VALUE;INDEX;VALUE;INDEX;VALUE;INDEX;VALUE;INDEX;VALUE;INDEX;VALUE;INDEX;VALUE;INDEX;VALUE;INDEX;VALUE;INDEX;VALUE';
tline = fgetl(fid);
tline = fgetl(fid);
%fHeader3='time;unitless;time;m3/s;time;unitless;time;unitless;time;m3;time;m3;time;unitless;time;unitless;time;unitless;time;unitless;time;unitless;time;unitless;time;unitless';
tline = fgetl(fid);
tline = fgetl(fid);
data=[];
while ischar(tline)%scan all lines with data
   matches = strfind(tline, ';');
   num = length(matches);
   strtime=tline(1:matches(1)-1);
   matchT = strfind(strtime, 'T');
   matchZ = strfind(strtime, 'Z');
   dvec=datevec(strtime(1:matchT),'yyyy-mm-dd');
   dvec(4)=str2num(strtime(matchT+1:matchT+2)); 
   dvec(5)=str2num(strtime(matchT+4:matchT+5)); 
   dvec(6)=str2num(strtime(matchT+7:matchT+8)); 
   pars=zeros((num+1)/2,1);
   k=1;
   for i=1:2:(num-1),
      pars(k)=str2double(tline(matches(i)+1:matches(i+1)-1));
      k=k+1;
   end
   pars(k)=str2double(tline(matches(i+2)+1:length(tline)));
   data=[data;datenum(dvec),pars'];
   tline = fgetl(fid);
end

            
            fclose(fid);
