﻿using System.Collections.Generic;

namespace DtectLoss.Common
{
    public class ScenarioConfig
    {
        public int Idx { get;}
        public string UsedFlowMeasTagName { get;}
        public string UsedTripTankMeasTagName { get;}
        public string UsedPitVolMeasTagName { get;}
        public bool UseForBacktesting { get;}
        public string AreaDppoohM2 { get;}
        public string AreaDprihM2 { get;}
        public List<string>? AlgorithmsToUse { get; set; }


        public ScenarioConfig(int idx, string usedFlowMeasTagName, string usedTripTankMeasTagName, string usedPitVolMeasTagName, bool useForBacktesting
        , string areaDppoohM2, string areaDprihM2)
        {
            Idx = idx;
            UsedFlowMeasTagName = usedFlowMeasTagName;
            UsedTripTankMeasTagName = usedTripTankMeasTagName;
            UsedPitVolMeasTagName = usedPitVolMeasTagName;
            UseForBacktesting = useForBacktesting;
            AreaDppoohM2 = areaDppoohM2;
            AreaDprihM2 = areaDprihM2;
        }
    }
}