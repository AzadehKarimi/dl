﻿namespace DtectLoss.Common
{
    public class DataVariable
    {
        public DataVariable(double t, double v)
        {
            T = t;
            V = v;
        }
        public double T { get; }
        public double V { get; }
    }
    

}