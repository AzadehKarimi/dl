﻿using System.Collections.Generic;
using DtectLoss.Common.Enums;

namespace DtectLoss.Common
{
    public struct BacktestEvaluateOutput
    {
        public int TrueNoOfAlams;
        public int TrueNoOfWarnings;
        public int AlgNoOfCorrectAlams;
        public int AlgNoOfWarnings;
        public int AlgNoOfFalseAlams;
        public int AlgLateAlams;
        public string AlgName;
        public string RigName;
        public string SimName;
        public string ResultText;
        public double FalseAlamsRate;
        public double ProbabilityOfDetection;
        public double KeyPerformanceIndex;
        public double TimeToDetection;
        public double AccumulatedVolumeAtDetection;
        public double DurationInHours;
        public double AlgUpTime;
        public double AlgUptimeInHours;
        public double AccumulatedVolIo;
        public List<(AlarmType alarmType, int)> AlarmTypeCount;
    }
}