﻿namespace DtectLoss.Common
{
    public class DataVariableMetadata
    {
        public int TimeColumnIndex { get;}
        public int ValueColumnIndex { get;}
        public string Name { get;}
        public string Unit { get;}

        public DataVariableMetadata(int timeColumnIndex, int valueColumnIndex, string name, string unit)
        {
            TimeColumnIndex = timeColumnIndex;
            ValueColumnIndex = valueColumnIndex;
            Name = name;
            Unit = unit;
        }

    }
}