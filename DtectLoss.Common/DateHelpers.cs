﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DtectLoss.Common
{
    public static class DateHelpers
    {
        public static string DateToString(double V)
        {
            return DateTime.FromOADate(V).ToString("MM/dd/yyyy HH:mm:ss");
        }

        public static double ToMatlabDateTime(this double date)
        {
            return DateTime.FromOADate(date).Ticks * 1e-7 / 86400 + 367;
        }

        public static double DiffInSeconds(this double endDateD, double startDateD)
        {
            return (endDateD.ToMatlabDateTime() - startDateD.ToMatlabDateTime()) * CommonConstants.SecondsIn24Hours;
        }

        public static double AddSeconds(this double time, double secondsToAdd)
        {
            return time + secondsToAdd / CommonConstants.SecondsIn24Hours;
        }
        public static double SubtractSeconds(this double time, double secondsToSubtract)
        {
            return time - secondsToSubtract / CommonConstants.SecondsIn24Hours;
        }

        public static double SecondsToOADate(this double seconds)
        {
            return seconds / CommonConstants.SecondsIn24Hours;
        }

        public static double OADateToSeconds(this double oadate)
        {
            return oadate * CommonConstants.SecondsIn24Hours;
        }
    }
}
