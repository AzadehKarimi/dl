﻿using System;
using System.Globalization;

namespace DtectLoss.Common
{
    public static class Helpers
    {
        private static readonly CultureInfo CultureInfo = new CultureInfo("nb-NO", false);
        public static string RenameToValidString(this string input)
        {
            return input.Replace(":", "_").Replace(".","_").Replace(" ", "_").Replace("-", "_");
        }

        public static double ValidDouble(this double input)
        {
            return double.IsNaN(input) ? CommonConstants.MissingValueReplacement : input;
        }

        public static int BoolToInt(bool val)
        {
            return Convert.ToInt32(val);
        }
        public static double ParseIso8601Date(string inputDate)
        {
            if (inputDate != null && inputDate.Length > 10)
            {
                var date = DateTime.Parse(inputDate, CultureInfo).ToUniversalTime().ToOADate();
                return date;
            }

            return 0;
        }
        public static double ParseValue(string inputValue)
        {
            var s = inputValue.Replace('.', ',');
            var num = double.TryParse(s, NumberStyles.Any, CultureInfo, out var number)
                ? number
                : CommonConstants.MissingValueReplacement;
            return num;
        }
    }
}
