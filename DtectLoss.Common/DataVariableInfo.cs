﻿using System.Collections.Generic;

namespace DtectLoss.Common
{
    public class DataVariableInfo
    {
        public string Name { get;}
        public string Unit { get;}
        public List<DataVariable> Variables { get;}

        public DataVariableInfo(string name, string unit, List<DataVariable> variables)
        {
            Variables = variables;
            Name = name;
            Unit = unit;
        }
    }
}