﻿using System.Collections.Generic;

namespace DtectLoss.Common
{
    public class VariableUnitMapping
    {
        public string? DefaultUnit { get; set; }
        public Dictionary<string, double>? UnitFactors { get; set; }
    }
}