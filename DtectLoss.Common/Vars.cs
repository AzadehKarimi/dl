﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DtectLoss.Common
{
    public static class V
    {
        public const string FlowInLpm = "flowIn_lpm";
        public const string FlowInfiltLpm = "flowInfilt_lpm";
        public const string FlowOut = "flowOut";
        public const string FlowOutLpm = "flowOut_lpm";
        public const string FlowOutFilteredLpm = "flowOutFiltered_lpm";
        public const string PresSp = "presSP";
        public const string PresSpBar = "presSP_bar";
        public const string VelBlockMph = "velBlock_mph";
        public const string VelRop = "velRop";
        public const string VelRopMph = "velRop_mph";
        public const string VelBitMph = "velBit_mph";
        public const string VolTripTank = "volTripTank";
        public const string VolTripTankM3 = "volTripTank_m3";
        public const string VolGainLossM3 = "volGainLoss_m3";
        public const string VolGainLossWithoutOffsetM3 = "volGainLossWithoutOffset_m3";
        public const string DepthBit = "depthBit";
        public const string DepthBitM = "depthBit_m";
        public const string DepthHoleM = "depthHole_m";
        public const string HeighHook = "heightHook";
        public const string HeighHookM = "heightHook_m";
        public const string Rpm = "rpm";
        public const string TempBhaDegC = "tempBHA_degC";
    }
} 