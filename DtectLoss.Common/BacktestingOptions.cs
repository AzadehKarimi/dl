﻿namespace DtectLoss.Common
{
    public class BacktestingOptions
    {
        public bool UseInfluxDbAsTimeseriesLogging { get; set; }
        public bool UseOutputScenarioResults { get; set; }
        public string? RootDir { get; set; }
        public string? OutputScenarioDir { get; set; }
        public int percentToRun { get; set; }
        public int PercentToRunInDebug { get; set; }
        public string[]? NameDesiredProcessTags { get; set; }
    }
}
