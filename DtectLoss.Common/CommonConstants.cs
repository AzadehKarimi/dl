﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DtectLoss.Common
{
    public static class CommonConstants
    {
        public const double SecondsIn24Hours = 60 * 60 * 24;
        public const double MissingValueReplacement = -0.99925;
        public const double TimeSlackSeconds = 10; // 5 Allow up to 5 sec difference in time stamp, set meas to -0.99925 if bigger
    }
}
