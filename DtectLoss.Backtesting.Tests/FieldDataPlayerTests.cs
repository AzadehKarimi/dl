﻿using System;
using System.Collections.Generic;
using System.Globalization;
using DtectLoss.Common;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DtectLoss.Backtesting.Tests
{
    [TestClass, TestCategory("Backtest"), TestCategory("NoResourceUsageIntegrationTest")]
    public class FieldDataPlayerTests
    {
        private const double Tolerance = 1.157407407407407e-06; // 0.1/86400=(0.1 second tolerance)
        //private const double missingValueReplacement = -0.99925;
        private const string Key1 = "Test1";
        private const string Key2 = "Test2";
        private const string Key3 = "Test3";

        [TestMethod]
        public void ValidInputInitializeOk()
        {
            var nullLogger = new Mock<ILogger<FieldDataPlayer>>().Object;
            var fdp = new FieldDataPlayer(nullLogger);
            var data = new Dictionary<string, DataVariableInfo>()
            {
                {Key1, new DataVariableInfo(
                
                    Key1, "m", new List<DataVariable>()
                    {
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:00").ToOADate(), 1.1)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:02").ToOADate(), 1.2)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:05").ToOADate(), 1.3)},
                    }
                )},
                {Key2, new DataVariableInfo(
                
                    Key2, "m", new List<DataVariable>()
                    {
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:00").ToOADate(), 2.1)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:02").ToOADate(), 2.2)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:05").ToOADate(), 2.3)},
                    }
                )},
                {Key3, new DataVariableInfo(
                
                    Key3, "m", new List<DataVariable>()
                    {
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:00").ToOADate(), 3.1)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:02").ToOADate(), 3.2)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:05").ToOADate(), 3.3)},
                    }
                )},
            };
            var desiredVariables = new string[] {Key1, Key2, Key3};
            fdp.Initialize(data, desiredVariables);

            Assert.IsTrue(Math.Abs(fdp.Output[Key1].V - 1.1) < Tolerance);
            Assert.IsTrue(fdp.Output.Count == 3);
        }
        [TestMethod]
        public void InitializeMissingDesiredVariable()
        {
            var nullLogger = new Mock<ILogger<FieldDataPlayer>>().Object;
            var fdp = new FieldDataPlayer(nullLogger);
            var data = new Dictionary<string, DataVariableInfo>()
            {
                {Key1, new DataVariableInfo(
                
                    Key1, "m", new List<DataVariable>()
                    {
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:00").ToOADate(), 1.1)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:02").ToOADate(), 1.2)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:05").ToOADate(), 1.3)},
                    }
                )},
                {Key2, new DataVariableInfo(
                
                    Key2, "m", new List<DataVariable>()
                    {
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:00").ToOADate(), 2.1)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:02").ToOADate(), 2.2)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:05").ToOADate(), 2.3)},
                    }
                )},
                {"Unknown", new DataVariableInfo(
                
                    Key3, "m", new List<DataVariable>()
                    {
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:00").ToOADate(), 3.1)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:02").ToOADate(), 3.2)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:05").ToOADate(), 3.3)},
                    }
                )},
            };
            var desiredVariables = new string[] {Key1, Key2, Key3};
            fdp.Initialize(data, desiredVariables);

            Assert.IsTrue(fdp.Output.Count == 2);
        }
        [TestMethod]
        public void ValidInputInitializeAndStepAsyncOk()
        {
            var nullLogger = new Mock<ILogger<FieldDataPlayer>>().Object;
            var fdp = new FieldDataPlayer(nullLogger);
            var data = new Dictionary<string, DataVariableInfo>()
            {
                {Key1, new DataVariableInfo(
                
                    Key1, "m", new List<DataVariable>()
                    {
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:00").ToOADate(), 1.1)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:02").ToOADate(), 1.2)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:05").ToOADate(), 1.3)},
                    }
                )},
                {Key2, new DataVariableInfo(
                
                    Key2, "m", new List<DataVariable>()
                    {
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:00").ToOADate(), 2.1)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:02").ToOADate(), 2.2)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:05").ToOADate(), 2.3)},
                    }
                )},
                {Key3, new DataVariableInfo(
                
                    Key3, "m", new List<DataVariable>()
                    {
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:00").ToOADate(), 3.1)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:02").ToOADate(), 3.2)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:05").ToOADate(), 3.3)},
                    }
                )},
            };
            var desiredVariables = new string[] {Key1, Key2, Key3};
            fdp.Initialize(data, desiredVariables);
            fdp.AsyncPlay();

            Assert.IsTrue(Math.Abs(fdp.Output[Key1].V - 1.2) < Tolerance);
            Assert.IsTrue(fdp.Output.Count == 3);
        }
        [TestMethod]
        public void ValidInputInitializeAndStepAsyncFillMissingStartOk()
        {
            var nullLogger = new Mock<ILogger<FieldDataPlayer>>().Object;
            var fdp = new FieldDataPlayer(nullLogger);
            var data = new Dictionary<string, DataVariableInfo>()
            {
                {Key1, new DataVariableInfo(
                
                    Key1, "m", new List<DataVariable>()
                    {
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:00").ToOADate(), 1.1)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:02").ToOADate(), 1.2)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:05").ToOADate(), 1.3)},
                    }
                )},
                {Key2, new DataVariableInfo(
                
                    Key2, "m", new List<DataVariable>()
                    {
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:05").ToOADate(), 2.1)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:07").ToOADate(), 2.2)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:08").ToOADate(), 2.3)},
                    }
                )},
                {Key3, new DataVariableInfo(
                
                    Key3, "m", new List<DataVariable>()
                    {
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:00").ToOADate(), 3.1)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:02").ToOADate(), 3.2)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:05").ToOADate(), 3.3)},
                    }
                )},
            };
            var desiredVariables = new string[] {Key1, Key2, Key3};
            fdp.Initialize(data, desiredVariables);
            fdp.AsyncPlay();

            Assert.IsTrue(Math.Abs(fdp.Output[Key2].V - CommonConstants.MissingValueReplacement) < Tolerance);
            Assert.IsTrue(fdp.Output.Count == 3);
        }
        [TestMethod]
        public void ValidInputInitializeAndStepAsyncFillMissingTimeValues()
        {
            var nullLogger = new Mock<ILogger<FieldDataPlayer>>().Object;
            var fdp = new FieldDataPlayer(nullLogger);
            var data = new Dictionary<string, DataVariableInfo>()
            {
                {Key1, new DataVariableInfo(
                
                    Key1, "m", new List<DataVariable>()
                    {
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:00").ToOADate(), 1.1)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:02").ToOADate(), 1.2)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:05").ToOADate(), 1.3)},
                    }
                )},
                {Key2, new DataVariableInfo(
                
                    Key2, "m", new List<DataVariable>()
                    {
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:00").ToOADate(), 2.1)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:07").ToOADate(), 2.2)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:08").ToOADate(), 2.3)},
                    }
                )},
                {Key3, new DataVariableInfo(
                
                    Key3, "m", new List<DataVariable>()
                    {
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:00").ToOADate(), 3.1)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:02").ToOADate(), 3.2)},
                        {new DataVariable(DateTime.Parse("1999-01-03 12:00:05").ToOADate(), 3.3)},
                    }
                )},
            };
            var desiredVariables = new[] {Key1, Key2, Key3};
            fdp.Initialize(data, desiredVariables);
            fdp.AsyncPlay();

            Assert.IsTrue(Math.Abs(fdp.Output[Key2].V - 2.1) < Tolerance);
            Assert.IsTrue(fdp.Output.Count == 3);
        }

        [TestMethod]
        public void ValidGetPossibleStep()
        {
            var ct = DateTime.Parse("1999-01-03 12:00:00").ToOADate();
            var nt = DateTime.Parse("1999-01-03 12:00:01").ToOADate();

            var possibleNextTime = new Dictionary<string, double>
            {
               {Key1, nt }, {Key2, nt }, {Key3, nt },
            };
            var possibleNextStep = new Dictionary<string, int>
            {
               {Key1, 1 }, {Key2, 1 }, {Key3, 1 },
            };
            var currentIndex = new Dictionary<string, int>
            {
               {Key1, 1 }, {Key2, 1 }, {Key3, 1 },
            };
            var currentTime = new Dictionary<string, double>
            {
               {Key1, ct }, {Key2, ct }, {Key3, ct },
            };
            var (minT, newIndex) =
                FieldDataPlayer.GetPossibleAsyncStep(possibleNextTime, possibleNextStep, currentIndex, currentTime);
            Assert.AreEqual(nt, minT);
            Assert.AreEqual(2, newIndex[Key1]);
            Assert.IsTrue(newIndex.Count == currentIndex.Count);
        }

        [TestMethod]
        public void ValidGetPossibleStepOneKey()
        {
            var Key1 = "Test1";
            var Key2 = "Test2";
            var Key3 = "Test3";
            var ct = DateTime.Parse("1999-01-03 12:00:00").ToOADate();
            var nt = DateTime.Parse("1999-01-03 12:00:01").ToOADate();

            var possibleNextTime = new Dictionary<string, double>
            {
               {Key1, nt }, {Key2, ct }, {Key3, ct },
            };
            var possibleNextStep = new Dictionary<string, int>
            {
               {Key1, 1 }, {Key2, 1 }, {Key3, 1 },
            };
            var currentIndex = new Dictionary<string, int>
            {
               {Key1, 1 }, {Key2, 1 }, {Key3, 1 },
            };
            var currentTime = new Dictionary<string, double>
            {
               {Key1, ct }, {Key2, ct }, {Key3, ct },
            };
            var (minT, newIndex) =
                FieldDataPlayer.GetPossibleAsyncStep(possibleNextTime, possibleNextStep, currentIndex, currentTime);
            Assert.AreEqual(nt, minT);
            Assert.AreEqual(2, newIndex[Key1]);
            Assert.AreEqual(1, newIndex[Key2]);
            Assert.IsTrue(newIndex.Count == currentIndex.Count);
        }
    }
}
