using System;
using System.Linq;
using DtectLoss.FileHandling;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using OfficeOpenXml;
using Microsoft.Extensions.Logging;

namespace DtectLoss.Backtesting.Tests
{
    [TestClass, TestCategory("Backtest"), TestCategory("NoResourceUsageIntegrationTest")]
    public class ExcelScenariosTests
    {
        private ExcelPackage excelPackage;
        public ExcelScenariosTests()
        {
            excelPackage = new ExcelPackage();
            
        }
        private ExcelWorksheet SetupEmptyWorksheet()
        {
            ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Testworksheet");
            return worksheet;
        }

        private ExcelWorksheet SetupDefaultWorksheet()
        {
            var worksheet = SetupEmptyWorksheet();
            worksheet.Cells[1, 1].Value = "ID";
            worksheet.Cells[2, 1].Value = 1.0;
            worksheet.Cells[1, 2].Value = "Operation";
            worksheet.Cells[2, 2].Value = "Drilling";
            worksheet.Cells[1, 3].Value = "Rig";
            worksheet.Cells[2, 3].Value = "SimRig";
            worksheet.Cells[1, 4].Value = "Field";
            worksheet.Cells[1, 5].Value = "Wellbore";
            worksheet.Cells[1, 6].Value = "Section";
            worksheet.Cells[1, 7].Value = "Type";
            worksheet.Cells[1, 8].Value = "Approval";
            worksheet.Cells[1, 9].Value = "Link";
            worksheet.Cells[1, 10].Value = "Start";
            worksheet.Cells[1, 11].Value = "Stop";
            worksheet.Cells[1, 12].Value = "StartEvent";
            worksheet.Cells[1, 13].Value = "StopEvent";
            worksheet.Cells[1, 14].Value = "Flow in";
            worksheet.Cells[1, 15].Value = "Hole depth";
            worksheet.Cells[1, 16].Value = "Bit depth";
            worksheet.Cells[1, 17].Value = "Gain/loss";
            worksheet.Cells[1, 18].Value = "StoplossComment";
            worksheet.Cells[1, 19].Value = "DtectLossUseFlowTag";
            worksheet.Cells[2, 19].Value = "flowOutPercent";
            worksheet.Cells[1, 20].Value = "DtectLossUseTripTankTag";
            worksheet.Cells[2, 20].Value = "volTripTank";
            worksheet.Cells[1, 21].Value = "DtectLossUsePitVolTag";
            worksheet.Cells[2, 21].Value = "volGainLoss";
            worksheet.Cells[1, 22].Value = "DtectLossUseForBacktest";
            worksheet.Cells[2, 22].Value = 1.0;
            worksheet.Cells[1, 23].Value = "SIMPLE_ALG_OBJ()";
            worksheet.Cells[2, 23].Value = 0.0;
            worksheet.Cells[1, 24].Value = "PDIFF_ALG_OBJ()";
            worksheet.Cells[2, 24].Value = 0.0;
            worksheet.Cells[1, 25].Value = "UKF_ALG_OBJ()";
            worksheet.Cells[2, 25].Value = 0.0;
            worksheet.Cells[1, 26].Value = "VOTING_ALG_OBJ()";
            worksheet.Cells[2, 26].Value = 1.0;
            worksheet.Cells[1, 27].Value = "AREA_POOH";
            worksheet.Cells[2, 27].Value = 0.003758226;
            worksheet.Cells[1, 28].Value = "AREA_RIH";
            worksheet.Cells[2, 28].Value = 0.015320131;
            return worksheet;
        }

        [TestMethod]
        public void ReadIncorrectDataReturnsEmpty()
        {
            var worksheet = SetupEmptyWorksheet();
            worksheet.Cells[1, 1].Value = "ID";
            worksheet.Cells["A2"].Value = 12001;
            //var nullLogger = Microsoft.Extensions.Logging.Abstractions.NullLoggerFactory.Instance;
            var nullLogger = new Mock<ILogger<ExcelReader>>().Object;
            var excelReader = new ExcelReader(nullLogger);
            var result = excelReader.ReadXlsConfigWorksheet(worksheet);
            Assert.IsTrue(!result.Any());
        }

        [TestMethod]
        public void ReadDefaultReturnsSingleResult()
        {
            var worksheet = SetupDefaultWorksheet();
            //var nullLogger = Microsoft.Extensions.Logging.Abstractions.NullLoggerFactory.Instance;
            var nullLogger = new Mock<ILogger<ExcelReader>>().Object;
            var excelReader = new ExcelReader(nullLogger);
            var result = excelReader.ReadXlsConfigWorksheet(worksheet);
            Assert.IsTrue(result.Any());
        }

        [TestMethod]
        public void MissingTagReturnsEmptyResult()
        {
            var worksheet = SetupDefaultWorksheet();
            worksheet.Cells[1, 20].Value = "Incorrect - DtectLossUseTripTankTag";
            //var nullLogger = Microsoft.Extensions.Logging.Abstractions.NullLoggerFactory.Instance;
            var nullLogger = new Mock<ILogger<ExcelReader>>().Object;
            var excelReader = new ExcelReader(nullLogger);
            var result = excelReader.ReadXlsConfigWorksheet(worksheet);
            Assert.IsTrue(!result.Any());
        }
        [TestMethod]
        public void MissingAlgorithmsReturnsEmptyResult()
        {
            var worksheet = SetupDefaultWorksheet();
            worksheet.Cells[1, 23].Value = "removed1";
            worksheet.Cells[2, 23].Value = 0.0;
            worksheet.Cells[1, 24].Value = "removed2";
            worksheet.Cells[2, 24].Value = 0.0;
            worksheet.Cells[1, 25].Value = "removed3";
            worksheet.Cells[2, 25].Value = 0.0;
            worksheet.Cells[1, 26].Value = "removed4";
            //var nullLogger = Microsoft.Extensions.Logging.Abstractions.NullLoggerFactory.Instance;
            var nullLogger = new Mock<ILogger<ExcelReader>>().Object;
            var excelReader = new ExcelReader(nullLogger);
            var result = excelReader.ReadXlsConfigWorksheet(worksheet);
            Assert.IsTrue(!result.Any());
        }
    }
}
