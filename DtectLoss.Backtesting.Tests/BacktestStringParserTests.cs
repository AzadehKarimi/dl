using System;
using System.Linq;
using DtectLoss.FileHandling;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using OfficeOpenXml;
using Microsoft.Extensions.Logging;

namespace DtectLoss.Backtesting.Tests
{
    [TestClass, TestCategory("Backtest")]
    public class BacktestResultStringParserTests
    {
        public BacktestResultStringParserTests()
        {
            
        }


        [TestMethod]
        public void ParseStatisticsCountDataSets()
        {
            var result = BacktestResultStringParser.ParseCountOfDataSets("Statistics after running 20 data sets: ");
            var expected = 20;
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void ParseStatisticsCountDataSetsMissingTrailingShouldFail()
        {
            Assert.ThrowsException<Sprache.ParseException>(() => BacktestResultStringParser.ParseCountOfDataSets("Statistics after running 20: "));
        }
        [TestMethod]
        public void ParseStatisticsCountDataSetsMissingNumberShouldFail()
        {
            Assert.ThrowsException<Sprache.ParseException>(() => BacktestResultStringParser.ParseCountOfDataSets("Statistics after running data sets: "));
        }
        
        
        /// <summary>
        /// //////
        /// </summary>
        
        [TestMethod]
        public void ParseTotalSimulationTimeStats()
        {
            var result = BacktestResultStringParser.ParseTotalSimulationTimeStats("Total simulation time: 673.72 hours, alg up-time: 642.58 hours (95.38%)");
            var expected = new TotalSimulationTimeStats("673.72", "642.58", "95.38");
            Assert.AreEqual(expected.Hours, result.Hours);
            Assert.AreEqual(expected.AlgUptimeHours, result.AlgUptimeHours);
            Assert.AreEqual(expected.AlgUptimePercent, result.AlgUptimePercent);
        }

        [TestMethod]
        public void ParseTotalSimulationTimeStatsFailsMissingText()
        {
            Assert.ThrowsException<Sprache.ParseException>(() => BacktestResultStringParser.ParseTotalSimulationTimeStats("Total simulation time 673.72 hours, alg up-time: 642.58 hours (95.38%)"));
        }
        
        [TestMethod]
        public void ParseTotalSimulationKpis()
        {
            //KPI: 0.35, FAR: 0.34 alarms / active hour, PD: 100.00 %
            var result = BacktestResultStringParser.ParseTotalSimulationKpis("KPI: 0.35, FAR: 0.34 alarms/active hour, PD: 100.00%");
            var expected = new TotalSimulationKpis("0.35", "0.34", "100.00");
            Assert.AreEqual(expected.Kpi, result.Kpi);
            Assert.AreEqual(expected.Far, result.Far);
            Assert.AreEqual(expected.Pd, result.Pd);
        }
    }

}
