using System;
using System.Collections.Generic;
using System.Linq;
using DtectLoss.Common;
using DtectLoss.FileHandling;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using OfficeOpenXml;
using Microsoft.Extensions.Logging;
using OfficeOpenXml.FormulaParsing.Excel.Functions.RefAndLookup;

namespace DtectLoss.Backtesting.Tests
{
    //[TestClass, TestCategory("Backtest"), TestCategory("NoResourceUsageIntegrationTest")]
    [TestClass, TestCategory("Backtest")]
    public class DataFileReaderTests
    {

        [TestMethod]
        public void ParseValidHeaderMetadata()
        {
            string[] nameRow = {"a1", "a1", "a2", "a2", "a3", "a3"};
            string[] metadataRow = {"INDEX", "VALUE", "INDEX", "VALUE", "INDEX", "VALUE"};
            string[] unitRow = {"time", "m", "time", "lpm", "time", "m3" };
            //var nullLogger = Microsoft.Extensions.Logging.Abstractions.NullLoggerFactory.Instance;
            var nullLogger = new Mock<ILogger<CsvDatafileReader>>().Object;
            var fileReader = new CsvDatafileReader(nullLogger);
            var result = fileReader.ParseVariableMetadata(nameRow, metadataRow, unitRow);
            Assert.IsTrue(result.Any());
        }
        [TestMethod]
        public void ParseInvalidHeaderMetadataNames()
        {
            string[] nameRow = {"a1", "a1", "a2", "a3", "a3"};
            string[] metadataRow = {"INDEX6", "VALUE", "INDEX", "VALUE", "INDEX", "VALUE"};
            string[] unitRow = {"time", "m", "time", "lpm", "time", "m3" };
            //var nullLogger = Microsoft.Extensions.Logging.Abstractions.NullLoggerFactory.Instance;
            var nullLogger = new Mock<ILogger<CsvDatafileReader>>().Object;
            var fileReader = new CsvDatafileReader(nullLogger);
            Assert.ThrowsException<InvalidOperationException>(() => fileReader.ParseVariableMetadata(nameRow, metadataRow, unitRow));
        }
        [TestMethod]
        public void ParseInvalidHeaderMetadataRowTypes()
        {
            string[] nameRow = {"a1", "a1", "a2", "a2", "a3", "a3"};
            string[] metadataRow = {"INDEX", "INDEX", "INDEX", "VALUE", "INDEX", "VALUE"};
            string[] unitRow = {"time", "m", "time", "lpm", "time", "m3" };
            //var nullLogger = Microsoft.Extensions.Logging.Abstractions.NullLoggerFactory.Instance;
            var nullLogger = new Mock<ILogger<CsvDatafileReader>>().Object;
            var fileReader = new CsvDatafileReader(nullLogger);
            Assert.ThrowsException<InvalidOperationException>(() => fileReader.ParseVariableMetadata(nameRow, metadataRow, unitRow));
        }
        [TestMethod]
        public void ParseInvalidHeaderMetadataMissingUnits()
        {
            string[] nameRow = {"a1", "a1", "a2", "a2", "a3", "a3"};
            string[] metadataRow = {"INDEX", "VALUE", "INDEX", "VALUE", "INDEX", "VALUE"};
            string[] unitRow = {"time", "m"};
            //var nullLogger = Microsoft.Extensions.Logging.Abstractions.NullLoggerFactory.Instance;
            var nullLogger = new Mock<ILogger<CsvDatafileReader>>().Object;
            var fileReader = new CsvDatafileReader(nullLogger);
            Assert.ThrowsException<IndexOutOfRangeException>(() => fileReader.ParseVariableMetadata(nameRow, metadataRow, unitRow));
        }

        [TestMethod]
        public void ValidMapMnemonicToVariableName()
        {
            Dictionary<string, DataVariableInfo> variables = new Dictionary<string, DataVariableInfo>
            {
                {"depthBit", null },
                {"tempIn", null },
                {"tempOut", null },
                {"flowIn", null },
            };
            var mnemonicsSource = MnemonicDataMapping.mapping;
            //var nullLogger = Microsoft.Extensions.Logging.Abstractions.NullLoggerFactory.Instance;
            var nullLogger = new Mock<ILogger<CsvDatafileReader>>().Object;
            var fileReader = new CsvDatafileReader(nullLogger);
            var newMapping = fileReader.MapMnemonicsToVariables(MnemonicDataMapping.mapping, variables);
            Assert.IsTrue(variables.Count == newMapping.Count);


        }

        [TestMethod]
        public void ValidMapMnemonicToVariableNameSpecialName()
        {
            Dictionary<string, DataVariableInfo> variables = new Dictionary<string, DataVariableInfo>
            {
                {"depthBit", null },
                {"tempIn", null },
                {"tempOut", null },
                {"GS_TFLO", null },
            };
            var mnemonicsSource = MnemonicDataMapping.mapping;
            //var nullLogger = Microsoft.Extensions.Logging.Abstractions.NullLoggerFactory.Instance;
            var nullLogger = new Mock<ILogger<CsvDatafileReader>>().Object;
            var fileReader = new CsvDatafileReader(nullLogger);
            var newMapping = fileReader.MapMnemonicsToVariables(MnemonicDataMapping.mapping, variables);
            Assert.IsTrue(variables.Count == newMapping.Count);
        }
        [TestMethod]
        public void InvalidMapMnemonicToVariableName()
        {
            Dictionary<string, DataVariableInfo> variables = new Dictionary<string, DataVariableInfo>
            {
                {"depthBit", null },
                {"tempIn", null },
                {"tempOut", null },
                {"not_found_in_list", null },
            };
            var mnemonicsSource = MnemonicDataMapping.mapping;
            //var nullLogger = Microsoft.Extensions.Logging.Abstractions.NullLoggerFactory.Instance;
            var nullLogger = new Mock<ILogger<CsvDatafileReader>>().Object;
            var fileReader = new CsvDatafileReader(nullLogger);
            Assert.ThrowsException<KeyNotFoundException>(() => fileReader.MapMnemonicsToVariables(MnemonicDataMapping.mapping, variables));
        }

    }
}
