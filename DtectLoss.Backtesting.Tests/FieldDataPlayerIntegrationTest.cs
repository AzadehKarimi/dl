﻿using System.IO;
using System.Linq;
using System.Reflection;
using DtectLoss.FileHandling;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DtectLoss.Backtesting.Tests
{
    [TestClass, TestCategory("Backtest")]
    public class FieldDataPlayerIntegrationTests
    {
        private string _fileName;
        private IFieldDataPlayer _fdp;
        private readonly string[] _varNames = { "depthBit", "flowIn", "flowOut", "volPitDrill"  }; 

        [TestInitialize]
        public void Initialize()
        {
            var nullLogger = new Mock<ILogger<CsvDatafileReader>>().Object;
            var csvDatafileReader = new CsvDatafileReader(nullLogger);
            _fileName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\HelperFiles\smallCurves.csv";
            var data = csvDatafileReader.ReadCsvFile(_fileName);
            var fdpLogger = new Mock<ILogger<FieldDataPlayer>>().Object;
            _fdp = new FieldDataPlayer(fdpLogger);
            _fdp.Initialize(data, _varNames );
        }

        [TestMethod]
        public void constructor_correctInit_correctFieldDataPlayer()
        {
            CollectionAssert.AreEqual(new [] {179, 179, 179, 17}, _fdp.NDataPoints.Values);
            CollectionAssert.AreEqual(_varNames, _fdp.VariableNames.ToArray());
            CollectionAssert.AreEqual(new [] {0,0,0,0}, _fdp.CurrentIndex.Values);
        }

        [TestMethod]
        public void initialize_correctInit_correctOutput()
        {
            const double expOutputBitDepthV = 1783.8;
            const double expOutputFlowInV = 0.017671363;
            const int expOutputFlowOutV = 0;
            const double expOutputPitDrillV = 76.571633;
            const int expOutputBitDepthT = 42511;
            const int expOutputFlowInT = 42511;
            const int expOutputFlowOutT = 42511;
            const int expOutputPitDrillT = 42511;

            Assert.AreEqual(expOutputBitDepthT, _fdp.Output["depthBit"].T);
            Assert.AreEqual(expOutputFlowInT, _fdp.Output["flowIn"].T);
            Assert.AreEqual(expOutputFlowOutT, _fdp.Output["flowOut"].T);
            Assert.AreEqual(expOutputPitDrillT, _fdp.Output["volPitDrill"].T);

            Assert.AreEqual(expOutputBitDepthV, _fdp.Output["depthBit"].V);
            Assert.AreEqual(expOutputFlowInV, _fdp.Output["flowIn"].V);
            Assert.AreEqual(expOutputFlowOutV, _fdp.Output["flowOut"].V);
            Assert.AreEqual(expOutputPitDrillV, _fdp.Output["volPitDrill"].V);

            Assert.AreEqual(expOutputBitDepthT, _fdp.StartTime);
            Assert.AreEqual(expOutputBitDepthT, _fdp.AbsTime);
        }

        [TestMethod]
        public void asyncPlay_sixtySeconds_correctOutput()
        {
            var nullLogger = new Mock<ILogger<CsvDatafileReader>>().Object;
            var csvDatafileReader = new CsvDatafileReader(nullLogger);
            _fileName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\HelperFiles\expectedOutputAsyncPlay.csv";
            var expOutput = csvDatafileReader.ReadCsvFile(_fileName);
            foreach (var ii in Enumerable.Range(0, expOutput["flowIn"].Variables.Count - 1))
            {
                _fdp.AsyncPlay();
                foreach (var kk in Enumerable.Range(0, _fdp.VariableNames.Count()))
                {
                    Assert.AreEqual(expOutput.ElementAt(kk).Value.Variables[ii].T, _fdp.Output[_varNames[kk]].T  , $"Verification of time failed at idx {ii} and variable {_varNames[kk]}");
                    Assert.AreEqual(expOutput.ElementAt(kk).Value.Variables[ii].V, _fdp.Output[_varNames[kk]].V  , $"Verification of value failed at idx {ii} and variable {_varNames[kk]}");
                }
            }
        }

        [TestMethod]
        public void asyncPlay_entireFile_noError()
        {
            var ii = 1;
            while (ii < 400 && _fdp.CurrentIndex.Values.Max() < _fdp.NDataPoints.Values.Max())
            {
                ii++;
                _fdp.AsyncPlay();
            }
        }

        [TestMethod]
        public void asyncPlay_entireFile_reportStatus()
        {
            Assert.AreEqual(0, _fdp.SimProgress);
            Assert.AreEqual(498, _fdp.SimDuration);
            var ii = 1;
            while (ii < 500 && _fdp.CurrentIndex.Values.Max() < _fdp.NDataPoints.Values.Max())
            {
                ii++;
                _fdp.AsyncPlay();
            }
            Assert.AreEqual(100, _fdp.SimProgress);
        }

        [TestMethod]
        public void asyncPlay_absTime_noDuplicates()
        {
            Assert.AreEqual(0, _fdp.SimProgress);
            Assert.AreEqual(498, _fdp.SimDuration);
            var prevAbsTime = _fdp.AbsTime;
            var ii = 0;
            while (_fdp.SimProgress < 100 && ii < 2e5)
            {
                _fdp.AsyncPlay();
                Assert.IsTrue(_fdp.AbsTime > prevAbsTime, $"Abs time is equal to prev abs time for idx {ii}");
                ii++;
            prevAbsTime = _fdp.AbsTime;
            }
        }
    }
}
