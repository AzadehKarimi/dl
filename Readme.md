# Overview - DtectLoss

```Text
To preview in VSCode, use ctrl-shift-p <Markdown: Open Preview>
```

DtectLoss is an algorithm that automatically detects influx or loss in the well.

DtectLoss is based on three models:

- Volume
- Flow
- Pressure

If 2 out of 3 models detects influx or loss, DtectLoss will generate an alarm.
The alarm is the only action from DtectLoss.

This repository contains the source code for the realtime process to calculate DtectLoss and also code (backtesting) to verify correct calculations.

## Purpose of this project

- A DtectLoss.Realtime component that can be used to calculate DtectLoss
- Extensive backtesting and unit testing to provide verification of calculations

## Intent and focus of this documentation

- Easily accessible documentation, primary focused on developers
- Overview over project, architecture, technology and development
- Simple Howto's
- The documentation should be easy to find, edit and update.

## Structure

- Documentation/
  - [Domain knowledge](Documentation/DomainKnowledge.md)
  - [Architecture](Documentation/Architecture.md)
  - [Development](Documentation/Development.md)
  - [Environments](Documentation/Environments.md)
  - [Backtesting/verification](Documentation/Backtesting.md)
  - [Testing](Documentation/Testing.md)
  - [Docker](Documentation/Docker.md)
  - [Technical debt and planned technical features](Documentation/TechnicalDebt.md)
  - [Resources](Documentation/Resources.md)

