﻿using System;
using System.Diagnostics;
using System.Globalization;
using DtectLoss.Backtesting;
using DtectLoss.Common;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;

namespace DtectLoss.BacktestingConsole
{
    class Program
    {
        static void Main(string[] args)
        {

            IConfiguration appSettings = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false, true)
                .Build();
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(appSettings)
                .CreateLogger();

            var testRunDateString = DateTime.Now.ToString(CultureInfo.CurrentCulture).RenameToValidString();

            var serviceCollection = new ServiceCollection();
            var backTestingConfiguration = appSettings.GetSection("Backtesting");
            ServiceConfiguration.ConfigureServices(serviceCollection, backTestingConfiguration);

            using var logContext = Serilog.Context.LogContext.PushProperty("LoggingTestrun", testRunDateString);
            var serviceProvider = serviceCollection.BuildServiceProvider();
            var logger = serviceProvider.GetService<ILogger<Program>>();

            logger.LogInformation("Serilog logging started on console startup.");

            try
            {
                logger.LogInformation("Reading configuration");
                var bm = serviceProvider.GetService<IBacktestScenarioManager>();
                var toc = Stopwatch.StartNew();
                bm.Initialize(testRunDateString);
                bm.PerformBacktestingScenarios();
                toc.Stop();
                logger.LogInformation($"Time to do Main Loop: {toc.Elapsed.Minutes} minutes, {toc.Elapsed.Seconds} seconds.");
                Console.WriteLine("Press a key to stop execution");
                Console.ReadKey();
            }
            catch (Exception e)
            {
                logger.LogError(e.Message);
                Console.ReadLine();
                throw e;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }
    }
}
