# Glossary

## Domain spesific terms

- Loss
- Influx
- Volume
- Flow
- Pressure
- Paddle
- Gain
- Tripping
- Downlinking
- Circulation
- Washing data
- Calibration
- Voting
- Algoritm
- Drilling State
- Realtime
- Backtesting
- Kalman filter
- Lowpass filter
- Linear regression
- PiecewiseLinFit
- Derivative


## Development related terms

- Ringbuffer. A circular buffer, circular queue, cyclic buffer or ring buffer is a data structure that uses a single, fixed-size buffer as if it were connected end-to-end. This structure lends itself easily to buffering data streams.
- State machine. A state machine is a mathematical model of computation. It's an abstract concept whereby the machine can have different states, but at a given time fulfills only one of them
- Docker. Container technology. In this project, it is used to spin ut InfluxDb and Grafana to avoid running those on the local machine.
- Logging. Output from running code, normally in console and log files.
- InfluxDb. A timeseries database. Not to be confused with the domain term Influx.
- Grafana. Grafana is a multi-platform open source analytics and interactive visualization software. It provides charts, graphs, and alerts for the web when connected to supported data sources
- Git. Git is a distributed version-control system for tracking changes in source code during software development
- Repository. A code repository is where snippets and patches of source code for software programs are archived in an organized way.
- Console application. A console application is a computer program designed to be used via a text-only computer interface
- DevOps. DevOps is a set of practices that combines software development and information-technology operations which aims to shorten the systems development life cycle and provide continuous delivery with high software quality
- Nuget. NuGet is a free and open-source package manager designed for the Microsoft development platform
- Package. Developers with code to share create packages and publish them to a public or private host. Package consumers obtain those packages from suitable hosts, add them to their projects, and then call a package's functionality in their project code
