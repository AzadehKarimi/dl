# Archictecture and technical design

## Introduction

### Purpose

The intention of this document is to provide an easy way for developers and architects to get and overview of structure, design and technology choice.

### Scope

This document provides an architectual overview of DtectLoss Realtime and DtectLoss Backtesting.

### Definitions and abbreviations

See [Glossary](Glossary.md)

## Architectual representation

This document presents the architecture as a series of views; use case view, logical view, process view and deployment view.

## Architectual Goals and Constraints

- A separate and self-deployed Realtime library to be used in other applications
- An extensive testing and verification system to ensure correct algorithms and facilitate verification of any changes.
- Should be easy to setup development environment on any machine.

## Use Case View

A description of the use-case view of the software architecture. The Use Case View is important input to the selection of the set of scenarios and/or use cases that are the focus of an iteration. It describes the set of scenarios and/or use cases that represent some significant, central functionality. It also describes the set of scenarios and/or use cases that have a substantial architectural coverage (that exercise many architectural elements) or that stress or illustrate a specific, delicate point of the architecture.

### Architectually-Significant Use Cases

![Use Cases](images/UsecaseOverviewDtectLoss.png)

The DtectLossRealtime actors are:

- DtectLossRealtime Client Application (external user of DtectLossRealtime library)
- DtectLossRealtime library. Performs state handling and calculations.
- Realtime input data (external data from rig)
- HMI to show alarms and interact with DtectLossRealtime Client

The DtectLossRealtime use cases are:

- Initialize DtectLossRealtime
- Soft Reset DtectLossRealtime
- Calculate DtectLossRealtime Step. Processing new data and output results.

The DtectLossRealtime backtesting actors are:

- Developers
- Testers
- Domain experts
- DtectLossBacktesting application
- InfluxDb. Storing timeseries data
- Grafana Dashboards to view timeseries data.

The DtectLossRealtime backtesting use cases are:

- Unit and integration tests
- Process and verify simulated data
- Process and verify field data
- Log timeseries calculations and verification results
- View and verify dashboard Graphs to timeseries data
- Create and develop dashboard Graps to timeseries data


## Logical View

A description of the logical view of the architecture. Describes the most important classes, their organization in service packages and subsystems, and the organization of these subsystems into layers.

![Use Cases](images/logical_architecture.png)



## Process View

A description of the process view of the architecture. Describes the tasks (processes and threads) involved in the system's execution, their interactions and configurations.

Overall Architecture Process View

(Showing Backtesting and Realtime processes)

![Architecture](images/architecture.png)

DtectLoss RealTime Initialize Process

![Architecture](images/DtectLossRealtimeInitializeProcess.png)

DtectLoss RealTime Step Process

![Architecture](images/DtectLossRealtimeStepProcess.png)

DtectLoss RealTime Calculation logic process

![Architecture](images/algorithmfrontpage.png)

DtectLoss Backtesting process

![Architecture](images/DtectLossBacktestingProcess.png)

Keywords:

.NET Core, C#, Accord.NET, InfluxDB, Grafana, Docker, Serilog
