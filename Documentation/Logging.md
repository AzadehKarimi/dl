# Logging

For backend we are using Serilog for logging. This library can make use of different "sinks" to store the logging data.

Basic logging is currently output to Console and File (see appsettings.json). For the purpose of backtesting in combination with InfluxDb/Grafana, there is also a Serilog sink to Influx DB. This is to help getting all information and statistics for a backtest testrun into Influx and Grafana log viewer.

Logging to InfluxDb cause some extra delay due to network traffic/size and can be disabled if you only need to run the calculations.

To avoid logging to InfluxDb, there is a setting in appsettings.json to enable/disable output to InfluxDb. (Backtesting.useInfluxDbAsTimeseriesLogging=true/false)

The Matlab code has also been modified to output to InfluxDb. See Backtesting documentation for more information on how to enable/disable Matlab logging.
