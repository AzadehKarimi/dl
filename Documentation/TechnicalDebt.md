# Technical debt

According to Wikipedia - Technical debt is a concept in software development that reflects the implied cost of additional rework caused by choosing an easy solution now instead of using a better approach that would take longer. Todo's in source code are useful, but this document helps making technical debt visible. Check todo items in code regularly to see if any items should be listed here.

## Code analysis

- After copying Matlab code, there are several unused variables that can be removed.
- There are several TODOs copied from the Matlab code.
- The Matlab code had several places where debug logging was hidden behind an always false bool. This code has been commented out to prevent warnings of unreachable code.

## Other / Planned

- Perform improvements after findings during conversion to C# 
- Refactor Matlab coding conventions to C# - example: obj...

## Not in use anymore

- See unused folder
- Should any of the unused algorithms be used?

## Suggestions for improvements

- Perform load testing and performance after several days of uptime
- Refactor to separate stateful logic from stateless, pure code
- Refactor to clarify intent where possible
- Add unit tests for classes not covered by unit tests. Verify by using code coverage tools

## Risks, things to improve

- DtectLossRealtime should be queued, and only available to run when the previous execution has finished.

## Algorithms

Paddle converter only updates the calibrated status in a GatherK state, should be moved out of the state machine.

---
