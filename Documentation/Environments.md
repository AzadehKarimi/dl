# Environments

The main environment is the development environment. All coding, unit tests, integration tests and backtests can be run from this environment.

In addition to development environment, we use Azure Devops Pipelines and Git Flow branches to support a CI/CD pipeline. Developers develop on feature/bug branches and send a pull request to merge with the development branch. Pull requests and Development runs a build/test pipeline to verify that all code compiles and that all unit tests are working ok. There is also a pipeline together with a .nuspec file for DtectLossRealtime that pushes new versions to a Nuget artifact package repository specified in Nuget.config.

The docker images and containers used for InfluxDb and Grafana will keep their data until they are removed, but you should be aware that any timeseries data or dashboards can be lost with the current docker configuration.
