# GIT Workflow

## Create a pull request

### Step 1:

#### Command line approach

Fetch remote branches

- `git fetch`

Switch to the 'development' branch:

- `git checkout development`

Create a feature branch

- `git checkout -b feature/<YOUR_BRANCH_NAME>`
- `git branch to verify you're on the correct branch`

Make and save you changes.

- `git add .` add/stage changes locally (TAB to select individual files)
- `git commit -m "Commit comment"` will commit changes to local repo

OR

- `git commit -am "Commit comment"`

![Command window](images/cmd_commit.png)

- `git push` will result in an error if there is no upstream branch (1st time)

  - `fatal: The current branch feature/git-documentation has no upstream branch.`

    `To push the current branch and set the remote as upstream, use`

copy the command example provided
`git push --set-upstream origin feature/git-documentation`
subsequent pushes will be set to the correct stream (after adding and committing local changes)

#### Visual Studio

![Branch view in VS Team Explorer](images/repo_VS_feature.png)

![VS Changes with comment](images/VS_changes.png)

#### VS Code

![VS Code GIT SC](images/vscodeAddFiles.png)

![VS Code commit](images/commit.png)

### Step 2: When all changes have been committed and pushed

- In Azure DevOps https://dev.azure.com/MHWirth/_git/beAware

  ![View in devops/Repos](images/DevOpsView.png)

- Click 'Create Pull Request'
  ![Pull request](images/Pull_request.png)

- Choose development branch as destination (if not previously selected)
  ![Choose dev branch](images/Dev_Branch.png)
- Fill in a title and description and link to Work Items

## Review a pull request

### Step 1

To see your pull requests. Visit https://dev.azure.com/MHWirth/_git/beAware/pullrequests?_a=mine

If the pull request is assigned to you, you should also receive an email like the following:

![Pull request overview](images/Pull_request_email.png)

Click on the desired pull request to see the details.

### Step 2: Review changes

![Pull request overview](images/Pull_request_overview.png)

### Step 3: Accept the pull request

To start the process of accepting the pull request, click on "Complete".

### Step 4: A confirmation window appears

To accept the pull request, click "Complete merge".

For now, leave everything as is.

![Accept the pull request](images/Pull_request_confirmation.png)

The issuer of the pull request should get a confirmation that the pull request have been approved

## TODO: Next steps

![Delete the source branch](images/DeleteSourceBranch.png)
