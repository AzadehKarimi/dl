# How to keep your feature branch updated with the origin/development branch

- When you have crated a new feature branch, it is not tracking any upstram branch
- Ta enable tracking/syncing with origin/development, do the following:

```
git branch -vv (to see which origin branches they track)
```

To set your branch to track origin/develpment:

```
git branch --set-upstream-to origin/development
```

When you are rebasing (playing your commits as a merge on top of origin/development):

```
git fetch
git rebase origin/development
```
