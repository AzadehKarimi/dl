# Git Tips and Tricks

A small collection of tips and tricks for a easier day-to-day usage of git.

## Git config

There are two git configurations, the local and the global, that are used in git. If a configuration setting is set in both local and global configuration files, the setting that is local to the current repository will override the global configuration setting.

The local configuration file are located in `<RepoDir>/.git/config`

The global configuration file are located in `~/.gitconfig`. On windows the `~` resolves to the user directory under `C:\Users\<Username>`. For MHWirth systems this resolves to: `H:\` and the full path for the file becomes: `H:\.gitconfig`-

### Example .gitconfig

```text

[core]
        editor = 'C:\\Users\\XXXXXX\\AppData\\Local\\Programs\\Microsoft VS Code\\Code.exe' --wait
        autocrlf = true
[push]
        default = simple
[fetch]
        prune = true
[user]
        name = Ola Nordman
        email = ola.nordman@mhwirth.com
[alias]
        track = add -A
        lg = log --decorate=full --oneline
        lgg = log --decorate=full --oneline --graph
        unstage = reset HEAD --
        unadd = reset HEAD
        uncommit = reset --soft HEAD^
        savegame  = "!f(){ git add . && git commit --all -m WIP ; } ; f"
        savegame-add = "!f(){ git add . && git commit --all --amend -m WIP ; } ; f"
        diffs = diff --stat
        wip = diff --stat HEAD
        mnff = merge --no-ff
        prune = remote prune origin

[log]
        date = relative
[format]
        pretty = format:%C(yellow)%h %Cblue%>(12)%ad %Cgreen%<(7)%aN%Cred%d %Creset%s


```

### Explanations to example file

```text

[core]
        editor = 'C:\\Users\\XXXXXX\\AppData\\Local\\Programs\\Microsoft VS Code\\Code.exe' --wait
        autocrlf = true
```

Core configuration that sets the default git editor to VSCore and enables `autocrlf`. The autocrlf feature converts from LF into CRLF when code is checked out. For more information see the section about "Formatting and Whitespace" in the [Git Documentation](https://git-scm.com/book/en/v2/Customizing-Git-Git-Configuration)

To reset back to the default vim editor you can just remove the `editor` line from the config.

```text
[push]
        default = simple
```

This sets the default push action to "simple". There are 6 different actions to choose from. For more information have a look at the [documentation](https://mirrors.edge.kernel.org/pub/software/scm/git/docs/git-config.html)

> simple - in centralized workflow, work like upstream with an added safety to refuse to push if the upstream branch’s name is different from the local one.
>
> When pushing to a remote that is different from the remote you normally pull from, work as current. This is the safest option and is suited for beginners. This mode has become the default in Git 2.0.

```text
[fetch]
        prune = true
```

Automatically prune remote branches from local computer when fetching. (and pulling) NB: This does not delete the local branch if you have any. See the alias `cleanup` for more info.

```text
[user]
        name = Ola Nordman
        email = ola.nordman@mhwirth.com
```

The default username and email that will be used when commiting code. It considered good practice to have one single user when committing to a codebase.

```text
[log]
        date = relative
```

Change the git log to show time relative to now instead of just a date/timestamp. e.g. "2 hours ago".

```text
[format]
        pretty = format:%C(yellow)%h %Cblue%>(12)%ad %Cgreen%<(7)%aN%Cred%d %Creset%s
```

The default "pretty" format used for `log`, `show`, and `whatchanged` commands.

### Aliases

The config file contains a fair share of aliases that can be used to make life with Git easier. Aliases are often a matter of personal preference like any other thing in the git config, but these has been the aliases that I (Karl) has used for a long time. A alias is used after the `git` command like `git <alias>`. A small explanation for each of the aliases is listed.

`track` - Adds untracked files to the index and at the time time stages changes on already tracked files.

`lg` - Shows a cleaner and more compact log than default.

`lgg` - SHow a cleaner and more compact log than default but also adds the tree/graph to the view.

`unstage` - Unstages changes

`unadd` - resets the stages changes and removes files from the index that has been added since last commit.

`uncommit` - reverts back to last commit. But keeps all changes done since last commit as modified or untracked files.

`savegame` - Created a commit with the comment "WIP". Good for when you want to save some work before leaving the computer. (Remember to ether uncommit or use alias `savegame-add` to ammend to the commit or create a new and better commit with a good commit message when you are done. Commit messages like "WIP" is not good practice)

`savegame-add` - Appends changes to the last commit and changes the commit message to "WIP". If there was a commit message on the commit from before, this will be overwritten. So use with caution.

`diffs` - Show a list of the current changed files (that are not staged) and their cummulative changes.

`wip` - Show a list of the current changed files and their cummulative changes.

`mnff` - Merge with no `fast forward`. (See git documentation for more information about "Fast Forward" merges.)

`prune` - Prunes the remote branches that no longer exists from the local repository.

`cleanup` - Prunes and cleans up both remote and local branches that are no longer existing remote and merged into the branch where the user currently are. Use with some caution.

## Nice to know git "commandlets"

- `git status -s` - Cleaner and compact status listing.
- `git diff --stat` - Get current changes. (That are not "staged")
- `git diff --stat HEAD` - See the changed files since last commit

## Git-flow scripts

These scripts makes it easier to follow the Git-flow approach.

- [Installation instructions](https://github.com/nvie/gitflow/wiki/Installation)

For more information about how Git-flow works, [check out this blog post](https://jeffkreeftmeijer.com/git-flow/).
