# Docker

## Getting started with Docker

Docker can simplify both the development environment and deployment to production environment.

In this project, Docker is used to host InfluxDb and Grafana to help provide time series data and graphs to backtesting verification.

Docker works best with Windows 10, but it is also possible to run on Windows 7.

See this article and others on the internet for more information:
https://medium.com/codingthesmartway-com-blog/docker-beginners-guide-part-1-images-containers-6f3507fffc98

## Portainer setup

Portainer can be used to view/inspect the docker images

Portainer setup and running on local machine
$ docker volume create portainer_data
$ docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
