# Versioning

Versioning is inspired by this [article](https://cloudblogs.microsoft.com/industry-blog/en-gb/technetuk/2019/06/18/perfecting-continuous-delivery-of-nuget-packages-for-azure-artifacts/) and using [GitVersion](https://gitversion.net/docs/) to generate versions.

## Semantic versioning

DtectLossRealtime uses semantic versioning: {major}.{minor}.{patch}-{tag}

- major version when you make incompatible API changes,
- minor version when you add functionality in a backwards-compatible manner, and
- patch version when you make backwards-compatible bug fixes.

Although GitVersion calculates the version automatically, the developer has to tell the system the nature of the change.

### Using commit messages to version

- Adding `+semver: breaking` or `+semver: major` will cause the major version to be increased.
- Adding `+semver: feature` or `+semver: minor` as a commit message will cause the minor version to be increased.
- Similarly using `+semver: patch` or `+semver: fix` updates the patch version.
Branch name
- To skip version increments, use `+semver: none` or `+semver: skip`

### Using branch names to version

If you create a branch with the version number in the branch name such as `release-1.2.0` or `feature/1.0.1` then GitVersion will take the version number from the branch name as a source.

### Using git tags to version

By tagging a commit, GitVersion will use that tag for the version of that commit, then increment the next commit automatically based on the increment rules for that branch

## Build pipeline to support automatic versioning

Include this build step in the nuget configuration

``` YAML
    steps:
      - task: UseGitVersion@5
        displayName: GitVersion
        inputs:
          versionSpec: '5.x'
          useConfigFile: true
          configFilePath: 'gitversion.yml'
          updateAssemblyInfo: false
```

Include the version to the build argument

``` YAML
          arguments: "-c Release /p:Version=$(GitVersion.MajorMinorPatch)"
```

Include the versioning scheme to the pack command

``` YAML
          versioningScheme: byBuildNumber
```

Include this default version number to the nuspec file, `Nuget pack` will overwrite this version using the build number.

``` C#
<Version>0.0.0.0</Version>
```

Include this default version number to the project file.

``` C#
<Version>0.0.0.0</Version>
```

This will generate default version when built locally.
