# Resources

Links and information to tools, articles, howtos, etc.

## Tools / Components

- [Portainer - Docker management](https://portainer.io/install.html)
- [Docker](https://www.docker.com/)
- [Grafana](https://grafana.com/)
- [InfluxDB](https://www.influxdata.com/)
- [Accord.Math](http://accord-framework.net/)
- [Math.Net](https://numerics.mathdotnet.com/)
- [Serilog](https://serilog.net/)

---
