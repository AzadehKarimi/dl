﻿# Backtesting and verification of calculations

Backtesting is how entire data series are sent as input to the Realtime calculations and calculates any alams, warning and status for DtectLoss. These can be logged for each timestep if necessary. End results of a backtest run are displayed to console and log files.

Only one simulation dataset is available. This consists of 15 hours of data prepared to enable and verify the current calculations. Field data is also available depending on agreements.

The most structured and comprehensive way of running the calculations is to:

```text
> Enable InfluxDb timeseries logging in appsettings.json
> Set the BacktestConsole as the launch project
> Press F5 to launch the debugger

The prerequisites are that Influx/Grafana needs to run (See info below)
```

During and after running the simulation, the console, file logs and Grafana will show results and detail graphs from the simulation calculations.

The simplest way of running the backtest is to use the integration test. This should run in less than 15 seconds and assert several KPI values for correctness.

## Grafana Zoom

Be aware of the timerange of the original data!
For example field data COSL-7 from 2015-11-09

Set custom range some years back, and zoom in on the dot.

## Backtesting - setup and usage

Backtesting uses a set of datafiles located in

```text
> /BackgroundInformation/simData/
> /BackgroundInformation/fieldData/
```
To choose between the data sources, use the Backtesting settings in BacktestingConsole.appsettings.

Backtesting is currently logging data output to influx db and displaying time data series and logs using Grafana.

A setup for creating docker containers for Influx time series logging and a viewing using Grafana is located in 

```text
> Backtesting/DockerGrafanaInflux/docker-compose.yml
```

Navigate to 

```text
> ../Backtesting/DockerGrafanaInflux/ in windows explorer
> type "cmd" to open a command line.
```

 Start the Influx and Grafana containers by running 

 ```text
 "docker-compose up".
 ```

When fully working, this will fetch and start Influx/Grafana docker images and configure data sources in InfluxDb. The only thing needed is to import the two Grafana Dashboards into Grafana (Se Dashboard information below).

NB! Important. Editors can mess up line endings in .sh files that needs to have unix format. Use sublime text - view - line endings - Unix to fix.
Typical error message is when docker is starting up, and an error saying something like "Directorycute not found".

NB! If any of the docker containers have been updated, use docker-compose build to rebuild the containers.

## Info on appsettings / ekd log

Check Backtesting/Appsettings, that serilog actually writes to ekdlog.

## InfluxDB

Databases used by C# will be created by running DtectLoss.BacktestingConsole from Visual Studio.
Database used by Matlab will have to be created manually, this should simply be called "MatlabEKD".

Use the influx command line, see below for further instructions.
CREATE DATABASE MatlabEKD

## Cleaning the database

The InfluxDB is persistent, meaning the database is kept even if the docker container is closed.
To clean the database and remove all old data, use this method.

```bash
<!-- Use docker ps to get the names of the containers. Use the name in the NAMES column. -->
<!-- Open a bash shell in the Influx container (a linux command line) by executing this from the command line: -->
docker exec -it dockergrafanainflux_influxdb_1 /bin/bash
<!-- When the container bash is started, type -->
influx
<!-- to open the command line interface to InfluxDB: "bash-4.4# influx" -->
<!-- From the Influx CLI, use -->
SHOW DATABASES
<!-- to get overview of databases. After running BacktestingConsole there should be an ekd database -->
<!-- Use -->
DROP DATABASE ekd
<!-- to delete the ekd database -->

```

## Configuring Grafana Data Sources

These should have been set up by the Docker setup. Make sure the Url and names are correct.
Verify in Settings/Datasources that source is operational.

InfluxDB
Url: http://influxdb:8086
Database:ekd

InfluxDB-MatlabEKD
Url: http://influxdb:8086
Database: MatlabEKD

EkdLog
Url: http://influxdb:8086
Database:EkdLog

## Grafana Dashboards

There are two Dashboards that can be added:

DefaultGrafanaDashboardTemplate.json
MatlabEKDDashboard.json

NB! Remember to export the dashboards and save to the template files if any important changes are made to the dashboards.

![](images/add-dashboard-2019-10-29-13-44-09.png)

Please ensure that the data sources has been added first.

## View results in Grafana

Use web browser to localhost:3000

Sample of how the Dashboard works:
![](images/main-dashboard-2019-10-29-13-42-13.png)

## Comparing Backtest results to previous results

An Excel document with worksheets for previous results are located in <BackgroundInformation/DtectLoss-master/backtestResultComparision.xlsx>

When a scenario is run, the evaluation results are stored as .csv files in <EvaluationResults>. These csv files can be opened in Excel and added as
sheets in the aggregated comparision document.

While this is helpful, it is difficult to compare. To facilitate better comparision, there is a separate project running as a console that aggregates
all the worksheets in the comparision document and outputs the results to InfluxDB with a corresponding Grafana dashboard.

By running this application, InfluxDB will be filled with the results from each worksheet, tagged with the run date/time. The results can then be
viewed in Grafana for comparision.
