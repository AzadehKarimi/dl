# Testing

This document focus on unit and integration tests. The integration tests also run a complete simulation backtests with assertions. See backtesting for more info.

## Usage

Use a testrunner like Visual Studio test or Resharper Unit Testing. Dotnet CLI can also be used to run tests.

The tests suite contains pure unit tests and integration tests. The integration tests only read information from files without mutating data and are therefore safe to run in development and in a CI/CD pipeline.

### With Resharper

- Run all tests with Ctrl+U+L
- Run the current focused unit test with Ctrl+U+R
- Debug the current focused unit test with Ctrl+U+D

### Using Azure Devops pipeline

- The unit tests are running as a required step after building. Results can be viewed in Azure Devops.
