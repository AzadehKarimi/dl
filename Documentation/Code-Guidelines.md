# Code Consistency & Guidelines

## Global code settings

### Setup with Visual Studio

## Recommended editors

- Visual Studio 2019. Alternative: VSCode, Jetbrains Rider.
- Visual Studio Code (for markdown documentation and viewing files)

## Visual Studio specifics

### Recommended extensions

Resharper is recommended as this will advice a set of practices and ease development.

### Performance

Bad performance? Try turning off autorecovery.

## Coding guidelines

- Try to use iterators and Linq when possible and avoid plain for loops
- Write pure functions and avoid side effects where possible
- Avoid using plain strings - use constants and enums
