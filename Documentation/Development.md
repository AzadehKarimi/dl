# Development

## Technology

This project is based on C#8 and Dotnet core 3.1. The source has been converted from Matlab.

Matlab souce and background information

- BackgroundInformation (All files used as basis for conversion to C#)

## Solutions and projects (C#)

DtectLoss.sln (Realtime and backtesting)

- DtectLoss.Backtesting (Reading data and performing simulation)
- DtectLoss.Backtesting.Models (Common models for backtesting)
- DtectLoss.Backtesting.ResultAggregator (Aggregates backtesting result)
- DtectLoss.Backtesting.Tests (Backtesting tests)
- DtectLoss.BacktestingConsole (Running backtest simulation)
- DtectLoss.Common (Common types and helper functions)
- DtectLoss.FileHandling (File handling used in backtesting)
- DtectLoss.IntegrationTests (Integration test of realtime)
- DtectLoss.Realtime.Tests (Realtime tests)
- DtectLoss.Realtime (Realtime calculations)

## Developer machine requirements

- Windows 10. Will also work with Windows 7, but use of Docker require extra tooling. It should be possible to run this on Linux due to use of .NET Core, but it has not been verified.
- .Net Core SDK 3.1
- Visual Studio 2019 and Visual Studio Code. Jetbrains Rider should also work.
- Matlab if you need to run/verify/debug Matlab code agains C#

## Tools used for documentation and architecture

Draw.io (also available as download) are used for process and architecture diagrams.
