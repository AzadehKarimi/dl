﻿using System;
using DtectLoss.Common;
using DtectLoss.FileHandling;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace DtectLoss.Backtesting.ResultAggregator
{
    public static class ServiceConfiguration
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging(configure => configure.AddSerilog()).AddSingleton<Program>();
            services.AddScoped<IExcelReader, ExcelReader>();
            services.AddScoped<IBacktestResultsLogging, BacktestResultsLogging>();
            services.AddScoped<ResultAggregator, ResultAggregator>();
        }
    }
}
