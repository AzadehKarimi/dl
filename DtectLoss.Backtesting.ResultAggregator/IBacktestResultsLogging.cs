﻿using System;
using DtectLoss.Backtesting.Models;

namespace DtectLoss.Backtesting.ResultAggregator
{
    public interface IBacktestResultsLogging
    {
        void Initialize(string dbName, string url, string userName, string pwd);
        void LogResult(string testRun, DateTime timeStamp, BacktestScenarioRunnerResult backtestResult);
    }
}