﻿using System;
using System.IO;
using System.Linq;
using DtectLoss.FileHandling;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;

namespace DtectLoss.Backtesting.ResultAggregator
{
    class Program
    {
        static void Main(string[] args)
        {
            IConfiguration appSettings = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false, true)
                .Build();
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(appSettings)
                .CreateLogger();
            var serviceCollection = new ServiceCollection();
            ServiceConfiguration.ConfigureServices(serviceCollection);
            var serviceProvider = serviceCollection.BuildServiceProvider();
            var aggregator = serviceProvider.GetService<ResultAggregator>();
            aggregator.AggregateAndLog();
        }
    }
}
