﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using DtectLoss.Common;
using DtectLoss.FileHandling;
using Microsoft.Extensions.Logging;

namespace DtectLoss.Backtesting.ResultAggregator
{
    public class ResultAggregator
    {
        private readonly ILogger<ResultAggregator> _logger;
        private readonly IExcelReader _excelReader;
        private readonly IBacktestResultsLogging _backtestResultsLogging;

        public ResultAggregator(ILogger<ResultAggregator> logger, IExcelReader excelReader, IBacktestResultsLogging backtestResultsLogging)
        {
            _logger = logger;
            _excelReader = excelReader;
            _backtestResultsLogging = backtestResultsLogging;
        }

        public void AggregateAndLog()
        {
            _logger.LogInformation("Serilog logging started on console startup.");
            var backtestEvaluationFile = "BackgroundInformation\\DtectLoss-master\\backtestResultComparison.xlsx";
            var projectDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent?.Parent?.Parent?.FullName;
            var filePath = Path.Combine(projectDirectory ?? throw new InvalidOperationException("Could not read project directory"), backtestEvaluationFile);
            var results = _excelReader.AggregateBacktestResults(filePath);

            //TODO: Move these settings to appsettings
            _backtestResultsLogging.Initialize("ekd", "http://localhost:8086/", "admin","admin");
            var startTime = DateTime.Parse("01/01/2020  00:00:00", new CultureInfo("nb-NO", false));
            var testRunDateString = DateTime.Now.ToString(CultureInfo.CurrentCulture).RenameToValidString();
            var testRun = $"BacktestEvaluation_{testRunDateString}";
            foreach (var result in results.Select((v,i) => new {i, v}))
            {
                _logger.LogInformation($"Logging to influxDb - {testRun}");
                var timeStamp = startTime.AddMinutes(result.i);
                _backtestResultsLogging.LogResult(testRun, timeStamp, result.v);
            }
        }
    }
}