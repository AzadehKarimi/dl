﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdysTech.InfluxDB.Client.Net;
using DtectLoss.Backtesting.Models;
using DtectLoss.Common;
using DtectLoss.Common.Enums;
using Microsoft.Extensions.Logging;

namespace DtectLoss.Backtesting.ResultAggregator
{
    public class BacktestResultsLogging : IBacktestResultsLogging
    {
        private readonly ILogger<BacktestResultsLogging> _logger;
        private InfluxDBClient? _client;
        private string _dbName = string.Empty;

        public BacktestResultsLogging(ILogger<BacktestResultsLogging> logger)
        {
            _logger = logger;
        }

        public async void Initialize(string dbName, string url, string userName, string pwd)
        {
            _dbName = dbName;
            _client = new InfluxDBClient(url, userName, pwd);
            await _client.CreateDatabaseAsync(dbName);
            _logger.LogInformation("Initialized influx backtest results logger");
        }

        public async void LogResult(string testRun, DateTime timeStamp, BacktestScenarioRunnerResult backtestResult)
        {
            var points = new List<InfluxDatapoint<InfluxValueField>>();
            points.Add(CreateTotalsDataPoint(testRun, timeStamp, backtestResult));
            points.AddRange(CreateRigDataPoints(testRun, timeStamp, backtestResult));
            await _client!.PostPointsAsync(_dbName, points);
        }

        private IEnumerable<InfluxDatapoint<InfluxValueField>> CreateRigDataPoints(string testRun, in DateTime timeStamp, BacktestScenarioRunnerResult backtestResult)
        {
            var time = timeStamp;
            var points =
                backtestResult.BacktestEvaluateOutput
                    .Select(x =>
                    CreateBacktestEvaluateRigPoint(testRun, time, x));
            return points;
        }

        private InfluxDatapoint<InfluxValueField> CreateBacktestEvaluateRigPoint(string testRun, in DateTime timeStamp, BacktestEvaluateOutput rig)
        {
            var valMixed = new InfluxDatapoint<InfluxValueField>();
            valMixed.UtcTimestamp = timeStamp;
            valMixed.Tags.Add("TestDate", timeStamp.ToShortDateString());
            valMixed.Tags.Add("TestTime", timeStamp.ToShortTimeString());
            valMixed.Tags.Add("EvalRig", rig.RigName);
            valMixed.Tags.Add("BacktestResultsRun", testRun);

            valMixed.Fields.Add($"Rig.Kpi.FAR", new InfluxValueField(rig.FalseAlamsRate));
            valMixed.Fields.Add($"Rig.Kpi.KPI", new InfluxValueField(rig.KeyPerformanceIndex));
            valMixed.Fields.Add($"Rig.Kpi.TDs", new InfluxValueField(rig.TimeToDetection));
            valMixed.Fields.Add($"Rig.Kpi.VDm3", new InfluxValueField(rig.AccumulatedVolumeAtDetection));
            valMixed.Fields.Add($"Rig.Count.CorrectAlarms", new InfluxValueField(rig.AlgNoOfCorrectAlams));
            valMixed.Fields.Add($"Rig.Count.TrueAlarms", new InfluxValueField(rig.TrueNoOfAlams));
            valMixed.Fields.Add($"Rig.Time.AlgUptime", new InfluxValueField(rig.AlgUpTime));
            valMixed.Fields.Add($"Rig.Time.SimDuration", new InfluxValueField(rig.DurationInHours));
            valMixed.Fields.Add($"Rig.Count.LateAlarms", new InfluxValueField(rig.AlgLateAlams));
            
            //valMixed.Fields.Add($"Rig.{rig.RigName}.Kpi.FAR", new InfluxValueField(rig.FalseAlamsRate));
            //valMixed.Fields.Add($"Rig.{rig.RigName}.Kpi.KPI", new InfluxValueField(rig.KeyPerformanceIndex));
            //valMixed.Fields.Add($"Rig.{rig.RigName}.Kpi.TDs", new InfluxValueField(rig.TimeToDetection));
            //valMixed.Fields.Add($"Rig.{rig.RigName}.Kpi.VDm3", new InfluxValueField(rig.AccumulatedVolumeAtDetection));
            //valMixed.Fields.Add($"Rig.{rig.RigName}.Count.CorrectAlarms", new InfluxValueField(rig.AlgNoOfCorrectAlams));
            //valMixed.Fields.Add($"Rig.{rig.RigName}.Count.TrueAlarms", new InfluxValueField(rig.TrueNoOfAlams));
            //valMixed.Fields.Add($"Rig.{rig.RigName}.Time.AlgUptime", new InfluxValueField(rig.AlgUpTime));
            //valMixed.Fields.Add($"Rig.{rig.RigName}.Time.SimDuration", new InfluxValueField(rig.DurationInHours));
            //valMixed.Fields.Add($"Rig.{rig.RigName}.Count.LateAlarms", new InfluxValueField(rig.AlgLateAlams));

            valMixed.MeasurementName = "BacktestResults.RigTimeSeriesEvaluation";
            valMixed.Precision = TimePrecision.Seconds;
            return valMixed;
        }

        private InfluxDatapoint<InfluxValueField> CreateTotalsDataPoint(string testRun, in DateTime timeStamp, BacktestScenarioRunnerResult backtestResult)
        {
            var valMixed = new InfluxDatapoint<InfluxValueField>();
            valMixed.UtcTimestamp = timeStamp;
            valMixed.Tags.Add("TestDate", timeStamp.ToShortDateString());
            valMixed.Tags.Add("TestTime", timeStamp.ToShortTimeString());
            valMixed.Tags.Add("BacktestResultsRun", testRun);

            valMixed.Fields.Add("Count.DataSets", new InfluxValueField((int)backtestResult.TotalNumberOfDatasets));
            valMixed.Fields.Add("Time.SimulationTimeHours", new InfluxValueField(backtestResult.TotalSimulationTime));
            valMixed.Fields.Add("Time.UptimeHours", new InfluxValueField(backtestResult.TotalUptimeHours));
            valMixed.Fields.Add("Time.UptimePercent", new InfluxValueField(backtestResult.TotalUptimePercent));
            valMixed.Fields.Add("Kpi.Mean", new InfluxValueField(backtestResult.MeanKpi));
            valMixed.Fields.Add("Kpi.FAR", new InfluxValueField(backtestResult.TotalFar));
            valMixed.Fields.Add("Kpi.PD", new InfluxValueField(backtestResult.TotalPd));

            foreach (var (alarmType, alarmTypeCount)  in backtestResult.TotalAlarmTypeCount)
            {
                var alarmTypeName = Enum.GetName(typeof(AlarmType), alarmType);
                valMixed.Fields.Add($"AlarmTypeCount.{alarmTypeName}", new InfluxValueField(alarmTypeCount));
            }
            valMixed.MeasurementName = "BacktestResults.Totals";
            valMixed.Precision = TimePrecision.Seconds;
            return valMixed;
        }
    }
}