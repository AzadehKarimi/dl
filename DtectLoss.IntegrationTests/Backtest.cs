using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using DtectLoss.Backtesting;
using DtectLoss.Common;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Serilog;

namespace DtectLoss.IntegrationTests
{
    
    [TestClass, TestCategory("Backtest")]
    public class Backtest
    {
        private ILogger<Backtest> _logger;
        private ServiceProvider _serviceProvider;
        private string _testRunDateString;

        [TestInitialize]
        public void Initialize()
        {
            IConfiguration appSettings = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false, true)
                .Build();
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(appSettings)
                .CreateLogger();
            _testRunDateString = DateTime.Now.ToString(CultureInfo.CurrentCulture).RenameToValidString();

            var serviceCollection = new ServiceCollection();
            var backTestingConfiguration = appSettings.GetSection("Backtesting");
            ServiceConfiguration.ConfigureServices(serviceCollection, backTestingConfiguration);
            using var logContext = Serilog.Context.LogContext.PushProperty("LoggingTestrun", _testRunDateString);
            _serviceProvider = serviceCollection.BuildServiceProvider();
            _logger = _serviceProvider.GetService<ILogger<Backtest>>();
            _logger.LogInformation("Integration test Serilog logging started on test startup.");
        }



        [TestMethod]
        public void RunSimDatasetBacktest()
        {
            _logger.LogInformation("Reading configuration");
            var bm = _serviceProvider.GetService<IBacktestScenarioManager>();
            var toc = Stopwatch.StartNew();
            bm.Initialize(_testRunDateString);
            var result = bm.PerformBacktestingScenarios();
            var backtestEvaluateOutput = result.BacktestEvaluateOutput.FirstOrDefault();

            toc.Stop();
            _logger.LogInformation($"Time to do Main Loop: {toc.Elapsed.Minutes} minutes, {toc.Elapsed.Seconds} seconds.");
            Assert.AreEqual(0.0, result.MeanKpi);
            Assert.AreEqual(0.0, result.TotalFar);
            Assert.AreEqual(1.0, result.TotalPd);
            Assert.AreEqual(6, backtestEvaluateOutput.AlgNoOfCorrectAlams);
            Assert.AreEqual(0, backtestEvaluateOutput.AlgNoOfFalseAlams);
            Assert.AreEqual(6, backtestEvaluateOutput.TrueNoOfAlams);
            Assert.AreEqual(6, backtestEvaluateOutput.AlgNoOfWarnings);
            Assert.AreEqual(6, backtestEvaluateOutput.TrueNoOfWarnings);
            Assert.AreEqual(14.998333333292976, backtestEvaluateOutput.DurationInHours);
            Assert.AreEqual(0, backtestEvaluateOutput.AlgLateAlams);
            Assert.AreEqual(331.83333221822977, backtestEvaluateOutput.TimeToDetection);
            Assert.AreEqual(0.47166666666666668, backtestEvaluateOutput.AccumulatedVolumeAtDetection);
            Assert.AreEqual(407.4093523519439, result.TotalAccumulatedVolIo);
        }
    }
}
