//To run - alternative 1 - in command line run: dotnet fsi FieldDtaSortedByNum.fsx
//Alternative 2: In VSCode with the Ionide F# plugin: Select all text with Ctrl-A and Press Alt-Enter to send to interactive

open System.IO
open System.Text.RegularExpressions

let path = @"C:\source\repos\DtectLoss\BackgroundInformation\DtectLoss-master\fieldData"
let files = Directory.GetFiles(path, "*.zip")

files
|> Array.map (Path.GetFileName)
|> Array.sortBy (fun x -> int (Regex.Match(x, @"(\d+)(?!.*\d)").Value)) // Regex to find the last number in filename
|> Array.iter (printfn "%s")