﻿using DtectLossRealtime.Enums;
using DtectLossRealtime.Models;

namespace DtectLossRealtime.EKDAlgoritm
{
    public interface IEKDAlgoritm
    {
        void Initialize(Voting votingParams, double[] pargIn);
        AlgResult StepIt(WashedData washedData, AlgData algData, StateMachine sysState, double timeCurrentD, double dTs);
        AlgResult ReturnResultStruct();
        AlarmState AlarmState { get; }
        bool ResetAlarm { get; }
        bool RecalibrateFlowOutNow { get; }
        void SoftReset();

    }
}
