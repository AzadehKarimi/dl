﻿using System;
using System.Globalization;
using Accord.Math;
using DtectLoss.Common;
using DtectLoss.Common.Enums;
using DtectLossRealtime.Calculations;
using DtectLossRealtime.Enums;
using DtectLossRealtime.Helpers;
using DtectLossRealtime.Models;
using static System.Double;
using static System.Math;
using static DtectLoss.Common.CommonConstants;


namespace DtectLossRealtime.EKDAlgoritm
{
    /// <summary>
    /// generate alarms based on expected flow out, expected pump pressure and
    /// expected gain/loss volume.
    /// Voting: alarm if 2/3 are above thresholds
    /// </summary>
    public class VotingAlg : IEKDAlgoritm
    {
        public double VolIOflowM3 { get; set; }
        public double VolIOpresM3 { get; set; }
        public double VolIOpitM3 { get; set; }
        public AlarmType AlarmType { get; set; }

        public double FlowIOflowLpm { get; private set; }
        public double FlowIOpresLpm { get; private set; }
        public double FlowIOpitLpm { get; private set; }
        public double PresIoBar { get; private set; }
        public double TimeHighFlowErrorS { get; private set; }
        public double TimeHighVolErrorS { get; private set; }
        public double VolIoM3 { get; private set; }
        public Voting Parameters { get; private set; }
        public AlarmState AlarmState { get; private set; }
        public bool ResetAlarm { get; private set; }
        public bool RecalibrateFlowOutNow { get; private set; }

        private double _flowIOtripLpm;
        private double _volIOtripM3;
        private double _timeRecalibrateFlowOutS;

        private double _timeRecalibratePresSpS;
        private string _alarmStatus = string.Empty;
        private double _timeCurrentD;
        private double _timeLastD;
        private double _timeAlarmActiveS;
        private double _timeSinceLastAlarmS;
        private double _timeSinceValidS;
        private double _dtS;
        private double _flowIOinjLpm;  //To remove slowly varying bias on flowmeter
        private FilterDerivative? _filtPitData;
        private FilterDerivative? _filtTripData;
        private const double LpmConversion = 60000.0;

        private const double TimeStepMaxS = 60;
        private const double FlowPumpsOnMinLpm = 100;

        private const bool DispDebug = false;
        private const double VolGainDetectorThresholdM3 = 0.35; // ensure that this limit is less than the volumeLimit_m3 in GAIN_DETECTOR_OBJ
        private const double LpmToM3Ps = 1 / 60000.0;
        //Pragma to disable unused warning for these variables
        #pragma warning disable 414
        private bool _recalibratePresSpNow;
        private bool _runSoftresetnow;
        #pragma warning restore 414

        public void Initialize(Voting votingParams, double[] pargIn)
        {
            FlowIOflowLpm = MissingValueReplacement;
            FlowIOpresLpm = 0;
            FlowIOpitLpm = 0;
            _flowIOtripLpm = 0;
            PresIoBar = 0;
            VolIOflowM3 = 0;
            VolIOpresM3 = 0;
            VolIOpitM3 = 0;
            _volIOtripM3 = 0;
            VolIoM3 = 0;
            AlarmState = AlarmState.Disabled;
            AlarmType = AlarmType.Normal;
            _timeCurrentD = 0;
            _timeLastD = 0;
            _dtS = 0;
            this.Parameters = votingParams;
            _flowIOinjLpm = 0;
            ResetAlarm = false;
            RecalibrateFlowOutNow = false;
            _timeRecalibrateFlowOutS = 0;
            _recalibratePresSpNow = false;
            _timeRecalibratePresSpS = 0;
            _timeAlarmActiveS = 0;
            _timeSinceLastAlarmS = 0;
            _timeSinceValidS = MissingValueReplacement;
            _runSoftresetnow = false;        // TODO: Seems to be not in use
            TimeHighFlowErrorS = 0;
            TimeHighVolErrorS = 0;
            _filtPitData = new FilterDerivative(Parameters.TimeFiltPitVolS);
            _filtTripData = new FilterDerivative(Parameters.TimeFiltPitVolS);
        }

        public void SoftReset() 
        {
            FlowIOflowLpm = MissingValueReplacement;
            FlowIOpresLpm = 0;
            FlowIOpitLpm = 0;
            _flowIOtripLpm = 0;
            PresIoBar = 0;
            VolIOflowM3 = 0;
            VolIOpresM3 = 0;
            VolIOpitM3 = 0;
            _volIOtripM3 = 0;
            VolIoM3 = 0;
            AlarmState = AlarmState.Disabled;
            AlarmType = AlarmType.Normal;
            _timeCurrentD = 0;
            _timeLastD = 0;
            _dtS = 0;
            _flowIOinjLpm = 0;
            ResetAlarm = false;
            RecalibrateFlowOutNow = false;
            _timeRecalibrateFlowOutS = 0;
            _recalibratePresSpNow = false;
            _timeRecalibratePresSpS = 0;
            _timeAlarmActiveS = 0;
            _timeSinceLastAlarmS = 0;
            TimeHighFlowErrorS = 0;
            TimeHighVolErrorS = 0;
            _runSoftresetnow = true;
        }

        public double SetFlowOut(WashedData washedData)
        {

            double flowOutLpm;
            if (!washedData.Values[V.FlowOutFilteredLpm].Equals(MissingValueReplacement) && !IsNaN(washedData.Values[V.FlowOutFilteredLpm]))
                flowOutLpm = washedData.Values[V.FlowOutFilteredLpm];
            else if (!washedData.Values[V.FlowOutLpm].Equals(MissingValueReplacement))
                flowOutLpm = washedData.Values[V.FlowOutLpm];
            else
                flowOutLpm = MissingValueReplacement;
            return flowOutLpm;
        }

        public void CalcIoFlow(AlgData algData, WashedData washedData, StateMachine sysState)
        {
            var flowOutLpm = SetFlowOut(washedData);

            // check if algData.flowOutExpected_lpm is empty to avoid error
            if (algData.FlowOutExpectedLpm != null && !flowOutLpm.Equals(MissingValueReplacement) && !algData.IsDownlinking &&
                algData.IsCalibratedExpFlowOut && algData.IsCalibratedMeasFlowOut && !washedData.Values[V.FlowInLpm].Equals(MissingValueReplacement))
                FlowIOflowLpm = flowOutLpm - algData.FlowOutExpectedLpm.Value;
            else
                FlowIOflowLpm = 0;

            var flowErrAccLpm = FlowIOflowLpm + _flowIOinjLpm;

            if (Abs(flowErrAccLpm) > Parameters.FlowDeadbandLpm && !washedData.Values[V.FlowInLpm].Equals(MissingValueReplacement)) // Accumulate flow error to volume
                VolIOflowM3 += _dtS * flowErrAccLpm * LpmToM3Ps;  // vIo[m3] = dt[s] * qIo[lpm]

            if (_timeRecalibrateFlowOutS > 60 || algData.ResetVolIOnow || !sysState.IsOnBottom)
            {
                FlowIOflowLpm = 0;
                VolIOflowM3 = 0;
            }
        }

        public void CalcIoPres(AlgData algData, WashedData washedData, StateMachine sysState)
        {
            if (!washedData.Values[V.PresSpBar].Equals(MissingValueReplacement) && !algData.IsDownlinking &&
                algData.IsCalibratedExpPresSp && !washedData.Values[V.FlowInLpm].Equals(MissingValueReplacement))
            {
                if (washedData.Values[V.PresSpBar] > 5) // Only use presSP when float is assumed open
                    PresIoBar = washedData.Values[V.PresSpBar] - algData.PresSpExpectedBar;
                else
                    PresIoBar = 0;

                FlowIOpresLpm = PresIoBar * Parameters.ScalePresToFlow;
            }
            else
                FlowIOpresLpm = 0;

            if (Abs(FlowIOpresLpm) > Parameters.FlowDeadbandLpm && algData.IsCalibratedExpPresSp &&
                !washedData.Values[V.FlowInLpm].Equals(MissingValueReplacement)) // Accumulate flow error to volume
            {
                if (!algData.IsDownlinking)
                    VolIOpresM3 += _dtS * FlowIOpresLpm * LpmToM3Ps;   // vIo[m3] = dt[s] * qIo[lpm]
                else
                    VolIOpresM3 = 0;

            }

            if (_timeRecalibratePresSpS > 60 || algData.ResetVolIOnow || !sysState.IsOnBottom)
            {
                FlowIOpresLpm = 0;
                VolIOpresM3 = 0;
            }
        }

        public void CalcIoPit(AlgData algData, WashedData washedData, StateMachine sysState)
        {
            if (!washedData.Values[V.VolGainLossM3].Equals(MissingValueReplacement) && algData.IsCalibratedExpPitVol &&
                !washedData.Values[V.FlowInLpm].Equals(MissingValueReplacement))
            {
                VolIOpitM3 = washedData.Values[V.VolGainLossM3] - algData.VolGainLossExpectedM3;
                var flowIOpitM3Ps = _filtPitData!.Smoothderivative2ndorder(VolIOpitM3, _timeCurrentD);
                FlowIOpitLpm = flowIOpitM3Ps / LpmToM3Ps;
            }

            else if (!algData.IsCalibratedExpPitVol)
                VolIOpitM3 = 0;

            if (!sysState.IsOnBottom)
            {
                FlowIOpitLpm = 0;
                VolIOpitM3 = 0;
            }
        }
        public void CalcIoTripTank(AlgData algData, StateMachine sysState)
        {
            if (!sysState.IsOnBottom)
            {
                _volIOtripM3 = algData.VolTripTankCumErrorM3;
                double? flowIOtripM3Ps = _filtTripData!.Smoothderivative2ndorder(_volIOtripM3, _timeCurrentD);
                _flowIOtripLpm = flowIOtripM3Ps.Value / LpmToM3Ps;
            }
            else
            {
                _volIOtripM3 = 0;
                _flowIOtripLpm = 0;
            }
        }

        public void SetTimeHighFlowError(AlgData algData, WashedData washedData)
        {
            if (!FlowIOflowLpm.Equals(MissingValueReplacement) && Abs(FlowIOflowLpm) > Max(Parameters.FlowIoAlarmLpm, washedData.Values[V.FlowInLpm] * Parameters.FlowIoAlarmPc / 100.0)
                && algData.IsCalibratedExpFlowOut && algData.IsCalibratedMeasFlowOut && !washedData.Values[V.FlowInLpm].Equals(MissingValueReplacement))
                TimeHighFlowErrorS += _dtS;
            else
                TimeHighFlowErrorS = 0;
        }

        public void SetTimeHighVolError(AlgData algData, WashedData washedData)
        {
            double volPitOnlyAlarmLimitM3;
            if (algData.IsRampingUp)
                volPitOnlyAlarmLimitM3 = Parameters.VolIoAlarmPumpStartM3;  // Less accurate pitvol model when ramping up pump
            else
                volPitOnlyAlarmLimitM3 = Parameters.VolIoAlarmM3;

            if (Abs(VolIOpitM3) > volPitOnlyAlarmLimitM3 && algData.IsCalibratedExpPitVol && !washedData.Values[V.FlowInLpm].Equals(MissingValueReplacement))
                TimeHighVolErrorS += _dtS;
            else
                TimeHighVolErrorS = 0;
        }

        public void SetInjection()
        {
            //TODO - Check how to better handle 0 values in division
            double flowerraccLpm = FlowIOflowLpm + _flowIOinjLpm;
            if (!Parameters.TimeConstInjFioS.IsEqual(0.0))
                _flowIOinjLpm -= _dtS * flowerraccLpm / Parameters.TimeConstInjFioS;   // injection - to make DQ->0
            else
                _flowIOinjLpm = 0.0;

            _flowIOinjLpm = Min(Parameters.FlowMaxInjectionLpm, Max(-Parameters.FlowMaxInjectionLpm, _flowIOinjLpm));

            if (!Parameters.TimeConstInjViofS.IsEqual(0.0))
                VolIOflowM3 -= _dtS * (Sign(VolIOflowM3) * Min(Parameters.FlowMaxInjectionLpm * LpmToM3Ps, Abs(VolIOflowM3 / Parameters.TimeConstInjViofS)));// injection - to make DV->0
            else
                VolIOflowM3 -= _dtS * (Sign(VolIOflowM3) * Min(Parameters.FlowMaxInjectionLpm * LpmToM3Ps, Abs(VolIOflowM3 / 1)));// injection - to make DV->0

            if (!Parameters.TimeConstInjViopS.IsEqual(0.0))
                VolIOpresM3 -= _dtS * (Sign(VolIOpresM3) * Min(Parameters.FlowMaxInjectionLpm * LpmToM3Ps, Abs(VolIOpresM3 / Parameters.TimeConstInjViopS)));// injection - to make DV->0
            else
                VolIOpresM3 -= _dtS * (Sign(VolIOpresM3) * Min(Parameters.FlowMaxInjectionLpm * LpmToM3Ps, Abs(VolIOpresM3 / 1.0)));// injection - to make DV->0
        }


        public void UpdateTime(double timeCurrentD, double dtS)
        {
            _dtS = dtS;
            _timeCurrentD = timeCurrentD;
        }

        public AlgResult StepIt(WashedData washedData, AlgData algData, StateMachine sysState, double timeCurrentD, double dTs)
        {
            UpdateTime(timeCurrentD, dTs);

            if (_dtS > TimeStepMaxS) // reset
            {
                _alarmStatus = $"Reset, time step = {_dtS}, sec";
                SoftReset(); // initialize(obj, obj.params);
                _timeLastD = _timeCurrentD;
                return ReturnResultStruct();
            }
            _timeLastD = _timeCurrentD;

            //debug
            var timeDebug = DateTime.FromOADate(_timeCurrentD).ToUniversalTime();
            var timeToDebugCondition = DateTime.Parse("30/10/2015 04:08:25", new CultureInfo("nb-NO", false)).ToUniversalTime();

            // TODO Figure out why RecalibrateNow is Set

            CalcIoFlow(algData, washedData, sysState);
            CalcIoPres(algData, washedData, sysState);
            CalcIoPit(algData, washedData, sysState);
            CalcIoTripTank(algData, sysState);
            SetTimeHighFlowError(algData, washedData);
            SetTimeHighVolError(algData, washedData);
            SetInjection();
            SetAlarmState(algData, washedData, sysState);
            return ReturnResultStruct();
        }

        public void CheckResetAlarm()
        {
            if (_timeAlarmActiveS > Parameters.TimeResetAlarmS)
            {
                AlarmState = AlarmState.Normal;
                AlarmType = AlarmType.Normal;
                _timeAlarmActiveS = 0;
                VolIOflowM3 = 0;
                VolIOpresM3 = 0;
                _volIOtripM3 = 0;
                VolIoM3 = 0;
                ResetAlarm = true;
                RecalibrateFlowOutNow = true; // XXX Test

            }
            else
                ResetAlarm = false;
        }

        public void SetAlarmStateTripping()
        {
            var highGainTrip = +_volIOtripM3 > Parameters.VolumeThresholdTrippingAlarmM3;
            var highLossTrip = -_volIOtripM3 > Parameters.VolumeThresholdTrippingAlarmM3;
            VolIoM3 = _volIOtripM3;
            VolIOpitM3 = 0;
            VolIOpresM3 = 0;
            VolIOflowM3 = 0;
            if (AlarmState == AlarmState.Alarm)  // Hold alarm for 5 minutes
                AlarmState = AlarmState.Alarm;
            else if (_timeSinceLastAlarmS < 300) // Inhibit alarm first 5 minutes after reset
            {
                AlarmState = AlarmState.Normal;
                AlarmType = AlarmType.Normal;
            }
            else if (highGainTrip)
            {
                AlarmState = AlarmState.Alarm;   // Influx Trip
                AlarmType = AlarmType.TripOnlyInfluxAlarm;
            }
            else if (highLossTrip)
            {
                AlarmState = AlarmState.Alarm; // Loss Trip
                AlarmType = AlarmType.TripOnlyLossAlarm;
            }
            else if (AlarmState == AlarmState.Alarm) // Old alarm
                AlarmState = AlarmState.Alarm; // Do nothing
            else if (_volIOtripM3 > Parameters.VolumeThresholdWarningM3 &&
                     _volIOtripM3 + _flowIOtripLpm / 60000.0 * Parameters.TimeWarningS >
                     Parameters.VolumeThresholdAlarmM3)
            {
                AlarmState = AlarmState.Warning; // Influx
                AlarmType = AlarmType.InfluxWarning;
            }
            else if (_volIOtripM3 < -Parameters.VolumeThresholdWarningM3 &&
                     _volIOtripM3 + _flowIOtripLpm / 60000.0 * Parameters.TimeWarningS <
                     -Parameters.VolumeThresholdAlarmM3)
            {
                AlarmState = AlarmState.Warning; // Loss
                AlarmType = AlarmType.LossWarning;
            }
            else
            {
                AlarmState = AlarmState.Normal; // normal
                AlarmType = AlarmType.Normal;
                VolIoM3 = 0;
            }
        }

        public AlarmConditions SetAlarmConditions(WashedData washedData, AlgData algData, BadCalibration badData)
        {
            var alarmConditions = new AlarmConditions();
            alarmConditions.HighGainVol = !badData.Vol && +VolIOpitM3 > Parameters.VolumeThresholdAlarmM3 && algData.TimeSinceResetGlS > 300; //Inhbit 5 minutes after volume has been reset
            alarmConditions.HighGainFlow = !badData.Flow && +VolIOflowM3 > Parameters.VolumeThresholdAlarmM3;
            alarmConditions.HighGainPres = !badData.Pres && +VolIOpresM3 > Parameters.VolumeThresholdAlarmM3;
            alarmConditions.HighLossVol = !badData.Vol && -VolIOpitM3 > Parameters.VolumeThresholdAlarmM3 && algData.TimeSinceResetGlS > 300; //Inhbit 5 minutes after volume has been reset
            alarmConditions.HighLossFlow = !badData.Flow && -VolIOflowM3 > Parameters.VolumeThresholdAlarmM3;
            alarmConditions.HighLossPres = !badData.Pres && -VolIOpresM3 > Parameters.VolumeThresholdAlarmM3;
            alarmConditions.Inhibit = algData.NumberPumpStops == 0 || algData.NumberPumpStops == 1 && algData.TimePumpsOnS < 10 * 60
                                                                        || _timeSinceLastAlarmS < Parameters.TimeInhibitAlarmAfterResetS; //Inhibit alarm until 1st pump stop + 10 minutes and 5 minutes after last alarm
            alarmConditions.HoldAlarm = _timeAlarmActiveS < Parameters.TimeResetAlarmS; //Hold alarm 5 minutes or until user has silenced it
            alarmConditions.AllowInfluxAlarm = !algData.IsRampingUp;
            alarmConditions.Startup = AlarmState == AlarmState.Disabled;
            alarmConditions.SlowGain = algData.VolGainDetectorM3 > VolGainDetectorThresholdM3 && !algData.IsRampingUp;
            alarmConditions.HighGainVolOnly = (VolIOpitM3 > 0 && TimeHighVolErrorS > Parameters.TimeVolOnlyAlarmS) && !algData.IsRampingUp && algData.TimeSinceResetGlS > 300;
            alarmConditions.HighLossVolOnly = (-VolIOpitM3 > 0 && TimeHighVolErrorS > Parameters.TimeVolOnlyAlarmS) && algData.TimeSinceResetGlS > 300;
            alarmConditions.HighGainFlowOnly = (FlowIOflowLpm > 0 && TimeHighFlowErrorS > Parameters.TimeFlowOnlyAlarmS
                                                                    && _timeRecalibrateFlowOutS == 0 && washedData.Values[V.FlowInLpm] > FlowPumpsOnMinLpm && !algData.IsRampingUp);
            alarmConditions.HighLossFlowOnly = -FlowIOflowLpm > 0 && TimeHighFlowErrorS > Parameters.TimeFlowOnlyAlarmS
                    && _timeRecalibrateFlowOutS == 0 && washedData.Values[V.FlowInLpm] > FlowPumpsOnMinLpm;

            return alarmConditions;
        }

        public void SetVolIoAlarm()
        {
            VolIoM3 = AlarmType switch
            {
                AlarmType.PitSlowGainAlarm => VolIOpitM3,
                AlarmType.PitOnlyLossAlarm => VolIOpitM3,
                AlarmType.PitOnlyInfluxAlarm => VolIOpitM3,
                AlarmType.FlowOnlyLossAlarm => VolIOflowM3,
                AlarmType.FlowOnlyInfluxAlarm => VolIOflowM3,
                AlarmType.FlowAndPressureLossAlarm => Max(VolIOflowM3, VolIOpresM3),
                AlarmType.FlowAndPressureInfluxAlarm => Min(VolIOflowM3, VolIOpresM3),
                AlarmType.FlowAndPitLossAlarm => Max(VolIOflowM3, VolIOpitM3),
                AlarmType.FlowAndPitInfluxAlarm => Min(VolIOflowM3, VolIOpitM3),
                AlarmType.PitAndPressureLossAlarm => Max(VolIOpitM3, VolIOpresM3),
                AlarmType.PitAndPressureInfluxAlarm => Min(VolIOpitM3, VolIOpresM3),
                _ => VolIoM3
            };
        }


        public void CheckForNewAlarm(AlarmConditions alarmConditions)
        {
            if (alarmConditions.HighGainFlow && alarmConditions.HighGainPres && alarmConditions.AllowInfluxAlarm)
            {
                AlarmState = AlarmState.Alarm; // Influx F + Pr
                AlarmType = AlarmType.FlowAndPressureInfluxAlarm;
            }
            else if (alarmConditions.HighGainFlow && alarmConditions.HighGainVol && alarmConditions.AllowInfluxAlarm)
            {
                AlarmState = AlarmState.Alarm; // Influx F + V
                AlarmType = AlarmType.FlowAndPitInfluxAlarm;
            }
            else if (alarmConditions.HighGainVol && alarmConditions.HighGainPres && alarmConditions.AllowInfluxAlarm)
            {
                AlarmState = AlarmState.Alarm; // Influx V + Pr
                AlarmType = AlarmType.PitAndPressureInfluxAlarm;

            }
            else if (alarmConditions.HighLossFlow && alarmConditions.HighLossPres)
            {
                AlarmState = AlarmState.Alarm; // Loss F + Pr
                AlarmType = AlarmType.FlowAndPressureLossAlarm;
            }
            else if (alarmConditions.HighLossFlow && alarmConditions.HighLossVol)
            {
                AlarmState = AlarmState.Alarm; // Loss F + V
                AlarmType = AlarmType.FlowAndPitLossAlarm;
            }
            else if (alarmConditions.HighLossVol && alarmConditions.HighLossPres)
            {
                AlarmState = AlarmState.Alarm; // Loss V + Pr
                AlarmType = AlarmType.PitAndPressureLossAlarm;
            }
            else if (alarmConditions.SlowGain)
            {
                AlarmState = AlarmState.Alarm; // Influx: pit only alarm
                AlarmType = AlarmType.PitSlowGainAlarm;
            }
            else if (alarmConditions.HighGainVolOnly)
            {
                AlarmState = AlarmState.Alarm; // Influx: pit only alarm
                AlarmType = AlarmType.PitOnlyInfluxAlarm;
            }
            else if (alarmConditions.HighLossVolOnly)
            {
                AlarmState = AlarmState.Alarm; // Loss: pit only alarm
                AlarmType = AlarmType.PitOnlyLossAlarm;
            }
            else if (alarmConditions.HighGainFlowOnly)
            {
                AlarmState = AlarmState.Alarm; // Influx: flow only alarm
                AlarmType = AlarmType.FlowOnlyInfluxAlarm;
            }
            else if (alarmConditions.HighLossFlowOnly)
            {
                AlarmState = AlarmState.Alarm; // Loss: flow only alarm
                AlarmType = AlarmType.FlowOnlyLossAlarm;
            }

            if (AlarmState == AlarmState.Alarm)
                SetVolIoAlarm();
        }

        public WarningConditions SetWarningConditions()
        {

            var warningConditions = new WarningConditions();
            //warning when alarm is expected timeWarning_s ahead
            warningConditions.HighGainVol = +VolIOpitM3 > Parameters.VolumeThresholdWarningM3 &&
                                              VolIOpitM3 + FlowIOpitLpm / LpmConversion * Parameters.TimeWarningS > Parameters.VolumeThresholdAlarmM3;
            warningConditions.HighGainFlow = +VolIOflowM3 > Parameters.VolumeThresholdWarningM3 &&
                                               VolIOflowM3 + FlowIOflowLpm / LpmConversion * Parameters.TimeWarningS > Parameters.VolumeThresholdAlarmM3;
            warningConditions.HighGainPres = +VolIOpresM3 > Parameters.VolumeThresholdWarningM3 &&
                                               VolIOpresM3 + FlowIOpresLpm / LpmConversion * Parameters.TimeWarningS > Parameters.VolumeThresholdAlarmM3;
            warningConditions.HighGainPitOnly = VolIOpitM3 > Parameters.VolumeThresholdWarningM3 &&
                                               VolIOpitM3 + FlowIOpitLpm / LpmConversion * Parameters.TimeWarningS > Parameters.VolIoAlarmM3;
            warningConditions.HighGainFlowOnly = VolIOflowM3 > Parameters.VolumeThresholdWarningM3 &&
                                               VolIOflowM3 + FlowIOflowLpm / LpmConversion * Parameters.TimeWarningS > Parameters.VolIoAlarmM3;

            warningConditions.HighLossVol = -VolIOpitM3 > Parameters.VolumeThresholdWarningM3 &&
                                              -VolIOpitM3 - FlowIOpitLpm / LpmConversion * Parameters.TimeWarningS > Parameters.VolumeThresholdAlarmM3;
            warningConditions.HighLossFlow = -VolIOflowM3 > Parameters.VolumeThresholdWarningM3 &&
                                             -VolIOflowM3 - FlowIOflowLpm / LpmConversion * Parameters.TimeWarningS > Parameters.VolumeThresholdAlarmM3;
            warningConditions.HighLossPres = -VolIOpresM3 > Parameters.VolumeThresholdWarningM3 &&
                                             -VolIOpresM3 - FlowIOpresLpm / LpmConversion * Parameters.TimeWarningS > Parameters.VolumeThresholdAlarmM3;
            warningConditions.HighLossPitOnly = -VolIOpitM3 > Parameters.VolumeThresholdWarningM3 &&
                                                -VolIOpitM3 - FlowIOpitLpm / LpmConversion * Parameters.TimeWarningS > Parameters.VolIoAlarmM3;
            warningConditions.HighLossFlowOnly = -VolIOflowM3 > Parameters.VolumeThresholdWarningM3 &&
                                                -VolIOflowM3 - FlowIOflowLpm / LpmConversion * Parameters.TimeWarningS > Parameters.VolIoAlarmM3;
            return warningConditions;
        }


        public void CheckForWarningOrNormal()
        {
            var warningConditions = SetWarningConditions();

            if (warningConditions.HighGainVol && warningConditions.HighGainPres)
            {
                AlarmState = AlarmState.Warning; // Influx pit & pres
                AlarmType = AlarmType.InfluxWarning;
                VolIoM3 = Min(VolIOpitM3, VolIOpresM3); // TODO: to ask if this is correct..??
            }
            else if (warningConditions.HighGainFlow && warningConditions.HighGainVol)
            {
                AlarmState = AlarmState.Warning; // Influx flow & pit
                AlarmType = AlarmType.InfluxWarning;
                VolIoM3 = Min(VolIOpitM3, VolIOpitM3);
            }
            else if (warningConditions.HighGainFlow && warningConditions.HighGainPres)
            {
                AlarmState = AlarmState.Warning; // Influx flow & pres
                AlarmType = AlarmType.InfluxWarning;
                VolIoM3 = Min(VolIOflowM3, VolIOpresM3);
            }
            else if (warningConditions.HighGainFlowOnly)
            {
                AlarmState = AlarmState.Warning;  // Influx Flow only
                AlarmType = AlarmType.InfluxWarning;
                VolIoM3 = VolIOflowM3;
            }
            else if (warningConditions.HighGainPitOnly)
            {
                AlarmState = AlarmState.Warning; // Influx Pit only
                AlarmType = AlarmType.InfluxWarning;
                VolIoM3 = VolIOpitM3;
            }
            else if (warningConditions.HighLossVol && warningConditions.HighLossPres)
            {
                AlarmState = AlarmState.Warning; // Influx pit & pres
                AlarmType = AlarmType.LossWarning;
                VolIoM3 = Max(VolIOpitM3, VolIOpresM3);
            }
            else if (warningConditions.HighLossFlow && warningConditions.HighLossVol)
            {
                AlarmState = AlarmState.Warning;  // Influx pit & pres
                AlarmType = AlarmType.LossWarning;
                VolIoM3 = Max(VolIOpitM3, VolIOflowM3);
            }
            else if (warningConditions.HighLossFlow && warningConditions.HighLossPres)
            {
                AlarmState = AlarmState.Warning;  // Loss flow & pres
                AlarmType = AlarmType.LossWarning;
                VolIoM3 = Max(VolIOflowM3, VolIOpresM3);
            }
            else if (warningConditions.HighLossFlowOnly)
            {
                AlarmState = AlarmState.Warning;  // Loss Flow only
                AlarmType = AlarmType.LossWarning;
                VolIoM3 = VolIOflowM3;
            }
            else if (warningConditions.HighLossPitOnly)
            {
                AlarmState = AlarmState.Warning;  // Loss pit only
                AlarmType = AlarmType.LossWarning;
                VolIoM3 = VolIOpitM3;
            }
            else
            {
                AlarmState = AlarmState.Normal;  // normal
                AlarmType = AlarmType.Normal;
                var vol = new[] { VolIOflowM3, VolIOpitM3, VolIOpresM3 };
                vol.Sort();
                VolIoM3 = vol[1]; // Use the median
            }
        }

        public void CheckRecalibrateFlowOut(AlarmConditions alarmConditions)
        {
            if (AlarmState != AlarmState.Alarm && ((alarmConditions.HighGainFlow && !alarmConditions.HighGainVol) ||
                (alarmConditions.HighLossFlow && !alarmConditions.HighLossVol)) && TimeHighFlowErrorS.Equals(0))
            {
                //TODO:Use logging here
                if (_timeRecalibrateFlowOutS.Equals(0) && DispDebug)
                    Console.WriteLine("Recalibrate flow out measurement next 60 sec as neither volume nor pressure indicate loss or influx");

                _timeRecalibrateFlowOutS += _dtS;
                if (_timeRecalibrateFlowOutS < Parameters.TimeWaitRecalibratePaddleS) // Wait 1 min in case of real loss
                    RecalibrateFlowOutNow = false;
                else if (_timeRecalibrateFlowOutS < Parameters.TimeWaitRecalibratePaddleS + 60)
                    RecalibrateFlowOutNow = true;
                else
                {
                    _timeRecalibrateFlowOutS = 0;
                    RecalibrateFlowOutNow = false;
                    VolIOflowM3 = 0;
                }
            }
            else
            {
                _timeRecalibrateFlowOutS = 0;
                RecalibrateFlowOutNow = false;
            }
        }

        public void CheckRecalibratePresSp(AlarmConditions alarmConditions)
        {
            if (AlarmState != AlarmState.Alarm && (alarmConditions.HighGainPres || alarmConditions.HighLossPres))
            {
                _timeRecalibratePresSpS += _dtS;
                if (_timeRecalibratePresSpS < Parameters.TimeWaitRecalibratePaddleS) // Wait 1 min in case of real loss
                    _recalibratePresSpNow = false;
                else if (_timeRecalibratePresSpS < Parameters.TimeWaitRecalibratePaddleS + 60)
                    _recalibratePresSpNow = true;
                else
                {
                    // TODO: Use logging here
                    //if (dispDebug)
                    //    Console.WriteLine("Reset volIOpres as neither volume nor flow out indicate loss or influx");

                    _timeRecalibratePresSpS = 0;
                    _recalibratePresSpNow = false;
                    VolIOpresM3 = 0;
                }
            }
            else
            {
                _timeRecalibratePresSpS = 0;
                _recalibratePresSpNow = false;
            }
        }

        public void SetAlarmStateDrilling(WashedData washedData, AlgData algData, BadCalibration badData)
        {
            // 1.Check for alarms
            var alarmConditions = SetAlarmConditions(washedData, algData, badData);
            if (AlarmState == AlarmState.Alarm && alarmConditions.HoldAlarm) // Old alarm
            {
                AlarmState = AlarmState.Alarm;  // Do nothing
                return;
            }
            if (alarmConditions.Startup || alarmConditions.Inhibit)
            {
                AlarmState = AlarmState.Normal;
                AlarmType = AlarmType.Normal;
                return;
            }
            if (AlarmState != AlarmState.Alarm)
                CheckForNewAlarm(alarmConditions);

            //TODO:Check if else conditions in this method, seems off

            // 2.Check for warnings or normal
            if (AlarmState != AlarmState.Alarm)
                CheckForWarningOrNormal();

            CheckRecalibrateFlowOut(alarmConditions);
            CheckRecalibratePresSp(alarmConditions);
        }

        public void UpdateTimers()
        {
            if (AlarmState == AlarmState.Alarm)
            {
                _timeAlarmActiveS += _dtS;
                _timeSinceLastAlarmS = 0;
            }
            else
            {
                _timeSinceLastAlarmS += _dtS;
                _timeAlarmActiveS = 0;
            }
            _runSoftresetnow = false;
            if (_timeSinceValidS.Equals(MissingValueReplacement))
            {
                // First
                if (AlarmState != AlarmState.Disabled || AlarmState == AlarmState.InputError)
                    _timeSinceValidS = 0;
            }
            else if (_timeSinceValidS.Equals(0))
            {
                // Was valid
                if (AlarmState == AlarmState.Disabled || AlarmState == AlarmState.InputError)
                    _timeSinceValidS = _dtS;
            }
            else
            {
                // Was not valid
                if (AlarmState == AlarmState.Disabled || AlarmState == AlarmState.InputError)
                    _timeSinceValidS += _dtS;
                else
                {
                    if (_timeSinceValidS > 6 * 60 * 60)
                        _runSoftresetnow = true;

                    _timeSinceValidS = 0;
                }
            }
        }

        //determine if an alarm has occured
        public void SetAlarmState(AlgData algData, WashedData washedData, StateMachine sysState)
        {
            // Reset alarm to normal after 5 minutes
            CheckResetAlarm();
            // Check if models have been calibrated
            var badCalibration = CheckCalibration(algData, sysState);

            if (badCalibration.Tot)
            {
                AlarmState = AlarmState.InputError;
                AlarmType = AlarmType.Normal;
                VolIoM3 = 0;
            }
            else if (!ResetAlarm)
            {
                if (!sysState.IsOnBottom) // Tripping mode.Currently tripping mode is defined as not on bottom and not bad data
                    SetAlarmStateTripping();
                else if (sysState.IsOnBottom) // true
                    SetAlarmStateDrilling(washedData, algData, badCalibration);
                else
                {
                    AlarmState = AlarmState.Normal;
                    AlarmType = AlarmType.Normal;
                }
            }

            UpdateTimers();
        }

        public AlgResult ReturnResultStruct()
        {
            var result = new AlgResult
            {
                FlowIOflowLpm = FlowIOflowLpm,
                FlowIOpresLpm = FlowIOpresLpm,
                FlowIOpitLpm = FlowIOpitLpm,
                FlowIOtripLpm = _flowIOtripLpm,
                PresIoBar = PresIoBar,
                VolIOflowM3 = VolIOflowM3,
                VolIOpresM3 = VolIOpresM3,
                VolIOpitM3 = VolIOpitM3,
                VolIOtripM3 = _volIOtripM3,
                VolIoM3 = VolIoM3,
                AlarmState = AlarmState,
                AlarmType = AlarmType,
                FlowIOinjLpm = _flowIOinjLpm,
                RecalibrateFlowOutNow = RecalibrateFlowOutNow,
                TimeSinceLastAlarmS = _timeSinceLastAlarmS,
                TimeSinceValidS = _timeSinceValidS,
                TimeAlarmActiveS = _timeAlarmActiveS,
                TimeHighFlowErrorS = TimeHighFlowErrorS,
                TimeHighVolErrorS = TimeHighVolErrorS
            };



            return result;
        }

        public static BadCalibration CheckCalibration(AlgData algData, StateMachine sysState)
        {
            var badCalibration = new BadCalibration
            {
                Vol = !algData.IsCalibratedExpPitVol,
                Flow = !(algData.IsCalibratedExpFlowOut && algData.IsCalibratedMeasFlowOut),
                Pres = !(algData.IsCalibratedExpPresSp)
            };
            if (sysState.IsOnBottom)
                badCalibration.Tot = badCalibration.Vol.ToInt() + badCalibration.Flow.ToInt() + badCalibration.Pres.ToInt() > 1;
            else
                badCalibration.Tot = false; //badTripTank;

            return badCalibration;
        }
    }
}
