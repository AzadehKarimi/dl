﻿using System;
using DtectLossRealtime.Models;

namespace DtectLossRealtime.EKDAlgoritm
{
    public class EkdAlgFactory
    {
        public IEKDAlgoritm GetEkdAlgoritm(string algName)
        {
            IEKDAlgoritm ekdAlgoritm =
                algName switch
                    {
                    "VOTING_ALG_OBJ()" => new VotingAlg(),
                    //"UKF_ALG_OBJ()" => new UkfAlg(),
                    //"SIMPLE_ALG_OBJ()" => new SimpleAlg(),
                    //"PDIFF_ALG_OBJ()" => new PdiffAlg(),
                    _ => throw new ArgumentException($"Not a valid algoritm name {algName}")
            };

            ekdAlgoritm.Initialize(ParameterValues.VotingParam, new double[]{});
            return ekdAlgoritm;
        }
    }
}
