﻿using System;
using System.Linq;
using Accord.Math;
using DtectLoss.Common;
using DtectLossRealtime.Helpers;
using DtectLossRealtime.Models;
using Microsoft.Extensions.Logging;
using static System.Math;

namespace DtectLossRealtime
{
    /// <summary>
    /// The purpose of this class is to compensate for delays in the flowIn
    /// measurements. We have seen from field data that in some cases the
    /// standpipe pump pressure starts to drop before we can see a reduction in
    /// the pump flow. This is not physical, but due to issues in the pump flow
    /// sensor (mail Ben Hern)
    /// </summary>
    public class StandpipePressureDelay
    {
        private readonly ILogger<StandpipePressureDelay> _logger;
        public double TimeDelayPresSpS;
        public bool IsCalibratingSPdelay;

        private readonly RingBuffer _presSPringBuffer;
        private readonly RingBuffer _flowInringBuffer;
        public double SamplesSinceLastFullFlow;
        public double SamplesSinceLastNoFlow;
        public double SamplesSinceLastCalibration;
        public double FactorForget;
        public double TimeDelayMaxS;
        public double TimeDelayLastS;
        public int TdSamplesBest;
        public int TdSamplesTemp;
        public bool IsObjFunImproving;
        private const int BufferSizeSamples               = 20; // 20 should be even number!
        private const double FullFlowThresholdLpm         = 1400;
        private const double NoFlowThresholdLpm           = 100;
        private const bool DispDebug                      = false;

        public StandpipePressureDelay(ILogger<StandpipePressureDelay> logger)
        {
            _logger = logger;
            _presSPringBuffer = new RingBuffer(BufferSizeSamples);
            _flowInringBuffer = new RingBuffer(BufferSizeSamples); 
            SamplesSinceLastFullFlow = 0;
            SamplesSinceLastNoFlow = 0;
            SamplesSinceLastCalibration = 0;
            TimeDelayPresSpS = 0;
            IsCalibratingSPdelay = false;
            FactorForget = ParameterValues.PresspParams.FactorForget; 
            TimeDelayMaxS = ParameterValues.PresspParams.TimeDelayMaxS; 
        }

        public void SoftReset()
        {
            SamplesSinceLastFullFlow = 0;
            SamplesSinceLastNoFlow = 0;
            SamplesSinceLastCalibration = 0;
            TimeDelayPresSpS = 0;
            IsCalibratingSPdelay = false;
        }

        public bool ShouldWeCalibrateNow(double flowInLpm)
        {
            bool doCalibration;
            if (flowInLpm > FullFlowThresholdLpm)
                SamplesSinceLastFullFlow = 0;
            else
                SamplesSinceLastFullFlow += 1;
           
            if (flowInLpm < NoFlowThresholdLpm)
                SamplesSinceLastNoFlow = 0;
            else
                SamplesSinceLastNoFlow += 1;

            if (SamplesSinceLastFullFlow.Equals(BufferSizeSamples / 2.0) 
                && SamplesSinceLastNoFlow < BufferSizeSamples 
                && SamplesSinceLastCalibration > BufferSizeSamples)
            {
                doCalibration = true;
                SamplesSinceLastCalibration = 0;
            }
            else
            {
                SamplesSinceLastCalibration += 1;
                doCalibration = false;
            }
            return doCalibration;
        }


        public void Calibrate()
        {
            var deringbuffer = _presSPringBuffer.Deringbuffer();
            var timevecD = deringbuffer.TbufferDStraight;
            var presSPvec = deringbuffer.VbufferStraight;
            var deringBufferResult = _flowInringBuffer.Deringbuffer();
            var flowVec = deringBufferResult.VbufferStraight;
            IsObjFunImproving = true;
            TdSamplesBest = 0;
            var objFunBest = CalcObjFun(TdSamplesBest, presSPvec, flowVec);
            TdSamplesTemp = 0;
            while (IsObjFunImproving)
            {
                TdSamplesTemp += 1;
                var objFun = CalcObjFun(TdSamplesTemp, presSPvec, flowVec);
                if (objFun >= objFunBest)
                    IsObjFunImproving = false;
                else // at least one of the objective improve on
                {
                    objFunBest = objFun;
                    TdSamplesBest = TdSamplesTemp;
                }
            }
            if (TdSamplesBest >= timevecD.Length -1 )
                TimeDelayLastS = 0;
            else
            {
               var end = timevecD[timevecD.Length - 1];
               var tt = timevecD[Max(0, timevecD.Length -1 - TdSamplesBest)];
               TimeDelayLastS = end.DiffInSeconds(tt);
               TimeDelayLastS = Min(TimeDelayMaxS, TimeDelayLastS);
                if (TimeDelayPresSpS == 0) // first time
                    TimeDelayPresSpS = TimeDelayLastS;
                else
                    TimeDelayPresSpS = (1 - FactorForget) * TimeDelayPresSpS + FactorForget * TimeDelayLastS;
               
                //if (DispDebug)
                //    _logger.LogDebug(@"New time delay for delayed PresSP meas = {timeDelay_last_s} %.2f sec, filtered value = {timeDelayPresSP_s} %.2f sec\n");
            }
        }

        public double CalcObjFun(int tdSamples, double[] presSPvec, double[] flowVec)
        {
            try
            {
                var flowMax = flowVec.Max();
                var presSpMax = presSPvec.Max();
                var flowMin = flowVec.Min();
                var presSpMin = presSPvec.Min();
                var flowNorm = flowMax - flowMin;
                var presSpNorm = presSpMax - presSpMin;
                double n = flowVec.Length - tdSamples;
                var presVecAdj = presSPvec
                    .Take(presSPvec.Length - tdSamples)
                    .Select(x => (x - presSpMin) / presSpNorm);
                var flowVecAdj = flowVec
                    .Skip(tdSamples)
                    .Select(x => (x - flowMin) / flowNorm);

                var tmpSum = presVecAdj.Zip(flowVecAdj, (p, f) => Pow(p - f, 2)).Sum();
                return 100 * Sqrt(1 / n * tmpSum);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                throw;
            }
        }

       public double GetDelayedPresSp(double timeNowD)
        {
            var (presSPdelayedBar, _) = _presSPringBuffer.GetPastValue(TimeDelayPresSpS, timeNowD);
            return presSPdelayedBar;
        }

        public void UpdateTimeDelay(double presSpBar, double flowInLpm, double timeNowD)
        {
           _presSPringBuffer.AddDataToCircularBuffer(timeNowD, presSpBar);
           _flowInringBuffer.AddDataToCircularBuffer(timeNowD, flowInLpm);

           var doCalibration = ShouldWeCalibrateNow(flowInLpm);
            if (doCalibration)
            {
                IsCalibratingSPdelay = true;
                Calibrate();
            }
            else
                IsCalibratingSPdelay = false;
        }
    }
}
