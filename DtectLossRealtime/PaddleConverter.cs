﻿using DtectLoss.Common;
using DtectLossRealtime.Calculations;
using DtectLossRealtime.Enums;
using DtectLossRealtime.Helpers;
using Microsoft.Extensions.Logging;
using System;
using static System.Math;
using static DtectLoss.Common.CommonConstants;

namespace DtectLossRealtime
{
    /// <summary>
    /// The paddle converter class 
    /// converts the paddle measurement in percent to a flowOut measurement in
    /// lpm. The conversion exploits the flowIn measurement in lpm and
    /// re-tunes a correlation between these two measurements for each 
    /// stand pipe change:
    /// 
    /// flowOut_lpm = K_lpm_per_prc * (flowOut_prc - Bias_prc),
    /// where K_lpm_per_prc is a scale factor and Bias_prc is an offset
    /// K_lpm_per_prc is calculated after every pump start up
    /// Bias_prc is calculated at the first two pump stops
    /// </summary>
    public class PaddleConverter
    {
        private readonly ILogger<PaddleConverter> _logger;
        public double FlowOutLpm;                // flow out to be estimated
        public bool IsCalibratingPaddleBias;      //
        public bool IsCalibratingPaddleK;
        public bool IsCalibratedPaddle;
        public int NumberPumpStops;
        public double KLpmPerPrc;              // the constant that gives flowOut_lpm = K_lpm_per_prc* (flowOut_prc - Bias_prc);
        public double BiasPrc;                   // Estimated bias so that  flowOut_prc = Bias_prc, when flowIn_lpm = 0
        public bool IsRampingUp;
        public StateCalibrationPaddle StateCalibrationPaddle;     // 0=normal, 1=gather data bias(zero flow), 2=gather data K(constant non-zero flow)
        public double TimePumpsOnS;              // Time with constant flow in
        public double TimePumpsOffS;             // Time with zero flow in
        public double TimeCalibratingS;          // Time calibrating bias/K

        private LowPass? _lpK;                    // low pass Filter object
        private LowPass? _lpBias;             // low pass Filter object
        private FilterDerivative? _filtData;
        private double? _flowDerLpmpsec;
        private bool _rampUpFlowIn;

        private bool _constFlowIn;
        private bool _zeroFlowIn;
        private double _timeLastD;
        private RingBuffer? _flowOutBuffer;
        private RingBuffer? _flowInBuffer;

        private const double TlpS = 30;       // lowpass Filter time constant in seconds
        private const double FlowInMinToTuneKLpm = 900;   // minimum flow before starting to tune K.
        private const double FlowCalibMinLpm = -1000;         // Measurement is not valid if below this
        private const double FlowCalibMaxLpm = +10000;        // Measurement is not valid if above this
        private const double FlowderThresholdLpmpsec = 10; // [lpm/sec] if the flow rate from the rig pumps change slower than this value -> constant/zero flow
        private const double TfiltFlowDerS = 20;   // Filter time constant for estimating time derivative of flow in from flow in
        private const double TimeWaitKCalibrationS = 120;  // [sec] 60 Waiting time after pumps are back on with constant flow in before calibration shall be initiated
        private const double TimeWaitBiasCalibrationS = 60; // [sec] Waiting time after pumps are back on with constant flow in before calibration shall be initiated
        private const double TimeTuningS = 60;   // 60 for how long should K be updated after a startup?
        private const double TimestepmaxResetS = 60 * 60;   // Reset (K= b = 0) if no flow out meas for 60mins
        private const int BufferSizeSamples = 200;
        private const bool UseRingBuffer = false; //true 
        //Pragma to disable unused warning for these variables
        #pragma warning disable 414
        private bool _rampDownFlowIn;
        #pragma warning restore 414

        public PaddleConverter(ILogger<PaddleConverter> logger)
        {
            _logger = logger;
        }
        public void Initalize()
        {
            // TODO: Add log message here
            _logger.LogDebug("Paddle Converter: Initializing");
            KLpmPerPrc = 0;
            BiasPrc = 0;
            _lpK = new LowPass(null, TlpS);
            _lpBias = new LowPass(null, TlpS);
            _filtData = new FilterDerivative(TfiltFlowDerS);
            _flowOutBuffer = new RingBuffer(BufferSizeSamples);
            _flowInBuffer = new RingBuffer(BufferSizeSamples);

            IsCalibratedPaddle = false;
            _rampUpFlowIn = false;
            _rampDownFlowIn = false;
            _constFlowIn = false;
            _zeroFlowIn = false;
            _flowDerLpmpsec = 0;
            TimePumpsOnS = 0;  // Time since pumps were turned on
            TimePumpsOffS = 0; // Time since pumps were turned on
            TimeCalibratingS = 0;
            _timeLastD = 0;
            NumberPumpStops = 0;
            IsCalibratingPaddleBias = false;
            IsCalibratingPaddleK = false;
            IsRampingUp = false;
            StateCalibrationPaddle = StateCalibrationPaddle.Normal;  // normal
            FlowOutLpm = MissingValueReplacement;
        }

        public void SoftReset()
        {
            _logger.LogDebug("Paddle Converter: SoftReset");
            KLpmPerPrc = 0;
            BiasPrc = 0;
            IsCalibratedPaddle = false;
            _rampUpFlowIn = false;
            _rampDownFlowIn = false;
            _constFlowIn = false;
            _zeroFlowIn = false;
            _flowDerLpmpsec = 0;
            TimePumpsOnS = 0;  // Time since pumps were turned on
            TimePumpsOffS = 0; // Time since pumps were turned on
            TimeCalibratingS = 0;
            _timeLastD = 0;
            NumberPumpStops = 0;
            IsCalibratingPaddleBias = false;
            IsCalibratingPaddleK = false;
            IsRampingUp = false;
            StateCalibrationPaddle = StateCalibrationPaddle.Normal;   // normal
            FlowOutLpm = MissingValueReplacement;
        }

        // run at each iteration to save the result of all algorithms
        public void Convert(double flowOutPrc, double flowInLpm, bool isDownlinking, bool recalibrateNow, StateMachine sysState, double timeNewD)
        {
            var dtS = timeNewD.DiffInSeconds(_timeLastD);

            if (double.IsNaN(flowOutPrc) || double.IsNaN(timeNewD) || dtS <= 0 || flowInLpm.Equals(MissingValueReplacement)) //do nothing
            {
                _timeLastD = timeNewD;
                FlowOutLpm = MissingValueReplacement;
                _logger.LogInformation($"Paddle converter error: flowOutPrc:{double.IsNaN(flowOutPrc)}, timeNew:{double.IsNaN(timeNewD)}, dtS:{dtS}, flowInLpm:{flowInLpm}");
            }
            else if (dtS > TimestepmaxResetS) // reset
            {
                Initalize(); 
                _timeLastD = timeNewD;
            }
            else // OK
            {
                _flowDerLpmpsec = _filtData!.Smoothderivative2ndorder(flowInLpm, timeNewD);
                _timeLastD = timeNewD;
                if (_flowDerLpmpsec > FlowderThresholdLpmpsec)
                {
                    _rampUpFlowIn = true;
                    _rampDownFlowIn = false;
                    _constFlowIn = false;
                    _zeroFlowIn = false;
                }
                else if (_flowDerLpmpsec < -FlowderThresholdLpmpsec)
                {
                    _rampUpFlowIn = false;
                    _rampDownFlowIn = true;
                    _constFlowIn = false;
                    _zeroFlowIn = false;
                }
                else // steady flow in
                {
                    if (flowInLpm < 100)
                    {
                        _zeroFlowIn = true;
                        _constFlowIn = false;
                        _rampUpFlowIn = false;
                        _rampDownFlowIn = false;
                    }
                    else if (flowInLpm > FlowInMinToTuneKLpm)
                    {
                        _zeroFlowIn = false;
                        _constFlowIn = true;
                        _rampUpFlowIn = false;
                        _rampDownFlowIn = false;
                    }
                    else // low flow rate on way up used to break the gel
                    {
                        _zeroFlowIn = false;
                        _constFlowIn = false;
                        _rampUpFlowIn = true;
                        _rampDownFlowIn = false;
                    }
                }
                if (!_constFlowIn)
                    TimePumpsOnS = 0;
                else
                    TimePumpsOnS += Min(_filtData.DtS, 30);

                if (!_zeroFlowIn)
                    TimePumpsOffS = 0;
                else
                {
                    if (TimePumpsOffS.Equals(0) && sysState.IsOnBottom)
                    {
                        NumberPumpStops += 1;
                        //if (Showdisp)
                        _logger.LogDebug($"PC.Convert: Pump stop number = {NumberPumpStops}, scale K = {KLpmPerPrc} lpm/%, bias = {BiasPrc} %");
                    }

                    TimePumpsOffS += Min(_filtData.DtS, 30);
                }

                _flowInBuffer!.AddDataToCircularBuffer(timeNewD, flowInLpm);        // Not in use
                _flowOutBuffer!.AddDataToCircularBuffer(timeNewD, flowOutPrc);      // Not in use

                if (StateCalibrationPaddle == StateCalibrationPaddle.Normal) // Normal
                {
                    if (TimePumpsOnS > TimeWaitKCalibrationS && TimePumpsOnS < TimeWaitKCalibrationS + TimeTuningS)
                    {
                        // Do not calibrate shortly after a pump start when flow out is transient
                        StateCalibrationPaddle = StateCalibrationPaddle.GatherDataK; // Gather data for K calibration
                    }
                    else if (recalibrateNow && flowInLpm > FlowInMinToTuneKLpm)
                        StateCalibrationPaddle = StateCalibrationPaddle.GatherDataK; // Gather data for K calibration
                    else if (TimePumpsOffS > TimeWaitBiasCalibrationS) // obj.timePumpsOff_s < obj.timeWaitBiasCalibration_s + obj.timeTuning_s
                    {
                        // Pumps have been off for a while
                        StateCalibrationPaddle = StateCalibrationPaddle.GatherDataBias;   // Gather data for bias calibration
                        // Do not calibrate shortly after a pump stop when flow out is transient
                    }
                    else
                        StateCalibrationPaddle = StateCalibrationPaddle.Normal;

                    TimeCalibratingS = 0;
                    IsCalibratingPaddleK = false;
                    IsCalibratingPaddleBias = false;
                }

                else if (StateCalibrationPaddle == StateCalibrationPaddle.GatherDataK) // Gather data for K calibration
                {
                    if (TimeCalibratingS < TimeTuningS)
                    {
                        StateCalibrationPaddle = StateCalibrationPaddle.GatherDataK;    // TODO: Code not doing anything
                        if (flowOutPrc - BiasPrc > 0 && !isDownlinking)
                        {
                            //_lpK!.FilterForCustomDt(flowInLpm / flowOutPrc - BiasPrc, dtS, 0);
                            _lpK!.FilterForCustomDt((double)(System.Convert.ToDecimal(flowInLpm) / (System.Convert.ToDecimal(flowOutPrc) - System.Convert.ToDecimal(BiasPrc))), dtS, 0);
                        }

                        IsCalibratingPaddleK = false;
                        IsCalibratingPaddleBias = false;
                        TimeCalibratingS += _filtData.DtS;
                    }
                    else
                    {
                        // Calibrate K
                        if (UseRingBuffer)
                        {
                            //Commented out to avoid warning of unreachable code due to always false
                            //var deringbuffer = _flowOutBuffer.Deringbuffer();
                            //var timeFlowOutVec = deringbuffer.TbufferDStraight;
                            //var flowOutVec = deringbuffer.VbufferStraight;
                            //var deringBufferResult = _flowInBuffer.Deringbuffer();
                            //var timeFlowInVec = deringBufferResult.TbufferDStraight;
                            //var flowInVec = deringBufferResult.VbufferStraight;
                            //if (true)
                            //{
                            //    ////To do - only calibrate if no vol/pres alarm is given 
                            //    CalcPaddleParams(out double? Bias, out double? K, timeFlowInVec, flowInVec,
                            //        timeFlowOutVec, flowOutVec);
                            //    if (Bias != null && K != null)
                            //    {
                            //        BiasPrc = Bias.Value;
                            //        KLpmPerPrc = K.Value;
                            //    }
                            //}
                        }
                        else
                        {
                            if (_lpK!.FilteredSignal.HasValue &&
                                (recalibrateNow && flowInLpm > FlowInMinToTuneKLpm ||
                                 TimeCalibratingS >= TimeTuningS))
                            {
                                KLpmPerPrc = _lpK.FilteredSignal ?? 0;
                                IsCalibratingPaddleK = true;
                            }
                            else
                                IsCalibratingPaddleK = false;


                            //Calibrate bias
                            if (_lpBias!.FilteredSignal.HasValue)
                            {
                                BiasPrc = _lpBias.FilteredSignal.Value;
                                IsCalibratingPaddleBias = true;
                            }
                            else
                                IsCalibratingPaddleBias = false;
                        }

                        // 8th April 2020: Moved out of GatherDataK state. Should always be executed. 
                        if (NumberPumpStops > 0 && !KLpmPerPrc.Equals(0)) 
                            IsCalibratedPaddle = true; 

                        StateCalibrationPaddle = StateCalibrationPaddle.Normal;
                        TimeCalibratingS = 0;
                    }
                }

                else if (StateCalibrationPaddle == StateCalibrationPaddle.GatherDataBias) // Gather data for bias calibration
                {
                    if (_zeroFlowIn) // timeCalibrating_s < timeTuning_s
                    {
                        StateCalibrationPaddle = StateCalibrationPaddle.GatherDataBias;     // TODO code not doing anything
                        if (!isDownlinking
                        ) //&& (obj.numberPumpStops <= obj.numberPumpStopsBiasCalibration) % Calibrate only on first 2 pump stops.TO DO: improve!!
                            _lpBias!.FilterForCustomDt(flowOutPrc, dtS, 0);

                        TimeCalibratingS += _filtData.DtS;
                    }
                    else
                    {
                        StateCalibrationPaddle = StateCalibrationPaddle.Normal;
                        IsCalibratingPaddleBias = false;
                        TimeCalibratingS = 0;
                        //// Calibrate bias
                        //if (false) // ~isempty(obj.lpBias.FilteredSignal) TODO - Never used
                        //{
                        //    BiasPrc = _lpBias.FilteredSignal.Value;
                        //    IsCalibratingPaddleBias = true;
                        //}
                        //else
                        //    IsCalibratingPaddleBias = false;

                    }
                }

                IsRampingUp = _rampUpFlowIn || TimePumpsOnS < TimeWaitKCalibrationS + TimeTuningS && TimePumpsOnS > 0;
                if (flowOutPrc < 0)
                    flowOutPrc = 0;
            }

            // 8th April 2020: Moved out of GatherDataK state. Should always be executed. 
            // TODO: Moved out of GatherDataK state as an experiment. Should be a executed independent of state. 
            //if (NumberPumpStops > 0 && !KLpmPerPrc.Equals(0))
            //    IsCalibratedPaddle = true;

            if (IsCalibratedPaddle)
            {
                FlowOutLpm = KLpmPerPrc * (flowOutPrc - BiasPrc); //obj.FilteredSignal;
                if (FlowOutLpm < FlowCalibMinLpm || FlowOutLpm > FlowCalibMaxLpm)
                    FlowOutLpm = MissingValueReplacement;
                else if (FlowOutLpm < 0)
                    FlowOutLpm = 0;
            }
            else
                FlowOutLpm = MissingValueReplacement;
        }

        public void CalcPaddleParams(out double? Bias, out double? K, double[] timeIn_vec, double[] flowIn_vec, double[] timeOut_vec, double[] flowOut_vec)
        {
            // Not verified yet as this code is never reached
            Bias = null;
            K = null;
            //// 1.Find last periode when flowin was zero
            //int? iEndZeroFlow = null;
            //int? iStartZeroFlow = null;
            //int? iStartFullFlow = null;
            //for (var i = flowIn_vec.Length - 1; i >= 0; i--)
            //{
            //    if (iEndZeroFlow == null && flowIn_vec[i] < 50)
            //        iEndZeroFlow = i;

            //    if (iStartZeroFlow == null && flowIn_vec[i] > 50)
            //        iStartZeroFlow = i;

            //    if (iStartFullFlow == null && flowIn_vec[i] - flowIn_vec[BufferSizeSamples] > 50)
            //        iStartFullFlow = i;
            //}

            //if (iStartZeroFlow != null && iEndZeroFlow != null)
            //{
            //    var iStartBiasCalc = Vec.Logical(timeIn_vec, x => x.DiffInSeconds(timeIn_vec[iStartZeroFlow.Value]) > TimeWaitBiasCalibrationS).Min();
            //    // 2.Calculate bias as average flowOut in this period excl 1.minute
            //    Bias = flowOut_vec.Subset(iStartBiasCalc, iEndZeroFlow.Value).Average();
            //    // 3.Find periode after pump was back on

            //    var iStartKCalc = Vec.Logical(timeIn_vec, x=> x.DiffInSeconds(timeIn_vec[iStartFullFlow.Value]) > TimeWaitKCalibrationS).Min();
            //    // 4.Calculate scale(gain)
            //    K = (flowOut_vec.Subset(iStartKCalc, BufferSizeSamples).Average() - Bias.Value) /
            //        flowIn_vec.Subset(iStartKCalc, BufferSizeSamples).Average();
            //}
            //else
            //{
            //    Bias = null;
            //    K = null;
            //}
        }
    }
}
