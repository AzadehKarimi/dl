﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using DtectLoss.Common;
using DtectLossRealtime.EKDAlgoritm;
using DtectLossRealtime.Enums;
using DtectLossRealtime.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using static DtectLoss.Common.CommonConstants;

namespace DtectLossRealtime
{
    public interface IDtectLossRealtime
    {
        void Initialize(string algorithmName, Dictionary<string, string> rawDataUnits, double startTime, double areaDppoohM2, double areaDprihM2);
        DtectLossRealtimeOutput StepAllAlgorithms(Dictionary<string, DataVariable> rawDataNonstandardUnits);
        string CheckData();
        void SetAreaDp(double areaDppoohM2, double areaDprihM2);
        void SoftReset();
        string GetVersion();
    }

    public class DtectLossRealtime : IDtectLossRealtime
    {
        private readonly IProcess _process;
        private readonly Describe _describe;
        private readonly ExpectedFlowOutFromFlowIn _flowOutFlowIn;
        private readonly ExpectedStandpipePres _expectedStandpipePres;
        private readonly ExpectedTripTankVol _expectedTripTankVol;
        private FlowlineAndPit _flowlineAndPit;
        private readonly GainDetector _gainDetector;
        private readonly FlowBackRecorder _flowBackRecorder;
        private readonly ILogger<DtectLossRealtime> _logger;
        private StateMachine _sysStateMachine;
        private readonly EvaluateRealTime _evaluateRealTime;
        private bool _recalibratePaddleNow;
        //private Dictionary<string, string> _rawDataUnits;
        //private string _algoritmShortname;
        private IEKDAlgoritm? _ekdAlgoritm;

        //% flowline and pit paramters used for initialization
        private const double V0 = 5; // [m3] Init only, not relevant for tuning
        private const double KInjFlowLine = 0; // [1/s] Shall not be used
        private static readonly double[] X0 = {5,0,0};
        private double _prevGoodVolGainLossM3;
        private readonly EkdAlgFactory _algFactory;
        private readonly IServiceProvider _serviceProvider;

        //parameters for the flowback recorder
        private const int NFlowbacks = 100; // [int] not relevant for tuning
        private const double MinVolumeM3 = 1; // [m3] not relevant for tuning
        private const double MinFlowrateLpm = 50; // [lpm] threshold for flow-rate from rig pumps.qp<minFlowrate -> pumps off - not relevant for tuning
        private const double FlowToleranceLpm = 400; // [lpm] stddev which is tolerated as steady state for flow-rate from rig pumps - not relevant for tuning
        private const double DtFlowrateS = 1; // [s] not relevant for tuning
        private const double DtVolGlS = 1; // [s] not relevant for tuning
        private const double TimeDurationS = 1200; // [s] not relevant for tuning

        public DtectLossRealtime(
            ILogger<DtectLossRealtime> logger,
            IProcess process,
            EvaluateRealTime evaluateRealTime,
            StateMachine stateMachine,
            Describe describe,
            ExpectedFlowOutFromFlowIn flowOutFlowIn,
            ExpectedStandpipePres expectedStandpipePres,
            ExpectedTripTankVol expectedTripTankVol,
            FlowlineAndPit flowlineAndPit,
            GainDetector gainDetector,
            FlowBackRecorder flowBackRecorder,
            EkdAlgFactory algFactory,
            IServiceProvider serviceProvider
            )
        {
            _process = process;
            _evaluateRealTime = evaluateRealTime;
            _logger = logger;
            _sysStateMachine = stateMachine;
            _describe = describe;
            _flowOutFlowIn = flowOutFlowIn;
            _expectedStandpipePres = expectedStandpipePres;
            _expectedTripTankVol = expectedTripTankVol;
            _flowlineAndPit = flowlineAndPit;
            _gainDetector = gainDetector;
            _flowBackRecorder = flowBackRecorder;
            _algFactory = algFactory;
            _serviceProvider = serviceProvider;
        }

        public string GetVersion()
        {
            if (Assembly.GetExecutingAssembly() != null)
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }            
            else
            {
                return string.Empty;
            }
        }

        public void Initialize(string algorithmName, Dictionary<string, string> rawDataUnits, double startTime, double areaDppoohM2, double areaDprihM2)
        {
            //_rawDataUnits = rawDataUnits;
            //_algoritmShortname = algorithmName.ToLower().Replace("_alg_obj()", "");
            _recalibratePaddleNow = false;
            _process.Initialize(startTime);
            _process.UpdateTagList(rawDataUnits);
            InitializeSubmodules(algorithmName, areaDppoohM2, areaDprihM2);
        }

        private void InitializeSubmodules(string algorithmName, double areaDppoohM2, double areaDprihM2)
        {
            _describe.Initialize();
            _flowOutFlowIn.Initalize(ParameterValues.FlowoutParams);
            _expectedStandpipePres.Initalize(ParameterValues.PresspParams);
            _expectedTripTankVol.Initialize(ParameterValues.TripTankParams);
            _expectedTripTankVol.SetAreadp(areaDppoohM2, areaDprihM2);
            _flowlineAndPit.Initialize(X0, V0, KInjFlowLine,ParameterValues.PitvolParams.KInjGainLossHz);
            _gainDetector.Initialize();
            _flowBackRecorder.Initialize(NFlowbacks, MinVolumeM3, MinFlowrateLpm, FlowToleranceLpm, DtFlowrateS, DtVolGlS, TimeDurationS, ParameterValues.PitvolParams);
            _ekdAlgoritm = _algFactory.GetEkdAlgoritm(algorithmName);
            _prevGoodVolGainLossM3 = MissingValueReplacement;
        }

        public void SetAreaDp(double areaDppoohM2, double areaDprihM2)
        {
            _expectedTripTankVol.SetAreadp(areaDppoohM2, areaDprihM2);
        }

        public void SoftReset()
        {
            _logger.LogInformation($"SoftReset called, time step = {_process.DtS}");
            _flowBackRecorder.SoftReset();
            _flowlineAndPit.SoftReset();
            _flowOutFlowIn.SoftReset();
            _expectedStandpipePres.SoftReset();
            _describe.SoftReset();
            _ekdAlgoritm!.SoftReset();
            _process.SoftReset();
            _sysStateMachine = _serviceProvider.GetService<StateMachine>();
            _evaluateRealTime.SoftReset();
            _gainDetector.SoftReset();
        }


        public DtectLossRealtimeOutput StepAllAlgorithms(Dictionary<string, DataVariable> rawDataNonstandardUnits)
        {
            var output = new DtectLossRealtimeOutput();
            try
            {
                var washedDataResult = _process.WashData(rawDataNonstandardUnits, _sysStateMachine, _recalibratePaddleNow);

                if (!washedDataResult.ErrorCode.Equals(DtectLossRealtimeErrorCodes.AllOk))
                {
                    //TODO Set output struct codes
                    //when output has been determined
                    output.ErrorCode = washedDataResult.ErrorCode;
                    output.WarningCode = washedDataResult.WarningCode;
                    _logger.LogError($"StepAll returning with errors: Error:{output.ErrorCode}, Warning: {output.WarningCode}");
                    return output;
                }

                //TODO: Set error and warning code cell output 
                //when output has been determined
                var washedData = washedDataResult.WashedData;

                //call the state machine
                var sysState = UpdateSysStateAndDescribe(washedDataResult.RawData!, washedDataResult!.WashedData!);

                var flowInOutOutput = _flowOutFlowIn.Step(washedData!.Values[V.FlowInLpm], washedData.Values[V.FlowOutLpm], washedData.IsDownlinking, _ekdAlgoritm!.AlarmState, _process.TimeCurrentD);
                var presExpOutput = _expectedStandpipePres.Step(washedData.Values[V.FlowInfiltLpm], washedData.Values[V.PresSpBar], washedData.IsDownlinking, _ekdAlgoritm.AlarmState, _process.TimeCurrentD);
                var volExpTripOutput = _expectedTripTankVol.Step(washedData.Values[V.DepthBitM], washedData.Values[V.DepthHoleM], washedData.Values[V.VolTripTankM3], washedData.Values[V.FlowInfiltLpm], _process.TimeCurrentD);

                //debug
                //var timeDebug = DateTime.FromOADate(_process.TimeCurrentD).ToUniversalTime();
                //var timeToDebugCondition = DateTime.Parse("30/10/2015 04:06:20", new CultureInfo("nb-NO", false)).ToUniversalTime();

                var volGainLossPredError = StepFlowbackRecorder(washedData);

                ResetExpectedVolGainLoss(washedData);
                var volGainLossPredModelled = StepFlowlineAndPit(flowInOutOutput, washedData); /// HERE
                var volGainLossExpectedM3 = volGainLossPredModelled + volGainLossPredError[0];
                var isCalibratedExpPitVolBoolean = IsCalibratedExpPitVol();

                var flowlineAndPitOutput = _flowlineAndPit.GenerateOutput(volGainLossExpectedM3, isCalibratedExpPitVolBoolean);
                _gainDetector.Step(_process.TimeCurrentD, washedData.Values[V.VolGainLossM3], washedData.Values[V.DepthBitM], washedData.Values[V.DepthHoleM], washedData.Values[V.FlowInLpm]);

                var gainDetectorOutput = _gainDetector.GenerateOutput();

                var isCalibrating = _expectedStandpipePres.IsCalibratingExpPresSp ||
                                    _process.PaddleConv.IsCalibratingPaddleBias ||
                                    _process.PaddleConv.IsCalibratingPaddleK ||
                                    _flowOutFlowIn.IsCalibratingFlowInFlowOut ||
                                    _process.PresSPdelay.IsCalibratingSPdelay;

                var algData = new AlgData()
                {
                    TimeCurrentD = _process.TimeCurrentD,
                    FlowOutExpectedLpm = _flowOutFlowIn.FlowOutExpectedLpm,
                    PresSpExpectedBar = _expectedStandpipePres.PresSpExpectedBar,
                    NumberPumpStops = washedData.NumberPumpStops,
                    TimePumpsOnS = washedData.TimePumpsOnS,
                    VolTripTankCumMeasM3 = _expectedTripTankVol.VolTTcumM3,
                    VolTripTankCumExpectedM3 = _expectedTripTankVol.VolTTcumExpM3,
                    VolTripTankCumErrorM3 = _expectedTripTankVol.VolTTcumErrM3,
                    TimeSinceResetGlS = _flowlineAndPit.TimeSinceResetGlS,
                    VolGainLossExpectedM3 = volGainLossExpectedM3,
                    VolGainDetectorM3 = _gainDetector.VolGainM3,
                    IsCalibratedExpPresSp = _expectedStandpipePres.IsCalibratedExpPresSp,
                    IsCalibratedExpPitVol = isCalibratedExpPitVolBoolean,
                    IsCalibratedExpFlowOut = flowInOutOutput.IsCalibratedExpFlowOut,
                    IsCalibratedMeasFlowOut = washedData.IsCalibratedMeasFlowOut,
                    IsCalibrating = isCalibrating,
                    IsDownlinking = washedData.IsDownlinking,
                    IsRampingUp = washedData.IsRampingUp,
                    ResetVolIOnow = washedData.IsCalibratingPaddleK
                };

                AlgResult algResult;
                if (!_sysStateMachine.IsDataValid)
                {
                    algResult = _ekdAlgoritm.ReturnResultStruct();
                }
                else
                {
                    // Step the main EKD algoritm
                    algResult = _ekdAlgoritm.StepIt(washedData, algData, _sysStateMachine, _process.TimeCurrentD, _process.DtS);
                    if (_ekdAlgoritm.ResetAlarm)
                    {
                        if (!washedData.Values[V.VolGainLossM3].Equals(MissingValueReplacement))
                        {
                            _flowlineAndPit.SetQuickConvergeNow(true);
                        } 
                        _expectedTripTankVol.ResetTripVol();
                        _gainDetector.ResetAfterAlarm();
                    }
                    // evaluate realtime the number of alarms


                    _evaluateRealTime.Step(_process.TimeCurrentD, _process.DtS, algResult.AlarmState, algResult.AlarmType);
                    if (_ekdAlgoritm.RecalibrateFlowOutNow && !washedData.Values[V.FlowOutLpm].Equals(MissingValueReplacement))
                    {
                        _recalibratePaddleNow = true;
                    }
                    else
                    {
                        _recalibratePaddleNow = false;
                    }

                }
                //Matlab code determine output based on if this is backtest or not. For now, assume this is handled by logging
                output.RawDataNonstandardUnits = rawDataNonstandardUnits;
                output.WashedData = washedData;
                output.RawData = washedDataResult.RawData!;
                output.IsCalibrating = isCalibrating;
                output.FlowInOutOutput = flowInOutOutput;
                output.PresSPexpOutput = presExpOutput;
                output.VolExpTripOutput = volExpTripOutput;
                output.FlowlineAndPitOutput = flowlineAndPitOutput;
                output.WashedProcessOutput = washedDataResult.WashedProcessOutput!;
                output.GainDetectorOutput = gainDetectorOutput;
                output.VolGainLossExpectedM3 = volGainLossExpectedM3;
                output.VolGainLossPredModelledM3 = volGainLossPredModelled;
                output.VolGainLossPredErrorM3 = volGainLossPredError[0];
                output.AlgData = algData;
                output.AlgResult = algResult;
                output.EvaluateRealTimeOutput = _evaluateRealTime.GenerateOutput();
                output.ErrorCode = washedDataResult.ErrorCode;
                output.WarningCode = washedDataResult.WarningCode;
                output.SysState = sysState;
                return output;
            }
            catch (Exception e)
            {
                _logger.LogError("Realtime error", e, e.InnerException);
                throw;
            }
        }

        public string CheckData()
        {
            _describe.CheckDataSeries();
            return _describe.DispSeriesAnalysis();
        }

        private bool IsCalibratedExpPitVol()
        {
            return _flowBackRecorder.KStore.Any(x => x > 0);
        }

        private double StepFlowlineAndPit(FlowInOutOutput flowInOutOutput, WashedData washedData)
        {
            // StepIt called with:
            // expected flow rate out
            // flowIn
            // gain/loss volume which is reset
            // boolean which indicate if output injection should be used.
            _flowlineAndPit.StepIt(new DataVariable(_process.TimeCurrentD, flowInOutOutput.FlowOutExpectedLpm),
                new DataVariable(_process.TimeCurrentD, washedData.Values[V.FlowInLpm]),
                new DataVariable(_process.TimeCurrentD, washedData.Values[V.VolGainLossM3]), _flowBackRecorder.PumpsOn);
            return _flowlineAndPit.X[1];
        }


        /// <summary>
        /// if gain/loss is reset, then reset expected volGainLoss.
        /// initialize previous good value of volGainLoss, which will be
        /// used for comparison with current volGainLoss
        /// </summary>
        /// <param name="washedData"></param>
        private void ResetExpectedVolGainLoss(WashedData washedData)
        {
            if (_prevGoodVolGainLossM3.Equals(MissingValueReplacement) &&
                !washedData.Values[V.VolGainLossM3].Equals(MissingValueReplacement))
            {
                _prevGoodVolGainLossM3 = washedData.Values[V.VolGainLossM3];
            }
            if ( !washedData.Values[V.VolGainLossM3].Equals(MissingValueReplacement))
            {
                _flowlineAndPit.ResetGlWithSlidingWindowOfFlowIn(_prevGoodVolGainLossM3, washedData.Values[V.VolGainLossM3], _process.VolGainLossThresholdM3PerTimeStep);
                _prevGoodVolGainLossM3 = washedData.Values[V.VolGainLossM3];
            }

            if (_flowBackRecorder.IsInit)
            {
                _flowlineAndPit.ResetGl(_prevGoodVolGainLossM3);
            }
        }

        private double[] StepFlowbackRecorder(WashedData washedData)
        {
            _flowBackRecorder.AddFlowToRingbuffer(_process.TimeCurrentD, washedData.Values[V.FlowInLpm]);
            _flowBackRecorder.AddPresToRingbuffer(_process.TimeCurrentD, washedData.Values[V.PresSpBar]);
            _flowBackRecorder.AddVolToRingbuffer(_process.TimeCurrentD, washedData.Values[V.VolGainLossM3]);
            _flowBackRecorder.AddExpVolToRingbuffer(_flowlineAndPit.PrevTimeStamp, _flowlineAndPit.X[1]);
            _flowBackRecorder.UpdateCirculationState(washedData.Values[V.FlowInLpm], washedData.Values[V.DepthBitM], washedData.Values[V.DepthHoleM]);
            _flowBackRecorder.UpdateTimeSpentAtState(_process.DtS);
            _flowlineAndPit = _flowBackRecorder.UpdateFap(_flowlineAndPit);
            _flowBackRecorder.UpdateConnectionData();

            var volGainLossPredError = new double[]{0};
            if (_flowBackRecorder.Mdl != null)
            {
                if (_flowBackRecorder.Mdl.Coeffs.Length > 0)
                {
                    volGainLossPredError = _flowBackRecorder.PredictFromModel();
                }
            }
            return volGainLossPredError;
        }

        private DrillingStates UpdateSysStateAndDescribe(Dictionary<string, double> rawData, WashedData washedData)
        {
            _sysStateMachine.Update(washedData, _process.DtS);
            _describe.DescribeData(rawData, washedData, _sysStateMachine, _process.DtS);

            return _sysStateMachine.CurrentState;
        }
    }
}
