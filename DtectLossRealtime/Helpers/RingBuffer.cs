﻿using System.Linq;
using Accord.Math;
using DtectLoss.Common;
using DtectLossRealtime.Models;
using static System.Math;
using static DtectLoss.Common.CommonConstants;
using static DtectLossRealtime.Helpers.MathFunctions;

namespace DtectLossRealtime.Helpers
{
    /// <summary>
    /// RingBuffer uses the posBufferLastWriteIdx to write in circles, avoiding shifting the values of the buffer.
    /// Buffer is initialized with the error value.
    /// VBuffer - 
    /// Tbuffer_d - 
    /// A serial date number represents a calendar date as the number of days that has passed since a fixed base date. In MATLAB, serial date number 1 is January 1, 0000
    /// 
    /// </summary>
    public class RingBuffer
    {
        public int BufferSizeSamples { get; }
        public double[] Vbuffer { get; }
        public double[] TbufferD { get; }
        private int _posBufferLastWriteIdx;
        private const double ErrVal = MissingValueReplacement;
        private double _vlast;
        private double? _getPastValueTimeOldD = null;

        public RingBuffer(int bufferSizeSamples)
        {
            BufferSizeSamples = bufferSizeSamples;
            Vbuffer = new double[bufferSizeSamples];
            TbufferD = Vector.Create(bufferSizeSamples, ErrVal);
            _posBufferLastWriteIdx = 0;
        }

        public void AddDataToCircularBuffer(double timeNewD, double value)
        {
            if ((timeNewD <= TbufferD[_posBufferLastWriteIdx]) || value.Equals(ErrVal)  || timeNewD.Equals(ErrVal))
            {     // do not add meaningless values to buffer.
                return;
            }
            if (_posBufferLastWriteIdx + 1 < BufferSizeSamples)
            {
                _posBufferLastWriteIdx ++;
            }
            else
            {
                _posBufferLastWriteIdx = 0;
            }
            Vbuffer[_posBufferLastWriteIdx] = value;
            TbufferD[_posBufferLastWriteIdx] = timeNewD;
        }

        public double GetAvgBufferSampleTime()
        {
            int oldestIdx;
            double avgBufferSampleTimeS;

            if (_posBufferLastWriteIdx == BufferSizeSamples - 1)
            {
                oldestIdx = 0;
            }
            else
            {
                oldestIdx = _posBufferLastWriteIdx + 1;
            }
            if (TbufferD[oldestIdx] == ErrVal)
            {
                avgBufferSampleTimeS = ErrVal;
            }
            else
            {
                avgBufferSampleTimeS = TbufferD[_posBufferLastWriteIdx].DiffInSeconds(TbufferD[oldestIdx]) / BufferSizeSamples;
            }
            return avgBufferSampleTimeS;
        }

        /// <summary>
        /// Value at last write index
        /// </summary>
        /// <returns></returns>
        public double GetLastValue()
        {
            _vlast = Vbuffer[_posBufferLastWriteIdx];
            return _vlast;
        }

        //TODO: Matlab implementation seems to take n+1 items. Verify.
        public double[] GetLastNValues(int n)
        {
            double[] result;
            n = n > BufferSizeSamples ? BufferSizeSamples : n;

            if (_posBufferLastWriteIdx - n >= 0)
            {
                result = Vbuffer.Skip(_posBufferLastWriteIdx - n).Take(n + 1).ToArray();
            }
            else
            {
                var vLastArr = Vbuffer.Take(_posBufferLastWriteIdx + 1);
                var vFirstArr = Vbuffer.Skip(BufferSizeSamples - n + _posBufferLastWriteIdx);
                result = vFirstArr.Concat(vLastArr).ToArray();
            }
            return result;
        }
        //TODO: Matlab implementation seems to take n+1 items. Verify.
        public double[] GetLastNValuesCorrectNotAsInMatlab(int n)
        {
            double[] result;
            n = n > BufferSizeSamples ? BufferSizeSamples : n;

            if (_posBufferLastWriteIdx - n >= 0)
            {
                result = Vbuffer.Skip(_posBufferLastWriteIdx - n + 1).Take(n).ToArray();
            }
            else
            {
                var vLastArr = Vbuffer.Take(_posBufferLastWriteIdx + 1);
                var vFirstArr = Vbuffer.Skip(BufferSizeSamples - n + _posBufferLastWriteIdx + 1);
                result = vFirstArr.Concat(vLastArr).ToArray();
            }
            return result;
        }

        public double[] GetLastNTimes(int n)
        {
            double[] result;
            n = n > BufferSizeSamples ? BufferSizeSamples : n;

            if (_posBufferLastWriteIdx - n >= 0)
            {
                result = TbufferD.Skip(_posBufferLastWriteIdx - n).Take(n + 1).ToArray();
            }
            else
            {
                var vLastArr = TbufferD.Take(_posBufferLastWriteIdx + 1);
                var vFirstArr = TbufferD.Skip(BufferSizeSamples - n + _posBufferLastWriteIdx);
                result = vFirstArr.Concat(vLastArr).ToArray();
            }
            return result;
        }
        public double[] GetLastNTimesCorrectNotAsInMatlab(int n)
        {
            double[] result;
            n = n > BufferSizeSamples ? BufferSizeSamples : n;

            if (_posBufferLastWriteIdx - n >= 0)
            {
                result = TbufferD.Skip(_posBufferLastWriteIdx - n + 1).Take(n).ToArray();
            }
            else
            {
                var vLastArr = TbufferD.Take(_posBufferLastWriteIdx + 1);
                var vFirstArr = TbufferD.Skip(BufferSizeSamples - n + _posBufferLastWriteIdx + 1);
                result = vFirstArr.Concat(vLastArr).ToArray();
            }
            return result;
        }

        /// <summary>
        /// Time at last write index
        /// </summary>
        /// <returns></returns>
        public double GetLastTime()
        {
            return TbufferD[_posBufferLastWriteIdx];
        }

        public (double[] timeWindow, double[] values) GetWindowOfLastValues(double timeD)
        {
            double[] timeWindowD;
            double[] values;

            if (timeD > GetLastTime())
            {
                timeWindowD = new double[]{};
                values = new double[]{};
                return (timeWindowD, values);
            }

            var deringBufferResult = Deringbuffer();
            var tbufferDStraight = deringBufferResult.TbufferDStraight;
            var vbufferStraight = deringBufferResult.VbufferStraight;
            var validDataPoints = deringBufferResult.ValidDataPoints;

            timeWindowD = tbufferDStraight.Subset(validDataPoints);
            values = vbufferStraight.Subset(validDataPoints);

            if (timeD > tbufferDStraight[0])
            {
                var indices = timeWindowD.Select(x => x > timeD).ToArray();
                values = values.Subset(indices);
                timeWindowD = timeWindowD.Subset(indices);
            }
            return (timeWindowD, values);
        }


        public double GetValueRange(double startTimeD)
        {
            double valueRange = 0;
            var (_, values) = GetWindowOfLastValues(startTimeD);
            if (values.Length > 0)
            {
                valueRange = Enumerable.Max(values) - Enumerable.Min(values);
            }
            return valueRange;
        }

        public DeringBufferResult Deringbuffer()
        {
            var tbufferDStraight = TbufferD[(_posBufferLastWriteIdx + 1)..].Concat(TbufferD[..(_posBufferLastWriteIdx + 1)]).ToArray();
            var vbufferStraight = Vbuffer[(_posBufferLastWriteIdx + 1)..].Concat(Vbuffer[..(_posBufferLastWriteIdx + 1)]).ToArray();
            var validTbufferDDataPoints = tbufferDStraight.Select(x => x != ErrVal);
            var validVbufferDDataPoints = tbufferDStraight.Select(x => x != ErrVal);
            var validDataPoints = validVbufferDDataPoints.LogicAnd(validTbufferDDataPoints);
            var dtDiff = tbufferDStraight.Subset(validDataPoints).Diff();
            var dtAvgD = dtDiff.Any() ? dtDiff.Average() : 0;
            return new DeringBufferResult(tbufferDStraight, vbufferStraight, dtAvgD, validDataPoints);
        }
        //TODO:Testing improvements on linq usage
        public DeringBufferResult DeringbufferV2()
        {
            var tbufferDStraight = TbufferD[(_posBufferLastWriteIdx + 1)..].Concat(TbufferD[..(_posBufferLastWriteIdx + 1)]).ToArray();
            var vbufferStraight = Vbuffer[(_posBufferLastWriteIdx + 1)..].Concat(Vbuffer[..(_posBufferLastWriteIdx + 1)]).ToArray();
            var validTbufferDDataPoints = tbufferDStraight.Select(x => x != ErrVal);
            var validVbufferDDataPoints = vbufferStraight.Select(x => x != ErrVal);
            var validDataPoints = validTbufferDDataPoints
                .LogicAnd(validVbufferDDataPoints);
            var dtDiff = tbufferDStraight.Subset(validDataPoints).Diff();
            var dtAvgD = dtDiff.Any() ? dtDiff.Average() : 0;
            return new DeringBufferResult(tbufferDStraight, vbufferStraight, dtAvgD, validDataPoints);
        }

        public DeringBufferAndResampleResult DeringbufferAndResample(bool doPadMissingValues = false)
        {
            double[] tbufferDStraightResampled;
            double[] vbufferStraightResampled;
            var deringBufferResult = Deringbuffer();
            var tbufferDStraight = deringBufferResult.TbufferDStraight;
            var vbufferStraight = deringBufferResult.VbufferStraight;
            var dtAvgD = deringBufferResult.DtAvgD;
            var validDataPoints = deringBufferResult.ValidDataPoints;
            //var validDataPoints2 = vbufferStraight.GetOnlyTrueValues(deringBufferResult.ValidDataPoints.ToBoolean());

            if (validDataPoints.Any() && validDataPoints.Sum() > 1)
            {
                var tValidD = tbufferDStraight.Subset(deringBufferResult.ValidDataPoints);
                var vValid = vbufferStraight.Subset(deringBufferResult.ValidDataPoints);

                var nInvalidDataPoints = validDataPoints.Count(x => !x);

                if (!doPadMissingValues)
                {
                    var missingVals = Vector.Create(nInvalidDataPoints, ErrVal);

                    //TODO: Added one extra in range to avoid exception in subset for valid points
                    tbufferDStraightResampled = missingVals.Concat(Vector.Range(tValidD.First(), tValidD.Last() + dtAvgD, dtAvgD)).ToArray();
                    vbufferStraightResampled = missingVals.Concat(Interp1(tValidD, vValid, tbufferDStraightResampled.Subset(validDataPoints)))
                                                            .ToArray();
                } else
                {
                    tbufferDStraightResampled = Vector.Interval(tValidD[0] - nInvalidDataPoints * dtAvgD, tValidD.Last(), tbufferDStraight.Length);
                    vbufferStraightResampled = Vector.Create(nInvalidDataPoints, vValid[0])
                                                       .Concat(Interp1(tValidD, vValid, tbufferDStraightResampled.Subset(validDataPoints)))
                                                       .ToArray();
                }
            }
            else
            {
                tbufferDStraightResampled = new double[]{};
                vbufferStraightResampled = new double[]{};
                dtAvgD = 0;
            }

            return new DeringBufferAndResampleResult(tbufferDStraightResampled, vbufferStraightResampled, dtAvgD);
        }

        public (double[] timePadded, double[] valuePadded) PadMissingValues(double[] tValidD, double[] vValid, double dtAvgD, int nInvalidDataPoints, bool useErrValForPadding)
        {
            double[] timePadded;
            double[] valuePadded;
            if (useErrValForPadding)
            {
                timePadded = Vector.Create(nInvalidDataPoints, ErrVal);
                valuePadded = timePadded;
            }
            else
            {
                timePadded = Vector.Interval(tValidD[0] - nInvalidDataPoints * dtAvgD, tValidD[0] - dtAvgD, nInvalidDataPoints);
                valuePadded = Vector.Create(nInvalidDataPoints, vValid[0]);
            }
            return (timePadded, valuePadded);
        }

        /// <summary>
        /// compute covariance of the first half of the values stored in
        /// the ringbuffer
        /// </summary>
        /// <param name="varCalc"></param>
        /// <param name="vbufferFirstHalf"></param>
        public (double? varCalc, double[] vbufferFirstHalf) VarFirstHalf()
        {
            double? varCalc;
            double[] vbufferFirstHalf;
            //TODO:Matlab implementation takes 4/10 values in the latter case. Maybe improve later?.
            var n1 = (int)Floor((double)BufferSizeSamples/2); //% number of samples in the first half

            if (_posBufferLastWriteIdx <= BufferSizeSamples - n1 - 1)
            {
                vbufferFirstHalf = Vbuffer[(_posBufferLastWriteIdx + 1)..(_posBufferLastWriteIdx + n1 + 1)];
            }
            else
            {
                vbufferFirstHalf = 
                    Vbuffer[(_posBufferLastWriteIdx + 1)..]
                    .Concat(Vbuffer.Take(Abs(_posBufferLastWriteIdx - n1)))
                    .ToArray();
            }

            var fbgz = vbufferFirstHalf.Where(x => x > 0).ToArray();
            if (fbgz.Any())
                varCalc = fbgz.Variance();
            else
                varCalc = null;
            return (varCalc, vbufferFirstHalf);
        }

        public (double vdelayedLpm, double dtDelayedS) GetPastValue(double timeDelayS, double timeNewD)
        {
            double vdelayedLpm;
            double dtDelayedS;
            if (_getPastValueTimeOldD == null)
            {
                _getPastValueTimeOldD = timeNewD;
            }
            (vdelayedLpm, dtDelayedS) = GetPastValueWithExplicitLastCallTime(timeDelayS, timeNewD, _getPastValueTimeOldD.Value);
            _getPastValueTimeOldD = timeNewD;
            return (vdelayedLpm, dtDelayedS);
        }

        // Missing unit tests, but tested by GetPastValue tests
        public (double VdelayedLpm, double dtDelayedS ) GetPastValueWithExplicitLastCallTime(double timeDelayS, double timeNewD, double timeOldD)
        {
            double vdelayedLpm;
            double dtDelayedS = 1;
            var idx = _posBufferLastWriteIdx;
            var idxPast = _posBufferLastWriteIdx;
            var deltaTMostRecentS = timeNewD.DiffInSeconds(TbufferD[idx]);
            var isFound = timeDelayS < deltaTMostRecentS;
            
            var whileCounter = 0;
            double deltaTCurrentS = 0;
            double deltaTPastS = 0;
            while (isFound == false)
            {
                whileCounter += 1;
                deltaTCurrentS = timeNewD.DiffInSeconds(TbufferD[idx]);
                deltaTPastS = timeNewD.DiffInSeconds(TbufferD[idxPast]);

                if (deltaTCurrentS >= timeDelayS && deltaTPastS <= timeDelayS)
                    isFound = true;
                else
                {
                    idxPast = idx;
                    if (idx > 0)
                        idx -= 1;
                    else
                        idx = BufferSizeSamples - 1;
                }


                if (whileCounter >= BufferSizeSamples || TbufferD[idx].Equals(ErrVal) || TbufferD[idxPast].Equals(ErrVal))
                {
                    vdelayedLpm = ErrVal;
                    return (vdelayedLpm, dtDelayedS);
                }
            }

            if (timeDelayS > 0)
            {
                var a = (timeDelayS - deltaTPastS) / (deltaTCurrentS - deltaTPastS);
                a = Max(0, a);
                a = Min(1, a);
                vdelayedLpm = Vbuffer[idx] * a + Vbuffer[idxPast] * (1 - a);
            }
            else
            {
                vdelayedLpm = Vbuffer[idx];
            }
            dtDelayedS = timeNewD.DiffInSeconds(timeOldD);
            return (vdelayedLpm, dtDelayedS);
        }

        /// <summary>
        /// given a time in serial time(_time_d), look up value in ring
        /// buffer which corresponds to time and return this (lookUpVal).
        /// If the time does not correspond exactly to a sampled value,
        /// then interpolate between available values.If no value is
        /// available, then return the error value obj.errVal.
        /// </summary>
        /// <param name="timeD"></param>
        /// <returns></returns>
        public double LookUp(double timeD)
        {
            double lookUpVal = MissingValueReplacement;
            if (TbufferD.All(x => x.Equals(MissingValueReplacement)))
            {
                return MissingValueReplacement;
            }

            if (timeD >= GetLastTime())
            {
                return GetLastValue();
            }
            int[] r;
            //traverse backwards through times to find closest matching time
            // Create a vector, containing the indexes backwards
            // Create vector from the end
            if (_posBufferLastWriteIdx == BufferSizeSamples - 1) 
            {
                r = Vector.Range(BufferSizeSamples - 1, 0, -1);
            }
            // Create vector from somewhere in the middle of the buffer, and concatenate. Example: [4,3,2,1,0] with [9,8,7,6,5]
            else
            {
                r = Vector.Range(_posBufferLastWriteIdx, -1, -1)
                    .Concat(Vector.Range(BufferSizeSamples - 1, _posBufferLastWriteIdx, -1))
                    .ToArray();
            }

            var ii = 0; // Start traversing backwards from the last write index
            while (TbufferD[r[ii]] > timeD)
            {
                ii += 1;
                if (ii > BufferSizeSamples - 1)
                    // The look up time is not in the buffer
                    return MissingValueReplacement;
            }

            // in case the time in the buffer equals look-up time
            if (TbufferD[r[ii]].Equals(timeD))
            {
                if (!Vbuffer[r[ii]].Equals(MissingValueReplacement))
                {
                    lookUpVal = Vbuffer[r[ii]];
                }
            }
            else //% need to interpolate between values
            {
                // check that the values in the buffer are not error values
                var currentT = TbufferD[r[ii]];
                var prevT = TbufferD[r[ii - 1]];
                if (currentT.Equals(MissingValueReplacement) || prevT.Equals(MissingValueReplacement))
                {
                    return MissingValueReplacement;
                }

                var dt = Diff(TbufferD,r[ii-1],r[ii]);
                var dV = Diff(Vbuffer,r[ii - 1],r[ii]);
                // interpolate value as V0+dV/dt*timeDiff
                lookUpVal = Vbuffer[r[ii - 1]] + (timeD - TbufferD[r[ii - 1]]) * dV / dt;
            }
            return lookUpVal;
        }
    }
}
