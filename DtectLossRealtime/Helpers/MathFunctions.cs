﻿#nullable enable
using System;
using System.Linq;
using DtectLoss.Common;
using static System.Double;


namespace DtectLossRealtime.Helpers
{
    public static class MathFunctions
    {
        /// <summary>
        /// 
        /// REMOVEOFFSET Remove abnormal changes typically found in pit volume
        /// yIn is a vector with the previous and current measurement,
        /// respectively.That is, yIn = [yPrev yCurrent].offsetIn is the offset
        /// used at previous time - step.If the change in yIn between the current
        /// and previous time-step is larger than the threshold, then the offset is
        /// updated.
        /// yOut =[];
        /// offsetOut =[];
        /// </summary>
        public static (double yOut, double offsetOut) RemoveOffset(double[] yIn, double offsetIn, double threshold, double flowIn = CommonConstants.MissingValueReplacement)
        {
            double yOut, offsetOut;
            const double errVal = CommonConstants.MissingValueReplacement;
            const double flowInLowLimLpm = 400;
            // Handle bad / missing meas: y(2) == errVal
            if (yIn[1].Equals(errVal))
            {
                yOut = errVal;
                offsetOut = offsetIn;
            }
            else
            {
                // compute yOut, disregarding the threshold for now
                yOut = yIn[1] + offsetIn;
                // maintain offset as is
                offsetOut = offsetIn;
                // compute difference between time - steps
                var yDiff = yIn.Diff();
                double updatedThreshold;
                // if the difference is larger than given threshold
                if (flowIn > flowInLowLimLpm)
                    updatedThreshold = threshold / 2.0;
                else
                    updatedThreshold = threshold;
                if (Math.Abs(yDiff[0]) > updatedThreshold)
                {
                    // update offset
                    offsetOut = offsetIn - yDiff[0];
                    // update yOut
                    yOut = yIn[1] + offsetOut;
                }
            }
            return (yOut, offsetOut);
        }


        // From: https://stackoverflow.com/questions/17506343/how-can-i-write-the-matlab-filter-function-myself
        public static double[] Filter1D(double[] b, double[] a, double[] x)
        {
            var a1 = GetRealArrayScalarDiv(a, a[0]);
            var b1 = GetRealArrayScalarDiv(b, a[0]);
            var sx = x.Length;
            var filter = new double[sx];
            filter[0] = b1[0] * x[0];
            for (var i = 1; i < sx; i++)
            {
                filter[i] = 0.0;
                for (var j = 0; j <= i; j++)
                {
                    var k = i - j;
                    if (j > 0)
                    {
                        if ((k < b1.Length) && (j < x.Length))
                        {
                            filter[i] += b1[k] * x[j];
                        }
                        if ((k < filter.Length) && (j < a1.Length))
                        {
                            filter[i] -= a1[j] * filter[k];
                        }
                    }
                    else
                    {
                        if ((k < b1.Length) && (j < x.Length))
                        {
                            filter[i] += (b1[k] * x[j]);
                        }
                    }
                }
            }
            return filter;
        }

        private static double[] GetRealArrayScalarDiv(double[] dDividend, double dDivisor)
        {
            if (dDividend == null)
            {
                throw new ArgumentException("The array must be defined or diferent to null");
            }
            if (dDividend.Length == 0)
            {
                throw new ArgumentException("The size array must be greater than Zero");
            }
            var dQuotient = new double[dDividend.Length];

            for (var i = 0; i < dDividend.Length; i++)
            {
                if (!(dDivisor == 0.0))
                {
                    dQuotient[i] = dDividend[i] / dDivisor;
                }
                else
                {
                    if (dDividend[i] > 0.0)
                    {
                        dQuotient[i] = PositiveInfinity;
                    }
                    if (dDividend[i] == 0.0)
                    {
                        dQuotient[i] = NaN;
                    }
                    if (dDividend[i] < 0.0)
                    {
                        dQuotient[i] = NegativeInfinity;
                    }
                }
            }
            return dQuotient;
        }
        public static double[] Diff(this double[] data)
        {
            if (data.Length <= 1)
                return new double[] {};
            var result = new double[data.Length - 1];
            for (var k = 0; k < data.Length - 1; k++)
            {
                result[k] = data[k + 1] - data[k];
            }
            return result;
        }

        public static double Diff(double[] data, int idxOld, int idxNew)
        {
            return data[idxNew] - data[idxOld];
        }

        //interp1 1-D interpolation(table lookup)
        //    Vq = interp1(X, V, Xq) interpolates to find Vq, the values of the
        //     underlying function V = F(X) at the query points Xq.
        public static double[] Interp1(double[] x, double[] v, double[] xq)
        {
            var retArray =
                Enumerable.Range(0, xq.Length)
                    .Select(k => Accord.Math.Tools.Interpolate1D(xq[k], x, v, v[0], v[^1]))
                    .ToArray();
            return retArray;
        }

        public static double[] Interp1Previous(double[] x, double[] v, double[] xq)
        {
            var retArray = new double[xq.Length];

            var lastX = x[^1];
            //Xq is a time value. Find the index for the lower time in X and use the value from V
            for (var k = 0; k < xq.Length; k++)
            {
                var idx = Array.FindLastIndex(x, t => xq[k] >= t);
                if (idx >= 0)
                {
                    if (xq[k] <= lastX)
                    {
                        retArray[k] = v[idx];
                    }
                    else
                    {
                        retArray[k] = NaN;
                    }
                }
                else
                {
                    retArray[k] = NaN;
                }
            }
            return retArray;
        }

        // get variance of array
        public static double Variance(this double[] array)
        {
            var result = Accord.Statistics.Measures.Variance(array);
            if (double.IsNaN(result))
            {
                return 0;
            }
            return result;
        }

        public static int ToInt(this bool input)
        {
            return input ? 1 : 0;
        }
    }
}
