﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DtectLoss.Common;
using Microsoft.Extensions.Logging;
using static DtectLoss.Common.Helpers;

namespace DtectLossRealtime.Helpers
{
    public interface IDefaultUnitsMappingReader
    {
        Dictionary<string, VariableUnitMapping> ReadDefaultUnitsMapping();
    }

    public class DefaultUnitsMappingReader : IDefaultUnitsMappingReader
    {
        private ILogger<DefaultUnitsMappingReader> _logger;

        public DefaultUnitsMappingReader(ILogger<DefaultUnitsMappingReader> logger)
        {
            _logger = logger;
        }

        public Dictionary<string, VariableUnitMapping> ReadDefaultUnitsMapping()
        {
            // The file is located in app root/data and linked+copied into application on build
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "defaultUnitsMapping.csv");
            using var fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
            var lines = new List<string[]>();
            using (var reader = new StreamReader(fileStream))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    lines.Add(line.Split(';'));
                    
                }
            }
            return ToVariableUnitMappings(lines);
        }

        private static Dictionary<string, VariableUnitMapping> ToVariableUnitMappings(List<string[]> lines)
        {
            var result =
                lines.Skip(1)
                    .Select(x =>
                        new
                        {
                            VariableName = x.FirstOrDefault(),
                            DefaultUnit = x.Skip(1).FirstOrDefault(),
                            UnitFactors =
                                x.Skip(2)
                                    .Zip(lines.FirstOrDefault().Skip(2), (factor, unit) => (factor, unit))
                                    .Where(e => !string.IsNullOrEmpty(e.factor))
                            .ToDictionary(f => f.unit.Replace("/", "p"), v => ParseValue(v.factor))
                        })
                    .ToDictionary(k => k.VariableName,
                        v => new VariableUnitMapping {DefaultUnit = v.DefaultUnit, UnitFactors = v.UnitFactors});
            return result;
        }
    }
}
