﻿#nullable enable
using System.Collections.Generic;
using System.Linq;

namespace DtectLossRealtime.Helpers
{
    // this class is intended to make it easier to switch matlab->c# by defining 
    // static helper functions which match 1:1 with matlab syntax.
    public static class Vec
    {
        public static Dictionary<string, string> CreateDictionaryFromEvenOddList(this string[] values) =>
            values.Where((s, i) => i % 2 == 0).Zip(values.Where((s, i) => i % 2 != 0), (k, v) => (k, v))
                .ToDictionary(t => t.k, t => t.v);

        public static Dictionary<string, bool> LogicAnd(this Dictionary<string, bool> first,
            Dictionary<string, bool> second) =>
            first.Keys
                .ToDictionary(k => k, v => second.ContainsKey(v) && (first[v] && second[v]));

        public static bool[] LogicAnd(this IEnumerable<bool> first, IEnumerable<bool> second) =>
            first
                .Zip(second, (a1, a2) => (a1, a2))
                .Select(x => x.a1 && x.a2)
                .ToArray();

        public static Dictionary<string, bool> LogicNot(this Dictionary<string, bool> values) =>
            values.Keys
                .ToDictionary(k => k, v => !values[v]);

        public static bool HasTrueValue(this Dictionary<string, bool> values, string key) =>
            values.ContainsKey(key) && values[key];

        public static T[] Subset<T>(this T[] values, bool[] boolValues) =>
            values
                .Where((v, i) => i < boolValues.Length && boolValues[i])
                .ToArray();

        public static IEnumerable<T> SubsetAsEnumerable<T>(this T[] values, bool[] boolValues) =>
            values
                .Where((v, i) => i < boolValues.Length && boolValues[i]);
        
        public static IEnumerable<T> SubsetAsEnumerable<T>(this IEnumerable<T> values,
            Dictionary<T, bool> boolValues) =>
            values
                .Where(x => boolValues.ContainsKey(x) && boolValues[x]);
        
        public static IEnumerable<T> SubsetOnlyFalseValues<T>(this Dictionary<T, bool> values) =>
            values.Where(x => !x.Value).Select(x => x.Key);
    }
}
