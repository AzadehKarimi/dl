﻿using System.Linq;
using DtectLoss.Common;
using DtectLossRealtime.Helpers;
using static System.Math;
using static DtectLoss.Common.CommonConstants;

namespace DtectLossRealtime
{
    /// <summary>
    /// Treat as downlink if pressure drops at least 20 bar with a few seconds,
    /// then goes at least 20 bar back up again in a few more seconds, let flag
    /// stay high until downlinking is done.
    /// </summary>
    public class DownLinkDetect
    {
        public bool IsDownlinking { get; private set; }
        private bool _isPumpProbablyRamping;
        private readonly RingBuffer _presSpRingBuffer;
        private readonly RingBuffer _flowInRingBuffer;
        private double _prevD;
        private double _presSpMBarPrev;
        private double _timeOfLastPosPresSpChangeAboveThresholdD;
        private double _timeOfLastNegPresSpChangeAboveThresholdD;
        private double _deltaPDeltaT;

        private const int BufferSizeSamples = 10;
        private const double IsdownlinkingThresholdPressureChangeBar = 20;
        private const double IsdownlinkingThresholdPressureDerivativeBarPerSec = 10 / 3.0;
        private const double IsdownlinkingTimeLimitBetweenUpAndDownSpikesSec = 20;
        private const double IsdowlinkingResetWhenDeltapInBufferBelowThreshold = 5;
        private const double FlowrateRampThresholdLpm = 200;
        private const double PressureRampThresholdBar = 15;

        public DownLinkDetect()
        {
            _presSpRingBuffer = new RingBuffer(BufferSizeSamples);
            _flowInRingBuffer = new RingBuffer(BufferSizeSamples);
            IsDownlinking = false;
            _presSpMBarPrev = 0;
            _prevD = 0;
            _timeOfLastPosPresSpChangeAboveThresholdD = 0;
            _timeOfLastNegPresSpChangeAboveThresholdD = 0;
        }

        public void Step(double presSpMBar, double tD, double flowInLpm = MissingValueReplacement)
        {
            var isMissingFlowInMeas = false;
            if(flowInLpm < 0)
            {
                flowInLpm = 0;
                isMissingFlowInMeas = true;
            }

            _presSpRingBuffer.AddDataToCircularBuffer(tD, presSpMBar);
            _flowInRingBuffer.AddDataToCircularBuffer(tD, flowInLpm);
            var deringbuffer = _presSpRingBuffer.Deringbuffer();
            var presSpVec = deringbuffer.VbufferStraight;
            var deringBufferResult = _flowInRingBuffer.Deringbuffer();
            var flowInVec = deringBufferResult.VbufferStraight;
            if (IsDownlinking == false)
            {
                if (!isMissingFlowInMeas)
                {
                    var (isFlowInIncreasing, isFlowInDecreasing) = CheckIfChanging(flowInVec, FlowrateRampThresholdLpm);
                    var (isPresSpIncreasing, isPresSpDecreasing) = CheckIfChanging(presSpVec, PressureRampThresholdBar);

                    _isPumpProbablyRamping = isFlowInIncreasing && isPresSpIncreasing || isFlowInDecreasing && isPresSpDecreasing;
                }
                else
                    _isPumpProbablyRamping = false;

                if (_isPumpProbablyRamping || presSpVec.Max() < 50)
                    return;

                if (tD != _prevD)
                {
                    var dTs = tD.DiffInSeconds(_prevD);
                    _deltaPDeltaT = (presSpMBar - _presSpMBarPrev) / dTs;
                }
                else
                    _deltaPDeltaT = 0;

                var pressureAbsChangeAboveThreshold = (presSpVec.Max() - presSpVec.Min()) > IsdownlinkingThresholdPressureChangeBar;
                if (_deltaPDeltaT > IsdownlinkingThresholdPressureDerivativeBarPerSec)
                    _timeOfLastPosPresSpChangeAboveThresholdD = tD;

                if (_deltaPDeltaT < -IsdownlinkingThresholdPressureDerivativeBarPerSec)
                    _timeOfLastNegPresSpChangeAboveThresholdD = tD;

                var timeBtwUpAndDownSPikesS = Max(_timeOfLastPosPresSpChangeAboveThresholdD, _timeOfLastNegPresSpChangeAboveThresholdD).DiffInSeconds(Min(_timeOfLastPosPresSpChangeAboveThresholdD, _timeOfLastNegPresSpChangeAboveThresholdD));

                var spikesInBothDirectionWithinTimeLimit = timeBtwUpAndDownSPikesS < IsdownlinkingTimeLimitBetweenUpAndDownSpikesSec;
                var didSpikesHappenRecently = tD.DiffInSeconds(_timeOfLastPosPresSpChangeAboveThresholdD) < IsdownlinkingTimeLimitBetweenUpAndDownSpikesSec;
                if (pressureAbsChangeAboveThreshold && spikesInBothDirectionWithinTimeLimit && didSpikesHappenRecently && !_isPumpProbablyRamping)
                    IsDownlinking = true;
            }
            else
            {
                if (presSpVec.Max() - presSpVec.Min() < IsdowlinkingResetWhenDeltapInBufferBelowThreshold)
                    IsDownlinking = false;
            }
            _presSpMBarPrev = presSpMBar;
            _prevD = tD;
        }

        public static (bool isIncreasing, bool isDecreasing) CheckIfChanging(double[] vec, double threshold)
        {
            var isIncreasing = false;
            var isDecreasing = false;
            if (!vec.Any() || vec.Length < 2)
            {
                return (false, false);
            }
            if (vec.Last() - vec.First() > threshold)
            {
                isIncreasing = true;
            }
            else if (vec.First() - vec.Last() > threshold)
            {
                isDecreasing = true;
            }
            return (isIncreasing, isDecreasing);
        }
    }
}
