﻿using System;
using System.Collections.Generic;
using System.Text;
using Accord.Math;

namespace DtectLossRealtime
{
    /// <summary>
    /// Define case for simulation
    /// </summary>
    public class SimulatorCaseDef
    {
        //const double g_ms2 = 9.81;
        //const double beta_bar = 15000; //15000
        //const double Tref_s = 10;    //s smoothen reference
        //const double MWsteel_sg = 7.859;          //steel density sg
        //const double Es_Pa = 200e9;               //Pa Youngs modulus elasticity
        //const double MWmud_sg = 1.31;
        //const double PVmud_cP = 17;   //17;
        //const double YPmud_lb100sqft = 23; //23;
        //const double tauymud_Pa = 7; //7
        //const double alpha_fl = 1; //0.4; // To calculate flow from flow line to pit
        //const double IDCs_in = 8.5;//10.5; in
        //const double ODdp_in = 5.5;        //in
        //const double IDdp_in = 4.778;      //in
        //const double tune_delay_wob_s = 10;
        //const double tune_delay_pos_s = 0.5;
        //const double shakerloss_frac = 0.005; //Losing 10 lpm over shakers when pumping 2000 lpm 

        //// Init states
        //const double holeDepth_m = 3000; //3000
        //const double blockPos_m = 2;
        //const double q_drilling_lpm = 2000;
        //const double q_gelbreak_lpm = 500;
        //const double timegelbreak_s = 60;
        //const double q_tripping_lpm = 1200; // trip tank pump flowrate
        //const double V_fl_m3 = 2;    // inital volume in the flowline during drilling(used to calculate K_fl_m3_per_lpm)
        //const double V_pit_m3 = 5;
        //const double V_tt_m3 = 3;    // partially full trip tank


        //const double rotation_rpm = 150;  // drill pipe rotation
        //const double rop_mph = 30;        // nominal rop
        //const double F_wob_ton = 4;    // nominal wob
        //const double dp_bit_bar = 30;   // friction loss bit at nominal flow
        //const double cdrag_ton_mph = 30/(50*60);  // 30 ton drag when tripping 50m/min
        //const double vel_max_trip_mph = 10 * 60;     // m/hr max trip vel
        //const double vel_max_rop_mph = 50;          // m/hr max rop
        //double pcutdot = 10 / Math.Sqrt(3600);      // 10 bar/h =? 10/3600 bar/s

        //public double D_ann_m;
        //public double D_dpInner_m;
        //public double D_dpOuter_m;
        //public double Aidp_m2;
        //public double Aann_m2;
        //public double Asdpopen_m2;
        //public double Asdpclosed_m2;
        //public double Vidp_m3;
        //public double Vann_m3;
        //public double Vtot_m3;
        //public double Mdp_kgpm4;
        //public double Mann_kgpm4;
        //public double Mtot_kgpm4;
        //public double cbit_bar_lpm2;
        //public double d_ton_mph;
        //public double ks_ton;
        //public double mdry_ton;
        //public double mwet_ton;
        //public double kpit_lpm_m3;
        //public double kshaker_lpm_m3;
        //public double dbdV_barm3;
        //public double Twob_s;
        //public double Tpos_s;
        //public double Tcwob_s;
        //public double Tcpos_s;
        //public double Kcwob;
        //public double Tiwob_s;
        //public double Kcpos;
        //public double Tipos_s;

        //public int NumStands;
        //public double simulationDuration_s;

        //public SimDrillingStates[] StateVector = new[]
        //{
        //    SimDrillingStates.Drilling,
        //    SimDrillingStates.PullOffBottom,
        //    SimDrillingStates.StopRotation,
        //    SimDrillingStates.StopPumps,
        //    SimDrillingStates.DrillAddStand,
        //    SimDrillingStates.StartPumps,
        //    SimDrillingStates.StartRotation,
        //    SimDrillingStates.GoOnBottom,
        //    SimDrillingStates.Drilling,
        //    SimDrillingStates.PullOffBottom,
        //    SimDrillingStates.StopRotation,
        //    SimDrillingStates.StopPumps,
        //    SimDrillingStates.DrillAddStand,
        //    SimDrillingStates.StartPumps,
        //    SimDrillingStates.StartRotation,
        //    SimDrillingStates.GoOnBottom,
        //    SimDrillingStates.Drilling,
        //    SimDrillingStates.PullOffBottom,
        //    SimDrillingStates.StopRotation,
        //    SimDrillingStates.StopPumps,
        //    SimDrillingStates.Wait,
        //    SimDrillingStates.Pull1Stand,
        //    SimDrillingStates.TripRemoveStand,
        //    SimDrillingStates.Pull1Stand,
        //    SimDrillingStates.TripRemoveStand,
        //    SimDrillingStates.Pull1Stand,
        //    SimDrillingStates.TripRemoveStand,
        //    SimDrillingStates.Pull1Stand,
        //    SimDrillingStates.TripRemoveStand,
        //    SimDrillingStates.Pull1Stand,
        //    SimDrillingStates.FlowCheck,
        //    SimDrillingStates.Run1Stand,
        //    SimDrillingStates.TripAddStand,
        //    SimDrillingStates.Run1Stand,
        //    SimDrillingStates.TripAddStand,
        //    SimDrillingStates.Run1Stand,
        //    SimDrillingStates.TripAddStand,
        //    SimDrillingStates.Run1Stand,
        //    SimDrillingStates.TripAddStand,
        //    SimDrillingStates.Run1Stand,
        //    SimDrillingStates.FlowCheck,
        //    SimDrillingStates.EndSimulation
        //};

        //public double [,] InfluxVecTime_s;
        //public double [] InfluxVecRate_lpm;
        //public double [,] DeltaQinVecTime_s;
        //public double [] DeltaQinVecRate_lpm;
        //public double StepPitTime_s;
        //public double StepPitDV_m3;
        //public double [] PaddleVec_lpm;
        //public double [] PaddleVec_pc;
        //public double q_out_noise_lpm;
        //public double qpaddle_accuracy_pc; // +-5% accuracy
        //public SimDrillingStates drillState;   //StateVector


        //public void Initialize()
        //{
        //    NumStands = 10;
        //    simulationDuration_s = 900 * 60; // 900 * 60;
            
        //    InfluxVecTime_s = new double [,] { { 580, 586 }, { 660, 672 }, { 720, 735 }, { 765, 769 }, { 800, 808 }, { 830, 842 } }.Multiply(60);
        //    InfluxVecRate_lpm = new double [] {-400, 300, +100, 700, 300, -200};
        //    DeltaQinVecTime_s = new double [,] {{90, 100}, {110, 120}, {160, 180}, {230, 250}, {300, 315}, {380, 390}}.Multiply(60);
        //    DeltaQinVecRate_lpm = new double[] {400, 200, 800, 400, -300, 500};
        //    StepPitTime_s = 700 * 60;
        //    StepPitDV_m3 = 5;
        //    PaddleVec_lpm = new double[] { 0, 400, 1000, 3000, 5000, 8000 };
        //    PaddleVec_pc = new double[] { 0, 0, 24, 83, 100, 100 };
        //    q_out_noise_lpm = 50;
        //    qpaddle_accuracy_pc = 5; // +-5 % accuracy
        //    drillState = SimDrillingStates.Drilling; // StateVector; //


        //    // Const calc
        //    D_ann_m = IDCs_in * 0.0254;
        //    D_dpInner_m = IDdp_in * 0.0254;
        //    D_dpOuter_m = ODdp_in * 0.0254;
        //    Aidp_m2 = Math.PI / 4 * Math.Pow(D_dpInner_m, 2);
        //    Aann_m2 = Math.PI / 4 * (Math.Pow(D_ann_m, 2) - Math.Pow(D_dpOuter_m, 2)); // Annulus area
        //    Asdpopen_m2 =
        //        Math.PI / 4 *
        //        (Math.Pow(D_dpOuter_m, 2) - Math.Pow(D_dpInner_m, 2)); // drillpipe cross section area open end
        //    Asdpclosed_m2 = Math.PI / 4 * (Math.Pow(D_dpOuter_m, 2)); // drillpipe cross section area closed end
        //    Vidp_m3 = Aidp_m2 * holeDepth_m;
        //    Vann_m3 = Aann_m2 * holeDepth_m;
        //    Vtot_m3 = (Aidp_m2 + Aann_m2) * holeDepth_m;
        //    Mdp_kgpm4 = MWmud_sg * 1000 * holeDepth_m / Aidp_m2; // "mass" mud in dp for momentum balance

        //    Mann_kgpm4 = MWmud_sg * 1000 * holeDepth_m / Aann_m2;
        //    Mtot_kgpm4 = holeDepth_m * MWmud_sg * 1000 * (1 / Aidp_m2 + 1 / Aann_m2);
        //    cbit_bar_lpm2 = dp_bit_bar / Math.Pow(q_drilling_lpm, 2); // bar / lpm ^ 2
        //    d_ton_mph = F_wob_ton / rop_mph; // Constant for linear relation rop vs wob

        //    ks_ton = Es_Pa * Asdpopen_m2 / g_ms2 / 1000; // ton string constant ton Youngs modulus steel 200 GPa
        //    mdry_ton = holeDepth_m * Asdpopen_m2 * MWsteel_sg; // dry weight of drill pipe(steel)
        //    mwet_ton = holeDepth_m * Asdpopen_m2 *
        //               (MWsteel_sg - MWmud_sg); // wet weight of drill pipe = dry weight - buoyancy
        //    var k = q_drilling_lpm / Math.Pow(V_fl_m3, alpha_fl);
        //    kpit_lpm_m3 = k * (1 - shakerloss_frac);
        //    kshaker_lpm_m3 = k * shakerloss_frac;

        //    dbdV_barm3 = beta_bar / Vtot_m3;
        //    Twob_s = (d_ton_mph + cdrag_ton_mph) * holeDepth_m / (ks_ton) *
        //             3600; // Open loop time constant wob control sec
        //    Tpos_s = cdrag_ton_mph * holeDepth_m / ks_ton * 3600; // Open loop time constant vel control sec
        //    Tcwob_s = 1 * Twob_s; // Closed loop time const wob control
        //    Tcpos_s = 0.2 * Tpos_s; // Closed loop time constant pos control

        //    // SIMC tuning rules
        //    Kcwob = 1 / (d_ton_mph + cdrag_ton_mph) * Twob_s / (Tcwob_s + tune_delay_wob_s);
        //    Tiwob_s = Math.Min(Twob_s, 4 * (Tcwob_s + tune_delay_wob_s));
        //    Kcpos = 1 / (Tcpos_s + tune_delay_pos_s);
        //    Tipos_s = 4 * (Tcpos_s + tune_delay_pos_s);

        //    // StateVector =[];
        //    SimDrillingStates [] Drill1Stand = new[]
        //    {
        //        SimDrillingStates.Drilling,       //1
        //        SimDrillingStates.PullOffBottom,  // 2
        //        SimDrillingStates.StopRotation,   // 3
        //        SimDrillingStates.StopPumps,      // 4
        //        SimDrillingStates.DrillAddStand,   //5
        //        SimDrillingStates.StartPumps,      // 6
        //        SimDrillingStates.StartRotation,    //7
        //        SimDrillingStates.GoOnBottom      // 8
        //        };
            
        //    SimDrillingStates [] DrillAndWait = new []
        //    {
        //        SimDrillingStates.Drilling,   // 9
        //        SimDrillingStates.PullOffBottom,  //10
        //        SimDrillingStates.StopRotation,   //11
        //        SimDrillingStates.StopPumps,      // 12
        //        SimDrillingStates.Wait          // 13

        //};

        //    for (int i = 1; i <= NumStands; i++)
        //    {
        //        StateVector [i] = Vec.ConcatVec(StateVector, Drill1Stand); //{StateVector[i], Drill1Stand}; //TODO: TO ASK how handle it in here
        //    }
              
        //   StateVector = Vec.ConcatVec(StateVector, DrillAndWait);

        //    for (int i = 1; i <= NumStands; i++)
        //    {
        //        StateVector = [StateVector, 
        //        SimDrillingStates.Pull1Stand,   // 14 //TODO: TO ASK how handle it in here
        //        SimDrillingStates.TripRemoveStand];
        //    }

        //    SimDrillingStates [] PullStandTripRemoveStand = new SimDrillingStates[] { SimDrillingStates.Pull1Stand, SimDrillingStates.TripRemoveStand };
        //    StateVector = Vec.ConcatVec(StateVector, PullStandTripRemoveStand);
        //    //StateVector = [StateVector, 
        //    //     SimDrillingStates.Pull1Stand,     // 22
        //    //     SimDrillingStates.FlowCheck];     // 23

        //    for (int i = 1; i <= NumStands; i++)
        //    {
        //        StateVector = [StateVector, 
        //        SimDrillingStates.Run1Stand,      // 24 //TODO: TO ASK how handle it in here
        //        SimDrillingStates.TripAddStand];  // 25
        //    }

        //    SimDrillingStates[] Run1StandFlowCheckEndSimulation = new SimDrillingStates[] { SimDrillingStates.Run1Stand, SimDrillingStates.FlowCheck, SimDrillingStates.EndSimulation };
        //    StateVector = Vec.ConcatVec(StateVector, Run1StandFlowCheckEndSimulation);
        //    //StateVector = new [,] {{StateVector},
        //    //    {SimDrillingStates.Run1Stand},      // 32
        //    //    {SimDrillingStates.FlowCheck},      // 33
        //    //    {SimDrillingStates.EndSimulation}   // 34
        //    //    };

        //}
    }
}
