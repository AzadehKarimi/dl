﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DtectLossRealtime.Helpers;

namespace DtectLossRealtime //TODO: 
{
    /// <summary>
    /// DETECT_OBJ Summary of this class goes here
    /// Detailed explanation goes here
    /// Use washed drilling data
    /// Run EKD algorithm to detect inlfuxes and losses
    /// Calculate relative compressibility dbdV_e_barm3, influx/loss rate
    /// qIo_lpm, influx/loss volume, warning/alarm
    /// Save “detection” results data to csv file.
    /// </summary>
    public class Detect
    {
        public double numDataPoints;
        public double avg_dt_s;
        public double flowDeadband_lpm;
        public double volumeThresholdWarning_m3;
        public double volumeThresholdAlarm_m3;
        RingBuffer buffer;

        public Detect(double flowDeadband_lpm, double volumeThresholdWarning_m3, double volumeThresholdAlarm_m3)
        {
            this.flowDeadband_lpm = flowDeadband_lpm;
            this.volumeThresholdWarning_m3 = volumeThresholdWarning_m3;
            this.volumeThresholdAlarm_m3 = volumeThresholdAlarm_m3;
        }

        public void initialize(Process process) // Constructor
        {
            //numDataPoints = process.numDataPoints;
            //buffer.t_s = Enumerable.Repeat(Double.NaN, (int)numDataPoints).ToArray();
            //buffer.sys_states = Enumerable.Repeat(Double.NaN, (int)numDataPoints).ToArray();
            //buffer.dbdV_e_barm3 = Enumerable.Repeat(Double.NaN, (int)numDataPoints).ToArray();
            //buffer.dpsp_dt_bar = Enumerable.Repeat(Double.NaN, (int)numDataPoints).ToArray();
            //buffer.qIo_pdiff_lpm = Enumerable.Repeat(Double.NaN, (int)numDataPoints).ToArray();
            //buffer.qIo_nodiff_lpm = Enumerable.Repeat(Double.NaN, (int)numDataPoints).ToArray();
            //buffer.VIo_pdiff_m3 = Enumerable.Repeat(Double.NaN, (int)numDataPoints).ToArray();
            //buffer.VIo_nodiff_m3 = Enumerable.Repeat(Double.NaN, (int)numDataPoints).ToArray();
            //buffer.alarm_nodiff = Enumerable.Repeat(Double.NaN, (int)numDataPoints).ToArray();
            //buffer.alarm_pdiff = Enumerable.Repeat(Double.NaN, (int)numDataPoints).ToArray();
            //avg_dt_s = process.avg_dt_s;
        }

        public void Runpdiff(Process process, double pdiff, double pdiff_noP)
        {
           //const double qin_lp_Tc = 1;
           //var qin_lp = new LowPass(avg_dt_s, qin_lp_Tc);

           //double VIo_pdiff_m3 = 0;
           //double VIo_nodiff_m3 = 0;
           //double alarm_pdiff = 0;
           //double alarm_nodiff = 0;

           // int i = 0;
           // while (i < numDataPoints) // run EKD algorithm(iMPD inspired -delta flow only)
           // {
           //     pdiff = pdiff.stepIt(process.buffer.q_p_lpm(i), process.buffer.q_out_lpm(i), process.buffer.p_p_bar(i), process.buffer.sys_states(i));
           //     pdiff_noP = pdiff_noP.stepIt(process.buffer.q_p_lpm(i), process.buffer.q_out_lpm(i), process.buffer.p_p_bar(i), process.buffer.sys_states(i));


           //     // To be improved
           //     if (Math.Abs(pdiff.qIo_lpm) > flowDeadband_lpm)
           //         VIo_pdiff_m3 = VIo_pdiff_m3 + avg_dt_s / 60000 * pdiff.qIo_lpm;  // To be fixed

           //        if (Math.Abs(VIo_pdiff_m3) > volumeThresholdAlarm_m3)
           //         alarm_pdiff = 2;
           //     else if (Math.Abs(VIo_pdiff_m3) > volumeThresholdWarning_m3)
           //         alarm_pdiff = 1;
           //     else
           //         alarm_pdiff = 0;
              
           //     // To be improved
           //     if (Math.Abs(pdiff_noP.qIo_lpm) > flowDeadband_lpm)
           //         VIo_nodiff_m3 = VIo_nodiff_m3 + avg_dt_s / 60000 * pdiff_noP.qIo_lpm;  // To be fixed

           //     if (Math.Abs(VIo_nodiff_m3) > volumeThresholdAlarm_m3)
           //         alarm_nodiff = 2;
           //     else if (Math.Abs(VIo_nodiff_m3) > volumeThresholdWarning_m3)
           //         alarm_nodiff = 1;
           //     else
           //         alarm_nodiff = 0;
              
           //     // //
           //     buffer.t_s[i] = process.buffer.t_s[i];
           //     buffer.sys_states[i] = process.buffer.sys_states[i];
           //     buffer.dbdV_e_barm3[i] = pdiff.betaDivByV_e_bar_per_m3;
           //     buffer.dpsp_dt_bar[i] = pdiff.dpsp_dt_bar;
           //     buffer.qIo_pdiff_lpm[i] = pdiff.qIo_lpm;
           //     buffer.qIo_nodiff_lpm[i] = pdiff_noP.qIo_lpm;
           //     buffer.VIo_pdiff_m3[i] = VIo_pdiff_m3;
           //     buffer.VIo_nodiff_m3[i] = VIo_nodiff_m3;
           //     buffer.alarm_pdiff[i] = alarm_pdiff;
           //     buffer.alarm_nodiff[i] = alarm_nodiff;
           //     i = i + 1;
           // }
        }

        public void PlotDetectionResults() // TODO: Plotting feature : to get back to this at later stage
        {
            //showlegend = false;
            //tm = obj.buffer.t_s / 60;
            //tm = tm - tm(1);
            //global fignr;
            //figure(fignr); fignr = fignr + 1;
            //ax(1) = subplot(321);
            //plot(tm, obj.buffer.sys_states, 'LineWidth', 2)
            //ylabel('System state')
            //legend('State')
            //title('Drill(1);FCheck(2);Cond(3);Trip(4)')
            //grid
            //ax(2) = subplot(322);
            //plot(tm, obj.buffer.dbdV_e_barm3, 'LineWidth', 2)
            //ylabel('beta/Vol [bar/m3]')
            //legend('Est beta/Vol')
            //grid
            //ax(3) = subplot(323);
            //plot(tm, obj.buffer.qIo_pdiff_lpm, tm, obj.buffer.qIo_nodiff_lpm, 'LineWidth', 2)
            //ylabel('[lpm]')
            //legend('qIo pdiff', 'qIo nodiff')
            //grid
            //ax(4) = subplot(324);
            //plot(tm, obj.buffer.dpsp_dt_bar, 'LineWidth', 2)
            //ylabel('[bar/sec]')
            //legend('dppdt')
            //grid
            //ax(5) = subplot(325);
            //plot(tm, obj.buffer.VIo_pdiff_m3, tm, obj.buffer.VIo_nodiff_m3, 'LineWidth', 2)
            //ylabel('[m3]')
            //legend('VIo pdiff', 'VIo nodiff')
            //grid
            //ax(6) = subplot(326);
            //plot(tm, obj.buffer.alarm_pdiff, tm, obj.buffer.alarm_nodiff, 'LineWidth', 2)
            //title('0=normal,1=warning,2=alarm')
            //ylabel('Alarm')
            //legend('alarm pdiff', 'alarm nodiff')
            //grid
            //linkaxes(ax, 'x')
        }

        public void Writerescsvfile(string filename) // save res to csv file TODO: to writing to csv file - to fix later
        {
            //fHeader1 = 'TIME;SYSSTATE;dbdV_e_barm3;dpsp_dt_bar;qIo_pdiff_lpm;VIo_pdiff_m3;alarm_pdiff;qIo_nodiff_lpm;VIo_nodiff_m3;alarm_nodiff';
            //fHeader2 = 'sec;unitless;bar/m3;bar/s;lpm;m3;unitless;lpm;m3;unitless';

            //data =[
            //buffer.sys_states;
            //buffer.dbdV_e_barm3;
            //buffer.dpsp_dt_bar;
            //buffer.qIo_pdiff_lpm;
            //buffer.VIo_pdiff_m3;
            //buffer.alarm_pdiff;
            //buffer.qIo_nodiff_lpm;
            //buffer.VIo_nodiff_m3;
            //buffer.alarm_nodiff;
            //    ];
            //const double Ncol = 9;
            //Nrow = length(buffer.t_s) - 1;
            //// open a file for writing
            //fid = fopen(filename, 'w');
            //if (fid == -1)
            //    disp(['Cannot open file ' filename ' for writing']);
            //else
            //disp(['Saving to file ' filename]);
            //fprintf(fid, '%s\n', fHeader1);
            //fprintf(fid, '%s\n', fHeader2);
            //tvec = datevec(buffer.t_s / 24 / 3600);

            //for (int i = 1:Nrow)  // time
            //    fprintf(fid, '%4.0f-%02.0f-%02.0fT%02.0f:%02.0f:%02.0fZ;%.2f', tvec(i, 1), tvec(i, 2), tvec(i, 3), tvec(i, 4), tvec(i, 5), tvec(i, 6));
            //for (int j = 1:Ncol)   // var
            //    fprintf(fid, ';%.2f;', data(j, i));

            //fprintf(fid, '\n');
            //end
            //fclose(fid);
        }
    }
}
