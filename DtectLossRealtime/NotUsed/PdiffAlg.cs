﻿using System;
using DtectLoss.Common;
using DtectLossRealtime.Calculations;
using DtectLossRealtime.Enums;
using DtectLossRealtime.Models;

namespace DtectLossRealtime.EKDAlgoritm
{
    /// <summary>
    /// This DtectLoss algorithm class uses the derivative of the standpipe pressure
    /// "pdiff" in addition to the flow in and flow out in its gain/loss equation:
    /// 
    /// qio = (Va+Vd)/beta * d(p_sp)/dt + q_out-q_in 
    /// 
    /// note that the term T_well = (Va+Vd)/beta is identified from data in this case.
    /// 
    /// d(p_sp(t))/dt  is found from p_sp(t) - lowpass(p_sp(t)) 
    /// 
    /// The class uses a version of the bulk modulus estimation rutine 
    /// developed by Statoil for the IMPD project to find the V/beta parameter.
    /// 
    /// The algorithm does not include:
    /// the hysteresis/nonlinear viscocity of that has to be overwon during
    /// startup after a connection, will give a small false "kick" as flowIn is above zero while flowOut is not  
    /// 
    /// TODO: try to make another version of pdiff that
    /// - has a nonlinear function for (c0 + c1*q + c2*q^2) for calibrating "actual flow out" during a connection
    /// (try offline regression first), as there is often a steady state bias during
    /// connection of 100 lpm or more even when  accounting for the bias at full flow.
    /// - try to make a version that of pdiff that re-calibrates beta and V during each
    /// connection, and otherwise is off, as there is no information during
    /// "steady flow", consider turning off pdiff during normal drilling,as there
    /// is no information on pdot then. (maybe just have a threshold on pdot for running inverted sums?)
    /// -there is also a transient in flowOut when stopping the pumps->add a time
    /// delay or time-constatnt to account for this?
    /// </summary>
    public class PdiffAlg : IEKDAlgoritm
    {
        private double betaperVol_e_barm3;
        private double flowBias_lpm;
        private bool isOnBounds;

        private double flowIo_lpm;
        private double flowIoFromPres_lpm;
        private double flowIoFromRatesWBias_lpm;
        private double volIo_m3;
        private double volIoAcc_m3;
        private double dpsp_dt_bar;
        Voting parameters;             // tunable paramters struct releated to the algorithm
        private int samplesSinceStartedDrilling;
        private double doPcalc;
        private double volumeThreshold_m3;

        public AlarmState AlarmState => alarmState;

        private double dt_s;
        private string alarmStatus;
        private double flowOutExpected_lpm;
        private double timeCurrent_d;
        private double timeLast_d;

        LowPass lpPresSP;
        private AlarmState alarmState;

        const double lpmToM3ps = 1 / 60000;
        const double M3PsTolpm = 60000;
        const double TfiltPresSP_s = 20;       // standpipe pressure Filter time constant in seconds
        const double beta_divBY_V_min = 5000 / 200; // maximum value for beta/V
        const double beta_divBY_V_max = 50000 / 10; // minimum value for beta/V
        const double timeStep_max_s = 60; // if no new data within this dt_s, the reset object
        const double flowBiasMax_lpm = 500; // expect that you should have at least 10 lpm here

        public bool ResetAlarm { get; }
        public bool RecalibrateFlowOutNow { get; }

        public PdiffAlg()
        {
            
        }

        public void Initialize(Voting parameters, params double[] varargin) // doPcalc
        {
            double doPcalc_internal;
            this.parameters = parameters; 
            if (varargin.Length < 1)
                doPcalc_internal = 1;
            else
                doPcalc_internal = varargin[0];

            //Initialize(null);
            //Initialize(null, 3,2,3,4,5,6,7,8);
            flowIo_lpm = 0;
            volIo_m3 = 0;
            volIoAcc_m3 = 0;
            dpsp_dt_bar = 0;
            samplesSinceStartedDrilling = 0;
            doPcalc = doPcalc_internal;
            if (doPcalc == 1)
                betaperVol_e_barm3 = 1;
            else
                betaperVol_e_barm3 = Double.MaxValue;

            lpPresSP = new LowPass(null, TfiltPresSP_s);
            alarmState = 0;
            timeCurrent_d = 0;
            timeLast_d = 0;
            dt_s = 0;
            flowOutExpected_lpm = 0;
        }

        // determine if an alarm has occured
        public void SetAlarmState(StateMachine sysState)
        {
            if (sysState.CurrentState == DrillingStates.Drilling)
            {
                if (Math.Abs(volIo_m3) > parameters.VolumeThresholdAlarmM3)
                    alarmState = AlarmState.Alarm;
                else if (Math.Abs(volIo_m3) > parameters.VolumeThresholdWarningM3)
                    alarmState = AlarmState.Warning;
                else
                    alarmState = AlarmState.Normal;
            }
            else
                alarmState = AlarmState.Disabled;

            if (isOnBounds = true)
                alarmState = AlarmState.Disabled;
        }

        // this function is called for every vector of new data
        public AlgResult StepIt(WashedData washedData, AlgData algData, StateMachine sysState, double timeCurrentD, double dTs)
        {
            bool DO_RESET;
            this.timeCurrent_d = timeCurrentD;
            this.dt_s = dTs;  // (obj.timeCurrent_d - obj.timeLast_d) * 86400;
            timeLast_d = timeCurrent_d;
            var flowIn_lpm = washedData.Values[V.FlowIn_lpm];
            var flowOut_lpm = washedData.Values[V.FlowOut_lpm];
            var presSP_bar = washedData.Values[V.PresSP_bar];
            if (dt_s <= 0)  //do nothing
                alarmStatus = $"Do nothing, time step = { dt_s} sec";
            else if (dt_s > timeStep_max_s) // reset
            {
                alarmStatus = $"Reset, time step = { dt_s} sec";
                Initialize(parameters);
                alarmState = AlarmState.Disabled;
            }
            else if (flowOut_lpm.Equals(-0.99925)  || flowIn_lpm.Equals(-0.99925) || presSP_bar.Equals(-0.99925))
                alarmState = AlarmState.InputError;
            else // ok - step alg
            {
                if (sysState.CurrentState == DrillingStates.Drilling)
                {
                    samplesSinceStartedDrilling += 1;
                    // --------------pdot------------ -
                    lpPresSP.FilterForCustomDt(presSP_bar, dt_s, 0);
                    var presSP_lowpass_bar = lpPresSP.FilteredSignal;
                    if (samplesSinceStartedDrilling * dt_s > TfiltPresSP_s * 5)
                        dpsp_dt_bar = (presSP_bar - presSP_lowpass_bar.Value) / TfiltPresSP_s;
                    else
                        dpsp_dt_bar = 0;

                    if (doPcalc == 1)
                    {
                        if (samplesSinceStartedDrilling < 3)
                            DO_RESET = true;
                        else
                            DO_RESET = false;

                        // ------------------------------"inverted sums" from impd bulk modulus estimation.

                        // [betaperVol_e_barm3, flowBias_m3ps, isOnBounds] = invertedSums(double presSP_bar, double (flowOut_lpm - flowIn_lpm) * lpmToM3ps, 
                        // beta_divBY_V_max, beta_divBY_V_min, flowBiasMax_lpm* lpmToM3ps, duble dt_s, double TfiltPresSP_s, bool DO_RESET);
                        //  var flowBias_lpm = flowBias_m3ps * M3PsTolpm;  
                    }

                    // ----------------------------------------------------------------------------------

                    flowIoFromPres_lpm = 1 / betaperVol_e_barm3 * M3PsTolpm * dpsp_dt_bar;
                    flowIoFromRatesWBias_lpm = flowOut_lpm - flowIn_lpm + flowBias_lpm; // by definition flowOut-flowIn
                    flowIo_lpm = flowIoFromPres_lpm + flowIoFromRatesWBias_lpm;
                    if (Math.Abs(flowIo_lpm) > parameters.FlowDeadbandLpm)
                        volIo_m3 += dt_s * flowIo_lpm * lpmToM3ps; // vIo[m3] = dt[s] * qIo[lpm]
                    else
                        volIo_m3 = 0; // volIo is for sharp losses

                    volIoAcc_m3 += dt_s * flowIo_lpm * lpmToM3ps; // longer term trends, is not reset based on flowDeadband
                    SetAlarmState(sysState);
                    // disp(sprintf('dpsp_dt: %f  1/obj.betaDivByV_e_bar_per_m3 * dpsp_dt: %f', dpsp_dt_bar, 1 / betaDivByV_e_bar_per_m3 * dpsp_dt_bar));

                    flowOutExpected_lpm = flowIn_lpm - 1 / betaperVol_e_barm3 * M3PsTolpm * dpsp_dt_bar;
                    flowIo_lpm = flowOut_lpm - flowOutExpected_lpm;
                    if (Math.Abs(flowIo_lpm) > parameters.FlowDeadbandLpm)
                        volIo_m3 = volIo_m3 + dt_s * flowIo_lpm * lpmToM3ps; // vIo[m3] = dt[s] * qIo[lpm]
                    else
                        volIo_m3 = 0;

                    SetAlarmState(sysState);
                }
            }
            SetAlarmState(sysState);
            return ReturnResultStruct();
        }

        public AlgResult ReturnResultStruct()
        {
            var algResult = new AlgResult
            {
                FlowIOflowLpm = flowIo_lpm,
                FlowIoFromPresLpm = flowIoFromPres_lpm,
                FlowIoFromRatesWBiasLpm = flowIoFromRatesWBias_lpm,
                VolIoM3 = volIo_m3,
                AlarmState = alarmState,
                BetaperVolEBarm3 = betaperVol_e_barm3,
                FlowBiasLpm = flowBias_lpm,
                IsOnBounds = isOnBounds,
                FlowOutExpectedLpm = flowOutExpected_lpm
            };
            return algResult;
        }

        
    }
}
