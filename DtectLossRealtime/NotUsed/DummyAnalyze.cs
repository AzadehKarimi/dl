﻿using System;
using System.Collections.Generic;
using System.Text;
using DtectLossRealtime.Enums;

namespace DtectLossRealtime
{
    /// <summary>
    /// DUMMY_ANALYZE_OBJ Summary of this class goes here
    /// Detailed explanation goes here
    /// </summary>
    public class DummyAnalyze
    {
        public double flowDeadband;
        public double volumeThreshold;
        public int noOfAlarms;
        AlarmState alarmState;

        public DummyAnalyze(double qdb, double vdb)
        {
            flowDeadband = qdb;
            volumeThreshold = vdb;
            noOfAlarms = 0;
            alarmState = 0;     // 0 = no, 1 = yes
        }

        public void Analyze(double [] qIo, double [] vIo)
        {
            for (int ii = 0; ii < qIo.Length; ii++)
            {
                if (Math.Abs(vIo[ii]) > volumeThreshold)
                {
                    noOfAlarms = noOfAlarms + 1;
                    alarmState = AlarmState.Warning;
                }
                else
                    alarmState = AlarmState.Normal;
            }

        }


    }
}
