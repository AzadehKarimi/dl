﻿using System;
using DtectLoss.Common;
using DtectLossRealtime.Enums;
using DtectLossRealtime.Models;

namespace DtectLossRealtime.EKDAlgoritm
{
    /// <summary>
    /// this is a very simple DtectLoss algorithm that is intended 
     // to be just for illustration purposes and comparison.
    /// </summary>
    public class SimpleAlg : IEKDAlgoritm
    {
        double flowIo_lpm;
        double volIo_m3;

        public AlarmState AlarmState => alarmState;

        Voting Parameters;
        AlgResult _algResult = new AlgResult(); // TODO: Find where the "result" Object has been created 
        double timeCurrent_d;
        double timeLast_d;
        double dt_s;
        bool alarmStatus;
        private AlarmState alarmState;
        const double timeStep_max_s = 60;


        public bool ResetAlarm { get; }
        public bool RecalibrateFlowOutNow { get; }

        public SimpleAlg()
        {
            //params = new double[];
            //flowIo_lpm = new double[];
            //volIo_m3 = new double[];
            Initialize(ParameterValues.VotingParam, new double[] {});
        }
        public void Initialize(Voting votingParams, double[] pargIn)
        {
            flowIo_lpm = 0;
            volIo_m3 = 0;
            alarmState = AlarmState.Normal;
            Parameters = votingParams;
            timeCurrent_d = 0;
            timeLast_d = 0;
            dt_s = 0;
        }

       public AlgResult ReturnResultStruct()
        {
            _algResult = new AlgResult();
            _algResult.FlowIOflowLpm = flowIo_lpm;
            _algResult.VolIoM3 = volIo_m3;
            _algResult.AlarmState = alarmState;
            return _algResult;
        }

        

        public AlgResult StepIt(WashedData washedData, AlgData algData, StateMachine sysState, double timeCurrentD, double dTs)
        {
            this.timeCurrent_d = timeCurrentD;
            this.dt_s = dTs;
            
            timeLast_d = timeCurrent_d;
            if (dt_s <= 0) // do nothing
                Console.WriteLine($"Do nothing, time step = {dt_s} sec");
            else if (dt_s > timeStep_max_s) // reset
            {
                Console.WriteLine($"Reset, time step = {dt_s} sec");
                Initialize(Parameters, new double[] {});
                alarmState = AlarmState.Disabled;
            }

            else // ok - step alg
            {
                double flowIn_lpm = washedData.Values[V.FlowIn_lpm];
                double flowOut_lpm = washedData.Values[V.FlowOut_lpm];

                if (sysState.CurrentState == DrillingStates.Drilling)
                {
                    const double LITERS_TO_M3 = 1 / 1000;
                    const double MIN_TO_SEC = 1 / 60;
                    flowIo_lpm = flowOut_lpm - flowIn_lpm;
                    if (Math.Abs(flowIo_lpm) > Parameters.FlowDeadbandLpm)
                        volIo_m3 = volIo_m3 + dt_s * flowIo_lpm * LITERS_TO_M3 * MIN_TO_SEC; // vIo[m3] = dt[s] * qIo[lpm]
                    else
                        volIo_m3 = 0;
                }
                else
                {
                    flowIo_lpm = 0;
                    volIo_m3 = 0;
                }
                setAlarmState(sysState);
            }
            return ReturnResultStruct();
        }

        // determine if an alarm has occured
        public void setAlarmState(StateMachine sysState)
        {
            if (sysState.CurrentState == DrillingStates.Drilling)
            {
                if (Math.Abs(volIo_m3) > Parameters.VolumeThresholdAlarmM3)
                    alarmState = AlarmState.Alarm;

                else if (Math.Abs(volIo_m3) > Parameters.VolumeThresholdWarningM3)
                    alarmState = AlarmState.Warning;

                else
                    alarmState = AlarmState.Normal;


            }
            else
                alarmState = AlarmState.Disabled;
        }

    }












}


