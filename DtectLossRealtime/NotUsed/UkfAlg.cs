﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accord.Math;
using Accord.Math.Decompositions;
using DtectLoss.Common;
using DtectLossRealtime.Enums;
using DtectLossRealtime.Helpers;
using DtectLossRealtime.Models;
using static System.Math;

namespace DtectLossRealtime.EKDAlgoritm
{
    /// <summary>
    /// This DtectLoss kick/loss algorithm class applies an unscented Kalman Filter
    /// to estimate influx/loss rate in addition to loss rate on shakers, mud volume in flow line, 
    /// mud volume in pit, relative compressibility (beta/V), inverse inertia (1/M), flow line factor (k),
    /// relative friction (c/M), and calibration factor for paddle (s=qout/qpaddle) based on
    /// measurements of flow in, pump pressure, flow out and pit volume:
    ///
    /// d/dt ppump = beta/Vol ( qp + qio - qout)
    /// d/dt qout  = 1/M (ppump - c*qpump)
    /// d/dt Vfl   = qout - qpit - qshaker
    /// d/dt Vpit  = qpit - qshaker
    ///
    /// qpit       = k*Vfl
    /// qpaddle    = s*qout
    /// 
    /// </summary>
    /// 
    public class UkfAlg : IEKDAlgoritm
    {
        //Inputs
        public double flowIn_lpm;                  //Filter alt 1,2,3,4
        public double flowOut_lpm;                 //Filter alt 1,2,3,4
        //States
        public double volPit_m3;                   //Filter alt 1,2,3,4
        public double presSP_bar;                  //Filter alt   2,3,4 
        public double volFL_m3;                    //Filter alt     3,4
        public double flowOutx_lpm;                //Filter alt       4
        public double volTT_m3;                    //Filter alt         5

        //Params
        public double flowIO_lpm;                  //Filter alt 1,2,3,4
        public double betaperVol_barm3;            //Filter alt   2,3,4
        public double kpit_lpmpm3;                 //Filter alt     3,4
        public double kshaker_lpmpm3;              //Filter alt     3,4
        public double spaddle_lpmpc;               //Filter alt     3,4
        public double Minv_barsplpm;               //Filter alt       4
        public double cfrac_barplpm;               //Filter alt       4
        public double flowTT_lpm;                  //Filter alt         5
        public double areaDP_m2;                   //Filter alt         5
        public double valveTT_01;                  //Filter alt         5 Normal value 1, 0 when return is routed to trip tank(tripping or flow check)
        public double velBlock_mps;                //Filter alt         5

        //noise terms
        public double volPitderxnoise_m3ps;        //Filter alt 1,2,3,4
        public double presSPderxnoise_barps;       //Filter alt   2,3,4  
        public double volFLderxnoise_m3ps;         //Filter alt     3,4 
        public double flowOutderxnoise_lpmps;      //Filter alt       4        
        public double volTTderxnoise_m3ps;         //Filter alt         5 


        public double flowIOderwnoise_lpmps;       //Filter alt 1,2,3,4 
        public double betaprVolderwnoise_barm3ps;  //Filter alt   2,3,4
        public double kpitderwnoise_lpmpm3ps;      //Filter alt     3,4
        public double kshakerderwnoise_lpmpm3ps;   //Filter alt     3,4
        public double spaddlederwnoise_lpmpcps;    //Filter alt     3,4
        public double Minvderwnoise_barplpm;       //Filter alt       4
        public double cfracderwnoise_barplpmps;    //Filter alt       4
        public double flowTTderwnoise_lpmps;       //Filter alt         5
        public double areaDPderwnoise_m2ps;        //Filter alt         5


        public double volPitynoise_m3;             //Filter alt 1,2,3,4
        public double flowOutynoise_lpm;           //Filter alt 1,2,3,4         
        public double presSPynoise_bar;            //Filter alt   2,3,4
        public double volTTynoise_m3;              //Filter alt         5
        //Deducted var
        public double flowShaker_lpm;              //Filter alt     3,4
        public double flowPit_lpm;                 //Filter alt     3,4
        public double presFric_bar;                //Filter alt       4
        public double volIO_m3;                    //Filter alt 1,2,3,4
        public double flowSteel_lpm;               //Filter alt         5


        public double [] statex;
        public double [] inputu;
        public double [] measy;
        public double [] paramw; //unknown parameters
        public double [] paramp; //known parameters

        Func<double[], double[], double[], double, double[]> edyn;
        Func<double[], double[], double[], double[]> hmeas;

        public double [,] varxwQ;
        public double [,] varxwP;
        public double [,] varyR;

        public double dt_s;
        public double timeprev_d;
        public double timestepmax_s;
        Voting parameters;             // tunable paramters struct releated to the algorithm

        public double timeLast_d;
        public double timeCurrent_d;
        public int filteralt;

        //samplesSinceStartedDrilling;
        public double volumeThreshold_m3;

        public AlarmState AlarmState => alarmState;

        public bool ResetAlarm { get; }
        public bool RecalibrateFlowOutNow { get; }
        public string alarmStatus;
        public string[] comments => _comments.ToArray();
        private List<string> _comments = new List<string>();
        private AlarmState alarmState;

        const bool debug = false; //false;//true;
        //const int filteralt = 2; //1 = simple with 1 state & 1 param, 2 = include pdot eq and fixed beta/ V, 3 = add flowline dyn and fixed k, 4 = add flow out as state, 5 = 2 + trip tank func
 
        const double betaperVol_barm3_min = 5000 / 200; // maximum value for beta/V
        const double betaperVol_barm3_max = 50000 / 10; // minimum value for beta/V

        public UkfAlg()
        {
            //params      = [];
            //flowIO_lpm = [];
            //volIO_m3 = [];
            //alarmState = 0;
            //alarmStatus = '';
            //comments = cellstr(''); //_comments
            //obj.samplesSinceStartedDrilling =[];
        }


        public void Initialize(Voting votingParams, double[] pargIn) //% doPcalc
        {
            this.parameters      = votingParams;
            flowIO_lpm = 0; // w1
            volIO_m3 = 0; // integrate flowIO

            if (filteralt == 1)
            {
                volPit_m3 = 0; // x1
                flowIn_lpm = 0; // u1
                flowOut_lpm = 0; // u2
                // Tuning params
                volPitderxnoise_m3ps = 100 / 60000; // vx1
                flowIOderwnoise_lpmps = 100; // vw2
                volPitynoise_m3 = 0.1; // vy1
                flowOutynoise_lpm = 1;   // vy2

            }
            else if (filteralt == 2)
            {
                volPit_m3 = 0;   // x1
                presSP_bar = 0;   // x2
                betaperVol_barm3 = 15000 / 200; // w2
                flowIn_lpm = 0;   // u1
                flowOut_lpm = 0;   // u2
                // Tuning params
                volPitderxnoise_m3ps = 500 / 60000; // vx1 100 / 60000
                presSPderxnoise_barps = 1;   // vx2 1
                flowIOderwnoise_lpmps = 200; // vw1 200
                betaprVolderwnoise_barm3ps = 0 * 1; // vw2
                volPitynoise_m3 = 1; // vm1 0.1
                flowOutynoise_lpm = 1;  // vm2 10
                presSPynoise_bar = 0.1;   // vm3 2
            }
                
            else if (filteralt == 3)
            {
                volPit_m3 = 0;   // x1
                presSP_bar = 0;   // x2
                volFL_m3 = 0;   // x3
                betaperVol_barm3 = 15000 / 156; // w2
                kpit_lpmpm3 = 1000; // w3
                kshaker_lpmpm3 = 10;  // w4
                spaddle_lpmpc = 1;   // w5
                flowIn_lpm = 0;   // u1
                flowOut_lpm = 0;   // u2
                flowPit_lpm = 0;
                flowShaker_lpm = 0;
                // Tuning params
                volPitderxnoise_m3ps = 100;  // vx1
                presSPderxnoise_barps = 1;    // vx2
                volFLderxnoise_m3ps = 0.01; // vx3
                flowIOderwnoise_lpmps = 50;   // vw1 200
                betaprVolderwnoise_barm3ps = 0 * 1;  // vw2
                kpitderwnoise_lpmpm3ps = 5;    // vw3
                kshakerderwnoise_lpmpm3ps = 1;    // vw4
                spaddlederwnoise_lpmpcps = 0;    // vw5
                volPitynoise_m3 = 0.1;  // vy1
                flowOutynoise_lpm = 10;   // vy2
                presSPynoise_bar = 2;    // vy3
            }
               
            else if (filteralt == 4)
            {
                volPit_m3 = 0;   // x1
                presSP_bar = 0;   // x2
                volFL_m3 = 0;   // x3
                flowOutx_lpm = 0;   // x4
                betaperVol_barm3 = 15000 / 156; // w2
                kpit_lpmpm3 = 1000; // w3
                kshaker_lpmpm3 = 10;  // w4
                spaddle_lpmpc = 1;   // w5
                Minv_barsplpm = 4e8 / 60000; // w6
                cfrac_barplpm = 77 / 2000;   // w7
                flowIn_lpm = 0;   // u1
                flowOut_lpm = 0;   // u2
                flowPit_lpm = 0;
                flowShaker_lpm = 0;
                presFric_bar = 0;
                // Tuning params
                volPitderxnoise_m3ps = 100;  // vx1
                presSPderxnoise_barps = 1;    // vx2
                volFLderxnoise_m3ps = 0.01; // vx3
                flowOutderxnoise_lpmps = 0.01; // vx4
                flowIOderwnoise_lpmps = 50;   // vw1 200
                betaprVolderwnoise_barm3ps = 0 * 1;  // vw2
                kpitderwnoise_lpmpm3ps = 5;    // vw3
                kshakerderwnoise_lpmpm3ps = 1;    // vw4
                spaddlederwnoise_lpmpcps = 0;    // vw5
                Minvderwnoise_barplpm = 0;    // vw6
                cfracderwnoise_barplpmps = 0;    // vw7
                volPitynoise_m3 = 0.1;  // vy1
                flowOutynoise_lpm = 10;   // vy2
                presSPynoise_bar = 2;    // vy3
            }
            else if (filteralt == 5)
            {
                volPit_m3 = 0;   // x1
                presSP_bar = 0;   // x2
                volTT_m3 = 0;   // x3
                betaperVol_barm3 = 15000 / 200; // w2
                flowTT_lpm = 0;   // w3
                areaDP_m2 = 0.0038;   // w4
                valveTT_01 = 1; // return to pit, not to flow line and pit
                velBlock_mps = 0;
                flowIn_lpm = 0;   // u1
                flowOut_lpm = 0;   // u2
                // Tuning params
                volPitderxnoise_m3ps = 100 / 60000; // vx1
                presSPderxnoise_barps = 1;   // vx2
                volTTderxnoise_m3ps = 10 / 60000; // vx3
                flowIOderwnoise_lpmps = 200; // vw1
                betaprVolderwnoise_barm3ps = 0 * 1; // vw2
                flowTTderwnoise_lpmps = 1000; // vw3
                areaDPderwnoise_m2ps = 0;   // vw4
                volPitynoise_m3 = 0.1; /// vm1
                flowOutynoise_lpm = 10;  // vm2
                presSPynoise_bar = 2;   // vm3
                volTTynoise_m3 = 0.1; // vm1
            }

            timeprev_d = 0;
            dt_s = 0;
            timestepmax_s = 60;
            timeLast_d = 0;
            timeCurrent_d = 0;

            // obj.samplesSinceStartedDrilling = 0;

            // obj.sysState = SYSTEM_STATES_OBJ(); % the state machine

            // UKF init
            if (filteralt == 1) // simple Filter with 1 state & 1 param
            {
                const int nx = 1; // number of states x = Vpit
                const int nu = 1; // number of inputs u =[qp; qout]
                const int nw = 1; // number of parameters to be estimated w =[qres]
                const int ny = 2; // number of measurements y =[Vpit; qout]
                hmeas = (xw, u, p) => new[] { xw[0], u[0] + xw[1] };                      // function to obtain measured state 
                edyn = (xw, u, p, dt) => new[] { xw[0] + dt * (xw[1] / 60000), xw[1] };   // extended dynamics x_k+1 = f(xk, uk, pk, dt); w_k + 1 = w_k
                varyR = Matrix.Identity<double>(ny); // covariance of measurement
                varxwQ = Matrix.Identity<double>(nx + nw); // apriori covariance of process
                varxwP = varxwQ;   // aposteriori covariance state
                statex = new double[] {volPit_m3}; // initial state
                inputu = new Double [] {flowIn_lpm, flowOut_lpm};
                measy = new double[] {volPit_m3};
                paramw = new double[] {flowIO_lpm};
                paramp = new double [] {};
            }
            else if (filteralt == 2) // Filter with 2 state & 2 param
            {
                const int nx = 2; // number of states x = Vpit
                const int nu = 2; // number of inputs u =[qp; qout]
                const int nw = 2; // number of parameters to be estimated w =[qres]
                const int np = 0; // number of fixed parameters
                const int ny = 3; // number of measurements y =[Vpit; qout; psp]
                hmeas = (xw, u, p) => new[] { xw[0], u[1], xw[1] };        // function to obtain measured state
                edyn = (xw, u, p, dt) => new[] { xw[0] + dt * (u[1] - u[0]) / 60000, xw[1] + dt * xw[3] * (u[0] + xw[2] - u[1]) / 60000, xw[2], xw[3] };    // extended dynamics x_k+1 = f(xk, uk, pk, dt); w_k + 1 = w_k
                varyR = Matrix.Identity<double>(ny); // covariance of measurement
                varxwQ = Matrix.Identity<double>(nx + nw); // apriori covariance of process
                varxwP = varxwQ;   // aposteriori covariance state
                statex = new double [] {volPit_m3, presSP_bar}; // initial state
                inputu = new Double [] {flowIn_lpm, flowOut_lpm};
                measy = new Double [] {volPit_m3, flowOut_lpm, presSP_bar};
                paramw = new Double [] {flowIO_lpm, betaperVol_barm3};
                paramp = new Double [] {};
            }
            else if (filteralt == 3) // Filter with 3 states & 2 param
            {
                const int nx = 3; // number of states x =[Vpit psp Vfl]
                const int nu = 2; // number of inputs u =[qp; qout]
                const int nw = 5; // number of parameters to be estimated default w =[qres beta / v kpit kshaker spaddle]
                const int np = 0; // number of fixed parameters default[]
                const int ny = 3; // number of measurements y =[Vpit; qout; psp]
                statex = new Double [] {volPit_m3,        // xw1
                presSP_bar,                               // xw2
                volFL_m3};                                // xw3 initial state
                paramw = new double [] {flowIO_lpm,       // xw4
                betaperVol_barm3,                         // xw5
                kpit_lpmpm3,                              // xw6
                kshaker_lpmpm3,                           // xw7
                spaddle_lpmpc};                           // xw8
                inputu = new double[]{flowIn_lpm, flowOut_lpm};
                measy = new Double [] {volPit_m3, flowOut_lpm, presSP_bar};
                paramp = new double[] {};
                edyn = (xw, u, p, dt) => new [] {xw[0] + dt * (xw[5] * xw[2] - u[0]) / 60000,
                xw[1] + dt * xw[4] * (u[0] + xw[3] - xw[7] * u[1]) / 60000,
                xw[2] + dt * (xw[7] * u[1] - (xw[5] + xw[6]) * xw[2]) / 60000,
                xw[3],
                xw[4],
                xw[5],
                xw[6],
                xw[7]}; // extended dynamics x_k+1 = f(xk, uk, pk, dt); w_k + 1 = w_k
                hmeas = (xw, u, p) => new [] {xw[0], u[1], xw[1]}; // function to obtain measured state
                varyR = Matrix.Identity<double>(ny); // covariance of measurement
                varxwQ = Matrix.Identity<double>(nx + nw); // apriori covariance of process
                varxwP = varxwQ;   // aposteriori covariance state
            }
            else if (filteralt == 4)
            {
                const int nx = 4; // number of states x =[Vpit psp Vfl qout]
                const int nu = 1; // number of inputs u =[qp]
                const int nw = 7; // number of parameters to be estimated default w =[qres beta / v kpit kshaker spaddle]
                const int np = 0; // number of fixed parameters default[]
                const int ny = 3; // number of measurements y =[Vpit; qout; psp]
                statex = new Double[] {volPit_m3,                // xw1
                                   presSP_bar,                   // xw2
                                   volFL_m3,                     // xw3
                                   flowOutx_lpm};                // xw4 initial state
                paramw = new Double [] {flowIO_lpm,              // xw5
                                   betaperVol_barm3,             // xw6
                                   kpit_lpmpm3,                  // xw7
                                   kshaker_lpmpm3,               // xw8
                                   spaddle_lpmpc,                // xw9
                                   Minv_barsplpm,                // xw10
                                   cfrac_barplpm};               // xw11

                inputu = new Double [] {flowIn_lpm};
                measy = new Double [] {volPit_m3, flowOut_lpm, presSP_bar};
                paramp = new double[] {};
                edyn = (xw, u, p, dt) => new [] {xw[0] + dt * (xw[6] * xw[2] - u[0]) / 60000,
                                                xw[1] + dt * xw[5] * (u[0] + xw[4] - xw[3]) / 60000,
                                                xw[2] + dt * (xw[3] - (xw[6] + xw[7]) * xw[2]) / 60000,
                                                xw[3] + dt * xw[9] * (xw[1] - xw[10] * u[0]),
                                                xw[4],
                                                xw[5],
                                                xw[6],
                                                xw[7],
                                                xw[8],
                                                xw[9],
                                                xw[10]};                      // extended dynamics x_k+1 = f(xk, uk, pk, dt); w_k + 1 = w_k
                hmeas = (xw, u, p) => new [] {xw[0], xw[8] * xw[3], xw[1]} ;  // function to obtain measured state
                varyR = Matrix.Identity<double>(ny);      // covariance of measurement
                varxwQ = Matrix.Identity<double>(nx + nw);   // apriori covariance of process
                varxwP = varxwQ;   // aposteriori covariance state
            }
            else if (filteralt == 5) // Filter with 2 state & 2 param
            {
                const int nx = 3; // number of states x = Vpit psp Vtt
                const int nu = 2; // number of inputs u =[qp; qout]
                const int nw = 4; // number of parameters to be estimated w =[qIO beta / V qtt Adp]
                const int np = 0; // number of fixed parameters
                const int ny = 4; // number of measurements y =[Vpit; qout; psp; Vtt]
                statex = new Double[] {volPit_m3,           // xw1
                presSP_bar,                                 // xw2
                volTT_m3};                                  // xw3 initial state
                paramw = new Double [] {flowIO_lpm,         // xw4
                betaperVol_barm3,                           // xw5
                flowTT_lpm,                                 // xw6
                areaDP_m2};                                 // xw7 initial param
                inputu = new double [] {flowIn_lpm, flowOut_lpm};
                measy = new Double [] {volPit_m3, flowOut_lpm, presSP_bar, volTT_m3};
                paramp = new double [] {valveTT_01, velBlock_mps};
                hmeas = (xw, u, p) => new [] {xw[0], u[1], xw[1], xw[2]}; // function to obtain measured state
                edyn = (xw, u, p, dt) => new [] {xw[0] + dt * (p[0] * u[1] - u[0]) / 60000,
                xw[1] + dt * xw[4] * (u[0] + xw[3] - u[1] + (1 - p[0]) * xw[5] + p[1] * xw[6] * 60000) / 60000,
                xw[2] + dt * ((1 - p[0]) * u[1] - xw[5]) / 60000, xw[3], xw[4], xw[5], xw[6]}; // extended dynamics x_k+1 = f(xk, uk, pk, dt); w_k + 1 = w_k
                varyR = Matrix.Identity<double>(ny); // covariance of measurement
                varxwQ = Matrix.Identity<double>(nx + nw);  // apriori covariance of process
                varxwP = varxwQ;   // aposteriori covariance state
            }
        }

        //determine if an alarm has occured
        public void SetAlarmState()
        {
            if (Abs(volIO_m3) > parameters.VolumeThresholdAlarmM3)
                alarmState = AlarmState.Alarm; // Alarm
            else if (Abs(volIO_m3) > parameters.VolumeThresholdWarningM3)
                alarmState = AlarmState.Warning; // Warning
            else
                alarmState = AlarmState.Normal; // Normal
        }


        // this function is called for every vector of new data
        // public void StepIt(/*out UkfOutput result,*/ ProcessData processData, ExpData expData, SysState sysState, double timeCurrent_d, double dt_s)
        public AlgResult StepIt(WashedData washedData, AlgData algData, StateMachine sysState, double timeCurrentD, double dTs)
        {
            this.timeCurrent_d = timeCurrentD;
            this.dt_s = dTs;  // (obj.timeCurrent_d - obj.timeLast_d) * 86400;
            timeLast_d = timeCurrent_d;

            var lpmToM3ps = 1 / 60000.0;
            //var dayTos = 1 / CommonConstants.SecondsIn24Hours;
            var dayTos = 1.0.SecondsToOADate();
            var hourTos = 1 / 3600.0;

            alarmStatus = "Normal";
            // obj.dt_s = timeCurrent_d - obj.timeLast_d)*86400;

            if (dt_s <= 0)  //do nothing
                alarmStatus = $"Do nothing, time step = {dt_s} sec";
            else if (dt_s > timestepmax_s) // reset
            {
                alarmStatus = $"Reset, time step = {dt_s} sec";
                Initialize(parameters, new double []{});
                alarmState = AlarmState.Disabled;
            }
            else if (washedData.Values.ContainsKey("flowIn_lpm") || washedData.Values.ContainsKey("volGainLossWithoutOffset_m3") 
                                                                 || washedData.Values.ContainsKey("flowOut_lpm") || washedData.Values.ContainsKey("presSP_bar"))
                alarmState = AlarmState.InputError;
            else // ok - step ukf
            {
                timeprev_d = timeprev_d + dt_s * dayTos;  // newtime_d; TO DO: input newtime, compute dt from prevtime
                // ukf
                if (filteralt == 1)
                {
                    inputu = new Double[] { washedData.Values[V.FlowIn_lpm] };
                   measy = new Double [] {washedData.Values[V.VolGainLossWithoutOffset_m3], washedData.Values[V.FlowOut_lpm]};
                   statex = new Double [] {volPit_m3};  // state
                   paramw = new Double [] {flowIO_lpm}; // param
                   paramp = new Double [] {};
                   varxwQ[1, 1] = Pow(volPitderxnoise_m3ps * dt_s, 2); // Volpit
                   varxwQ[2, 2] = Pow(flowIOderwnoise_lpmps * dt_s, 2); // flowIO
                   varyR[1, 1] = Pow(volPitynoise_m3, 2);
                   varyR[2, 2] = Pow(flowOutynoise_lpm, 2);
                }
                else if (filteralt == 2)
                {
                    inputu = new Double [] {washedData.Values[V.FlowIn_lpm], washedData.Values[V.FlowOut_lpm]};
                    measy = new Double [] {washedData.Values[V.VolGainLossWithoutOffset_m3], washedData.Values[V.FlowOut_lpm], washedData.Values[V.PresSP_bar]};
                    statex = new Double [] {volPit_m3, presSP_bar};        // state
                    paramw = new Double [] {flowIO_lpm, betaperVol_barm3}; // unknown param
                    paramp = new Double [] {}; // fixed known param
                    varxwQ[1, 1] = Pow(volPitderxnoise_m3ps * dt_s, 2); // Volpit
                    varxwQ[2, 2] = Pow(presSPderxnoise_barps * dt_s, 2); // presPump
                    varxwQ[3, 3] = Pow(flowIOderwnoise_lpmps * dt_s, 2); // flowIO
                    varxwQ[4, 4] = Pow(betaprVolderwnoise_barm3ps * dt_s, 2); // beta / Vol
                    varyR[1, 1] = Pow(volPitynoise_m3, 2);
                    varyR[2, 2] = Pow(flowOutynoise_lpm, 2);
                    varyR[3, 3] = Pow(presSPynoise_bar, 2);
                }
                else if (filteralt == 3)
                {
                    inputu = new double [] {washedData.Values[V.FlowIn_lpm], washedData.Values[V.FlowOut_lpm]};
                    measy = new double [] {washedData.Values[V.VolGainLossWithoutOffset_m3], washedData.Values[V.FlowOut_lpm], washedData.Values[V.PresSP_bar]};
                    statex = new Double [] {volPit_m3, presSP_bar, volFL_m3};      // state
                    paramw = new double [] {flowIO_lpm, betaperVol_barm3, kpit_lpmpm3, kshaker_lpmpm3, spaddle_lpmpc}; // unknown param
                    paramp = new Double [] {}; // fixed known param
                    varxwQ[1, 1] = Pow(volPitderxnoise_m3ps * dt_s, 2);        // Volpit
                    varxwQ[2, 2] = Pow(presSPderxnoise_barps * dt_s, 2);       // presPump
                    varxwQ[3, 3] = Pow(volFLderxnoise_m3ps * dt_s, 2);         // VolFL
                    varxwQ[4, 4] = Pow(flowIOderwnoise_lpmps * dt_s, 2);       // flowIO
                    varxwQ[5, 5] = Pow(betaprVolderwnoise_barm3ps * dt_s, 2);  // beta / Vol
                    varxwQ[6, 6] = Pow(kpitderwnoise_lpmpm3ps * dt_s, 2);      // kpit
                    varxwQ[7, 7] = Pow(kshakerderwnoise_lpmpm3ps * dt_s, 2);   // kshaker
                    varxwQ[8, 8] = Pow(spaddlederwnoise_lpmpcps * dt_s, 2);    // spaddle
                    varyR[1, 1] = Pow(volPitynoise_m3, 2);
                    varyR[2, 2] = Pow(flowOutynoise_lpm, 2);
                    varyR[3, 3] = Pow(presSPynoise_bar, 2);
                }
                else if (filteralt == 4)
                {
                    inputu = new Double [] {washedData.Values[V.FlowIn_lpm]};
                    measy = new Double [] {washedData.Values[V.VolGainLossWithoutOffset_m3], washedData.Values[V.FlowOut_lpm], washedData.Values[V.PresSP_bar]};
                    statex = new Double[] {volPit_m3, presSP_bar, volFL_m3, flowOutx_lpm};      // state
                    paramw = new Double[]{flowIO_lpm, betaperVol_barm3, kpit_lpmpm3, kshaker_lpmpm3, spaddle_lpmpc, Minv_barsplpm, cfrac_barplpm}; // unknown param
                    paramp = new Double[] {}; // fixed known param
                    varxwQ[1, 1] = Pow(volPitderxnoise_m3ps * dt_s, 2);           // Volpit
                    varxwQ[2, 2] = Pow(presSPderxnoise_barps * dt_s, 2);      // presPump
                    varxwQ[3, 3] = Pow(volFLderxnoise_m3ps * dt_s, 2);        // VolFL
                    varxwQ[4, 4] = Pow(flowOutderxnoise_lpmps * dt_s, 2);     // FlowOut
                    varxwQ[5, 5] = Pow(flowIOderwnoise_lpmps * dt_s, 2);      // flowIO
                    varxwQ[6, 6] = Pow(betaprVolderwnoise_barm3ps * dt_s, 2); // beta / Vol
                    varxwQ[7, 7] = Pow(kpitderwnoise_lpmpm3ps * dt_s, 2);     // kpit
                    varxwQ[8, 8] = Pow(kshakerderwnoise_lpmpm3ps * dt_s, 2);  // kshaker
                    varxwQ[9, 9] = Pow(spaddlederwnoise_lpmpcps * dt_s, 2);   // spaddle
                    varxwQ[10, 10] = Pow(Minvderwnoise_barplpm * dt_s, 2);    // Minv
                    varxwQ[11, 11] = Pow(cfracderwnoise_barplpmps * dt_s, 2); // cfrac
                    varyR[1, 1] = Pow(volPitynoise_m3, 2);
                    varyR[2, 2] = Pow(flowOutynoise_lpm, 2);
                    varyR[3, 3] = Pow(presSPynoise_bar, 2);
                }
                else if (filteralt == 5)
                {
                    inputu = new Double [] {washedData.Values[V.FlowIn_lpm], washedData.Values[V.FlowOut_lpm]};
                    measy = new Double [] {washedData.Values[V.VolGainLossWithoutOffset_m3], washedData.Values[V.FlowOut_lpm],
                        washedData.Values[V.PresSP_bar], washedData.Values[V.VolTripTank_m3]};
                    statex =new Double [] {volPit_m3, presSP_bar, volPit_m3};      // state
                    paramw = new Double[] {flowIO_lpm, betaperVol_barm3, flowTT_lpm, areaDP_m2}; // unknown param
                    paramp = new Double[] {valveTT_01, washedData.Values[V.VelBlock_mph] * hourTos};     // known param
                    varxwQ[1, 1] = Pow(volPitderxnoise_m3ps * dt_s, 2);         // Volpit
                    varxwQ[2, 2] = Pow(presSPderxnoise_barps * dt_s, 2);        // presPump
                    varxwQ[3, 3] = Pow(volTTderxnoise_m3ps * dt_s, 2);          // VolTT
                    varxwQ[4, 4] = Pow(flowIOderwnoise_lpmps * dt_s, 2);        // flowIO
                    varxwQ[5, 5] = Pow(betaprVolderwnoise_barm3ps * dt_s, 2);   // beta / Vol
                    varxwQ[6, 6] = Pow(flowTTderwnoise_lpmps * dt_s, 2);        // flowIO
                    varxwQ[7, 7] = Pow(areaDPderwnoise_m2ps * dt_s, 2);         // flowIO
                    varyR[1, 1] = Pow(volPitynoise_m3, 2);
                    varyR[2, 2] = Pow(flowOutynoise_lpm, 2);
                    varyR[3, 3] = Pow(presSPynoise_bar, 2);
                    varyR[4, 4] = Pow(volTTynoise_m3, 2);
                    velBlock_mps = washedData.Values[V.VelBlock_mph] * hourTos;
                }
               
                if (sysState.CurrentState == DrillingStates.Tripping || sysState.CurrentState == DrillingStates.FlowCheck)
                {
                    if (washedData.Values[V.VolTripTank_m3].Equals(-0.99925))
                        measy[1] = 0;
                    else
                        alarmStatus = $"Tripping, vol = {washedData.Values[V.VolTripTank_m3]}, m3";
                    
                    valveTT_01 = 0;   // Tripping / flow check
                }
                else
                    valveTT_01 = 1;   // Not tripping / flow check

                if (filteralt != 5 && (sysState.PreviousState == DrillingStates.Tripping ||
                                       sysState.PreviousState == DrillingStates.FlowCheck)
                                   && !(sysState.CurrentState == DrillingStates.Tripping ||
                                        sysState.CurrentState == DrillingStates.FlowCheck)) // reset
                {
                    alarmStatus = $"Reset after tripping, time = , {timeprev_d * dayTos / 60},  min";
                    // TO DO: reset with old state and param vector or increase uncertainty on vector by modifying Q / R matrices
                    Initialize(parameters, new double []{});
                }
               
                // UKF
                double [] xw = statex.Concat(paramw).ToArray(); // Stack state and parameters to be estimated in one vector

                if (Vec.Logical(measy,x=> double.IsNaN(x)).Sum() + Vec.Logical(inputu, x=> double.IsNaN(x)).Sum() > 0)
                {
                    alarmState = AlarmState.InputError; // Bad meas
                    alarmStatus = $"Bad meas, UKF not running, time = , {timeprev_d * dayTos / 60}, min";
                }
                else if (valveTT_01 == 1) // Normal: drilling, connection
                {
                    Ukf(out xw, out varxwP, edyn, paramp, xw, varxwP, varxwQ, hmeas, measy, varyR, inputu, dt_s); 
                    SetAlarmState();
                }
                else // Tripping / flow check
                {
                    if (filteralt == 5)
                    {
                        Ukf(out xw, out varxwP, edyn, paramp, xw, varxwP, varxwQ, hmeas, measy, varyR, inputu, dt_s); 
                        SetAlarmState();
                    }
                    else
                    {
                        alarmState  = AlarmState.InputError; // Bad
                        alarmStatus = $"UKF not running, time = ', {timeprev_d * dayTos / 60}, min";
                    }
                }


                statex = Vec.SubsetIdx(xw, 0, statex.Length - 1);
                paramw = Vec.SubsetIdx(xw, statex.Length, statex.Length + paramw.Length - 2);
                if (filteralt == 1)
                {
                    volPit_m3 = statex[0];
                    flowIO_lpm = paramw[0];
                }

                else if (filteralt == 2)
                {
                    volPit_m3          = statex[0];
                    presSP_bar         = statex[1];
                    flowIO_lpm         = paramw[0];
                    betaperVol_barm3   = paramw[1];
                    //Constrain values on states and parameters
                    presSP_bar         = Max(0, presSP_bar);
                    betaperVol_barm3   = Max(betaperVol_barm3_min, Min(betaperVol_barm3_max, betaperVol_barm3));
                }
                else if (filteralt == 3)
                {
                    volPit_m3 = statex[0];
                    presSP_bar = statex[1];
                    volFL_m3 = statex[2];
                    flowIO_lpm = paramw[0];
                    betaperVol_barm3 = paramw[1];
                    kpit_lpmpm3 = paramw[2];
                    kshaker_lpmpm3 = paramw[3];
                    spaddle_lpmpc = paramw[4];
                    // Constrain values on states and parameters
                    presSP_bar = Max(0, presSP_bar);
                    volFL_m3 = Max(0, volFL_m3);
                    betaperVol_barm3 = Max(betaperVol_barm3_min, Min(betaperVol_barm3_max, betaperVol_barm3));
                    kpit_lpmpm3 = Max(10, Min(2000, kpit_lpmpm3));
                    kshaker_lpmpm3 = Max(0, Min(2000, kshaker_lpmpm3));
                    spaddle_lpmpc = Max(1, Min(1, spaddle_lpmpc));
                    flowPit_lpm = volFL_m3 * kpit_lpmpm3;
                    flowShaker_lpm = volFL_m3 * kshaker_lpmpm3;
                }
                else if (filteralt == 4)
                {
                    volPit_m3 = statex[0];
                    presSP_bar = statex[1];
                    volFL_m3 = statex[2];
                    flowOutx_lpm = statex[3];
                    flowIO_lpm = paramw[0];
                    betaperVol_barm3 = paramw[1];
                    kpit_lpmpm3 = paramw[2];
                    kshaker_lpmpm3 = paramw[3];
                    spaddle_lpmpc = paramw[4];
                    Minv_barsplpm = paramw[5];
                    cfrac_barplpm = paramw[6];
                    //Constrain values on states and parameters
                    presSP_bar = Max(0, presSP_bar);
                    volFL_m3 = Max(0, volFL_m3);
                    flowOutx_lpm = Max(0, flowOutx_lpm);
                    betaperVol_barm3 = Max(betaperVol_barm3_min, Min(betaperVol_barm3_max, betaperVol_barm3));
                    kpit_lpmpm3 = Max(10, Min(2000, kpit_lpmpm3));
                    kshaker_lpmpm3 = Max(0, Min(2000, kshaker_lpmpm3));
                    spaddle_lpmpc = Max(1, Min(1, spaddle_lpmpc));
                    Minv_barsplpm = Max(0.005, Min(0.01, Minv_barsplpm));
                    cfrac_barplpm = Max(10 / 2000, Min(200 / 2000, cfrac_barplpm));
                    flowPit_lpm = volFL_m3 * kpit_lpmpm3;
                    flowShaker_lpm = volFL_m3 * kshaker_lpmpm3;
                    presFric_bar = cfrac_barplpm * washedData.Values[V.FlowIn_lpm];
                }
                else if (filteralt == 5)
                {
                    volPit_m3 = statex[0];
                    presSP_bar = statex[1];
                    volTT_m3 = statex[2];
                    flowIO_lpm = paramw[0];
                    betaperVol_barm3 = paramw[1];
                    flowTT_lpm = paramw[2];
                    areaDP_m2 = paramw[3];
                    // Constrain values on states and parameters
                    presSP_bar = Max(0, presSP_bar);
                    betaperVol_barm3 = Max(betaperVol_barm3_min, Min(betaperVol_barm3_max, betaperVol_barm3));
                    if (valveTT_01 == 1)
                        flowTT_lpm = 0;
                    else
                        flowTT_lpm = Max(0, flowTT_lpm);
                   
                    areaDP_m2 = Max(0.001, Min(0.005, areaDP_m2));
                    flowSteel_lpm = velBlock_mps * areaDP_m2 * 60000;
                }
                
                if ((AlarmState >= 0) && (Abs(flowIO_lpm)) > parameters.FlowDeadbandLpm)
                    volIO_m3 = volIO_m3 + dt_s * flowIO_lpm*lpmToM3ps;   // vIo[m3] = dt[s] *qIo[lpm]
                else
                    volIO_m3      = 0;
            }
            if (!alarmStatus.Equals("Normal"))
            {
                if (debug)
                    Console.WriteLine(alarmStatus);

                _comments.Add(Environment.NewLine + alarmStatus);
                //comments = [comments; alarmStatus]; 
            }
            return ReturnResultStruct();
        }

    public AlgResult ReturnResultStruct()
    {
            var result = new AlgResult(); 
            result.VolPitM3  = volPit_m3;
            //result.flowIO_lpm = flowIO_lpm;
            result.FlowIOflowLpm = flowIO_lpm;
            result.VolIoM3   = volIO_m3;
            result.AlarmState = AlarmState;

            if (filteralt == 2)
            {
                result.PresSpBar = presSP_bar;
                result.BetaperVolBarm3 = betaperVol_barm3;
            }
            else if (filteralt == 3)
            { 
                result.PresSpBar = presSP_bar;
                result.VolFlM3 = volFL_m3;
                result.BetaperVolBarm3 = betaperVol_barm3;
                result.KpitLpmpm3 = kpit_lpmpm3;
                result.KshakerLpmpm3 = kshaker_lpmpm3;
                result.SpaddleLpmpc = spaddle_lpmpc;
                result.FlowPitLpm = flowPit_lpm;
                result.FlowShakerLpm = flowShaker_lpm;
            }
            else if (filteralt == 4)
            {
                result.PresSpBar = presSP_bar;
                result.VolFlM3 = volFL_m3;
                result.FlowOutxLpm = flowOutx_lpm;
                result.BetaperVolBarm3 = betaperVol_barm3;
                result.KpitLpmpm3 = kpit_lpmpm3;
                result.KshakerLpmpm3 = kshaker_lpmpm3;
                result.SpaddleLpmpc = spaddle_lpmpc;
                result.MinvBarsplpm = Minv_barsplpm;
                result.CfracBarplpm = cfrac_barplpm;
                result.FlowPitLpm = flowPit_lpm;
                result.FlowShakerLpm = flowShaker_lpm;
                result.PresFricBar = presFric_bar;
            }
            else if (filteralt == 5)
            {
                result.PresSpBar = presSP_bar;
                result.VolTtM3 = volTT_m3;
                result.BetaperVolBarm3 = betaperVol_barm3;
                result.FlowTtLpm = flowTT_lpm;
                result.AreaDpM2 = areaDP_m2;
                result.ValveTt01 = valveTT_01;
                result.FlowSteelLpm = flowSteel_lpm;
                result.VelBlockMps = velBlock_mps;
            }
            return result;
        }

        public void Ukf(out double[] x, out double[,] P, Func<double[], double[], double[], double, double[]> f, double[] p,
            double[] xp, double[,] Pp, double[,] Q, Func<double[], double[], double[], double[]> h,double[] y, double[,] R, double[] u, double dt) // UKF Unscented Kalman Filter for nonlinear dynamic systems
        {
            // for nonlinear dynamic system(for simplicity, noises are assumed as additive) :
            // x_k + 1 = f(p_k, x_k, u_k) + vp_k
            // y_k = h(x_k) + vm_k
            // where process noise vp ~N(0, Q), i.e.gaussian noise with covariance Q
            // measurement noise vm ~N(0, R), i.e.gaussian noise with covariance R
            // Inputs:   f: function handle for f(x, u, p, dt)
            // x: "a priori" state estimate
            // p: known parameters
            // w: unknow parameters(to be estimated)
            // P: "a priori" estimated state covariance
            // h: function handle for h(x, u, w)
            // y: current measurement
            // Q: process noise covariance
            // R: measurement noise covariance
            // Output:   x: "a posteriori" state estimate
            // P: "a posteriori" state covariance

            x = new double[] { };
            P = new double[,] { };
            var L = x.Length;     // number of states

            var m = y.Length;     // number of measurements
            var alpha = 1e-3;     // default, tunable
            var ki = 0;           // default, tunable
            var beta = 2;         // default, tunable
            var lambda = Pow(alpha, 2) * (L + ki) - L; // scaling factor
            var c = L + lambda;                    // scaling factor
            var Wm = (new double[] { lambda / c }).Concat(Vector.Create(2 * L, 0.0).Add(0.5 / c)).ToArray(); // weights for means
            var Wc = Wm;
            Wc[0] = Wc[0] + (1 - Pow(alpha, 2) + beta);         // weights for covariance
            c = Sqrt(c);
            Sigmas(out double [,] X, x, P, c);                       // sigma points around xw
            Ut_state(out double[] x1, out double [,] X1, out double [,] P1, out double [,] X2, f, p, u, X, Wm, Wc, L, Q, dt);  // unscented transformation of process
            // X1 = sigmas(x1, P1, c);                               // sigma points around x1
            // X2 = X1 - x1(:, ones(1, size(X1, 2)));                // deviation of X1
            Ut_meas(out double[] z1, out double[,] Z1, out double[,] P2, out double[,] Z2, h, p, u, X1, Wm, Wc, m, R);            // unscented transformation of measurments
            var P12 = X2.Multiply(Matrix.Diagonal(Wc)).Multiply(Z2.Transpose());
            var K = P12.Divide(P2);
            x = x1.Add(Matrix.Dot(K, y.Subtract(z1)));
            P = P1.Subtract(P12.Multiply(K.Transpose()));
        }

       public void Ut_state(out double[] y, out double[,] Y, out double [,] P, out double [,] Y1, 
           Func<double[],double[],double[], double, double[]> f, double [] p, double [] u, double[,] X, double[] Wm, double[] Wc, int m,
            double [,] R, double dt)
        {
            // Unscented Transformation
            // Input:
            // f: nonlinear map
            // p: parameter struct
            //        u: input vector
            //        X: sigma points
            //       Wm: weights for mean
            //       Wc: weights for covraiance
            //        m: number of outputs of f
            //        R: additive covariance
            //Output:
            //        y: transformed mean
            //        Y: transformed smapling points
            //        P: transformed covariance
            //       Y1: transformed deviations

            var L = X.GetLength(1); // size(X,2);
            y = new double[m];
            Y = new double[m, L];
            for (var k = 0; k < L; k++)
            {
                Matrix.SetColumn(Y, k, f(Matrix.GetColumn(X, k), u, p, dt));
                y = Matrix.GetColumn(Y, k, null).Multiply(Wm[k]).Add(y); 
            }
            double[,] Z = new double[y.Rows(), L];
            for (var i = 0; i < y.Rows(); i++)
            {
                for (var j = 0; j < L; j++)
                {
                    Z[i, j] = y[i];
                }
            }
            Y1 = Y.Subtract(Z);
            P = Y1.Multiply(Matrix.Diagonal(Wc)).Multiply(Y1.Transpose()).Add(R); //ToDo:make sure it's correct
        }

        public void Ut_meas(out double[] y, out double[,] Y, out double [,] P, out double [,] Y1,
            Func<double[], double[], double[], double[]> h, double[] p, double[] u, double[,] X, double[] Wm,
            double[] Wc, int m, double [,] R)
        {
            // Unscented Transformation
            // Input:
            // hmeas: nonlinear map of outputs
            // p: parameter struct
            // X: sigma points
            // Wm: weights for mean
            // Wc: weights for covraiance
            // m: numer of outputs of f
            // R: additive covariance
            // Output:
            // y: transformed mean
            // Y: transformed smapling points
            // P: transformed covariance
            // Y1: transformed deviations

            var L = X.GetLength(1);    //size(X, 2);
            y = new double[m];
            Y = new double[m, L];
            for (var k = 0; k < L; k++)
            {
                Matrix.SetColumn(Y, k, h(Matrix.GetColumn(X, k), u, p));
                y = Matrix.GetColumn(Y, k, null).Multiply(Wm[k]).Add(y);

            }
            double [,] Z = new double[y.Rows(), L]; 
            for (var i = 0; i < y.Rows(); i++)
            {
                for (var j = 0; j < L; j++)
                {
                    Z[i, j] = y[i];
                }
            }
            Y1 = Y.Subtract(Z);
            P = Y1.Multiply(Matrix.Diagonal(Wc)).Multiply(Y1.Transpose()).Add(R); //ToDo:make sure it's correct
        }

        public void Sigmas(out double[,] X, double[] x, double[,] P, double c)
        {
            // Sigma points around reference point
            // Inputs:
            // x: reference point
            // P: covariance
            // c: coefficient
            // Output:
            // X: Sigma points
            var cd = new CholeskyDecomposition(P);
            double[,] A = cd.Solve(P).Transpose().Multiply(c);
            //Y = x(:, ones(1, numel(x)));
            double [,] Y = new double[x.Rows(), x.Length];
              for (var i = 0; i < x.Rows(); i++)
              {
                  for (var j = 0; j < x.Length; j++)
                  {
                    Y[i, j] = x[i];
                  }
              }
              X = Matrix.Concatenate(x.Transpose(), Y.Add(A)).Concatenate(Y.Subtract(A)); //TODO: if this is correct vector x and matrix Y.Add(A) can concatenate??
        }
    }
}
