﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DtectLossRealtime.Enums;

namespace DtectLossRealtime
{
    /// <summary>
    /// simulator class that sim solves dynamic equations for
    /// conventional drilling with/without loss
    /// </summary>
    public class Simulator
    {
        public double p_p_bar; // simulated state: pump pressure.
        public double q_out_lpm; // simulated state: flow out.
        public double V_fl_m3; // simulated state: volume in the flowline
        public double V_pit_m3; // simualted state: volume in the pit
        public double q_fl_lpm; // simulated(derived) variable: flowline flow
        public double t; // simualtion clock
       // public double buffer;
        public class buffer
        {
            public double[] t;
            public double [] p_p_bar;
            public double [] qin_lpm;
            public double [] qloss_lpm;
            public double [] q_fl_lpm;
            public double [] qout_lpm;
            public double [] bitDepth_m;
            public double [] holeDepth_m;
            public double [] VdivbyBeta;
            public double [] V_pit_m3;
            public double [] V_fl_m3;
            public double [] qIo_nodiff_lpm;
            public double [] betaDivByV_bar_per_m3;
            public double [] betaDivByV_e_bar_per_m3;

        }
        public int curBufferIdx;

        public double dt_s; // sampling time
        public SimulatorSolvers solver;
        public double sys_states;
        public double c_dp_bar_per_lpm2;
        public double c_ann_bar_per_lpm;
        public double M_kg_per_m4;
        public double A_dp_m2;
        public double A_ann_m2;
        public double V_m3;
        public double betaDivByV_bar_per_m3;
        public double K_fl_m3_per_lpm;      // the "choke coeffiecient" in the equation V_fl_m3 =K* q_fl_lpm;

        const double beta_bar = 13000;
        const double holeDepth_m = 3000;
        const double deltap_ann_bar = 10; // annular frictional pressure drop at drilling flow
        const double deltap_dp_bar = 50;       // frictional pressure drop at drillpipe

        // "nominal flows and volumes" 
        const double q_drilling_lpm = 2000;
        const double V_fl0_m3 = 5; // inital volume in the flowline during drilling(used to calculate K_fl_m3_per_lpm)

        const double D_ann_m = 8 * 0.0254;
        const double D_dpInner_m = 5.5 * 0.0254;
        const double D_dpOuter_m = 4.778 * 0.0254;
        const double rhoMud_kg_per_m3 = 1.63 * 1000;  // 1.63 sg;
        const double alpha_fl = 0.4;

        public int isCurrentlyBuffering; //TODO: TO ASK AND FIX
        public double valuesToBuffer;
        public double pressureNoiseAmpl_prc;
        public double flowNoiseAmpl_prc;

        public Simulator()
        {

        }

        public void Initialize(double dt_s, SimulatorSolvers solver) //TODO: to ask about solver?
        {
            this.dt_s = dt_s;
            this.solver = solver;
            // sys_states = SYSTEM_STATES_OBJ(); 
            t = 0;
            isCurrentlyBuffering = 0;
            curBufferIdx = 0;

            A_dp_m2 = Math.PI * Math.Pow(D_dpInner_m / 2, 2);
            A_ann_m2 = Math.PI * Math.Pow(D_ann_m / 2, 2) - Math.PI * Math.Pow(D_dpOuter_m / 2, 2);
            c_dp_bar_per_lpm2 = deltap_dp_bar / Math.Pow(q_drilling_lpm, 2);  // deltap_dp = c_dp * q_p ^ 2
            c_ann_bar_per_lpm = deltap_ann_bar / (q_drilling_lpm);     // deltap_ann = c_ann * q_o
            M_kg_per_m4 = holeDepth_m * rhoMud_kg_per_m3 * (1 / A_dp_m2 + 1 / A_ann_m2);
            V_m3 = (A_dp_m2 + A_ann_m2) * holeDepth_m;
            betaDivByV_bar_per_m3 = beta_bar / V_m3;
            K_fl_m3_per_lpm = q_drilling_lpm / Math.Pow(V_fl0_m3, alpha_fl);
        }

        public void StartSavingIterations(buffer buffer, double required_buffer_duration_s)
        {
            isCurrentlyBuffering = 1;
            valuesToBuffer = required_buffer_duration_s / dt_s;
            curBufferIdx = 0;
            
            buffer.t = Enumerable.Repeat(double.NaN, (int)valuesToBuffer).ToArray();
            buffer.p_p_bar = Enumerable.Repeat(Double.NaN, (int)valuesToBuffer).ToArray();
            buffer.qin_lpm = Enumerable.Repeat(Double.NaN, (int)valuesToBuffer).ToArray();
            buffer.qloss_lpm = Enumerable.Repeat(Double.NaN, (int)valuesToBuffer).ToArray();
            buffer.q_fl_lpm = Enumerable.Repeat(Double.NaN, (int)valuesToBuffer).ToArray();
            buffer.qout_lpm = Enumerable.Repeat(Double.NaN, (int)valuesToBuffer).ToArray();
            buffer.bitDepth_m = Enumerable.Repeat(Double.NaN, (int)valuesToBuffer).ToArray();
            buffer.holeDepth_m = Enumerable.Repeat(Double.NaN, (int)valuesToBuffer).ToArray();
            buffer.VdivbyBeta = Enumerable.Repeat(Double.NaN, (int)valuesToBuffer).ToArray();
            buffer.V_pit_m3 = Enumerable.Repeat(Double.NaN, (int)valuesToBuffer).ToArray();
            buffer.V_fl_m3 = Enumerable.Repeat(Double.NaN, (int)valuesToBuffer).ToArray();
            buffer.qIo_nodiff_lpm = Enumerable.Repeat(Double.NaN, (int)valuesToBuffer).ToArray();
            buffer.betaDivByV_bar_per_m3 = Enumerable.Repeat(Double.NaN, (int)valuesToBuffer).ToArray();
            buffer.betaDivByV_e_bar_per_m3 = Enumerable.Repeat(Double.NaN, (int)valuesToBuffer).ToArray();

        }

        public void StepIt(double q_p_lpm, double qloss_lpm, double bitDepth_m, double holeDepth_m, buffer buffer)
        {
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            const double lpmTOm3ps = 1 / 60000;  // conversion from liters per minute to m3 per second
            double m3psTOlpm = 1 / lpmTOm3ps;
            const double barToPa = 100000;

            if (t.Equals(0)) // set inital values
            {
                q_out_lpm = 2000;
                p_p_bar = 60;
                V_fl_m3 = V_fl0_m3;
                V_pit_m3 = 5;
            }

            if (solver == SimulatorSolvers.FORWARD_EULER)
            {
                // simplyfied steadified flow dynamics equation
                // M dotq_o = p_p - deltap_dp - deltap_ann - delta rho* g*h = 0
                // ==> (neglecting cuttings)
                // M dotq_o = p_p - c_ann * q_out ^ 2 + c_dp * q_in
                var fann_bar = c_ann_bar_per_lpm * (q_out_lpm);
                var fdp_bar = c_dp_bar_per_lpm2 * Math.Pow(q_out_lpm, 2);
                q_out_lpm = q_out_lpm + dt_s * (1 / M_kg_per_m4) * (p_p_bar - fann_bar - fdp_bar) * barToPa * m3psTOlpm;
                // simplified bulk modulus equation for pressure dynamics
                q_out_lpm = Math.Max(q_out_lpm, 0);
                p_p_bar = p_p_bar + dt_s * betaDivByV_bar_per_m3 * (q_p_lpm + qloss_lpm - q_out_lpm) * lpmTOm3ps;
                p_p_bar = Math.Max(p_p_bar, 0);

                // flowline flow
                q_fl_lpm = Math.Pow(Math.Max(0.001, V_fl_m3), alpha_fl) * K_fl_m3_per_lpm;
                // flowline massbalance
                V_fl_m3 = V_fl_m3 + dt_s * (q_out_lpm - q_fl_lpm) * lpmTOm3ps;
                // pit massbalan ce
                V_pit_m3 = V_pit_m3 + dt_s * (q_fl_lpm - q_p_lpm) * lpmTOm3ps;
            }
            else
                Console.WriteLine("SIMUALTOR_OBJ.m: ERROR solver not supported");

            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
           // sys_states = sys_states.stateMachine(q_p_lpm, bitDepth_m, holeDepth_m, 150, 1); TODO: to find in source code and fix here

            t = t + dt_s;
            Console.WriteLine($"t:, {t}");
            // buffer results if specified
            if (isCurrentlyBuffering == 1) 
            {
                curBufferIdx = curBufferIdx + 1;
                if (curBufferIdx >= valuesToBuffer)
                {
                    isCurrentlyBuffering = 0;
                    Console.WriteLine("Simulation finished buffering");
                }
                else
                {
                    buffer.t[curBufferIdx] = t;
                    // states / algebraic simualted variables
                    buffer.p_p_bar[curBufferIdx] = p_p_bar;
                    buffer.qout_lpm[curBufferIdx] = q_out_lpm;
                    buffer.q_fl_lpm[curBufferIdx] = q_fl_lpm;
                    buffer.V_pit_m3[curBufferIdx] = V_pit_m3;
                    buffer.V_fl_m3[curBufferIdx] = V_fl_m3;
                    // input
                    buffer.qin_lpm[curBufferIdx] = q_p_lpm;
                    buffer.qloss_lpm[curBufferIdx] = qloss_lpm;
                    buffer.bitDepth_m[curBufferIdx] = bitDepth_m;
                    buffer.holeDepth_m[curBufferIdx] = holeDepth_m;
                    // param of interest
                    buffer.betaDivByV_bar_per_m3[curBufferIdx] = betaDivByV_bar_per_m3;
                }
            }
        }
    }
}
