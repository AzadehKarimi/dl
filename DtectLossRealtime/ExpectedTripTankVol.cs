﻿using Accord.Math;
using System.Linq;
using DtectLoss.Common;
using DtectLossRealtime.Enums;
using DtectLossRealtime.Helpers;
using DtectLossRealtime.Models;
using Microsoft.Extensions.Logging;
using static System.Convert;
using static System.Math;
using static DtectLoss.Common.CommonConstants;

namespace DtectLossRealtime
{
    /// <summary>
    /// this class calculates dill pipe area areaDP_m2 (open/closed end), so that:
    /// 
    /// d/dt volTripTank_m3(t) = areaDP_m2*velBlock_mph(t)/3600
    /// to do that this class buffers data during tripping.
    /// </summary>
    public class ExpectedTripTankVol
    {
        public double VolTTcumM3 { get; set; }
        public double VolTTcumExpM3 { get; set; }
        public double VolTTcumErrM3 { get; private set; }
        public double TimeLastStandD { get; private set; }
        public double DepthBitPrevM { get; private set; }
        public double NumStand { get; private set; }

        public double DepthLastStandM { get; set; }

        //Config params
        public double AreaDppoohM2 { get; private set; }
        public double AreaDprihM2 { get; private set; }
        public bool IsNewStandTripTank { get; private set; }

        //Updated every new depth sample
        private readonly ILogger<ExpectedTripTankVol> _logger;
        //Updated after every stand
        private double _volSteelLastStandM3;
        private double _numOkStand;
        private double _volTtLastStandM3;
        private double _volTTdiffM3;
        private double _lengthLastStandM;
        private double _lengthStandAverageM;
        private double _areaDpLastStandM2;
        private double _areaDpAverageM2;
        private double _volTtPrevM3;    //previous value
        private double _depthDeltaM;
        //Logic values
        private StateTripping _stateTripping;  // 1=RIH, 0=OnBottom/Steady, -1=POOH
        private bool _isOnBottom;
        private bool _isPipeSteady;   // 1=steady, 0=is moving
        private bool _isvolTtSteady;  //  1=steady, 0=is changing
        private bool _isOkLastStand;
        private double? _stepTimeOldD;
        private readonly RingBuffer _volTripTankBuffer;
        private readonly RingBuffer _depthBitBuffer;
        private double _timePrevDepthD;
        private double _timePrevVolD;
        private double _dtDepthS;
        private bool _doDebugText;
        private bool _isnewtrip;
        private const int BufferSizeSamples = 1000;
        private const int NValuesforSteady = 5;
        private const double DepthMaxMinThresholdM = 23;   //Minimum length since last calibration
        private const double FactorForget = 1.0/5.0;
        private const double VelMaxRopMph = 100;  //Tripping defined as bit depth changes faster than this + off bottom
        private const double FlowTtMaxLpm = 100;  //Steady TT volume defined as less flow than this in/out of tank


        public ExpectedTripTankVol(ILogger<ExpectedTripTankVol> logger)
        {
            _logger = logger;
            _depthBitBuffer = new RingBuffer(BufferSizeSamples);
            _volTripTankBuffer = new RingBuffer(BufferSizeSamples);
        }

        public void InitTrip()
        {
            NumStand = 0;
            _numOkStand = 0;
            _volTtLastStandM3 = 0;
            DepthLastStandM = 0;
            _volSteelLastStandM3 = 0;
            _isOkLastStand = false;
            _volTTdiffM3 = 0;
            _lengthLastStandM = 0;
            _areaDpLastStandM2 = 0;
            _areaDpAverageM2 = 0;
            VolTTcumM3 = 0;
            VolTTcumExpM3 = 0;
            VolTTcumErrM3 = 0;
            TimeLastStandD = 0;
            _lengthStandAverageM = 0;
        }

        public void SetAreadp(double areaDppoohM2, double areaDprihM2)
        {
            AreaDppoohM2 = areaDppoohM2;
            AreaDprihM2 = areaDprihM2;
        }

        public void Initialize(Triptank _)
        {
            AreaDppoohM2 = 0.01;   // Default values
            AreaDprihM2 = 0.02;    // Default values
            // Updated every new depth sample
            _volTtPrevM3 = 0;
            DepthBitPrevM = 0;
            _depthDeltaM = 0;
            _timePrevDepthD = 0;
            _timePrevVolD = 0;
            _dtDepthS = 0;
            _isOnBottom = false;
            _isPipeSteady = true;   // 1 = steady, 0 =is moving
            _isvolTtSteady = true;
            _stateTripping = StateTripping.OnBottom;     // 1 = RIH, 0 = OnBottom / Steady, -1 = POOH
            IsNewStandTripTank = false;
            _isOkLastStand = false;
            // Updated every new stand
            InitTrip();
            _doDebugText = false; // false;
        }

        public void Reset()
        {
            InitTrip();
            _volTtPrevM3 = 0;
            DepthBitPrevM = 0;
            _depthDeltaM = 0;
            _timePrevDepthD = 0;
            _timePrevVolD = 0;
            _dtDepthS = 0;
            _isOnBottom = false;
            _isPipeSteady = true;    // 1 = steady, 0 =is moving
            _isvolTtSteady = true;
            _stateTripping = 0;      // 1 = RIH, 0 = OnBottom / Steady, -1 = POOH
            IsNewStandTripTank = false;
            _isOkLastStand = false;
        }

        public void ResetTripVol()
        {
            VolTTcumExpM3 = VolTTcumM3;
            VolTTcumErrM3 = 0;
        }

        public bool HaveWeDrilledAnotherStandNow(double depthBitM, double volTripTankM3)
        {
            // 1: Check if pipe is moving or not and if volume has changed
            var depthVec = _depthBitBuffer.GetLastNValues(NValuesforSteady);
            var timeVecD = _depthBitBuffer.GetLastNTimes(NValuesforSteady);

            var ixDepth = depthVec.Select(x => x > 0).ToArray();
            depthVec = depthVec.Subset(ixDepth);
            timeVecD = timeVecD.Subset(ixDepth);

            var volVec = _volTripTankBuffer.GetLastNValues(NValuesforSteady);
            
            var ixVol = volVec.Select(x => x > 0).ToArray();
            volVec = volVec.Subset(ixVol);

            if (!depthVec.Any() || !timeVecD.Any() || !volVec.Any())
                return false;
          
            var depthMaxMinM = depthVec.Max() - depthVec.Min();
            var volMaxMinM3 = volVec.Max() - volVec.Min();
            var timeMaxMinH = 24 * (timeVecD.Max() - timeVecD.Min());
            if (timeMaxMinH > 0) 
            {
                _isPipeSteady = depthMaxMinM / timeMaxMinH < VelMaxRopMph;
                _isvolTtSteady = volMaxMinM3 * 1000 / (timeMaxMinH * 60) < FlowTtMaxLpm;
            }
            else
                return false;
           
            // 2: Check if depth has changed more than 20m since last stand
            var hasDepthChangedEnough = Abs(depthBitM - DepthLastStandM) > DepthMaxMinThresholdM;
            const bool hasVolTtChangedEnough = true;  // Comment from Matlab - XXX To be changed? abs(volTripTank_m3 -obj.volTTLastStand_m3) > volMaxMinThreshold_m3;
            return (_isPipeSteady && hasDepthChangedEnough && hasVolTtChangedEnough);  // && isvolTTSteady
        }

        public TripTankOutput SetOutput() 
        {
            TripTankOutput tripTankOutput = new TripTankOutput
            {
                NumStand = NumStand,
                NumOkStand = _numOkStand,
                VolTtLastStandM3 = _volTtLastStandM3,
                DepthLastStandM = DepthLastStandM,
                IsOkLastStand = _isOkLastStand,
                IsNewStandTripTank = IsNewStandTripTank,
                VolTTdiffM3 = _volTTdiffM3,
                LengthLastStandM = _lengthLastStandM,
                LengthStandAverageM = _lengthStandAverageM,
                VolSteelLastStandM3 = _volSteelLastStandM3,
                AreaDpLastStandM2 = _areaDpLastStandM2,
                AreaDpAverageM2 = _areaDpAverageM2,
                VolTTcumM3 = VolTTcumM3,
                VolTTcumExpM3 = VolTTcumExpM3,
                VolTTcumErrM3 = VolTTcumErrM3,
                TimeLastStandD = TimeLastStandD,
                DepthDeltaM = _depthDeltaM,
                StateTripping = (int) _stateTripping,
                IsOnBottom = _isOnBottom,
                IsPipeSteady = _isPipeSteady,
                IsvolTtSteady = _isvolTtSteady
            };
            return tripTankOutput;
        }

        public TripTankOutput Step(double depthBitM, double depthHoleM, double volTripTankM3, double flowInLpm, double timeNewD)
        {
            if (depthBitM.Equals(MissingValueReplacement))
            {
                return SetOutput(); 
            }
            _dtDepthS = timeNewD.DiffInSeconds(_timePrevDepthD);
            _depthDeltaM = depthBitM - DepthBitPrevM;
            _timePrevDepthD = timeNewD;
            DepthBitPrevM = depthBitM;

            if (!volTripTankM3.Equals(MissingValueReplacement))
            {
                _volTtPrevM3 = volTripTankM3;
                _timePrevVolD = timeNewD;
            }
            
            //Persistent
            if (_stepTimeOldD == null)
                _stepTimeOldD = timeNewD;

            if (Abs(_dtDepthS) > 60 || Abs(_depthDeltaM) > 100)
            {
                // First sample
                _dtDepthS = 0;
                _depthDeltaM = 0;
                return SetOutput();
            }
               
            _depthBitBuffer.AddDataToCircularBuffer(timeNewD, depthBitM);
            if (!volTripTankM3.Equals(MissingValueReplacement))
              _volTripTankBuffer.AddDataToCircularBuffer(timeNewD, volTripTankM3);
           
            // State machine
            if (_isOnBottom)
            {
                _isOnBottom = depthHoleM - depthBitM < 30;
                if (!_isOnBottom)   // Reset when we go off bottom
                    InitTrip();
            }
            else
                _isOnBottom = depthHoleM - depthBitM < 30 && flowInLpm > 200;
           
            if (_isOnBottom)
                _stateTripping = StateTripping.OnBottom;

            if (_isOnBottom || volTripTankM3.Equals(MissingValueReplacement))
                IsNewStandTripTank = false;
            else
                IsNewStandTripTank = HaveWeDrilledAnotherStandNow(depthBitM, volTripTankM3); 
          
            // State machine
            if (IsNewStandTripTank)
            {
                TimeLastStandD = timeNewD;
                if (NumStand.Equals(0) && DepthLastStandM.Equals(0)) //First stand
                {
                    _isnewtrip = true;
                    if (depthBitM < DepthLastStandM)
                        _stateTripping = StateTripping.POOH;    // POOH
                    else if (depthBitM > DepthLastStandM)
                        _stateTripping = StateTripping.RIH;   // RIH
                    else
                        _logger.LogWarning("ExpectedTripTankVol - ?");
                }
                else if (depthBitM > DepthLastStandM) //RIH
                {
                    _isnewtrip = (_stateTripping != StateTripping.RIH); //Change of direction from POOH to RIH or from onBottom
                    _stateTripping = StateTripping.RIH; //RIH
                    _isOkLastStand = (Abs(depthBitM - DepthLastStandM) < 40) &&
                                    Abs(volTripTankM3 - _volTtLastStandM3) < 3; // no empty TT && (volTripTank_m3 > obj.volTTLastStand_m3)
                }
                else // POOH 
                {
                    _isnewtrip = (_stateTripping != StateTripping.POOH);  // Change of direction from RIH to POOH or from onBottom
                    _stateTripping = StateTripping.POOH;    // POOH
                    _isOkLastStand = (Abs(depthBitM - DepthLastStandM) < 40) && 
                                    Abs(volTripTankM3 - _volTtLastStandM3) < 3; // no refill TT && (volTripTank_m3 < obj.volTTLastStand_m3) 
                }

                if (_isnewtrip)
                {
                    InitTrip();
                    DepthLastStandM = depthBitM;
                    _volTtLastStandM3 = volTripTankM3;
                    TimeLastStandD = timeNewD;
                }
                else // Update
                {
                    _lengthLastStandM = Abs(depthBitM - DepthLastStandM);
                    DepthLastStandM = depthBitM;
                    _volTTdiffM3 = (volTripTankM3 - _volTtLastStandM3);
                    _volTtLastStandM3 = volTripTankM3;
                    if (_stateTripping == StateTripping.POOH)
                        _volSteelLastStandM3 = _lengthLastStandM * AreaDppoohM2;
                    else if (_stateTripping == StateTripping.RIH)
                        _volSteelLastStandM3 = _lengthLastStandM * AreaDprihM2;

                    VolTTcumM3 += ToDouble(_isOkLastStand) * _volTTdiffM3;
                    VolTTcumExpM3 += ToDouble(_isOkLastStand) * (int)_stateTripping * _volSteelLastStandM3;
                    VolTTcumErrM3 = VolTTcumM3 - VolTTcumExpM3;

                    _areaDpLastStandM2 = (int)_stateTripping * _volTTdiffM3 / _lengthLastStandM;
                    NumStand += 1;

                    if (_isOkLastStand)
                    {
                        _numOkStand += 1;
                        var forget = Max(FactorForget, 1.0 / _numOkStand);
                        _areaDpAverageM2 = (1 - forget) * _areaDpAverageM2 + forget * _areaDpLastStandM2;
                        _lengthStandAverageM = (1 - forget) * _lengthStandAverageM + forget * _lengthLastStandM;
                        if (_lengthStandAverageM < 23 || _lengthLastStandM < 23)
                            _logger.LogDebug($"Lenght last stand = {_lengthLastStandM} % .1f, average = {_lengthStandAverageM} % .1f");

                        if (_doDebugText)
                        {
                            _logger.LogDebug($"Updated average area of drill pipe = {_areaDpLastStandM2}, %.4f m2, filtered value = {_areaDpAverageM2}, '%.4f m2");
                            _logger.LogDebug($"Updated average length of stand = {_lengthLastStandM}, '%.1f m, filtered value = {_lengthStandAverageM}, '%.1f m");
                        }
                    }
                }
            }
            else   // Not new stand
                _volSteelLastStandM3 = 0;
            _stepTimeOldD = timeNewD;
            return SetOutput();
        }
    }
}
