﻿using Accord.Math;
using DtectLoss.Common;
using DtectLossRealtime.Calculations;
using DtectLossRealtime.Enums;
using DtectLossRealtime.Helpers;
using DtectLossRealtime.Models;
using static System.Math;
using static DtectLoss.Common.CommonConstants;

namespace DtectLossRealtime
{
    /// <summary>
    ///  this class calculates the standpipe pressure
    /// 
    /// </summary>
    public class ExpectedStandpipePres
    {
        public double PresSpExpectedBar { get; private set; }
        public bool IsCalibratingExpPresSp { get; private set; }
        public bool IsCalibratedExpPresSp { get; private set; }

        private readonly PiecewiseLinFit _pwlf;
        private double _presSpExpWithoutInjectionBar; // The one calculated from pwlf
        private double _pressSpInjectionBar; // Small additional term
        private double _presSpErrBar;
        private bool _isSteadyPresSp;
        private PresSp _parameters;
        private double _timePrevD;
        private RingBuffer? _presSpBuffer;
        private RingBuffer? _presSpExpectedBuffer;
        private RingBuffer? _flowInBuffer;
        private FilterDerivative? _filtFlow;
        private FilterDerivative? _filtPres;
        private LowPass? _filtPresErr;
        private double? _flowInDerLpmpsec;
        private double? _presSpDerBarpsec;
        private double _flowMaxMinLpm;

        private double _presMaxMinBar;
        private StateMachineTags _stateMachine; // 0=no flow, 1=ramp up, 2=steady non-zero flow, 3=ramp down 
        private bool _isSteadyFlowIn;
        private double _timesincePumpStartS;
        //private double _flowInPreviousLpm;
        //private double _presSpPreviousBar;
        private const int BufferSizeSamples = 200;
        private const double FlowMaxMinThresholdLpm = 100;
        private const double PresMaxMinThresholdBar = 10;
        private const int NvaluesforSteady = 5;
        private const double TimeDelayMaxS = 20;
        private const double FlowInMinLimitLpm = 0;
        private const double FlowInMaxLimitLpm = 10000;
        private const double ErrVal = MissingValueReplacement;

        public ExpectedStandpipePres(PiecewiseLinFit pwlf)
        {
            _pwlf = pwlf;
        }
        public void Initalize(PresSp parameters)
        {
            IsCalibratingExpPresSp = false;
            PresSpExpectedBar = 0;
            _presSpExpWithoutInjectionBar = 0;
            _pressSpInjectionBar = 0;
            _presSpErrBar = 0;
            _parameters = parameters;
            _timePrevD = 0;
            //_flowInPreviousLpm = 0;
            //_presSpPreviousBar = 0;
            _presSpBuffer = new RingBuffer(BufferSizeSamples);
            _presSpExpectedBuffer = new RingBuffer(BufferSizeSamples);
            _flowInBuffer = new RingBuffer(BufferSizeSamples);
            _filtFlow = new FilterDerivative(20);
            _filtPres = new FilterDerivative(20);
            _filtPresErr = new LowPass(1, 60);
            _flowInDerLpmpsec = 0;
            _presSpDerBarpsec = 0;
            _flowMaxMinLpm = 0;
            _presMaxMinBar = 0;
            IsCalibratedExpPresSp = false;
            IsCalibratingExpPresSp = false;
            _stateMachine = 0;
            _isSteadyPresSp = false;
            _isSteadyFlowIn = false;
            _pwlf.Initialize(6000, 0, 60, 50); // 6000, 0, 12, 50); // 20, 100
            _flowMaxMinLpm = 0;
            _presMaxMinBar = 0;
            _timesincePumpStartS = 0;
        }

        public void SoftReset()
        {
            IsCalibratingExpPresSp = false;
            PresSpExpectedBar = 0;
            _presSpExpWithoutInjectionBar = 0;
            _pressSpInjectionBar = 0;
            _presSpErrBar = 0;
            _timePrevD = 0;
            //_flowInPreviousLpm = 0;
            //_presSpPreviousBar = 0;
            _flowInDerLpmpsec = 0;
            _presSpDerBarpsec = 0;
            _flowMaxMinLpm = 0;
            _presMaxMinBar = 0;
            IsCalibratedExpPresSp = false;
            IsCalibratingExpPresSp = false;
            _stateMachine = 0;
            _isSteadyPresSp = false;
            _isSteadyFlowIn = false;
            _flowMaxMinLpm = 0;
            _presMaxMinBar = 0;
            _timesincePumpStartS = 0;
        }


        public PresSPexpOutput Step(double flowInLpm, double presSpBar, bool isDownlinking, AlarmState alarmState, double timeNewD)
        {
            // Assuming pre-filtered flow-in
            if (flowInLpm < FlowInMinLimitLpm || flowInLpm > FlowInMaxLimitLpm)
            {
                return GenerateOutput(presSpBar);
            }

            var dtS = timeNewD.DiffInSeconds(_timePrevD);
            _timePrevD = timeNewD;

            if (!isDownlinking && flowInLpm < 10000 && presSpBar < 1000) //true,
            {
                _flowInBuffer!.AddDataToCircularBuffer(timeNewD, flowInLpm);
                _presSpBuffer!.AddDataToCircularBuffer(timeNewD, presSpBar);
            }

            var flowVec = _flowInBuffer!.GetLastNValues(NvaluesforSteady);
            var presVec = _presSpBuffer!.GetLastNValues(NvaluesforSteady);
            _flowMaxMinLpm = flowVec.Max() - flowVec.Min();
            _presMaxMinBar = presVec.Max() - presVec.Min();
            _isSteadyFlowIn = _flowMaxMinLpm < FlowMaxMinThresholdLpm;
            _isSteadyPresSp = _presMaxMinBar < PresMaxMinThresholdBar;


            _flowInDerLpmpsec = _filtFlow!.Smoothderivative2ndorder(flowInLpm, timeNewD);
            _presSpDerBarpsec = _filtPres!.Smoothderivative2ndorder(presSpBar, timeNewD);

            const double flowZerolimLpm = 100;
            if (_stateMachine == StateMachineTags.NoFlow) //Zero flow = 0
            {
                if (_isSteadyFlowIn) //Steady
                {
                    if (flowInLpm > flowZerolimLpm)
                        _stateMachine = StateMachineTags.SteadyNonZeroFlow;
                    else
                        _stateMachine = StateMachineTags.NoFlow;
                }
                else
                {
                    if (flowInLpm > flowZerolimLpm)
                        _stateMachine = StateMachineTags.RampUp;
                    else
                        _stateMachine = StateMachineTags.NoFlow;
                }
            }

            else if (_stateMachine == StateMachineTags.RampUp) //Ramp up
            {
                if (_isSteadyFlowIn) //Steady
                {
                    if (flowInLpm > flowZerolimLpm)
                    {
                        _stateMachine = StateMachineTags.SteadyNonZeroFlow; //Steady non-zero flow
                        _timesincePumpStartS = 0;
                    }
                    else
                        _stateMachine = StateMachineTags.NoFlow; //Pumps off  
                }
                else if (_flowInDerLpmpsec < 0)
                    _stateMachine = StateMachineTags.RampDown; //Ramping down
                else
                    _stateMachine = StateMachineTags.RampUp; //Still ramping up
            }

            else if (_stateMachine == StateMachineTags.SteadyNonZeroFlow) //Steady flow
            {
                if (_isSteadyFlowIn) //Steady
                {
                    if (flowInLpm > flowZerolimLpm)
                    {
                        _stateMachine = StateMachineTags.SteadyNonZeroFlow; //Steady non-zero flow
                        _timesincePumpStartS += dtS;
                    }
                    else
                        _stateMachine = StateMachineTags.NoFlow; //Pumps off   
                }
                else if (_flowInDerLpmpsec < 0)
                    _stateMachine = StateMachineTags.RampDown;
                else
                    _stateMachine = StateMachineTags.RampUp;
            }
            else if (_stateMachine == StateMachineTags.RampDown) //Ramp down
            {
                if (_isSteadyFlowIn)
                {
                    if (flowInLpm > flowZerolimLpm)
                        _stateMachine = StateMachineTags.SteadyNonZeroFlow;
                    else
                        _stateMachine = StateMachineTags.NoFlow;
                }
                else if (_flowInDerLpmpsec < 0)
                    _stateMachine = StateMachineTags.RampDown; //Ramping down
                else
                    _stateMachine = StateMachineTags.RampUp; //Still ramping up 
            }

            _presSpExpWithoutInjectionBar = _pwlf.InterpolWithExtrapolation(flowInLpm);
            if (_presSpExpWithoutInjectionBar.Equals(ErrVal))
            {
                _presSpExpWithoutInjectionBar = presSpBar;
                IsCalibratingExpPresSp = true;
            }
            else
            {
                if (!isDownlinking &&
                    (_stateMachine == StateMachineTags.NoFlow || _stateMachine == StateMachineTags.SteadyNonZeroFlow) &&
                    _isSteadyPresSp && ((int) alarmState < 2))
                {
                    var preserrBar = _presSpExpWithoutInjectionBar + _pressSpInjectionBar - presSpBar;
                    _filtPresErr!.FilterForCustomDt(preserrBar, dtS, 0);
                    _presSpErrBar = _filtPresErr.FilteredSignal ?? 0;
                    const double timeConstantS = 120.0;
                    const double dP0dtmaxBarps = 1 / 60.0; //~1 bar/min
                    const double p0InjMaxBar = 20.0; //10
                    if (dtS < 10)
                    {
                        var dP0dtBarps = preserrBar / timeConstantS;
                        dP0dtBarps = Sign(dP0dtBarps) * Min(dP0dtmaxBarps, Abs(dP0dtBarps));
                        _pressSpInjectionBar = Max(-p0InjMaxBar,
                            Min(p0InjMaxBar, _pressSpInjectionBar - dtS * dP0dtBarps));
                    }
                    IsCalibratingExpPresSp = (Abs(_presSpErrBar) >= _parameters.PresDeadbandCalibrateBar);
                }
                else
                {
                    IsCalibratingExpPresSp = false;
                    _presSpErrBar = 0;
                }
            }
            if (IsCalibratingExpPresSp)
                _pwlf.Train(flowInLpm, presSpBar);

            if (_presSpExpWithoutInjectionBar > 0)
                PresSpExpectedBar = Max(0, _presSpExpWithoutInjectionBar + _pressSpInjectionBar);
            else
                PresSpExpectedBar = presSpBar;

            if (IsCalibratedExpPresSp || _pwlf.IsTrained)
                IsCalibratedExpPresSp = true;
            else
                IsCalibratedExpPresSp = false;
            if (!(isDownlinking) && flowInLpm < FlowInMaxLimitLpm && presSpBar < 1000) //true,
                _presSpExpectedBuffer!.AddDataToCircularBuffer(timeNewD, PresSpExpectedBar);

            return GenerateOutput(presSpBar);
        }
        public PresSPexpOutput GenerateOutput(double presSpBar)
        {
            var presSPexpOutput = new PresSPexpOutput
            {
                PresSpExpectedBar = PresSpExpectedBar,
                PresSpExpWithoutInjectionBar = _presSpExpWithoutInjectionBar,
                PressSpInjectionBar = _pressSpInjectionBar,
                PresSpErrBar = _presSpErrBar,
                IsCalibratedExpPresSp = IsCalibratedExpPresSp,
                IsCalibratingExpPresSp = IsCalibratingExpPresSp,
                IsSteadyPresSp = _isSteadyPresSp,
                IsSteadyandNonZeroPresSp = _isSteadyPresSp && (presSpBar > 20),
                IsSteadyFlowIn = _isSteadyFlowIn,
                StateMachine = _stateMachine,
                PresSpDerBarpsec = _presSpDerBarpsec ?? 0,
            };
            return presSPexpOutput;
        }
    }
}