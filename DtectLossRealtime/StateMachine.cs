﻿using System;
using DtectLoss.Common;
using DtectLossRealtime.Enums;
using DtectLossRealtime.Models;
using Microsoft.Extensions.Logging;

namespace DtectLossRealtime
{
    /// <summary>
    /// 
    /// STATE_MACHINE_OBJ is a the state machine class.
    ///  it can be updated with new data, and holds the current and previous
    ///  drilling states.
    /// 
    ///  the drilling states allowed are defined in the enum in DrillingStates.m.
    ///  for instance:
    ///  bad data, drilling, tripping, circulating, not circulating(flow check etc), other.
    /// 
    /// Consider adding tripping as a new mode rather than a state.
    /// </summary>
    public class StateMachine
    {
        public DrillingStates CurrentState { get; private set; }
        public DrillingStates PreviousState { get; private set; }
        public bool IsOnBottom { get; set; }

        private readonly ILogger<StateMachine> _logger;
        private const double FlowThresholdLpm = 50; // [lpm] if the flow rate from the rig pumps are higher than this value -> circulating
        private const double DepthThresholdM = 10; // [m] if the bit close than this value to the holeDepth -> onBottom
        //const double hookloadThreshold = 50; // [ton] if the hookload is small, then the drill pipe is in slips
        private const double RpmThreshold = 50; // [rpm] if the rpm is small, then the drill pipe is not rotating
        private const double VelThresholdMph = 80; // [m/hr] if the block vel is small, then we are not tripping
        //const double TimeThresholdTripping = 1; //[min]
        //const double TimeThresholdOnBottom = 6; //[min]
        private const double DepthBitAndHoleNoiseM = 0.01;
        private const double TimeThresholdS = 10 * 60; // 6Time before state is changed from drilling to flow check (to avoid flow check mode during connections)
        private bool _isCirculating;
        private bool _isConstBitPos;
        // InSlips,
        public bool IsRotating;
        public bool IsDataValid;
        public bool IsPooh;
        public bool IsRih;
        // ConstFlowIn,
        private double _timePumpsOffS;
        private double _timePipeNotMovingS;

        public StateMachine(ILogger<StateMachine> logger)
        {
            _logger = logger;
            CurrentState = DrillingStates.Other;
            PreviousState = DrillingStates.BadData;
            IsDataValid = false;
            _isCirculating = false;
            _isConstBitPos = false;
            //InSlips = false;
            IsRotating = false;
            IsPooh = false;
            IsRih = false;
            //ConstFlowIn = false;
            _timePumpsOffS = 0; // Time since pumps were turned off
            _timePipeNotMovingS = 0; // Time since pipe was moved
        }
        public void ChangeState(DrillingStates toState)
        {
            PreviousState = CurrentState;
            CurrentState = toState;
        }

        public void Update(WashedData washedData, double dtS)
        {
            IsDataValid = washedData.Values[V.FlowInLpm] >= 0 
                          && washedData.Values[V.DepthBitM] >= 0 
                          && washedData.Values[V.DepthHoleM] >= 0 
                          && washedData.Values[V.DepthHoleM] + DepthBitAndHoleNoiseM >= washedData.Values[V.DepthBitM];

            if (IsOnBottom)
                IsOnBottom = !(washedData.Values[V.DepthHoleM] - washedData.Values[V.DepthBitM] >= DepthThresholdM);
            else
                IsOnBottom = washedData.Values[V.DepthHoleM] - washedData.Values[V.DepthBitM] < DepthThresholdM 
                             && washedData.Values[V.FlowInLpm] > FlowThresholdLpm;

            if (washedData.Values[V.FlowInLpm] >= FlowThresholdLpm)
                _timePumpsOffS = 0;
            else
                _timePumpsOffS += dtS;

            if (Math.Abs(washedData.Values[V.VelBlockMph]) > VelThresholdMph)
                _timePipeNotMovingS = 0;
            else
                _timePipeNotMovingS += dtS;

            _isCirculating = (washedData.Values[V.FlowInLpm] >= FlowThresholdLpm);
            _isConstBitPos = (Math.Abs(washedData.Values[V.VelBlockMph]) < VelThresholdMph); // TODO from matlab
            // obj.InSlips = (hookload <= obj.hookloadThreshold);
            IsRotating = washedData.Values.ContainsKey(V.Rpm) && (washedData.Values[V.Rpm] > RpmThreshold);
            IsPooh = !_isConstBitPos & (washedData.Values[V.VelBlockMph] < 0);// TODO from matlab
            IsRih = !_isConstBitPos & (washedData.Values[V.VelBlockMph] > 0); // TODO from matlab
            // obj.ConstFlowIn = false; // TODO from matlab

            if (!IsDataValid)
                ChangeState(DrillingStates.BadData);
            else if (IsOnBottom)
            {
                if (_isCirculating) // Drilling or sliding
                    ChangeState(DrillingStates.Drilling);
                else if (_timePumpsOffS > TimeThresholdS) // No flow
                    ChangeState(DrillingStates.FlowCheck);
            }
            else
            {
                if (!_isConstBitPos) // tripping
                    ChangeState(DrillingStates.Tripping);
                else if (_timePipeNotMovingS > TimeThresholdS) // No tripping
                {
                    if (_isCirculating)
                        ChangeState(DrillingStates.Conditioning);
                    else  // No flow
                        ChangeState(DrillingStates.FlowCheck);
                }
            }
        }
    }
}
