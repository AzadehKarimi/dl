﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accord.Math;
using Accord.Statistics;
using Accord.Statistics.Models.Regression.Linear;
using DtectLoss.Common;
using DtectLossRealtime.Calculations;
using DtectLossRealtime.Enums;
using DtectLossRealtime.Helpers;
using DtectLossRealtime.Models;
using Microsoft.Extensions.Logging;
using static System.Double;
using static System.Math;
using static DtectLossRealtime.Helpers.MathFunctions;

namespace DtectLossRealtime
{
    /// <summary>
    /// FLOWBACK_RECORDER_OBJ Keep track of previous flowbacks and
    /// corresponding flow-rates.
    /// A number of flowbacks and corresponding flow-rates can be stored.
    /// The volume of the flowback is computed as the difference between
    /// the gain/loss volume just before a pumps off event, and the volume
    /// just before the pumps are turned back on. The average flow-rate in
    /// a period before pumps off is used as the corresponding flow-rate.
    /// The flow-rate must be within a defined min/max window during a
    /// time-period for the flowback to be valid and recorded.
    /// </summary>
    public class FlowBackRecorder
    {
        //TODO: Many parameters are only parameters due to usage from unit tests. Consider rewriting to avoid this
        public double TimeSamplingS { get; } = 3;
        public double QWinLpm { get; } = 250;
        public int[] DelayArraySamples { get; } = Vector.Range(110, 0);
        public int NFbs { get; private set; }
        public double MinVol { get; private set; }
        public double MinFlow { get; private set; }
        public double FlowTol { get; private set; }

        public int NRbFlow { get; private set; }
        public int NRbVol { get; private set; }
        public RingBuffer? RbFlowIn { get; private set; }
        public RingBuffer? RbVol { get; private set; }
        public RingBuffer? RbExpVol { get; private set; }
        public RingBuffer? RbPresSp { get; private set; }


        public int IdxStore { get; set; }
        public int IdxCon { get; set; }
        public bool IsInit { get; set; }
        public double[,] FlowInStore { get; private set; } = { };
        public double[,] TimeFlowInStore { get; private set; } = { };
        public double[,] VolStore { get; private set; } = { };
        public double[,] TimeVolStore { get; private set; } = { };
        public double[,] ExpVolStore { get; private set; } = { };

        public double[,] TimeExpVolStore { get; private set; } = { };

        public double[] KStore { get; private set; } = { };
        public double[] V0Store { get; private set; } = { };
        public double[] QStore { get; private set; } = { };
        public int[] PumpOffIdxStore { get; private set; } = { };
        public int[] VolOffIdxStore { get; private set; } = { };

        public LinReg? Mdl { get; private set; }
        public CirculationState? Cs { get; private set; }
        public CirculationStates StateAtPrevTime { get; private set; }
        public double TimeSpentAtState { get; private set; }
        public PitVol Parameters { get; private set; }
        public bool PumpsOn { get; private set; }

        private double[,] _flowInCon = { };
        private double[,] _timeFlowInCon = { };
        private double[,] _volCon = { };
        private double[,] _timeVolCon = { };
        private double[,] _expVolCon = { };
        private double[,] _timeExpVolCon = { };
        private bool[] _isValidConnection = { };
        private int[] _nValidDataPoints = { };
        private double[,] _yStore = { };
        private double[,] _store = { };
        private readonly ILogger<FlowBackRecorder> _logger;
        private readonly LinReg _linRegTmp;
        private double[] _pumpRampDownFilt = { };
        private double[] _expV0Store = { }; // [m3] size: [nFbs x 1] vector for storing volumes of expected flowbacks
        private int[] _expVolOffIdxStore = { }; // [int] size: [nFbs x 1] index to when the pump off event has been identified.
        private bool _timeSpentOfFlowbackExceeded; // [boolean]
        private bool _prevTimeSpentOfFlowbackExceeded; // [boolean]
        private bool _timeSpentOfRampUpExceeded; // [boolean]
        private bool _prevTimeSpentOfRampUpExceeded; // [boolean]
        private bool _storeConnectionDataTrigger; // [boolean]
        private bool _updateFapTimeTrigger; // [boolean]


        private const double ErrVal = CommonConstants.MissingValueReplacement;
        private const double FlowbackTimerLimS = 500; // [s] when the pumps have been shut down for this period, then it is time to store the flow back data.
        private const double RampUpTimerLimS = 600; // [s] wait this duration after pumps have been started before storing connection data.
        private const double DbLimM3 = 0.3; // [m3] limit for when drainback starts
        private const double FlowbackDurationLimS = 100; // [s] lower limit for how long a flowback must be to be valid.
        private const double VolDiffLimM3PerS = 10e3 / 60e3; // [m3/s] upper limit for change in volGainLoss.Flowbacks with a higher rate of change is not valid.
        private const double TimeVolSamplingLimS = 5; // [s] if volGainLoss is sampled with less than this period, then allow a higher rate of change in volume of a flowback.
        private const double MultiplierHighSamplingOfVolGainLoss = 2; // [float]
        private const double VolChangeLimM3 = 2; // [m3] upper limit for range of volGainLoss in the period before flowback starts
        private readonly double _volWindowDurationD = 300.0.SecondsToOADate(); // [days] window which is used to consider the range of volGainLoss before flowback starts.
        private const int NMinFlowValuesForInit = 10; // [int] minimium number of datapoints necessary to store in the ringbuffer before considering it ready for use
        private const int NMinVolValuesForInit = 10;  // [int] minimium number of datapoints necessary to store in the ringbuffer before considering it ready for use
        // parameters for the analyzeRamp method
        private const int NRunningAvgWin = 5; // [int] number of samples to use for the running avg filtering.
        private const double FlowChangeTolLpmPerS = 32; // [lpm/s]
        private const double MaxNPlateaus = 2; // maximum number of plateaus before computing steady state flow-rate.
        private const double PlateauTimeDefS = 10; // [s] duration which defines a plateau
        private const double SsDurationS = 180; // [s] duration with steady flow-rate defining steady state
        private const double StartSearchLpm = 200; // [lpm] flow-rate threshold which defines the start for the search for a plateau
        private const int NSamplesToDiscardAtEndOfFlowIn = 5; // [int] number of samples to discard at the end of a flowIn ramp down time series to avoid including some of the ramp up.
        // parameter used by the getFlowbackStatistics method
        // parameters used by the calcParams method
        private const double LeastSquareTimeLimitS = 200; // [s] upper time limit for flowback data which shall be included in the least squares fit.
        // parameters for connection data and learning
        private const int NConToRecord = 8; // number of connections which are recorded and used for machine learning
        private const int MaxDelaySamples = 110; // [int]
        // const double delayArray_samples = flip([1:1:24  28:5:110]); // [array of int]
        private const int NDelays = 110; // [int]
        private const double TimeDiffBetweenStackedTimeSeriesS = 180; // [s]
        private const double TimeToIncludeBeforePumpShutDownS = 400; // [s]
        private const double FlowInMaxLimLpm = 10000; // [lpm]
        private const double VolGainLossErrorLimM3 = 1; // [m3]
        private const double DeltaTimeForMissingDataS = 120; // [s]
        private const double ConnectionDurationLimS = 20; // [s]
        private const int MultiplierForVolDiffLimDuringConnection = 3; // [int] 
        private const bool DispDebug = false;

        public FlowBackRecorder(ILogger<FlowBackRecorder> logger, LinReg linRegTmp)
        {
            _logger = logger;
            _linRegTmp = linRegTmp;
        }

        /// <summary>
        /// the number of samples for the ringbuffer for flow - rate is
        /// computed using timeDuration(time - window for recording of
        /// 
        /// flow - rate and gain / loss volume) [s], and dtFlowrate(average
        /// time - step for the flow-rate) [s].Similar for the gain/ loss
        /// volume.
        /// </summary>
        public void Initialize(int nFlowbacks, double minVolume, double minFlowRate, double flowTolerance,
            double dtFlowrate, double dtVolGl,
            double timeDuration, PitVol parameters)
        {
            _logger.LogDebug("Flowback recorder: Initializing");
            Parameters = parameters;
            NFbs = nFlowbacks;
            MinVol = minVolume;
            MinFlow = minFlowRate;
            FlowTol = flowTolerance;

            NRbFlow = (int)Ceiling(timeDuration / dtFlowrate);
            NRbVol = (int)Ceiling(timeDuration / dtVolGl);
            RbFlowIn = new RingBuffer(NRbFlow);
            RbVol = new RingBuffer(NRbVol);
            RbPresSp = new RingBuffer(NRbFlow);
            RbExpVol = new RingBuffer(NRbFlow);

            IdxStore = 0;

            FlowInStore = new double[nFlowbacks, NRbFlow];
            TimeFlowInStore = new double[nFlowbacks, NRbFlow];
            VolStore = new double[nFlowbacks, NRbVol];
            TimeVolStore = new double[nFlowbacks, NRbVol];
            ExpVolStore = new double[nFlowbacks, NRbFlow];
            TimeExpVolStore = new double[nFlowbacks, NRbFlow];

            IdxCon = 0;
            _flowInCon = Matrix.Create(NConToRecord, NRbFlow, NaN);
            _timeFlowInCon = Matrix.Create(NConToRecord, NRbFlow, NaN);
            _volCon = Matrix.Create(NConToRecord, NRbFlow, NaN);
            _timeVolCon = Matrix.Create(NConToRecord, NRbFlow, NaN);
            _expVolCon = Matrix.Create(NConToRecord, NRbFlow, NaN);
            _timeExpVolCon = Matrix.Create(NConToRecord, NRbFlow, NaN);
            _isValidConnection = new bool[NConToRecord];
            _nValidDataPoints = new int[NConToRecord];

            KStore = new double[nFlowbacks];
            V0Store = new double[nFlowbacks];
            _expV0Store = new double[nFlowbacks];
            QStore = new double[nFlowbacks];
            PumpOffIdxStore = new int[nFlowbacks];
            VolOffIdxStore = new int[nFlowbacks];
            _expVolOffIdxStore = new int[nFlowbacks];

            _yStore = new double[nFlowbacks, NRbVol];
            _store = new double[nFlowbacks, NRbVol];

            IsInit = true;

            Cs = new CirculationState();
            StateAtPrevTime = Cs.CurrentState;
            TimeSpentAtState = 0;
            _timeSpentOfFlowbackExceeded = false;
            _prevTimeSpentOfFlowbackExceeded = false;
            _updateFapTimeTrigger = false;
            _timeSpentOfRampUpExceeded = false;
            _prevTimeSpentOfRampUpExceeded = false;
            _storeConnectionDataTrigger = false;
            PumpsOn = false;
            Mdl = null;
        }

        public void SoftReset()
        {
            IdxStore = 0;
            FlowInStore = new double[NFbs, NRbFlow];
            TimeFlowInStore = new double[NFbs, NRbFlow];
            VolStore = new double[NFbs, NRbVol];
            TimeVolStore = new double[NFbs, NRbVol];
            ExpVolStore = new double[NFbs, NRbFlow];
            TimeExpVolStore = new double[NFbs, NRbFlow];
            IdxCon = 0;
            _flowInCon = Matrix.Create(NConToRecord, NRbFlow, NaN);
            _timeFlowInCon = Matrix.Create(NConToRecord, NRbFlow, NaN);
            _volCon = Matrix.Create(NConToRecord, NRbFlow, NaN);
            _timeVolCon = Matrix.Create(NConToRecord, NRbFlow, NaN);
            _expVolCon = Matrix.Create(NConToRecord, NRbFlow, NaN);
            _timeExpVolCon = Matrix.Create(NConToRecord, NRbFlow, NaN);
            _isValidConnection = new bool[NConToRecord];
            _nValidDataPoints = new int[NConToRecord];
            KStore = new double[NFbs];
            V0Store = new double[NFbs];
            _expV0Store = new double[NFbs];
            QStore = new double[NFbs];
            PumpOffIdxStore = new int[NFbs];
            VolOffIdxStore = new int[NFbs];
            _expVolOffIdxStore = new int[NFbs];
            _yStore = new double[NFbs, NRbVol];
            _store = new double[NFbs, NRbVol];
            IsInit = true;
            Cs = new CirculationState();
            StateAtPrevTime = Cs.CurrentState;
            TimeSpentAtState = 0;
            _timeSpentOfFlowbackExceeded = false;
            _prevTimeSpentOfFlowbackExceeded = false;
            _updateFapTimeTrigger = false;
            _timeSpentOfRampUpExceeded = false;
            _prevTimeSpentOfRampUpExceeded = false;
            _storeConnectionDataTrigger = false;
            PumpsOn = false;
            Mdl = null;
        }

        public void AddVolToRingbuffer(double volTimeD, double volM3)
        {
            // only add volume to ringbuffer is new timestamp is greater
            // than the previous.
            if (volM3.Equals(CommonConstants.MissingValueReplacement) || volTimeD.Equals(CommonConstants.MissingValueReplacement)) return;
            RbVol!.AddDataToCircularBuffer(volTimeD, volM3);
            UpdateInitState();
        }

        public void AddExpVolToRingbuffer(double? volTimeD, double? volM3)
        {
            // only add expected volume to ringbuffer is new timestamp is
            // greater than the previous.
            if (volTimeD != null && volM3 != null && !volM3.Equals(CommonConstants.MissingValueReplacement) && !volTimeD.Equals(CommonConstants.MissingValueReplacement))
                RbExpVol!.AddDataToCircularBuffer(volTimeD.Value, volM3.Value);
        }

        public void AddFlowToRingbuffer(double flowInTimeD, double flowInRateLpm)
        {
            // only add flow-rate to ringbuffer is new timestamp is greater than the previous.
            RbFlowIn!.AddDataToCircularBuffer(flowInTimeD, flowInRateLpm);
            UpdateInitState();
        }

        public void AddPresToRingbuffer(double presSpTimeD, double presSpBar)
        {
            RbPresSp!.AddDataToCircularBuffer(presSpTimeD, presSpBar);
        }

        /// <summary>
        /// update state machine for circulation
        /// </summary>
        public void UpdateCirculationState(double flowInLpm, double depthBitM, double depthHoleM)
        {
            StateAtPrevTime = Cs!.CurrentState;
            Cs.StateMachine(flowInLpm, depthBitM, depthHoleM);
            if (PumpHasBeenTurnedOn())
                PumpsOn = true;
            else if (PumpHasBeenTurnedOff())
                PumpsOn = false;
            _logger.LogDebug("Flowback recorder: CircState:{0}, PumpOn:{1}", typeof(CirculationState).ToString(), PumpsOn.ToString());
        }

        public bool PumpHasBeenTurnedOn()
        {
            return Cs!.CurrentState == CirculationStates.Circulating && StateAtPrevTime == CirculationStates.NotCirculating;
        }

        public bool PumpHasBeenTurnedOff()
        {
            return Cs!.CurrentState == CirculationStates.NotCirculating && (StateAtPrevTime == CirculationStates.Circulating
                    || StateAtPrevTime == CirculationStates.Drilling);
        }

        public void UpdateTimeSpentAtState(double timeStepS)
        {
            if (PumpHasBeenTurnedOn() || PumpHasBeenTurnedOff())
            {
                TimeSpentAtState = 0;
                _timeSpentOfFlowbackExceeded = false;
                _prevTimeSpentOfFlowbackExceeded = false;
                _timeSpentOfRampUpExceeded = false;
                _prevTimeSpentOfRampUpExceeded = false;
            }
            else
                TimeSpentAtState += timeStepS;

            _prevTimeSpentOfFlowbackExceeded = _timeSpentOfFlowbackExceeded;

            if (TimeSpentAtState > FlowbackTimerLimS)
                _timeSpentOfFlowbackExceeded = true;

            _prevTimeSpentOfRampUpExceeded = _timeSpentOfRampUpExceeded;

            if (TimeSpentAtState > RampUpTimerLimS)
                _timeSpentOfRampUpExceeded = true;

            _updateFapTimeTrigger = !_prevTimeSpentOfFlowbackExceeded && _timeSpentOfFlowbackExceeded && Cs!.CurrentState == CirculationStates.NotCirculating;
            _storeConnectionDataTrigger = !_prevTimeSpentOfRampUpExceeded && _timeSpentOfRampUpExceeded && Cs!.CurrentState != CirculationStates.NotCirculating;
        }

        public void UpdateInitState()
        {
            if (RbFlowIn!.TbufferD.Count(x => !x.Equals(ErrVal)) > NMinFlowValuesForInit || 
                RbVol!.TbufferD.Count(x => !x.Equals(ErrVal)) > NMinVolValuesForInit)
                IsInit = false;
            else
                IsInit = true;
        }

        public void ResetInitState()
        {
            IsInit = true;
        }

        /// <summary>
        /// make sure that the data storages are reset before storing any
        /// new data
        /// </summary>
        public void ClearStoredDataAtCurrentIndex()
        {
            FlowInStore.SetRow(IdxStore, new double[NRbFlow]);
            TimeFlowInStore.SetRow(IdxStore, new double[NRbFlow]);
            VolStore.SetRow(IdxStore, new double[NRbVol]);
            TimeVolStore.SetRow(IdxStore, new double[NRbVol]);
            ExpVolStore.SetRow(IdxStore, new double[NRbFlow]);
            TimeExpVolStore.SetRow(IdxStore, new double[NRbFlow]);
            KStore[IdxStore] = 0;
            V0Store[IdxStore] = 0;
            _expV0Store[IdxStore] = 0;
            QStore[IdxStore] = 0;
            PumpOffIdxStore[IdxStore] = 0;
            VolOffIdxStore[IdxStore] = 0;
            _expVolOffIdxStore[IdxStore] = 0;
            _yStore.SetRow(IdxStore, new double[NRbVol]);
            _store.SetRow(IdxStore, new double[NRbVol]);
        }

        public void ClearStoredConnectionDataAtCurrentIndex()
        {
            _flowInCon.SetRow(IdxCon, Vector.Create(NRbFlow, NaN));
            _timeFlowInCon.SetRow(IdxCon, Vector.Create(NRbFlow, NaN));
            _volCon.SetRow(IdxCon, Vector.Create(NRbFlow, NaN));
            _timeVolCon.SetRow(IdxCon, Vector.Create(NRbFlow, NaN));
            _expVolCon.SetRow(IdxCon, Vector.Create(NRbFlow, NaN));
            _timeExpVolCon.SetRow(IdxCon, Vector.Create(NRbFlow, NaN));
            _isValidConnection[IdxCon] = false;
            _nValidDataPoints[IdxCon] = 0;
        }


        public void StoreFlowbackData()
        {
            var (t, m) = StoreOnlyNonEmptyData(RbFlowIn!);
            var (u, w) = StoreOnlyNonEmptyData(RbVol!);
            var (x, y) = StoreOnlyNonEmptyData(RbExpVol!);
            TimeFlowInStore.SetRow(IdxStore, t);
            FlowInStore.SetRow(IdxStore, m);
            TimeVolStore.SetRow(IdxStore, u);
            VolStore.SetRow(IdxStore, w);
            TimeExpVolStore.SetRow(IdxStore, x);
            ExpVolStore.SetRow(IdxStore, y);
        }

        public void StoreConnectionData(double[] timeBaseD, double[] flowInCurrentConClipped, double[] volCurrentConClipped, double[] expVolCurrentConClipped)
        {
            var nDataPoints = timeBaseD.Length;

            var newTimeFlowInConRow = _timeFlowInCon.GetRow(IdxCon);
            Array.Copy(timeBaseD, newTimeFlowInConRow, nDataPoints);
            _timeFlowInCon.SetRow(IdxCon, newTimeFlowInConRow);

            var newTimeVolConRow = _timeVolCon.GetRow(IdxCon);
            Array.Copy(timeBaseD, newTimeVolConRow, nDataPoints);
            _timeVolCon.SetRow(IdxCon, newTimeVolConRow);

            var newTimeExpVolConRow = _timeExpVolCon.GetRow(IdxCon);
            Array.Copy(timeBaseD, newTimeExpVolConRow, nDataPoints);
            _timeExpVolCon.SetRow(IdxCon, newTimeExpVolConRow);

            var newFlowInConRow = _flowInCon.GetRow(IdxCon);
            Array.Copy(flowInCurrentConClipped, newFlowInConRow, nDataPoints);
            _flowInCon.SetRow(IdxCon, newFlowInConRow);

            var newVolConRow = _volCon.GetRow(IdxCon);
            Array.Copy(volCurrentConClipped, newVolConRow, nDataPoints);
            _volCon.SetRow(IdxCon, newVolConRow);

            var newExpVolConRow = _expVolCon.GetRow(IdxCon);
            Array.Copy(expVolCurrentConClipped, newExpVolConRow, nDataPoints);
            _expVolCon.SetRow(IdxCon, newExpVolConRow);

            _isValidConnection[IdxCon] = true;
            _nValidDataPoints[IdxCon] = nDataPoints;
        }

        public (double[] t, double[] v) StoreOnlyNonEmptyData(RingBuffer ringBuffer)
        {
            double[] t;
            double[] v;
            var deringbufferAndResample = ringBuffer.DeringbufferAndResample(true);
            var tResampled = deringbufferAndResample.TbufferDStraightResampled;
            var vResampled = deringbufferAndResample.VbufferStraightResampled;
            if (tResampled.Any() || vResampled.Any())
            {
                t = tResampled;
                v = vResampled;
            }
            else
            {
                t = new double[ringBuffer.BufferSizeSamples]; //zeros(1, ringBuffer.bufferSizeSamples);
                v = new double[ringBuffer.BufferSizeSamples]; //zeros(1, ringBuffer.bufferSizeSamples);
            }
            return (t, v);
        }

        /// <summary>
        /// analyze the rig pump flow rate time series which is stored
        /// when starting to ramp up the rig pumps.Identify if it is
        /// usable to calculate parameters for the flowback. If so, then
        /// return with the index to when ramping down starts. If not,
        /// then return empty rIdx. If the the sequence is valid, then
        /// find the flow-rate used when drilling (qDrill[lpm]) before
        /// ramping down the pumps.
        /// Filter with running avg of 5 samples on the flipped/reversed
        /// flow-rate
        /// </summary>
        public (double? timeOfPumpRamp, double? qDrillLpm, int? rIdx) AnalyzeRamp(double[] flowInTimeD, double[] flowInLpm, string rampType)
        {
            double? timeOfPumpRamp = null;
            double? qDrillLpm;
            int? rIdx;
            if (Equals(rampType, "down")) //TODO: to fix Filter function ??
            {
                var bArg = Vector.Ones(NRunningAvgWin).Select(x => x / NRunningAvgWin).ToArray();
                _pumpRampDownFilt = Filter1D(
                    bArg, 
                    new double[] { 1 }, 
                    (flowInLpm[..^(NSamplesToDiscardAtEndOfFlowIn)]).Reverse().ToArray()
                    );

            }
            else // it is ramp up and the flow - rate is not flipped.
            {
                var bArg = Vector.Ones(NRunningAvgWin).Select(x => x / NRunningAvgWin).ToArray();
                _pumpRampDownFilt = Filter1D(bArg, new double[] { 1 }, flowInLpm);
            }

            var n = _pumpRampDownFilt.Length;
            var dtS = flowInTimeD.Reverse().ToArray().Diff().Select(x => Abs(x.OADateToSeconds())).ToArray();
            double? meanDtS = null;
            int? nSsSamples = null; // number of samples which defines steady state
            if (dtS.Any())
            {
                meanDtS = dtS.Average();
                nSsSamples = (int)Ceiling(SsDurationS / meanDtS.Value);
            }

            var (counterPlateau, indexToMostRecentPlateau) = FindMostRecentPlateau(meanDtS, nSsSamples, n, dtS);

            (rIdx, qDrillLpm) = IsFlowStable(counterPlateau, indexToMostRecentPlateau, nSsSamples, rampType, n, flowInLpm);
            if (rIdx.HasValue)
                timeOfPumpRamp = flowInTimeD[rIdx.Value];

            return (timeOfPumpRamp, qDrillLpm, rIdx);
        }

        /// <summary>
        /// traverse look for a time-period where the
        /// flow - rate has stabilized
        /// </summary>
        public (int cPlateau, int idxPlateauStart) FindMostRecentPlateau(double? meanDtS, int? nSsSamples, int n, double[] dtS)
        {
            var nPlateaus = 0; // [-] number of plateaus encountered
            var idxPlateauStart = 0; // index to when the plateau starts
            var cPlateau = 0;  // [samples] counter for which starts when encountering a plateau
            if (!meanDtS.HasValue || !nSsSamples.HasValue)
                return (cPlateau, idxPlateauStart);

            var nDefPlateau = (int)Ceiling(PlateauTimeDefS / meanDtS.Value); // number of consecutive samples which defines a plateau
            var searchIdx = Max(1, Array.FindIndex(_pumpRampDownFilt, x => x > StartSearchLpm)); // index where search for plateau starts
            if (searchIdx == -1)
            {
                // if no pump shut-down in sequence, then return empty.
                return (cPlateau, idxPlateauStart);
            }
            // loop through the reversed and filtered ramp-down sequence.
            // End loop if: at end of sequence; if maximum number of
            // plateaus have been encountered; or if a plateau has lasted
            // long enough to consider the flow rate as steady
            while (searchIdx < n && nPlateaus <= MaxNPlateaus && cPlateau <= nSsSamples)
            {
                // compute the change in flow rate per second
                var dfdt = (_pumpRampDownFilt[searchIdx] - _pumpRampDownFilt[searchIdx - 1]) / dtS[searchIdx - 1];
                if (Abs(dfdt) < FlowChangeTolLpmPerS)
                {
                    // start counting if the rate of change is small enough
                    cPlateau += 1;
                }
                else if (dfdt < -FlowChangeTolLpmPerS)
                {
                    // if the flow is ramped down again, then return empty
                    return (cPlateau, idxPlateauStart);
                }
                else
                {
                    // reset if the flow rate is not considered stable
                    cPlateau = 0;
                }

                // if enough samples are counted, then count this as a
                // plateau
                if (cPlateau == nDefPlateau)
                {
                    nPlateaus = nPlateaus + 1;
                    // store index to where the plateau started
                    idxPlateauStart = searchIdx - nDefPlateau;
                }
                searchIdx += 1;
            }
            return (cPlateau, idxPlateauStart);
        }

        /// <summary>
        /// only return with values if the flow - rate is considered to be stable
        /// </summary>
        public (int? rIdx, double? qDrillLpm) IsFlowStable(int cPlateau, int idxPlateauStart, int? nSsSamples, string rampType, int n, double[] flowInLpm)
        {
            int? rIdx;
            if (!nSsSamples.HasValue)
                return (null, null);

            if (!(cPlateau > nSsSamples)) 
                return (null, null);
            // reverse index back to actual index for the start of the pump ramp down.
            if (Equals(rampType, "down"))
                rIdx = n - idxPlateauStart - 1  - NSamplesToDiscardAtEndOfFlowIn;
            else
                rIdx = n - idxPlateauStart - 1;

            // the flow rate is the mean in the period considered as steady state.
            double? qDrillLpm = (flowInLpm[Max(0, rIdx.Value - nSsSamples.Value)..(rIdx.Value+1)]).Average();
            return (rIdx, qDrillLpm);
        }

        /// <summary>
        /// find indicies to when pumps have been shut off.
        /// </summary>
        public void StoreShutDownIndicies(double? timeOfPumpRamp)
        {
            var pumpOffIdx = Array.FindLastIndex(FlowInStore.GetRow(IdxStore)[..^1], x => x > MinFlow);
            if (pumpOffIdx == -1)
                pumpOffIdx = 0;
            var pumpOffTime = TimeFlowInStore[IdxStore, pumpOffIdx];
            PumpOffIdxStore[IdxStore] = pumpOffIdx;

            int volOffIdx;
            int expVolOffIdx;
            if (timeOfPumpRamp != null)
            {
                // index to volGainLoss where the rig pumps start to ramp down
                volOffIdx = Array.FindLastIndex(TimeVolStore.GetRow(IdxStore), x => x <= timeOfPumpRamp);
                expVolOffIdx = Array.FindLastIndex(TimeExpVolStore.GetRow(IdxStore), x => x <= timeOfPumpRamp);
            }
            else
            {
                // index to pump off for volGainLoss
                volOffIdx = Max(0, Array.FindLastIndex(TimeVolStore.GetRow(IdxStore), x => x <= pumpOffTime) - 1);
                expVolOffIdx = Max(0, Array.FindLastIndex(TimeExpVolStore.GetRow(IdxStore), x => x <= pumpOffTime) - 1);
            }

            // if volOffIdx is empty, then the ramp down is not
            // recorded and the flowback data is not valid.The
            // index is anyhow stored to avoid errors later in the code.
            if (volOffIdx == -1)
                volOffIdx = 0;
            VolOffIdxStore[IdxStore] = volOffIdx;
            if (expVolOffIdx == -1)
                expVolOffIdx = 0;

            _expVolOffIdxStore[IdxStore] = expVolOffIdx;
        }

        public (bool isValidFlowback, double newV0) QualifyFlowback(double? qDrillLpm)
        {
            double newV0 = 0;
            if (!qDrillLpm.HasValue)
                return (false, newV0);

            var volOffIdx = VolOffIdxStore[IdxStore];
            if (volOffIdx <= 0)
                return (false, newV0);

            // compute the maximum rate of change in gain / loss
            // volume[m3 / s].
            var dt = TimeVolStore.GetRow(IdxStore).Skip(volOffIdx).ToArray().Diff();

            var meanDt = dt.Length > 0 && dt.Sum() > 0 ? dt.Average() : (double?)null;

            double? maxVolDiff = null;
            var vsd = VolStore.GetRow(IdxStore).Skip(volOffIdx).ToArray().Diff();
            if (vsd.Length > 0 && vsd.Sum() > 0)
            {
                maxVolDiff = vsd.Divide(dt).Select(x => Abs(x.SecondsToOADate())).Max();
            }

            if ((!meanDt.HasValue || meanDt > TimeVolSamplingLimS && (!maxVolDiff.HasValue || maxVolDiff >= VolDiffLimM3PerS)) 
                || (meanDt <= TimeVolSamplingLimS && (!maxVolDiff.HasValue || maxVolDiff >= MultiplierHighSamplingOfVolGainLoss * VolDiffLimM3PerS)))
            {
                //if (DispDebug)
                _logger.LogDebug("QualFlowBack: Time: {0}. Discarding flowback due to too high rate of change: meanDt:{1}, maxVolDiff:{2}", 
                    DateHelpers.DateToString(TimeVolStore[IdxStore, VolOffIdxStore[IdxStore]]), meanDt, maxVolDiff); //TODO:??
                return (false, newV0);
            }

            // check that the change in gain / loss in a time period before
            // flow back starts is sufficiently small before changing any
            // parameters.
            var volWindowIdx = Array.FindLastIndex(TimeVolStore.GetRow(IdxStore, null), x => x < TimeVolStore[IdxStore, volOffIdx] - _volWindowDurationD);
            if (volWindowIdx < 0)
                volWindowIdx = 0;
            if (VolStore.GetRow(IdxStore)[volWindowIdx..(volOffIdx + 1)].Max() -
                VolStore.GetRow(IdxStore)[volWindowIdx.. (volOffIdx + 1)].Min() >=
                VolChangeLimM3)
            {
                //if (DispDebug)
                _logger.LogDebug("QualFlowBack: Time: {0}. Discarding flowback due to sudden change in volume: Max:{1}, Min:{2}, volWindowIdx:{3}, volOffIdx:{4}", 
                    DateHelpers.DateToString(TimeVolStore[IdxStore, VolOffIdxStore[IdxStore]]), VolStore.GetRow(IdxStore)[volWindowIdx..(volOffIdx + 1)].Max(),
                    VolStore.GetRow(IdxStore)[volWindowIdx..(volOffIdx + 1)].Min(), volWindowIdx, volOffIdx);

                return (false, newV0);
            }

            // compute the duration of the flowback.If it is too short, then do not estimate any parameters.
            //double flowbackDuration_s = (timeVolStore[idxStore, Matrix.Columns(timeVolStore) - 1] - timeVolStore[idxStore, volOffIdx]) * CommonConstants.SecondsIn24Hours; // in seconds
            //double flowbackDuration_s = (timeVolStore[idxStore, timeVolStore.Columns() - 1].SubtractSeconds(timeVolStore[idxStore, volOffIdx])); // in seconds
            var flowbackDurationS = TimeVolStore[IdxStore, TimeVolStore.Columns() - 1].DiffInSeconds(TimeVolStore[IdxStore, volOffIdx]); // in seconds
            if (flowbackDurationS <= FlowbackDurationLimS)
            {
                //if (DispDebug)
                _logger.LogDebug("QualFlowBack: Time: {0}. Discarding flowback due to short duration ({1:F2} sec) of flowback. Min duration {2:F2} sec.", DateHelpers.DateToString(TimeVolStore[IdxStore, VolOffIdxStore[IdxStore]]), flowbackDurationS, FlowbackDurationLimS);
                return (false, newV0);
            }

            // calculate new initial volume in flow line
            // before ramping down rig pumps.
            newV0 = (VolStore.GetRow(IdxStore)[(volOffIdx - 1)..]).Max() - VolStore[IdxStore, volOffIdx];
            if (newV0 <= MinVol)
            {
                //if (DispDebug)
                _logger.LogDebug("QualFlowBack: Time: {0}. Discarding flowback due to small flowback volume: {1:F}. Minimum {2} m3.", DateHelpers.DateToString(TimeVolStore[IdxStore, VolOffIdxStore[IdxStore]]), newV0, MinVol);

                return (false, newV0);
            }

            var isVolumeWithinTolerance = CompareFlowbackVolumeWithPrev(newV0, qDrillLpm);
            // if the code reaches this line, then the flowback is
            // considered valid.
            if (isVolumeWithinTolerance) return (true, newV0);
            //if (DispDebug)
            _logger.LogDebug("QualFlowBack: Time: {0}. Discarding flowback since it is outside 3*stddev compared to the previous flowbacks.", DateHelpers.DateToString(TimeVolStore[IdxStore, VolOffIdxStore[IdxStore]]));

            return (false, newV0);
        }

        public bool CompareFlowbackVolumeWithPrev(double newV0, double? qDrillLpm)
        {
            if (qDrillLpm == null)
                return false;
            var (meanV0, stdV0) = GetFlowbackStatistics(qDrillLpm.Value);
            if (meanV0 == null || stdV0 == null)
            {
                // if there is nothing to compare with, we have to use this
                return true;
            }
            return newV0 < meanV0 + 3 * stdV0 && newV0 > Min(MinVol, meanV0.Value - 3 * stdV0.Value);
        }

        /// <summary>
        /// retrive mean flowback volume and standard error, meanV0
        /// [m3] and stdV0[m3], respectively from the nTd flowbacks
        /// where the flow rate is approximately qDrill_lpm
        /// </summary>
        public (double? meanV0, double? stdV0) GetFlowbackStatistics(double qDrillLpm)
        {
            var nTdIdx = GetNTdLogicalIndexExcludingCurrent();

            // find indicies where the flow rate before starting the
            // flowback were qDrill_lpm +/ -qWin_lpm
            var qLowIdx = QStore.Select(x => x > qDrillLpm - QWinLpm).ToArray();
            var qHighIdx = QStore.Select(x => x < qDrillLpm + QWinLpm).ToArray();
            var qIdx = nTdIdx.LogicAnd(qLowIdx).LogicAnd(qHighIdx);

            if (qIdx.Sum() < 2)
            {
                // need at least two valid flowbacks to compute meaningful
                // mean and standard deviation
                return (null, null);
            }
            var flowBacksM3 = V0Store.Subset(qIdx);
            double? meanV0 = flowBacksM3.Average();
            double? stdV0 = flowBacksM3.StandardDeviation();
            return (meanV0, stdV0);
        }

        /// <summary>
        /// make an array of logical indicies which can be used to look
        /// up the nTd last recorded values, including the current one.
        /// </summary>
        public bool[] GetNTdLogicalIndex()
        {
            bool[] idx;   // logical indicies

            if (IdxStore >= Parameters.NTd)
            {
                var startLength = IdxStore - Parameters.NTd;
                var startValues = Vector.Create(startLength, false);
                var trueValueLength = IdxStore - startLength;
                var trueValues = Vector.Create(trueValueLength, true);
                var endValues = Vector.Create(NFbs - trueValueLength - startLength, false);
                idx = startValues.Concat(trueValues).Concat(endValues).ToArray();
            }
            else
            {
                var startLength = IdxStore + 1;
                var startValues = Vector.Create(startLength, true);
                var middleValueLength = NFbs - Parameters.NTd;
                var middleValues = Vector.Create(middleValueLength, false);
                var endValues = Vector.Create(NFbs - middleValueLength - startLength, true);
                idx = startValues.Concat(middleValues).Concat(endValues).ToArray();
            }
            return idx;
        }

        /// <summary>
        /// make an array of logical indicies which can be used to look
        /// up the nTd last recorded values, excluding the current one.
        /// </summary>
        public bool[] GetNTdLogicalIndexExcludingCurrent()
        {
            bool[] idx; // logical indicies

            if (IdxStore >= Parameters.NTd)
            {
                var startLength = IdxStore - Parameters.NTd;
                var startValues = Vector.Create(startLength, false);
                var trueValueLength = IdxStore - startLength;
                var trueValues = Vector.Create(trueValueLength, true);
                var endValues = Vector.Create(NFbs - trueValueLength - startLength, false);
                idx = startValues.Concat(trueValues).Concat(endValues).ToArray();
            }
            else if (IdxStore == 0)
            {
                var startLength = NFbs - Parameters.NTd;
                var startValues = Vector.Create(startLength, false);
                var trueValues = Vector.Create(NFbs - startLength, true);
                idx = startValues.Concat(trueValues).ToArray();
            }
            else
            {
                var startLength = IdxStore;
                var startValues = Vector.Create(startLength, true);
                var middleValueLength = NFbs - Parameters.NTd;
                var middleValues = Vector.Create(middleValueLength, false);
                var endValues = Vector.Create(NFbs - middleValueLength - startLength, true);
                idx = startValues.Concat(middleValues).Concat(endValues).ToArray();
            }
            return idx;
        }

        /// <summary>
        /// traverse backwards through flowbacks and find the first valid
        /// one.Return last valid k - value and index to last volume,
        /// idxVol.
        /// </summary>
        public void GetLastValidVolIdxAndKIdx(out int? idxVol, out int kIdx)
        {
            idxVol = null;
            kIdx = 0;

            int[] r;
            if (IdxStore == NFbs)
                r = Vector.Range(NFbs -1, -1, -1);
            else
                r = Vector.Range(IdxStore, -1, -1).Concat(Vector.Range(NFbs - 1, IdxStore, -1)).ToArray();

            var ii = 0;
            while (Array.TrueForAll(VolStore.GetRow(r[ii]), x => x.Equals(0)))
            {
                ii += 1;
                if (ii >= NFbs)
                {
                    // No valid flowback to use for computation
                    return;
                }
            }

            idxVol = r[ii];

            var jj = 0;
            double? kVal = null;
            while (KStore[r[jj]].Equals(0) && !kVal.HasValue)
            {
                jj += 1;
                if (jj >= NFbs)
                {
                    // The k is not set
                    kVal = 0;
                    jj -= 1; // to avoid index out of bounds on next iteration
                }
            }
            if (!kVal.HasValue)
                kIdx = r[jj];
        }

        /// <summary>
        /// retrieve drainback data for idxVol
        /// </summary>
        public (double[] db, double[] volTime) GetDrainbackData(int idxVol)
        {
            var db = new double[VolStore.Columns() - (VolOffIdxStore[idxVol] - 1)];
            var volTime = new double[TimeVolStore.Columns() - (VolOffIdxStore[idxVol] - 1)];

            if (!(idxVol > NFbs || idxVol < 0) && VolOffIdxStore[idxVol] > 0) // avoid index out of bounds
            {
                var volStoreTmp = VolStore.GetRow(idxVol);
                db = volStoreTmp[VolOffIdxStore[idxVol]..].Subtract(volStoreTmp[VolOffIdxStore[idxVol]]);

                var timeVolStoreTmp = TimeVolStore.GetRow(idxVol);
                volTime = timeVolStoreTmp[VolOffIdxStore[idxVol]..]
                    .Subtract(timeVolStoreTmp.Last()).Select(x => x.OADateToSeconds()).ToArray();
            }
            return (db, volTime);
        }

        /// <summary>
        /// store data which is necessary for performing the least
        /// squares calculation for finding the parameters related to the
        /// flowline flow-rate.
        /// </summary>
        public void StoreLeastSquaresData(double[] db, double[] volTime, int idxVol)
        {
            var idxTd = Array.FindIndex(db, x => x > DbLimM3);
            // check if maybe it is better to use the previous index as
            // starting point, may be to conservative
            if (idxTd > 0)
            {
                var v1 = db[idxTd - 1];
                var v2 = db[idxTd];
                if (DbLimM3 / (v2 - v1) < 0.5)
                    idxTd -= 1;
            }

            // calculate y-term used for least squares
            var t = volTime.Skip(idxTd).ToArray().Subtract(volTime[idxTd]);
            var normDb = db.Skip(idxTd).Select(x => x / db.Max()).ToArray();
            var normDbIdx = normDb.Zip(t, (vn, vt) => vn > 0 && vn < 1 && vt < 300).ToArray();

            var y = normDb.Subset(normDbIdx).Select(x => Min(20, -Log(1 - x))).ToArray();
            var nY = y.Count();
            // save y and t for later use
            var tmpYStore = _yStore.GetRow(idxVol);
            Array.Copy(y, tmpYStore, nY);
            _yStore.SetRow(idxVol, tmpYStore);
            var tmpTStore = _store.GetRow(idxVol);
            Array.Copy(t.Subset(normDbIdx), tmpTStore, nY);
            _store.SetRow(idxVol, tmpTStore);
        }

        /// <summary>
        /// retrieve expected drainback data for idxVol
        /// </summary>
        public (double[] expDb, double[] expVolTime) GetExpDrainbackData(int idxVol)
        {
            var expDb = new double[]{};
            var expVolTime = new double[]{};

            if (!(idxVol > NFbs || idxVol < 0) && _expVolOffIdxStore[idxVol] > 0) // avoid index out of bounds
            {
                var expVolStoreTmp = ExpVolStore.GetRow(idxVol);
                expDb = expVolStoreTmp[_expVolOffIdxStore[idxVol]..].Subtract(expVolStoreTmp[_expVolOffIdxStore[idxVol]]);
                var timeExpVolStoreTmp = TimeExpVolStore.GetRow(idxVol);
                expVolTime = timeExpVolStoreTmp[_expVolOffIdxStore[idxVol]..]
                    .Subtract(timeExpVolStoreTmp[^1]).Select(x => x.OADateToSeconds()).ToArray();
            }
            return (expDb, expVolTime);
        }

        public void StoreAndUpdateFlowlineParameters(out FlowlineAndPit fapOut, FlowlineAndPit fapin, double newV0, double qDrillLpm)
        {
            QStore[IdxStore] = qDrillLpm;
            V0Store[IdxStore] = newV0;
            fapOut = fapin;
            CalcParams(out var kVol, out var kFlow, out var meanV0, qDrillLpm);

            // update the flowlineAndPitObj
            if (meanV0 != null)
                fapOut.UpdateVolDrainback(meanV0.Value);

            if (kVol != null && kFlow != null)
                fapOut.UpdateKFlowline(kVol.Value);
            if (kFlow != null) fapOut.UpdateK2Flowline(kFlow.Value);
            // save constant
            KStore[IdxStore] = fapOut.KFlowline;
        }

        public void CalcParams(out double? kVolHz, out double? kFlow, out double? meanV0M3, double qDrillLpm)
        {
            kVolHz = null;
            kFlow = null; // unitless
            meanV0M3 = null;
            var idx = GetNTdLogicalIndex();

            //Testing arrays with logic operations
            idx = idx.LogicAnd(KStore.Select(x => x > 0));
            idx[IdxStore] = true; // use current dataset as a minimum.

            // stack data
            //var idxBoolMask = Vec.IntArrayToBoolArray(idx);
            var trueColMask = Vector.Create(_yStore.Columns(), true);
            var yArray = _yStore.Get(idx, trueColMask).Reshape(MatrixOrder.FortranColumnMajor);
            trueColMask = Vector.Create(_store.Columns(), true);
            var yIdxGtZero = yArray.Select(x => x > 0);
            var tArray = _store.Get(idx, trueColMask).Reshape(MatrixOrder.FortranColumnMajor);
            var tLtLim = tArray.Select(x => x < LeastSquareTimeLimitS); // consider only time less than specified by limit
            if (yIdxGtZero.Any(x => x) && tLtLim.Any(x => x))
            {
                // carry out least square fit to find the inverse of the
                // time - constant for the flowback
                // kVol_hz = yArray(yIdxGtZero & tLtLim) / tArray(yIdxGtZero & tLtLim);
                kVolHz = 1 / 60.0;
                // scatter(tArray(yIdxGtZero), yArray(yIdxGtZero))
                // carry out least squares fit to find the average flowback
                // volume(V0)
                var idxValidFlowbacks = V0Store.Select(x => x > 0).ToArray();
                var nValidFlowbacks = idxValidFlowbacks.Sum();
                var diffValidFlowbacks = V0Store.Subset(idxValidFlowbacks).Diff();
                if (diffValidFlowbacks.Select(x => !x.Equals(0)).Any(x => x))
                {
                    var v0StoreMatrix = V0Store.Subset(idxValidFlowbacks).ToMatrix(asColumnVector: true);

                    var qStoreRow1 = QStore.Subset(idxValidFlowbacks);
                    var qStoreRow2 = Vector.Create(nValidFlowbacks, 1.0);
                    double[][] qStoreJagged = {qStoreRow1, qStoreRow2};
                    
                    var qStoreMatrix = qStoreJagged.ToMatrix(true);

                    var meanV0Coeffs = v0StoreMatrix.Transpose().Divide(qStoreMatrix.Transpose(), true);

                    var kDbM3PerLpm = meanV0Coeffs[0,0];
                    var kVm3 = meanV0Coeffs[0,1];
                    meanV0M3 = kDbM3PerLpm * qDrillLpm + kVm3;
                }
                else
                    meanV0M3 = V0Store[IdxStore];

                kFlow = 1 - kVolHz * meanV0M3 / (qDrillLpm / 6e4);
            }
            else
            {
                // otherwise, return with empty kVol, kFlow, and V0.
                return;
            }
        }


        /// <summary>
        /// This function updates the flowline and pit object with new
        /// values for kFlowLine and volDrainback, if there has been a
        /// connection with a sufficently large flowback
        /// </summary>
        public FlowlineAndPit UpdateFap(FlowlineAndPit fapin)
        {
            var fapOut = fapin;
            if ((Cs!.CurrentState == CirculationStates.Circulating && StateAtPrevTime == CirculationStates.NotCirculating) ||
                _updateFapTimeTrigger)
            {
                if (!IsInit)
                {
                    ClearStoredDataAtCurrentIndex();
                    StoreFlowbackData();

                    // find when the pumps start to ramp down and the
                    // steady state flow-rate before ramping down.
                    var (timeOfPumpRampD, qDrillLpm, _) = AnalyzeRamp(TimeFlowInStore.GetRow(IdxStore), FlowInStore.GetRow(IdxStore), "down");
                    StoreShutDownIndicies(timeOfPumpRampD);
                    var (isValidFlowback, newV0) = QualifyFlowback(qDrillLpm);
                    if (isValidFlowback && timeOfPumpRampD != null && qDrillLpm != null)
                    {
                        GetLastValidVolIdxAndKIdx(out var idxVol, out _);

                        if (idxVol.HasValue)
                        {
                            var (db, volTime) = GetDrainbackData(idxVol.Value);
                            StoreLeastSquaresData(db, volTime, idxVol.Value);

                            var (expDb, _) = GetExpDrainbackData(idxVol.Value);
                            if (expDb.Any())
                                _expV0Store[idxVol.Value] = expDb.Max();
                            // calculate new coefficient using least squares
                            StoreAndUpdateFlowlineParameters(out fapOut, fapin, newV0, qDrillLpm.Value);
                            //if (DispDebug)
                            _logger.LogDebug("UpdateFAP:Time: {0}. New time-constant: {1:F} s, qDrill: {2:F} lpm, mean V0: {3:F} m3.", DateHelpers.DateToString(TimeVolStore[IdxStore, VolOffIdxStore[IdxStore]]), 1 / fapOut.KFlowline, QStore[IdxStore], fapOut.VolDrainback);
                        }
                    }

                    // iterate the counter for storing snapshots from
                    // ringbuffers and constants
                    IdxStore += 1;
                    if (IdxStore >= NFbs)
                        IdxStore = 0;
                }
            }
            return fapOut;
        }

        public void UpdateConnectionData()
        {
            if (_storeConnectionDataTrigger && !IsInit)
            {
                QualifyConnection(out var isAValidConnection, out var timeBase_d, out var flowInCurrentConClipped, out var volCurrentConClipped,
                    out var expVolCurrentConClipped);
                if (!isAValidConnection)
                    return;

                ClearStoredConnectionDataAtCurrentIndex();
                StoreConnectionData(timeBase_d, flowInCurrentConClipped, volCurrentConClipped, expVolCurrentConClipped);
                FitConnectionData();
                IdxCon += 1;
                if (IdxCon >= NConToRecord)
                    IdxCon = 0;
            }
        }

        public void FitConnectionData()
        {
            var nC = _isValidConnection.Sum(b => b.ToInt());
            if (nC == 0)
                return;

            var errorMat = Matrix.Create(nC, NRbFlow, NaN);
            var flowInMat = Matrix.Create(nC, NRbFlow, NaN);
            var deltaTimeMat = new double[nC, NRbFlow];
            var rowCounter = 0;
            var conRange = Enumerable.Range(0, NConToRecord).ToArray();
            conRange = conRange.Subset(_isValidConnection);
            foreach (var ii in conRange)
            {
                errorMat.SetRow(rowCounter, _volCon.GetRow(ii).Subtract(_expVolCon.GetRow(ii)));
                flowInMat.SetRow(rowCounter, _flowInCon.GetRow(ii));
                var T = 
                    _timeVolCon.GetRow(ii)[.._nValidDataPoints[ii]]
                    .Diff()
                    .Select(z => z.OADateToSeconds()).ToArray();
                deltaTimeMat.SetRow(rowCounter, T);
                deltaTimeMat[rowCounter, _nValidDataPoints[ii] - 1] = TimeDiffBetweenStackedTimeSeriesS;
                rowCounter += 1;
            }

            var flowArray = flowInMat.Transpose().Reshape(MatrixOrder.FortranColumnMajor);
            var deltaTimeArray = deltaTimeMat.Transpose().Reshape(MatrixOrder.FortranColumnMajor);
            var errorArray = errorMat.Transpose().Reshape(MatrixOrder.FortranColumnMajor);
            var timeArray = new List<double>(deltaTimeArray[..^1].CumulativeSum());
            timeArray.Insert(0, 0.0);
            var noNanMask = new bool[errorArray.Length];
            for (var i = 0; i < errorArray.Length; i++)
            {
                noNanMask[i] = !IsNaN(errorArray[i]) && !IsNaN(flowArray[i]) && !IsNaN(timeArray[i]);
            }

            var flowArrayWoNan = flowArray.Subset(noNanMask);
            var timeArrayWoNan = (timeArray.ToArray().Subset(noNanMask));
            var errorArrayWoNan = errorArray.Subset(noNanMask);
            var timeBaseResampled = Vector.Range(timeArrayWoNan[0], timeArrayWoNan[^1], TimeSamplingS);
            var flowArrayInterp = Interp1(timeArrayWoNan, flowArrayWoNan, timeBaseResampled);
            var errorArrayInterp = Interp1(timeArrayWoNan, errorArrayWoNan, timeBaseResampled);
            var noNanMaskNew = errorArrayInterp.Select(xx => !IsNaN(xx)).ToArray();
            var flowArrayWoNanInterp = flowArrayInterp.Subset(noNanMaskNew);
            var errorArrayWoNanInterp = errorArrayInterp.Subset(noNanMaskNew);
            var x = new double[flowArrayWoNanInterp.Count() - MaxDelaySamples, NDelays + 1];
            x.SetColumn(x.Columns() - 1, flowArrayWoNanInterp[MaxDelaySamples..]);

            for (var jj = 0; jj < NDelays; jj++)
            {
                x.SetColumn(jj, flowArrayWoNanInterp[(MaxDelaySamples - DelayArraySamples[jj])..^(DelayArraySamples[jj])]);
            }
            var y = errorArrayWoNanInterp[MaxDelaySamples..];
            //TODO: This is done with a tmp mdl since mdl is checked for null in QualifyConnection. Check if this can be improved
            if (Mdl == null)
                Mdl = _linRegTmp;
            Mdl.FitData(x, y);
        }

        public void QualifyConnection(out bool isValidConnection, out double[] timeBaseD,
            out double[] flowInCurrentConClipped, out double[] volCurrentConClipped, out double[] expVolCurrentConClipped)
        {
            isValidConnection = false;
            timeBaseD = new double[] { };
            flowInCurrentConClipped = new double[] { };
            volCurrentConClipped = new double[] { };
            expVolCurrentConClipped = new double[] { };
            var deringbufferAndResample = RbFlowIn!.DeringbufferAndResample(true);
            var timeFlowInCurrentConD = deringbufferAndResample.TbufferDStraightResampled;
            var flowInCurrentCon = deringbufferAndResample.VbufferStraightResampled;

            if (timeFlowInCurrentConD.Diff().Select(x => x.OADateToSeconds()).Max() > DeltaTimeForMissingDataS)
            {
                //if (DispDebug)
                _logger.LogDebug("QualConn: Time: {0}. Discarding connection due to missing data in buffer", DateHelpers.DateToString(timeFlowInCurrentConD[timeFlowInCurrentConD.Length - 1]));
                return;
            }

            var connectionEndIdx = Array.FindLastIndex(flowInCurrentCon, x => x <= StartSearchLpm);
            if (connectionEndIdx == -1)
            {
                //if (DispDebug)
                _logger.LogDebug("QualConn: Time: {0}. Discarding connection because the end of the ramp down was not found", DateHelpers.DateToString(timeFlowInCurrentConD[timeFlowInCurrentConD.Length - 1]));
                return;
            }

            var connectionStartIdx = Array.FindLastIndex(flowInCurrentCon[..Max(0, connectionEndIdx - 1)], x => x > StartSearchLpm);
            if (connectionStartIdx == -1)
            {
                //if (DispDebug)
                _logger.LogDebug("QualConn: Time: {0}. Discarding connection because the start of the ramp down was not found", DateHelpers.DateToString(timeFlowInCurrentConD[connectionEndIdx]));
                return;
            }

            var connectionDurationS = timeFlowInCurrentConD[connectionEndIdx].DiffInSeconds(timeFlowInCurrentConD[connectionStartIdx]);
            if (connectionDurationS <= ConnectionDurationLimS)
            {
                //if (DispDebug)
                _logger.LogDebug("QualConn: Time: {0}. Discarding connection due to short duration.", DateHelpers.DateToString(timeFlowInCurrentConD[connectionStartIdx]));
                return;
            }

            var (timeStartOfPumpRampDown, _, _) = AnalyzeRamp(
                timeFlowInCurrentConD[..(connectionStartIdx + 1)], 
                flowInCurrentCon[..(connectionStartIdx + 1)],
                "down");
            if (!timeStartOfPumpRampDown.HasValue)
            {
                //if (DispDebug)
                _logger.LogDebug("QualConn: Time: {0}. Discarding connection since a period with stable flow prior to the connection was not found", DateHelpers.DateToString(timeFlowInCurrentConD[connectionStartIdx]));
                return;
            }
            // the mean absolute error between model and measured vol gain loss
            // must be less than a limit
            var deringBufferAndResampleResultRbVol = RbVol!.DeringbufferAndResample(true);
            var timeVolCurrentConD = deringBufferAndResampleResultRbVol.TbufferDStraightResampled;
            var volCurrentCon = deringBufferAndResampleResultRbVol.VbufferStraightResampled;
            var deringbufferAndResampleRbExpVol = RbExpVol!.DeringbufferAndResample(true);
            var timeExpVolCurrentConD = deringbufferAndResampleRbExpVol.TbufferDStraightResampled;
            var expVolCurrentCon = deringbufferAndResampleRbExpVol.VbufferStraightResampled;
            var volCurrentConWoOffset = volCurrentCon;
            var expVolCurrentConWoOffset = expVolCurrentCon;
            var volCurrentConInterp = Interp1Previous(timeVolCurrentConD, volCurrentConWoOffset, timeExpVolCurrentConD); 
            var noNanMask = new bool[volCurrentConInterp.Length];
            for (var i = 0; i < volCurrentConInterp.Length; i++)
            {
                noNanMask[i] = !IsNaN(volCurrentConInterp[i]) && !IsNaN(expVolCurrentConWoOffset[i]) && !IsNaN(timeExpVolCurrentConD[i]);
            }
            var volCurrentConInterpWoNan = volCurrentConInterp.Subset(noNanMask);
            var expVolCurrentConWoNan = expVolCurrentConWoOffset.Subset(noNanMask);
            var timeExpVolCurrentConWoNanD = timeExpVolCurrentConD.Subset(noNanMask);

            var idxConStart = Array.FindIndex(timeExpVolCurrentConWoNanD, x => x >= timeStartOfPumpRampDown.Value.SubtractSeconds(TimeToIncludeBeforePumpShutDownS));
            volCurrentConClipped = volCurrentConInterpWoNan[idxConStart..];
            expVolCurrentConClipped = (expVolCurrentConWoNan[idxConStart..].Add(volCurrentConInterpWoNan[idxConStart])).Subtract(expVolCurrentConWoNan[idxConStart]);
            if ((volCurrentConClipped.Subtract(expVolCurrentConClipped)).Select(x => Abs(x)).Average() > VolGainLossErrorLimM3 || Mdl == null)
                expVolCurrentConClipped = ScaleExpVolCurve(volCurrentConClipped, expVolCurrentConClipped);

            // compute the maximum rate of change in gain/loss
            // volume[m3 / s].
            timeBaseD = timeExpVolCurrentConWoNanD[idxConStart..];
            var maxVolDiff = volCurrentConClipped
                .Diff()
                .Divide(timeBaseD.Diff())
                .Select(x => Abs(x.SecondsToOADate()))
                .Max();

            if (maxVolDiff >= VolDiffLimM3PerS * MultiplierForVolDiffLimDuringConnection)
            {
                //if (DispDebug)
                _logger.LogDebug("QualConn: Time: {0}. Discarding connection since the change in flow rate {1:F} lpm into the pits is larger than {2:F} lpm.", DateHelpers.DateToString(timeFlowInCurrentConD[connectionStartIdx]), maxVolDiff * 60e3, MultiplierForVolDiffLimDuringConnection * VolDiffLimM3PerS * 60e3);
                return;
            }
            var flowInCurrentConWoNan = flowInCurrentCon.Subset(noNanMask);
            flowInCurrentConClipped = flowInCurrentConWoNan[idxConStart..];
            isValidConnection = true;
        }

        public double[] PredictFromModel()
        {
            var (_, val) = PickDelayedSamples();
            var b = val.Select(x => Max(x, 0)).Select(x => Min(x, FlowInMaxLimLpm)).ToArray();
            var c = Matrix.Create(1, b.Length, b);
            return Mdl!.PredictFromData(c);
        }


        public (List<double> time_d, double[] val) PickDelayedSamples()
        {
            List<double> timeD;
            double[] val;
            var deringbuffer = RbFlowIn!.Deringbuffer();
            var t = deringbuffer.TbufferDStraight;
            var v = deringbuffer.VbufferStraight;
            var dtAvgD = deringbuffer.DtAvgD;
            var validDataPoints = deringbuffer.ValidDataPoints;

            if (validDataPoints.Any(x => !x))
            {
                var tValidD = t.Subset(validDataPoints);
                var vValid = v.Subset(validDataPoints);
                var nInvalidDataPoints = validDataPoints.Count(x => !x);
                var (timePadded, valuePadded) = RbFlowIn.PadMissingValues(tValidD, vValid, dtAvgD, nInvalidDataPoints, false);
                var tPad = timePadded.Concat(tValidD).ToArray();
                var vPad = valuePadded.Concat(vValid).ToArray();
                var tPadEnd = tPad[^1];
                timeD = DelayArraySamples
                    .Select(x => tPadEnd.SubtractSeconds(x * TimeSamplingS))
                    .ToList();
                timeD.Add(tPadEnd);
                val = Interp1(tPad, vPad, timeD.ToArray());
            }
            else
            {
                var tEnd = t[^1];
                timeD = DelayArraySamples
                    .Select(x => tEnd.SubtractSeconds(x * TimeSamplingS))
                    .ToList();
                timeD.Add(tEnd);
                val = Interp1(t, v, timeD.ToArray());
            }
            return (timeD, val);
        }

        /// <summary>
        /// use least squares to get the a factor and bias which scale
        /// the expected vol curve to fit the measured.
        /// </summary>
        public static double[] ScaleExpVolCurve(double[] volCurrentConClipped, double[] expVolCurrentConClipped)
        {
            var vc = Matrix.Create(volCurrentConClipped.Length, 1, volCurrentConClipped);
            var vb = Matrix.Create(expVolCurrentConClipped.Length, 2, NaN);
            vb.SetColumn(0, expVolCurrentConClipped);
            vb.SetColumn(1, Vector.Create(expVolCurrentConClipped.Length, 1.0));
            var coeffs = vb.Solve(vc, true);
            var expVolCurrentConClippedAndScaled = expVolCurrentConClipped.Multiply(coeffs[0,0]).Add(coeffs[1,0]);
            return expVolCurrentConClippedAndScaled;
        }

        public static void RemoveOffsetFromTimeseries(out double[] signalWoOffset, double[] signal, double threshold)
        {
            var offset = -signal[1];
            signalWoOffset = new double[signal.Length];
            for (var ii = 1; ii < signal.Length; ii++)
            {
                (signalWoOffset[ii], offset) = RemoveOffset(
                    signal[(ii - 1).. (ii+1)],
                    offset,
                    threshold,
                    0.0);
            }
        }
    }
}
