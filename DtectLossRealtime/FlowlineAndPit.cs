﻿using System.Collections.Generic;
using System.Linq;
using Accord.Math;
using DtectLoss.Common;
using DtectLossRealtime.Calculations;
using DtectLossRealtime.Helpers;
using DtectLossRealtime.Models;
using static System.Math;
using static DtectLoss.Common.CommonConstants;

namespace DtectLossRealtime
{
    /// <summary>
    /// FLOWLINE_AND_PIT_OBJ Model of expeced flow line and pit dynamics.
    /// Detailed explanation goes here
    /// </summary>
    public class FlowlineAndPit
    {
        public double[] X { get; private set; } = { };
        public double VolDrainback { get; private set; }
        public double KOutputInjectionFl { get; private set; }
        // term to compensate for difference in actual
        // and estimate gain/loss volume.Used for flow
        // line dynamics.
        public double KOutputInjectionGl { get; private set; }
        // term to compensate for difference in actual
        // and estimate gain/loss volume.Used for gain
        // loss dynamics
        public double KIntOutputInjectionGl { get; private set; }
        // term to compensate for difference in actual
        // and estimate gain/loss volume.Used for gain
        // loss dynamics
        // gain/loss volume
        public double Dt { get; private set; }
        public double? PrevTimeStamp { get; private set; }
        public double Td { get; private set; }
        public double KFlowline { get; private set; }
        public double K2Flowline { get; private set; }
        public double TimeSinceResetGlS { get; private set; }

        //FilterDerivative filterDer; // FILTERDERIVATIVE_OBJ for the flow-rate from the rig pumps.
        private FilterDerivative? _pitDer; // FILTERDERIVATIVE_OBJ for the pit volume
        private double _totalInjectionRateLpm; // [lpm] total injection flow-rate to track the
        private double? _flowPitDerLpm; // estimated losses in well and shakers based on derivative of measured pit volume
        private RingBuffer? _rbFlowIn; // ringbuffer for flow in
        // the flow rate from the flowline to the pit is modelled as
        // qFL = kFlowline* VFlowline + k2Flowline* qp
        private double _volGainLossPrevGoodM3; // [m3] previous measurement of volGainLoss which was good.
        private bool _quickConvergeNowBool; // [bool] true if quick converge is active
        private double _timeCounterQuickConvergeS; // [s] time counter for quick converge
        private double _qOutEstM3PerSPos;
        private double _volGainLossM3;
        private const double ErrVal = MissingValueReplacement;
        private const double M3SToLpm = 60000;
        private const double VolStepM3 = 3;
        private const double UpperInjLimit = 300; // [lpm] upper limit of injection term
        private const double LowerInjLimit = -300; // [lpm] lowerlimit of injection term
        private const double PitDerivativeTimeConstant = 900; // [s] time constant for the first 
        // order Filter for finding derivative
        // of the pit volume
        private const int FlowInBufferSizeSamples = 540; // [samples] number of samples in ringbuffer for flow-in.
        private const double MaxDt = 60; // [s] maximum allowed time-step.Reset if above.
        private const double Damping = 1.0; // [-] damping for the gain/loss observer.Used to calculate tuning.
        private const double FlowInLowLimForGlResetLpm = 400; // [lpm] limit which is used to determine if a reset should be performed.
        private const double TimeLimQuickConvergeS = 90; // [s] time-limit for quick convergence.On for this period of time.
        private const double KVolResetHz = 1 / 60.0;
        private const double KFlowReset = -0.5;
        private const double VolDrainbackResetM3 = 6;

        public void SoftReset()
        {
            SetQuickConvergeNow(true);
            KFlowline = KVolResetHz;
            K2Flowline = KFlowReset;
            VolDrainback = VolDrainbackResetM3;
            TimeSinceResetGlS = 0;
        }

        public void Initialize(double[] x0, double v0Fl, double kOiFl, double kOiGl, double? timeStep = null)
        {
            X = x0;
            VolDrainback = v0Fl;
            KOutputInjectionFl = kOiFl;
            KOutputInjectionGl = kOiGl;
            // tune observer for gain loss as a mass - spring - damper system
            var w0 = kOiGl / (2.0 * Damping);
            KIntOutputInjectionGl = Pow(w0, 2);
            Td = 0; // set time - delay to zero initially
            if (timeStep != null)
                Dt = timeStep.Value;
            _totalInjectionRateLpm = 0;
            _flowPitDerLpm = 0;
            _pitDer = new FilterDerivative(PitDerivativeTimeConstant);
            _rbFlowIn = new RingBuffer(FlowInBufferSizeSamples);
            KFlowline = KVolResetHz;
            K2Flowline = KFlowReset;
            _volGainLossPrevGoodM3 = X[1];
            _quickConvergeNowBool = true;
            _timeCounterQuickConvergeS = 0;
            TimeSinceResetGlS = 0;
        }

        /// <summary>
        /// update time-step(dT) and time stamp based on time from variables.
        /// </summary>
        /// <param name="qOutExp"></param>
        /// <param name="qPump"></param>
        /// <param name="volGl"></param>
        public void UpdateTimeStep(DataVariable qOutExp, DataVariable qPump, DataVariable volGl)
        {
            var currentTime = new List<double> { qOutExp.T, qPump.T, volGl.T }; 
            if (!PrevTimeStamp.HasValue)
            {
                PrevTimeStamp = currentTime.Max();
                Dt = 0;
                return;
            }
            var newTimeStamp = currentTime.Concat(new List<double>() { PrevTimeStamp.Value }).Max();
            Dt = Max(0, Round(newTimeStamp.DiffInSeconds(PrevTimeStamp.Value)));
            PrevTimeStamp = newTimeStamp;
        }

        public void SetTimeStep(double timeStep)
        {
            // timeStep should be in seconds
            Dt = Max(0, Min(MaxDt, timeStep));
        }

        /// <summary>
        /// This function updates the estimates of the volume in the flow
        /// line and pit/ (gain / loss).Input: qOut[lpm] expected flow
        /// rate out of well and into flow line.qPump[lpm] flow rate
        /// from rig pumps.volGainLoss[m3] gain / loss volume without
        /// sudded jumps due to reset etc.useOutputInjection[boolean]
        /// should be true when drilling in steady state, otherwise false.
        /// </summary>
        public void UpdateState(double qOutLpm, double qPumpLpm, double volGainLossM3, bool useOutputInjection, double qPumpT)
        {
            CheckIfQuickConvergeShouldBeOn();
            if (!_quickConvergeNowBool)
            {
                // step time normally
                double[] xDot = Dyn(qOutLpm, qPumpLpm, volGainLossM3, useOutputInjection);
                X = X.Add(xDot.Multiply(Dt)); 
            }
            else
            {
                // reset states
                var valFlowIn = _rbFlowIn!.GetLastNValues(10);
                var validMeas = valFlowIn.Select(x => !x.Equals(ErrVal)).ToArray();
                    if (validMeas.Any())
                        X[0] = valFlowIn.Subset(validMeas).Average() * (1 - K2Flowline) / KFlowline / M3SToLpm;
                

                if (!volGainLossM3.Equals(ErrVal))
                    X[1] = volGainLossM3;
                else
                    X[1] = _volGainLossPrevGoodM3;

                X[2] = 0;
            }

            X[0] = Max(X[0], 0); // ensure that volume in flowline is positive.
            X[2] = Min(Max(X[2], LowerInjLimit / M3SToLpm), UpperInjLimit / M3SToLpm); // ensure that integrated injection is not too large

            if (volGainLossM3.Equals(ErrVal)) return;
            if (Abs(volGainLossM3 - _pitDer!.PrevInp / M3SToLpm) > VolStepM3)
                _pitDer.PrevInp = volGainLossM3 * M3SToLpm;
            else
                _flowPitDerLpm = _pitDer.Smoothderivative1storder(volGainLossM3 * M3SToLpm, qPumpT);
        }

        public void CheckIfQuickConvergeShouldBeOn()
        {
            _timeCounterQuickConvergeS = Max(0, Min(_timeCounterQuickConvergeS + Dt, TimeLimQuickConvergeS));

            if (_timeCounterQuickConvergeS - TimeLimQuickConvergeS == 0)
                _quickConvergeNowBool = false;
        }

        public void SetQuickConvergeNow(bool desiredQuickConvergeNowBool)
        {
            if (desiredQuickConvergeNowBool)
            {
                _timeCounterQuickConvergeS = 0;
                _quickConvergeNowBool = true;
            }
            else
                _quickConvergeNowBool = false;
        }

        public double[] Dyn(double qPumpDelayedLpm, double qPumpLpm, double volGainLossM3, bool useOutputInjection)
        {
            double[] xDot;
            // calculation of xDot
            // output injection terms.
            var x1Inj = KOutputInjectionFl * (volGainLossM3 - X[1]);
            var x2Inj = Max(LowerInjLimit / M3SToLpm, Min(KOutputInjectionGl * (volGainLossM3 - X[1]), UpperInjLimit / M3SToLpm));
            // double integrator for output injection term to track slope of
            // volGainLoss
            var x2InjIntDot = KIntOutputInjectionGl * Sign(volGainLossM3 - X[1]) * Max(Abs(volGainLossM3 - X[1]), Pow(volGainLossM3 - X[1], 2));


            // ensure that the flow - rates are positive
            var qPumpM3SPos = Max(0, qPumpLpm) / M3SToLpm;
            if (!qPumpDelayedLpm.Equals(MissingValueReplacement))
                _qOutEstM3PerSPos = Max(0, qPumpDelayedLpm) / M3SToLpm;
            else
                _qOutEstM3PerSPos = qPumpM3SPos;

            // function for flow - rate from flowline
            var qFlM3PerS = Max(0, X[0] * KFlowline) + K2Flowline * _qOutEstM3PerSPos;

            // calculate xDot
            if (useOutputInjection)
            {
                // total flow - rate injected into estimated volGainLoss to track
                // the measured one.
                var totalInjectionRateM3PerS = x2Inj + X[2];
                _totalInjectionRateLpm = totalInjectionRateM3PerS * M3SToLpm;
                xDot = new[] {_qOutEstM3PerSPos - qFlM3PerS - x1Inj, qFlM3PerS - _qOutEstM3PerSPos + totalInjectionRateM3PerS, x2InjIntDot }; 
            }
            else
            {
                _totalInjectionRateLpm = 0;
                xDot = new[] {_qOutEstM3PerSPos - qFlM3PerS, qFlM3PerS - _qOutEstM3PerSPos, 0}; 
            }

            return xDot;
        }

        public void StepIt(DataVariable qOut, DataVariable qPumpLpm, DataVariable volGl, bool useOutputInjection)
        {
            if (volGl.V.Equals(ErrVal))  
                _volGainLossM3 = _volGainLossPrevGoodM3;
            else
            {
                _volGainLossM3 = volGl.V;   
                _volGainLossPrevGoodM3 = volGl.V; 
            }

            UpdateTimeStep(qOut, qPumpLpm, volGl);
            _rbFlowIn!.AddDataToCircularBuffer(qPumpLpm.T, qPumpLpm.V); 
            var qDelayed = _rbFlowIn.LookUp(qPumpLpm.T.SubtractSeconds(Td));    
            if (Dt > MaxDt)
                ResetState(_volGainLossM3);
            else if (Dt > 0)
                UpdateState(qDelayed, qPumpLpm.V, _volGainLossM3, useOutputInjection, qPumpLpm.T); 
        }

        public void ResetGLIfAboveThresholds(double prevGoodVolGainLossM3, double volGainLossM3, double volGainLossThresholdM3PerTimeStep, double flowInLpm)
        {
            if (flowInLpm > FlowInLowLimForGlResetLpm)
            {
                if (Abs(prevGoodVolGainLossM3 - volGainLossM3) > volGainLossThresholdM3PerTimeStep / 2.0)
                    ResetGl(volGainLossM3);
                else
                    TimeSinceResetGlS += Dt;
            }
            else
            {
                if (Abs(prevGoodVolGainLossM3 - volGainLossM3) > volGainLossThresholdM3PerTimeStep)
                    ResetGl(volGainLossM3);
                else
                    TimeSinceResetGlS += Dt;
            }
        }

        public void ResetGlWithSlidingWindowOfFlowIn(double prevGoodVolGainLossM3, double volGainLossM3, double volGainLossThresholdM3PerTimeStep)
        {
            var flowInIsStableAndPumpsRunning = CheckIfFlowInInRingBufferIsStable();

            if (flowInIsStableAndPumpsRunning)
            {
                if (Abs(prevGoodVolGainLossM3 - volGainLossM3) > volGainLossThresholdM3PerTimeStep / 2.0)
                    ResetGl(volGainLossM3);
                else
                    TimeSinceResetGlS += Dt;
            }
            else
            {
                if (Abs(prevGoodVolGainLossM3 - volGainLossM3) > volGainLossThresholdM3PerTimeStep)
                    ResetGl(volGainLossM3);
                else
                    TimeSinceResetGlS += Dt;
            }
        }

        public bool CheckIfFlowInInRingBufferIsStable()
        {
            var deringbuffer = _rbFlowIn!.Deringbuffer();
            var tbufferDStraight = deringbuffer.TbufferDStraight;
            var vbufferStraight = deringbuffer.VbufferStraight;
            var validDataPoints = deringbuffer.ValidDataPoints;

            if (!validDataPoints.Any() || validDataPoints.Count(x => x) < FlowInBufferSizeSamples / 2)
                // no, or not enough, valid data points
                return false;

            if (vbufferStraight.Subset(validDataPoints).All(x => x < 50))
                    // pumps are considered not running
                    return false;

            if (Abs(Enumerable.Max(vbufferStraight.Subset(validDataPoints)) - 
                Enumerable.Min(vbufferStraight.Subset(validDataPoints))) > 500)
                // flow is not considered stable
                return false;

            return !tbufferDStraight.Subset(validDataPoints).Diff().Select(x => x.OADateToSeconds()).ToArray().Any(x => x > MaxDt);
        }

        public void ResetState(double volGlM3) 
        {
            // reset all states
            X = new[] {VolDrainback, volGlM3, 0};
        }

        public void ResetGl(double volGlM3)
        {
            // reset only the gain / loss estimate
            X[1] = volGlM3;
            TimeSinceResetGlS = 0;
        }

        public void ResetInt(double volGlM3)
        {
            // reset gain/loss estimate and integrator for output injection
            X[1] = volGlM3;
            X[2] = 0;
        }

        public void SetTimeDelay(double newTd)
        {
            Td = Min(600, Max(newTd, 0));
        }

        public void UpdateKFlowline(double newK)
        {
            if (newK < 1 && newK > 0.0001) // must be between 0.0001 and 1
                KFlowline = newK;
        }

        public void UpdateK2Flowline(double newK2)
        {
            if (newK2 < 10 && newK2 > -10) // must be between -2 and 2
                K2Flowline = newK2;
        }

        public void UpdateVolDrainback(double newVolDrainback)
        {
            if (newVolDrainback < 30 && newVolDrainback > 0) // must be between 0 and 30 m3
            {
                VolDrainback = newVolDrainback;
            }
        }

        public FlowlineAndPitOutput GenerateOutput(double volGainLossExpectedM3, bool isCalibratedExpPitVolBoolean)
        {
            var result = new FlowlineAndPitOutput()
            {
                VolFlExpectedM3 = X[0],    // estimated volume in flow line
                VolGlExpectedM3 = volGainLossExpectedM3,
                FlowPitDerLpm = _flowPitDerLpm ?? 0.0,
                IsCalibratedExpPitVol = isCalibratedExpPitVolBoolean,
            };
            return result;
        }
    }
}
