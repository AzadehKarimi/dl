﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using DtectLoss.Common;
//using DtectLoss.FileHandling;
using DtectLossRealtime.Calculations;
using DtectLossRealtime.Enums;
using DtectLossRealtime.Helpers;
using DtectLossRealtime.Models;
using Microsoft.Extensions.Logging;
using static System.Double;
using static System.Math;
using static DtectLoss.Common.CommonConstants;

namespace DtectLossRealtime
{
    public interface IProcess
    {
        void Initialize(double startTime);
        WashDataResult WashData (Dictionary<string, DataVariable> rawDataNonstandardUnits, StateMachine sysState, bool recalibratePaddleNow);
        void UpdateTagList(Dictionary<string, string> rawDataUnits);
        double DtS { get; }
        double TimeCurrentD { get; }
        double VolGainLossThresholdM3PerTimeStep { get; }
        PaddleConverter PaddleConv { get; }
        StandpipePressureDelay PresSPdelay { get; }
        void SoftReset();
    }



    /// <summary>
    /// is in charging of washing data: convert units, adjust measurements, filtering
    /// it processes a struct of rawData in and converts it according to the
    /// unit specifier into the standard units
    /// 
    /// this class is intended for running in real-time and processes one
    /// vector of data each time it is called
    /// 
    /// process obj also adjusts for pit volume offests and converts
    /// paddle measurements from percent to lpm.
    /// 
    /// </summary>
    public class Process : IProcess
    {
        public Dictionary<string, string> RawDataUnits { get; private set; } = new Dictionary<string, string>();
        public Dictionary<string, double> DefaultDataUnitsFactors { get; private set; } = new Dictionary<string, double>();
        public Dictionary<string, string> DefaultDataUnits { get; private set; } = new Dictionary<string, string>();
        public double VolGainLossThresholdM3PerTimeStep { get; } = 3;
        public Dictionary<string, bool> AvailableRequiredTags { get; private set; } = new Dictionary<string, bool>();
        public Dictionary<string, bool> AvailableOptionalTags { get; private set; } = new Dictionary<string, bool>();
        public IEnumerable<string> NameSpace { get; private set; } = new List<string>();
        public IEnumerable<(string k, string v)> UndefinedTags { get; private set; } = new List<(string k, string v)>();
        public double DtS { get; private set; }
        public double TimeCurrentD { get; private set; }
        public PaddleConverter PaddleConv { get; }
        public StandpipePressureDelay PresSPdelay { get; }

        public void SoftReset()
        {
            _kalmanFilter.SoftReset();
            PaddleConv.SoftReset();
            PresSPdelay.SoftReset();
        }

        private readonly IDefaultUnitsMappingReader _defaultUnitsMappingReader;
        private readonly ILogger<Process> _logger;
        private double _timeLastD;
        private Dictionary<string, bool> _availableRequiredUnits = new Dictionary<string, bool>();
        private Dictionary<string, bool> _availableOptionalUnits = new Dictionary<string, bool>();
        private Dictionary<string, double> _rawDataStdUnitsLastGoodValue;
        private double _volGainLossPrevM3 = 0;        //  previous measurement(from raw data, not washed)
        private double _volGainLossOffsetM3 = 0;

        private const double HeighthookDisconnectLimitForVelblockEstM = 9;
        private const double DepthbitResetLimitForVelbitEstM = 50;

        private const double MissingValCode = MissingValueReplacement;
        private bool _isenabledDelayedPressp = true;
        private bool _isDownlinking = false;
        private int _debugCounter = 0;

        // init of variables necessary for removal of sudden changes in gain/loss
        private const double TfiltHookHeightS = 20;                // Filter time constant for estimating block velocity from hook height
        private const double TfiltBitDepthS = 20;                 // Filter time constant for estimating bit velocity from bit depth
        // tag list
        private readonly string[] _requiredTags = new String[] { "flowIn", "depthBit", "depthHole" };
        private readonly string[] _requiredUnits = new string[] { "lpm", "m", "m" };  // corresponding default units

        private readonly string[] _optionalTags = new string[] { "flowOut", "flowOut1", "presSP", "presSP1", "volGainLoss", "volPitDrill", "volTripTank", "volTripTank2", "heightHook", "rpm", "torque", "weightOnBit", "rop"};
        private readonly string[] _optionalUnits = new String[] { "lpm", "lpm", "bar", "bar", "m3", "m3", "m3", "m3", "m", "", "kNm", "kN", "mph" };   // corresponding default units

        private string[] _calculatedTags = { "velBlock", "velBit", "velRop" };
        private string[] _calculatedUnits = { "mph", "mph", "mph" };    // corresponding default units
        private readonly HybridKalmanFilter _kalmanFilter;
        private readonly DownLinkDetect _downlinkDetect;
        private readonly FilterDerivative _filtHookHeight;
        private readonly FilterDerivative _filtBitDepth;
        private readonly LowPass _lowPassFlowIn;
        private Dictionary<string, string> _nameSpaceWithUnits = new Dictionary<string, string>();


        public Process(ILogger<Process> logger, IDefaultUnitsMappingReader defaultUnitsMappingReader, StandpipePressureDelay standpipePressureDelay, PaddleConverter paddleConverter)
        {
            _logger = logger;
            _defaultUnitsMappingReader = defaultUnitsMappingReader;
            PresSPdelay = standpipePressureDelay;
            _rawDataStdUnitsLastGoodValue = new Dictionary<string, double>();
            _kalmanFilter = new HybridKalmanFilter();
            _downlinkDetect = new DownLinkDetect();
            PaddleConv = paddleConverter;
            PaddleConv.Initalize(); //In Matlab, parameters are initialized, but never used
            _filtHookHeight = new FilterDerivative(TfiltHookHeightS);
            _filtBitDepth = new FilterDerivative(TfiltBitDepthS);
            _lowPassFlowIn = new LowPass(1, ParameterValues.VotingParam.FlowInLowpassTcS);
            _volGainLossPrevM3 = 0;        //  previous measurement(from raw data, not washed)
            _volGainLossOffsetM3 = 0;
            _filtHookHeight = new FilterDerivative(TfiltHookHeightS);
            _filtBitDepth = new FilterDerivative(TfiltBitDepthS);
        }


        public void Initialize(double startTime)
        {
            _timeLastD = startTime;
            TimeCurrentD = startTime;
            DtS = 0;
            _kalmanFilter.Initialize(Pow(ParameterValues.PitvolParams.FlowstdRKfLpm, 2), 0, Pow(ParameterValues.PitvolParams.FlowstdP0KfLpm, 2));
            _logger.LogInformation("Process initialized.");
        }

        /// <summary>
        /// call this for each iteration to wash a single vector of inputs
        /// according to the units stored in rawDataUnits
        /// </summary>
        /// <param name="rawDataNonstandardUnits"></param>
        /// <param name="sysState"></param>
        /// <param name="recalibratePaddleNow"></param>
        /// <returns></returns>
        public WashDataResult WashData(Dictionary<string, DataVariable> rawDataNonstandardUnits, StateMachine sysState, bool recalibratePaddleNow)
        {


            _debugCounter++;
            var rawDataUnifiedTime = UnifyTime(rawDataNonstandardUnits);
            var preProcessResult = PreProcess(rawDataUnifiedTime);

            if (preProcessResult.ErrorCode != DtectLossRealtimeErrorCodes.AllOk)
            {
                // if a tag is missing unit, or if a required tag is missing, then return with a descriptive error code.
                return new WashDataResult(null, null, null, preProcessResult.ErrorCode, preProcessResult.WarningCode);
            }

            //based on the unit, check that value is within range, and
            //check for inf.save the last good value for future use.
            var (rawDataStdUnit, lastGoodValue) = ReplaceBadValuesWithLastGoodValue(preProcessResult.RawDataStdUnit, _rawDataStdUnitsLastGoodValue);
            _rawDataStdUnitsLastGoodValue = lastGoodValue;



            var washedData = new WashedData { Values = rawDataStdUnit.Select(x => x).ToDictionary(x => x.Key, x => x.Value) };

            // handle flow in measurements
            if (rawDataStdUnit[V.FlowInLpm] >= 0)
            {
                _lowPassFlowIn.FilterForCustomDt(rawDataStdUnit[V.FlowInLpm], DtS, 0);
                //Ok to use - always not null after previous call
                washedData.Values[V.FlowInfiltLpm] = _lowPassFlowIn!.FilteredSignal ?? 0;
            }
            else
                washedData.Values[V.FlowInfiltLpm] = MissingValueReplacement;

            if (ParameterValues.PaddleParam.UseFlowInFilt)
                washedData.Values[V.FlowInLpm] = washedData.Values[V.FlowInfiltLpm];

            // Temporary range check
            if (washedData.Values[V.FlowInLpm] < 0 || washedData.Values[V.FlowInLpm] > 10000)
                washedData.Values[V.FlowInLpm] = MissingValueReplacement;


            // wash standpipe pressure and update downlinkObj
            // handle that presSP is missing by doing nothing
            if (AvailableOptionalTags.ContainsKey(V.PresSp) && AvailableOptionalTags[V.PresSp])
            {
                _downlinkDetect.Step(rawDataStdUnit[V.PresSpBar], TimeCurrentD, rawDataStdUnit[V.FlowInLpm]);
                if (!_downlinkDetect.IsDownlinking)
                {
                    PresSPdelay.UpdateTimeDelay(rawDataStdUnit[V.PresSpBar], washedData.Values[V.FlowInfiltLpm], TimeCurrentD);
                }
                if (_isenabledDelayedPressp && PresSPdelay.TimeDelayPresSpS > 0 && !_downlinkDetect.IsDownlinking)
                    washedData.Values[V.PresSpBar] = PresSPdelay.GetDelayedPresSp(TimeCurrentD);
                else
                    washedData.Values[V.PresSpBar] = rawDataStdUnit[V.PresSpBar];
            }

            washedData.IsDownlinking = _downlinkDetect.IsDownlinking;
            _isDownlinking = _downlinkDetect.IsDownlinking;

            // wash rop - use time derivative of bit depth if measured rop is not available
            if (rawDataNonstandardUnits.ContainsKey(V.VelRop))
            {
                if (rawDataNonstandardUnits[V.VelRop].V.Equals(MissingValueReplacement) && rawDataStdUnit.ContainsKey(V.VelBitMph))
                {
                    washedData.Values[V.VelRopMph] = rawDataStdUnit[V.VelBitMph];
                }
            }
            else if (rawDataStdUnit.ContainsKey(V.VelBitMph))
            {
                washedData.Values[V.VelRopMph] = rawDataStdUnit[V.VelBitMph];
            }
            else
            {
                washedData.Values[V.VelRopMph] = MissingValueReplacement;
            }

            // wash trip tank volume - use active volume if measured trip tank volume is not available
            if (rawDataNonstandardUnits.ContainsKey(V.VolTripTank))
            {
                if (rawDataNonstandardUnits[V.VolTripTank].V.Equals(MissingValueReplacement) && rawDataStdUnit.ContainsKey(V.VolTripTankM3))
                {
                    washedData.Values[V.VolTripTankM3] = rawDataStdUnit[V.VolTripTankM3];
                }
            }
            else if (rawDataStdUnit.ContainsKey(V.VolGainLossM3))
            {
                washedData.Values[V.VolTripTankM3] = rawDataStdUnit[V.VolGainLossM3];
            }
            else
            {
                washedData.Values[V.VolTripTankM3] = MissingValueReplacement;
            }

            //debug
            //var timeDebug = DateTime.FromOADate(TimeCurrentD).ToUniversalTime();
            //var timeToDebugCondition = DateTime.Parse("30/10/2015 04:34:56", new CultureInfo("nb-NO", false)).ToUniversalTime();

            // Handle flow out measurements
            PaddleConv.Convert(rawDataStdUnit[V.FlowOutLpm], washedData.Values[V.FlowInLpm], washedData.IsDownlinking, recalibratePaddleNow,
                sysState, TimeCurrentD);

            // Range check
            if (washedData.Values[V.DepthBitM] < 0 || washedData.Values[V.DepthBitM] > 10000)
                washedData.Values[V.DepthBitM] = MissingValueReplacement;

            washedData.IsCalibratingPaddleBias = PaddleConv.IsCalibratingPaddleBias;
            washedData.IsCalibratingPaddleK = PaddleConv.IsCalibratingPaddleK;
            washedData.IsCalibratedMeasFlowOut = PaddleConv.IsCalibratedPaddle;
            washedData.BiasPaddlePrc = PaddleConv.BiasPrc;
            washedData.KPaddleLpmPerPrc = PaddleConv.KLpmPerPrc;
            washedData.IsRampingUp = PaddleConv.IsRampingUp;
            washedData.StateCalibrationPaddle = PaddleConv.StateCalibrationPaddle;
            washedData.TimePumpsOnS = PaddleConv.TimePumpsOnS;
            washedData.TimePumpsOffS = PaddleConv.TimePumpsOffS;
            washedData.TimeCalibratingS = PaddleConv.TimeCalibratingS;
            washedData.Values[V.FlowOutLpm] = PaddleConv.FlowOutLpm;
            washedData.NumberPumpStops = PaddleConv.NumberPumpStops;

            // update filtered flow rate from Kalman Filter
            // covariance computation
            _kalmanFilter.AddFlowAndDepthMeas(TimeCurrentD, washedData.Values[V.FlowOutLpm], washedData.Values[V.DepthBitM], washedData.Values[V.DepthHoleM]);
            // kalman Filter for flow out
            _kalmanFilter.Update(washedData.Values[V.FlowInLpm], washedData.Values[V.FlowOutLpm], DtS);
            washedData.Values[V.FlowOutFilteredLpm] = _kalmanFilter.Xk;

            double tmpVolGainLossWithoutOffsetM3;

            double[] volGanLossTmp = { _volGainLossPrevM3, rawDataStdUnit[V.VolGainLossM3] };
            (tmpVolGainLossWithoutOffsetM3, _volGainLossOffsetM3) = MathFunctions.RemoveOffset(volGanLossTmp, _volGainLossOffsetM3,  VolGainLossThresholdM3PerTimeStep, washedData.Values[V.FlowInLpm]);
            washedData.Values[V.VolGainLossWithoutOffsetM3] = tmpVolGainLossWithoutOffsetM3;
            if (!(rawDataStdUnit[V.VolGainLossM3].Equals(MissingValCode)))
                _volGainLossPrevM3 = rawDataStdUnit[V.VolGainLossM3];

            var washedProcessOutput = new WashedProcessOutput
            {
                TimeDelayPresSpS = PresSPdelay.TimeDelayPresSpS,
                IsCalibratingSPdelay = PresSPdelay.IsCalibratingSPdelay
            };
            return new WashDataResult(washedData, rawDataStdUnit, washedProcessOutput, DtectLossRealtimeErrorCodes.AllOk, DtectLossRealtimeErrorCodes.AllOk);
    }

        /// <summary>
        /// protect against inf and out-of-range
        /// TODO: exctend this class to explot the unit in the tag name to
        /// check the range.
        /// </summary>
        /// <param name="rawDataStdUnit"></param>
        /// <param name="lastGoodValue"></param>
        /// <returns></returns>
        private static (Dictionary<string, double>, Dictionary<string, double>) ReplaceBadValuesWithLastGoodValue(Dictionary<string, double> rawDataStdUnit, Dictionary<string, double> lastGoodValue)
        {
            var badValues =
                rawDataStdUnit
                    .Where(x => IsNaN(x.Value) || IsInfinity(x.Value))
                    .ToList();
            var badValuesDict = badValues
                .ToDictionary(k => k.Key, v => v.Value);
            var goodValues = rawDataStdUnit
                .Except(badValues)
                .ToDictionary(k => k.Key, v => v.Value);

            KeyValuePair<string, double> ReplaceBad(KeyValuePair<string, double> input)
            {
                if (!badValuesDict.ContainsKey(input.Key)) return input;
                // If a previous good value exists...
                if (lastGoodValue.ContainsKey(input.Key))
                {
                    return new KeyValuePair<string, double>(input.Key, lastGoodValue[input.Key]) ;
                }
                return new KeyValuePair<string, double>(input.Key, MissingValueReplacement) ;
            }

            // New, updated dictionary where bad values have been replaced
            var newRawDataStdUnit =
                rawDataStdUnit
                    .Select(ReplaceBad)
                    .ToDictionary(k => k.Key, v => v.Value);

            // New, updated dictionary where good values has been added/updated
            var newLastGoodValues = goodValues
                .Aggregate(lastGoodValue, (accLastGoodValue, goodValue) =>
                {
                    accLastGoodValue[goodValue.Key] = goodValue.Value;
                    return accLastGoodValue;
                });

            return (newRawDataStdUnit, newLastGoodValues);
        }

        /// <summary>
        /// preprocess the data to make sure required tags are present
        /// and that necessary units are present.Convert the raw data to
        /// std units.
        /// </summary>
        /// <param name="rawDataNonstandardUnits"></param>
        /// <returns></returns>
        public PreProcessResult PreProcess(Dictionary<string, DataVariable> rawDataNonstandardUnits)
        {
            var (errorCode, warningCode) = GenerateErrorAndWarningCode(rawDataNonstandardUnits);
            if (errorCode != DtectLossRealtimeErrorCodes.AllOk)
            {
                _logger.LogError($"PreProcess: Error: {errorCode}, warning: {warningCode}");
                return new PreProcessResult(new Dictionary<string, double>(), errorCode, warningCode);
            }

            // convert units of the tags defined in the name space
            var rawDataStdUnit =
                _nameSpaceWithUnits.ToDictionary(k => k.Value,
                    v => (rawDataNonstandardUnits[v.Key].V.Equals(MissingValueReplacement))
                        ? MissingValueReplacement
                        : (double)(Convert.ToDecimal(rawDataNonstandardUnits[v.Key].V) * Convert.ToDecimal(DefaultDataUnitsFactors[v.Key])));

            // store tags which are not in the name space for later use
            var undefinedTagsToAdd = UndefinedTags.ToDictionary(k => k.v, _ => MissingValueReplacement);
            rawDataStdUnit = rawDataStdUnit.Concat(undefinedTagsToAdd).ToDictionary(k => k.Key, v => v.Value);

            //----------------special calculated tags ------------
            if (rawDataNonstandardUnits.ContainsKey(V.VelRop) && DefaultDataUnitsFactors.ContainsKey(V.VelRop))
            {
                if (!rawDataNonstandardUnits[V.VelRop].V.Equals(MissingValueReplacement))
                {
                    rawDataStdUnit.Add(V.VelRopMph, rawDataNonstandardUnits[V.VelRop].V * DefaultDataUnitsFactors[V.VelRop]);
                }
                else
                {
                    rawDataStdUnit.Add(V.VelRopMph, MissingValueReplacement);
                }
            }
            else
            {
                rawDataStdUnit.Add(V.VelRopMph, MissingValueReplacement);
            }

            // estimate block velcity using heightHook, if tag is available
            if (rawDataStdUnit.ContainsKey(V.HeighHookM) && rawDataNonstandardUnits.ContainsKey(V.HeighHook))
            {
                rawDataStdUnit.Add(V.VelBlockMph, EstimateVelBlockUsingHeightHookMeas(rawDataStdUnit[V.HeighHookM], rawDataNonstandardUnits[V.HeighHook].T));
            }
            else
            {
                rawDataStdUnit.Add(V.VelBlockMph, MissingValueReplacement);
            }

            // estimate bit velcity using depthBit, if tag is available
            if (rawDataStdUnit.ContainsKey(V.DepthBitM))
            {
                rawDataStdUnit.Add(V.VelBitMph, EstimateVelBitUsingDepthBitMeas(rawDataStdUnit[V.DepthBitM], rawDataNonstandardUnits[V.DepthBit].T));
            }
            else
            {
                rawDataStdUnit.Add(V.VelBitMph, MissingValueReplacement);
            }
            return new PreProcessResult(rawDataStdUnit, errorCode, warningCode);
        }

        private double EstimateVelBlockUsingHeightHookMeas(double heightHookM, double heightHookT)
        {
            double velBlockMph;
            if (heightHookM.Equals(MissingValueReplacement))
                velBlockMph = MissingValueReplacement;
            else
            {
                if (Abs(heightHookM - _filtHookHeight.PrevInp) > HeighthookDisconnectLimitForVelblockEstM) // Assume connected / disconnected pipe i heightHook_m deviates more than 9m from previously
                {
                    _filtHookHeight.PrevInp = heightHookM;
                    _filtHookHeight.PrevOut = 0;
                }
                double? velBlockMps = _filtHookHeight.Smoothderivative2ndorder(heightHookM, heightHookT);
                velBlockMph = velBlockMps.Value * 3600;
            }
            return velBlockMph;
        }

        public double EstimateVelBitUsingDepthBitMeas(double depthBitM, double depthBitT)
        {
            double velBitMph;
            if (depthBitM.Equals(MissingValueReplacement))
                velBitMph = MissingValueReplacement;
            else
            {
                if (Abs(depthBitM - _filtBitDepth.PrevInp) > DepthbitResetLimitForVelbitEstM)  // Assume connected / disconnected pipe i heightHook_m deviates more than 9m from previously
                {
                    _filtBitDepth.PrevInp = depthBitM;
                    _filtBitDepth.PrevOut = 0;
                }
                double? velBitMps = _filtBitDepth.Smoothderivative2ndorder(depthBitM, depthBitT);
                velBitMph = velBitMps.Value * 3600;
            }
            return velBitMph;
        }

        private (DtectLossRealtimeErrorCodes errorCode, DtectLossRealtimeErrorCodes warningCode) GenerateErrorAndWarningCode(Dictionary<string, DataVariable> rawDataNonstandardUnits)
        {
            var errorCode = DtectLossRealtimeErrorCodes.AllOk;
            var warningCode = DtectLossRealtimeErrorCodes.AllOk;

            //% check that the input contains data.If not, then return.
            if (!rawDataNonstandardUnits.Any())
            {
                return (DtectLossRealtimeErrorCodes.MissingAllValues, warningCode);
            }
            // check that the required tags are present. If not, then return
            // with errorCode indicating the first tag which is missing.
            if (AvailableRequiredTags.Values.Any(x => !x))
            {
                errorCode = GenerateNoValueErrorCode(AvailableRequiredTags.FirstOrDefault(x => !x.Value).Key);
                return (errorCode, warningCode);
            }
            // check that the required tags have a conversion factor. If
            // not, then return with an error code indictating the first tag
            // which is missing unit.
            if (_availableRequiredUnits.Values.Any(x => !x))
            {
                errorCode = GenerateNoUnitErrorCode(_availableRequiredUnits.FirstOrDefault(x => !x.Value).Key);
                return (errorCode, warningCode);
            }
            return (errorCode, warningCode);
        }

        public DtectLossRealtimeErrorCodes GenerateNoValueErrorCode(string tagName)
        {
            var errorField = $"NoValue{tagName}";
            return Enum.TryParse(errorField, true, out DtectLossRealtimeErrorCodes enumCode) ? enumCode : DtectLossRealtimeErrorCodes.MissingValueNonSpecfic;
        }
        public DtectLossRealtimeErrorCodes GenerateNoUnitErrorCode(string tagName)
        {
            var errorField = $"NoUnit{tagName}";
            return Enum.TryParse(errorField, true, out DtectLossRealtimeErrorCodes enumCode) ? enumCode : DtectLossRealtimeErrorCodes.MissingUnitNonSpecfic;
        }

        public Dictionary<string, DataVariable> UnifyTime(Dictionary<string, DataVariable> rawDataNonstandardUnits)
        {
            // Matlab comment
            // use the most recent time of the tags defined in the name
            // space as time base
            // store all the times in an array, find the maximum, and set
            // tags older than the allowed time - slack to missing value code.

            var timebase =
                NameSpace
                    .Select(x => rawDataNonstandardUnits[x].T)
                    .Max();

            DataVariable SetMissingValueForOlderTimes(DataVariable variable)
            {
                if (Abs(timebase.DiffInSeconds(variable.T)) > TimeSlackSeconds)
                {
                    return new DataVariable(variable.T, MissingValueReplacement);
                }
                return variable;
            }

            // The Matlab code only modified values in the namespace
            // TODO: Verify if this has some implications
            var updatedRawDataNonstandardUnits =
                rawDataNonstandardUnits
                    .ToDictionary(k => k.Key, v => SetMissingValueForOlderTimes(v.Value));

            // Update the stored times and time-step
            // TODO: Should this be moved outside of this method?
            _timeLastD = TimeCurrentD;
            TimeCurrentD = timebase;
            DtS = TimeCurrentD.DiffInSeconds(_timeLastD);

            return updatedRawDataNonstandardUnits;
        }

        /// <summary>
        /// This was a function in dtectlossRealtime, but was moved since all data was updated in the process object
        /// Called from dtectLossRealtime initialize
        /// </summary>
        /// <param name="rawDataUnits"></param>
        public void UpdateTagList(Dictionary<string, string> rawDataUnits)
        {
            SpecifyUnits(rawDataUnits);
            SetAvailableTags(rawDataUnits);
            DefineNamespace();
        }

        /// <summary>
        /// define the namespace as tags which are available with a
        /// defined unit conversion
        /// Sets nameSpace, _nameSpaceWithUnits, _undefinedTags, 
        /// </summary>
        private void DefineNamespace()
        {
            var availableRequiredTagsAndUnits = AvailableRequiredTags.LogicAnd(_availableRequiredUnits);
            var availableOptionalTagsAndUnits = AvailableOptionalTags.LogicAnd(_availableOptionalUnits);

            if (!availableRequiredTagsAndUnits.Values.Any(x => x))
            {
                // return with empty name space if no valid combination of
                // required tags and units
                //TODO:Check if there is a need for warning
                return;
            }
            var requiredTagsWithUnits = _requiredTags.SubsetAsEnumerable(availableRequiredTagsAndUnits);
            var optionalTagsWithUnits = _optionalTags.SubsetAsEnumerable(availableOptionalTagsAndUnits);

            // in case the flowOut tag is available but has a undefined
            // unit, make sure it is included in the namespace anyway.It
            // will be converted to lpm later.Put the remainder of the
            // optional and required tags in _undefinedTags
            var flowOutExistsInOptionalTags = _optionalTags.ToDictionary(k => k, v => v.Equals(V.FlowOut));
            var volTripTankExistsInOptionalTags = _optionalTags.ToDictionary(k => k, v => v.Equals(V.VolTripTank));

            IEnumerable<string> missingOptionalTags;
            IEnumerable<string> missingOptionalUnits;
            if (AvailableOptionalTags.HasTrueValue(V.FlowOut) && !_availableOptionalUnits.HasTrueValue(V.FlowOut))
            {
                NameSpace = requiredTagsWithUnits.Concat(optionalTagsWithUnits).Append(V.FlowOut);
                missingOptionalTags = _optionalTags
                    .Except(availableOptionalTagsAndUnits.LogicNot().LogicAnd(flowOutExistsInOptionalTags.LogicNot())
                        .SubsetOnlyFalseValues());
                //TODO: Check if logicnots can be simplified. Fixed in a rush to complete unit test.
                missingOptionalUnits = _optionalUnits
                    .Zip(availableOptionalTagsAndUnits.LogicNot().LogicAnd(flowOutExistsInOptionalTags.LogicNot()).Select(x => x.Value), (u, used) => (u, used)).Where(x => x.used)
                    .Select(x => x.u);
            }
            else if (AvailableOptionalTags.HasTrueValue(V.VolTripTank) && !_availableOptionalUnits.HasTrueValue(V.VolTripTank))
            {
                NameSpace = requiredTagsWithUnits.Concat(optionalTagsWithUnits).Append(V.VolTripTank);
                missingOptionalTags = _optionalTags
                    .Except(availableOptionalTagsAndUnits.LogicNot().LogicAnd(volTripTankExistsInOptionalTags.LogicNot())
                        .SubsetOnlyFalseValues());
                missingOptionalUnits = _optionalUnits
                    .Zip(availableOptionalTagsAndUnits.LogicNot().LogicAnd(volTripTankExistsInOptionalTags.LogicNot()).Select(x => x.Value), (u, used) => (u, used)).Where(x => x.used)
                    .Select(x => x.u);
            }
            else
            {
                NameSpace = requiredTagsWithUnits.Concat(optionalTagsWithUnits);
                missingOptionalTags = _optionalTags
                    .Except(availableOptionalTagsAndUnits.LogicNot()
                        .SubsetOnlyFalseValues());
                missingOptionalUnits = _optionalUnits
                    .Zip(availableOptionalTagsAndUnits.Select(x => x.Value), (u, used) => (u, used)).Where(x => !x.used)
                    .Select(x => x.u);
            }

            _nameSpaceWithUnits = AppendUnits(NameSpace, DefaultDataUnits);

            var missingRequiredTags = _requiredTags.SubsetAsEnumerable(availableRequiredTagsAndUnits.LogicNot());
            var missingRequiredUnits = _requiredUnits.SubsetAsEnumerable(availableRequiredTagsAndUnits.LogicNot());
            var tmpUndefinedTags = missingRequiredTags.Concat(missingOptionalTags);
            var undefinedUnits = missingRequiredUnits.Concat(missingOptionalUnits).ToArray();

            UndefinedTags =
                tmpUndefinedTags.Select((x, i) => (x, undefinedUnits[i].Length > 0 ? x + "_" + undefinedUnits[i] : x));

        }

        private Dictionary<string, string> AppendUnits(IEnumerable<string> nameSpace, Dictionary<string, string> units)
        {
            return nameSpace.ToDictionary(k => k,
                v => v.Equals("rpm", StringComparison.OrdinalIgnoreCase) ? v : v + "_" + units[v]);
        }

        /// <summary>
    ///% indicate if the tag is available
    /// </summary>
    /// <param name="rawDataUnits"></param>
    public void SetAvailableTags(Dictionary<string, string> rawDataUnits)
        {
            AvailableRequiredTags = _requiredTags.ToDictionary(x => x, rawDataUnits.ContainsKey);
            AvailableOptionalTags = _optionalTags.ToDictionary(x => x, rawDataUnits.ContainsKey);
        }

        /// <summary>
        /// specify the unit conversion factor for each tag.
        ///  Sets defaultUnitsFactor, defaultDataUnits, availableRequiredUnits and availableOptionalUnits. Also warnings
        /// </summary>
        /// <param name="rawDataUnits"></param>
        public void SpecifyUnits(Dictionary<string, string> rawDataUnits)
        {
            RawDataUnits = rawDataUnits;
            var defaultUnitsMapping = _defaultUnitsMappingReader.ReadDefaultUnitsMapping();
            var tmpDefaultDataUnitsFactors = GetDefaultUnitsFactors(defaultUnitsMapping, RawDataUnits);
            DefaultDataUnitsFactors = tmpDefaultDataUnitsFactors.ToDictionary(x => x.Key, v => v.Value.factor);
            DefaultDataUnits = GetDefaultUnits(defaultUnitsMapping, RawDataUnits);
            _availableRequiredUnits = 
                _requiredTags
                .ToDictionary(
                    x => x,
                    v => rawDataUnits.ContainsKey(v)
                         && tmpDefaultDataUnitsFactors.ContainsKey(v)
                         && tmpDefaultDataUnitsFactors[v].conversionExists);
            _availableOptionalUnits = 
                _optionalTags
                .ToDictionary(
                    x => x,
                    v => rawDataUnits.ContainsKey(v)
                         && tmpDefaultDataUnitsFactors.ContainsKey(v)
                         && tmpDefaultDataUnitsFactors[v].conversionExists);

        }

        private Dictionary<string, string> GetDefaultUnits(Dictionary<string, VariableUnitMapping> defaultUnitsMapping, Dictionary<string, string> rawDataUnits)
        {
            var units =
                rawDataUnits
                    .ToList()
                    .ToDictionary(x => x.Key, y =>  GetUnit(defaultUnitsMapping, y.Key).Replace("/", "p") );
            return units;
        }

        private string GetUnit(IReadOnlyDictionary<string, VariableUnitMapping> defaultUnitsMapping, string variableName)
        {
            if (defaultUnitsMapping.ContainsKey(variableName))
            {
                return defaultUnitsMapping[variableName].DefaultUnit!;
            }
            _logger.LogWarning($"PROCESS_OBJ:variableNotInUnitMap: The variable {variableName} has no default unit. The factor 1 will be used");
            return "";
        }

        private Dictionary<string, (bool conversionExists, double factor)> GetDefaultUnitsFactors(Dictionary<string, VariableUnitMapping> defaultUnitsMapping, Dictionary<string, string> rawDataUnits)
        {
            var factors =
                rawDataUnits
                    .ToList()
                    .ToDictionary(x => x.Key, y =>  GetUnitFactor(defaultUnitsMapping, y.Key, y.Value) );
            return factors;
        }

        private (bool conversionExists, double factor) GetUnitFactor(IReadOnlyDictionary<string, VariableUnitMapping> defaultUnitsMapping, string variableName, string unitName)
        {
            if (defaultUnitsMapping.ContainsKey(variableName) && defaultUnitsMapping[variableName].DefaultUnit!.Equals(unitName))
            {
                return (true, 1);
            }
            if (defaultUnitsMapping.ContainsKey(variableName) &&  defaultUnitsMapping[variableName].UnitFactors!.ContainsKey(unitName.Replace("/","p")))
            {
                return (true, defaultUnitsMapping[variableName].UnitFactors![unitName.Replace("/", "p")]);
            }
            _logger.LogWarning($"PROCESS_OBJ:noConversionInUnitMap: The variable {variableName} is missing a conversion factor for {unitName}. The factor 1 will be used");
            return (false, 1);
        }
    }
}
