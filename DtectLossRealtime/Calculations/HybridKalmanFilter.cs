﻿using System.Linq;
using DtectLoss.Common;
using DtectLossRealtime.Helpers;
using static System.Math;
#nullable enable

namespace DtectLossRealtime.Calculations
{
    /// <summary>
    /// HYBRID_KALMAN_FILTER Hybrid Kalman Filter for one state (scalar)
    /// https://en.wikipedia.org/wiki/Kalman_filter
    /// </summary>
    public class HybridKalmanFilter
    {
        public double Xk { get; private set; }
        public bool SleepModeActive { get; private set; }
        public bool TotalLossOfReturnFlowRateBoolean { get; private set; }
        public double TimeDurationWithZeroReturnFlowS { get; private set; }
        public int NBuffer { get; } = 100 + 45;

        private double _r;
        private double _pk;
        private double _xPrevPri;
        private double _pPrevPri;
        private RingBuffer _ringBuffer;
        private readonly double _q = Pow(50, 2);
        private const double A = -1/6.0;
        private const double B = 1/6.0;
        private const double C = 1;
        private const double TimeStepUpperLimS = 60; // [s] do not update if the time-step is larger than this limit
        private const double ErrVal = CommonConstants.MissingValueReplacement;
        private const double DistanceConsideredDrillingM = 0.5; // [m]
        private const double TimeDurationConsideredAsLossOfReturnFlowS = 20; // [s]
        private const double FlowOutLowLimitLpm = 5;

        public HybridKalmanFilter()
        {
            _ringBuffer = new RingBuffer(NBuffer); // ringbuffer
            SleepModeActive = true;
            TotalLossOfReturnFlowRateBoolean = false;
        }

        public void Initialize(double covMeas, double x, double p)
        {
            SleepModeActive = true;
            TotalLossOfReturnFlowRateBoolean = false;
            _r = covMeas;
            Xk = x;
            _pk = p;
            _xPrevPri = x;
            _pPrevPri = p;
            SleepModeActive = x <= 0;
            TimeDurationWithZeroReturnFlowS = 0;
        }

        public void SoftReset()
        {
           Xk = 0;
           _xPrevPri = 0;
           SleepModeActive = true;
           TotalLossOfReturnFlowRateBoolean = false;
           TimeDurationWithZeroReturnFlowS = 0;
        }

        public void AddFlowAndDepthMeas(double flowOutTimeD, double flowOutLpm, double depthBitM, double depthHoleM)
        {
            if (Abs(depthBitM - depthHoleM) < DistanceConsideredDrillingM)
            {
                _ringBuffer.AddDataToCircularBuffer(flowOutTimeD, flowOutLpm);
                var (varCalc, _) = _ringBuffer.VarFirstHalf();
                if (varCalc != null)
                    SetMeasCovar(varCalc.Value);
            }
            else
                SetMeasCovar(_q);
        }

        public double SetMeasCovar(double measCovar)
        {
            _r = measCovar;
            return _r = measCovar;
        }

        public void Update(double flowInLpm, double flowOutLpm, double dtS)
        {
            // if time step is zero, do nothing.
            if (dtS.Equals(0))
                return;
               
            // if time step is too large, or measurment or input is missing,
            // set estimate to errVal.
            if (dtS > TimeStepUpperLimS || flowOutLpm.Equals(ErrVal) || flowInLpm.Equals(ErrVal))
            {
                Xk = ErrVal;
                return;
            }

            // if the measured return flow - rate is different from zero,
            // deactivate sleep mode.
            if (!flowOutLpm.Equals(0))
                SleepModeActive = false;
            else if (_ringBuffer.Vbuffer.All(x => x.Equals(0)))
                SleepModeActive = true;

            // if the measured return flow - rate has not been above zero
            // since init, or if the measured return flow has been around
            // zero for a given time, then return with zero as predicted
            //return flow - rate.
            CheckForLossOfReturnFlow(flowOutLpm, dtS);
            if (SleepModeActive || TotalLossOfReturnFlowRateBoolean)
            {
                Xk = 0;
                return;
            }
            
            // Predict
            //a priori state estimate
            var xKPri = _xPrevPri + dtS * (A * _xPrevPri + B * flowInLpm);
            // a priori estimate covariance
            var pkPri = _pPrevPri + dtS * (Pow(A, 2) * _pPrevPri + _q);


           // Update
            var yErrK = flowOutLpm - C * xKPri;
            var sk = Pow(C, 2) * pkPri + _r;
            if (sk == 0) // avoid division with zero
                sk = 1e-5;
           
            var kk = pkPri * C / sk;
            Xk = xKPri + kk * yErrK;
            _pk = (1 - kk * C) * pkPri;

            _xPrevPri = Xk;
            _pPrevPri = _pk;
        }

        public void CheckForLossOfReturnFlow(double flowOutLpm, double timeStepS)
        {
            ZeroReturnFlowTimer(flowOutLpm, timeStepS);
            TotalLossOfReturnFlowRateBoolean = TimeDurationWithZeroReturnFlowS > TimeDurationConsideredAsLossOfReturnFlowS;
        }

        public void ZeroReturnFlowTimer(double flowOutLpm, double timeStepS)
        {
            if (flowOutLpm > FlowOutLowLimitLpm || flowOutLpm.Equals(ErrVal))
                TimeDurationWithZeroReturnFlowS = 0;
            else
                TimeDurationWithZeroReturnFlowS += timeStepS;
        }

    }
}
