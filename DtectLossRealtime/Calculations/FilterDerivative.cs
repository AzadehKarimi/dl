﻿using DtectLoss.Common;
using System;
using static System.Double;
using static System.Math;

namespace DtectLossRealtime.Calculations
{
    /// <summary>
    /// Filter data and estimate time derivative
    /// </summary>
    public class FilterDerivative
    {
        public double DtS { get; private set; }
        public double PrevInp { get; set; }
        public double PrevOut { get; set; }

        private readonly double _tfiltS;
        private readonly double _damping;
        private readonly double _timestepmaxS;
        private double _prevtimeD;
        private double _prevoutder;
        private double _newoutder;

        public FilterDerivative(double tfiltS)
        {
            _prevtimeD = 0;
            DtS = 0;
            PrevInp = 0;
            PrevOut = 0;
            _prevoutder = 0;
            _tfiltS = tfiltS;
            _damping = 1;
            _timestepmaxS = 60;  // Reset Filter if timestep is bigger than this
        }

        public double Smoothderivative1storder(double newinp, double newtimeD)
        {
            double newout;
            DtS = newtimeD.DiffInSeconds(_prevtimeD);
            if (IsNaN(newinp) || IsNaN(newtimeD) || DtS <= 0)  //do nothing
                newout = PrevOut;

            else if (DtS < _timestepmaxS)
                newout = (1 - DtS / _tfiltS) * PrevOut + (newinp - PrevInp) / _tfiltS;
            else   // reset
                newout = 0;
            
            if (IsNaN(newout))
                newout = PrevOut;
            
            _prevtimeD = newtimeD;
            PrevOut = newout;
            PrevInp = newinp;
            return newout;
        }

        public double Smoothderivative2ndorder(double newinp, double newtimeD)
        {
            double newout;
            DtS = newtimeD.DiffInSeconds(_prevtimeD);
            if (IsNaN(newinp) || IsNaN(newtimeD) ||  DtS <= 0) // do nothing
            {
                _newoutder = _prevoutder;
                newout = PrevOut;
            }
            else if (DtS < _timestepmaxS)
            {
                _newoutder = (double)((1m - 2m * Convert.ToDecimal(_damping) * Convert.ToDecimal(DtS) / Convert.ToDecimal(_tfiltS)) * Convert.ToDecimal(_prevoutder) 
                            + Convert.ToDecimal(newinp - PrevInp) / Convert.ToDecimal(Pow(_tfiltS, 2))) 
                            - (DtS / Pow(_tfiltS, 2) * PrevOut);
                newout = PrevOut + DtS * _prevoutder;
            }
            else   // reset
            {
                _newoutder = 0;
                newout = 0;
            }

            if (IsNaN(newout) || IsNaN(_newoutder))
            {
                newout = PrevOut;
                _newoutder = _prevoutder;
            }
            
            _prevtimeD = newtimeD;
            _prevoutder = _newoutder;
            PrevOut = newout;
            PrevInp = newinp;
            return newout;
        }
    }
}
