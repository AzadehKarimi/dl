﻿#nullable enable
using Accord.Math;
using Accord.Statistics;
using Accord.Statistics.Models.Regression.Linear;
using Microsoft.Extensions.Logging;

namespace DtectLossRealtime.Calculations
{
    public class LinReg
    {
        private readonly ILogger<LinReg> _logger;
        public double[] Coeffs { get; private set; } = { };
        private double _sse;
        private double? _ssr;
        private double? _sst;
        private double _rmse;
        private double _rSquared;
        private const bool DoDebug = false;
        private readonly OrdinaryLeastSquares _ols = new OrdinaryLeastSquares();

        public LinReg(ILogger<LinReg> logger)
        {
            _logger = logger;
            _ols.UseIntercept = false;
        }

        public void FitData(double[,] x, double[] y)
        {
            var n = y.Length;
            if (x.GetLength(1) + 1 < n)
            {
                var a = x.Concatenate(Vector.Create(n, 1.0));
                var lr = _ols.Learn(a.ToJagged(), y);
                Coeffs = lr.Weights;
                CalcSse(x, y);
                CalcRSquared(x, y);
                //if (DoDebug)
                //    _logger.LogDebug("RMSE: %0.2f, RSquared: %0.2f\n", _rmse, _rSquared);
            }
        }

        public double[] PredictFromData(double[,] x)
        {
            return x.Concatenate(Vector.Create(x.GetLength(0), 1.0)).Dot(Coeffs);
        }

        private void CalcSse(double[,] x, double[] y)
        {
            var yEst = PredictFromData(x);
            _sse = y.Subtract(yEst)
                            .Pow(2)
                            .Sum();
            var m = x.GetLength(1) + 1; //size(X, 2) + 1;
            var n = x.GetLength(0); //size(X, 1);
            if (n <= m) return;
            double v = n - m;
            _rmse = _sse / v;
        }

        private void CalcSsr(double[,] x, double[] y)
        {
            var yEst = PredictFromData(x);
            _ssr = (yEst.Subtract(y.Mean()).Pow(2)).Sum();
        }

        private void CalcSst(double[] y)
        { 
           _sst = (y.Subtract(y.Mean()).Pow(2)).Sum();
        }

        private void CalcRSquared(double[,] x, double[] y)
        {
            CalcSsr(x, y);
            CalcSst(y);
            if (_ssr != null && _sst != null && _sst > 0.0)
             _rSquared = _sst.Value / _ssr.Value;
        }
    }
}

