﻿#nullable enable
using System.Linq;
using Accord.Math;
using Microsoft.Extensions.Logging;
using static System.Double;
using static System.Math;
using static DtectLoss.Common.CommonConstants;

namespace DtectLossRealtime.Calculations
{
    /// <summary>
    /// partition input data, train samples in each grid and estimate output by interpolation
    /// </summary>
    public class PiecewiseLinFit
    {
        public bool IsTrained { get; private set; }

        private readonly ILogger<PiecewiseLinFit> _logger;
        private int _nInput;
        private double _nFilt;
        private double _sizeGrid;
        private double[] _inputArr = {};
        private double[] _outputArr = {};
        private int[] _numArr = {};
        private const int NumArrCountCriteria = 5;

        public PiecewiseLinFit(ILogger<PiecewiseLinFit> logger)
        {
            _logger = logger;
        }

        public void Initialize(double inputMax, double inputMin, int nInput, double nFilt)
        {
            _nInput = nInput;
            _nFilt = nFilt;
            _sizeGrid = (inputMax - inputMin) / nInput;
            _inputArr = new double[nInput];
            _outputArr = new double[nInput];
            _numArr = new int[nInput];
            IsTrained = false;
        }

        public void Train(double newinput, double newoutput)
        {
            newinput = Max(newinput, 0);
            var index = (int)Floor(newinput / _sizeGrid);
            if (index >= 0 && index < _nInput)
            {
                _numArr[index] = _numArr[index] + 1;
                _inputArr[index] = _inputArr[index] + 1 / Min(_numArr[index], _nFilt) * (newinput - _inputArr[index]);
                _outputArr[index] = _outputArr[index] + 1 / Min(_numArr[index], _nFilt) * (newoutput - _outputArr[index]);
            }
            else
            {
                _logger.LogError("PIECEWISELINFIT_OBJ:train - Error");
            }
            var nonzeroind = _numArr.Find(x => x > NumArrCountCriteria).ToList();
            IsTrained = nonzeroind.Count > 1;
        }

        public double InterpolWithExtrapolation(double newinput)
        {
            double expout;
            var nonzeroind = _numArr.Find(x => x > NumArrCountCriteria).ToList();
            if (nonzeroind.Count > 2)
            {
                for (var k = 1; k <= nonzeroind.Count - 1; k++) // make array monotonely increasing
                {
                    if (_outputArr[nonzeroind[k]] < _outputArr[nonzeroind[k - 1]])
                    {
                        //Use number of instants to weigh average
                        double n1 = Min(100, _numArr[nonzeroind[k]]);
                        double n2 = Min(100, _numArr[nonzeroind[k - 1]]);

                        _outputArr[nonzeroind[k]] = (n1 * _outputArr[nonzeroind[k]] + n2 * _outputArr[nonzeroind[k - 1]]) / (n1 + n2);
                        _outputArr[nonzeroind[k - 1]] = _outputArr[nonzeroind[k]];
                    }
                }

                var arrx = _inputArr.Get(nonzeroind);
                var arry = _outputArr.Get(nonzeroind);
                if (arry.Sum().Equals(0))
                    expout = MissingValueReplacement;
                else
                {
                    // C# rewrite - Added extra extrapolation that was done in Matlab.
                    // TODO: Refactor later
                    // The unit test sometimes fail with a larger variance. Check if this is due to extrapolation
                    // The Matlab unit test also sometimes fails
                    if (newinput > arrx.Max())
                    {
                        var last2Y = arry.Reverse().Take(2).ToArray();
                        var last2X = arrx.Reverse().Take(2).ToArray();
                        var slope = (last2Y[1] - last2Y[0]) / (last2X[1] - last2X[0]);
                        var est = newinput * slope;
                        var b = (slope * last2X[0] - last2Y[0]) * -1;
                        expout = est + b;
                    }
                    else if (newinput < arrx.Min())
                    {
                        var last2Y = arry.Take(2).ToArray();
                        var last2X = arrx.Take(2).ToArray();
                        var slope = (last2Y[1] - last2Y[0]) / (last2X[1] - last2X[0]);
                        var est = newinput * slope;
                        var b = (slope * last2X[0] - last2Y[0]) * -1;
                        expout = est + b;
                    }
                    else
                    {
                        expout = Tools.Interpolate1D(newinput, arrx, arry, arry.Min(), arry.Max()); // 'spline'
                    }
                }
               
                expout = Min(1.1 * arry.Max(), expout);
                expout = Max(0.9 * arry.Min(), expout);
         
                if (IsNaN(expout))
                    expout = MissingValueReplacement;
                else if (expout < 0)
                    expout = 0;
            }
            else
            {
                expout = MissingValueReplacement;  
                // disp('Not trained yet');
            }
            return expout;
        }
    }
}
