﻿using System;

namespace DtectLossRealtime.Calculations
{
    public class LowPass
    {
        public double? FilteredSignal;
        private double? _prevFilteredSignal;
        private readonly double _dtS; // sampling time constant
        private readonly double _tS;  // timeconstant of lowpass Filter

        public LowPass(double? dtS, double ts)
        {
            _dtS = dtS ?? double.NaN;
            _tS = ts;
        }

        public void Filter(double signal, int reset)
        {
            double a;
            if  (_dtS >= _tS)
                a = 0;
            else
            {
               a = (double)(1m / (1m + Convert.ToDecimal(_dtS) / Convert.ToDecimal(_tS)));
            }
            FilterInternal(signal, reset, a);
        }

        public void FilterForCustomDt(double signal, double dts, int reset)
        {
            double a;
            if  (dts >= _tS)
                a = 0;
            else
            {
                a = (double)(1m / (1m + Convert.ToDecimal(dts) / Convert.ToDecimal(_tS)));
            }
            FilterInternal(signal, reset, a);
        }
           
        // for a given value of "a" calculate the filtered signal
        public void FilterInternal(double signal, int reset, double a)
        {
            if (_prevFilteredSignal ==null || a.Equals(0.0)) // initalize on first call
               FilteredSignal = signal; 
            else
               FilteredSignal = (double)(Convert.ToDecimal(a) * Convert.ToDecimal(_prevFilteredSignal.Value) + (1m - Convert.ToDecimal(a)) * Convert.ToDecimal(signal));
           
            _prevFilteredSignal = FilteredSignal.Value;

            if (reset != 1) return;
            FilteredSignal = signal;
            _prevFilteredSignal = signal;
        }
    }
}
