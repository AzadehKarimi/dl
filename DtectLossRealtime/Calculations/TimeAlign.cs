﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;
using Accord.Math;
using DtectLoss.Common;
using DtectLossRealtime.Models;
using Microsoft.Extensions.Logging;
using static DtectLoss.Common.CommonConstants;

namespace DtectLossRealtime.Calculations
{
    public class TimeAlign
    {
        private readonly ILogger<TimeAlign> _logger;
        private int _nVec;
        private int _bestIndex;
        //private double _resultingSum;
        //private LowPass _lp;
        //private readonly Stopwatch _stopwatch = new Stopwatch();
        private readonly bool doDebugText = false; //true;
        public bool IsObjFunImproving;
        public double DtAvgD;

        public TimeAlign(ILogger<TimeAlign> logger)
        {
            _logger = logger;
        }

        public void Initialize(Flowout parameters)
        {
            _nVec = parameters.NVec;
        }

        //aligns values in inputSignal and refSignal, both values sharing the same 
        //time-vector timeVec_d
        //inputSignal: signal to be shifted and filtered to match refSignal
        //refSignal: reference signal that inputSignal should try to match
        //timeVec_d : vector of time in matlab-days

        public (int timeDelaySamples, double timeConstantS, double timeDelayS) Align(double[] inputSignal,
            double[] refSignal, double[] timeVecD)
        {
            //if (doDebugText)
            //   _stopwatch.Start();

            DtAvgD= timeVecD.Skip(1).Zip(timeVecD.Take(timeVecD.Length - 1), (d, d1) => d - d1).Average();
            double nSamples = inputSignal.Length;

            var timeDelaySamples = 0;
            double timeConstantS = 0;
            var funLast = CalcDiffSum(timeDelaySamples, timeConstantS, timeVecD, inputSignal, refSignal);
            var timeConstantBest = timeConstantS;
            var timeDelaySamplesBest = timeDelaySamples;
            var funBest = funLast;

            //if (doDebugText) 
            //    _logger.LogDebug("\nStart align \n");

            // first iterate just on time-delay,Tc=0
            timeDelaySamples = 1;
            IsObjFunImproving = true;
            while (IsObjFunImproving && timeDelaySamples < nSamples)
            {
                var fun = CalcDiffSum(timeDelaySamples, timeConstantS, timeVecD, inputSignal, refSignal);
                //if (doDebugText) 
                //    _logger.LogDebug(
                //        $"Testing delay - timeDelay:{timeDelaySamples} %i samples | timeConstant:{timeConstantS} %.2f sec| obj:{fun} %.4f \n");

                if (fun < funLast)
                {
                    if (fun < funBest)
                    {
                        funBest = fun;
                        timeDelaySamplesBest = timeDelaySamples;
                    }
                    // keep looking
                    timeDelaySamples += 1;
                    funLast = fun;
                }
                else
                    IsObjFunImproving = false;
            }

            timeDelaySamples = timeDelaySamplesBest;

            // part 2: try decreasing time-delay by multiples of dt_avg_d
            // and increasing Tc likewise, see if objective function
            // improves
            IsObjFunImproving = true;
            var timeConstantMaxS = (DtAvgD * 3) * SecondsIn24Hours;

            var timeConstantResolutionS = timeConstantMaxS / Convert.ToDouble(_nVec);
            while (IsObjFunImproving && timeDelaySamples >= 1)
            {
                timeDelaySamples -= 1;
                
                var timeConstantVecS = Enumerable.Range(1, _nVec)
                    .Select(x => timeConstantBest + x * timeConstantResolutionS)
                    .ToArray();

                // From matlab - todo: you could improve the results somewhat by generalizing this to a sort of binary search algorithm for Tc
                var funVec = new List<double>();
                foreach (var ii in Enumerable.Range(0, _nVec))
                {
                    funVec.Add(CalcDiffSum(timeDelaySamples, timeConstantVecS[ii], timeVecD, inputSignal, refSignal));
                    //if (doDebugText)
                    //    _logger.LogDebug( $"Testing time Constant - timeDelay: {timeDelaySamples} %i samples | timeConstant: {timeConstantVecS[ii]} %.2f sec | obj: {funVec[ii]} %.4f \n");
                }

                if (funVec.Min() > funBest)
                    IsObjFunImproving = false;
                else
                {
                    for (var i = 0; i < funVec.Count; i++)
                    {
                        if (funVec[i] <= (funVec).Min())
                        {
                            _bestIndex = i;
                            break;
                        }
                        // best_index = Fun_Vec.IndexOf(Fun_Vec.Min());
                    }

                    funBest = funVec[_bestIndex];
                    timeConstantBest = timeConstantVecS[_bestIndex];
                    timeDelaySamplesBest = timeDelaySamples;
                    if (doDebugText)
                        _logger.LogDebug($"Testing best so far: timeDelay:{timeDelaySamplesBest} %i samples | timeConstant:{timeConstantBest} %.2f sec | obj:{funBest} %.4f \n");
                }
            }

            timeConstantS = timeConstantBest;
                timeDelaySamples = timeDelaySamplesBest;
                var timeDelayS = DtAvgD.OADateToSeconds() * timeDelaySamples;
                //if (doDebugText)
                //    _logger.LogDebug("Align function duration: " + (_stopwatch.ElapsedMilliseconds /1000) + "sec\n");
                //_stopwatch.Reset();

                return (timeDelaySamples, timeConstantS, timeDelayS);
        }

        // inputSignal and refSignal are values of time-series to be calculated difference
        // for
        public double CalcDiffSum(int timeDelaySamples, double timeConstantS, double[] timeVecD,
            double[] inputSignal, double[] refSignal)
        {
            var inputSignalShifted = inputSignal.Take(inputSignal.Length - timeDelaySamples).ToArray();

            var deltaTs = (timeVecD[1] - timeVecD[0]) * SecondsIn24Hours;
            double[] inputSignalShiftedAndFiltered; 
            if (timeConstantS > 0)
            {
                var lp = new LowPass(deltaTs, timeConstantS);
                var inputSignalShiftedAndFilteredList = new List<double>();
                lp.Filter(refSignal[0], 1);
                inputSignalShiftedAndFilteredList.Add(lp.FilteredSignal ?? 0);
                for (var k = 1; k < inputSignalShifted.Length; k++)
                {
                    lp.Filter(inputSignalShifted[k], 0);
                    inputSignalShiftedAndFilteredList.Add(lp.FilteredSignal ?? 0);
                }

                inputSignalShiftedAndFiltered = inputSignalShiftedAndFilteredList.ToArray();
            }
            else
                inputSignalShiftedAndFiltered = inputSignalShifted;

            var refSignaladjusted = refSignal.Skip(timeDelaySamples).ToArray();
            double N = inputSignal.Length - timeDelaySamples;
            //if (doDebugText)
            //    _logger.LogDebug( $"calcDiffSum - timeDelay:{timeDelaySamples}, {timeDelaySamples * deltaTS:N2} %i samples (%.2f sec) | timeConstant:{timeConstantS} %.2f sec | obj:{_resultingSum} %.4f\n");

            var result = inputSignalShiftedAndFiltered.Subtract(refSignaladjusted).Pow(2).Select(x => x / N).Sum();
            return result;
        }
    }

}
