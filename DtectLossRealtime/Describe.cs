﻿using System.Collections.Generic;
using System.Text;
using DtectLoss.Common;
using DtectLossRealtime.Enums;
using DtectLossRealtime.Models;

namespace DtectLossRealtime
{
  /// <summary>
  ///   DESCRIBE_OBJ Summary of this class goes here
  ///  Detailed explanation goes here
  ///    Describe washed input data: check minimum data set available(flow in/out, pit vol, bit depth and hole depth)
  ///   Statistics: system states(drilling, tripping, flow check, conditioning)
  ///   TO DO: identify and count number of connections drilling and tripping
  /// </summary>
  public class Describe
  {
    // Statistics
    private double _numDataPoints;
    //private double _numPumpStarts;
    private double _lengthTimeSeriesS;
    private double _dtS;
    private double _timeBadDataS;
    private double _timeDrillingS;
    private double _timeFlowCheckS;
    private double _timeTrippingS;
    private double _timeConditioningS;
    private double _timeStepMaxS;
    private double _timeStepAvgS;
    private double _depthBitMinM;
    private double _depthBitMaxM;
    private double _depthHoleMinM;
    private double _depthHoleMaxM;
    private double _velBlockMinMph;
    private double _velBlockMaxMph;
    private double _rotationMaxRpm;
    private double _flowOutMaxLpm;
    private double _flowInMaxLpm;
    private double _volPitMinM3;
    private double _volPitMaxM3;
    private double _volTripTankMinM3;
    private double _volTripTankMaxM3;
    private double _presSpMaxBar;
    private bool _seriesOk;
    private double _flowOutrawMinLpm;
    private double _flowOutrawMaxLpm;
    private double _tempBhaMinDegC;
    private double _tempBhaMaxDegC;
    private const double TimeStepRejectS = 60;

    public void Initialize()
    {
      //_numPumpStarts = 0;
      _lengthTimeSeriesS = 0;
      _dtS = 0;
      _timeBadDataS = 0;
      _timeDrillingS = 0;
      _timeFlowCheckS = 0;
      _timeTrippingS = 0;
      _timeConditioningS = 0;
      _timeStepMaxS = 0;
      _timeStepAvgS = 0;
      _flowOutrawMinLpm = 0;
      _flowOutrawMaxLpm = 0;
      _depthBitMinM = 0;
      _depthBitMaxM = 0;
      _depthHoleMinM = 0;
      _depthHoleMaxM = 0;
      _velBlockMinMph = 0;
      _velBlockMaxMph = 0;
      _rotationMaxRpm = 0;
      _flowOutMaxLpm = 0;
      _flowInMaxLpm = 0;
      _volPitMinM3 = 0;
      _volPitMaxM3 = 0;
      _volTripTankMinM3 = 0;
      _volTripTankMaxM3 = 0;
      _presSpMaxBar = 0;
      _tempBhaMinDegC = 0;
      _tempBhaMaxDegC = 0;
      _seriesOk = false;
    }

    public void SoftReset()
    {
      Initialize();
    }

    public void DescribeData(Dictionary<string, double> rawData, WashedData washedData, StateMachine sysState, double dtS)
    {
      _dtS = dtS;
      if (!(dtS > 0) || !(dtS < TimeStepRejectS)) return;
      _numDataPoints += 1;
      _lengthTimeSeriesS += dtS;
      switch (sysState.CurrentState)
      {
        case DrillingStates.BadData:
          _timeBadDataS += dtS;
          break;
        case DrillingStates.Drilling:
          _timeDrillingS += dtS;
          break;
        case DrillingStates.Tripping:
          _timeTrippingS += dtS;
          break;
        case DrillingStates.FlowCheck:
          _timeFlowCheckS += dtS;
          break;
        case DrillingStates.Conditioning:
          _timeConditioningS += dtS;
          break;
      }

      if (dtS > _timeStepMaxS)
        _timeStepMaxS = dtS;

      _timeStepAvgS = _lengthTimeSeriesS / _numDataPoints;

      if (_depthBitMinM.Equals(0) || _depthBitMinM > washedData.Values[V.DepthBitM])
        _depthBitMinM = washedData.Values[V.DepthBitM];
      if (_depthBitMaxM.Equals(0) || _depthBitMaxM < washedData.Values[V.DepthBitM])
        _depthBitMaxM = washedData.Values[V.DepthBitM];
      if (_depthHoleMinM.Equals(0) || _depthHoleMinM > washedData.Values[V.DepthHoleM])
        _depthHoleMinM = washedData.Values[V.DepthHoleM];
      if (_depthHoleMaxM.Equals(0) || _depthHoleMaxM < washedData.Values[V.DepthHoleM])
        _depthHoleMaxM = washedData.Values[V.DepthHoleM];
      if (_flowInMaxLpm.Equals(0) || _flowInMaxLpm < washedData.Values[V.FlowInLpm])
        _flowInMaxLpm = washedData.Values[V.FlowInLpm];
      if (_flowOutMaxLpm.Equals(0) || _flowOutMaxLpm < washedData.Values[V.FlowOutLpm])
        _flowOutMaxLpm = washedData.Values[V.FlowOutLpm];
      if (_flowOutrawMinLpm.Equals(0) || _flowOutrawMinLpm > rawData["flowOut_lpm"])
        _flowOutrawMinLpm = rawData["flowOut_lpm"];
      if (_flowOutrawMaxLpm.Equals(0) || _flowOutrawMaxLpm < rawData["flowOut_lpm"])
        _flowOutrawMaxLpm = rawData["flowOut_lpm"];
      if (_volPitMinM3.Equals(0) || _volPitMinM3 > washedData.Values[V.VolGainLossWithoutOffsetM3])
        _volPitMinM3 = washedData.Values[V.VolGainLossWithoutOffsetM3];
      if (_volPitMaxM3.Equals(0) || _volPitMaxM3 < washedData.Values[V.VolGainLossWithoutOffsetM3])
        _volPitMaxM3 = washedData.Values[V.VolGainLossWithoutOffsetM3];
      if (_volTripTankMinM3.Equals(0) || _volTripTankMinM3 > washedData.Values[V.VolTripTankM3])
        _volTripTankMinM3 = washedData.Values[V.VolTripTankM3];
      if (_volTripTankMaxM3.Equals(0) || _volTripTankMaxM3 < washedData.Values[V.VolTripTankM3])
        _volTripTankMaxM3 = washedData.Values[V.VolTripTankM3];
      if (_presSpMaxBar.Equals(0) || _presSpMaxBar < washedData.Values[V.PresSpBar])
        _presSpMaxBar = washedData.Values[V.PresSpBar];
      if (_velBlockMinMph.Equals(0) || _velBlockMinMph > washedData.Values[V.VelBlockMph])
        _velBlockMinMph = washedData.Values[V.VelBlockMph];
      if (_velBlockMaxMph.Equals(0) || _velBlockMaxMph < washedData.Values[V.VelBlockMph])
        _velBlockMaxMph = washedData.Values[V.VelBlockMph];
      if (_rotationMaxRpm.Equals(0) || _rotationMaxRpm < washedData.Values[V.Rpm])
        _rotationMaxRpm = washedData.Values[V.Rpm];
      if (rawData.ContainsKey(V.TempBhaDegC))
      {
        if (_tempBhaMinDegC.Equals(0) || _tempBhaMinDegC > washedData.Values[V.TempBhaDegC])
          _tempBhaMinDegC = washedData.Values[V.TempBhaDegC];
        if (_tempBhaMaxDegC.Equals(0) || _tempBhaMaxDegC < washedData.Values[V.TempBhaDegC])
          _tempBhaMaxDegC = washedData.Values[V.TempBhaDegC];
      }
    }

    public void CheckDataSeries()
    {
      _seriesOk = _lengthTimeSeriesS > 3600 && _flowInMaxLpm > 0 && _flowOutMaxLpm > 0 &&
          (_volPitMaxM3 - _volPitMinM3 > 1 || _volTripTankMaxM3 - _volTripTankMinM3 > 0.1);
    }

    public string DispSeriesAnalysis()
    {
      StringBuilder textResult = new StringBuilder();
      if (_seriesOk)
        textResult.AppendLine("Time series OK");
      else
        textResult.AppendLine("Bad time series");
      textResult.AppendLine($"Length time series = {_lengthTimeSeriesS / 60:F2} min with {_numDataPoints} data points");
      textResult.AppendLine($"Time with bad data = {_timeBadDataS / 60:F2} min ({_timeBadDataS / _lengthTimeSeriesS * 100:F2} %)");
      textResult.AppendLine($"Time spent drilling = {_timeDrillingS / 60:F2} min ({_timeDrillingS / _lengthTimeSeriesS * 100:F2} %)");
      textResult.AppendLine($"Time spent tripping = {_timeTrippingS / 60:F2} min ({_timeTrippingS / _lengthTimeSeriesS * 100:F2} %)");
      textResult.AppendLine($"Time spent flow checking = {_timeFlowCheckS / 60:F2} min ({_timeFlowCheckS / _lengthTimeSeriesS * 100:F2} %) ");
      textResult.AppendLine($"Time spent conditioning = {_timeConditioningS / 60:F2} min ({_timeConditioningS / _lengthTimeSeriesS * 100:F2} %)");
      textResult.AppendLine($"Time step:     max = {_timeStepMaxS:F2} sec,  average = {_timeStepAvgS:F2} sec");
      textResult.AppendLine($"Bit depth:     min = {_depthBitMinM:F2} m,    max = {_depthBitMaxM:F2} m");
      textResult.AppendLine($"Hole depth:    min = {_depthHoleMinM:F2} m,    max = {_depthHoleMaxM:F2} m. Drilled {_depthHoleMaxM - _depthHoleMinM:F2} m.");
      textResult.AppendLine($"Flow in:       max = {_flowInMaxLpm:F2}     lpm");
      textResult.AppendLine($"Flow out washed:      max = {_flowOutMaxLpm:F2}     lpm");
      textResult.AppendLine($"Flow out raw lpm:     min = {_flowOutrawMinLpm:F2}      lpm, max = {_flowOutrawMaxLpm:F2}     lpm");
      textResult.AppendLine($"Pit volume:    min = {_volPitMinM3:F2}      m3,   max = {_volPitMaxM3:F2}   m3");
      textResult.AppendLine($"TT volume:     min = {_volTripTankMinM3:F2}   m3,   max = {_volTripTankMaxM3:F2}   m3");
      textResult.AppendLine($"Pump pressure: max = {_presSpMaxBar:F2}   bar");

      if (_tempBhaMaxDegC > _tempBhaMinDegC)
        textResult.AppendLine($"BHA temp:      min = {_tempBhaMinDegC:F2}  degC, max = {_tempBhaMaxDegC:F2} degC");

      textResult.AppendLine($"Block velocity:min = {_velBlockMinMph / 60:F2} m/min, max = {_velBlockMaxMph / 60:F2} m/min");

      if (_timeDrillingS > 0)
        textResult.AppendLine($"ROP average:   net = {(_depthHoleMaxM - _depthHoleMinM) / _timeDrillingS * 3600:F2} m/hr,  gross = {(_depthHoleMaxM - _depthHoleMinM) / _lengthTimeSeriesS * 3600:F2} m/hr");

      textResult.AppendLine($"Rotation:      max = {_rotationMaxRpm:F2}    rpm");
      return textResult.ToString();
    }
  }
}
