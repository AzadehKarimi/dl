﻿using System.Linq;
using Microsoft.Extensions.Logging;
using DtectLoss.Common;
using DtectLossRealtime.Calculations;
using DtectLossRealtime.Enums;
using DtectLossRealtime.Helpers;
using DtectLossRealtime.Models;
using static DtectLoss.Common.CommonConstants;

namespace DtectLossRealtime
{
    /// <summary>
    /// this class calculates timeconstant T and timedelay td between
    /// flowIn and flowOut so that:
    /// 
    /// flowOut(t) = 1/(1+timeConst_s*s)*flowIn(t-timeDelay_s) 
    /// 
    /// to do that this class buffers data during a connection.
    /// Note that for DeepSeaAtlantic-19 the flowOut_lpm_wash is low-pass
    /// filtered and takes three minutes to reach zero flow after 
    /// flowIn is stopped. To avoid false alarms, we would like flowOutExpected
    /// to also handle such cases.
    /// </summary>
    public class ExpectedFlowOutFromFlowIn
    {
        private readonly ILogger<ExpectedFlowOutFromFlowIn> _logger; 
        private double _timeConstFlowOutS;
        private double _timeDelayFlowOutS;
        private double _timeDelayMinS;
        private double _timeDelayMaxS;
        private double _vol0FlowOutPumpStartM3;
        private bool _isCalibratedExpFlowOut;
        private bool _doesBufferContainShutdownOverThresholdMinutes;
        private readonly TimeAlign _timeAlign;
        private Flowout _flowOutParameters; //Tuning params
        private LowPass? _lowPassFlow;
        private double? _stepTimeOldD;
        private double _dtAvgD;
        private double _samplesInBufferBelowShutdownThreshold;
        //private double _samplesInBufferAboveFullflowThreshold;
        private double _samplesSinceLastCalibration;
        private double _samplesSinceLastFullFlow;
        private RingBuffer? _isDownlinkingBuffer;
        private RingBuffer? _flowInBuffer;
        private RingBuffer? _flowOutBuffer;
        private double _timePrevD;
        // BUFFER_SIZE_SAMPLES* dt_s must be longer than it takes for flowOut to settle after pump shutdown
        private const int BufferSizeSamples = 100;

        public ExpectedFlowOutFromFlowIn(ILogger<ExpectedFlowOutFromFlowIn> logger, TimeAlign timeAlign)
        {
            _logger = logger;
            _timeAlign = timeAlign;
        }

        public bool IsCalibratingFlowInFlowOut { get; private set; }

        public double FlowOutExpectedLpm { get; private set; }

        public void Initalize(Flowout parameters)
        {
            _flowOutParameters = parameters;
            _vol0FlowOutPumpStartM3 = parameters.Vol0FlowOutPumpStartThresholdM3;
            _timeConstFlowOutS = 0;
            _timeDelayFlowOutS = 0;
            FlowOutExpectedLpm = 0;
            _timePrevD = 0;
            IsCalibratingFlowInFlowOut = false;
            _isCalibratedExpFlowOut = false;
            _lowPassFlow = new LowPass(1, _timeConstFlowOutS);
            _flowInBuffer = new RingBuffer(BufferSizeSamples);
            _flowOutBuffer = new RingBuffer(BufferSizeSamples);
            _isDownlinkingBuffer = new RingBuffer(BufferSizeSamples);
            _timeAlign.Initialize(parameters);
            _samplesSinceLastCalibration = double.MaxValue;
            _samplesSinceLastFullFlow = double.MaxValue;
            _samplesInBufferBelowShutdownThreshold = 0;
            //_samplesInBufferAboveFullflowThreshold = 0;
            _timeDelayMinS = MissingValueReplacement;
            _timeDelayMaxS = MissingValueReplacement;
        }


        public void SoftReset()
        {
            _vol0FlowOutPumpStartM3 = _flowOutParameters.Vol0FlowOutPumpStartThresholdM3;
            _timeConstFlowOutS = 0;
            _timeDelayFlowOutS = 0;
            FlowOutExpectedLpm = 0;
            _timePrevD = 0;
            IsCalibratingFlowInFlowOut = false;
            _isCalibratedExpFlowOut = false;
            _samplesSinceLastCalibration = double.MaxValue;
            _samplesSinceLastFullFlow = double.MaxValue;
            _samplesInBufferBelowShutdownThreshold = 0;
            //_samplesInBufferAboveFullflowThreshold = 0;
            _timeDelayMinS = MissingValueReplacement;
            _timeDelayMaxS = MissingValueReplacement;
        }

        // time-delay is found by direct search.
        public void IdentifyTimeConstantAndTimeDelayBasedOnBuffer()
        {
            // part one: set Tc to zero and find time - delay
            var deringbufferAndResampleFlowInBuffer = _flowInBuffer!.DeringbufferAndResample();
            var t1DRaw = deringbufferAndResampleFlowInBuffer.TbufferDStraightResampled;
            var flowInLpmRaw = deringbufferAndResampleFlowInBuffer.VbufferStraightResampled;
            var deringBufferAndResampleResultFlowOutBuffer = _flowOutBuffer!.DeringbufferAndResample();
            var flowOutLpmRaw = deringBufferAndResampleResultFlowOutBuffer.VbufferStraightResampled;
            _dtAvgD = deringBufferAndResampleResultFlowOutBuffer.DtAvg;
            var deringbufferAndResampleIsDownlinkingBuffer = _isDownlinkingBuffer!.DeringbufferAndResample();
            var isDownlinkingVec = deringbufferAndResampleIsDownlinkingBuffer.VbufferStraightResampled;
            var ind = isDownlinkingVec.Select(x => x.Equals(0)).ToArray();

            // if no data is valid, return without doing anything.
            if (!ind.Any(x => x))
                return;
            var flowInLpm = flowInLpmRaw.SubsetAsEnumerable(ind);
            var flowOutLpm = flowOutLpmRaw.SubsetAsEnumerable(ind);
            var t1D = t1DRaw.Subset(ind);

            // if T1_d is a scalar, then
            // calcSumOfSquaredDifferenceBtwBufferedFlowInAndFlowOut will
            // error.This is a quick fix.
            if (t1D.Length < 2)
            {
                _logger.LogDebug("ExpFlowOutFromFlowIn: t1D is a scalar. Length: {0}", t1D.Length);
                return;
            }


            var (tdSamples, timeConstS, _) = _timeAlign.Align(flowInLpm.ToArray(), flowOutLpm.ToArray(), t1D);
            if (tdSamples >= BufferSizeSamples)
            {
                _logger.LogDebug("ExpFlowOutFromFlowIn: Timealign detected too many samples: {0}", tdSamples);
                return;
            }

            _timeDelayFlowOutS = _dtAvgD.OADateToSeconds() * tdSamples;
            _timeDelayMinS = _dtAvgD.OADateToSeconds() * (tdSamples - 0.99);
            _timeDelayMaxS = _dtAvgD.OADateToSeconds() * (tdSamples + 0.99);

            _timeConstFlowOutS = timeConstS;
            _lowPassFlow = new LowPass(1, _timeConstFlowOutS);
        }

        public bool ShouldWeDetermineTimeDelayNow()
        {
            bool answer;
            if (_flowInBuffer!.GetLastValue() < _flowOutParameters.FlowInShutdownThresholdLpm)
                _samplesInBufferBelowShutdownThreshold += 1;
            else
                _samplesInBufferBelowShutdownThreshold = 0;

            if (_flowInBuffer.GetLastValue() > _flowOutParameters.FlowInFullflowThresholdLpm)
                _samplesSinceLastFullFlow = 0;
            else
                _samplesSinceLastFullFlow += 1;

            var avgBufferSampleTimeS = _flowInBuffer.GetAvgBufferSampleTime();
            if (avgBufferSampleTimeS.Equals(MissingValueReplacement))
                _doesBufferContainShutdownOverThresholdMinutes = false;
            else
                _doesBufferContainShutdownOverThresholdMinutes = _samplesInBufferBelowShutdownThreshold * avgBufferSampleTimeS > _flowOutParameters.FlowInShutDownDurationThresholdToCalibrateFlowS; // this assumes that the shutdown length in time

            var haveNotRecentlyDoneAcalibration = _samplesSinceLastCalibration > BufferSizeSamples;
            var fullFlowSeenRecently = _samplesSinceLastFullFlow < BufferSizeSamples;
            // 14th April 2020: _samplesSinceLastFullFlow has to be larger than _samplesInBufferBelowShutdownThreshold, AND comparable. Not one in samples, and the other in seconds. 
            //_logger.LogDebug("ExpFlowOut: LastCalib: {0}, LastFullFlow: {1}, BelowShutdown(sec): {2}, ContainShutDown: {3}",
            //_samplesSinceLastCalibration, _samplesSinceLastFullFlow, _samplesInBufferBelowShutdownThreshold * avgBufferSampleTimeS, _doesBufferContainShutdownOverThresholdMinutes);

            if (_doesBufferContainShutdownOverThresholdMinutes && haveNotRecentlyDoneAcalibration && fullFlowSeenRecently)
            {
                answer = true;
                _samplesSinceLastCalibration = 0;
            }
            else
            {
                _samplesSinceLastCalibration += 1;
                answer = false;
            }
            IsCalibratingFlowInFlowOut = answer;
            return answer;
        }

        public FlowInOutOutput Step(double flowInLpm, double flowOutLpm, bool isDownlinking, AlarmState _, double timeNewD)
        {
            FlowInOutOutput flowInOutOutput;
            if (flowInLpm == MissingValueReplacement)
            {
                flowInOutOutput.IsCalibratingFlowInFlowOut = IsCalibratingFlowInFlowOut;
                flowInOutOutput.IsCalibratedExpFlowOut = _isCalibratedExpFlowOut;
                flowInOutOutput.FlowOutExpectedLpm = FlowOutExpectedLpm;
                flowInOutOutput.TimeDelayFlowOutS = _timeDelayFlowOutS;
                flowInOutOutput.TimeConstFlowOutS = _timeConstFlowOutS;
                flowInOutOutput.Vol0FlowOutPumpStartM3 = _vol0FlowOutPumpStartM3;
                return flowInOutOutput;
            }
            var dtS = timeNewD.DiffInSeconds(_timePrevD);
            _timePrevD = timeNewD;

            if (_stepTimeOldD == null)
                _stepTimeOldD = timeNewD;

            if (!flowInLpm.Equals(MissingValueReplacement) && !flowOutLpm.Equals(MissingValueReplacement))
            {
                _flowInBuffer!.AddDataToCircularBuffer(timeNewD, flowInLpm);
                _flowOutBuffer!.AddDataToCircularBuffer(timeNewD, flowOutLpm);
                _isDownlinkingBuffer!.AddDataToCircularBuffer(timeNewD, isDownlinking.ToInt());
            }

            // if there is a shutdown in the centre of the buffered data,
            // then it do an estimation of the time delay
            if (flowOutLpm.Equals(MissingValueReplacement)) // missing flow out, for instance on StenaDon
                FlowOutExpectedLpm = flowInLpm;
            else
            {
                if (ShouldWeDetermineTimeDelayNow())
                    IdentifyTimeConstantAndTimeDelayBasedOnBuffer();

                if (_stepTimeOldD == null)
                {
                    _stepTimeOldD = timeNewD;
                }

                var (flowInDelayedLpm, dtNewestDatapointS) = _flowInBuffer!.GetPastValueWithExplicitLastCallTime(_timeDelayFlowOutS, timeNewD, _stepTimeOldD.Value);
                if (flowInDelayedLpm.Equals(MissingValueReplacement))
                    FlowOutExpectedLpm = MissingValueReplacement;
                else
                {
                    // disp(dt_newestDatapoint_s);
                    if (_timeConstFlowOutS > 0)
                    {
                        if (dtNewestDatapointS > 0) // depending on the sampling of the signal(non-synchronous) there may not be any newer datapoint, 
                        {
                            _lowPassFlow!.FilterForCustomDt(flowInDelayedLpm, dtNewestDatapointS, 0);
                            FlowOutExpectedLpm = _lowPassFlow.FilteredSignal ?? 0;
                        }
                        else
                        {
                            if (_lowPassFlow!.FilteredSignal != null)
                                FlowOutExpectedLpm = _lowPassFlow.FilteredSignal.Value;
                            else
                                FlowOutExpectedLpm = flowInDelayedLpm;
                        }
                    }
                    else
                        FlowOutExpectedLpm = flowInDelayedLpm;
                }

                if (dtS > 30 || dtS < 0) // bad time step
                    _vol0FlowOutPumpStartM3 = 0;
                else
                {
                    if (flowInLpm < 50) // Pump off
                        _vol0FlowOutPumpStartM3 = 0;
                    else if (_vol0FlowOutPumpStartM3 < _flowOutParameters.Vol0FlowOutPumpStartThresholdM3) // Pump running
                        _vol0FlowOutPumpStartM3 += dtS * flowInLpm / 60000.0;
                }

                if (flowInLpm > 50 && // Pump running
                        flowOutLpm < 100 &&  // No / low measured flow out
                        _vol0FlowOutPumpStartM3 < _flowOutParameters.Vol0FlowOutPumpStartThresholdM3) // Accumulate void volume
                    FlowOutExpectedLpm = 0; // Set expected flow out to 0 until void volume is filled

                // 8thApril 2020: Removed dtNewestDatapoints > 0 as condition 
                //if (!_isCalibratedExpFlowOut && (_timeConstFlowOutS > 0 /*|| dtNewestDatapointS > 0*/)) 
                if (!_isCalibratedExpFlowOut && (_timeConstFlowOutS > 0 || dtNewestDatapointS > 0))
                    _isCalibratedExpFlowOut = true;
            }

            flowInOutOutput.IsCalibratingFlowInFlowOut = IsCalibratingFlowInFlowOut;
            flowInOutOutput.IsCalibratedExpFlowOut = _isCalibratedExpFlowOut;
            flowInOutOutput.FlowOutExpectedLpm = FlowOutExpectedLpm;
            flowInOutOutput.TimeDelayFlowOutS = _timeDelayFlowOutS;
            flowInOutOutput.TimeConstFlowOutS = _timeConstFlowOutS;
            flowInOutOutput.Vol0FlowOutPumpStartM3 = _vol0FlowOutPumpStartM3;
            _stepTimeOldD = timeNewD;
            return flowInOutOutput;
        }
    }
}


