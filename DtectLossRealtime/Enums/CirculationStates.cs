﻿namespace DtectLossRealtime.Enums
{
    public enum CirculationStates
    {
        NotCirculating = 0,
        Circulating = 1,
        Drilling = 2
    }
}
