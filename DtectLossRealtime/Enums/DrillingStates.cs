﻿namespace DtectLossRealtime.Enums
{
    public enum DrillingStates
    {
        // notCirculating (0)
        //   circulating (1)
        //   drilling (2)
        BadData = -1,
        Other = 0,
        Drilling = 1, // RotaryDrilling and Sliding, on bottom
        FlowCheck = 2, // Testing
        Conditioning = 3, // Circulate bottoms up etc
        Tripping = 4 // Washing/reaming/working pipe
        // Connection (60) %ToDo - from pulling off bottom to on bottom drilling
    }
}
