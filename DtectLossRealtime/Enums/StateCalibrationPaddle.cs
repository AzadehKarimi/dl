﻿namespace DtectLossRealtime.Enums
{
    public enum StateCalibrationPaddle
    {
        Normal = 0,
        GatherDataK = 1,
        GatherDataBias = 2,
    }
}
