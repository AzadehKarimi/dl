﻿namespace DtectLossRealtime.Enums
{
    public enum AlarmState
    {
        InputError= -2,
        Disabled = -1,
        Normal = 0,
        Warning = 1,
        Alarm = 2
    }
}
