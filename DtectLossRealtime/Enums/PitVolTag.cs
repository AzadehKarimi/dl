﻿namespace DtectLossRealtime.Enums
{
    public enum PitVolTag
    {
        Error = 0,
        volGainLoss = 1,
        NA = 2, // not available
    }
}
