﻿namespace DtectLossRealtime.Enums
{
    public enum FlowOutMeterType
    {
        Error = 0,
        FlowOutCoriolis = 1,                   // use the "FlowOutCoriolis" tag
        FlowOutPercent = 2,                    // use the FlowOutPercent tag
        FlowOutPercentIsAFraction = 3,         // the FlowOutPercent tag goes from 0-1(fraction) not 0-100(percent)
        FlowOut = 4,                           // use the "FlowOut" tag
        FlowOutTagIsFlowOutPercentValue = 5,   // It was beleived that the FlowOutPercent tag had been mis-mapped to FlowOut on Stenadon, turned out not to be the case
        FlowOutMissing = 6                     // no flow-out measurement, rely on other inputs for alarm (Stenadon)
    }
}
