﻿namespace DtectLossRealtime.Enums
{
    public enum SimulatorSolvers
    {
        ODE45 = 0,
        FORWARD_EULER = 1
    }
}
