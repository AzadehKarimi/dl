﻿namespace DtectLossRealtime.Enums
{
    /// <summary>
    /// %DRILLINGSTATES States which represents the basic drilling state flow
    /// </summary>
    public enum StreamingMonitorTypes
    {
        Undefined = 0,
        HistoricFromTsCollect = 1,
        LiveOverWitsml = 2
    }
}
