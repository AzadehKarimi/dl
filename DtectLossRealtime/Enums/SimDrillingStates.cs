﻿namespace DtectLossRealtime.Enums
{
    public enum SimDrillingStates
    {
    StartSimulation = 0,
    Drilling = 1,
    PullOffBottom = 2,
    StopRotation = 3,
    StopPumps = 4,
    Wait = 5,
    StartPumps = 6,
    StartRotation = 7,
    GoOnBottom = 8,
    Pull1Stand = 9,
    FlowCheck = 10,
    Run1Stand = 11,
    EndSimulation = 12,
    TripAddStand = 13,
    TripRemoveStand = 14,
    DrillAddStand = 15
    }
}
