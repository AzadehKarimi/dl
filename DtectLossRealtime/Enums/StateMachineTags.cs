﻿namespace DtectLossRealtime.Enums
{
    public enum StateMachineTags
    {
        NoFlow = 0,
        RampUp = 1,
        SteadyNonZeroFlow = 2,
        RampDown = 3
    }
}
