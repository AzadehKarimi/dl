﻿# Realtime DtectLoss calculation

Constructor and interface

```csharp

public DtectLossRealtime(
    ILogger<DtectLossRealtime> logger,
    IProcess process,
    EvaluateRealTime evaluateRealTime,
    StateMachine stateMachine,
    Describe describe,
    ExpectedFlowOutFromFlowIn flowOutFlowIn,
    ExpectedStandpipePres expectedStandpipePres,
    ExpectedTripTankVol expectedTripTankVol,
    FlowlineAndPit flowlineAndPit,
    GainDetector gainDetector,
    FlowBackRecorder flowBackRecorder,
    EkdAlgFactory algFactory
    )


public interface IDtectLossRealtime
{
    void Initialize(
     string algorithmName,
     Dictionary<string, string>
     rawDataUnits,
     double startTime,
     double areaDppoohM2,
     double areaDprihM2);

    DtectLossRealtimeOutput StepAllAlgorithms(
      Dictionary<string,
      DataVariable> rawDataNonstandardUnits);

    string CheckData();
}
```

- Note: Currently, Default units are read from a file and requires a file to be present in the application running this project.
