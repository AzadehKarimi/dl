﻿using System.Linq;
using Accord.Math;
using DtectLoss.Common;
using DtectLossRealtime.Calculations;
using DtectLossRealtime.Helpers;
using DtectLossRealtime.Models;
using static System.Math;
using static DtectLoss.Common.CommonConstants;
using static DtectLossRealtime.Helpers.MathFunctions;

namespace DtectLossRealtime
{
    public class GainDetector
    {
        public double VolGainM3 { get; private set; }
        public double FlowBasedOnDerivativeOfGainLossLpm { get; private set; }
        public double TimeStepMaxS { get; } = 60;
        public int NSamples { get; } = 600;
        public RingBuffer? RingBufferVolGainLoss { get;private set; }

        private RingBuffer? _ringBufferFlowIn;
        private double _volGainLossPrevM3;
        private double _volGainLossOffsetM3;
        private readonly LinReg _linReg;
        private double? _prevTimeD;
        private bool _isValid;
        private double _dt;
        private double _timeD;
        private const double VolumeLimitM3 = 1;
        private const double FlowRateLimLpm = 1000;
        private const double VolGainNegativeFlowRateResetLpm = -10;
        private const double FlowInThresholdLpm = 10;
        private const double FlowInRangeThresholdLpm = 100;
        private readonly double _windowForLeastSquaresD = 600.0.SecondsToOADate();
        private readonly double _windowForFlowRangeD = 600.0.SecondsToOADate();
        private const double FractionOfWindowNecessaryForLeastSquares = 0.5;
        private const double BitOfBottomThresholdM = 1.5;
        private const double VolGainLossThresholdM3PerTimeStep = 3;
        private const double LpmToM3PerS = 1.0 / 60e3;


        public GainDetector(LinReg linReg)
        {
            _linReg = linReg;
        }

        public void Initialize()
        {
            VolGainM3 = 0;
            FlowBasedOnDerivativeOfGainLossLpm = 0;
            RingBufferVolGainLoss = new RingBuffer(NSamples);
            _ringBufferFlowIn = new RingBuffer(NSamples);
            _volGainLossPrevM3 = 0;
            _volGainLossOffsetM3 = 0;
        }

        public void SoftReset()
        {
            VolGainM3 = 0;
            FlowBasedOnDerivativeOfGainLossLpm = 0;
            _volGainLossPrevM3 = 0;
            _volGainLossOffsetM3 = 0;
        }

        public void ResetAfterAlarm()
        {
            VolGainM3 = 0;
            Initialize();
        }

        public void Step(double timeD, double volGainLossM3, double depthBitM, double depthHoleM, double flowInLpm)
        {
            DifferentiateVolume(timeD, volGainLossM3, depthBitM, depthHoleM, flowInLpm);

            if ((Abs(depthHoleM - depthBitM) > BitOfBottomThresholdM) 
                || depthBitM.Equals(MissingValueReplacement) && depthHoleM.Equals(MissingValueReplacement) 
                || !CheckIfMeasIsValid(FlowBasedOnDerivativeOfGainLossLpm))
                return;
            IntegrateVolume(timeD, FlowBasedOnDerivativeOfGainLossLpm);
        }

        public void DifferentiateVolume(double timeD, double volGainLossM3, double depthBitM, double depthHoleM, double flowInLpm)
        {
            _timeD = timeD;
            FlowBasedOnDerivativeOfGainLossLpm = MissingValueReplacement;

            if (!(CheckIfMeasIsValid(volGainLossM3) 
                  && CheckIfMeasIsValid(depthBitM) 
                  && CheckIfMeasIsValid(depthHoleM) 
                  && CheckIfMeasIsValid(flowInLpm)))
                return;

            _ringBufferFlowIn!.AddDataToCircularBuffer(timeD, flowInLpm);
            var flowInRangeLpm = _ringBufferFlowIn.GetValueRange(timeD - _windowForFlowRangeD);

            if (Abs(depthHoleM - depthBitM) > BitOfBottomThresholdM 
                || (depthBitM.Equals(MissingValueReplacement) && depthHoleM.Equals(MissingValueReplacement)) 
                || flowInLpm < FlowInThresholdLpm 
                || flowInRangeLpm > FlowInRangeThresholdLpm)
                return;
            double volGainLossWithoutOffsetM3 = 0;

            if (CheckIfMeasIsValid(volGainLossM3))
            {
                (volGainLossWithoutOffsetM3, _volGainLossOffsetM3 ) = RemoveOffset(new []{_volGainLossPrevM3, volGainLossM3}, _volGainLossOffsetM3, VolGainLossThresholdM3PerTimeStep, flowInLpm);
               _volGainLossPrevM3 = volGainLossM3;
            }

            _prevTimeD = RingBufferVolGainLoss!.GetLastTime();
            RingBufferVolGainLoss.AddDataToCircularBuffer(timeD, volGainLossWithoutOffsetM3);

            var flowRateM3PerS = LeastSquaresSlopeOfGainLossVolume();
            FlowBasedOnDerivativeOfGainLossLpm = FlowRateLimiterAndConverter(flowRateM3PerS);
        }

        private double LeastSquaresSlopeOfGainLossVolume()
        {
            var flowRateM3PerS = 0.0;
            var timeStartOfWindowD = _timeD - _windowForLeastSquaresD;
            var (timeWindow, values) = RingBufferVolGainLoss!.GetWindowOfLastValues(timeStartOfWindowD);
            var tDiff = timeWindow.Select(x => x - timeWindow[0]).ToArray().Diff();
            var tTotalValidTimeStepsD = tDiff.Where(x => x < TimeStepMaxS).Sum();
            if (tTotalValidTimeStepsD >= _windowForLeastSquaresD * FractionOfWindowNecessaryForLeastSquares)
            {
                var tInSecWithStartTimeZeroTmp = timeWindow.Select(x => x.DiffInSeconds(timeWindow[0])).ToArray().ToMatrix();
                _linReg.FitData(tInSecWithStartTimeZeroTmp.Transpose(), values);
                if (_linReg.Coeffs.Any())
                    flowRateM3PerS = _linReg.Coeffs[0];
            }
            return flowRateM3PerS;
        }
        private static double FlowRateLimiterAndConverter(double flowRateM3PerS)
        {
           return Max(Min(FlowRateLimLpm, flowRateM3PerS / LpmToM3PerS), -FlowRateLimLpm);
        }
        public void IntegrateVolume(double timeD, double flowRateLpm)
        {
            if (_prevTimeD == null)
                return;
            if (_prevTimeD.Value.Equals(MissingValueReplacement))
                return;
            if (flowRateLpm >= VolGainNegativeFlowRateResetLpm && !timeD.Equals(MissingValueReplacement))
            {
                _dt = timeD.DiffInSeconds(_prevTimeD.Value);
                if (_dt > TimeStepMaxS)
                    VolGainM3 = 0;
                else
                    VolGainM3 = Max(Min(VolGainM3 + _dt * flowRateLpm * LpmToM3PerS, VolumeLimitM3), 0);
            }
            else if (flowRateLpm < VolGainNegativeFlowRateResetLpm)
                VolGainM3 = 0;
        }

        public bool CheckIfMeasIsValid(double meas)
        {
            _isValid = !meas.Equals(MissingValueReplacement);
            return _isValid;
        }

        public GainDetectorOutput GenerateOutput()
        {
            var result = new GainDetectorOutput()
            {
                VolGainDetectorM3 = VolGainM3,
                FlowDerGlAvgLpm = FlowBasedOnDerivativeOfGainLossLpm
            };
            return result;
        }
    }
}
