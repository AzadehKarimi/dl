﻿using System;
using System.Linq;
using DtectLoss.Common;
using DtectLoss.Common.Enums;
using DtectLossRealtime.Enums;
using DtectLossRealtime.Helpers;
using DtectLossRealtime.Models;
using static System.Math;

namespace DtectLossRealtime
{
    /// <summary>
    /// EVALUATE_REALTIME_OBJ Count alarms on the fly
    /// Detailed explanation goes here
    /// </summary>
    public class EvaluateRealTime
    {
        private readonly RingBuffer _alarmTypeRingBuffer;
        private double _nTotalAlarms;
        private double _upTimeInPercent;
        private bool _isInitialized;
        private double _upTimeD; // time in days which the algorithm has been active
        private double _downTimeD; // time in days with AlarmState< 0
        private double _totalTimeD;
        private double _nAlarmsPer12Hours; // average computed for the entire time-span
        private int _nAlarmsLast12Hours; // number of alarms during the last 12 hours
        private AlarmState _prevAlarmState;
        private int[] _alarmTypeCount = {};
        private int _alarmTypeIndexOffset;
        //private bool[] _alarmMask;
        private const double MaxNumberOfAlarms = 99;
        private const int NRingBuffer = 99;

        public EvaluateRealTime()
        {
            _alarmTypeRingBuffer = new RingBuffer(NRingBuffer);
            _isInitialized = false;
            _upTimeD = 0;
            _totalTimeD = 0;
            _nTotalAlarms = 0;
            _nAlarmsLast12Hours = 0;
            GenerateAlarmParams();
        }

        public void GenerateAlarmParams()
        {
            var s = Enum.GetNames(typeof(AlarmType));
            _alarmTypeIndexOffset = s.Length / 2;
            _alarmTypeCount = new int[s.Length];
            //_alarmMask = s.Select(x => x.Contains("Alarm")).ToArray();
        }

        public void Initialize(AlarmState alarmState)
        {
            _prevAlarmState = alarmState;
            _isInitialized = true;
        }

        public void SoftReset()
        {
            _isInitialized = false;
            _upTimeD = 0;
            _downTimeD = 0;
            _totalTimeD = 0;
            _nTotalAlarms = 0;
            _nAlarmsPer12Hours = 0;
            _nAlarmsLast12Hours = 0;
            GenerateAlarmParams();
        }

        public void UpdateTimeCount(AlarmState alarmState, double timeStepS)
        {
            if (alarmState < 0)
                _downTimeD += timeStepS.SecondsToOADate();
            else
                _upTimeD += timeStepS.SecondsToOADate();

            _totalTimeD += timeStepS.SecondsToOADate();
        }

        public void AddAlarmIfNotPitSlowGainAlarm(double currentTimeD, AlarmType alarmType, AlarmState alarmState)
        {
            if (CheckIfAlarmIsValid(alarmState, alarmType))
            {
                _nTotalAlarms += 1;
                _alarmTypeRingBuffer.AddDataToCircularBuffer(currentTimeD, (int)alarmType);
                UpdateAlarmCount();
            }
        }

        public bool CheckIfAlarmIsValid(AlarmState alarmState, AlarmType alarmType)
        {
            return alarmState == AlarmState.Alarm && _prevAlarmState != AlarmState.Alarm && alarmType != AlarmType.PitSlowGainAlarm;
        }

        public void CalculateNumberOfAlarmsPer12Hours()
        {
            if (_upTimeD > 0)
                _nAlarmsPer12Hours = Min(MaxNumberOfAlarms, _nTotalAlarms / _upTimeD / 2.0);
        }

        public void UpdateAlarmCount()
        {
            var alarmTypeIndex = +_alarmTypeIndexOffset;
            _alarmTypeCount[alarmTypeIndex] = _alarmTypeCount[alarmTypeIndex] + 1;
        }

        public void CountNAlarmsLast12Hours(double currentTimeD)
        {
            var timeD = _alarmTypeRingBuffer.TbufferD;
            _nAlarmsLast12Hours = timeD
                .Select(x => x - currentTimeD + 0.5 > 0 && x > 0)
                .Count(x => x);
        }

        public EvaluateRealTimeOutput GenerateOutput()
        {
            if (_totalTimeD > 0)
                _upTimeInPercent = _upTimeD / _totalTimeD * 100;
            else
                _upTimeInPercent = 0;

            var outputStruct = new EvaluateRealTimeOutput()
            {
                NumberOfAlarms = _nTotalAlarms,
                AlarmsLast12Hours = _nAlarmsLast12Hours,
                AlarmsPer12Hours = _nAlarmsLast12Hours,
                UpTimeH = _upTimeD * 24,
                TotalTimeD = _totalTimeD,
                UpTimePercent = _upTimeInPercent,
                TripOnlyLossAlarm = _alarmTypeCount[0],
                PitOnlyLossAlarm = _alarmTypeCount[1],
                FlowOnlyLossAlarm = _alarmTypeCount[2],
                FlowAndPressureLossAlarm = _alarmTypeCount[3],
                FlowAndPitLossAlarm = _alarmTypeCount[4],
                PitAndPressureLossAlarm = _alarmTypeCount[5],
                PitAndPressureInfluxAlarm = _alarmTypeCount[9],
                FlowAndPitInfluxAlarm = _alarmTypeCount[10],
                FlowAndPressureInfluxAlarm = _alarmTypeCount[11],
                FlowOnlyInfluxAlarm = _alarmTypeCount[12],
                PitOnlyInfluxAlarm = _alarmTypeCount[13],
                TripOnlyInfluxAlarm = _alarmTypeCount[14],
                PitSlowGainAlarm = _alarmTypeCount[15]
            };
            return outputStruct;
        }

        public EvaluateRealTimeOutput Step(double currentTimeD, double timeStepS, AlarmState alarmState, AlarmType alarmType)
        {
            UpdateTimeCount(alarmState, timeStepS);
            if (_isInitialized)
            {
                AddAlarmIfNotPitSlowGainAlarm(currentTimeD, alarmType, alarmState);
                CountNAlarmsLast12Hours(currentTimeD);
                CalculateNumberOfAlarmsPer12Hours();
                _prevAlarmState = alarmState;
            }
            else
                Initialize(alarmState);

            return GenerateOutput();
        }
    }
}
