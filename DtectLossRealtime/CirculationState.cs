﻿using System;
using System.Collections.Generic;
using System.Text;
using DtectLossRealtime.Enums;

namespace DtectLossRealtime
{
    /// <summary>
    /// CIRCULATION_STATES_OBJ States which the circulation system can be in, e.g.
    /// drilling, circulating, not circulating.
    /// The available states are defined as enums in CirculationStates class
    /// </summary>
    public class CirculationState
    {
        private const double QThreshold = 50; // [lpm] threshold for flow-rate from rig pump.If the flow rate from the rig pumps are lower than this value -> not circulating
        private const double DepthThreshold = 0.5; // [m] threshold for when bit is on bottom.If the bit close than this value to the holeDepth -> drilling
        public CirculationStates CurrentState { get; private set; }
        public CirculationStates PreviousState { get; private set; }

        public CirculationState()
        {
            CurrentState = CirculationStates.NotCirculating;
            PreviousState = CirculationStates.Circulating;
        }

        public void ChangeState(CirculationStates toState)
        {
            PreviousState = CurrentState;
            CurrentState = toState;
        }

        public void StateMachine(double qpLpm, double bitDepthM, double holeDepthM)
        {
            if (qpLpm < 0 || bitDepthM < 0 || holeDepthM < 0)
                return;

            if (qpLpm <= QThreshold)
                ChangeState(CirculationStates.NotCirculating); // from any state, go back to notCirculating if not flowing
            else if (qpLpm > QThreshold && CurrentState == CirculationStates.NotCirculating)
                ChangeState(CirculationStates.Circulating); // from notCirculating to circulating
            else if (bitDepthM > holeDepthM - DepthThreshold && CurrentState == CirculationStates.Circulating)
                ChangeState(CirculationStates.Drilling); // from circulating to drilling
            else if (bitDepthM <= holeDepthM - DepthThreshold && CurrentState == CirculationStates.Drilling)
                ChangeState(CirculationStates.Circulating); // from drilling back to circulating
        }
    }
}
