﻿namespace DtectLossRealtime.Models
{
    public struct Voting
    {
        public double PitThresholdM3;
        public double FlowDeadbandLpm;
        public double VolumeThresholdWarningM3;
        public double VolumeThresholdAlarmM3;
        public double FlowInLowpassTcS;
        public double VolumeThresholdTrippingAlarmM3;
        public double TimeResetAlarmS;
        public double TimeInhibitAlarmAfterResetS;
        public double TimeConstInjViopS;
        public double TimeConstInjViofS;
        public double TimeConstInjFioS;
        public double TimeWarningS;
        public double TimeFlowOnlyAlarmS;
        public double TimeVolOnlyAlarmS;
        public double FlowIoAlarmLpm;
        public double FlowIoAlarmPc;
        public double VolIoAlarmM3;
        public double VolIoAlarmPumpStartM3;
        public double TimeFiltPitVolS;
        public double FlowMaxInjectionLpm;
        public double ScalePresToFlow;
        public double TimeWaitRecalibratePaddleS;
    }

    public struct Paddle
    {
        public double TlpS;
        public double FlowInMinToTuneKLpm;
        public double FlowderThresholdLpmpsec;
        public double TfiltFlowDerS;
        public double TimeWaitKCalibrationS;
        public double TimeWaitBiasCalibrationS;
        public double TimeTuningS;
        public double NumberPumpStopsBiasCalibration;
        public bool UseFlowInFilt;
    }

    public struct Flowout
    {
        public double FlowInShutdownThresholdLpm;
        public double FlowInFullflowThresholdLpm;
        public double FlowInShutDownDurationThresholdToCalibrateFlowS;
        public double Vol0FlowOutPumpStartThresholdM3;
        public int NVec;
    }
    public struct Triptank
    {
        public int NVec;
    }

    public struct PresSp
    {
        public double FlowInShutdownThresholdLpm;
        public double FlowInFullflowThresholdLpm;
        public double FlowInShutDownDurationThresholdToCalibrateFlowS;
        public double FlowInChangeToTriggerHighRateRecalLpm;
        public double NumberOfSamplesToWaitAfterFlowChangeToRecal;
        public bool Usefminsearch;
        public double FlowPivotLpm;
        public double FactorForget;
        public double PresDeadbandCalibrateBar;
        public double TimeDelayMaxS;
    }

    public struct PitVol
    {
        public double KInjGainLossHz;
        public double UpperInjLimitLpm;
        public double LowerInjLimitLpm;
        public double PitDerivativeTimeConstantS;
        public double MaxTimeDelayS;
        public double Damping;
        public int NTd;
        public double FlowstdRKfLpm;
        public double FlowstdP0KfLpm;
    }

    public static class ParameterValues
    {
        public static Voting VotingParam;
        public static Paddle PaddleParam;
        public static Flowout FlowoutParams;
        public static PresSp PresspParams;
        public static PitVol PitvolParams;
        public static Triptank TripTankParams;

        static ParameterValues()
        {
            VotingParam.PitThresholdM3 = 0;
            VotingParam.FlowDeadbandLpm = 100; //50
            VotingParam.VolumeThresholdWarningM3 = 0.5; //0.8
            VotingParam.VolumeThresholdAlarmM3 = 1;
            VotingParam.FlowInLowpassTcS = 5; //5
            VotingParam.VolumeThresholdTrippingAlarmM3 = 0.7;
            VotingParam.TimeResetAlarmS = 300; //Reset volume and alarm automatically after 5 minutes
            VotingParam.TimeInhibitAlarmAfterResetS = 300; //Inhibit alarm 5 minutes after reset
            VotingParam.TimeConstInjViopS = 200;
            VotingParam.TimeConstInjViofS = 400; //200
            VotingParam.TimeConstInjFioS = 600; //400
            VotingParam.TimeWarningS = 120; //desired time from warning to alarm
            VotingParam.TimeFlowOnlyAlarmS = 30;
            VotingParam.TimeVolOnlyAlarmS = 30;
            VotingParam.FlowIoAlarmLpm = 800; //1000
            VotingParam.FlowIoAlarmPc = 40; //75
            VotingParam.VolIoAlarmM3 = 2;
            VotingParam.VolIoAlarmPumpStartM3 = 6;
            VotingParam.TimeFiltPitVolS = 30;
            VotingParam.FlowMaxInjectionLpm = 20; //50
            VotingParam.ScalePresToFlow = 50; //Temporary scaling error of 1 bar in pressure to 50 lpm in flow (loss/influx)
            VotingParam.TimeWaitRecalibratePaddleS = 30; //20 - desired waiting time before calibration in case of real gain/loss

            PaddleParam.TlpS = 30; // lowpass Filter time constant in seconds
            PaddleParam.FlowInMinToTuneKLpm = 900;// minimum flow before starting to tune K.
            PaddleParam.FlowderThresholdLpmpsec = 10; // [lpm / sec] if the flow rate from the rig pumps change slower than this value->constant / zero flow
            PaddleParam.TfiltFlowDerS = 20; // Filter time constant for estimating time derivative of flow in from flow in
            PaddleParam.TimeWaitKCalibrationS = 120; // [sec] 60 Waiting time after pumps are back on with constant flow in before calibration shall be initiated
            PaddleParam.TimeWaitBiasCalibrationS = 60; // [sec] Waiting time after pumps are back on with constant flow in before calibration shall be initiated
            PaddleParam.TimeTuningS = 60; // 60 for how long should K be updated after a startup ?
            PaddleParam.NumberPumpStopsBiasCalibration = 3;
            PaddleParam.UseFlowInFilt = true; // false

            FlowoutParams.FlowInShutdownThresholdLpm = 300;  // make these slack
            FlowoutParams.FlowInFullflowThresholdLpm = 1000; // make these slack
            FlowoutParams.FlowInShutDownDurationThresholdToCalibrateFlowS = 120;
            FlowoutParams.Vol0FlowOutPumpStartThresholdM3 = 0.3; // 0.4 After pump start, fill this volume before flowout > 0.
            FlowoutParams.NVec = 20; // To be optimized

            PresspParams.FlowInShutdownThresholdLpm = 300;
            PresspParams.FlowInFullflowThresholdLpm = 1200;
            PresspParams.FlowInShutDownDurationThresholdToCalibrateFlowS = 120;
            PresspParams.FlowInChangeToTriggerHighRateRecalLpm = 99;
            PresspParams.NumberOfSamplesToWaitAfterFlowChangeToRecal = 50;
            PresspParams.Usefminsearch = false; // false; both true and false are ok
            PresspParams.FlowPivotLpm = 1500;
            PresspParams.FactorForget = 1 / 2.0; // average of last 2 time delay estimates(1 / 5)
            PresspParams.PresDeadbandCalibrateBar = 1;  // Train if offset is more than 1 bar
            PresspParams.TimeDelayMaxS = 10;

            PitvolParams.KInjGainLossHz = 1 / 400.0; // 1 / 50[1 / s]
            PitvolParams.UpperInjLimitLpm = 300; // [lpm] upper limit of injection term
            PitvolParams.LowerInjLimitLpm = -300; // [lpm] lowerlimit of injection term
            PitvolParams.PitDerivativeTimeConstantS = 900; // [s] time constant for the first order Filter for finding derivative of  pit volume
            PitvolParams.MaxTimeDelayS = 180; // [s] max allowed time delay between flowline and pit
            PitvolParams.Damping = 1; // [-] damping for the gain/ loss observer.Used to calculate tuning.
            PitvolParams.NTd = 8; // 4 number of time - delays which should be used to calculate mean of time - delay - tuning parameter
            PitvolParams.FlowstdRKfLpm = 50;
            PitvolParams.FlowstdP0KfLpm = 50;
        }
    }
}
