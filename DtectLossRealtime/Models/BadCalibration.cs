﻿namespace DtectLossRealtime.Models
{
    public struct BadCalibration
    {
        public bool Vol;
        public bool Flow;
        public bool Pres;
        public bool Tot;
    }
}