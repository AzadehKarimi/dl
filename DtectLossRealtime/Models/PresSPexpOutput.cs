﻿using DtectLossRealtime.Enums;

namespace DtectLossRealtime.Models
{
    public struct PresSPexpOutput
    {
        public double PresSpExpectedBar;
        public double PresSpExpWithoutInjectionBar;
        public double PressSpInjectionBar;
        public double PresSpErrBar;
        public bool IsCalibratedExpPresSp;
        public bool IsCalibratingExpPresSp;
        public bool IsSteadyPresSp;
        public bool IsSteadyandNonZeroPresSp;
        public bool IsSteadyFlowIn;
        public StateMachineTags StateMachine;
        public double PresSpDerBarpsec;
    }
}