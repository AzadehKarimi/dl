﻿using System.Collections.Generic;
using DtectLossRealtime.Enums;

namespace DtectLossRealtime.Models
{
    public class WashedData
    {
        public WashedData()
        {
            Values = new Dictionary<string, double>();
        }
        public Dictionary<string, double> Values;
        public bool SelectpresSpBoolean;
        public bool SelectflowoutBoolean;
        public bool IsDownlinking;
        public bool IsCalibratingPaddleBias;
        public bool IsCalibratingPaddleK;
        public bool IsCalibratedMeasFlowOut;
        public double BiasPaddlePrc;
        public double KPaddleLpmPerPrc;
        public bool IsRampingUp;
        public double TimePumpsOnS;
        public double TimePumpsOffS;
        public double TimeCalibratingS;
        public int NumberPumpStops;
        public StateCalibrationPaddle StateCalibrationPaddle;


    }
}
