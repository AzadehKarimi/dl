﻿namespace DtectLossRealtime.Models
{
    public struct FlowInOutOutput
    {
        public bool IsCalibratingFlowInFlowOut;
        public double TimeConstFlowOutS;
        public double TimeDelayFlowOutS;
        public double FlowOutExpectedLpm;
        public double Vol0FlowOutPumpStartM3;
        public bool IsCalibratedExpFlowOut;
    }
}