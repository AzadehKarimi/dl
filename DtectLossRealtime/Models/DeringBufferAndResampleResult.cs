﻿namespace DtectLossRealtime.Models
{
    public class DeringBufferAndResampleResult
    {
        public DeringBufferAndResampleResult(double[] tbufferDStraightResampled, double[] vbufferStraightResampled, double dtAvg)
        {
            TbufferDStraightResampled = tbufferDStraightResampled;
            VbufferStraightResampled = vbufferStraightResampled;
            DtAvg = dtAvg;
        }

        public double[] TbufferDStraightResampled { get; }
        public double[] VbufferStraightResampled { get; }
        public double DtAvg { get; }
    }
}