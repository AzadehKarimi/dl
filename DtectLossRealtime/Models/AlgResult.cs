﻿using DtectLoss.Common.Enums;
using DtectLossRealtime.Enums;

namespace DtectLossRealtime.Models
{
    public class AlgResult
    {
        public double VolPitM3;
        public double VolIoM3;
        public AlarmState AlarmState;
        public AlarmType AlarmType;
        public double PresSpBar;
        public double BetaperVolBarm3;

        public double VolFlM3;
        public double KpitLpmpm3;
        public double KshakerLpmpm3;
        public double SpaddleLpmpc;
        public double FlowPitLpm;
        public double FlowShakerLpm;

        public double FlowOutxLpm;
        public double MinvBarsplpm;
        public double CfracBarplpm;
        public double PresFricBar;

        public double VolTtM3;
        public double FlowTtLpm;
        public double AreaDpM2;
        public double ValveTt01;
        public double FlowSteelLpm;
        public double VelBlockMps;
        public double FlowIoFromPresLpm;
        public double FlowIoFromRatesWBiasLpm;
        public double BetaperVolEBarm3;
        public double FlowBiasLpm;
        public bool IsOnBounds;
        public double FlowOutExpectedLpm;

        public double FlowIOflowLpm;
        public double FlowIOpresLpm;
        public double FlowIOpitLpm;
        public double FlowIOtripLpm;
        public double PresIoBar;
        public double VolIOflowM3;
        public double VolIOpresM3;
        public double VolIOpitM3;
        public double VolIOtripM3;
        public double FlowIOinjLpm;
        public bool RecalibrateFlowOutNow;
        public double TimeSinceLastAlarmS;
        public double TimeSinceValidS;
        public double TimeAlarmActiveS;
        public double TimeHighFlowErrorS;
        public double TimeHighVolErrorS;
    }
}
