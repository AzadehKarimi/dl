﻿using DtectLossRealtime.Enums;

namespace DtectLossRealtime.Models
{
    public struct RealtimeOutputStatus
    {
        public bool IsCalibrating;
        public DtectLossRealtimeErrorCodes ErrorCode;
        public DtectLossRealtimeErrorCodes WarningCode;
        public DrillingStates SysState;
        public double VolGainLossExpectedM3;
        public double VolGainLossPredModelledM3;
        public double VolGainLossPredErrorM3;
    }
}
