﻿namespace DtectLossRealtime.Models
{
    public struct EvaluateRealTimeOutput
    {
        public double NumberOfAlarms;
        public int AlarmsLast12Hours;
        public double AlarmsPer12Hours;
        public double UpTimeH;
        public double TotalTimeD;
        public double UpTimePercent;
        public int TripOnlyLossAlarm;
        public int PitOnlyLossAlarm;
        public int FlowOnlyLossAlarm;
        public int FlowAndPressureLossAlarm;
        public int FlowAndPitLossAlarm;
        public int PitAndPressureLossAlarm;
        public int PitAndPressureInfluxAlarm;
        public int FlowAndPitInfluxAlarm;
        public int FlowAndPressureInfluxAlarm;
        public int FlowOnlyInfluxAlarm;
        public int PitOnlyInfluxAlarm;
        public int TripOnlyInfluxAlarm;
        public int PitSlowGainAlarm;
    }
}