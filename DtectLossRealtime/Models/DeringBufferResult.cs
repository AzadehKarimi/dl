﻿namespace DtectLossRealtime.Models
{
    public class DeringBufferResult
    {
        public DeringBufferResult(double[] tbufferDStraight, double[] vbufferStraight, double dtAvgD, bool[] validDataPoints)
        {
            TbufferDStraight = tbufferDStraight;
            VbufferStraight = vbufferStraight;
            DtAvgD = dtAvgD;
            ValidDataPoints = validDataPoints;
        }

        public double[] TbufferDStraight { get; }
        public double[] VbufferStraight { get; }
        public double DtAvgD { get; }
        public bool[] ValidDataPoints { get; }
    }
}