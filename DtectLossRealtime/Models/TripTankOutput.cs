﻿namespace DtectLossRealtime.Models
{
    public struct TripTankOutput
    {
        public double NumStand;
        public double NumOkStand;
        public double VolTtLastStandM3;
        public double DepthLastStandM;
        public bool IsOkLastStand;
        public bool IsNewStandTripTank;
        public double VolTTdiffM3;
        public double LengthLastStandM;
        public double LengthStandAverageM;
        public double VolSteelLastStandM3;
        public double AreaDpLastStandM2;
        public double AreaDpAverageM2;
        public double VolTTcumM3;
        public double VolTTcumExpM3;
        public double VolTTcumErrM3;
        public double TimeLastStandD;
        public double DepthDeltaM;
        public int StateTripping;
        public bool IsOnBottom;
        public bool IsPipeSteady;
        public bool IsvolTtSteady;
    }
}