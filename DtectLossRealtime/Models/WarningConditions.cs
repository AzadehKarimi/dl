﻿namespace DtectLossRealtime.Models
{
    public struct WarningConditions
    {
        public bool HighGainVol;
        public bool HighGainFlow;
        public bool HighGainPres;
        public bool HighGainPitOnly;
        public bool HighGainFlowOnly;
        public bool HighLossVol;
        public bool HighLossFlow;
        public bool HighLossPres;
        public bool HighLossPitOnly;
        public bool HighLossFlowOnly;
    }
}