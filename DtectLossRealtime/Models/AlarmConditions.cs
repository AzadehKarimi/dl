﻿namespace DtectLossRealtime.Models
{
    public struct AlarmConditions
    {
        public bool HighGainVol;
        public bool HighGainFlow;
        public bool HighGainPres;
        public bool HighLossVol;
        public bool HighLossFlow;
        public bool HighLossPres;
        public bool Inhibit;
        public bool HoldAlarm;
        public bool AllowInfluxAlarm;
        public bool Startup;
        public bool SlowGain;
        public bool HighGainVolOnly;
        public bool HighLossVolOnly;
        public bool HighGainFlowOnly;
        public bool HighLossFlowOnly;
    }
}