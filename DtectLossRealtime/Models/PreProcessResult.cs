﻿#nullable enable
using System.Collections.Generic;
using DtectLossRealtime.Enums;

namespace DtectLossRealtime.Models
{
    public class PreProcessResult
    {
        public PreProcessResult(Dictionary<string, double> rawDataStdUnit, DtectLossRealtimeErrorCodes errorCode, DtectLossRealtimeErrorCodes warningCode)
        {
            RawDataStdUnit = rawDataStdUnit;
            ErrorCode = errorCode;
            WarningCode = warningCode;
        }

        public Dictionary<string, double> RawDataStdUnit { get; set; }
        public DtectLossRealtimeErrorCodes ErrorCode { get; set; }
        public DtectLossRealtimeErrorCodes WarningCode { get; set; }
    }
}