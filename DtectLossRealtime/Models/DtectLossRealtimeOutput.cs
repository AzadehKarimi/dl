﻿using System.Collections.Generic;
using DtectLoss.Common;
using DtectLossRealtime.Enums;

namespace DtectLossRealtime.Models
{
    public class DtectLossRealtimeOutput
    {
        public Dictionary<string, DataVariable> RawDataNonstandardUnits { get; set; } = new Dictionary<string, DataVariable>();
        public WashedData WashedData { get; set; } = new WashedData();
        public Dictionary<string,double> RawData { get; set; } = new Dictionary<string, double>();
        public bool IsCalibrating { get; set; }
        public FlowInOutOutput FlowInOutOutput { get; set; }
        public PresSPexpOutput PresSPexpOutput { get; set; }
        public TripTankOutput VolExpTripOutput { get; set; }
        public FlowlineAndPitOutput FlowlineAndPitOutput { get; set; }
        public WashedProcessOutput WashedProcessOutput { get; set; } = new WashedProcessOutput();
        public GainDetectorOutput GainDetectorOutput { get; set; }
        public double VolGainLossExpectedM3 { get; set; }
        public double VolGainLossPredModelledM3 { get; set; }
        public double VolGainLossPredErrorM3 { get; set; }
        public AlgData AlgData { get; set; } = new AlgData();
        public AlgResult AlgResult { get; set; } = new AlgResult();
        public EvaluateRealTimeOutput EvaluateRealTimeOutput { get; set; }
        public DtectLossRealtimeErrorCodes ErrorCode { get; set; }
        public DtectLossRealtimeErrorCodes WarningCode { get; set; }
        public DrillingStates SysState { get; set; }
    }
}
