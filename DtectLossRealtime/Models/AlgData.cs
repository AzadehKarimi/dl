﻿#nullable enable
namespace DtectLossRealtime.Models
{
    public class AlgData
    {
        public double TimeCurrentD { get; set; }
        public double? FlowOutExpectedLpm { get; set; }
        public double PresSpExpectedBar { get; set; }
        public int NumberPumpStops { get; set; }
        public double TimePumpsOnS { get; set; }
        public double VolTripTankCumMeasM3 { get; set; }
        public double VolTripTankCumExpectedM3 { get; set; }
        public double VolTripTankCumErrorM3 { get; set; }
        public double TimeSinceResetGlS { get; set; }
        public double VolGainLossExpectedM3 { get; set; }
        public double VolGainDetectorM3 { get; set; }
        public bool IsCalibratedExpPresSp { get; set; }
        public bool IsCalibratedExpPitVol { get; set; }
        public bool IsCalibratedExpFlowOut { get; set; }
        public bool IsCalibratedMeasFlowOut { get; set; }
        public bool IsCalibrating { get; set; }
        public bool IsDownlinking { get; set; }
        public bool IsRampingUp { get; set; }
        public bool ResetVolIOnow { get; set; }
    }
}
