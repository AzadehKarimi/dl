﻿namespace DtectLossRealtime.Models
{
    public struct FlowlineAndPitOutput
    {
        public double VolFlExpectedM3;
        public double VolGlExpectedM3;
        public double FlowPitDerLpm;
        public bool IsCalibratedExpPitVol;
    }
}