﻿namespace DtectLossRealtime.Models
{
    public struct GainDetectorOutput
    {
        public double VolGainDetectorM3;
        public double FlowDerGlAvgLpm;
    }
}