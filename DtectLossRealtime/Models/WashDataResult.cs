﻿using System.Collections.Generic;
using DtectLossRealtime.Enums;

namespace DtectLossRealtime.Models
{
    public class WashDataResult
    {
        public WashDataResult(WashedData? washedData, Dictionary<string, double>? rawData, WashedProcessOutput? washedProcessOutput, DtectLossRealtimeErrorCodes errorCode, DtectLossRealtimeErrorCodes warningCode)
        {
            WashedData = washedData;
            RawData = rawData;
            WashedProcessOutput = washedProcessOutput;
            ErrorCode = errorCode;
            WarningCode = warningCode;
        }

        public WashedData? WashedData { get; set; }
        public Dictionary<string, double>? RawData { get; set; }
        public WashedProcessOutput? WashedProcessOutput { get; set; }
        public DtectLossRealtimeErrorCodes ErrorCode { get; set; }
        public DtectLossRealtimeErrorCodes WarningCode { get; set; }
    }
}