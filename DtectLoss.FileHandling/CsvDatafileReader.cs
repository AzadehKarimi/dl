﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using DtectLoss.Common;
using Microsoft.Extensions.Logging;
using static DtectLoss.Common.Helpers;


namespace DtectLoss.FileHandling
{
    public interface ICsvDatafileReader
    {
        Dictionary<string, DataVariableInfo> ReadCsvFile(string filePath);
        Dictionary<string, DataVariableInfo> ReadCsvFileFromZip(string scenarioDataFilePath);
        Dictionary<string, DataVariableInfo> ReadTrueCsvFileFromZip(string scenarioDataFilePath);
    }

    public class CsvDatafileReader : ICsvDatafileReader
    {
        private readonly ILogger<CsvDatafileReader> _logger;

        public CsvDatafileReader(ILogger<CsvDatafileReader> logger)
        {
            _logger = logger;
        }

        public Dictionary<string, DataVariableInfo> ReadCsvFile(string filePath)
        {
            var fileStream = new FileStream(filePath, FileMode.Open);
            var lines = new List<string[]>();
            using var reader = new StreamReader(fileStream);
            return ReadCsvFile(reader);
        }
        public Dictionary<string, DataVariableInfo> ReadCsvFile(StreamReader reader)
        {
            var lines = new List<string[]>();
            var nameRow = reader.ReadLine().Split(';');
            var metadataRow = reader.ReadLine().Split(';');
            var unitRow = reader.ReadLine().Split(';');

            var variableMetadata = ParseVariableMetadata(nameRow, metadataRow, unitRow);

            string line;
            while ((line = reader.ReadLine()) != null)
            {
                lines.Add(line.Split(';'));
                
            }
            var variables =
                variableMetadata
                    .Select(x => (x.Name, x.Unit, GetAllVariableValues(x, lines)))
                    .ToDictionary(x => x.Name, x => new DataVariableInfo(x.Name, x.Unit, x.Item3));

            var results = MapMnemonicsToVariables(MnemonicDataMapping.mapping, variables);
            return results;
        }

        public Dictionary<string, DataVariableInfo> ReadTrueCsvFileFromZip(string scenarioDataFilePath)
        {
            using var fileStream = new FileStream(scenarioDataFilePath, FileMode.Open);
            using var zip = new ZipArchive(fileStream);
            var result = new Dictionary<string, DataVariableInfo>();
            var entry = zip.Entries.FirstOrDefault(x => x.Name.ToLower().Contains("true"));
            if (entry != null)
            {
                using var streamReader = new StreamReader(entry.Open());
                result = ReadCsvFile(streamReader);
            }
            return result;
        }
        public Dictionary<string, DataVariableInfo> ReadCsvFileFromZip(string scenarioDataFilePath)
        {
            try
            {
                using var fileStream = new FileStream(scenarioDataFilePath, FileMode.Open);
                using var zip = new ZipArchive(fileStream);
                var entry = zip.Entries.FirstOrDefault(x => !x.Name.ToLower().Contains("true"));
                using var streamReader = new StreamReader(entry.Open());
                return ReadCsvFile(streamReader);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message + $" while reading {scenarioDataFilePath}");
                throw;
            }
        }

        private List<DataVariable> GetAllVariableValues(DataVariableMetadata metadata, IEnumerable<string[]> lines)
        {
            var timeColumnIndex = metadata.TimeColumnIndex;
            var result =
                //(from line in lines.TakeWhile(x => !string.IsNullOrEmpty(x[timeColumnIndex]))
                (from line in lines.Where(x => !string.IsNullOrEmpty(x[timeColumnIndex]))

                select new DataVariable
                (
                    ParseIso8601Date(line[metadata.TimeColumnIndex]),
                    ParseValue(line[metadata.ValueColumnIndex])
                )).ToList();
            _logger.LogInformation($"{result.Count} time/value pairs read from {metadata.Name}");
            return result;
        }



        private static DataVariableMetadata VerifyColumnTypeAndCopyIfValid(DataVariableMetadata dataVariableMetadata, string[] metadataRow)
        {
            if (metadataRow[dataVariableMetadata.TimeColumnIndex].Equals("INDEX") && metadataRow[dataVariableMetadata.ValueColumnIndex].Equals("VALUE"))
            {
                return dataVariableMetadata;
            }
            throw new InvalidOperationException("Incorrect INDEX and VALUE columns");
        }

        /// <summary>
        /// Finds column names, column indexes and units
        /// </summary>
        /// <param name="nameRow"></param>
        /// <param name="metadataRow"></param>
        /// <param name="unitRow"></param>
        /// <returns></returns>
        public List<DataVariableMetadata> ParseVariableMetadata(string [] nameRow, string[] metadataRow, string[] unitRow)
        {
            var cols = nameRow
                .GroupBy(x => x)
                .Select((v, index) => new 
                    {Name = v.Key,
                     TimeColumnIndex = index * 2,
                     ValueColumnIndex = index * 2 + 1})
                .Select(x => new DataVariableMetadata(x.TimeColumnIndex, x.ValueColumnIndex, x.Name,unitRow[x.ValueColumnIndex]))
                .Select(x => VerifyColumnTypeAndCopyIfValid(x, metadataRow));
            return cols.ToList();
        }

        public Dictionary<string, DataVariableInfo> MapMnemonicsToVariables(List<string> mapping, Dictionary<string, DataVariableInfo> variableInfo)
        {
            var mappingDict = // a dictionary of all words with the first item on each line as value
                (from vMapping in mapping
                    let stringsInCurrentLine = vMapping.Split(';')
                    from mapped in stringsInCurrentLine
                        .Where(x => !string.IsNullOrEmpty(x))
                        .Distinct()
                        .ToDictionary(x => x, v => stringsInCurrentLine.FirstOrDefault())
                 select mapped).ToDictionary(a => a.Key, y => y.Value);

            var newMappedVariables =
                variableInfo.Keys
                    .Select(x => (mappingDict[x], variableInfo[x]))
                    .ToDictionary(x => x.Item1, v => v.Item2);
            return newMappedVariables;
        }
    }
}
