﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using OfficeOpenXml.FormulaParsing.Utilities;

namespace DtectLoss.FileHandling
{
    public static class FileUtilities
    {
        public static IEnumerable<string> GetAllZipFilesInDirectoryRecursively(string path)
        {
            return Directory.EnumerateFiles(path, "*.zip", SearchOption.AllDirectories);
        }

        public static IEnumerable<string> FilterFilesMatchingScenarioId(IEnumerable<string> files, int scenarioId)
        {
            return files.Where(x => x.Contains($"-{scenarioId}"));
        }
        public static IEnumerable<string> GetAllXlsxFilesInDirectory(string path)
        {
            return Directory.EnumerateFiles(path, "*.xlsx", SearchOption.TopDirectoryOnly);
        }
    }
}
