﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DtectLoss.Backtesting.Models;
using DtectLoss.Common;
using DtectLoss.Common.Enums;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;

namespace DtectLoss.FileHandling
{
    public interface IExcelReader
    {
        IEnumerable<ScenarioConfig> ReadXlsConfig(string filePath);
        List<BacktestScenarioRunnerResult> AggregateBacktestResults(string filePath);
    }

    public class ExcelReader : IExcelReader
    {
        private readonly ILogger<ExcelReader> _logger;

        public ExcelReader(ILogger<ExcelReader> logger)
        {
            _logger = logger;
        }

        public IEnumerable<ScenarioConfig> ReadXlsConfig(string filePath)
        {
            List<ScenarioConfig> scenarioConfigurations;
            var existingExcelFile = new FileInfo(filePath);
            using (var package = new ExcelPackage(existingExcelFile))
            {
                var worksheet = package.Workbook.Worksheets[0];
                scenarioConfigurations = ReadXlsConfigWorksheet(worksheet).ToList();
            }
            return scenarioConfigurations;
        }

        public List<BacktestScenarioRunnerResult> AggregateBacktestResults(string filePath)
        {
            var existingExcelFile = new FileInfo(filePath);
            List<BacktestScenarioRunnerResult> results;
            using (var package = new ExcelPackage(existingExcelFile))
            {
                var worksheets = package.Workbook.Worksheets;

                results = worksheets.Select(ReadBacktestEvaluationWorksheet).ToList();
            }
            return results;
        }

        private BacktestScenarioRunnerResult ReadBacktestEvaluationWorksheet(ExcelWorksheet worksheet)
        {
            var evaluation = new BacktestScenarioRunnerResult();
            evaluation.RunIdentifier = worksheet.Name;
            ReadAndParseDatasetStats(worksheet, evaluation);
            ReadAndParseTotalSimulationTimeStats(worksheet, evaluation);
            ReadAndParseTotalKpis(worksheet, evaluation);
            ReadAndParseScenarioResults(worksheet, evaluation);


            var totalGroupedAlarms = evaluation.BacktestEvaluateOutput
                .SelectMany(x => x.AlarmTypeCount)
                .GroupBy(x => x.alarmType)
                .Select(x => (x.Key, x.Sum(c => c.Item2)))
                .ToList();

            evaluation.TotalAlarmTypeCount = totalGroupedAlarms;
            var totalRowCount = worksheet.Dimension.End.Row;
            var totalColumnCount = worksheet.Dimension.End.Column;
            _logger.LogInformation($"Reading Excel Backtest Evaluation Worksheet. Column count is {totalColumnCount}. Row Count is {totalRowCount}");
            return evaluation;
        }

        private static void ReadAndParseScenarioResults(ExcelWorksheet worksheet, BacktestScenarioRunnerResult evaluation)
        {
            var indexes = GetTimeseriesIndexes(worksheet);
            var scenarioResults = Enumerable.Range(indexes.kpiIndex + 1, indexes.kpiIndexEnd - indexes.kpiIndex)
                .Select(row => ReadAndParseScenarioKpi(row, worksheet))
                .Where(x => !x.RigName.Equals("sum", StringComparison.CurrentCultureIgnoreCase))
                .ToList();
            var scenarioAlarmCounts = Enumerable
                .Range(indexes.alarmIndex + 1, indexes.alarmIndexEnd - indexes.alarmIndex)
                .Select(row => ReadAndParseScenarioAlarmCount(row, worksheet))
                .ToDictionary(x => x.name, y => y.counts);

            var fullScenarioResults = scenarioResults.Select(x =>
            {
                x.AlarmTypeCount = scenarioAlarmCounts[x.RigName];
                return x;
            });

            evaluation.BacktestEvaluateOutput = fullScenarioResults;
        }

        private static (string name, List<(AlarmType, int)> counts) ReadAndParseScenarioAlarmCount(int row, ExcelWorksheet worksheet)
        {
            var name = worksheet.Cells[row, 1].Value.ToString().Trim();
            var counts = new List<(AlarmType, int)>();
            counts.Add((AlarmType.PitOnlyLossAlarm, (int) GetValue(worksheet.Cells[row, 3].Value)));
            counts.Add((AlarmType.FlowOnlyLossAlarm, (int) GetValue(worksheet.Cells[row, 4].Value)));
            counts.Add((AlarmType.FlowAndPressureLossAlarm, (int) GetValue(worksheet.Cells[row, 5].Value)));
            counts.Add((AlarmType.FlowAndPitLossAlarm, (int) GetValue(worksheet.Cells[row, 6].Value)));
            counts.Add((AlarmType.PitAndPressureLossAlarm, (int) GetValue(worksheet.Cells[row, 7].Value)));

            var flowAndPressureLossWarning  = (int) GetValue(worksheet.Cells[row, 8].Value);
            var pitOnlyLossWarning = (int) GetValue(worksheet.Cells[row, 9].Value);
            counts.Add((AlarmType.LossWarning, flowAndPressureLossWarning + pitOnlyLossWarning));
            
            counts.Add((AlarmType.Normal, (int) GetValue(worksheet.Cells[row, 10].Value)));

            var pitOnlyInfluxWarning = (int) GetValue(worksheet.Cells[row, 11].Value);
            var flowAndPressureInfluxWarning = (int) GetValue(worksheet.Cells[row, 12].Value);
            counts.Add((AlarmType.InfluxWarning, pitOnlyInfluxWarning + flowAndPressureInfluxWarning ));


            counts.Add((AlarmType.PitAndPressureInfluxAlarm, (int) GetValue(worksheet.Cells[row, 13].Value)));
            counts.Add((AlarmType.FlowAndPitInfluxAlarm, (int) GetValue(worksheet.Cells[row, 14].Value)));
            counts.Add((AlarmType.FlowAndPressureInfluxAlarm, (int) GetValue(worksheet.Cells[row, 15].Value)));
            counts.Add((AlarmType.FlowOnlyInfluxAlarm, (int) GetValue(worksheet.Cells[row, 16].Value)));
            counts.Add((AlarmType.PitOnlyInfluxAlarm, (int) GetValue(worksheet.Cells[row, 17].Value)));

            return (name, counts);
        }


        private static BacktestEvaluateOutput ReadAndParseScenarioKpi(int row, ExcelWorksheet worksheet)
        {
            var result = new BacktestEvaluateOutput();
            result.RigName = worksheet.Cells[row, 1].Value.ToString().Trim();
            result.FalseAlamsRate = GetValue(worksheet.Cells[row, 2].Value);
            result.KeyPerformanceIndex = GetValue(worksheet.Cells[row, 3].Value);
            result.AlgNoOfCorrectAlams = (int) GetValue(worksheet.Cells[row, 4].Value);
            result.AlgNoOfFalseAlams = (int) GetValue(worksheet.Cells[row, 5].Value);
            result.TrueNoOfAlams = (int) GetValue(worksheet.Cells[row, 6].Value);
            result.AlgUpTime = GetValue(worksheet.Cells[row, 7].Value);
            result.DurationInHours = GetValue(worksheet.Cells[row, 8].Value);
            result.AlgLateAlams = (int) GetValue(worksheet.Cells[row, 9].Value);
            result.TimeToDetection = GetValue(worksheet.Cells[row, 10].Value);
            result.AccumulatedVolumeAtDetection = GetValue(worksheet.Cells[row, 11].Value);
            result.AlgName = "Voting";
            result.ResultText = "";

            return result;
        }

        private static void ReadAndParseDatasetStats(ExcelWorksheet worksheet, BacktestScenarioRunnerResult evaluation)
        {
            const int column = 1;
            const int row = 1;
            if (worksheet.Cells[row, column].Value != null)
            {
                var dataSetsText = worksheet.Cells[row, column].Value.ToString();
                evaluation.TotalNumberOfDatasets = BacktestResultStringParser.ParseCountOfDataSets(dataSetsText);
            }
            else
            {
                evaluation.TotalNumberOfDatasets = 0;
            }
        }
        private static void ReadAndParseTotalSimulationTimeStats(ExcelWorksheet worksheet, BacktestScenarioRunnerResult evaluation)
        {
            const int column = 1;
            const int row = 2;
            if (worksheet.Cells[row, column].Value != null)
            {
                var text = worksheet.Cells[row, column].Value.ToString();
                if (worksheet.Cells[row, column+1].Value != null)
                {
                    text = text + worksheet.Cells[row, column + 1].Value.ToString();
                }
                var stats = BacktestResultStringParser.ParseTotalSimulationTimeStats(text);
                evaluation.TotalSimulationTime = stats.Hours.ValidDouble();
                evaluation.TotalUptimeHours = stats.AlgUptimeHours.ValidDouble();
                evaluation.TotalUptimePercent = stats.AlgUptimePercent.ValidDouble();
            }
            else
            {
                evaluation.TotalSimulationTime = CommonConstants.MissingValueReplacement;
                evaluation.TotalUptimeHours = CommonConstants.MissingValueReplacement;
                evaluation.TotalUptimePercent = CommonConstants.MissingValueReplacement;
            }
        }
        private static void ReadAndParseTotalKpis(ExcelWorksheet worksheet, BacktestScenarioRunnerResult evaluation)
        {
            const int column = 1;
            const int row = 3;
            if (worksheet.Cells[row, column].Value != null)
            {
                var text = worksheet.Cells[row, column].Value.ToString();
                if (worksheet.Cells[row, column+1].Value != null)
                {
                    text = text + worksheet.Cells[row, column + 1].Value.ToString();
                }
                if (worksheet.Cells[row, column+2].Value != null)
                {
                    text = text + worksheet.Cells[row, column + 2].Value.ToString();
                }
                var stats = BacktestResultStringParser.ParseTotalSimulationKpis(text);
                evaluation.MeanKpi = stats.Kpi.ValidDouble();
                evaluation.TotalFar = stats.Far.ValidDouble();
                evaluation.TotalPd = stats.Pd.ValidDouble();
            }
            else
            {
                evaluation.MeanKpi = CommonConstants.MissingValueReplacement;
                evaluation.TotalFar = CommonConstants.MissingValueReplacement;
                evaluation.TotalPd = CommonConstants.MissingValueReplacement;
            }
        }

        public IEnumerable<ScenarioConfig> ReadXlsConfigWorksheet(ExcelWorksheet worksheet)
        {
            var totalRowCount = worksheet.Dimension.End.Row;
            var totalColumnCount = worksheet.Dimension.End.Column;
            _logger.LogInformation($"Reading Excel Scenario file. Column count is {totalColumnCount}. Row Count is {totalRowCount}");
            var columnHeadingIndexes = GetColumnHeadingIndexes(worksheet);
            var scenarioConfigurations = Enumerable.Range(2, totalRowCount)
                .Select(i => ParseRowToScenarioConfig(columnHeadingIndexes, worksheet, i))
                .Where(scenario => scenario != null)
                .ToList();
            _logger.LogInformation($"Scenario count: {scenarioConfigurations.Count}");
            return scenarioConfigurations!;
        }

        private ScenarioConfig? ParseRowToScenarioConfig(Dictionary<string, int> columnHeadingIndexes, ExcelWorksheet worksheet, int row)
        {
            try
            {
                var idxColumn = worksheet.Cells[row, 1];
                ScenarioConfig? scenarioConfig;
                if (idxColumn?.Value != null)
                {
                    scenarioConfig = new ScenarioConfig(
                        (int) Math.Ceiling((double)idxColumn.Value),
                        worksheet.Cells[row, columnHeadingIndexes["DtectLossUseFlowTag"]].Value.ToString(),
                        worksheet.Cells[row, columnHeadingIndexes["DtectLossUseTripTankTag"]].Value.ToString(),
                        worksheet.Cells[row, columnHeadingIndexes["DtectLossUsePitVolTag"]].Value.ToString(),
                        Convert.ToBoolean((double) worksheet.Cells[row, columnHeadingIndexes["DtectLossUseForBacktest"]].Value),
                        GetAreaValue(worksheet.Cells[row, columnHeadingIndexes["AREA_POOH"]].Value),
                        GetAreaValue(worksheet.Cells[row, columnHeadingIndexes["AREA_RIH"]].Value)
                    );

                    var algColumns =
                        columnHeadingIndexes.Where(x => x.Key.Contains("ALG_OBJ"))
                            .Where(c => worksheet.Cells[row, c.Value].Value != null &&
                                        Convert.ToBoolean((double) worksheet.Cells[row, c.Value].Value))
                            .Select(c => c.Key)
                            .ToList();
                    if (algColumns.Count > 0)
                    {
                        scenarioConfig.AlgorithmsToUse = algColumns;
                    }
                    else
                    {
                        _logger.LogInformation("Missing Algorithms to use in scenario configuration.");
                        scenarioConfig = null;
                    }
                }
                else
                {
                    _logger.LogInformation("Could not parse the current row");
                    scenarioConfig = null;
                }
                return scenarioConfig;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Could not parse the current row");
                return null;
            }
        }

        private static string GetAreaValue(object value)
        {
            var strVal = value.ToString();
            return double.TryParse(strVal, out _) ? strVal : "";
        }
        private static double GetValue(object value)
        {
            if (value != null)
            {
                if (double.TryParse(value.ToString(), out var parsedOk))
                {
                    return parsedOk.ValidDouble();
                }
                return CommonConstants.MissingValueReplacement;
            }
            return CommonConstants.MissingValueReplacement;
        }

        public Dictionary<string, int> GetColumnHeadingIndexes(ExcelWorksheet worksheet)
        {
            var totalColumnCount = worksheet.Dimension.End.Column;
            var columnHeadingIndexes =
                Enumerable.Range(1, totalColumnCount)
                    .Select(i => worksheet.Cells[1, i] != null ? ((string) worksheet.Cells[1, i].Value, i) : (null, i))
                    .Where(scenario => scenario.Item1 != null)
                    .ToDictionary(x => x.Item1, y => y.i);
            return columnHeadingIndexes!;
        }
        public static (int kpiIndex, int kpiIndexEnd, int alarmIndex, int alarmIndexEnd) GetTimeseriesIndexes(ExcelWorksheet worksheet)
        {
            var totalRowCount = worksheet.Dimension.End.Row;
            var columnNames =
                Enumerable.Range(1, totalRowCount)
                    .Select(row =>
                        worksheet.Cells[row, 1] != null
                            ? ((string) worksheet.Cells[row, 1].Value, row)
                            : (null, row))
                    .Select(row => string.IsNullOrEmpty(row.Item1) ? ("", row.row) : (row.Item1, row.row));

            var kpiIndex = columnNames.FirstOrDefault(x =>
                x.Item1.Trim().Equals("Time series", StringComparison.CurrentCultureIgnoreCase));
            var kpiIndexEnd = columnNames.FirstOrDefault(x =>
                x.Item1.Trim().Equals("", StringComparison.CurrentCultureIgnoreCase) && x.row > kpiIndex.row);

            var alarmIndex = columnNames.FirstOrDefault(x => x.Item1.Trim().Equals("Time series",
                    StringComparison.CurrentCultureIgnoreCase) && x.row > kpiIndex.row);
            var alarmIndexEnd = columnNames.FirstOrDefault(x => x.Item1.Trim().Equals("",
                    StringComparison.CurrentCultureIgnoreCase) && x.row > alarmIndex.row);

            kpiIndexEnd.row = kpiIndexEnd.row - 1;
            if (alarmIndexEnd.row == 0)
            {
                alarmIndexEnd.row = totalRowCount;
            }
            else
            {
                alarmIndexEnd.row = alarmIndexEnd.row - 1;
            }

            return (kpiIndex.row, kpiIndexEnd.row, alarmIndex.row, alarmIndexEnd.row);
        }
    }
}
