﻿using System.Collections.Generic;

namespace DtectLoss.FileHandling
{
    public static class MnemonicDataMapping
    {
        public static List<string> mapping = new List<string>
        {
            "time; TIME;;;;;",
            "depthHole;DEPTH;DMEA;GS_DMEA;HDEP;;",
            "tvdHole;;DVER;GS_DVER;HDTV;;",
            "depthBit;BITDEP;DBTM;GS_DBTM;DEP;;",
            "tvdBit;depthBitTVD;;;BDTV;;",
            "heightHook;BLOCKCOMP;BPOS;GS_BPOS;BPOS;;",
            "velRop;ROP_AVG;ROP;GS_ROP;ROPA;;",
            "weightHookload;HKLD_AVG;HKLD;GS_HKLD;HKDLAV;;",
            "weightOnBit;WOB_AVG;SWOB;GS_SWOB;WOBAVG;;",
            "torque;TORQUE_AVG;TQA;GS_TQA;TQABAV;;",
            "rpm;SURF_RPM;RPM;GS_RPM;RPMSAVG;;",
            "densityIn;MWIN;MWTI;GS_MWTI;DMIAVG;;",
            "densityOut;MWOUT;MDOA;GS_MDOA;DMOAVG;;",
            "flowOut;FLOWOUT;MFOA;GS_MFOA;FLOAVG;;",
            "flowIn;FLOWIN;TFLO;GS_TFLO;FLIAVG;;",
            "flowOutCoriolis;FLOWOUT_CORIOLIS;MPD_FLOWOUT;GS_MPD_FLOWOUT;;;",
            "flowOutCoriolisFiltered;;FLAGFOUT;GS_FLAGFOUT;;;",
            "flowOutPercent;FLOWOUTPC;MFOP;GS_MFOP;;;",
            "presSP;PUMP;SPPA;GS_SPPA;SPPAVG;;",
            "presDownHole;;;;;;",
            "presDownHoleFlowOff;;;;;;",
            "tempIn;MTIN;GS_MTIA;GS_GS_MTIA;TMIAVG;;",
            "tempOut;MTOUT;MTOA;GS_MTOA;TMOAVG;;",
            "tempDownHole;;;;;;",
            "volPitDrill;PIT_DRILL;TVA;GS_TVA;PITT;;",
            "volGainLoss;GL_DRILL;TVCA;GS_TVCA;;;",
            "volTripTank;;TTV1;GS_TTV1;TRPT;;",
            "volTripTank2;;TTV2;GS_TTV2;;;",
            "volTripTankIn;PIT_TRIPIN;;;;;",
            "volTripTankOut;PIT_TRIPOUT;;;;;",
            "densECDMWD;ACTECDX;ECD_ARC_RT;GS_ECD_ARC_RT;PWEA-T;PWEA;PWEA-T__DOWNHOLE_TIME",
            "densECDMEM;;;;PWEA-T__MEMORY_TIME;;",
            "ecdDownHole;;;;;;",
            "ecdDownHoleFlowOff;;;;;;",
            "presBHA;;;;PWPA-T;;",
            "tempBHA;TCDX;ATMP_ARC_RT;GS_ATMP_ARC_RT;PWTAZ-T;;",
            "activityBHI;RigActivityCode;activity;;;;",
            "activitySLB;;ACTC;GS_ACTC;;;",
            "activityHAL;;;;TDA;;",
            "heaveRig;RIGHEAVE;;;;;",
            "velBlock;BVEL;;;;;",
            "flowIO;FLOWRES;qIo;qIO;;;",
            "volIO;VOLRES;vIo;vIO;;;",
            "ignore;IGNORE;;;;;",
        };
    }
}