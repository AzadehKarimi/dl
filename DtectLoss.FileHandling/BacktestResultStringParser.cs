﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Sprache;

namespace DtectLoss.FileHandling
{
    public class TotalSimulationTimeStats
    {
        //private static readonly CultureInfo cultureInfo = new CultureInfo("nb-NO", false);
        private static readonly CultureInfo cultureInfo = new CultureInfo("en-US", false);
        public double Hours { get; }
        public double AlgUptimeHours { get; }
        public double AlgUptimePercent { get; }

        public TotalSimulationTimeStats(string hours, string algUptimeHours, string algUptimePercent)
        {
            if (!double.TryParse(hours, NumberStyles.AllowDecimalPoint, cultureInfo, out var hoursParsed)) throw new ArgumentException("Not a valid stats number");
            if (!double.TryParse(algUptimeHours, NumberStyles.AllowDecimalPoint, cultureInfo, out var algUptimeHoursParsed)) throw new ArgumentException("Not a valid stats number");
            if (!double.TryParse(algUptimePercent, NumberStyles.AllowDecimalPoint, cultureInfo, out var algUptimePercentParsed)) throw new ArgumentException("Not a valid stats number");
            Hours = hoursParsed;
            AlgUptimeHours = algUptimeHoursParsed;
            AlgUptimePercent = algUptimePercentParsed;
        }
    }
    public class TotalSimulationKpis
    {
        private static readonly CultureInfo cultureInfo = new CultureInfo("en-US", false);
        public double Kpi { get; }
        public double Far { get; }
        public double Pd { get; }
        public TotalSimulationKpis(string kpi, string far, string pd)
        {
            if (!double.TryParse(kpi, NumberStyles.AllowDecimalPoint, cultureInfo, out var kpiParsed)) throw new ArgumentException("Not a valid stats number");
            if (!double.TryParse(far, NumberStyles.AllowDecimalPoint, cultureInfo, out var farParsed)) throw new ArgumentException("Not a valid stats number");
            if (!double.TryParse(pd, NumberStyles.AllowDecimalPoint, cultureInfo, out var pdParsed)) throw new ArgumentException("Not a valid stats number");
            Kpi = kpiParsed;
            Far = farParsed;
            Pd = pdParsed;
        }
    }
    public static class BacktestResultStringParser
    {
        //private static readonly CultureInfo cultureInfo = new CultureInfo("nb-NO", false);
        public static Parser<string> Number = Parse.Digit.AtLeastOnce().Text().Token();
        public static Parser<string> DecimalParser = Parse.DecimalInvariant.Text().Token().Or(Parse.String("NaN").Text().Token());

        public static int ParseCountOfDataSets(string input)
        {
            var countParser =
                from stringStart in Parse.String("Statistics after running")
                from leading in Parse.WhiteSpace.Many()
                from c in Number
                from trailingSpace in Parse.WhiteSpace.Many()
                from trailingText in Parse.String("data sets:")
                select c;

            var countString = countParser.Parse(input);
            if (!int.TryParse(countString, out var result))
                throw new ArgumentException("Not a valid stats number");
            return result;
        }

        public static TotalSimulationTimeStats ParseTotalSimulationTimeStats(string input)
        {
            var statsParser =
                
                from stringStart in Parse.String("Total simulation time:") 
                from a in DecimalParser
                from string2Start in Parse.String("hours, alg up-time:")
                from b in DecimalParser
                from string3Start in Parse.String("hours ")
                from lParen in Parse.Char('(')
                from c in DecimalParser
                from percent in Parse.Char('%')
                from rParen in Parse.Char(')')
                select new TotalSimulationTimeStats(a, b, c);
            //Total simulation time: 673.72 hours, alg up-time: 642.58 hours (95.38%)
            var result = statsParser.Parse(input);
            return result;
        }

        public static TotalSimulationKpis ParseTotalSimulationKpis(string input)
        {
            //KPI: 0.35, FAR: 0.34 alarms/active hour, PD: 100.00%
            var kpiParser =
                from stringStart in Parse.String("KPI:")
                from a in DecimalParser
                from string2Start in Parse.String(", FAR:")
                from b in DecimalParser
                from string3Start in Parse.String("alarms/active hour, PD:")
                from c in DecimalParser
                from percent in Parse.Char('%')
                select new TotalSimulationKpis(a, b, c);
            var result = kpiParser.Parse(input);
            return result;
        }
    }
}
